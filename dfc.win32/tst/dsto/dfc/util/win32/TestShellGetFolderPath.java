package dsto.dfc.util.win32;

import dsto.dfc.util.win32.Win32;


/**
 * @author Matthew Phillips
 */
public class TestShellGetFolderPath
{
  public static void main (String [] args)
  {
    System.out.println
      ("Local app data = " + Win32.shellGetFolderPath (Win32.CSIDL_APPDATA));
  }
}
