package dsto.dfc.util.win32;

import dsto.dfc.logging.Log;

/**
 * @author Matthew Phillips
 */
public class TestFullScreen
{
  public static void main (String [] args) throws Exception
  {
    for (;;)
    {
      Log.trace ("Fullscreen = " + Win32.winInFullScreenMode (), TestFullScreen.class);
      
      Thread.sleep (2000);
    }
  }
}
