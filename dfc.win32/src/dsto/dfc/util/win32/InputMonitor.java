package dsto.dfc.util.win32;

/**
 * Provides global monitoring of keyboard and mouse input events.
 *
 * @todo remove Swing dependency
 * 
 * @author Matthew Phillips
 * @version $Revision$
 */
public class InputMonitor
{
  private static boolean monitorEnabled = false;

  static
  {
    Win32.initLibrary ();
  }

  private InputMonitor ()
  {
    // zip
  }

  /**
   * Enable/disable global event monitoring. Must have registered messages
   * window.
   *
   * @see #getLastInputEventTime
   */
  public static void setMonitorEnabled (boolean newValue)
  {
    if (newValue != monitorEnabled)
    {
      monitorEnabled = newValue;

      if (monitorEnabled)
        winAddInputHook ();
      else
        winRemoveInputHook ();
    }
  }

  public static boolean isMonitorEnabled ()
  {
    return monitorEnabled;
  }

  /**
   * Get the last time (in seconds) that an input event was seen.  Only
   * available when monitorEnabled == true.
   *
   * @see #setMonitorEnabled
   */
  public static long getLastInputEventTime ()
  {
    return winGetLastInputEventTime ();
  }

  private static native long winGetLastInputEventTime ();

  private static native void winAddInputHook ();

  private static native void winRemoveInputHook ();
}