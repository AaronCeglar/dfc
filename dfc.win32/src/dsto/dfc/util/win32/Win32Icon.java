package dsto.dfc.util.win32;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Wrapper for a win32 icon.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class Win32Icon
{
  private int hicon;

  static
  {
    Win32.initLibrary ();
  }

  /**
   * Create a Win32 icon by loading it from a .ICO resource stream.
   *
   * @param resName The resource name of the icon
   * (eg "/images/smiley.ico").
   */
  public Win32Icon (String resName) throws IOException
  {
    if (resName.length () == 0)
      throw new IllegalArgumentException ("Illegal empty resource name");
    
    // class loader resource resolution doesn't like '/' at start
    if (resName.charAt (0) == '/')
      resName = resName.substring (1);
    
    InputStream iconStr =
      Thread.currentThread ().getContextClassLoader ().getResourceAsStream (resName);
    
    if (iconStr != null)
    {
      BufferedInputStream byteStr = new BufferedInputStream  (iconStr);
      byte [] iconBytes = new byte [4096];

      int byteCount = byteStr.read (iconBytes);
      byteStr.close ();

      hicon = winCreateIcon (iconBytes, byteCount);

      if (hicon == -1)
        throw new IOException ("Failed to create icon from " + resName);
    } else
    {
      throw new IOException (resName + " not found");
    }
  }

  public int getHandle ()
  {
    return hicon;
  }

  public void dispose ()
  {
    if (hicon != -1)
    {
      winDestroyIcon (hicon);
      hicon = -1;
    }
  }

  private static native int winCreateIcon (byte [] iconBytes, int byteCount);

  private static native void winDestroyIcon (int handle);
}