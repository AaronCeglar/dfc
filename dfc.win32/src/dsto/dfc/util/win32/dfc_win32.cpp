/*
 * DSTO Foundation Classes native Win32 support.
 *
 * $Id$
 */
#include "stdafx.h"
#include "dfc_win32.h"

#define APPBAR_CALLBACK      (WM_APP+100)
#define TASKBAR_CALLBACK     (WM_APP+101)
#define INPUT_EVENT_CALLBACK (WM_APP+102)
#define MAX_TRAY_ICONS        5

// types
typedef struct
{
  int uid;
  char *tooltip;
  HICON hicon;
} TrayIcon;

// functions

void TrayIconHandleEvent (jint iconId, jint eventId);
void TrayIconRecreateIcons ();
BOOL TrayIconCreateIcon (TrayIcon *icon);
void PowerHandleEvent (jint wParam);
void AppBarDock (HWND hwnd, int position, int width, int height);
void AppBarUndock (HWND hwnd);
void AppBarQuerySetPos (UINT uEdge, LPRECT lprc, PAPPBARDATA pabd);
void AppBarCallback (HWND hwndAccessBar, UINT uNotifyMsg,  LPARAM lParam);
void AppBarPosChanged (PAPPBARDATA pabd);
void AppBarConstrainMove (LPWINDOWPOS pos);
void InputMonitorHandleInputEvent (WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK AppBarMessageWndProc (HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK MessageWndProc (HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK InputMonitorKeyboardHook (int hookCode, WPARAM vkCode, LPARAM msgInfo);
LRESULT CALLBACK InputMonitorMouseHook (int hookCode, WPARAM msgId, LPARAM mouseCoords);

// global vars

#pragma data_seg(".shared")
  // in shared section so that hook callbacks can get access

  HWND messageWindow = NULL;    // the current message window
  HHOOK keyboardHook = 0;       // the current global keyboard hook
  HHOOK mouseHook = 0;          // the current global mouse hook
  long lastInputEventTime = 0L; // last time an input event occurred
  int dllClientCount = 0;       // the number of clients connected to this DLL
#pragma data_seg()
#pragma comment(linker, "/SECTION:.shared,RWS") 

HINSTANCE dllInstance;       // this DLL
WNDPROC origAppBarWndProc;   // original app bar window proc
WNDPROC origMessageWndProc;  // original message window proc
int appBarPosition = -1;     // App bar position (ABE_* constants)
RECT appBarRect;             // App bar rectangle
TrayIcon trayIcons [MAX_TRAY_ICONS]; // all task bar icons
UINT taskbarRestartMessage;  // The message sent when the taskbar restarts

/*JNIEXPORT jint JNICALL JNI_OnLoad (JavaVM *vm, void *reserved)
{
  return JNI_VERSION_1_2;
}*/

void DllInit (HINSTANCE hinstance)
{
  dllClientCount++;

  dllInstance = hinstance;

  // mark all taskbar icon slots as empty
  for (int i = 0; i < MAX_TRAY_ICONS; i++)
    trayIcons [i].hicon = NULL;
}

void DllShutdown ()
{
  dllClientCount--;

  if (dllClientCount == 0 && keyboardHook != 0)
  {
    UnhookWindowsHookEx (keyboardHook);
    UnhookWindowsHookEx (mouseHook);

    keyboardHook = 0;
  }
}

BOOL APIENTRY DllMain( HINSTANCE hinstance, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved)
{
  switch (ul_reason_for_call)
	{
  case DLL_PROCESS_ATTACH:
    DllInit (hinstance);
    break;
  case DLL_THREAD_ATTACH:
    break;
  case DLL_THREAD_DETACH:
    break;
  case DLL_PROCESS_DETACH:
    DllShutdown ();
    break;
  }
   
  return TRUE;
}

/*************************** Message Window *****************************/

JNIEXPORT void JNICALL Java_dsto_dfc_util_win32_Win32_winRegisterMessageWindow
  (JNIEnv *jvm, jclass windowsClass, jint hwindow)
{
  messageWindow = (HWND)hwindow;

  taskbarRestartMessage = RegisterWindowMessage (TEXT("TaskbarCreated"));

  // hijack the window's window proc
  origMessageWndProc = (WNDPROC)GetWindowLong (messageWindow, GWL_WNDPROC);
  SetWindowLong (messageWindow, GWL_WNDPROC, (LONG)MessageWndProc);
}

LRESULT CALLBACK MessageWndProc
  (HWND hwnd,        
   UINT uMsg,        
   WPARAM wParam,    
   LPARAM lParam)
{ 
  if (uMsg == TASKBAR_CALLBACK)
  {
    TrayIconHandleEvent ((jint)wParam, (jint)lParam);

    return TRUE;
  } else if (uMsg == INPUT_EVENT_CALLBACK)
  {
    // a mouse or keyboard event has been noticed: see InputMonitorHandleInputEvent()
    lastInputEventTime = time (NULL);

    return TRUE;
  } else
  {
    // messages that get passed down to original wndproc go here

    if (uMsg == WM_POWERBROADCAST)
    {
      if (wParam == PBT_APMRESUMESUSPEND || wParam == PBT_APMSUSPEND)
        PowerHandleEvent (wParam);
    } else if (uMsg == taskbarRestartMessage)
    {
      TrayIconRecreateIcons ();
    }
  
    return CallWindowProc (origMessageWndProc, hwnd, uMsg, wParam, lParam);
  }
}

/************************** Callbacks ***********************************/

static JavaVM *globalJvm = NULL;

static jclass globalTaskbarClass = NULL;
static jclass globalWin32Class = NULL;
static jmethodID taskBarCallback = NULL;
static jmethodID powerCallback = NULL;

static void connectVM (JNIEnv *env)
{
  if (globalJvm == NULL)
    env->GetJavaVM (&globalJvm);
}

/************************** Misc ****************************************/

JNIEXPORT void JNICALL Java_dsto_dfc_util_win32_Win32_winSetAlwaysOnTop
  (JNIEnv *jvm, jclass javaClass, jint hwnd, jboolean ontop)
{
  if (ontop == JNI_TRUE)
  {
    SetWindowPos ((HWND)hwnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
  } else
  {
    SetWindowPos ((HWND)hwnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
  }
}

JNIEXPORT void JNICALL Java_dsto_dfc_util_win32_Win32_winSetOnTop
  (JNIEnv *, jclass, jint hwnd, jboolean ontop)
{
  if (ontop == JNI_TRUE)
  {
    SetWindowPos ((HWND)hwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
  } else
  {
    SetWindowPos ((HWND)hwnd, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
  }
}

JNIEXPORT jboolean JNICALL Java_dsto_dfc_util_win32_Win32_winInFullScreenMode
  (JNIEnv *, jclass)
{
  int screenWidth = GetSystemMetrics (SM_CXSCREEN);
  int screenHeight = GetSystemMetrics (SM_CYSCREEN);
  
  //HWND foreground = GetForegroundWindow ();
  //HWND foreground = GetTopWindow (NULL);

  // detect fullscreen window from looking at what is under center of screen
  // the methods above simply do not work
  POINT p;
  p.x = screenWidth / 2;
  p.y = screenHeight / 2;
  HWND foreground = WindowFromPoint (p);

  if (foreground != NULL)
  {
    // walk up the parent chain
    HWND parent;

    do
    {
      parent = GetParent (foreground);

      if (parent != NULL)
        foreground = parent;
    } while (parent != NULL);  

    WINDOWINFO info;

    GetWindowInfo (foreground, &info);

    return (info.rcWindow.right - info.rcWindow.left) >= screenWidth &&
           (info.rcWindow.bottom - info.rcWindow.top) >= screenHeight ? JNI_TRUE : JNI_FALSE;
  } else
  {
    // have to assume not in FS
    return JNI_FALSE;
  }
}

JNIEXPORT void JNICALL Java_dsto_dfc_util_win32_Win32_winRegisterWin32Class
  (JNIEnv *env, jclass win32Class)
{
  connectVM (env);

  powerCallback = 
    env->GetStaticMethodID (win32Class, "powerCallback", "(I)V");
    
  globalWin32Class = win32Class;
}

/*
JNIEXPORT void JNICALL Java_dsto_dfc_util_win32_Win32_winSetTransparent
  (JNIEnv *, jclass, jint hwnd, int transparency)
{
  if (transparency < 100)
  {
    SetWindowLong
      ((HWND)hwnd, GWL_EXSTYLE, GetWindowLong ((HWND)hwnd, GWL_EXSTYLE) | WS_EX_LAYERED);

    SetLayeredWindowAttributes ((HWND)hwnd, 0, (255 * transparency) / 100, LWA_ALPHA);
  } else
  {
    SetWindowLong ((HWND)hwnd, GWL_EXSTYLE, GetWindowLong ((HWND)hwnd, GWL_EXSTYLE) & ~WS_EX_LAYERED);

    RedrawWindow ((HWND)hwnd, NULL, NULL, RDW_ERASE | RDW_INVALIDATE | RDW_FRAME | RDW_ALLCHILDREN);
  }
}
*/

/************************** Power mode support **************************/

static void PowerHandleEvent (jint wParam)
{
  JNIEnv *env;

  globalJvm->GetEnv ((void **)&env, JNI_VERSION_1_1);

  env->CallStaticVoidMethod (globalWin32Class, powerCallback, wParam);
}

/************************** Icon support ********************************/

JNIEXPORT jint JNICALL Java_dsto_dfc_util_win32_Win32Icon_winCreateIcon
  (JNIEnv *jvm, jclass iconClass, jbyteArray iconByteArray, jint byteCount)
{
  jbyte *iconBytes = new jbyte [byteCount];
  jvm->GetByteArrayRegion (iconByteArray, 0, byteCount, iconBytes);

  // find icon number in resource dir
  int iconNum = LookupIconIdFromDirectoryEx
    ((PBYTE)iconBytes, TRUE,0, 0, LR_DEFAULTCOLOR);

  HICON hicon = CreateIconFromResourceEx
    (
     (PBYTE)iconBytes + iconNum,  // pointer to icon or cursor bits
     byteCount - iconNum, // number of bytes in bit buffer
     TRUE,      // icon or cursor flag
     0x00030000,      // format version
     16, 16, LR_DEFAULTCOLOR
     );

  delete iconBytes;

  return (hicon == NULL) ? -1 : (jint)hicon;
}


JNIEXPORT void JNICALL Java_dsto_dfc_util_win32_Win32Icon_winDestroyIcon
  (JNIEnv *jvm, jclass iconClass, jint hicon)
{
  DestroyIcon ((HICON)hicon);
}

/************************* TaskBar support *****************************/

JNIEXPORT jboolean JNICALL Java_dsto_dfc_util_win32_TaskBar_winAddTrayIcon
  (JNIEnv *env, jclass taskBarClass, jint hicon, jstring tooltip, jint uid)
{
  jboolean isCopy;
  const char *tipUTF = env->GetStringUTFChars (tooltip, &isCopy);
  int tipLength = env->GetStringUTFLength (tooltip);

  // find an empty icon slot
  TrayIcon *icon = NULL;

  for (int i = 0; i < MAX_TRAY_ICONS && icon == NULL; i++)
  {
    if (trayIcons [i].hicon == NULL)
    {
      icon = trayIcons + i;

      char *tooltip = new char [tipLength + 1];
      strncpy (tooltip, tipUTF, tipLength);
      tooltip [tipLength] = '\0';

      icon->hicon = (HICON)hicon;
      icon->tooltip = tooltip;
      icon->uid = uid;
    }
  }

  if (icon == NULL)
    return JNI_FALSE;

  BOOL result = TrayIconCreateIcon (icon);

  env->ReleaseStringUTFChars (tooltip, tipUTF);

  if (taskBarCallback == NULL)
  {
    connectVM (env);

    taskBarCallback = 
      env->GetStaticMethodID (taskBarClass, "taskbarCallback", "(II)V");
    
    globalTaskbarClass = taskBarClass;
  }

  return result == TRUE ? JNI_TRUE : JNI_FALSE;
}

JNIEXPORT void JNICALL Java_dsto_dfc_util_win32_TaskBar_winUpdateTrayIcon
  (JNIEnv *env, jclass taskBarClass, jint uid, jint hicon)
{
  // find icon
  TrayIcon *icon = NULL;

  for (int i = 0; i < MAX_TRAY_ICONS && icon == NULL; i++)
  {
    if (trayIcons [i].hicon != NULL && trayIcons [i].uid == uid)
      icon = trayIcons + i;
  }

  if (icon != NULL)
  {
    icon->hicon = (HICON)hicon;

    NOTIFYICONDATA tnid; 
    
    tnid.cbSize = sizeof(NOTIFYICONDATA); 
    tnid.hWnd = messageWindow;
    tnid.uID = uid; 
    tnid.uFlags = NIF_ICON; 
    tnid.hIcon = (HICON)hicon;
    
    Shell_NotifyIcon (NIM_MODIFY, &tnid); 
  }
}

JNIEXPORT void JNICALL Java_dsto_dfc_util_win32_TaskBar_winRemoveTrayIcon
(JNIEnv *jvm, jclass taskBarClass, jint uid)
{
  // find icon
  TrayIcon *icon = NULL;
  
  for (int i = 0; i < MAX_TRAY_ICONS && icon == NULL; i++)
  {
    if (trayIcons [i].hicon != NULL && trayIcons [i].uid == uid)
    {
      icon = trayIcons + i;

      icon->hicon = NULL;
    }
  }

  if (icon != NULL)
  {
    NOTIFYICONDATA tnid; 
    
    tnid.cbSize = sizeof(NOTIFYICONDATA); 
    tnid.hWnd = messageWindow;
    tnid.uID = uid; 
    
    Shell_NotifyIcon (NIM_DELETE, &tnid);

    delete icon->tooltip;
  }
}

// create a tray icon using Shell_NotifyIcon
BOOL TrayIconCreateIcon (TrayIcon *icon)
{
  NOTIFYICONDATA tnid; 
  
  tnid.cbSize = sizeof(NOTIFYICONDATA); 
  tnid.hWnd = messageWindow;
  tnid.uID = icon->uid;
  tnid.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP; 
  tnid.uCallbackMessage = TASKBAR_CALLBACK; 
  tnid.hIcon = (HICON)icon->hicon;
  
  lstrcpyn (tnid.szTip, icon->tooltip, sizeof (tnid.szTip));
  tnid.szTip [min (sizeof (tnid.szTip) - 1, strlen (icon->tooltip))] = '\0';

  return Shell_NotifyIcon (NIM_ADD, &tnid); 
}

// re-create all icons
void TrayIconRecreateIcons ()
{
  for (int i = 0; i < MAX_TRAY_ICONS; i++)
  {
    TrayIcon *icon = trayIcons + i;

    if (icon->hicon != NULL)
      TrayIconCreateIcon (icon);
  }
}

void TrayIconHandleEvent (jint iconId, jint eventId)
{
  JNIEnv *env;

  globalJvm->GetEnv ((void **)&env, JNI_VERSION_1_1);

  env->CallStaticVoidMethod (globalTaskbarClass, taskBarCallback, iconId, eventId);
}

/********************** AppBar support **************************/

static BOOL inFullScreen = FALSE;

JNIEXPORT void JNICALL Java_dsto_dfc_util_win32_Win32_winAppbarDock
  (JNIEnv *jvm, jclass javaClass, jint hwnd, jint position, jint width, jint height)
{
  AppBarDock ((HWND)hwnd, position, width, height);
}

JNIEXPORT void JNICALL Java_dsto_dfc_util_win32_Win32_winAppbarUndock
  (JNIEnv *jvm, jclass javaClass, jint hwnd)
{
  AppBarUndock ((HWND)hwnd);
}

void AppBarDock (HWND hwnd, int position, int width, int height)
{
  appBarPosition = -1;

  // set window's request width/height
  SetWindowPos (hwnd, NULL, 0, 0, width, height, SWP_NOMOVE | SWP_NOZORDER);

  APPBARDATA abd; 

  // Specify the structure size and handle to the appbar. 
  abd.cbSize = sizeof(APPBARDATA); 
  abd.hWnd = hwnd; 
  abd.uCallbackMessage = APPBAR_CALLBACK;

  SHAppBarMessage (ABM_NEW, &abd);

  // Store app bar size and pos
  GetWindowRect (hwnd, &appBarRect);

  AppBarQuerySetPos (position, &appBarRect, &abd);

  // hijack the window's window proc to add APPBAR_CALLBACK message handling
  origAppBarWndProc = (WNDPROC)GetWindowLong (hwnd, GWL_WNDPROC);
  SetWindowLong ((HWND)hwnd, GWL_WNDPROC, (LONG)AppBarMessageWndProc);

  appBarPosition = position;
}

void AppBarUndock (HWND hwnd)
{
  appBarPosition = -1;

  // restore old wndproc
  SetWindowLong (hwnd, GWL_WNDPROC, (LONG)origAppBarWndProc);

  APPBARDATA abd; 

  // remove from app bar list
  abd.cbSize = sizeof(APPBARDATA); 
  abd.hWnd = hwnd; 

  SHAppBarMessage (ABM_REMOVE, &abd);
}

/*
 * Message handler for app bar windows.
 */
LRESULT CALLBACK AppBarMessageWndProc
  (HWND hwnd,        
   UINT uMsg,        
   WPARAM wParam,    
   LPARAM lParam)
{
  APPBARDATA abd; 

  // Specify the structure size and handle to the appbar. 
  abd.cbSize = sizeof(APPBARDATA); 
  abd.hWnd = hwnd; 

  if (uMsg == APPBAR_CALLBACK)
  {
    AppBarCallback (hwnd, wParam, lParam);

    return TRUE;
  } else
  {
    switch (uMsg)
    { 
    case WM_ACTIVATE:
      SHAppBarMessage (ABM_ACTIVATE, &abd); break;
    case WM_WINDOWPOSCHANGED:
      SHAppBarMessage (ABM_WINDOWPOSCHANGED, &abd); break;
	case WM_WINDOWPOSCHANGING:
      AppBarConstrainMove ((LPWINDOWPOS)lParam); break;
    }

    return CallWindowProc (origAppBarWndProc, hwnd, uMsg, wParam, lParam);
  }
}

// AppBarQuerySetPos - sets the size and position of an appbar. 
// uEdge - screen edge to which the appbar is to be anchored 
// lprc - current bounding rectangle of the appbar 
// pabd - address of the APPBARDATA structure with the hWnd and 
//     cbSize members filled 
void AppBarQuerySetPos (UINT uEdge, LPRECT lprc, PAPPBARDATA pabd)
{ 
  int iHeight = 0; 
  int iWidth = 0; 

  pabd->rc = *lprc; 
  pabd->uEdge = uEdge; 

  // Copy the screen coordinates of the appbar's bounding 
  // rectangle into the APPBARDATA structure. 
  if ((uEdge == ABE_LEFT) || (uEdge == ABE_RIGHT))
  { 
    iWidth = pabd->rc.right - pabd->rc.left; 
    pabd->rc.top = 0; 
    pabd->rc.bottom = GetSystemMetrics(SM_CYSCREEN); 

    // push rect to edge
    if (uEdge == ABE_RIGHT)
    {
      pabd->rc.right = GetSystemMetrics(SM_CXSCREEN);
      pabd->rc.left = pabd->rc.right - iWidth;
    } else
    {
      pabd->rc.left = 0;
      pabd->rc.right = iWidth;
    }
  } else
  { 
    iHeight = pabd->rc.bottom - pabd->rc.top; 
    pabd->rc.left = 0; 
    pabd->rc.right = GetSystemMetrics(SM_CXSCREEN); 

    // push rect to edge
    if (uEdge == ABE_BOTTOM)
    {
      pabd->rc.bottom = GetSystemMetrics(SM_CYSCREEN);
      pabd->rc.top = pabd->rc.bottom - iHeight;
    } else
    {
      pabd->rc.top = 0;
      pabd->rc.bottom = iHeight;
    }
  } 

  // Query the system for an approved size and position. 
  SHAppBarMessage (ABM_QUERYPOS, pabd); 

  // Adjust the rectangle, depending on the edge to which the 
  // appbar is anchored. 
  switch (uEdge)
  { 
  case ABE_LEFT: 
    pabd->rc.right = pabd->rc.left + iWidth; 
    break; 

  case ABE_RIGHT: 
    pabd->rc.left = pabd->rc.right - iWidth; 
    break; 

  case ABE_TOP: 
    pabd->rc.bottom = pabd->rc.top + iHeight; 
    break; 

  case ABE_BOTTOM: 
    pabd->rc.top = pabd->rc.bottom - iHeight; 
    break; 
  } 

  // Pass the final bounding rectangle to the system. 
  SHAppBarMessage (ABM_SETPOS, pabd); 

  // store new location
  appBarRect.left = pabd->rc.left;
  appBarRect.top = pabd->rc.top;
  appBarRect.bottom = pabd->rc.bottom;
  appBarRect.right = pabd->rc.right;

  // Move and size the appbar so that it conforms to the 
  // bounding rectangle passed to the system. 
  MoveWindow (pabd->hWnd, pabd->rc.left, pabd->rc.top, 
              pabd->rc.right - pabd->rc.left, 
              pabd->rc.bottom - pabd->rc.top, TRUE); 
}

/*
 * Handles app bar events.
 */
void AppBarCallback (HWND hwndAccessBar, UINT uNotifyMsg, LPARAM lParam)
{ 
  APPBARDATA abd; 
  UINT uState;

  abd.cbSize = sizeof(abd); 
  abd.hWnd = hwndAccessBar; 

  switch (uNotifyMsg)
  { 
  case ABN_STATECHANGE: 

    uState = SHAppBarMessage (ABM_GETSTATE, &abd);

    // Check to see if the taskbar's always-on-top state has 
    // changed and, if it has, change the appbar's state 
    // accordingly. 
    SetWindowPos
      (hwndAccessBar, 
       (ABS_ALWAYSONTOP & uState) ? HWND_TOPMOST : HWND_BOTTOM, 
       0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
	    
    break; 


  case ABN_FULLSCREENAPP: 

    // for some reason on Windows XP we get a lot of random FS
	// app stop messages, plus the result of ABM_GETSTATE seems
	// to be wrong. Need to screen bogus ones by saving previous
	// state
    if (inFullScreen != lParam)
	{
	  inFullScreen = lParam;

	  uState = SHAppBarMessage (ABM_GETSTATE, &abd); 

	  SetWindowPos
	    (hwndAccessBar, 
		 lParam ? HWND_BOTTOM : HWND_TOPMOST, 
	     0, 0, 0, 0, 
	     SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE); 
	}

	break;


  case ABN_POSCHANGED: 

    // The taskbar or another appbar has changed its 
    // size or position. 
    AppBarPosChanged (&abd); 

    break; 
  } 
}

/*
 * Called by AppBarCallback when an appbar window has changed position.
 */
void AppBarPosChanged (PAPPBARDATA pabd) 
{ 
  RECT rc; 
  int iHeight; 
  int iWidth; 

  rc.top = 0; 
  rc.left = 0; 
  rc.right = GetSystemMetrics(SM_CXSCREEN); 
  rc.bottom = GetSystemMetrics(SM_CYSCREEN); 

  iHeight = appBarRect.bottom - appBarRect.top; 
  iWidth = appBarRect.right - appBarRect.left; 

  switch (appBarPosition)
  { 
  case ABE_TOP: 
    rc.bottom = rc.top + iHeight; 
    break; 

  case ABE_BOTTOM: 
    rc.top = rc.bottom - iHeight; 
    break; 

  case ABE_LEFT: 
    rc.right = rc.left + iWidth; 
    break; 

  case ABE_RIGHT: 
    rc.left = rc.right - iWidth; 
    break; 
  } 
  
  AppBarQuerySetPos (appBarPosition, &rc, pabd); 
}

/*
 * Used to stop strange jumping of app bar above its requested position. Size is also
 * constrained, since otherwise it will expand to fit entire virtual desktop on multi
 * monitor systems. What is actually causing this remains a mystery.
 */
void AppBarConstrainMove (LPWINDOWPOS pos)
{
  if (appBarPosition != -1)
  {
    pos->x = appBarRect.left;
    pos->y = appBarRect.top;
    pos->cx = appBarRect.right - appBarRect.left;
    pos->cy = appBarRect.bottom - appBarRect.top;
  }
}

/*************************** Input monitor support **************************/


JNIEXPORT jlong JNICALL Java_dsto_dfc_util_win32_InputMonitor_winGetLastInputEventTime
  (JNIEnv *jvm, jclass inputMonitor)
{
  return lastInputEventTime;
}

JNIEXPORT void JNICALL Java_dsto_dfc_util_win32_InputMonitor_winAddInputHook
  (JNIEnv *jvm, jclass inputMonitor)
{
  if (keyboardHook == 0)
  {
    keyboardHook = SetWindowsHookEx (WH_KEYBOARD, InputMonitorKeyboardHook, dllInstance, 0);
    mouseHook = SetWindowsHookEx (WH_MOUSE, InputMonitorMouseHook, dllInstance, 0);
  }
}

JNIEXPORT void JNICALL Java_dsto_dfc_util_win32_InputMonitor_winRemoveInputHook
  (JNIEnv *jvm, jclass inputMonitor)
{
  if (keyboardHook != 0)
  {
    UnhookWindowsHookEx (keyboardHook);
    UnhookWindowsHookEx (mouseHook);

    keyboardHook = 0;
    mouseHook = 0;
  }
}

LRESULT CALLBACK InputMonitorKeyboardHook
  (int hookCode, WPARAM vkCode, LPARAM msgInfo)
{
  if (hookCode == HC_ACTION && !(msgInfo & 0x80000000))
    InputMonitorHandleInputEvent (vkCode, msgInfo);
 
  return CallNextHookEx (keyboardHook, hookCode, vkCode, msgInfo);
}

LRESULT CALLBACK InputMonitorMouseHook
  (int hookCode, WPARAM msgId, LPARAM mouseCoords)
{
  if (hookCode == HC_ACTION)
    InputMonitorHandleInputEvent (msgId, mouseCoords);

  return CallNextHookEx (mouseHook, hookCode, msgId, mouseCoords);
}
 

void InputMonitorHandleInputEvent (WPARAM wParam, LPARAM lParam)
{
  // alternative way is to post message which is handled in app thread
  // PostMessage (messageWindow, INPUT_EVENT_CALLBACK, wParam, lParam);
  lastInputEventTime = time (NULL);
}

JNIEXPORT jstring JNICALL Java_dsto_dfc_util_win32_Win32_shellGetFolderPath
  (JNIEnv *env, jclass clazz, jint type)
{
  char szPath [MAX_PATH];

  if (SUCCEEDED (SHGetFolderPath (NULL, type|CSIDL_FLAG_CREATE, NULL, 0, szPath)))
  {
    return env->NewStringUTF (szPath);
  } else
  {
    return NULL;
  }
}