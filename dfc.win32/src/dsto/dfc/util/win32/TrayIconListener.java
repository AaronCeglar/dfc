package dsto.dfc.util.win32;

/**
 * Defines a listener to events on a TaskBar.TrayIcon.
 * 
 * @author mpp
 * @version $Revision$
 */
public interface TrayIconListener
{
  /**
   * Handle a mouse event in a tray icon. Note that the thread that this is
   * called from is unknown.
   * 
   * @param icon The icon.
   * @param event The mouse event. One of the Win32.WM_* constants.
   */
  public void handleMouseEvent (TaskBar.TrayIcon icon, int event);
}
