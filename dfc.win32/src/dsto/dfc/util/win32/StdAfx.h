// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

// Needed to get things like GetWindowInfo () out of winuser.h
#define WINVER 0x0501
#define _WIN32_WINNT 0x0501

#if !defined(AFX_STDAFX_H__7DDC1FEB_FF12_11D4_B970_42EA9B000000__INCLUDED_)
#define AFX_STDAFX_H__7DDC1FEB_FF12_11D4_B970_42EA9B000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


// Insert your headers here

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

#include <windows.h>
#include <winuser.h>
#include <shellapi.h>
#include <shlobj.h>
#include <time.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include <jawt.h>
#include <jawt_md.h>


// TODO: reference additional headers your program requires here

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__7DDC1FEB_FF12_11D4_B970_42EA9B000000__INCLUDED_)
