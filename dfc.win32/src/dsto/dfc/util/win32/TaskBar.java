package dsto.dfc.util.win32;

import java.util.ArrayList;

/**
 * Utilties for accessing the Windows taskbar, including the tray.
 *
 * @version $Revision$
 */
public class TaskBar
{  
  protected static int uidCounter = 1;
  private static ArrayList<TrayIcon> trayIcons = new ArrayList<TrayIcon> (1);

  static
  {
    Win32.initLibrary ();
  }

  public static TrayIcon addTrayIcon (Win32Icon icon, String tooltip)
  {
    return addTrayIcon (icon, tooltip, null);
  }
  
  /**
   * 
   * @exception IllegalArgumentException If there are too many icons.
   */
  public static TrayIcon addTrayIcon (Win32Icon icon, String tooltip,
                                      TrayIconListener listener)
  {
    TrayIcon trayIcon = new TrayIcon (icon, tooltip, listener);
    
    boolean result =
      winAddTrayIcon (icon.getHandle (), tooltip, trayIcon.uid);
    
    if (result)
      trayIcons.add (trayIcon);
    else
      throw new IllegalArgumentException ("Too many icons");    

    return trayIcon;
  }

  public static void removeTrayIcon (TrayIcon trayIcon)
  {
    winRemoveTrayIcon (trayIcon.uid);
    
    trayIcons.remove (trayIcon);
  }

  protected static void taskbarCallback (int uid, int event)
  {
    for (int i = 0; i < trayIcons.size (); i++)
    {
      TrayIcon icon = trayIcons.get (i);
      
      if (icon.uid == uid)
      {
        if (icon.listener != null)
          icon.listener.handleMouseEvent (icon, event);

        break;
      }
    }
  }
  
  protected static native boolean winAddTrayIcon (int hicon,
                                                      String tooltip, int uid);

  protected static native void winUpdateTrayIcon (int uid, int handle);
  
  protected static native void winRemoveTrayIcon (int uid);

  /**
   * Represents an icon added to the tray.
   *
   * @see #addTrayIcon
   */
  public static final class TrayIcon
  {
    protected Win32Icon icon;
    protected String tooltip;
    protected int uid;
    protected TrayIconListener listener;

    protected TrayIcon (Win32Icon icon, String tooltip, TrayIconListener listener)
    {
      this.icon = icon;
      this.tooltip = tooltip;
      this.uid = uidCounter++;
      this.listener = listener;
    }
    
    public Win32Icon getIcon ()
    {
      return icon;
    }
    
    public void setIcon (Win32Icon newIcon)
    {
      this.icon = newIcon;
      
      winUpdateTrayIcon (uid, icon.getHandle ());
    }
  }
}
