package dsto.dfc.util.win32;

import dsto.dfc.logging.Log;

/**
 * General Win32 initialisation and utilities.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public final class Win32
{
  public static final int WIN32_OK = 0;
  public static final int WIN32_NOK_PLATFORM = 1;
  public static final int WIN32_NOK_LIBRARY = 2;
  public static final int WIN32_NOK_OTHER = 3;

  // constants below are from winuser.h
  
  public static final int WM_MOUSEFIRST    =              0x0200;
  public static final int WM_MOUSEMOVE     =              0x0200;
  public static final int WM_LBUTTONDOWN   =              0x0201;
  public static final int WM_LBUTTONUP     =              0x0202;
  public static final int WM_LBUTTONDBLCLK =              0x0203;
  public static final int WM_RBUTTONDOWN   =              0x0204;
  public static final int WM_RBUTTONUP     =              0x0205;
  public static final int WM_RBUTTONDBLCLK =              0x0206;
  public static final int WM_MBUTTONDOWN   =              0x0207;
  public static final int WM_MBUTTONUP     =              0x0208;
  public static final int WM_MBUTTONDBLCLK =              0x0209;

  // constants below correspond to the ABE_* constants in shellapi.h
  
  public static final int DESKTOP_LEFT = 0;
  public static final int DESKTOP_TOP = 1;
  public static final int DESKTOP_RIGHT = 2;
  public static final int DESKTOP_BOTTOM = 3;

  // contants below are from the PBT_* constants in winuser.h
  public static final int PBT_APMSUSPEND = 0x0004;
  public static final int PBT_APMRESUMESUSPEND = 0x0007;
  
  // contants below are from the CSIDL_* constants in shfolder.h
  public static final int CSIDL_APPDATA = 0x001A;
  public static final int CSIDL_LOCAL_APPDATA = 0x001C;
  
  private static int win32State;
  
  private static PowerListener powerListener;
    
  static
  {
    win32State = -1;
    initLibrary ();
  }

  private Win32 ()
  {
    // zip
  }

  /**
   * Load and initialize the Win32 DLL.  May be called more than once -
   * all calls after first have no effect.
   *
   * @see #getWin32State
   */
  public static void initLibrary ()
  {
    if (win32State != -1)
      return;

    // assume failure
    win32State = WIN32_NOK_OTHER;

    if (System.getProperty ("os.name", "other").startsWith ("Windows"))
    {
      try
      {
        System.loadLibrary ("dfc_win32");

        win32State = WIN32_OK;
      } catch (UnsatisfiedLinkError ex)
      {
        Log.diagnostic ("Failed to load DFC Win32 DLL", Win32.class, ex);
        
        win32State = WIN32_NOK_LIBRARY;
      } catch (Throwable ex)
      {
        win32State = WIN32_NOK_OTHER;
      }
    } else
    {
      win32State = WIN32_NOK_PLATFORM;
    }
  }
  
  /**
   * Get the state of the Win32 library.
   *
   * @return One of WIN32_OK (loaded OK), WIN32_NOK_LIBRARY (not OK - could
   * not find library), WIN32_NOK_PLATFORM (not OK - not a Win32 platform) or
   * WIN32_NOK_OTHER (not OK - some other error occurred during
   * initialisation).
   *
   * @see #isWin32Available
   */
  public static int getWin32State ()
  {
    return win32State;
  }

  /**
   * True if Win32 calls are available.  Synonym for
   * <code>getWin32State () == WIN32_OK</code>
   *
   * @see #getWin32State
   */
  public static boolean isWin32Available ()
  {
    return win32State == WIN32_OK;
  }
  
  public static void setPowerListener (PowerListener l)
  {
    powerListener = l;
    
    winRegisterWin32Class ();
  }

  protected static void powerCallback (int pbtEvent)
  {
    if (powerListener != null)
    {
      if (pbtEvent == PBT_APMSUSPEND)
        powerListener.powerSuspend ();
      else if (pbtEvent == PBT_APMRESUMESUSPEND)
        powerListener.powerResume ();
    }
  }
  
  /**
   * Call the Win32 SHGetFolderPath () function to get a local folder location.
   * 
   * @param type The type of location. One of the CSIDL_* constants.
   * 
   * @return The path, or null if the call failed. 
   */
  public static native String shellGetFolderPath (int type);
  
  // native windows functions
  // (public for use by SWT/Swing modules - do not call from client code)
  
  public static native void winSetAlwaysOnTop (int hwnd, boolean onTop);

  public static native void winSetOnTop (int hwnd, boolean onTop);
  
  // @todo decide whether to complete this
  // public static native void winSetTransparent (int hwnd, int percent);
  
  public static native void winAppbarDock (int hwnd, int position,
                                              int width, int height);

  public static native void winAppbarUndock (int hwnd);

  public static native boolean winInFullScreenMode ();
  
  public static native void winRegisterMessageWindow (int hwnd);
  
  private static native void winRegisterWin32Class ();
}
