package dsto.dfc.util.win32;

/**
 * @author phillipm
 */
public interface PowerListener
{
  public void powerSuspend ();
  
  public void powerResume ();
}
