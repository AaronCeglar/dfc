@echo off
if "%OS%"=="Windows_NT" setlocal

if not defined DFC_HOME echo Please set DFC_HOME && goto end

set CLASSPATH=%DFC_HOME%\lib\dfc.jar;%CLASSPATH%

call "%DFC_HOME%\build.bat" %1 %2 %3 %4 %5

:end