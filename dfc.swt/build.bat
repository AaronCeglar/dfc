@echo off
if "%OS%"=="Windows_NT" setlocal

if not defined MODULES_HOME set MODULES_HOME=..
if not exist %MODULES_HOME%\dfc.core echo Please set MODULES_HOME && goto exit

set CLASSPATH=%MODULES_HOME%\dfc.core\lib\dfc.jar;%CLASSPATH%
set JAVA_OPTIONS=-Ddsto.c2d.storepass=%DSTO_C2D_STOREPASS%

set DFC_HOME=%MODULES_HOME%\dfc.core

call "%MODULES_HOME%\dfc.core\build.bat" %1 %2 %3 %4 %5

:end
