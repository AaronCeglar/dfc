package dsto.dfc.swt.text;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.jface.preference.JFacePreferences;
import org.eclipse.jface.resource.JFaceResources;

import dsto.dfc.swt.DFC_SWT;

public class TestStyledTextLinks
{

  /**
   * @param args
   */
  public static void main (String [] args)
  {
    Display display = new Display ();
    
    // JFaceColors.getHyperlinkText () returns black (default) outside Eclipse
    JFaceResources.getColorRegistry ().put
      (JFacePreferences.HYPERLINK_COLOR,
       display.getSystemColor (SWT.COLOR_BLUE).getRGB ());
    
    Shell shell = new Shell (display);
    shell.setText ("Styled Text Links");
    shell.setLayout (new GridLayout (1, true));
    
    // styled text
    StyledText textField = new StyledText (shell, SWT.WRAP | SWT.V_SCROLL | SWT.BORDER);
    GridData layoutData = new GridData (GridData.FILL_BOTH);
    layoutData.widthHint = 300;
    layoutData.heightHint = 100;
    textField.setLayoutData (layoutData);
    
    DFC_SWT.setMargins (textField, 4, 2, 4, 2);
    
    StyledTextLinkOTron links = new StyledTextLinkOTron (textField);
    links.openInBrowser (false);
    links.addListener (SWT.Selection, new Listener ()
    {
      public void handleEvent (Event e)
      {
        System.out.println ("clicked " + e.text);
        e.doit = true;
      }
    });
    
    textField.setText ("Hello http://there world\nA mailto:foo@foobar.com link\nAn ftp://link\nA file c:\\development\\dfc.swt\\build.xml.");
    
    // start
    shell.pack ();
    shell.open ();
    
    while (!shell.isDisposed ())
    {
      if (!display.readAndDispatch ())
        display.sleep ();
    }
    
    display.dispose ();
  }

}
