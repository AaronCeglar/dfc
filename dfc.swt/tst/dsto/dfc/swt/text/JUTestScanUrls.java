package dsto.dfc.swt.text;

import java.util.List;

import junit.framework.TestCase;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import dsto.dfc.swt.text.UrlScanner.UrlRange;

//import dsto.dfc.swt.text.StyledTextLinkOTron.UrlRange;

/**
 * Test the URL scanner in {@link StyledTextLinkOTron}.
 */
public class JUTestScanUrls extends TestCase
{
  public void testScan () throws Exception
  {
    UrlRange [] ranges;
    
    Shell parent = new Shell (new Display ());
    StyledText styledText = new StyledText (parent, SWT.WRAP);
    
    StyledTextLinkOTron tron = new StyledTextLinkOTron (styledText);
    
    ranges = scan (tron, "");
    assertEquals (0, ranges.length);
    
    ranges = scan (tron, "abc def");
    assertEquals (0, ranges.length);
    
    ranges = scan (tron, "http:");
    assertEquals (0, ranges.length);
    
    String text = "http:hello";
    ranges = scan (tron, text);
    assertEquals (1, ranges.length);
    String url = text.substring (ranges[0].start, ranges [0].start + ranges[0].length);
    assertEquals ("http:hello", url);
    
    text = "some text http:hello/there";
    ranges = scan (tron, text);
    assertEquals (1, ranges.length);
    url = text.substring (ranges[0].start, ranges [0].start + ranges[0].length);
    assertEquals ("http:hello/there", url);
    
    text = "some text http:hello/there text ftp://ftp/url";
    ranges = scan (tron, text);
    assertEquals (2, ranges.length);
    url = text.substring (ranges[0].start, ranges [0].start + ranges[0].length);
    assertEquals ("http:hello/there", url);
    url = text.substring (ranges[1].start, ranges [1].start + ranges[1].length);
    assertEquals ("ftp://ftp/url", url);
    
    text = " mailto:123 hello:";
    ranges = scan (tron, text);
    assertEquals (1, ranges.length);
    url = text.substring (ranges[0].start, ranges [0].start + ranges[0].length);
    assertEquals ("mailto:123", url);
    
    ranges = scan (tron, " f123:123 hello:");
    assertEquals (0, ranges.length);

    // test stripping characters off end 
    text = "http://hello.there/world.";
    ranges = scan (tron, text);
    assertEquals (1, ranges.length);
    url = text.substring (ranges[0].start, ranges [0].start + ranges[0].length);
    assertEquals ("http://hello.there/world", url);

    text = "http://hello.there/world:...)";
    ranges = scan (tron, text);
    assertEquals (1, ranges.length);
    url = text.substring (ranges[0].start, ranges [0].start + ranges[0].length);
    assertEquals ("http://hello.there/world", url);

    ranges = scan (tron, " f123:123 hello:");
    assertEquals (0, ranges.length);
    
    text = "A URL (http://hello.there/world):";
    ranges = scan (tron, text);
    assertEquals (1, ranges.length);
    url = text.substring (ranges[0].start, ranges [0].start + ranges[0].length);
    assertEquals ("http://hello.there/world", url);

    ranges = scan (tron, " f123:123 hello:");
    assertEquals (0, ranges.length);
    
    ranges = scan (tron, " f123:123 hello:");
    assertEquals (0, ranges.length);
    
    ranges = scan (tron, "A URL (http:.");
    assertEquals (0, ranges.length);
    
    ranges = scan (tron, "A URL (http:):");
    assertEquals (0, ranges.length);

    ranges = scan (tron, "http:../hello:");
    assertEquals (1, ranges.length);
    url = "http:../hello:".substring (ranges[0].start, ranges [0].start + ranges[0].length);
    assertEquals ("http:../hello", url);

    ranges = scan (tron, "hello (\\\\server\\path) there //server2/path/1.");
    assertEquals (2, ranges.length);
    url = "hello (\\\\server\\path) there //server2/path/1.".substring (ranges[0].start, ranges [0].start + ranges[0].length);
    assertEquals ("\\\\server\\path", url);
    url = "hello (\\\\server\\path) there //server2/path/1.".substring (ranges[1].start, ranges [1].start + ranges[1].length);
    assertEquals ("//server2/path/1", url);
    
    ranges = scan (tron, "hello c:\\path1\\path2.txt there or c:/path3/path4.txt.");
    assertEquals (2, ranges.length);
    url = "hello c:\\path1\\path2.txt there or c:/path3/path4.txt.".substring (ranges[0].start, ranges [0].start + ranges[0].length);
    assertEquals ("c:\\path1\\path2.txt", url);
    url = "hello c:\\path1\\path2.txt there or c:/path3/path4.txt.".substring (ranges[1].start, ranges [1].start + ranges[1].length);
    assertEquals ("c:/path3/path4.txt", url);
    
    ranges = scan (tron, "/\\\\hello\\there");
    assertEquals (1, ranges.length);
    url = "/\\\\hello\\there".substring (ranges[0].start, ranges [0].start + ranges[0].length);
    assertEquals ("\\\\hello\\there", url);    
    
    ranges = scan (tron, "\\//hello/there");
    assertEquals (1, ranges.length);
    url = "\\//hello/there".substring (ranges[0].start, ranges [0].start + ranges[0].length);
    assertEquals ("//hello/there", url);    
    
    // test allowing any scheme
    tron.validSchemes ().add ("*");
    
    ranges = scan (tron, " f123:123 hello:");
    assertEquals (1, ranges.length);
    url = " f123:123 hello:".substring (ranges[0].start, ranges [0].start + ranges[0].length);
    assertEquals ("f123:123", url);
    
//     print (ranges);
  }

  private UrlRange [] scan (StyledTextLinkOTron tron, String text)
  {
    List<UrlRange> ranges = tron.scanForUrls (text, 0);
      
    UrlRange [] urlRanges = new UrlRange [ranges.size ()];
    ranges.toArray (urlRanges);
    
    return urlRanges;
  }
  
  void print (UrlRange[] ranges)
  {
    System.out.println ("ranges.length == " + ranges.length);
    for (int i = 0; i < ranges.length; i++)
    {
      System.out.println (ranges [i]);
    }
  }
}
