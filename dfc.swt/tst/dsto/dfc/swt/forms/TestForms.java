package dsto.dfc.swt.forms;

import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import dsto.dfc.databeans.DataBean;
import dsto.dfc.databeans.IDataBean;
import dsto.dfc.databeans.io.XmlOutput;

/**
 * @author mpp
 * @version $Revision$
 */
public class TestForms
{
  public static void main (String [] args)
  {
    Display display = new Display ();
    final Shell shell = new Shell (display);

    shell.setText ("Form");
    shell.setLayout (new FillLayout ());

    Composite mainPanel = new Composite (shell, SWT.NONE);
    mainPanel.setLayout (new GridLayout (1, true));

    Composite personPanel = new Composite (mainPanel, SWT.NONE);
    personPanel.setLayoutData (new GridData (GridData.FILL_BOTH));
    personPanel.setLayout (new GridLayout (2, false));

    Label nameLabel = new Label (personPanel, SWT.NONE);
    nameLabel.setText ("Name:");
    Text nameField = new Text (personPanel, SWT.BORDER);

    Label ageLabel1 = new Label (personPanel, SWT.NONE);
    ageLabel1.setText ("Age:");
    Spinner ageSpinner = new Spinner (personPanel, SWT.BORDER);
    ageSpinner.setMinimum (0);
    ageSpinner.setMaximum (40);
    ageSpinner.setIncrement (1);
    ageSpinner.setPageIncrement (5);

    Label ageLabel2 = new Label (personPanel, SWT.NONE);
    ageLabel2.setText ("Age:");
    Scale ageScale = new Scale (personPanel, SWT.BORDER);
    ageScale.setMinimum (0);
    ageScale.setMaximum (40);
    ageScale.setIncrement (1);
    ageScale.setPageIncrement (5);

    Label ageLabel3 = new Label (personPanel, SWT.NONE);
    ageLabel3.setText ("Age:");
    Slider ageSlider = new Slider (personPanel, SWT.BORDER);
    ageSlider.setMinimum (0);
    ageSlider.setMaximum (40);
    ageSlider.setIncrement (1);
    ageSlider.setPageIncrement (5);

    Label maleLabel = new Label (personPanel, SWT.NONE);
    maleLabel.setText ("Male:");
    Button maleButton = new Button (personPanel, SWT.CHECK);

    Composite buttonPanel = new Composite (mainPanel, SWT.NONE);
    buttonPanel.setLayoutData (new GridData (GridData.FILL_HORIZONTAL));
    buttonPanel.setLayout (new RowLayout ());
    Button applyButton = new Button (buttonPanel, SWT.NONE);
    applyButton.setText ("Apply");
    Button changeButton = new Button (buttonPanel, SWT.NONE);
    changeButton.setText ("Change");

    final DataBean person = new DataBean ();
    person.setValue ("name", "Matthew", IDataBean.TRANSIENT);
    person.setValue ("age", 30, IDataBean.TRANSIENT);
    person.setValue ("male", true, IDataBean.TRANSIENT);
    DataBean address = new DataBean ();
    address.setValue ("street", "George", IDataBean.TRANSIENT);
    address.setValue ("number", 42, IDataBean.TRANSIENT);
    person.setValue ("address", address, IDataBean.TRANSIENT);

    final Pipe personPipe = new MultiPipe ();

    personPipe.setErrorHandler (new PipeErrorHandler ()
    {
      public void handlePipeError (Pipe source, String message)
      {
        MessageBox dialog = new MessageBox (shell, SWT.ICON_ERROR);

        dialog.setText ("Input Error");
        dialog.setMessage (message);

        dialog.open ();
      }
    });

    Pipes.connect (personPipe, "name", nameField);
    Pipes.connect (personPipe, "age", ageSpinner);
    Pipes.connect (personPipe, "age", ageScale);
    Pipes.connect (personPipe, "age", ageSlider);
    Pipes.connect (personPipe, "male", maleButton);

//    personPipe.connect (new PropertyPipe ("name")).connect (new TextPipe (nameField));
//    personPipe.connect (new PropertyPipe ("age")).connect (new TranslatorPipe (NumberTranslator.INT)).connect (new TextPipe (ageField));
//    personPipe.connect (new PropertyPipe ("male")).connect (new ButtonPipe (maleButton));

    personPipe.input (person);

    applyButton.addSelectionListener (new SelectionListener ()
    {
      public void widgetSelected(SelectionEvent e)
      {
        if (personPipe.flush ())
        {
          XmlOutput xmlStream = new XmlOutput ();
          try
          {
            xmlStream.write (System.out, person);
          } catch (IOException ex)
          {
            ex.printStackTrace ();
          }
        }
      }

      public void widgetDefaultSelected(SelectionEvent e)
      {
        // zip
      }
    });

    changeButton.addSelectionListener (new SelectionListener ()
    {
      int counter;

      public void widgetSelected(SelectionEvent e)
      {
        person.setValue ("name", "Hello! " + (counter++));
      }

      public void widgetDefaultSelected(SelectionEvent e)
      {
        // zip
      }
    });

    shell.pack ();
    shell.open ();

    while (!shell.isDisposed ())
    {
      if (!display.readAndDispatch ())
        display.sleep ();
    }

    personPipe.dispose ();

    display.dispose ();
  }
}
