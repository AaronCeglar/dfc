package dsto.dfc.swt.persistence;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.eclipse.swt.widgets.Control;
import org.junit.Before;
import org.junit.Test;

import dsto.dfc.databeans.DataBean;

public class JUTestFileDocument
{

  private FileDocument doc;

  @Before
  public void setUp () throws Exception
  {
    doc = new TestFileDocument ();
  }

  @Test
  public void testJavaBeanChange ()
  {
    JavaBean model = new JavaBean ();
    doc.setModel (model);

    assertFalse (doc.isChanged ());

    model.setA ("foo");

    assertTrue (doc.isChanged ());
  }

  @Test
  public void testDataBeanChange ()
  {
    DataBean model = new DataBean ();
    model.setValue ("a", 1);
    doc.setModel (model);

    assertFalse (doc.isChanged ());

    model.setValue ("a", 2);

    assertTrue (doc.isChanged ());
  }

  @Test
  public void testSwingChange ()
  {
    ChangingObject model = new ChangingObject ();
    doc.setModel (model);

    assertFalse (doc.isChanged ());

    model.change ();

    assertTrue (doc.isChanged ());
  }

  public class ChangingObject
  {
    private List<ChangeListener> listeners = new ArrayList<ChangeListener> ();

    public void addChangeListener (ChangeListener l)
    {
      listeners.add (l);
    }

    public void change ()
    {
      ChangeEvent e = new ChangeEvent (this);
      for (ChangeListener l : listeners)
        l.stateChanged (e);
    }
  }

  public class TestFileDocument extends FileDocument
  {
    private Object model;

    @Override
    protected Object createModelInstance ()
    {
      return null;
    }

    @Override
    protected String getDefaultExtension ()
    {
      return null;
    }

    @Override
    public String getType ()
    {
      return null;
    }

    @Override
    protected void basicSetModel (Object newModel)
      throws IllegalArgumentException
    {
      this.model = newModel;
    }

    @Override
    public Control getClient ()
    {
      return null;
    }

    @Override
    public Object getModel ()
    {
      return model;
    }

  }
}
