package dsto.dfc.swt.persistence;

import static java.io.File.createTempFile;
import static junit.framework.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import dsto.dfc.databeans.DataBean;

public class JUTestSerialization
{

  private File file;

  @Before
  public void setUp () throws Exception
  {
    file = createTempFile ("test", ".ser");
    file.deleteOnExit ();
  }

  @Test
  public void testSaveAsBinaryObjectStringBoolean ()
    throws FileNotFoundException, IOException, ClassNotFoundException
  {
    Object data = "This is serialisable data";

    String filePath = file.getCanonicalPath ();
    Serialiser.saveAsBinary (data, filePath, false);

    Object result = Deserialiser.read (filePath);

    assertEquals (data, result);

    // now with compression
    Serialiser.saveAsBinary (data, filePath, true);

    result = Deserialiser.read (filePath);

    assertEquals (data, result);
  }

  @Test
  public void testSaveAsBinaryObjectOutputStreamBoolean ()
    throws FileNotFoundException, IOException, ClassNotFoundException
  {
    Object data = "This is serialisable data";

    ByteArrayOutputStream outputStream = new ByteArrayOutputStream ();
    Serialiser.saveAsBinary (data, outputStream, false);

    ByteArrayInputStream inputStream =
      new ByteArrayInputStream (outputStream.toByteArray ());
    Object result = Deserialiser.read (inputStream);

    assertEquals (data, result);

    // now with compression
    outputStream = new ByteArrayOutputStream ();
    Serialiser.saveAsBinary (data, outputStream, true);

    inputStream = new ByteArrayInputStream (outputStream.toByteArray ());
    result = Deserialiser.read (inputStream);

    assertEquals (data, result);
  }

  @Test
  public void testSaveAsKBMLObjectStringBoolean ()
    throws FileNotFoundException, IOException, ClassNotFoundException
  {
    JavaBean data = makeJavaBean ();

    String filePath = file.getCanonicalPath ();
    Serialiser.saveAsKBML (data, filePath, false);

    JavaBean result = (JavaBean) Deserialiser.read (filePath);

    testData (result);
  }

  @Test
  public void testSaveAsKBMLObjectOutputStreamBoolean ()
    throws FileNotFoundException, IOException, ClassNotFoundException
  {
    JavaBean data = makeJavaBean ();

    ByteArrayOutputStream outputStream = new ByteArrayOutputStream ();
    Serialiser.saveAsKBML (data, outputStream, false);

    ByteArrayInputStream inputStream =
      new ByteArrayInputStream (outputStream.toByteArray ());
    JavaBean result = (JavaBean) Deserialiser.read (inputStream);

    testData (result);
  }

  @Test
  public void testSaveAsDBXMLObjectStringBoolean ()
    throws FileNotFoundException, IOException, ClassNotFoundException
  {
    DataBean data = makeData ();

    String filePath = file.getCanonicalPath ();
    Serialiser.saveAsDBXML (data, filePath, false);

    DataBean result = (DataBean) Deserialiser.read (filePath);

    testData (result);

    // with compression on
    file.delete ();
    Serialiser.saveAsDBXML (data, filePath, true);

    result = (DataBean) Deserialiser.read (filePath);

    testData (result);
  }

  @Test
  public void testSaveAsDBXMLObjectOutputStreamBoolean ()
    throws FileNotFoundException, IOException, ClassNotFoundException
  {
    DataBean data = makeData ();

    ByteArrayOutputStream outputStream = new ByteArrayOutputStream ();
    Serialiser.saveAsDBXML (data, outputStream, false);

    ByteArrayInputStream inputStream =
      new ByteArrayInputStream (outputStream.toByteArray ());
    DataBean result = (DataBean) Deserialiser.read (inputStream);

    testData (result);

    // with compression on
    outputStream = new ByteArrayOutputStream ();
    Serialiser.saveAsDBXML (data, outputStream, true);

    inputStream = new ByteArrayInputStream (outputStream.toByteArray ());
    result = (DataBean) Deserialiser.read (inputStream);

    testData (result);
  }

  private DataBean makeData ()
  {
    DataBean data = new DataBean ();
    data.setValue ("a", "b");
    data.setValue ("b", 'c');
    data.setValue ("c", 0x0d);
    data.setValue ("d", new DataBean ());
    return data;
  }

  private void testData (DataBean result)
  {
    assertEquals ("b", result.getStringValue ("a"));
    assertEquals ('c', result.getCharValue ("b"));
    assertEquals (0x0d, result.getIntValue ("c"));
    assertEquals (0, result.getBeanValue ("d").getPropertyNames ().length);
  }

  private JavaBean makeJavaBean ()
  {
    JavaBean bean = new JavaBean ();
    bean.setA ("b");
    bean.setB ('c');
    bean.setC (0x0d);
    bean.setD (true);
    return bean;
  }

  private void testData (JavaBean bean)
  {
    assertEquals ("b", bean.getA ());
    assertEquals ('c', bean.getB ());
    assertEquals (0x0d, bean.getC ());
    assertEquals (true, bean.getD ());
  }
}
