/**
 *
 */
package dsto.dfc.swt.persistence;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import dsto.dfc.util.PropertyEventSource;

public class JavaBean implements PropertyEventSource
{
  private String a;
  private char b;
  private int c;
  private boolean d;

  private PropertyChangeSupport support = new PropertyChangeSupport (this);

  public String getA ()
  {
    return a;
  }

  public void setA (String newA)
  {
    String oldA = this.a;
    this.a = newA;
    support.firePropertyChange ("a", oldA, newA);
  }

  public char getB ()
  {
    return b;
  }

  public void setB (char newB)
  {
    char oldB = this.b;
    this.b = newB;
    support.firePropertyChange ("b", oldB, newB);
  }

  public int getC ()
  {
    return c;
  }

  public void setC (int newC)
  {
    int oldC = this.c;
    this.c = newC;
    support.firePropertyChange ("c", oldC, newC);
  }

  public boolean getD ()
  {
    return d;
  }

  public void setD (boolean newD)
  {
    boolean oldD = this.d;
    this.d = newD;
    support.firePropertyChange ("d", oldD, newD);
  }

  public void addPropertyChangeListener (PropertyChangeListener l)
  {
    support.addPropertyChangeListener (l);
  }

  public void removePropertyChangeListener (PropertyChangeListener l)
  {
    support.removePropertyChangeListener (l);
  }
}