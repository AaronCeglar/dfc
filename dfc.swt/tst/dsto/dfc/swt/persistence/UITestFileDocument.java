package dsto.dfc.swt.persistence;

import static dsto.dfc.swt.commands.Commands.getContextView;
import static dsto.dfc.swt.commands.Commands.getMenuView;
import static dsto.dfc.swt.commands.Commands.getToolbarView;
import static java.io.File.createTempFile;
import static org.eclipse.swt.SWT.LEFT;
import static org.eclipse.swt.SWT.OK;
import static org.eclipse.swt.SWT.PUSH;
import static org.eclipse.swt.layout.GridData.FILL_HORIZONTAL;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import dsto.dfc.databeans.DataBean;
import dsto.dfc.swt.commands.CommandView;
import dsto.dfc.swt.commands.CommandViewToolBarProvider;

public class UITestFileDocument
{
  static TestFileDocument doc;
  private static Label label;

  public static void main (String[] args) throws IOException
  {
    Display display = new Display ();
    final Shell shell = new Shell (display);
    shell.setLayout (new GridLayout ());

    CoolBar toolbar = new CoolBar (shell, SWT.NONE);
    spanHorizontally (toolbar);

    File file = createTempFile ("test", ".dbxml");
    file.deleteOnExit ();
    doc = new TestFileDocument (shell, file);

    BasicDocumentSaveCommand saveCmd = new BasicDocumentSaveCommand (doc);
    BasicDocumentSaveAsCommand saveAsCmd = new BasicDocumentSaveAsCommand (doc);

    CommandView menuView = getMenuView (shell);
    menuView.addCommand (saveCmd);
    menuView.addCommand (saveAsCmd);

    menuView.setDefaultCommand (saveCmd);

    CommandView toolbarView = getToolbarView (shell);
    toolbarView.addCommand (saveCmd);
    toolbarView.addCommand (saveAsCmd);

    CommandView contextView = getContextView (shell);
    contextView.addCommand (saveCmd);
    contextView.addCommand (saveAsCmd);

    contextView.setDefaultCommand (saveCmd);

    label = new Label (shell, LEFT);
    spanHorizontally (label);
    updateLabel ();
    doc.addPropertyChangeListener (new PropertyChangeListener ()
    {
      public void propertyChange (PropertyChangeEvent evt)
      {
        updateLabel ();
      }
    });

    final Button changeButton = new Button (shell, PUSH);
    spanHorizontally (changeButton);
    changeButton.setText ("Change the document!");
    changeButton.addSelectionListener (new SelectionAdapter ()
    {
      @Override
      public void widgetSelected (SelectionEvent e)
      {
        doc.setChanged (true);
        updateLabel ();
      }
    });

    final Button checkSaveButton = new Button (shell, PUSH);
    spanHorizontally (checkSaveButton);
    checkSaveButton.setText ("Check save changes");
    checkSaveButton.addSelectionListener (new SelectionAdapter ()
    {
      @Override
      public void widgetSelected (SelectionEvent e)
      {
        boolean result = doc.checkSaveChanges ();

        MessageBox dialog = new MessageBox (shell, OK);
        dialog.setText ("Check the result please");
        dialog.setMessage ("The result was " + result + "\n\nIt should be:\n" +
                           "  - save() for YES\n  - true for NO\n  - false" +
                           " for CANCEL");
        dialog.open ();
      }
    });

    new CommandViewToolBarProvider (toolbar, toolbarView);

    shell.setSize (300, 200);
    shell.setVisible (true);
    while (!shell.isDisposed ())
      if (display.readAndDispatch ())
        display.sleep ();
    display.dispose ();
  }

  private static void spanHorizontally (Control control)
  {
    control.setLayoutData (new GridData (FILL_HORIZONTAL));
  }

  static void updateLabel ()
  {
    String text = "File name:\n\t" + doc.getFile ();
    if (doc.isChanged ())
      text += "*";
    label.setText (text);
  }

  public static class TestFileDocument extends FileDocument
  {
    private DataBean model;
    private Control client;

    public TestFileDocument (Control client, File file)
    {
      this.client = client;
      setModel (createModelInstance ());
      setFile (file);
    }

    @Override
    protected void customizeChooser (String mode, FileDialog chooser)
    {
      super.customizeChooser (mode, chooser);
    }

    @Override
    protected Object createModelInstance ()
    {
      return new DataBean ();
    }

    @Override
    protected String getDefaultExtension ()
    {
      return "dbxml";
    }

    @Override
    public String getType ()
    {
      return "DataBean";
    }

    @Override
    protected void basicSetModel (Object newModel)
      throws IllegalArgumentException
    {
      if (newModel instanceof DataBean)
        this.model = (DataBean) newModel;
      else
        throw new IllegalArgumentException ("New model is not a DataBean");
    }

    @Override
    public Control getClient ()
    {
      return client;
    }

    @Override
    public Object getModel ()
    {
      return model;
    }
  }
}
