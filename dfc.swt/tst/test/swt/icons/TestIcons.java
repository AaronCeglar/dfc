package test.swt.icons;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import dsto.dfc.swt.icons.FlowIcon;
import dsto.dfc.swt.icons.Icon;
import dsto.dfc.swt.icons.ImageResourceIcon;
import dsto.dfc.swt.icons.StackIcon;

/**
 * @author Matthew Phillips
 */
public class TestIcons
{
  public static void main (String [] args)
  {
    Display display = new Display ();
    final Shell shell = new Shell (display);
  
    shell.setText ("Test Icons");
    shell.setLayout (new RowLayout ());
    
    Icon icon = new FlowIcon (new ImageResourceIcon ("exclamation.gif"), new ImageResourceIcon ("eye.gif"));
    Label label = new Label (shell, SWT.NONE);
    label.setImage (icon.createImage (display));
    
    icon = new StackIcon (new ImageResourceIcon ("eye.gif"), new ImageResourceIcon ("exclamation.gif"));
    label = new Label (shell, SWT.NONE);
    label.setImage (icon.createImage (display));
    
    shell.pack ();
    shell.open ();
    
    while (!shell.isDisposed ())
    {
      if (!display.readAndDispatch ())
        display.sleep ();
    }
    
  }
}
