package test.swt.commands;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import dsto.dfc.swt.commands.AbstractCommand;
import dsto.dfc.swt.commands.BasicToggleCommand;
import dsto.dfc.swt.commands.CommandView;
import dsto.dfc.swt.commands.CommandViewToolBarProvider;
import dsto.dfc.swt.commands.Commands;


/**
 * @author mpp
 * @version $Revision$
 */
public class TestCommandViewMenuProvider
{
  protected static CommandView menuView;
  protected static CommandView contextView;
  protected static CommandView toolView;
  
  public static void main (String[] args)
  {
    Display display = new Display ();
    Shell shell = new Shell (display);
    shell.setText ("Menus");
    shell.setLayout (new GridLayout (1, true));
    CoolBar coolBar = new CoolBar (shell, 0);
    Composite panel = new Composite (shell, 0);
    panel.setLayoutData (new GridData (GridData.FILL_BOTH));
    coolBar.setLayoutData (new GridData (GridData.FILL_HORIZONTAL));

    contextView = Commands.getContextView (panel);
    menuView = Commands.getMenuView (shell);
    toolView = Commands.getCommandView (shell, CommandView.TOOLBAR_VIEW);

    NullCommand fileOpen = new NullCommand ("file.Open", "file_open.gif", "oc", "Open a file", true, 'o', (SWT.CONTROL | 'O'));
    NullCommand fileView = new NullCommand ("file.View File", "edit_cut.gif", "oc.View.view", "View a file", false, 'v', 0);
    NullCommand fileEdit = new NullCommand ("file.Edit File", null, "oc.View.view", "Edit a file", false, 'e', 0);
    NullCommand fileExit = new NullCommand ("file.Exit", "file_close.gif", "exit", "Exit", false, 'x', (SWT.CONTROL | SWT.SHIFT | 'X'));
    NullBooleanCommand frobCommand = new NullBooleanCommand ("file.Frobulate", null, "frob", "Frobulate mode", false, 'f', 0);
    
    frobCommand.setMainMenuGroup ("Edit.frob");
    
    contextView.addCommand (fileOpen);
    contextView.addCommand (fileView);
    contextView.addCommand (fileExit);
    contextView.addCommand (fileEdit);
    contextView.addCommand (frobCommand);
    
    menuView.addCommand (fileOpen);
    menuView.addCommand (fileView);
    menuView.addCommand (fileExit);
    menuView.addCommand (fileEdit);
    menuView.addCommand (frobCommand);

    toolView.addCommand (fileOpen);
    toolView.addCommand (fileView);
    toolView.addCommand (fileExit);
    toolView.addCommand (fileEdit);
    toolView.addCommand (frobCommand);
    
    menuView.setDefaultCommand (fileOpen);
    contextView.setDefaultCommand (fileOpen);
    
    new CommandViewToolBarProvider (coolBar, toolView);
    
    //fileExit.setEnabled (false);
    //fileExit.setContextMenuGroup ("oc.View.exit");
    
    //menuView.removeCommand (fileExit);
    
    shell.open ();
    
    // start the event loop. We stop when the user has done
    // something to dispose our window.
    while (!shell.isDisposed ())
    {
      if (!display.readAndDispatch ())
        display.sleep ();
    }
    
    display.dispose ();
  }
  
  private static final class NullCommand extends AbstractCommand
  {
    public NullCommand (String name, String iconName, String group, 
                          String description,
                          boolean interactive, char mnemonic, int accelerator)
    {
      super (name, iconName, group, description,
                          interactive, mnemonic, accelerator);
    }
    
    public void execute ()
    {
      System.out.println ("execute " + getName ());
      
      //setEnabled (!isEnabled ());
      
      if (toolView.containsCommand (this))
        toolView.removeCommand (this);
      else
        toolView.addCommand (this, 0);
    }
  }
  
  private static final class NullBooleanCommand extends BasicToggleCommand
  {
    public NullBooleanCommand (String name, String iconName, String group, 
                          String description,
                          boolean interactive, char mnemonic, int accelerator)
    {
      super (name, iconName, group, description,
                          interactive, mnemonic, accelerator);
    }
    
    public void execute ()
    {
      System.out.println ("execute " + getName ());
      
      super.execute ();
    }
  }
}
