package test.swt.commands;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.CoolItem;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

/**
 * @author Matthew Phillips
 */
public class TestCoolbar
{

  public static void main (String [] args)
  {
    Display display = new Display ();
    Shell shell = new Shell (display);
  
    shell.setText ("Test Coolbar");
    shell.setLayout (new FillLayout ());

    CoolBar coolbar = new CoolBar (shell, SWT.NONE);
    
    CoolItem item1 = new CoolItem (coolbar, 0);
    ToolBar toolbar1 = new ToolBar (coolbar, SWT.FLAT | SWT.RIGHT | SWT.HORIZONTAL | SWT.WRAP);
    ToolItem toolItem1 = new ToolItem (toolbar1, SWT.PUSH);
    toolItem1.setText ("Hello!");
    item1.setControl (toolbar1);
    
    item1.setMinimumSize(toolbar1.computeSize (SWT.DEFAULT, SWT.DEFAULT));
    item1.setSize (item1.getMinimumSize());
    
//    CoolItem item2 = new CoolItem (coolbar, 0);
//    ToolBar toolbar2 = new ToolBar (coolbar, SWT.FLAT | SWT.RIGHT | SWT.HORIZONTAL | SWT.WRAP);
//    ToolItem toolItem2 = new ToolItem (toolbar2, SWT.PUSH);
//    toolItem2.setText ("Hello 2!");
//    item2.setControl (toolbar2);
    
    shell.pack ();
    shell.open ();
    
    while (!shell.isDisposed ())
    {
      if (!display.readAndDispatch ())
        display.sleep ();
    }
    
  }
}
