package test.swt.commands;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import dsto.dfc.swt.commands.AbstractCommand;
import dsto.dfc.swt.commands.BasicToggleCommand;
import dsto.dfc.swt.commands.CommandView;
import dsto.dfc.swt.commands.CommandViewToolBarProvider;
import dsto.dfc.swt.commands.Commands;

/**
 * Demo-friendly command view tester.
 *  
 * @author mpp
 * @version $Revision$
 */
public class DemoCommands
{
  private static CommandView menuView;
  private static CommandView contextView;
  private static CommandView toolView;
  
  public static void main (String[] args)
  {
    Display display = new Display ();
    Shell shell = new Shell (display);
    shell.setText ("Menus");
    shell.setLayout (new GridLayout (1, true));
    CoolBar coolBar = new CoolBar (shell, 0);
    Composite panel = new Composite (shell, 0);
    panel.setLayoutData (new GridData (GridData.FILL_BOTH));
    coolBar.setLayoutData (new GridData (GridData.FILL_HORIZONTAL));

    contextView = Commands.getContextView (panel);
    menuView = Commands.getMenuView (shell);
    toolView = Commands.getCommandView (shell, CommandView.TOOLBAR_VIEW);

    DummyCommand fileOpen = new DummyCommand ("file.Open", "file_open.gif", "File.document", "document", "Open file", true, 'o', (SWT.CONTROL | 'O'));
    DummyCommand fileSave = new DummyCommand ("file.Save", "file_save.gif", "File.document", "document", "Save file", false, 'v', SWT.CONTROL | 'S');
    
    DummyCommand fileAbout = new DummyCommand ("app.About", null, "File.app", "app", "Show about window", true, '\0', 0);
    DummyCommand fileExit = new DummyCommand ("app.Exit", null, "File.app", "app", "Exit", false, '\0', 0);

    DummyCommand sendEmail = new DummyCommand ("send.Email", null, "File.send.Send To.send", "app.Send To.send", "Send to email", true, '\0', 0);
    DummyCommand sendFloppy = new DummyCommand ("app.Floppy", null, "File.send.Send To.send", "app.Send To.send", "Send to floppy", true, '\0', 0);
    
    DummyCommand editCut = new DummyCommand ("edit.Cut", "edit_cut.gif", "Edit.cnp", "cnp", "Cut from clipboard", false, '\0', SWT.CTRL|'X');
    DummyCommand editCopy = new DummyCommand ("edit.Copy", "edit_copy.gif", "Edit.cnp", "cnp", "Copy from clipboard", false, '\0', SWT.CTRL|'C');
    DummyCommand editPaste = new DummyCommand ("edit.Paste", "edit_paste.gif", "Edit.cnp", "cnp", "Paste from clipboard", false, '\0', SWT.CTRL|'V');
    
    DummyBooleanCommand frobCommand = new DummyBooleanCommand ("opts.Lock", null, "opts", "Lock mode", false, 'l', 0);
    
    frobCommand.setMainMenuGroup ("Edit.opts");
    
    contextView.addCommand (sendEmail);
    contextView.addCommand (sendFloppy);

    contextView.addCommand (editCut);
    contextView.addCommand (editCopy);
    contextView.addCommand (editPaste);
    
    menuView.addCommand (fileOpen);
    menuView.addCommand (fileSave);
    
    menuView.addCommand (sendEmail);
    menuView.addCommand (sendFloppy);
    
    menuView.addCommand (fileAbout);
    menuView.addCommand (fileExit);
    
    menuView.addCommand (editCut);
    menuView.addCommand (editCopy);
    menuView.addCommand (editPaste);
    menuView.addCommand (frobCommand);

    toolView.addCommand (fileOpen);
    toolView.addCommand (fileSave);
    toolView.addCommand (editCut);
    toolView.addCommand (editCopy);
    toolView.addCommand (editPaste);
    toolView.addCommand (frobCommand);
    
    toolView.addCommand (new DummyCommand ("app.View 1", null, "group1.Views.group1", "group1.Views.group1", "View 1", false, '\0', 0));
    toolView.addCommand (new DummyCommand ("app.View 2", null, "group1.Views.group1", "group1.Views.group1", "View 2", false, '\0', 0));
    
    //menuView.setDefaultCommand (fileOpen);
    //contextView.setDefaultCommand (fileOpen);
    
    new CommandViewToolBarProvider (coolBar, toolView);
    
    shell.open ();
    
    while (!shell.isDisposed ())
    {
      if (!display.readAndDispatch ())
        display.sleep ();
    }
    
    display.dispose ();
  }
  
  private static final class DummyCommand extends AbstractCommand
  {
    public DummyCommand (String name, String iconName, String mainGroup, 
                         String group,
                         String description, boolean interactive, char mnemonic, int accelerator)
    {
      super (name, iconName, group, description,
             interactive, mnemonic, accelerator);
      
      setMainMenuGroup (mainGroup);
    }
    
    public void execute ()
    {
      System.out.println ("execute " + getName ());    }
  }
  
  private static final class DummyBooleanCommand extends BasicToggleCommand
  {
    public DummyBooleanCommand (String name, String iconName, String group, 
                                String description,
                                boolean interactive, char mnemonic, int accelerator)
    {
      super (name, iconName, group, description,
             interactive, mnemonic, accelerator);
    }
    
    public void execute ()
    {
      System.out.println ("execute " + getName ());
      
      super.execute ();
    }
  }
}
