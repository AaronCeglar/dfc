package dsto.dfc.swt.commands;

import static org.eclipse.swt.SWT.CTRL;

import org.eclipse.swt.widgets.Control;

/**
 * Base class for File Save commands.
 */
public class BasicFileSaveCommand extends AbstractBasicFileSaveCommand
{
  public BasicFileSaveCommand (Control owner)
  {
    super (owner, "file.Save", "file_save.gif", "Save current file", false,
           CTRL | 's');
  }
}
