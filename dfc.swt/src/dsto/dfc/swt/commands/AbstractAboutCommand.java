package dsto.dfc.swt.commands;

import org.eclipse.swt.widgets.Shell;

/**
 * Base for commands that show an "about box.
 *
 * @version $Revision$
 */
public abstract class AbstractAboutCommand extends AbstractCommand
{
  protected Shell shell;

  public AbstractAboutCommand (Shell shell)
  {
    super ("help.About", NO_ICON, "help",
           "Show information about this application", true, 'a', 0);
    
    this.shell = shell;
    
    setMainMenuGroup ("Help.about");
  }
}
