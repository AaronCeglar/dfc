package dsto.dfc.swt.commands;

import static org.eclipse.swt.SWT.CTRL;
import static org.eclipse.swt.SWT.SHIFT;

import org.eclipse.swt.widgets.Control;

/**
 * Base class for File "Save As..." commands.
 */
public class BasicFileSaveAsCommand extends AbstractBasicFileSaveCommand
{
  public BasicFileSaveAsCommand (Control owner)
  {
    super (owner, "file.Save As", "Save current file as...", true,
           (CTRL | SHIFT | 's'));
  }
}
