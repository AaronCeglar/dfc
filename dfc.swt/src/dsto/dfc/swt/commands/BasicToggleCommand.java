package dsto.dfc.swt.commands;

import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.ToolBar;

import dsto.dfc.swt.icons.Icon;

/**
 * Base class for commands that can be "on" or "off". Provides custom
 * checkbox menu and toolbar items.
 *
 * @version $Revision: 1.5 $
 */
public class BasicToggleCommand
  extends AbstractCommand implements CustomMenuCommand, CustomToolbarCommand
{
  protected boolean value = false;

  public BasicToggleCommand 
    (String name, String iconName, String group, 
     String description, boolean interactive, char mnemonic, int accelerator)
  {
    super (name, iconName, group, description, interactive, mnemonic, accelerator);
  }

  public BasicToggleCommand 
    (String name, Icon icon, String group, String description,
     boolean interactive, char mnemonic, int accelerator)
  {
    super (name, icon, group, description, interactive, mnemonic, accelerator);   
  }

  public void execute ()
  {
    setValue (!getValue ());
    
    fireCommandExecuted ();
  }

  public boolean getValue ()
  {
    return value;
  }

  public void setValue (boolean newValue)
  {
    doSetValue (newValue);
  }
  
  /**
   * Actually sets the value and fires an event. Subclasses may call this to
   * update the checked state without executing the check change logic implied
   * in setValue ().
   */
  protected void doSetValue (boolean newValue)
  {
    if (value != newValue)
    {
      value = newValue;
    
      firePropertyChange ("value", newValue);
    }
  }

  public CommandMenuItemProvider createMenuProvider
    (CommandView commandView, Menu menu, int index)
  {
    return new CommandMenuCheckboxProvider
      (commandView, this, "value", menu, index);
  }
  
  public CommandToolbarItemProvider createToolbarProvider
    (CommandView commandView, ToolBar toolbar, int index)
  {
    return new CommandToolbarCheckboxProvider
      (commandView, this, "value", toolbar, index);
  }
}
