package dsto.dfc.swt.commands;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Widget;

/**
 * Command UI provider that generates a ToolItem for the command.
 * 
 * @author mpp
 * @version $Revision$
 */
public class CommandToolbarButtonProvider
  extends AbstractCommandItemProvider implements CommandToolbarItemProvider
{
  public CommandToolbarButtonProvider (CommandView commandView, Command command,
                                       ToolBar toolbar, int index) 
  {
    super (commandView, command, toolbar, index);
  }

  protected Item createItem (Widget toolbar, int index)
  {
    return new ToolItem ((ToolBar)toolbar, SWT.PUSH, index);
  }

  protected void updateIcon ()
  {
    super.updateIcon ();
    
    CommandViewToolBarProvider.itemSizeChanged ((ToolBar)parent);
  }
  
  protected void updateDisplayName ()
  {
    // if no icon for command, or the show name hint is set in the command
    // view, then set the text of the button
    if (command.getIcon () == null ||
        commandView.getCommandProperty
          (command.getName (), Commands.SHOW_NAME_KEY, false))
    {
      item.setText (CommandRegistry.getDisplayName (command, true));
    } else
    {
      item.setText ("");
    }
       
    CommandViewToolBarProvider.itemSizeChanged ((ToolBar)parent);
  }
  
  protected void updateDescription ()
  {
    ((ToolItem)item).setToolTipText (command.getDescription ());
  }
  
  protected void updateEnabled ()
  {
    ((ToolItem)item).setEnabled (command.isEnabled ());
  }
}
