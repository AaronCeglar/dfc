package dsto.dfc.swt.commands;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Widget;

import dsto.dfc.logging.Log;
import dsto.dfc.util.Beans;

/**
 * Command UI provider that generates a checkbox ToolItem for the command.
 * 
 * @author mpp
 * @version $Revision$
 */
public class CommandToolbarCheckboxProvider extends CommandToolbarButtonProvider
{
  private String valueProperty;
  
  /**
   * @param commandView The command view.
   * @param command The command. 
   * @param valueProperty The name of the property on the command that
   * represents the boolean value to sync with the item. The command must have
   * a get method for this property.
   * @param toolbar The toolbar.
   * @param index The index.
   */
  public CommandToolbarCheckboxProvider (CommandView commandView,
                                         Command command,
                                         String valueProperty,
                                         ToolBar toolbar,
                                         int index)
  {
    super (commandView, command, toolbar, index);
    
    this.valueProperty = valueProperty;
    
    updateCheck ();
  }
  
  protected Item createItem (Widget toolbar, int index)
  {
    return new ToolItem ((ToolBar)toolbar, SWT.CHECK, index);
  }
  
  protected void updateProperty (String name)
  {
    if (name.equals (valueProperty))
      updateCheck ();
    else
      super.updateProperty (name);
  }

  private void updateCheck ()
  {
    try
    {
      Boolean value = (Boolean)Beans.getPropertyValue (command, valueProperty);
      
      ((ToolItem)item).setSelection (value.booleanValue ());
    } catch (NoSuchMethodException ex)
    {
      Log.internalError ("Failed to read check property", this, ex);
    }
  }
}
