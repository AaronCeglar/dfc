package dsto.dfc.swt.commands;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Widget;

/**
 * Command UI provider that generates a MenuItem for the command.
 * 
 * @author mpp
 * @version $Revision$
 */
public class CommandMenuButtonProvider
  extends AbstractCommandItemProvider implements CommandMenuItemProvider
{
  public CommandMenuButtonProvider (CommandView commandView,
                                    Command command,
                                    Menu menu,
                                    int index)
  {
    super (commandView, command, menu, index);
  
    if (commandView.getDefaultCommand () == command)
      menu.setDefaultItem ((MenuItem)item);
      
    commandView.addPropertyChangeListener (this);
  }
  
  public void dispose ()
  {
    commandView.removePropertyChangeListener (this);
    
    super.dispose ();
  }

  protected Item createItem (Widget menu, int index)
  {
    MenuItem newItem = new MenuItem ((Menu)menu, SWT.PUSH, index);
    
    updateAccelerator (newItem);
      
    return newItem;
  }

  public int getItemStart ()
  {
    return ((Menu)parent).indexOf ((MenuItem)item);
  }
  
  protected void updateDisplayName ()
  {
    MenuItem menuItem = (MenuItem)item;

    String text = CommandRegistry.getDisplayName (command);

    /*
     * Add accelerator text for main menu on Windows (doesn't seem to
     * happen automatically)
     */
    if (command.getAccelerator () != 0
        && commandView.getViewName ()
            .equals (CommandView.MAIN_MENU_VIEW)
        && SWT.getPlatform ().equals ("win32"))
    {
      text =
        text + '\t' + Action.convertAccelerator (command.getAccelerator ());
    }

    item.setText (text);

    if (command == commandView.getDefaultCommand ())
      menuItem.getParent ().setDefaultItem (menuItem);
  }
  
  protected void updateIcon ()
  {
    /*
     * Don't show menu icons on Mac OS X: Mac apps usually don't have
     * them and they don't align properly anyway.
     */
    if (!SWT.getPlatform ().equals ("carbon"))
    {
      super.updateIcon ();
    }
  }

  protected void updateEnabled ()
  {
    ((MenuItem)item).setEnabled (command.isEnabled ());
  }
  
  protected void updateAccelerator ()
  {
    updateAccelerator ((MenuItem)item);
      
    updateDisplayName ();
  }
  
  protected void updateAccelerator (MenuItem newItem)
  {
    int accelerator = command.getAccelerator ();
    
    if (SWT.getPlatform ().equals ("carbon"))
    {
      // switch CTRL to command key for Mac OS X
      
      if ((accelerator & SWT.CONTROL) != 0)
      {
        accelerator &= ~SWT.CONTROL;
        accelerator |= SWT.COMMAND;
      }
      
      // only use accelerators on main menu, not context menus
      if (commandView.getViewName ().equals (CommandView.MAIN_MENU_VIEW))
        newItem.setAccelerator (accelerator);
    } else
    {
      newItem.setAccelerator (accelerator);
    }
  }
  
  protected void updateProperty (String name)
  {
    if (name.equals ("defaultCommand"))
      updateDisplayName ();
  }
}
