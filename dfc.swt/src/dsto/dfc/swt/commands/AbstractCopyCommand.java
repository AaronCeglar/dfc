package dsto.dfc.swt.commands;

import org.eclipse.swt.SWT;

public abstract class AbstractCopyCommand extends AbstractCommand
{
  public AbstractCopyCommand ()
  {
    super ("edit.Copy", "edit_copy.gif", "cnp",
           "Copy selection to the clipboard", false, 'c', SWT.CTRL | 'c');
           
    setMainMenuGroup ("Edit.cnp");
  }
}