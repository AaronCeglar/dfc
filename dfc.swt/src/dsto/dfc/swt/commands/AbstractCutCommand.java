package dsto.dfc.swt.commands;

import org.eclipse.swt.SWT;

public abstract class AbstractCutCommand extends AbstractCommand
{
  public AbstractCutCommand ()
  {
    super ("edit.Cut", "edit_cut.gif", "edit",
           "Cut selection to the clipboard", false, 't', SWT.CTRL | 'x');
    
    setMainMenuGroup ("Edit.cnp");
  }
}
