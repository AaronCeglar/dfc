package dsto.dfc.swt.commands;

import dsto.dfc.swt.icons.Icon;
import dsto.dfc.swt.icons.Images;

import dsto.dfc.util.BasicPropertyEventSource;
import dsto.dfc.util.Copyable;
import dsto.dfc.util.EventListenerList;

/**
 * Base class for most commands. Subclasses only have to implement
 * {@link #execute()}.
 *
 * @version $Revision$
 */
public abstract class AbstractCommand
  extends BasicPropertyEventSource implements Command, Copyable
{
  /** Value to use when a command has no icon. Resolves problem in
   * distinguishing constructors when passing null as icon argument. */
  public static final Icon NO_ICON = null;
  
  private Icon icon;
  private String displayName;
  private boolean enabled;
  private int accelerator;
  private char mnemonic;
  private String toolbarGroup;
  private String contextMenuGroup;
  private String mainMenuGroup;
  private boolean interactive;
  private String description;
  private String name;
  private EventListenerList listeners = new EventListenerList ();

  /**
   * Create a new instance.
   * 
   * @param name The command name.
   * @param icon The icon resource name (passed to {@link Images}).
   * @param group The base group for the command. This is used for the default
   * context and toolbar groups. The main menu group is also derived from this.
   * @param description A tooltip-length description of the command.
   * @param interactive True of the command will interact with the user when
   * executed.
   */
  public AbstractCommand (String name, Icon icon, String group, 
                          String description,
                          boolean interactive)
  {
    this (name, icon, group,  description, interactive, '\0', 0);
  }
  
  /**
   * Create a new instance.
   * 
   * @param name The command name.
   * @param iconName The icon resource name (passed to {@link Images}).
   * @param group The base group for the command. This is used for the default
   * context and toolbar groups. The main menu group is also derived from this.
   * @param description A tooltip-length description of the command.
   * @param interactive True of the command will interact with the user when
   * executed.
   */
  public AbstractCommand (String name, String iconName, String group, 
                          String description,
                          boolean interactive)
  {
    this (name, iconName, group,  description, interactive, '\0', 0);
  }
  
  /**
   * Create a new instance.
   * 
   * @param name The command name.
   * @param iconName The icon resource name (passed to {@link Images}).
   * @param group The base group for the command. This is used for the default
   * context and toolbar groups. The main menu group is also derived from this.
   * @param description A tooltip-length description of the command.
   * @param interactive True of the command will interact with the user when
   * executed.
   * @param mnemonic A menu mnemonic for the command.
   * @param accelerator The global accelerator key for the command.
   */
  public AbstractCommand (String name, String iconName, String group, 
                          String description,
                          boolean interactive, char mnemonic, int accelerator)
  {
    this (name, (iconName != null) ? Images.createIcon (iconName) : null,
          group, description, interactive, mnemonic, accelerator);
  }

  /**
   * Create a new instance.
   * 
   * @param name The command name.
   * @param icon The command icon.
   * @param group The base group for the command. This is used for the default
   * context and toolbar groups. The main menu group is also derived from this.
   * @param description A tooltip-length description of the command.
   * @param interactive True of the command will interact with the user when
   * executed.
   * @param mnemonic A menu mnemonic for the command.
   * @param accelerator The global accelerator key for the command.
   */
  public AbstractCommand (String name, Icon icon, String group, String description,
                          boolean interactive, char mnemonic, int accelerator)
  {
    this.name = name;
    this.icon = icon;
    this.description = description;
    this.interactive = interactive;
    this.mnemonic = mnemonic;
    this.accelerator = accelerator;
    this.displayName = null;
    this.enabled = true;
    
    this.contextMenuGroup = group;
    this.toolbarGroup = group;
    this.mainMenuGroup = "Tools." + group;
  }
  
  public Object clone () throws CloneNotSupportedException
  {
    AbstractCommand copy = (AbstractCommand)super.clone ();
    
    copy.listeners = new EventListenerList ();
    
    return copy;
  }

  public abstract void execute ();

  public void addCommandListener (CommandListener l)
  {
    listeners.addListener (l);
  }
  
  public void removeCommandListener (CommandListener l)
  {
    listeners.removeListener (l);
  }
  
  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return contextMenuGroup;
    else if (viewName.equals (CommandView.TOOLBAR_VIEW))
      return toolbarGroup;
    else if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return mainMenuGroup;
    else
      return null;
  }
  
  public Icon getIcon ()
  {
    return icon;
  }

  public void setIcon (Icon newIcon)
  {
    Icon oldIcon = icon;
    
    icon = newIcon;
    
    firePropertyChange ("icon", oldIcon, newIcon);
  }

  public String getName ()
  {
    return name;
  }

  public String getDescription ()
  {
    return description;
  }

  public boolean isInteractive ()
  {
    return interactive;
  }

  public String getMainMenuGroup ()
  {
    return mainMenuGroup;
  }
  
  public String getContextMenuGroup ()
  {
    return contextMenuGroup;
  }
  
  public String getToolbarGroup ()
  {
    return toolbarGroup;
  }
  
  public char getMnemonic ()
  {
    return mnemonic;
  }

  /**
   * eg SWT.CONTROL | SWT.SHIFT | 'T', SWT.ALT | SWT.F2
   */
  public int getAccelerator ()
  {
    return accelerator;
  }

  public String getDisplayName ()
  {
    return displayName != null ? displayName : createDefaultDisplayName ();
  }

  public void setDisplayName (String newDisplayName)
  {
    String oldDisplayName = getDisplayName ();
    
    displayName = newDisplayName;
    
    firePropertyChange ("displayName", oldDisplayName, newDisplayName);
  }

  /**
   * Generates a display name based on the last component of the
   * command name. eg "file.oc.Edit" becomes "Edit". Adds mnemonic &
   * if mnemonic is set.
   */
  protected String createDefaultDisplayName ()
  {
    String commandName = getName ();
    String defaultDisplayName;
    
    int i = commandName.lastIndexOf ('.');

    if (i == -1)
      defaultDisplayName = commandName;
    else
      defaultDisplayName = commandName.substring (i + 1, commandName.length ());
    
    // add mnemonic & character
    if (mnemonic != 0)
    {
      int mnemonicIndex = defaultDisplayName.toLowerCase ().indexOf (mnemonic);
      
      if (mnemonicIndex != -1)
      {
        defaultDisplayName =
          defaultDisplayName.substring (0, mnemonicIndex) + '&' +
          defaultDisplayName.substring (mnemonicIndex);
      }
    }
    
    return defaultDisplayName;
  }

  /**
   * Simply returns the command name followed by empty brackets.
   */
  public String getLogString ()
  {
    return getName () + " ()";
  }

  public boolean isEnabled ()
  {
    return enabled;
  }

  public void setEnabled (boolean newValue)
  {
    boolean oldValue = enabled;
    
    enabled = newValue;

    firePropertyChange ("enabled", oldValue, newValue);
  }
  
  /**
   * Sets the accelerator.
   * 
   * @param newAccelerator The accelerator to set
   */
  public void setAccelerator (int newAccelerator)
  {
    int oldAccelerator = accelerator;
    
    this.accelerator = newAccelerator;
    
    firePropertyChange ("accelerator", oldAccelerator, newAccelerator);
  }

  /**
   * Sets the contextMenuGroup.
   * 
   * @param newContextMenuGroup The contextMenuGroup to set
   */
  public void setContextMenuGroup (String newContextMenuGroup)
  {
    String oldContextMenuGroup = newContextMenuGroup;
    this.contextMenuGroup = newContextMenuGroup;
    
    firePropertyChange (CommandView.CONTEXT_MENU_VIEW, oldContextMenuGroup, newContextMenuGroup);
  }

  /**
   * Sets the description.
   * 
   * @param newDescription The description to set
   */
  public void setDescription (String newDescription)
  {
    String oldDescription = description;
    
    this.description = newDescription;
    
    firePropertyChange ("description", oldDescription, newDescription);
  }

  /**
   * Sets the interactive.
   * 
   * @param newInteractive The interactive to set
   */
  public void setInteractive (boolean newInteractive)
  {
    boolean oldInteractive = interactive;
    
    this.interactive = newInteractive;
    
    firePropertyChange ("interactive", oldInteractive, newInteractive);
  }

  /**
   * Sets the mainMenuGroup.
   * 
   * @param newMainMenuGroup The mainMenuGroup to set
   */
  public void setMainMenuGroup (String newMainMenuGroup)
  {
    String oldMainMenuGroup = mainMenuGroup;
    
    this.mainMenuGroup = newMainMenuGroup;
    
    firePropertyChange (CommandView.MAIN_MENU_VIEW, oldMainMenuGroup, newMainMenuGroup);
  }

  /**
   * Sets the mnemonic.
   * 
   * @param newMnemonic The mnemonic to set
   */
  public void setMnemonic (char newMnemonic)
  {
    char oldMnemonic = mnemonic;
    
    this.mnemonic = newMnemonic;
    
    firePropertyChange ("mnemonic", oldMnemonic, newMnemonic);
  }

  /**
   * Sets the name.
   * 
   * @param newName The name to set
   */
  public void setName (String newName)
  {
    String oldName = name;
    
    this.name = newName;
    
    firePropertyChange ("name", oldName, newName);
  }

  /**
   * Sets the toolbarGroup.
   * 
   * @param newToolbarGroup The toolbarGroup to set
   */
  public void setToolbarGroup (String newToolbarGroup)
  {
    String oldToolbarGroup = toolbarGroup;
    
    this.toolbarGroup = newToolbarGroup;
    
    firePropertyChange (CommandView.TOOLBAR_VIEW, oldToolbarGroup, newToolbarGroup);
  }
  
  public void fireCommandExecuted ()
  {
    fireCommandExecuted (null);
  }
  
  public void fireCommandExecuted (Object data)
  {
    if (listeners.hasListeners ())
    {
      listeners.fireEvent ("commandExecuted", new CommandEvent (this, data));
    }
  }
}
