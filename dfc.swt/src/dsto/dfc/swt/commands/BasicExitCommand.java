package dsto.dfc.swt.commands;


/**
 * Base class for Exit commands. By default calls System.exit ().
 *
 * @version $Revision$
 */
public class BasicExitCommand extends AbstractCommand
{
  public BasicExitCommand ()
  {
    super ("file.Exit", NO_ICON, "exit",
           "Exit the application", false, 'x', 0);
    
    setMainMenuGroup ("File.exit");
  }

  public void execute ()
  {
    System.exit (0);
  }
}