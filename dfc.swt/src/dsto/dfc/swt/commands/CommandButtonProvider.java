package dsto.dfc.swt.commands;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import dsto.dfc.swt.icons.Icon;
import dsto.dfc.swt.icons.IconCache;

/**
 * Connects and synchronises a button with a command.
 * 
 * @todo Support text on the button: currently requires an icon.
 * 
 * @author Matthew Phillips
 */
public class CommandButtonProvider
  implements Listener, PropertyChangeListener
{
  private Command command;
  private Button button;

  public CommandButtonProvider (Composite parent, Command command)
  {
    this (new Button (parent, SWT.PUSH), command);
  }
  
  public CommandButtonProvider (Button button, Command command)
  {
    this.command = command;
    this.button = button;
    
    button.addListener (SWT.Selection, this);
    button.addListener (SWT.Dispose, this);
    command.addPropertyChangeListener (this);
    
    updateIcon ();    
    updateTooltip ();
    updateEnabled ();
  }
  
  private void dispose ()
  {
    command.removePropertyChangeListener (this);
    button.removeListener (SWT.Selection, this);
    button.removeListener (SWT.Dispose, this);
    
    uncacheIcon ();   
  }

  private void uncacheIcon ()
  {
    Icon icon = (Icon)button.getData ("commandIcon");
    
    if (icon != null)
      IconCache.getDefault ().remove (icon);
  }
  
  private void updateTooltip ()
  {
    button.setToolTipText (command.getDescription ());
  }

  private void updateIcon ()
  {
    uncacheIcon ();
    
    Icon icon = command.getIcon ();
    
    button.setData ("commandIcon", icon);
    
    if (icon != null)
    {
      Image image = IconCache.getDefault ().put (icon);
      
      button.setImage (image);
    } else
    {
      button.setImage (null);
    }
  }
  
  private void updateEnabled ()
  {
    button.setEnabled (command.isEnabled ());
  }

  public void handleEvent (Event e)
  {
    if (e.type == SWT.Selection)
    {
      if (command.isEnabled ())
        command.execute ();
    } else if (e.type == SWT.Dispose)
    {
      dispose ();
    }
  }
  
  public void propertyChange (PropertyChangeEvent e)
  {
    String property = e.getPropertyName ();
    
    if (property.equals ("icon"))
      updateIcon ();
    else if (property.equals ("description"))      
      updateTooltip ();
    else if (property.equals ("enabled"))
      updateEnabled ();
  }
}
