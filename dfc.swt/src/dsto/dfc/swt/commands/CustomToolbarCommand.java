package dsto.dfc.swt.commands;

import org.eclipse.swt.widgets.ToolBar;

/**
 * Interface for commands that wish to provide a custom toolbar UI.
 * 
 * @version $Revision$
 */
public interface CustomToolbarCommand
{

  /**
   * Create a toolbarUI provider for the command.
   * 
   * @param view The command view.
   * @param toolbar The host toolbar for the UI.
   * @param index The index for the UI item(s).
   * @return A CommandMenuItemProvider that provides the UI.
   */
  public CommandToolbarItemProvider createToolbarProvider (CommandView view,
                                                           ToolBar toolbar,
                                                           int index);
}
