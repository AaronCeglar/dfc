package dsto.dfc.swt.commands;

import dsto.dfc.swt.icons.Icon;
import dsto.dfc.util.PropertyEventSource;

/**
 * Defines a command that may presented in a UI. Most commands will extend
 * {@link dsto.dfc.swt.commands.AbstractCommand}.
 *
 * @version $Revision$
 * 
 * @see dsto.dfc.swt.commands.AbstractCommand
 */
public interface Command extends PropertyEventSource
{
  /**
   * Execute the command. Commands should fire
   * {@link CommandListener#commandExecuted(CommandEvent)} when executed.
   */
  public void execute ();

  /**
   * The symbolic icon for the command. May be null.
   */
  public Icon getIcon ();
  
  /**
   * The formal identifying name for this command (eg edit.Copy, file.Open).
   * This <em>must not change</em> during the lifetime of a command
   * instance.
   */
  public String getName ();

  /**
   * The name of the command as it should be displayed (eg on a menu).  May
   * be null in which case the command name (or some derivative) will be
   * used.
   */
  public String getDisplayName ();

  /**
   * A short description of the command, suitable for a tooltip or use
   * in a status panel.
   */
  public String getDescription ();

  /**
   * A string that is suitable to describe this command and its
   * parameters for logging purposes.
   */
  public String getLogString ();

  /**
   * True if the command is enabled, ie able to execute in the current
   * context.
   */
  public boolean isEnabled ();

  /**
   * True if this command will interact with the user when execute ()
   * is called.
   */
  public boolean isInteractive ();
  
  /**
   * Get the preferred group that this command should appear in for a
   * given view.  eg "File.doc" for a main menu or "edit" for a context
   * menu. Deeply nested groups such as "View.window.Windows.recent" will be
   * placed in nested pullright menus.<p>
   * 
   * NOTE: groups may be overridden by a setting in the CommandRegistry.<p>
   *
   * If the command changes its group it should generate a property change event
   * with the property name the name of the view, the old value set to the old
   * group and the new value set to the new group.
   *
   * @param viewName The name of the view.
   * @return The group in the view, or null if this command does not
   * appear in the given view.
   */
  public String getGroupInView (String viewName);

  /**
   * Get a character shortcut for the command (eg for use on a menu).  May
   * Return 0 for no mnemonic.
   */
  public char getMnemonic ();

  /**
   * Get a keyboard accelerator for the command.
   * 
   * @return The SWT acclerator (eg SWT.CONTROL | SWT.SHIFT | 'T',
   * SWT.ALT | SWT.F2) or 0 for no accelerator.
   */
  public int getAccelerator ();
  
  // setters for command properties that may be overridden by clients
  
  public void setDisplayName (String newDisplayName);
  
  public void setIcon (Icon newIcon);
  
  public void setEnabled (boolean newValue);
  
  public void setAccelerator (int newAccelerator);
  
  /**
   * Sets the mnemonic.
   * 
   * @param newMnemonic The mnemonic to set
   */
  public void setMnemonic (char newMnemonic);
  
  /**
   * Sets the group that command appears in context menus.
   * 
   * @see #getGroupInView(String)
   */
  public void setContextMenuGroup (String newContextMenuGroup);

  /**
   * Sets the group that command appears in toolbars.
   * 
   * @see #getGroupInView(String)
   */
  public void setToolbarGroup (String newToolbarGroup);
  
  /**
   * Sets the group that command appears in the main menu.
   * 
   * @see #getGroupInView(String)
   */
  public void setMainMenuGroup (String newMainMenuGroup);
  
  /**
   * Sets the description.
   * 
   * @param newDescription The description to set
   */
  public void setDescription (String newDescription);
  
  // event listeners
  
  public void addCommandListener (CommandListener l);
   
  public void removeCommandListener (CommandListener l);
}
