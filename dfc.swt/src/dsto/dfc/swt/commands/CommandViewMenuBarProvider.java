package dsto.dfc.swt.commands;

import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

/**
 * Synchronizes a menu bar with a CommandView.
 *
 * @version $Revision$
 */
public final class CommandViewMenuBarProvider implements CommandViewNodeListener
{
  private CommandView commandView;
  /** The root node for the menubar */
  private CommandViewNode root;
  /** The menu bar we are managing */
  private Menu menuBar;
  /** Maps watched tree nodes to their menus */
  private HashMap nodeToMenu = new HashMap (15);

  /**
   * Create a new instance.
   *
   * @param menuBar The menu bar to synchronise.
   * @param commandView The tree of commands.
   * @param root The root node of the tree to use for this menu.
   */
  public CommandViewMenuBarProvider (Menu menuBar,
                                     CommandView commandView,
                                     CommandViewNode root)
  {
    this.menuBar = menuBar;
    this.commandView = commandView;
    this.root = root;

    root.addCommandViewNodeListener (this);

    addEntries ();
  }

  /**
   * Remove all listeners and references.
   */
  public void dispose ()
  {
    root.removeCommandViewNodeListener (this);

    for (int i = 0; i < root.getChildCount (); i++)
    {
      CommandViewNode node = root.getChild (i);

      removeMenu (node);
    }
  }

  /**
   * Populate menu bar with entries.
   */
  private void addEntries ()
  {
    for (int i = 0; i < root.getChildCount (); i++)
    {
      CommandViewNode node = root.getChild (i);

      addMenu (node);
    }
  }

  /**
   * Add a menu to the menu bar.
   *
   * @param node The node containing the command to add.
   * @param nodeIndex The index of the node.
   */
  private void addMenu (CommandViewNode node)
  {
    // sanity check: only add groups to menu bar
    if (node.getCommand () != null)
      throw new IllegalArgumentException ("attempt to add a command as a menu");

    MenuItem menuItem =
      new MenuItem (menuBar, SWT.CASCADE, node.getIndex ());
    Menu menu = new Menu (menuItem);
    menuItem.setText (node.getName ());
    menuItem.setMenu (menu);

//    menuItem.setMnemonic (CommandRegistry.getMenuMnemonic (menuName));

    CommandViewMenuProvider menuProvider =
      new CommandViewMenuProvider (menu, null, commandView, node);

    menu.setData ("menuSync", menuProvider);

    nodeToMenu.put (node, menu);
  }

  /**
   * Remove a menu to the menu bar.
   *
   * @param node The node containing the command to add.
   */
  private void removeMenu (CommandViewNode node)
  {
    Menu menu = (Menu)nodeToMenu.remove (node);

    CommandViewMenuProvider menuProvider =
      (CommandViewMenuProvider)menu.getData ("menuSync");

    menuProvider.dispose ();

    menu.setData ("menuSync", null);

    menu.getParentItem ().dispose ();
  }

  // CommandViewNodeListener interface

  public void childAdded (CommandViewNodeEvent e)
  {
    addMenu (e.getNode ());
  }

  public void childRemoved (CommandViewNodeEvent e)
  {
    removeMenu (e.getNode ());
  }

  public void childMoved (CommandViewNodeEvent e)
  {
    removeMenu (e.getNode ());
    addMenu (e.getNode ());
  }

  public void commandChanged (CommandViewNodeEvent e)
  {
    // should never see this
    throw new Error ("command event on a menu-level node");
  }
}
