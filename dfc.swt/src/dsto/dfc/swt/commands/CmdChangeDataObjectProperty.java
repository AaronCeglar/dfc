package dsto.dfc.swt.commands;

import java.util.Arrays;

import org.eclipse.swt.widgets.Display;

import dsto.dfc.util.Disposable;
import dsto.dfc.util.Objects;

import dsto.dfc.databeans.IDataObject;
import dsto.dfc.databeans.ListDataObject;
import dsto.dfc.databeans.PropertyEvent;
import dsto.dfc.databeans.PropertyListener;

import dsto.dfc.swt.icons.Icon;
import dsto.dfc.swt.icons.ImageResourceIcon;
import dsto.dfc.swt.util.UIPropertyListener;

/**
 * Command to change a property of one or more target data objects. It
 * becomes enabled only when one or more target bjects have property
 * values different from the target value.
 * <p>
 * 
 * NOTE: this command must be disposed after use since it adds
 * listeners to the target targetObjects.
 * 
 * @author Matthew Phillips
 */
public class CmdChangeDataObjectProperty
  extends AbstractCommand implements Command, PropertyListener, Disposable
{
  protected IDataObject [] targetObjects;
  protected Object property;
  protected Object value;
  protected ListDataObject objectList;
  protected UIPropertyListener listener;

  /**
   * Create a new instance.
   * 
   * @param name The command name.
   * @param property The property to set on the target objects.
   * @param value The vaue to set.
   * @param group The command group.
   * @param description The command description.
   * @param iconName The command icon resource name.
   */
  public CmdChangeDataObjectProperty (String name, Object property, Object value,
                                      String group, String description,
                                      String iconName)
  {
    this (name, property, value, group, description,
          new ImageResourceIcon (iconName));
    
  }
  
  /**
   * Create a new instance.
   * 
   * @param name The command name.
   * @param property The property to set on the target objects.
   * @param value The vaue to set.
   * @param group The command group.
   * @param description The command description.
   * @param icon The command icon.
   */
  public CmdChangeDataObjectProperty (String name, Object property, Object value,
                                String group, String description,
                                Icon icon)
  {
    super (name, icon, group, description, false, '\0', 0);
    
    this.property = property;
    this.value = value;
    this.objectList = new ListDataObject ();
    
    listener = new UIPropertyListener (Display.getCurrent (), this);
    
    objectList.addPropertyListener (listener);
    
    updateEnabled ();
  }
  
  public void dispose ()
  {
    setTargetObjects (null);
  }

  /**
   * Set the target objects to be changed. May be null.
   */
  public void setTargetObjects (IDataObject [] targetObjects)
  {
    this.targetObjects = targetObjects;
    
    objectList.clear ();
    
    if (targetObjects != null)
      objectList.addAll (Arrays.asList (targetObjects));
    
    updateEnabled ();
  }

  public IDataObject [] getTargetObjects ()
  {
    return targetObjects;
  }
  
  private void updateEnabled ()
  {
    setEnabled (testShouldEnable ());
  }

  /**
   * Called when target property changes to see if the command should
   * be enabled. Default is to disable when all targets have command's
   * target value. Subclasses may override.
   */
  protected boolean testShouldEnable ()
  {
    return targetObjects != null && hasDifferentValue ();
  }
  
  protected boolean hasDifferentValue ()
  {
    for (int i = 0; i < targetObjects.length; i++)
    {
      if (!Objects.objectsEqual (targetValue (i), value))
        return true;
    }
    
    return false;
  }

  /**
   * Get the value from the n'th target object.
   */
  protected Object targetValue (int n)
  {
    return targetObjects [n].getValue (property);
  }

  public void execute ()
  {
    for (int i = 0; i < targetObjects.length; i++)
      targetObjects [i].setValue (property, value);
    
    fireCommandExecuted (targetObjects);
  }
  
  public void propertyValueChanged (PropertyEvent e)
  {
    if (e.path.length () == 2 && e.path.last ().equals (property))
      updateEnabled ();
  }
}
