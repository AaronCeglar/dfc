package dsto.dfc.swt.commands;

/**
 * Interface for providers of menu command representations (eg menu button,
 * checkboxes, etc). The constructor of instances of this interface create
 * and manage the UI for a single command in a menu.
 * 
 * @author mpp
 * @version $Revision$
 */
public interface CommandMenuItemProvider
{
  /**
   * The number of items the provider created.
   */
  public int getItemCount ();
  
  /**
   * The index of the first item the provider created.
   */
  public int getItemStart ();
  
  /**
   * Dispose the provider and all items created.
   */
  public void dispose ();
}
