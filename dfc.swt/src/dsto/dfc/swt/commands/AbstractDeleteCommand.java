package dsto.dfc.swt.commands;

import org.eclipse.swt.SWT;

/**
 * Basic delete command.
 *
 * @version $Revision$
 */
public abstract class AbstractDeleteCommand extends AbstractCommand
{
  public AbstractDeleteCommand ()
  {
    super ("edit.Delete", "delete.gif", "delete",
           "Delete the selected entry", false, 'd', SWT.BS);
    
    setMainMenuGroup ("Edit.change");
  }
}