package dsto.dfc.swt.commands;

import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;

import dsto.dfc.util.Beans;
import dsto.dfc.util.Disposable;
import dsto.dfc.util.Objects;

import dsto.dfc.logging.Log;

import dsto.dfc.swt.icons.Icon;

import static dsto.dfc.swt.icons.Images.createIcon;

/**
 * Command that sets the value of a Bean property (ie via a setX () method). The
 * command is displayed as checked (or selected when shown as a button on a
 * toolbar) when the current property value matches the command. The target Bean
 * must support property change notification via the JFace
 * IPropertyChangeListener or java.beans.PropertyChangeListener interface.
 * 
 * @author Matthew Phillips
 */
public class CmdSetBeanProperty extends BasicToggleCommand
  implements IPropertyChangeListener, Disposable
{
  private Object bean;
  private String property;
  private Object propertyValue;
  
  /**
   * Create an instance of the command with no icon.
   * 
   * @param bean The bean to update.
   * @param property The property of the bean to set.
   * @param value The value of the property to set.
   * @param name The name of the command.
   * @param group The group of the command.
   * @param description The command's description.
   */
  public CmdSetBeanProperty (Object bean, String property, Object value,
                             String name, String group, String description)
  {
    this (bean, property, value, name, (String)null, group, description);
  }
  
  /**
   * Create an instance of the command.
   * 
   * @param bean The bean to update.
   * @param property The property of the bean to set.
   * @param value The value of the property to set.
   * @param name The name of the command. May be null.
   * @param iconName The name of the icon resource.
   * @param group The group of the command.
   * @param description The command's description.
   */
  public CmdSetBeanProperty (Object bean, String property, Object value,
                             String name, String iconName,
                             String group, String description)
  {
    this (bean, property, value, name, createIcon (iconName), group, 
          description);
  }

  /**
   * Create an instance of the command.
   * 
   * @param bean The bean to update.
   * @param property The property of the bean to set.
   * @param value The value of the property to set.
   * @param name The name of the command.
   * @param icon The icon for the command. May be null.
   * @param group The group of the command.
   * @param description The command's description.
   */
  public CmdSetBeanProperty (Object bean, String property, Object value,
                             String name, Icon icon,
                             String group, String description)
  {
    super (name, icon, group, description, false, '\0', 0);
    
    this.bean = bean;
    this.property = property;
    this.propertyValue = value;
    
    updateChecked ();
    
    if (!Beans.addListener (bean, "propertyChange",
                            IPropertyChangeListener.class, this))
    {
      // try the java.beans version
      if (!Beans.addListener (bean, "propertyChange",
                              PropertyChangeListener.class, this))
      {
        Log.warn ("Could not listen for property events: " +
                  "command check state will get out of sync", this);
      }
    }
  }
  
  public void dispose ()
  {
    if (!Beans.removeListener (bean, "propertyChange",
                            IPropertyChangeListener.class, this))
    {
      Beans.removeListener (bean, "propertyChange",
                            PropertyChangeListener.class, this);
    }
  }
  
  public void execute ()
  {
    try
    {
      Beans.setPropertyValue (bean, property, propertyValue);
      
      fireCommandExecuted (propertyValue);
    } catch (NoSuchMethodException ex)
    {
      Log.internalError ("No property called " + property, this, ex);
    } catch (InvocationTargetException ex)
    {
      Log.internalError ("Error while setting " + property +
                         " to " + propertyValue, this, ex);
    }
  }

  public void propertyChange (PropertyChangeEvent e)
  {
    if (e.getProperty ().equals (property))
      updateChecked ();
  }

  /**
   * Update the checked state of the command by comparing the target property
   * value with the current value.
   */
  private void updateChecked ()
  {
    try
    {
      Object currentValue = Beans.getPropertyValue (bean, property);
      
      setValue (Objects.objectsEqual (propertyValue, currentValue));
    } catch (NoSuchMethodException ex)
    {
      Log.internalError ("No property called " + property, this, ex);
    }
  }
}
