package dsto.dfc.swt.commands;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.Decorations;
import org.eclipse.swt.widgets.Menu;

/**
 * Generic command utility methods.
 * 
 * @author mpp
 * @version $Revision$
 */
public final class Commands
{
  public  static final Object SHOW_NAME_KEY = "toolbar_show_name";
  
  private Commands ()
  {
    // zip
  }

  /**
   * Connect a command view with a menu. The menu is synchronised with
   * entries from the view.
   * 
   * @param view The command view. Must be a context menu view.
   * @param menu The menu.
   * 
   * @throws IllegalArgumentException if command view is not a context
   *           menu view.
   * 
   * @see CommandViewMenuProvider
   */
  public static void syncMenu (CommandView view, Menu menu)
  {
    if (!view.getViewName ().equals (CommandView.CONTEXT_MENU_VIEW))
      throw new IllegalArgumentException
        ("Command view is not a context menu view: " + view.getViewName ());
    
    new CommandViewMenuProvider (menu, null, view, view.getTreeRoot ());
  }
  
  /**
   * Get/create the context menu view for a control. The context menu created
   * from the view is installed as the control's menu via setMenu ().
   */
  public static CommandView getContextView (Control control)
  {
    CommandView view =
      (CommandView)control.getData (CommandView.CONTEXT_MENU_VIEW);
    
    if (view == null)
    {
      view = new CommandView (CommandView.CONTEXT_MENU_VIEW);
      control.setData (CommandView.CONTEXT_MENU_VIEW, view);
      
      Menu menu = new Menu (control);
      control.setMenu (menu);
      
      new CommandViewMenuProvider (menu, control, view, view.getTreeRoot ());
    }
    
    return view;
  }
  
  public static void setContextMenuView (Control control, CommandView view)
  {
    control.setData (CommandView.CONTEXT_MENU_VIEW, view);
  }
   
  /**
   * Get/create the main menu view for a control. The menu bar created from
   * the view is installed shell's menu bar.
   */
  public static CommandView getMenuView (Decorations shell)
  {
    CommandView view =
      (CommandView)shell.getData (CommandView.MAIN_MENU_VIEW);
    
    if (view == null)
    {
      view = new CommandView (CommandView.MAIN_MENU_VIEW);
      
      createMenu (shell, view);
    }
    
    return view;
  }
  
  public static void createMenu (Decorations shell, CommandView view)
  {
    shell.setData (CommandView.MAIN_MENU_VIEW, view);
    
    Menu menu = new Menu (shell, SWT.BAR);
    
    new CommandViewMenuBarProvider (menu, view, view.getTreeRoot ());
    
    shell.setMenuBar (menu);
  }

  public static void setMenuView (Control control, CommandView view)
  {
    control.setData (CommandView.MAIN_MENU_VIEW, view);
  }
   
  public static CommandView getToolbarView (Control control)
  {
    return getCommandView (control, CommandView.TOOLBAR_VIEW); 
  }
  
  public static void setToolbarView (Control control, CommandView view)
  {
    control.setData (CommandView.TOOLBAR_VIEW, view);
  }
  
  /**
   * Get/create the menu view for a control.
   */
  public static CommandView getCommandView (Control control, String viewName)
  {
    CommandView view = (CommandView)control.getData (viewName);
    
    if (view == null)
    {
      view = new CommandView (viewName);
      control.setData (viewName, view);
    }
    
    return view;
  }

  /**
   * Create a toolbar from the specified command view.
   */
  public static void createToolbar (CoolBar coolbar,
                                  CommandView commandView)
  {
    new CommandViewToolBarProvider (coolbar, commandView);
  }

  /**
   * Shortcut to set the {@link #SHOW_NAME_KEY} command property to true. This
   * will hint to the toolbar UI generator to show the command's name as well
   * as its icon on the toolbar. Tip: you should call this before adding the
   * command to the view for the toolbar UI to honor it.
   */
  public static void showName (CommandView view, Command command)
  {
    showName (view, command.getName ());
  }
  
  /**
   * Shortcut to set the {@link #SHOW_NAME_KEY} command property to true. This
   * will hint to the toolbar UI generator to show the command's name as well
   * as its icon on the toolbar. Tip: you should call this before adding the
   * command to the view for the toolbar UI to honor it.
   */
  public static void showName (CommandView view, String commandName)
  {
    view.setCommandProperty (commandName, SHOW_NAME_KEY, Boolean.TRUE);
  }
  
  /**
   * Find a command in the context or toolbar view for a control.
   */
  public static Command findCommand (Control control, String name)
  {
    CommandView view = getCommandView (control);
    
    if (view != null)
      return view.findCommand (name);
    else
      return null;
  }
  
  /**
   * Find a command in the context or toolbar view for a control.
   */
  public static Command findCommand (Control control, Class commandClass)
  {
    CommandView view = getCommandView (control);
    
    if (view != null)
      return view.findCommand (commandClass);
    else
      return null;
  }
  
  private static CommandView getCommandView (Control control)
  {
    CommandView view =
      (CommandView)control.getData (CommandView.CONTEXT_MENU_VIEW);
    
    if (view != null)
      return view;
    else
      return (CommandView)control.getData (CommandView.TOOLBAR_VIEW);
  }
}
