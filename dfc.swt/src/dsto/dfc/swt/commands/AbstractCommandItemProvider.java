package dsto.dfc.swt.commands;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Widget;

import dsto.dfc.swt.icons.Icon;
import dsto.dfc.swt.icons.IconCache;

/**
 * Abstract base class for command UI providers that represent a command with
 * an SWT Item. Subclasses implement {@link #createItem(Widget, int)} and may
 * choose to override any of the update* () methods.
 *
 * @author mpp
 * @version $Revision$
 */
public abstract class AbstractCommandItemProvider
  implements Listener, PropertyChangeListener
{
  protected Widget parent;
  protected Item item;
  protected Command command;
  protected CommandView commandView;

  public AbstractCommandItemProvider (CommandView commandView,
                                      Command command,
                                      Widget parent,
                                      int index)
  {
    this.parent = parent;
    this.command = command;
    this.commandView = commandView;
    this.item = createItem (parent, index);

    // update display attributes
    updateDisplayName ();
    updateIcon ();
    updateEnabled ();
    updateAccelerator ();
    updateDescription ();

    item.addListener (SWT.Selection, this);
    item.addListener (SWT.Dispose, this);
    command.addPropertyChangeListener (this);
  }

  /**
   * Create the item for this provider.
   */
  protected abstract Item createItem (Widget parentWidget, int index);

  public void dispose ()
  {
    command.removePropertyChangeListener (this);

    item.dispose ();
  }

  public int getItemCount ()
  {
    return 1;
  }

  // command/menu item synchronization logic

  protected void updateDisplayName ()
  {
    String text = CommandRegistry.getDisplayName (command);

    item.setText (text);
  }

  protected void updateIcon ()
  {
    Icon icon = command.getIcon ();
    Icon oldIcon = (Icon)item.getData ("commandIcon");

    if (oldIcon != null)
      IconCache.getDefault ().remove (oldIcon);

    item.setData ("commandIcon", icon);

    if (icon != null)
    {
      Image image = IconCache.getDefault ().put (icon);

      item.setImage (image);
    } else
    {
      item.setImage (null);
    }
  }

  protected void updateEnabled ()
  {
    // zip
  }

  protected void updateAccelerator ()
  {
    // zip
  }

  protected void updateDescription ()
  {
    // zip
  }

  protected void updateProperty (String string)
  {
    // zip
  }

  // PropertyChangeListener interface

  public void propertyChange (PropertyChangeEvent e)
  {
    final String name = e.getPropertyName ();

    // ensure we're on the SWT thread
    Runnable updater = new Runnable ()
    {
      public void run ()
      {
        if (name.equals ("displayName") || name.equals ("interactive"))
          updateDisplayName ();
        else if (name.equals ("icon"))
          updateIcon ();
        else if (name.equals ("enabled"))
          updateEnabled ();
        else if (name.equals ("accelerator"))
          updateAccelerator ();
        else if (name.equals ("description"))
          updateDescription ();
        else
          updateProperty (name);
      }
    };

    if (Thread.currentThread () != parent.getDisplay ().getThread ())
      parent.getDisplay ().syncExec (updater);
    else
      updater.run ();
  }

  public void handleEvent (Event e)
  {
    if (e.type == SWT.Selection)
    {
      if (command.isEnabled ())
        command.execute ();
    } else if (e.type == SWT.Dispose)
    {
      command.removePropertyChangeListener (this);

      if (command.getIcon () != null)
        IconCache.getDefault ().remove (command.getIcon ());
    }
  }
}
