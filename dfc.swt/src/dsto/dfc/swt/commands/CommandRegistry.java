package dsto.dfc.swt.commands;

import java.util.ArrayList;

/**
 * Registry of view to group mappings for commands.
 *
 * @version $Revision$
 */
public final class CommandRegistry
{
  public static String getGroupInView (String view,
                                       Command command)
  {
    // TODO: implement
    return command.getGroupInView (view);
  }

  public static String [] splitGroup (String group)
  {
    ArrayList elements = new ArrayList ();
    int start = 0;
    int end = -1;

    do
    {
      end = group.indexOf ('.', start);
      if (end == -1)
        end = group.length ();

      elements.add (group.substring (start, end));
      
      start = end + 1;
    } while (start < group.length ());

    String [] groups = new String [elements.size ()];
    elements.toArray (groups);

    return groups;
  }

  public static String getDisplayName (Command command)
  {
    return getDisplayName (command, false);
  }
  
  /**
   * Return the name that should be displayed for a command, taking into
   * account whether the command is interactive. If the command does not
   * specify a display name, the last component of the command name is used.
   * eg an interactive "file.New" command with no specified display name
   * would be displayed as "New...".
   *
   * @param command The command.
   * @param compact If true, the display name is made more compact by omitting
   * the "..." for interactive commands.
   * @return The display name for command.
   */
  public static String getDisplayName (Command command, boolean compact)
  {
    String displayName = command.getDisplayName ();

    if (displayName == null)
    {
      // if no display name, use last part of command name
      displayName = command.getName ();
      int start = displayName.lastIndexOf ('.');

      if (start == -1)
        start = 0;
      else
        start++;

      displayName = displayName.substring (start, displayName.length ());
    }

    if (!compact && command.isInteractive ())
      displayName += "...";

    return displayName;
  }

  /**
   * Get the short version of a command name eg 'file.Open' becomes 'Open'.
   */
  public static String getShortName (Command command)
  {
    String shortName = command.getName ();

    int start = shortName.lastIndexOf ('.');

    if (start == -1)
      start = 0;
    else
      start++;

    shortName = shortName.substring (start, shortName.length ());

    return shortName;
  }

  /**
   * Get the default mnemonic that should be used for menu titles.
   */
  public static char getMenuMnemonic (String menuName)
  {
    if (menuName.equalsIgnoreCase ("file"))
      return 'f';
    else if (menuName.equalsIgnoreCase ("edit"))
      return 'e';
    else if (menuName.equalsIgnoreCase ("view"))
      return 'v';
    else if (menuName.equalsIgnoreCase ("tools"))
      return 't';
    else if (menuName.equalsIgnoreCase ("help"))
      return 'h';
    else if (menuName.equalsIgnoreCase ("window"))
      return 'w';
    else
      return 0;
  }
}

