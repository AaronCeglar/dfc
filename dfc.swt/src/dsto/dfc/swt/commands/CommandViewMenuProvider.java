package dsto.dfc.swt.commands;

import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Widget;

import dsto.dfc.util.Debug;

/**
 * Synchronizes a popup menu with a CommandView.
 *
 * @version $Revision$
 */
public final class CommandViewMenuProvider
  implements CommandViewNodeListener, Listener
{
  private final CommandView commandView;
  /** The root node for the menu: all 1st level (eg groups) and 2nd
      level (commands and pull-rights) children become menu items. */
  private final CommandViewNode root;
  /** The menu we are managing */
  private final Menu menu;
  private final Widget control;
  /** Maps command tree nodes to their MenuProvider or menu item(s) in
   *  the case of separators and pullrights. */
  private final HashMap nodeToItemProvider;

  /**
   * Create a new instance.
   *
   * @param menu The menu to manage.
   * @param control The control hosting the menu (may be null). If non-null,
   * this control is monitored for double-clicks to invoke the default command.
   * This option will usually only be used by context menus.
   * @param commandView The command view backing the menu.
   */
  public CommandViewMenuProvider (Menu menu,
                                  Widget control,
                                  CommandView commandView)
  {
    this (menu, control, commandView, commandView.getTreeRoot ());
  }
  
  /**
   * Create a new instance.
   *
   * @param menu The menu to manage.
   * @param control The control hosting the menu (may be null). If non-null,
   * this control is monitored for double-clicks to invoke the default command.
   * This option will usually only be used by context menus.
   * @param commandView The command view backing the menu.
   * @param root The root of the command tree.
   */
  public CommandViewMenuProvider (Menu menu,
                                  Widget control,
                                  CommandView commandView,
                                  CommandViewNode root)
  {
    this.commandView = commandView;
    this.control = control;
    this.menu = menu;
    this.root = root;
    this.nodeToItemProvider = new HashMap (15);

    addEntries ();

    menu.addListener (SWT.Dispose, this);
    
    if (control != null)
      control.addListener (SWT.MouseDoubleClick, this);
    
    root.addCommandViewNodeListener (this);
  }

  /**
   * Remove all listeners and references created for menu
   * synchronisation.
   */
  public void dispose ()
  {
    root.removeCommandViewNodeListener (this);

    if (control != null)
      control.removeListener (SWT.MouseDoubleClick, this);

    for (int i = 0; i < root.getChildCount (); i++)
    {
      CommandViewNode node = root.getChild (i);

      if (node.getCommand () != null)
        removeCommand (node);
      else
        removeGroup (node, i);
    }
  }

  /**
   * Populate menu with entries from root.
   */
  private void addEntries ()
  {
    int index = 0;

    for (int i = 0; i < root.getChildCount (); i++)
      index += addGroup (root.getChild (i), index);
  }

  /**
   * Add a command to the menu.
   *
   * @param node The node containing the command to add.
   * @param node menuIndex The index where the menu item(s) for the
   * command should be added.
   * @return The number of menu items created.
   * @see #findMenuIndex
   */
  private int addCommand (CommandViewNode node, int menuIndex)
  {
    // create command menu item(s)
    int itemCount = createMenuItems (node, menuIndex);

    // watch command's node
    node.addCommandViewNodeListener (this);

    return itemCount;
  }

  /**
   * Add a group to the menu.
   *
   * @param groupNode The node representing a group (ie an immediate
   * child of root).
   * @param menuIndex The index where the menu item(s) for the group
   * should be added.
   * @return The number of menu items created.
   */
  private int addGroup (CommandViewNode groupNode, int menuIndex)
  {
    int currentMenuIndex = menuIndex;
    int groupIndex = groupNode.getIndex ();
    CommandViewNode parent = groupNode.getParent ();
    
    // separator is not visible if this is the first group in the menu
    if (groupIndex > 0)
    {
      // if not first group, create a separator for group
      MenuItem separator = new MenuItem (menu, SWT.SEPARATOR, currentMenuIndex);
      nodeToItemProvider.put (groupNode, separator);
      
      currentMenuIndex++;
    } else if (parent.getChildCount () > 1 && menu.getItemCount () > 0)
    {
      // if group is first in menu and there is a following group, create the
      // following group's separator

      MenuItem separator = new MenuItem (menu, SWT.SEPARATOR, currentMenuIndex);
      nodeToItemProvider.put (parent.getChild (1), separator);
    }

    // watch group node
    groupNode.addCommandViewNodeListener (this);

    // for each group entry (ie each command or pullright)
    for (int i = 0; i < groupNode.getChildCount (); i++)
    {
      CommandViewNode childNode = groupNode.getChild (i);

      // if entry is a command
      if (childNode.getCommand () != null)
      {
        currentMenuIndex += addCommand (childNode, currentMenuIndex);
      } else
      {
        // create pullright menu
        addPullRight (childNode, currentMenuIndex);

        currentMenuIndex++;
      }
    }

    return currentMenuIndex - menuIndex;
  }

  /**
   * Create a pullright menu for a group-within-a-group (ie a child of
   * a group that is not a command node).
   *
   * @param group The pullright node.
   * @param menuIndex The index where the menu item(s) for the group
   * should be added.
   */
  private void addPullRight (CommandViewNode node, int menuIndex)
  {
    // create pullright menu
    MenuItem menuItem = new MenuItem (menu, SWT.CASCADE, menuIndex);
    
    // NOTE: on Win32, we add spaces at end of text to avoid having arrow
    // sometimes appear with no space after text (windows bug?)
    if (SWT.getPlatform ().equals ("win32"))      
      menuItem.setText (node.getName () + "    ");
    else
      menuItem.setText (node.getName ());

    Menu submenu = new Menu (menuItem);
    
    menuItem.setMenu (submenu);

    new CommandViewMenuProvider (submenu, null, commandView, node);

    nodeToItemProvider.put (node, menuItem);
  }

  /**
   * Remove a command from the menu.
   *
   * @param node The command node to remove.
   */
  private void removeCommand (CommandViewNode node)
  {
    destroyMenuItems (node);
  }

  /**
   * Remove a group from the menu.
   *
   * @param groupNode The group node to remove.
   * @param groupIndex The previous index of the group in root (in
   * case groupNode has already been removed from its parent).
   */
  private void removeGroup (CommandViewNode groupNode, int groupIndex)
  {
    groupNode.removeCommandViewNodeListener (this);
    
    MenuItem separator = (MenuItem)nodeToItemProvider.remove (groupNode);

    if (separator != null)
      separator.dispose ();

    // for each group entry...
    for (int i = 0; i < groupNode.getChildCount (); i++)
    {
      CommandViewNode childNode = groupNode.getChild (i);

      if (childNode.getCommand () != null)
        removeCommand (childNode);
      else
        removePullRight (childNode);
    }

    // if group was first node and there is following group, make
    // following group's separator invisible
    CommandViewNode parent = groupNode.getParent ();

    if (groupIndex == 0 && parent.getChildCount () > 0)
    {
      MenuItem firstSeparator =
        (MenuItem)nodeToItemProvider.remove (parent.getChild (0));
  
      if (firstSeparator != null)
        firstSeparator.dispose ();
    }
  }

  /**
   * Remove a pullright menu item.
   *
   * @param node The node representing a pullright menu.
   */
  private void removePullRight (CommandViewNode node)
  {
    // remove menu item
    MenuItem menuItem = (MenuItem)nodeToItemProvider.remove (node);
    
    menuItem.dispose ();
  }

  /**
   * Move a group of menu items to match a group node move.  Assumes
   * group node has already moved.
   *
   * @param groupNode The group node that moved.
   * @param oldIndex The old index of the group node.
   * @param newIndex The new index of the group node.
   */
  private void moveGroup (CommandViewNode groupNode,
                          int oldIndex, int newIndex)
  {
    removeGroup (groupNode, oldIndex);
    addGroup (groupNode, newIndex);
  }

  /**
   * Move menu items to match a group child (command or pullright)
   * move.  Assumes node has already moved.
   *
   * @param childNode The node that moved.
   */
  private void moveGroupChild (CommandViewNode childNode, int newIndex)
  {
    if (childNode.getCommand () != null)
    {
      removeCommand (childNode);
      addCommand (childNode, newIndex);
    } else
    {
      removePullRight (childNode);
      addPullRight (childNode, newIndex);
    }
  }

  /**
   * Create and register menu item(s) for a command.
   *
   * @param commandNode The command node to create items for.
   * @param menuIndex The index for the new items.
   * @return The number of items that were created.
   */
  private int createMenuItems (CommandViewNode commandNode, int menuIndex)
  {
    Command command = commandNode.getCommand ();
    CommandMenuItemProvider menuProvider;
    
    if (command instanceof CustomMenuCommand)
    {
      // delegate to the CustomMenuProvider
      menuProvider =
        ((CustomMenuCommand)command).createMenuProvider (commandView, menu, menuIndex);
      
    } else
    {
      menuProvider = new CommandMenuButtonProvider (commandView, command, menu, menuIndex);
    }

    nodeToItemProvider.put (commandNode, menuProvider);

    return menuProvider.getItemCount ();
  }

  /**
   * Destroy the menu items created for a command node.
   *
   * @param node The command node.
   */
  private void destroyMenuItems (CommandViewNode node)
  {
    CommandMenuItemProvider menuProvider =
      (CommandMenuItemProvider)nodeToItemProvider.remove (node);
    
    menuProvider.dispose ();
  }
  
  /**
   * Canonically calculate the correct index on the menu for a first
   * level (group) or second level (command or pullright) child of
   * root.
   *
   * @param node The node to find index for.
   * @return The menu index for node.
   */
  private int findMenuIndex (CommandViewNode node)
  {
    int index = 0;

    // scan groups
    for (int i1 = 0; i1 < root.getChildCount (); i1++)
    {
      CommandViewNode group = root.getChild (i1);

      if (group == node)
        return index;

      // increment index for separator
      if (i1 > 0)
        index += 1;

      // scan group children
      for (int i2 = 0; i2 < group.getChildCount (); i2++)
      {
        CommandViewNode groupChild = group.getChild (i2);

        if (node == groupChild)
          return index;

        index += getMenuItemCount (groupChild);
      }
    }

    return index;
  }

  /**
   * Get the number of menu items associated with command node.
   */
  private int getMenuItemCount (CommandViewNode node)
  {
    Object item = nodeToItemProvider.get (node);

    if (item instanceof CommandMenuItemProvider)
      return ((CommandMenuItemProvider)item).getItemCount ();
    else
      return 1;
  }

  // Listener interface
  
  public void handleEvent (Event e)
  {
    if (e.widget == menu)
    {
      dispose ();
    } else if (e.type == SWT.MouseDoubleClick)
    {
      Command defaultCommand = commandView.getDefaultCommand ();
      
      if (defaultCommand != null && defaultCommand.isEnabled ())
        defaultCommand.execute ();
    }
  }
  
  // CommandViewNodeListener interface

  public void childAdded (CommandViewNodeEvent e)
  {
    CommandViewNode node = e.getNode ();

    // check that a command hasn't popped up outside a group node
    Debug.assertTrue (node.getCommand () == null ||
                      node.getParent () != root,
                      "Command \"" + node + "\" found in group context",
                      this);

    if (node.getParent () == root)
      addGroup (node, findMenuIndex (node));
    else if (node.getCommand () != null)
      addCommand (node, findMenuIndex (node));
    else
      addPullRight (node, findMenuIndex (node));
  }

  public void childRemoved (CommandViewNodeEvent e)
  {
    CommandViewNode node = e.getNode ();

    if (node.getParent () == root)
      removeGroup (node, e.getOldIndex ());
    else if (node.getCommand () != null)
      removeCommand (node);
    else
      removePullRight (node);
  }

  public void childMoved (CommandViewNodeEvent e)
  {
    CommandViewNode node = e.getNode ();

    if (node.getParent () == root)
      moveGroup (node, e.getOldIndex (), e.getNewIndex ());
    else
      moveGroupChild (e.getNode (), e.getNewIndex ());
  }

  public void commandChanged (CommandViewNodeEvent e)
  {
    CommandViewNode node = e.getSourceNode ();
    CommandMenuItemProvider menuProvider =
      (CommandMenuItemProvider)nodeToItemProvider.get (node);
    int menuIndex = menuProvider.getItemStart ();

    destroyMenuItems (node);
    createMenuItems (node, menuIndex);
  }
}
