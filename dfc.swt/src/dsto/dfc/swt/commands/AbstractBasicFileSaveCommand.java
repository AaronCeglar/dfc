package dsto.dfc.swt.commands;

import static dsto.dfc.swt.commands.CommandRegistry.getShortName;
import static org.eclipse.swt.SWT.ICON_ERROR;
import static org.eclipse.swt.SWT.OK;
import static org.eclipse.swt.SWT.SAVE;

import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

/**
 * Abstract base class for File "Save" and "Save As..." commands. Places
 * commands in the "oc" group, and "File.oc" main menu group with 's' for a
 * mnemonic.
 */
public abstract class AbstractBasicFileSaveCommand extends AbstractCommand
{
  protected Control owner;

  public AbstractBasicFileSaveCommand (Control owner, String name,
                                       String description, boolean interactive,
                                       int accelerator)
  {
    this (owner, name, (String) null, description, interactive, accelerator);
  }

  public AbstractBasicFileSaveCommand (Control owner, String name,
                                       String iconName, String description,
                                       boolean interactive, int accelerator)
  {
    super (name, iconName, "oc", description, interactive, 's', accelerator);

    this.owner = owner;

    setMainMenuGroup ("File.oc");
  }

  /**
   * Provides a basic implementation: pops up a file chooser and calls either
   * saveFile () or dialogCancelled ().
   *
   * @see #initFileChooser
   * @see #saveFile
   * @see #dialogCancelled
   */
  @Override
  public void execute ()
  {
    FileDialog chooser = new FileDialog (getShell (), SAVE);
    chooser.setText (getShortName (this));

    initFileChooser (chooser);

    String result = chooser.open ();

    if (result != null)
      saveFile (chooser);
    else
      dialogCancelled (chooser);
  }

  /**
   * Superclasses may override this to setup the file chooser before display.
   */
  protected void initFileChooser (FileDialog chooser)
  {
    // zip
  }

  /**
   * Superclasses may override this to save to a file selected via the chooser.
   */
  protected void saveFile (FileDialog chooser)
  {
    // zip
  }

  /**
   * Superclasses may override this to handle the case where the user cancels
   * the dialog.
   */
  protected void dialogCancelled (FileDialog chooser)
  {
    // zip
  }

  protected void handleError ()
  {
    handleError (null);
  }

  protected void handleError (Throwable exception)
  {
    String message;

    if (exception != null)
      message = "Error while saving file: " + exception;
    else
      message = "Error saving file";

    MessageBox dialog = new MessageBox (getShell (), ICON_ERROR | OK);
    dialog.setText (getShortName (this));
    dialog.setMessage (message);
    dialog.open ();
  }

  protected Shell getShell ()
  {
    return (owner != null) ? owner.getShell () : null;
  }
}
