package dsto.dfc.swt.commands;

import java.util.EventListener;

/**
 * Defines the events fired by
 * {@link dsto.dfc.swt.commands.Command}'s.
 * 
 * @author mpp
 * @version $Revision$
 */
public interface CommandListener extends EventListener
{
  /**
   * Signals that a command has been executed.
   */
  public void commandExecuted (CommandEvent e);
}
