package dsto.dfc.swt.commands;

/**
 * Interface for providers of toolbar command representations (eg buttons,
 * checkboxes, etc). The constructor of the provider creates toolbar items
 * for a single command on a toolbar.
 * 
 * @author mpp
 * @version $Revision$
 */
public interface CommandToolbarItemProvider
{
  /**
   * The number of items the provider creates on the toolbar.
   */
  public int getItemCount ();

  /**
   * Dispose all items created on the toolbar.
   */
  public void dispose ();
}
