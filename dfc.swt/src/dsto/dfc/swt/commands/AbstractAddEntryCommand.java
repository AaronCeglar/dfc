package dsto.dfc.swt.commands;


/**
 * Base class for commands that add a new entry to a control.
 *
 * @version $Revision$
 */
public abstract class AbstractAddEntryCommand extends AbstractCommand
{
  public AbstractAddEntryCommand ()
  {
    super ("edit.Add Entry", "plus.gif", "edit",
           "Add a new entry", false, 'a', 0);
           
    setMainMenuGroup ("Edit.change");
  }
}
