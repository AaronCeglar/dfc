package dsto.dfc.swt.commands;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import dsto.dfc.util.BasicPropertyEventSource;
import dsto.dfc.util.Debug;

/**
 * A list of {@link Command}'s and sub-CommandView's intended to
 * define the commands presented in a given UI context, such as a main
 * menu or a toolbar.  See {@link dsto.dfc.swt.commands.CommandViewMenuProvider}
 * and {@link dsto.dfc.swt.commands.CommandViewToolBarProvider} for examples of
 * how to present command views in a UI.<p>
 * 
 * The internal implementation manages a tree of {@link CommandViewNode}'s
 * representing the merged commands both from this view and the commands from
 * child command views.<p>
 *
 * @version $Revision$
 *
 * @see Command
 * @see Commands
 */
public class CommandView
  extends BasicPropertyEventSource implements Serializable
{
  public static final String MAIN_MENU_VIEW = "main_menu";
  public static final String CONTEXT_MENU_VIEW = "context_menu";
  public static final String TOOLBAR_VIEW = "toolbar";

  private String viewName;
  private CommandViewNode treeRoot;
  private ArrayList entries = new ArrayList ();
  /** Map command and group entries to their 'vine' root nodes. */
  private HashMap mapEntryToVine = new HashMap ();
  private Command defaultCommand;
  private Map commandProperties;
  private Listener listener;

  /**
   * Copy constructor.
   */
  public CommandView (CommandView view)
  {
    this (view.getViewName ());

    for (Iterator i = view.entries.iterator (); i.hasNext ();)
    {
      Object entry = i.next ();
      
      if (entry instanceof String)
        addGroup ((String)entry);
      else if (entry instanceof Command)
        addCommand((Command)entry);
      else
        addView ((CommandView)entry);
    }
  }
  
  /**
   * Create a new command view.
   *
   * @param viewName The view name (usually one of MAIN_MENU_VIEW,
   * CONTEXT_MENU_VIEW or TOOLBAR_VIEW).
   */
  public CommandView (String viewName)
  {
    this.viewName = viewName;
    this.treeRoot = new CommandViewNode ();
    this.commandProperties = Collections.EMPTY_MAP;

    listener = new Listener ();
  }

  public String getViewName ()
  {
    return viewName;
  }

  public Command getDefaultCommand ()
  {
    return defaultCommand;
  }

  /**
   * The default command for this view.  Some UI controls, such as
   * context menus, will execute this command by default, when the user
   * executes an action such as double-clicking an item.
   */
  public void setDefaultCommand (Command newValue)
  {
    Command oldValue = defaultCommand;

    defaultCommand = newValue;

    firePropertyChange ("defaultCommand", oldValue, newValue);
  }

  /**
   * Set a client property for a command.
   *
   * @param commandName The command's name (eg as obtained by getName ()).
   * @param key The property key.
   * @param value The property value.
   *
   * @see #getCommandProperty(String,Object,Object)
   */
  public void setCommandProperty (String commandName, Object key, Object value)
  {
    Map properties = (Map)commandProperties.get (commandName);

    if (properties == null)
    {
      if (commandProperties == Collections.EMPTY_MAP)
        commandProperties = new HashMap ();

      properties = new HashMap ();

      commandProperties.put (commandName, properties);
    }

    properties.put (key, value);
  }

  /**
   * Get a boolean client property for a command.
   *
   * @param commandName The command's name (eg as obtained by getName ()).
   * @param key The property key.
   * @param defaultValue The value to return if property is not set.
   * @return The property value, or null if not found.  If the property is not
   * set in this view sub views are also searched.
   *
   * @exception ClassCastException if the property exists but is not boolean.
   *
   * @see #setCommandProperty
   */
  public boolean getCommandProperty (String commandName, Object key,
                                     boolean defaultValue)
  {
    Object value = getCommandProperty (commandName, key);

    if (value == null)
      return defaultValue;
    else
      return ((Boolean)value).booleanValue ();
  }

  /**
   * Get a client property for a command.
   *
   * @param commandName The command's name (eg as obtained by getName ()).
   * @param key The property key.
   * @param defaultValue The value to return if property is not set.
   * @return The property value, or null if not found.  If the property is not
   * set in this view sub views are also searched.
   *
   * @see #setCommandProperty
   */
  public Object getCommandProperty (String commandName, Object key,
                                    Object defaultValue)
  {
    Object value = getCommandProperty (commandName, key);

    if (value == null)
      return defaultValue;
    else
      return value;
  }

  /**
   * Get a client property for a command.
   *
   * @param commandName The command's name (eg as obtained by getName ()).
   * @param key The property key.
   * @return The property value, or null if not found.  If the property is not
   * set in this view sub views are also searched.
   *
   * @see #setCommandProperty
   */
  public Object getCommandProperty (String commandName, Object key)
  {
    Map properties = (Map)commandProperties.get (commandName);
    Object property = null;

    if (properties != null)
      property = properties.get (key);

    // if property is null, search sub views
    for (int i = 0; i < entries.size () && property == null; i++)
    {
      Object entry = entries.get (i);

      if (entry instanceof CommandView)
        property = ((CommandView)entry).getCommandProperty (commandName, key);
    }

    return property;
  }

  /**
   * Remove all command client properties.
   *
   * @see #setCommandProperty
   */
  public void clearCommandProperties ()
  {
    commandProperties = Collections.EMPTY_MAP;
  }

  /**
   * Find the first view in a breadth-first search of subviews that
   * contains a command with a matching name.
   *
   * @param commandName The name of the command.
   * @return The command view that the command was found in, or null
   * if not found.
   * 
   * @see #findViewForCommand(Class)
   */
  public CommandView findViewForCommand (String commandName)
  {
    if (getCommand (commandName) != null)
    {
      return this;
    } else
    {
      for (int i = 0; i < entries.size (); i++)
      {
        Object entry = entries.get (i);

        if (entry instanceof CommandView)
        {
          CommandView view =
            ((CommandView)entry).findViewForCommand (commandName);

          if (view != null)
            return view;
        }
      }
    }

    return null;
  }

  /**
   * Find the first instance of a command with a matching name in a
   * breadth-first search of subviews.
   * 
   * @see #findCommand(Class)
   */
  public Command findCommand (String commandName)
  {
    CommandView view = findViewForCommand (commandName);

    if (view != null)
      return view.getCommand (commandName);
    else
      return null;
  }

  /**
   * Find the first view in a breadth-first search of subviews that
   * contains a command of the given class.
   *
   * @param commandClass The type of the command.
   * @return The command view that the command was found in, or null
   * if not found.
   * 
   * @see #findViewForCommand(String)
   */
  public CommandView findViewForCommand (Class commandClass)
  {
    if (getCommand (commandClass) != null)
    {
      return this;
    } else
    {
      for (int i = 0; i < entries.size (); i++)
      {
        Object entry = entries.get (i);

        if (entry instanceof CommandView)
        {
          CommandView view =
            ((CommandView)entry).findViewForCommand (commandClass);

          if (view != null)
            return view;
        }
      }
    }

    return null;
  }

  /**
   * Find the first instance of a command with a matching class in a
   * breadth-first search of subviews.
   * 
   * @see #findCommand(String)
   */
  public Command findCommand (Class commandClass)
  {
    CommandView view = findViewForCommand (commandClass);

    if (view != null)
      return view.getCommand (commandClass);
    else
      return null;
  }
  
  /**
   * Get a snapshot collection of all commands both in this view and in sub-views.
   */
  public Collection getAllCommands ()
  {
    ArrayList commands = new ArrayList ();
    
    getAllCommands (treeRoot, commands);
    
    return commands;
  }
  
  private static void getAllCommands (CommandViewNode node, ArrayList commands)
  {
    if (node.getCommand () != null)
      commands.add (node.getCommand ());
    
    for (Iterator i = node.childIterator (); i.hasNext ();)
    {
      getAllCommands ((CommandViewNode)i.next (), commands);
    }
  }

  /**
   * Logically activate this view, bringing any of its overloaded
   * commands to the 'top', making them active. 
   */
  public void activate ()
  {
    // fire bogus property change event
    firePropertyChange ("active", false, true);
  }

  /**
   * The root of the merged command tree.
   */
  public CommandViewNode getTreeRoot ()
  {
    return treeRoot;
  }

  public boolean containsView (CommandView subView)
  {
    return entries.contains (subView);
  }

  public boolean containsCommand (Command command)
  {
    return entries.contains (command);
  }
  
  public boolean containsCommand (String name)
  {
    for (Iterator i = entries.iterator (); i.hasNext ();)
    {
      Object item = i.next ();
      
      if (item instanceof Command && ((Command)item).getName ().equals (name))
        return true;
    }
    
    return false;
  }

  public boolean containsGroup (String group)
  {
    return entries.contains (group);
  }

  /**
   * Add a child command view to this view.
   */
  public void addView (CommandView view)
  {
    Debug.assertTrue (view.getViewName ().equals (getViewName ()),
                      "Attempt to add view type " + view.getViewName () +
                      " to " + getViewName (), this);

    addView (view, entries.size ());
  }

  /**
   * Add a child command view with a specific index.
   */
  public void addView (CommandView view, int index)
  {
    if (!containsView (view))
    {
      if (index < entries.size ())
        shiftMajorIndexes (treeRoot, index, 1);

      // add view to sub-views list
      entries.add (index, view);

      // add listener to sub-view's command tree
      view.addPropertyChangeListener (listener);

      // merge view subtrees
      CommandViewNode subtreeRoot = view.getTreeRoot ();
      subtreeRoot.addCommandViewNodeListener (listener);  // merge listens to children

      for (int i = 0; i < subtreeRoot.getChildCount (); i++)
        merge (treeRoot, subtreeRoot.getChild (i), index);
    }
  }

  /**
   * Remove a child command view.
   */
  public void removeView (CommandView view)
  {
    int index = entries.indexOf (view);

    if (index != -1)
    {
      // remove listener from sub-view's command tree
      view.removePropertyChangeListener (listener);

      CommandViewNode subtreeRoot = view.getTreeRoot ();
      subtreeRoot.removeCommandViewNodeListener (listener);  // unmerge removes child listeners

      // remove commands from tree
      for (int i = 0; i < subtreeRoot.getChildCount (); i++)
        unmerge (treeRoot, subtreeRoot.getChild (i));

      // remove tree from entries
      entries.remove (view);

      if (index < entries.size ())
        shiftMajorIndexes (treeRoot, index, -1);

      /** @todo remove any command view properties */
    }
  }

  /**
   * Add a command to the end of the view.
   * 
   * @see #addCommand(Command, int)
   */
  public void addCommand (Command command)
  {
    addCommand (command, entries.size ());
  }

  /**
   * Add a collection of commands.
   * 
   * @see #addCommand(Command, int)
   */
  public void addCommands (Collection commands)
  {
    for (Iterator i = commands.iterator (); i.hasNext (); )
      addCommand ((Command)i.next ());
  }

  public void removeCommands (Collection commands)
  {
    for (Iterator i = commands.iterator (); i.hasNext (); )
      removeCommand ((Command)i.next ());
  }

  /**
   * Remove all commands and command views from this view.
   */
  public void removeAll ()
  {
    for (int i = entries.size () - 1; i >= 0; i--)
      removeEntry (entries.get (i), i);
  }

  /**
   * Add a command to the view at a specific index. Adding a second command
   * with the same logical name causes an error.
   */
  public void addCommand (Command command, int index)
  {
    if (!containsCommand (command.getName ()))
    {
      // replace any previous command with the same name. duplicates cause
      // problems when they're removed
      removeCommand (command.getName ());
      
      command.addPropertyChangeListener (listener);

      // add to command tree
      String group = CommandRegistry.getGroupInView (viewName, command);
      CommandViewNode vine = group == null ? null : makeVine (group, command);

      addEntry (command, vine, index);
    } else
    {
      throw new IllegalArgumentException
        ("Command with name \"" + command.getName () + "\" already in view");
    }
  }

  public void removeCommand (Command command)
  {
    command.removePropertyChangeListener (listener);

    removeEntry (command);
  }

  public Command removeCommand (String commandName)
  {
    Command command = getCommand (commandName);

    if (command != null)
      removeCommand (command);

    return command;
  }

  /**
   * @see #addGroup(String,int)
   */
  public void addGroup (String group)
  {
    addGroup (group, entries.size ());
  }

  /**
   * Add an empty command group to the view.  This can be used to
   * impose command orderings other than the default.  For example, to
   * force all commands in the 'file.cut_and_paste' group to occur
   * first in the command view, the 'file.cut_and_paste' group could
   * be added at index 0.
   */
  public void addGroup (String group, int index)
  {
    addEntry (group, makeVine (group), index);
  }

  public void removeGroup (String group)
  {
    removeEntry (group);
  }

  public Command getCommand (String commandName)
  {
    for (int i = 0; i < entries.size (); i++)
    {
      if (entries.get (i) instanceof Command &&
          ((Command)entries.get (i)).getName ().equals (commandName))
        return (Command)entries.get (i);
    }

    return null;
  }

  public void removeCommand (Class commandClass)
  {
    Command command = getCommand (commandClass);

    if (command != null)
      removeCommand (command);
  }

  public Command getCommand (Class commandClass)
  {
    for (int i = 0; i < entries.size (); i++)
    {
      if (commandClass.isAssignableFrom (entries.get (i).getClass ()))
        return (Command)entries.get (i);
    }

    return null;
  }

  /**
   * The number of entries (views and commands in this view).
   */
  public int getEntryCount ()
  {
    return entries.size ();
  }

  /**
   * Get the command at a given ordinal.
   *
   * @exception IndexOutOfBoundsException if index is not valid.
   * @exception ClassCastException if entry at index is not a command.
   */
  public Command getCommand (int index)
    throws IndexOutOfBoundsException, ClassCastException
  {
    return (Command)entries.get (index);
  }

  /**
   * Get the command at a given ordinal.
   *
   * @exception IndexOutOfBoundsException if index is not valid.
   * @exception ClassCastException if entry at index is not a command.
   */
  public CommandView getView (int index)
    throws IndexOutOfBoundsException, ClassCastException
  {
    return (CommandView)entries.get (index);
  }

  /**
   * Get the the entry at a given index in the view.
   *
   * @param index The index of the entry.
   * @return The entry (either a Command or a CommandView).
   */
  public Object getEntry (int index)
  {
    return entries.get (index);
  }

  /**
   * Get the index associated with a sub view a CommandView.
   */
  public int getIndex (CommandView view)
  {
    return entries.indexOf (view);
  }

  public int getIndex (Command command)
  {
    return entries.indexOf (command);
  }

  /**
   * Get the index associated with a node of a subtree contained in
   * this view.
   *
   * @param node A node from a direct subtree of this view.
   * @return The index of the entry (command or view) that it
   * corresponds to.
   */
  protected int getMajorIndex (CommandViewNode node)
  {
    while (node.getParent () != null)
      node = node.getParent ();

    for (int i = 0; i < entries.size (); i++)
    {
      Object entry = entries.get (i);
      CommandViewNode n; // n == node corresponding to entry

      if (entry instanceof CommandView)
      {
        // n is root of command view's tree
        n = ((CommandView)entry).getTreeRoot ();
      } else
      {
        // either a command or a group (String): lookup its vine
        n = (CommandViewNode)mapEntryToVine.get (entry);
      }

      if (n == node)
        return i;
    }

    return -1;
  }

  /**
   * Shift all the major indicies of nodes of a tree by a given
   * amount.
   *
   * @param node The root of the tree.
   * @param startIndex The lower bound of indexes to change.
   * @param delta The amount to shift the index by.
   * @see CommandViewNode#shiftMajorIndexes
   */
  protected void shiftMajorIndexes (CommandViewNode node,
                                    int startIndex, int delta)
  {
    node.shiftMajorIndexes (startIndex, delta);

    for (int i = 0; i < node.getChildCount (); i++)
      shiftMajorIndexes (node.getChild (i), startIndex, delta);
  }

  /**
   * Make a vine (tree where all nodes have at most one child) from a
   * command.
   *
   * @param group The group to place the command in.
   * @param command The command.
   * @return The root of the new vine.
   */
  protected CommandViewNode makeVine (String group, Command command)
  {
    String [] groupBits = CommandRegistry.splitGroup (group);

    CommandViewNode node = null;

    for (int i = 0; i < groupBits.length; i++)
    {
      node = new CommandViewNode (node, groupBits [i]);
      node.connectParent ();
    }

    node = new CommandViewNode (node, command);
    node.connectParent ();

    return node.findRoot ();
  }

  protected CommandViewNode makeVine (String group)
  {
    String [] groupBits = CommandRegistry.splitGroup (group);

    CommandViewNode node = null;

    for (int i = groupBits.length - 1; i >= 0; i--)
    {
      node = new CommandViewNode (node, groupBits [i]);
      node.connectParent ();
    }

    return node;
  }

  /**
   */
  protected void addEntry (Object entry,
                           CommandViewNode rootNode, int index)
  {
    if (!entries.contains (entry))
    {
      if (index < entries.size ())
        shiftMajorIndexes (treeRoot, index, 1);

      // add entry to list
      entries.add (index, entry);

      if (rootNode != null)
      {
        mapEntryToVine.put (entry, rootNode);

        // merge subtree
        merge (treeRoot, rootNode, index);
      }
    }
  }

  protected void removeEntry (Object entry)
  {
    int index = entries.indexOf (entry);

    if (index != -1)
      removeEntry (entry, index);
  }

  protected void removeEntry (Object entry, int index)
  {
    CommandViewNode vine =
      (CommandViewNode)mapEntryToVine.remove (entry);

    if (vine != null)
      unmerge (treeRoot, vine);

    entries.remove (index);

    if (index < entries.size ())
      shiftMajorIndexes (treeRoot, index, -1);
  }

  /**
   * Merge (mount) a subtree node and all its children onto a node of
   * this tree.
   *
   * @param parent The parent of the new merged node.
   * @param subtreeNode The subtree node to merge into a child of
   * parent.
   * @param majorIndex The major index to associate with the merge.
   * @see #unmerge
   * @see CommandViewNode#mergeChild
   */
  protected void merge (CommandViewNode parent,
                        CommandViewNode subtreeNode, int majorIndex)
  {
    subtreeNode.addCommandViewNodeListener (listener);

    int minorIndex = subtreeNode.getIndex ();

    // first item in command vine is root and has no index
    if (minorIndex == -1)
      minorIndex = 0;

    CommandViewNode node =
      parent.mergeChild (subtreeNode, majorIndex, minorIndex);

    for (int i = 0; i < subtreeNode.getChildCount (); i++)
      merge (node, subtreeNode.getChild (i), majorIndex);
  }

  /**
   * Reverse the effect of merge ().
   *
   * @param parent The parent of the merged node.
   * @param subtreeNode The subtree node that should be unmerged.
   */
  protected void unmerge (CommandViewNode parent,
                          CommandViewNode subtreeNode)
  {
    subtreeNode.removeCommandViewNodeListener (listener);

    CommandViewNode node = parent.findChild (subtreeNode.getName ());

    // IMPORTANT: children first
    for (int i = 0; i < subtreeNode.getChildCount (); i++)
      unmerge (node, subtreeNode.getChild (i));

    parent.unmergeChild (subtreeNode);
  }

  /**
   * Remerge a subtree node and all its children with a changed minor
   * index.  May be used when the subtreeNode has changed position in
   * its parent.
   *
   * @param parent The parent of the merged node.
   * @param subtreeNode The subtree node.
   * @see #merge
   */
  protected void remergeMinorIndex (CommandViewNode parent,
                                    CommandViewNode subtreeNode)
  {
    CommandViewNode subNode =
      parent.remergeMinorIndex (subtreeNode, subtreeNode.getIndex ());

    for (int i = 0; i < subtreeNode.getChildCount (); i++)
      remergeMinorIndex (subNode, subtreeNode.getChild (i));
  }

  /**
   * Updates command tree when a command changes its group.
   */
  protected void updateCommandGroup (Command command)
  {
    CommandViewNode node = (CommandViewNode)mapEntryToVine.get (command);
    int index = getIndex (command);

    // if command was in tree, remove
    if (node != null)
      unmerge (treeRoot, node);

    mapEntryToVine.remove (command);

    String group = CommandRegistry.getGroupInView (viewName, command);

    if (group != null)
    {
      CommandViewNode vine = makeVine (group, command);
      mapEntryToVine.put (command, vine);

      merge (treeRoot, vine, index);
    }
  }

  protected void activateSubtree (CommandViewNode root, CommandViewNode subRoot)
  {
    if (subRoot.getCommand () != null)
      root.setCommand (subRoot.getCommand ());

    for (int i = 0; i < subRoot.getChildCount (); i++)
    {
      CommandViewNode subRootChild = subRoot.getChild (i);
      CommandViewNode rootChild = root.findChild (subRootChild.getName ());

      activateSubtree (rootChild, subRootChild);
    }
  }

  protected void commandPropertyChanged (PropertyChangeEvent e)
  {
    if (e.getPropertyName ().equals (viewName))
    {
      Command command = (Command)e.getSource ();

      updateCommandGroup (command);
    }
  }

  protected void viewPropertyChanged (PropertyChangeEvent e)
  {
    if (e.getPropertyName ().equals ("active"))
      activateSubtree (treeRoot, ((CommandView)e.getSource ()).getTreeRoot ());
  }

  // command tree change handlers

  protected void subtreeChildAdded (CommandViewNodeEvent e)
  {
    CommandViewNode subtreeNode = e.getNode ();
    CommandViewNode parent = treeRoot.followNamePath (subtreeNode.getParent ());
    int majorIndex = getMajorIndex (e.getSourceNode ());

    // Debug.assert (parent != null, "Failed to resolve parent node for " +
    //                                subtreeNode.getName (), this);
    // Debug.assert (majorIndex != -1, "Source node " +
    //                                 e.getSourceNode ().getName () +
    //                                 " not in view when adding node " +
    //                                 e.getNode ().getName (), this);

    merge (parent, subtreeNode, majorIndex);
  }

  protected void subtreeChildRemoved (CommandViewNodeEvent e)
  {
    CommandViewNode subtreeNode = e.getNode ();
    CommandViewNode parent = treeRoot.followNamePath (subtreeNode.getParent ());

    // Debug.assert (parent != null, "failed to resolve parent node for " +
    //               subtreeNode.getName (), this);

    unmerge (parent, subtreeNode);
  }

  protected void subtreeChildMoved (CommandViewNodeEvent e)
  {
    CommandViewNode subtreeNode = e.getNode ();
    CommandViewNode parent = treeRoot.followNamePath (subtreeNode.getParent ());

    // Debug.assert (parent != null, "failed to resolve parent node for " +
    //               subtreeNode.getName (), this);

    remergeMinorIndex (parent, subtreeNode);
  }

  protected void subtreeCommandChanged (CommandViewNodeEvent e)
  {
    CommandViewNode subtreeNode = e.getSourceNode ();
    CommandViewNode node = treeRoot.followNamePath (subtreeNode);

    // Debug.assert (node != null, "failed to resolve node for " +
    //               subtreeNode.getName (), this);

    node.setCommand (e.getNewCommand ());
  }

  /**
   * Listener for subtree structure changes and command property changes.
   */
  class Listener
    implements PropertyChangeListener, CommandViewNodeListener
  {
    public void propertyChange (PropertyChangeEvent e)
    {
      if (e.getSource () instanceof Command)
        commandPropertyChanged (e);
      else
        viewPropertyChanged (e);
    }

    public void childAdded (CommandViewNodeEvent e)
    {
      subtreeChildAdded (e);
    }

    public void childRemoved (CommandViewNodeEvent e)
    {
      subtreeChildRemoved (e);
    }

    public void childMoved (CommandViewNodeEvent e)
    {
      subtreeChildMoved (e);
    }

    public void commandChanged (CommandViewNodeEvent e)
    {
      subtreeCommandChanged (e);
    }
  }
}
