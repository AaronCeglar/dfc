package dsto.dfc.swt.commands;

import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.CoolItem;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import dsto.dfc.util.Debug;

import dsto.dfc.swt.controls.MenuToolItem;

/**
 * Synchronizes a command view with a SWT CoolBar.
 */
public final class CommandViewToolBarProvider
  implements CommandViewNodeListener, DisposeListener
{
  private CoolBar coolbar;
  private CommandView commandView;
  private CommandViewNode root;
  private HashMap nodeToProvider;

  public CommandViewToolBarProvider (CoolBar coolbar,
                                     CommandView commandView)
  {
    this (coolbar, commandView, commandView.getTreeRoot ());
  }
  
  /**
   * Create a new instance, adding all the entries, and setting up
   * all the listeners.
   *
   * @param coolbar The JToolBar that you want syncronized.
   * @param commandView The CommandView that you want synchronized.
   * @param root The root CommandViewNode that you want to start from.
   */
  public CommandViewToolBarProvider (CoolBar coolbar,
                                     CommandView commandView,
                                     CommandViewNode root)
  {
    this.coolbar = coolbar;
    this.commandView = commandView;
    this.root = root;
    this.nodeToProvider = new HashMap ();

    addEntries ();

    coolbar.addDisposeListener (this);
    root.addCommandViewNodeListener (this);
  }

  /**
   * Remove all listeners, and then remove all the commands and
   * command groups.
   */
  public void dispose ()
  {
    root.removeCommandViewNodeListener (this);

    for (int i = 0; i < root.getChildCount (); i++)
    {
      CommandViewNode node = root.getChild (i);

      if (node.getCommand () != null)
        removeCommand (node);
      else
        removeGroup (node, i);
    }
  }

  private void addEntries ()
  {
    int index = 0;

    for (int i = 0; i < root.getChildCount (); i++)
      index += addGroup (root.getChild (i), index);
  }

  private int addGroup (CommandViewNode groupNode, int toolBarIndex)
  {
    groupNode.addCommandViewNodeListener (this);

    CoolItem groupItem = new CoolItem (coolbar, SWT.DEFAULT, toolBarIndex);
    nodeToProvider.put (groupNode, groupItem);
    
    ToolBar toolbar =
      new ToolBar (coolbar, SWT.FLAT | SWT.RIGHT | SWT.HORIZONTAL | SWT.WRAP);
    
    toolbar.setData ("provider", this);
    toolbar.setData ("coolitem", groupItem);
    
    groupItem.setControl (toolbar);

    for (int i = 0; i < groupNode.getChildCount (); i++)
    {
      CommandViewNode childNode = groupNode.getChild (i);

      if (childNode.getCommand () != null)
        addCommand (childNode, i);
      else
        addDropdown (childNode, i);
    }
    
    return 1;
  }

  private void addCommand (CommandViewNode node, int index)
  {
    createToolBarItems (node, index);

    node.addCommandViewNodeListener (this);
  }
  
  private void addDropdown (CommandViewNode node, int index)
  {
    createDropdown (node, index);
  }

  private void createDropdown (CommandViewNode node, int index)
  {
    ToolBar toolbar = getToolbar (node);
    Menu menu = new Menu (toolbar);
    new CommandViewMenuProvider (menu, null, commandView, node);
    
    MenuToolItem item = new MenuToolItem (toolbar, menu, index);
    
    // if no icon found (ie no default menu item), use node name as text 
    ToolItem toolItem = item.getToolItem ();
    
    if (toolItem.getImage () == null)
      toolItem.setText (node.getName ());
    
    nodeToProvider.put (node, item);
    
    updateCoolItemSize (node.getParent ());
  }

  private void updateCoolItemSize (CommandViewNode group)
  {
    updateCoolItemSize ((CoolItem)nodeToProvider.get (group));
  }
  
  private void updateCoolItemSize (CoolItem item)
  {
    if (!item.isDisposed ())
    {
      ToolBar toolBar = (ToolBar)item.getControl ();
      
      Point size = toolBar.computeSize (SWT.DEFAULT, SWT.DEFAULT);
      toolBar.setSize (size);

      // On Windows, CoolItem.computeSize () seems to report too wide a size.
      // On non-Windows, not doing this results in the item being too narrow.
      if (!SWT.getPlatform ().equals ("win32"))
        size = item.computeSize (size.x, size.y);
      
      item.setMinimumSize (size);
      item.setPreferredSize (size);
      item.setSize (size);
    }
  }

  private ToolBar getToolbar (CommandViewNode node)
  {
    CoolItem item = (CoolItem)nodeToProvider.get (node.getParent ());

    return (ToolBar)item.getControl ();
  }
  
  private int createToolBarItems (CommandViewNode commandNode, int index)
  {
    ToolBar toolbar = getToolbar (commandNode);
    Command command = commandNode.getCommand ();
    CommandToolbarItemProvider toolbarProvider;
    
    /** @todo add support for using custom controls */
    if (command instanceof CustomToolbarCommand)
    {
      // delegate to the CustomToolbarCommand
      toolbarProvider =
        ((CustomToolbarCommand)command).createToolbarProvider
          (commandView, toolbar, index);
      
    } else
    {
      toolbarProvider =
        new CommandToolbarButtonProvider (commandView, command, toolbar, index);
    }

    nodeToProvider.put (commandNode, toolbarProvider);

    updateCoolItemSize (commandNode.getParent ());
    
    return toolbarProvider.getItemCount ();
  }

  /**
   * Remove a command from the menu.
   *
   * @param node The command node to remove.
   */
  private void removeCommand (CommandViewNode node)
  {
    CommandToolbarItemProvider provider =
      (CommandToolbarItemProvider)nodeToProvider.remove (node);
    
    provider.dispose ();
    
    node.removeCommandViewNodeListener (this);
    
    updateCoolItemSize (node.getParent ());
  }

  private void removeGroup (CommandViewNode groupNode, int groupIndex)
  {
    groupNode.removeCommandViewNodeListener (this);  
    
    for (int i = 0; i < groupNode.getChildCount (); i++)
    {
      CommandViewNode childNode = groupNode.getChild (i);

      if (childNode.getCommand () != null)
        removeCommand (childNode);
      else
        removeDropdown (childNode);
    }

    CoolItem item = (CoolItem)nodeToProvider.remove (groupNode);    
    
    if (!item.isDisposed ())
    {
      item.getControl ().dispose ();
      item.dispose ();
    }
  }

  /**
   * Remove a CommandMenuButton.
   *
   * @param node The node representing a command menu button.
   */
  private void removeDropdown (CommandViewNode node)
  {
    MenuToolItem item = (MenuToolItem)nodeToProvider.remove (node);

    item.dispose ();    
    
    updateCoolItemSize (node.getParent ());
  }

  /**
   * Canonically calculate the correct index on the toolbar for a first
   * level (group) or second level (command or subToolBar) child of
   * root.
   *
   * @param node The node to find index for.
   * @return The coolbar index for node.
   */
  private int findToolBarIndex (CommandViewNode node)
  {
    for (int i1 = 0; i1 < root.getChildCount (); i1++)
    {
      CommandViewNode group = root.getChild (i1);

      if (node == group)
        return i1;
      
      int index2 = 0;
      
      // scan group children
      for (int i2 = 0; i2 < group.getChildCount (); i2++)
      {
        CommandViewNode groupChild = group.getChild (i2);

        if (groupChild == node)
          return index2;

        index2 += getToolbarItemCount (groupChild);
      }
    }

    return root.getChildCount ();
  }

  private int getToolbarItemCount (CommandViewNode node)
  {
    Object item = nodeToProvider.get (node);

    if (item instanceof CommandToolbarItemProvider)
      return ((CommandToolbarItemProvider)item).getItemCount ();
    else
      return 1;
  }

  /**
   * Move a group of buttons to match a group node move.  Assumes
   * group node has already moved.
   *
   * @param groupNode The group node that moved.
   * @param oldIndex The old index of the group node.
   * @param newIndex The new index of the group node.
   */
  private void moveGroup (CommandViewNode groupNode,
                          int oldIndex, int newIndex)
  {
    removeGroup (groupNode, oldIndex);
    addGroup (groupNode, newIndex);
  }

  /**
   * Move buttons to match a group child (command or subToolBar)
   * move.  Assumes node has already moved.
   *
   * @param childNode The node that moved.
   */
  private void moveGroupChild (CommandViewNode childNode,
                               int newIndex)
  {
    if (childNode.getCommand () != null)
    {
      removeCommand (childNode);
      addCommand (childNode, newIndex);
    } else
    {
      removeDropdown (childNode);
      addDropdown (childNode, newIndex);
    }
  }

  // DisposeListener interface
  
  public void widgetDisposed (DisposeEvent e)
  {
    dispose ();
  }
  
  // CommandViewNodeListener Interface.

  public void childAdded (CommandViewNodeEvent e)
  {
    CommandViewNode node = e.getNode ();

    // check that a command hasn't popped up outside a group node
    Debug.assertTrue (node.getCommand () == null ||
                      node.getParent () != root,
                      "command '" + node + "' found in group context",
                      this);

    if (node.getParent () == root)
    {
      // If this is a first level child we create a group.
      addGroup (node, findToolBarIndex (node));
    } else if (node.getCommand () != null)
    {
      // Just a normal command
      addCommand (node, findToolBarIndex (node));
    } else
    {
      // requires a dropdown
      addDropdown (node, findToolBarIndex (node));
    }
  }

  public void childRemoved (CommandViewNodeEvent e)
  {
    CommandViewNode node = e.getNode ();

    if (node.getParent () == root)
    {
      // If this is a first level child we destroy the group.
      removeGroup (node, e.getOldIndex ());
    } else if (node.getCommand () != null)
    {
      // Remove the command as it is just a normal command.
      removeCommand (node);
    } else
    {
      // Must be a command menu button then, therefore we remove it.
      removeDropdown (node);
    }
  }

  public void childMoved (CommandViewNodeEvent e)
  {
    CommandViewNode node = e.getNode ();

    if (node.getParent () == root)
      moveGroup (node, e.getOldIndex (), e.getNewIndex ());
    else
      moveGroupChild (e.getNode (), e.getNewIndex ());
  }

  public void commandChanged (CommandViewNodeEvent e)
  {
    CommandViewNode node = e.getSourceNode ();
    int toolBarIndex = findToolBarIndex (node);

    removeCommand (node);
    addCommand (node, toolBarIndex);
  }

  /**
   * Item providers can call this method to let the toolbar provider
   * know the size of one of the items on the provider's toolbar may
   * have changed (e.g. if the text or image changes). The toolbar
   * provider will re-layout the toolbar/coolbar as needed.
   * 
   * @param toolbar The toolbar instance that the provider created its
   *          items in.
   */
  public static void itemSizeChanged (ToolBar toolbar)
  {
    CommandViewToolBarProvider provider =
      (CommandViewToolBarProvider)toolbar.getData ("provider");
    
    provider.updateCoolItemSize ((CoolItem)toolbar.getData ("coolitem"));
  }
}
