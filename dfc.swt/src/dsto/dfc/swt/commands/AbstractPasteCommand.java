package dsto.dfc.swt.commands;

import org.eclipse.swt.SWT;

public abstract class AbstractPasteCommand extends AbstractCommand
{
  public AbstractPasteCommand ()
  {
    super ("edit.Paste", "edit_paste.gif", "cnp",
           "Paste from the clipboard", false, 'p', SWT.CTRL | 'v');
    
    setMainMenuGroup ("Edit.cnp");
  }
}
