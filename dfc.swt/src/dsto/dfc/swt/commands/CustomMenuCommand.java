package dsto.dfc.swt.commands;

import org.eclipse.swt.widgets.Menu;

/**
 * Interface for commands that wish to provide a custom menu UI.
 * 
 * @version $Revision$
 */
public interface CustomMenuCommand extends Command
{
  /**
   * Create a menu UI provider for the command.
   * 
   * @param view The command view.
   * @param menu The host menu for the UI.
   * @param index The index for the UI item(s).
   * @return A CommandMenuItemProvider that provides the UI.
   */
  public CommandMenuItemProvider createMenuProvider
    (CommandView view, Menu menu, int index);
}