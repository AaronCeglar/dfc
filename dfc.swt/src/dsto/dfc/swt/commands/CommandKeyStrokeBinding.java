package dsto.dfc.swt.commands;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Widget;

import dsto.dfc.swt.commands.Command;

/**
 * Binds a widget key stroke listener to a Command. When the widget receives a keystroke the binding
 * checks whether the keystroke conforms to the command's accelerator and whether
 * the command is enabled, if so the binding calls the command's execute method.
 * @author David Karunaratne
 *
 */
public class CommandKeyStrokeBinding implements Listener
{
  Widget widget;
  Command command;
  
  public CommandKeyStrokeBinding (Widget widget, Command command)
  {
    this.widget = widget;
    this.command = command;
    
    widget.addListener (SWT.KeyDown, this);
  }
  
  
  public void handleEvent (Event evt)
  {
    if (evt.type == SWT.KeyDown)
    {
      int accelerator = evt.keyCode + evt.stateMask;

      if (command.isEnabled () && command.getAccelerator () == accelerator)
        command.execute ();
    }
  }
}
