package dsto.dfc.swt.commands;

import java.util.EventObject;

/**
 * @see dsto.dfc.swt.commands.CommandListener
 * 
 * @author mpp
 * @version $Revision$
 */
public class CommandEvent extends EventObject
{
  private Object data;
  
  public CommandEvent (Command source)
  {
    this (source, null);
  }
  
  public CommandEvent (Command source, Object data)
  {
    super (source);
    
    this.data = data;
  }

  public Command getCommand ()
  {
    return (Command)getSource ();
  }
  
  /**
   * Extra command-specific data.
   */
  public Object getData ()
  {
    return data;
  }
}
