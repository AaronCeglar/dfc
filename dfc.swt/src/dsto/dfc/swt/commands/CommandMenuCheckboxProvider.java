package dsto.dfc.swt.commands;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Widget;

import dsto.dfc.logging.Log;
import dsto.dfc.util.Beans;

/**
 * Command UI provider that generates a checkbox MenuItem for the command.
 * 
 * @author mpp
 * @version $Revision$
 */
public class CommandMenuCheckboxProvider extends CommandMenuButtonProvider
{
  private String valueProperty;
  
  public CommandMenuCheckboxProvider (CommandView commandView,
                                      Command command,
                                      String valueProperty,
                                      Menu menu,
                                      int menuIndex)
  {
    super (commandView, command, menu, menuIndex);
    
    this.valueProperty = valueProperty;
    
    updateCheck ();
  }
  
  protected Item createItem (Widget menu, int index)
  {
    MenuItem newItem = new MenuItem ((Menu)menu, SWT.CHECK, index);
    
    updateAccelerator (newItem);
    
    return newItem;
  }
  
  protected void updateProperty (String name)
  {
    if (name.equals (valueProperty))
      updateCheck ();
    else
      super.updateProperty (name);
  }

  private void updateCheck ()
  {
    try
    {
      Boolean value = (Boolean)Beans.getPropertyValue (command, valueProperty);
      
      ((MenuItem)item).setSelection (value.booleanValue ());
    } catch (NoSuchMethodException ex)
    {
      Log.internalError ("Failed to read check property", this, ex);
    }
  }
}
