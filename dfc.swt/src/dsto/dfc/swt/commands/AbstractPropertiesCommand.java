package dsto.dfc.swt.commands;

/**
 * Base class for Properties type commands.
 *
 * @version $Revision$
 */
public abstract class AbstractPropertiesCommand extends AbstractCommand
{
  public AbstractPropertiesCommand ()
  {
    super ("edit.Properties", "properties.gif", "props",
           "Show properties of the selection", false, 'r', 0);
    
    setMainMenuGroup ("Edit.props");
  }
}
