package dsto.dfc.swt.commands;

import org.eclipse.jface.window.Window;
import org.eclipse.jface.window.WindowManager;

import dsto.dfc.databeans.IDataBean;

/**
 * Command to toggle a style setting on a JFace window. Since most styles
 * cannot be changed on a shell once created, the style setting is changed by
 * toggling the boolean value of given data bean property and re-creating the
 * window using close/open. It is assumed that the window bases its style
 * settings on the changed property.
 * 
 * @author mpp
 * @version $Revision$
 */
public class CmdToggleWindowStyle extends BasicToggleCommand
{
  private Window window;
  private IDataBean preferences;
  private String styleProperty;
  
  /**
   * Create the command.
   * 
   * @param name The name of the command eg "Always On Top", "Resizable", etc.
   * @param window The window.
   * @param preferences The preferences to modify.
   * @param styleProperty The property to toggle.
   */
  public CmdToggleWindowStyle (String name,
                               Window window,
                               IDataBean preferences, String styleProperty)
  {
    super ("window." + name, NO_ICON, "window", "", false, '\0', 0);

    this.window = window;
    this.preferences = preferences;
    this.styleProperty = styleProperty;
    
    setValue (preferences.getBooleanValue (styleProperty));
  }

  public void execute ()
  {
    super.execute ();
    
    preferences.setValue (styleProperty, getValue ());

    WindowManager manager = window.getWindowManager ();
    
    window.close ();
    window.open ();    
    
    if (manager != null)
      manager.add (window);
  }
}
