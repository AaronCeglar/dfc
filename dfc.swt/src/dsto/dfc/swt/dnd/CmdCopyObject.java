package dsto.dfc.swt.dnd;

import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Display;

import dsto.dfc.swt.commands.AbstractCopyCommand;

/**
 * Command to copy an object to the clipboard.
 * 
 * @see dsto.dfc.swt.dnd.CmdPasteObject
 * 
 * @author Matthew Phillips
 */
public class CmdCopyObject extends AbstractCopyCommand
{
  protected Transfer [] transferFormats;
  protected Object object;
  
  /**
   * Create a new instance.
   * 
   * @param transfer The SWT Transfer type. eg an instance of
   * {@link DataObjectTransfer}.
   */
  public CmdCopyObject (Transfer transfer)
  {
    super ();
    
    this.transferFormats = new Transfer [] {transfer};
    
    setEnabled (false);
  }
  
  public Object getObject ()
  {
    return object;
  }
  
  /**
   * Set the object to be copied.
   */
  public void setObject (Object newValue)
  {
    this.object = newValue;
    
    setEnabled (object != null);
  }
  
  public void setTransferFormat (Transfer transfer)
  {
    setTransferFormats (new Transfer [] {transfer});
  }
  
  public void setTransferFormats (Transfer [] transfer)
  {
    this.transferFormats = transfer;
  }
  
  /**
   * Create the data to be transferred when the command is executed. The default
   * is to simply wrap object in an array. Subclasses may override to generate
   * data at execution time.
   */
  protected Object [] createTransferData ()
  {
    return new Object [] {object};
  }
  
  public void execute ()
  {
    Clipboard clipboard = null;
    
    try
    {
      clipboard = new Clipboard (Display.getCurrent ());
      
      clipboard.setContents (createTransferData (), transferFormats);
      
      fireCommandExecuted (object);
    } finally
    {
      if (clipboard != null)
        clipboard.dispose ();
    }    
  }
}
