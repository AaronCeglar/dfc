package dsto.dfc.swt.dnd;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;

import org.eclipse.swt.dnd.ByteArrayTransfer;
import org.eclipse.swt.dnd.TransferData;

import dsto.dfc.databeans.io.XmlInput;
import dsto.dfc.databeans.io.XmlOutput;

import static dsto.dfc.logging.Log.diagnostic;

/**
 * SWT clipboard transfer type for DBXML-encoded objects.
 * 
 * @see dsto.dfc.databeans.io.XmlInput
 * @see dsto.dfc.databeans.io.XmlOutput
 * 
 * @author Matthew Phillips
 */
public class DataObjectTransfer extends ByteArrayTransfer
{
  private String [] typeNames;
  private int [] typeIds;
  
  /**
   * Create a new instance.
   * 
   * @param type The base type of the object that will be transferred by this
   * format. This is simply done as a shortcut for generating a logical type
   * name, no test for type conformance is performed on transfer.
   */
  public DataObjectTransfer (Class type)
  {
    this (type.getName ());
  }
  
  /**
   * Create a new instance.
   * 
   * @param typeName A logical name for the type (eg "PersonRecord").
   */
  public DataObjectTransfer (String typeName)
  {
    String fullName = "dataobject/" + typeName;
    
    this.typeNames = new String [] {fullName};
    this.typeIds = new int [] {registerType (fullName)};
  }

  protected int [] getTypeIds ()
  {
    return typeIds;
  }

  protected String [] getTypeNames ()
  {
    return typeNames;
  }
  
  protected void javaToNative (Object object, TransferData transferData)
  {
    if (object == null)
      return;

    if (!(object instanceof Object []))
    {
      diagnostic ("DataObjectTransfer: requires an array as the data to transfer", this);
      
      return;
    }

    if (isSupportedType (transferData))
    {
      Object [] objects = (Object [])object;
      
      if (objects.length != 1)
      {
        diagnostic ("DataObjectTransfer: requires a single element array to transfer", this);
        
        return;
      }

      try
      {
        StringWriter writer = new StringWriter (4 * 1024);

        XmlOutput xmlOutput = new XmlOutput ();

        xmlOutput.write (writer, objects [0], true);
        
        super.javaToNative (writer.toString ().getBytes (), transferData);

      } catch (IOException ex)
      {
        diagnostic ("Failed encode XML for transfer", this, ex);
      }
    }
  }

  protected Object nativeToJava (TransferData transferData)
  {
    Object [] objects = null;
    
    if (isSupportedType (transferData))
    {
      byte [] data = (byte [])super.nativeToJava (transferData);
      
      if (data == null)
        return null;

      objects = new Object [1];
      
      try
      {
        InputStreamReader reader =
          new InputStreamReader (new ByteArrayInputStream (data));
        
        XmlInput xmlInput = new XmlInput ();
        objects [0] = xmlInput.read (reader);
        
      } catch (Exception ex)
      {
        diagnostic ("Failed decode XML for transfer", this, ex);
        
        return null;
      }      
    }
    
    return objects;
  }
}
