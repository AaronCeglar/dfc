package dsto.dfc.swt.dnd;

import org.eclipse.swt.dnd.TextTransfer;

import dsto.dfc.databeans.IDataObject;

/**
 * Command that copies selected properties of an IDataObject to
 * the clipboard as text.
 * 
 * @author Matthew Phillips
 */
public class CmdCopyDataObjectProperties extends CmdCopyObject
{
  private String [] properties;
  
  /**
   * Create a new instance copying properties from a given data object.
   */
  public CmdCopyDataObjectProperties (IDataObject dataObject)
  {
    super (TextTransfer.getInstance ());
    
    setObject (dataObject);
    
    setEnabled (false);
  }
  
  
  public String [] getProperties ()
  {
    return properties;
  }

  /**
   * Set the names of the properties of the current data object that will be
   * copied when command is executed.
   * 
   * @param properties The properties copy. May be null.
   */
  public void setProperties (String [] properties)
  {
    this.properties = properties;
    
    setEnabled (object != null && properties != null && properties.length > 0);
  }
  
  protected Object [] createTransferData ()
  {
    IDataObject dataObject = (IDataObject)object;
    StringBuilder text = new StringBuilder ();
    String lineSeparator = System.getProperty ("line.separator", "\n");
    
    for (int i = 0; i < properties.length; i++)
    {
      String property = properties [i];
      
      if (i > 0)
        text.append (lineSeparator);

      text.append (property).append (": ").append (dataObject.getValue (property));
    }
    
    return new Object [] {text.toString ()};
  }
}
