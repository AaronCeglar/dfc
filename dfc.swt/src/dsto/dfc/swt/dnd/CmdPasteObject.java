package dsto.dfc.swt.dnd;

import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Display;

import dsto.dfc.swt.commands.AbstractPasteCommand;

/**
 * Command to paste an object from the clipboard. Clients may
 * either subclass and override {@link #pasteObject(Object[])} or listen
 * for command executed events.
 * 
 * @see dsto.dfc.swt.dnd.CmdCopyObject
 * 
 * @author Matthew Phillips
 */
public class CmdPasteObject extends AbstractPasteCommand
{
  private Transfer objectTransfer;

  /**
   * Create a new instance.
   * 
   * @param objectTransfer The SWT Transfer type. eg an instance of
   * {@link DataObjectTransfer}.
   */
  public CmdPasteObject (Transfer objectTransfer)
  {
    super ();
    
    this.objectTransfer = objectTransfer;
  }

  
  public void setTransfer (Transfer transfer)
  {
    this.objectTransfer = transfer;
  }
  
  public void execute ()
  {
    Clipboard clipboard = null;
    Object object = null;
    
    try
    {
      clipboard = new Clipboard (Display.getCurrent ());
    
      object = clipboard.getContents (objectTransfer);

    } finally
    {
      if (clipboard != null)
        clipboard.dispose ();
    }
    
    if (object != null)
    {
      if (object instanceof Object[])
        pasteObject ((Object[]) object);
      else 
        pasteObject (new Object[] {object});
    }
  }

  /**
   * Paste object(s) from the clipboard. Default action is to fire command
   * executed event with objects as data. Subclasses may override.
   */
  protected void pasteObject (Object [] objects)
  {
    fireCommandExecuted (objects);
  }
}
