package dsto.dfc.swt.text;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.LineStyleEvent;
import org.eclipse.swt.custom.LineStyleListener;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import org.eclipse.jface.resource.JFaceColors;

import dsto.dfc.util.Disposable;

import dsto.dfc.swt.text.UrlScanner.UrlRange;
import dsto.dfc.swt.util.Documents;

import static dsto.dfc.swt.DFC_SWT.messageBox;

/**
 * Adds automatic clickable links to URL's in a styled text widget.
 * <p>
 * 
 * Note: the link colour is extracted from
 * {@link JFaceColors#getHyperlinkText(org.eclipse.swt.widgets.Display)}
 * which may not be set. To set manually use:
 * 
 * <pre>
 *   JFaceResources.getColorRegistry ().put
 *     (JFacePreferences.HYPERLINK_COLOR,
 *      display.getSystemColor (SWT.COLOR_BLUE).getRGB ());
 * </pre>
 * 
 * See <ECLIPSE_HOME>/plugins/org.eclipse.rcp.source...
 * /org.eclipse.ui.workbench.../org/eclipse/ui/internal/dialogs/ProductInfoDialog.java
 * for an another example of doing this, including keyboard traversal.
 * 
 * @author Matthew Phillips
 */
public class StyledTextLinkOTron
  implements LineStyleListener, Listener, Disposable
{
  private static final int SINGLE_CLICK_MAX_TIME = 300;
  
  private boolean openInBrowser;
  private List<Listener> listeners;
  private UrlRange [] urlRanges;
  private StyledText textField;
  private Color urlColor;
  private Cursor handCursor;
  private Cursor busyCursor;
  private int mouseDownTime;
  private UrlScanner urlScanner;
  
  /**
   * Create a new instance.
   */
  public StyledTextLinkOTron (StyledText textField)
  {
    Display display = textField.getDisplay ();
    
    this.urlScanner = new UrlScanner ();
    this.openInBrowser = true;
    this.listeners = new ArrayList<Listener> ();
    this.textField = textField;
    
    this.urlColor = JFaceColors.getHyperlinkText (display);
    this.handCursor = display.getSystemCursor (SWT.CURSOR_HAND);
    this.busyCursor = display.getSystemCursor (SWT.CURSOR_WAIT);
    
    // fall back on blue for hyperlinks
    if (urlColor == null)
      urlColor = display.getSystemColor (SWT.COLOR_BLUE);
    
    textField.addLineStyleListener (this);
    textField.addListener (SWT.MouseMove, this);
    textField.addListener (SWT.MouseDown, this);
    textField.addListener (SWT.MouseUp, this);
  }

  public void dispose ()
  {
    if (textField != null)
    {
      textField.removeLineStyleListener (this);
      textField.removeListener (SWT.MouseDown, this);
      textField.removeListener (SWT.MouseMove, this);
      textField.removeListener (SWT.MouseUp, this);
      textField.removeListener (SWT.Dispose, this);
      
      textField = null;
    }
  }
  
  /**
   * Set to true to automatically open links in the system browser
   * (default true).
   */
  public void openInBrowser (boolean newValue)
  {
    openInBrowser = newValue;
  }
  
  public boolean openInBrowser ()
  {
    return openInBrowser;
  }

  /**
   * Add a SWT.Selection listener to listen for URL clicks (URL in
   * Event.text). Can veto/ok open in browser with Event.doit.
   */
  public void addListener (int event, Listener listener)
  {
    if (event == SWT.Selection)
    {
      listeners = new ArrayList (listeners);
      listeners.add (listener);
    }
  }

  public void removeListener (int event, Listener listener)
  {
    if (event == SWT.Selection)
    {
      listeners = new ArrayList (listeners);
      listeners.remove (listener);
    }
  }
  
  /**
   * The set of valid URL schemes that are recognised as links. Can
   * add "*" to this to allow any vaguely URL-looking link. Default is
   * {"http", "ftp", "mailto"}.
   */
  public Set validSchemes ()
  {
    return urlScanner.validSchemes ();
  }
  
  public void handleEvent (Event e)
  {
    if (e.type == SWT.MouseMove)
    {
      textField.setCursor (urlRangeAt (e) == null ? null : handCursor);
    } else if (e.type == SWT.MouseUp)
    { 
      // if a quick click process as a single click
      boolean singleClick = e.time - mouseDownTime < SINGLE_CLICK_MAX_TIME;
      
      if (singleClick && e.button == 1)
      {
        UrlRange range = urlRangeAt (e);
        
        if (range != null)
        {
          String url = textField.getText (range.start, range.start+range.length);
          Event selectedEvent = fireUrlSelected (url);
          
          if (selectedEvent.doit)
          {
            textField.setSelectionRange (range.start, range.length);
            textField.setCursor (busyCursor);
  
            openUrl (url);
            
            textField.setCursor (null);
          }
        }
      }
    } else if (e.type == SWT.MouseDown)
    {
      mouseDownTime = e.time;
      UrlRange range = urlRangeAt (e);
      
      if (range != null)
        textField.setSelectionRange (range.start, range.length);
    }
  }

  private void openUrl (String url)
  {
    try
    {
      Documents.open (url);
    } catch (IOException ex)
    {
      messageBox
        (textField.getShell (), SWT.ICON_WARNING, "Open URL",
         "Could not open \"" + url + "\"", ex);
    }
  }

  private Event fireUrlSelected (String url)
  {
    Event event = new Event ();
    event.widget = textField;
    event.data = url;
    event.text = url;
    event.doit = openInBrowser;
    
    for (Listener listener : listeners)
      listener.handleEvent (event);
    
    return event;
  }

  /**
   * Find the URL range at a given mouse event.
   */
  public UrlRange urlRangeAt (Event e)
  {
    try
    {
      return urlRangeAt
        (textField.getOffsetAtLocation (new Point (e.x, e.y)));
    } catch (IllegalArgumentException ex)
    {
      // dodgy, but no apparent way to test whether x, y is in range
      return null;
    }
  }
  
  /**
   * Find the URL range for a given offset into the text.
   */
  public UrlRange urlRangeAt (int offset)
  {
    maybeRebuildUrlRanges ();
    
    for (int i = 0; i < urlRanges.length; i++)
    {
      UrlRange range = urlRanges [i];
      
      if (offset >= range.start)
      {
        if (offset < range.start + range.length)
          return range;
      } else
      {
        return null;
      }
    }
    
    return null;
  }

  /**
   * Build URL range mapping in urlRanges if not currently valid.
   */
  private void maybeRebuildUrlRanges ()
  {
    if (urlRanges == null)
    {
      List<UrlRange> ranges = scanForUrls (textField.getText (), 0);
      
      urlRanges = new UrlRange [ranges.size ()];
      ranges.toArray (urlRanges);
    }
  }
  
  private void clearUrlRangeCache ()
  {
    urlRanges = null;
  }

  /**
   * Called by text field to get style for a line.
   */
  public void lineGetStyle (LineStyleEvent e)
  {
    // clear cache when this is called => change to text
    clearUrlRangeCache ();
    
    List<UrlRange> ranges = scanForUrls (e.lineText, e.lineOffset);
    
    e.styles = new StyleRange [ranges.size ()];
    
    for (int i = 0; i < ranges.size (); i++)
    {
      UrlRange urlRange = ranges.get (i);
      StyleRange styleRange = new StyleRange ();

      styleRange.start = urlRange.start;
      styleRange.length = urlRange.length;
      styleRange.underline = true;
      styleRange.foreground = urlColor;

      e.styles [i] = styleRange;
    }
  }
  
  protected List<UrlRange> scanForUrls (String text, int offset)
  {
    return urlScanner.scanForUrls (text, offset);
  }
}
