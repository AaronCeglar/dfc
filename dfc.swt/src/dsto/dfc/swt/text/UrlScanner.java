/**
 * 
 */
package dsto.dfc.swt.text;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static dsto.dfc.swt.util.Documents.looksLikeDOSFile;
import static dsto.dfc.swt.util.Documents.looksLikeUNCFile;

/**
 * Scans for URLs within a given text.
 */
public class UrlScanner
{
  private static final int IN_WILDERNESS = 0;
  private static final int IN_URL = 1;
  private static final int IN_PREFIX = 2;
  private static final int AT_SCHEME_SEP = 3;
  private static final int IN_BACK_SLASH = 4;
  private static final int IN_FORWARD_SLASH = 5;

  private static final String PUNCT_CHARS = ":;,.!?)]>}\"'";

  private Set<String> validSchemes;

  public UrlScanner ()
  {
    this.validSchemes = new HashSet<String> ();
    validSchemes.add ("file");
    validSchemes.add ("http");
    validSchemes.add ("https");
    validSchemes.add ("ftp");
    validSchemes.add ("mailto");
  }
  
  /**
   * The set of valid URL schemes that are recognised as links. Can
   * add "*" to this to allow any vaguely URL-looking link. Default is
   * {"http", "ftp", "mailto"}.
   */
  public Set validSchemes ()
  {
    return validSchemes;
  }

  /**
   * Scan text for URL's. adding UrlRange's to a list as they're
   * found. Uses a simple state machine to scan in a single pass.
   * 
   * @param text The text to scan.
   * @param offset The offset to add to each range.
   */
  public List<UrlRange> scanForUrls (String text, int offset)
  {
    ArrayList<UrlRange> ranges = new ArrayList<UrlRange> ();
    int index = 0;
    int state = IN_WILDERNESS;
    int start = -1;

    // scan each character in text
    while (index <= text.length ())
    {
      boolean eol = index == text.length ();
      char c = eol ? 0 : text.charAt (index);

      if (Character.isWhitespace (c) || eol)
      {
        if (state == IN_URL)
        {
          index = trimUrl (text, index);

          String url = text.substring (start, index);

          String scheme = schemeOf (url);

          if ((scheme != null && 
              (validSchemes.contains ("*") || 
               validSchemes.contains (scheme))) || 
               looksLikeDOSFile (url) || 
               looksLikeUNCFile (url))
          {
            ranges.add (new UrlRange (start + offset, index - start, url));
          }
        }

        state = IN_WILDERNESS;
        start = -1;
      } else if (Character.isLetter (c))
      {
        if (state == IN_WILDERNESS)
        {
          state = IN_PREFIX;
          start = index;
        } else if (state == AT_SCHEME_SEP)
        {
          state = IN_URL;
        }
      } else if (c == ':')
      {
        if (state == IN_PREFIX)
          state = AT_SCHEME_SEP;
      } else if (c == '\\')
      {
        if (state == IN_WILDERNESS || state == IN_FORWARD_SLASH)
        {
          state = IN_BACK_SLASH;
          start = index;
        } else if (state == IN_BACK_SLASH)
        {
          // second \ in prefix means probable UNC path
          state = AT_SCHEME_SEP;
        }
      } else if (c == '/')
      {
        if (state == IN_WILDERNESS || state == IN_BACK_SLASH)
        {
          state = IN_FORWARD_SLASH;
          start = index;
        } else if (state == IN_FORWARD_SLASH)
        {
          // second / in prefix means probable UNC path
          state = AT_SCHEME_SEP;
        }
      } else
      {
        if (state == AT_SCHEME_SEP && !isEndPunct (c))
          state = IN_URL;
      }

      index++;
    }
    return ranges;
  }

  /**
   * Trim characters off the end of a URL that probably don't belong.
   */
  private static int trimUrl (String url, int index)
  {
    do
    {
      index--;
    } while (index >= 0 && isEndPunct (url.charAt (index)));

    return index + 1;
  }

  private static boolean isEndPunct (char c)
  {
    return PUNCT_CHARS.indexOf (c) != -1;
  }

  /**
   * Get a URL's scheme (eg "http").
   */
  private static String schemeOf (String url)
  {
    int colon = url.indexOf (':');

    return colon < 2 ? null : url.substring (0, colon);
  }

  /**
   * A range in the text field that represents a URL.
   */
  public static class UrlRange
  {
    public int start;
    public int length;

    public UrlRange (int start, int length, String url)
    {
      this.start = start;
      this.length = length;
    }

    public String toString ()
    {
      return "Range [" + start + ", " + length;
    }
  }
}
