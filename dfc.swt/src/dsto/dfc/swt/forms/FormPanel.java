package dsto.dfc.swt.forms;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import dsto.dfc.logging.Log;

/**
 * Utility base class for panels that uses the pipes framework to
 * handle form editing. Clients connect their controls using the
 * connect () methods or by using the root "formPipe" pipe directly
 * and then use {@link #setFormValue(Object)}to load the value into
 * the form.
 *
 * @see dsto.dfc.swt.forms.Forms
 * @see dsto.dfc.swt.forms.Pipes
 *
 * @author mpp
 * @version $Revision$
 */
public abstract class FormPanel
  extends Composite implements PipeErrorHandler
{
  protected MultiPipe formPipe;

  public FormPanel (Composite parent, int style)
  {
    super (parent, style);

    formPipe = new MultiPipe ();
    formPipe.setErrorHandler (this);

    RootPipe rootPipe = new RootPipe ();
    rootPipe.connect (formPipe);

    // dispose () is not called when we're a child, need to listen for event
    addListener (SWT.Dispose, new Listener ()
    {
      public void handleEvent (Event e)
      {
        formPipe.dispose ();
      }
    });
  }

  @Override
  public void dispose ()
  {
    formPipe.dispose ();

    super.dispose ();
  }

  public void commit ()
  {
    performFlush ();
  }

  /**
   * The the value being edited by the form.
   * @param value
   */
  protected void setFormValue (Object value)
  {
    formPipe.input (value);
  }

  /**
   * Shortcut for {@link Pipes#connect(Pipe, String, Text)}.
   */
  protected TextEndpoint connect (String property, Text textField)
  {
    return Pipes.connect (formPipe, property, textField);
  }

  protected TextEndpoint connect (String property, StyledText textField)
  {
    return Pipes.connect (formPipe, property, textField);
  }
  
  protected NumberEndpoint connect (String property, Spinner spinnerField)
  {
    return Pipes.connect (formPipe, property, spinnerField);
  }
  
  protected NumberEndpoint connect (String property, Slider sliderField)
  {
    return Pipes.connect (formPipe, property, sliderField);
  }
  
  protected NumberEndpoint connect (String property, Scale scaleField)
  {
    return Pipes.connect (formPipe, property, scaleField);
  }
  
  protected ButtonEndpoint connect (String property, Button buttonField)
  {
    return Pipes.connect (formPipe, property, buttonField);
  }
  
  /**
   * Connect a property to a target using a {@link PropertyPipe}.
   */
  protected PropertyPipe connect (String property, Pipe target)
  {
    PropertyPipe propertyPipe = new PropertyPipe (property);

    formPipe.connect (propertyPipe).connect (target);

    return propertyPipe;
  }

  protected void performFlush ()
  {
    formPipe.flush ();
  }

  /**
   * Called when an error occurs when loading/unloading the pipe.
   * Subclasses may override to add better error reporting (currently
   * reports a log diagnostic).
   */
  public void handlePipeError (Pipe source, String message)
  {
    Log.diagnostic ("Pipe error: " + message, this);
  }

  /**
   * Root pipe triggers a flush whenever a pipe attempts to output anything to
   * it.
   */
  class RootPipe extends AbstractBiDiPipe implements Pipe
  {
    private boolean flushing = false;

    public boolean output (Object value)
    {
      if (!flushing)
      {
        flushing = true;
        performFlush ();
        flushing = false;
      }

      return true;
    }

    public boolean isDisposed ()
    {
      return false;
    }

    public boolean input (Object value)
    {
      return false;
    }
  }
}
