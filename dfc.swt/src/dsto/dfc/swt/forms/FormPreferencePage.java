package dsto.dfc.swt.forms;

import org.eclipse.jface.preference.PreferencePage;

/**
 * Base class for preference pages based on editing a value using forms
 * framework pipes. Sets up a base multi pipe for the form (formPipe) which
 * subclasses will connect to in {@link #createContents}. Error handling and
 * flushing is handled automatically.
 * 
 * @author mpp
 * @version $Revision$
 */
public abstract class FormPreferencePage
  extends PreferencePage implements PipeErrorHandler
{
  protected MultiPipe formPipe;
  
  public FormPreferencePage ()
  {
    noDefaultAndApplyButton ();
    
    formPipe = new MultiPipe ();
    formPipe.setErrorHandler (this);
    
    RootPipe rootPipe = new RootPipe ();
    rootPipe.connect (formPipe);
  }
  
  public void dispose ()
  {
    formPipe.dispose ();
    
    super.dispose ();
  }
  
  public boolean performOk ()
  {
    performFlush ();
    
    return isValid ();
  }
  
  public boolean okToLeave()
  {
    performFlush ();
    
    return isValid ();
  }
  
  protected void performFlush ()
  {
    setErrorMessage (null);
    
    setValid (formPipe.flush ());
  }
  
  public void handlePipeError (Pipe source, String message)
  {
    setErrorMessage (message);
  }
  
  /**
   * Root pipe triggers a flush whenever a pipe attempts to output anything to
   * it.
   */
  class RootPipe extends AbstractBiDiPipe implements Pipe
  {
    private boolean flushing = false;
    
    public boolean output (Object value)
    {
      if (!flushing)
      {
        flushing = true;
        performFlush ();
        flushing = false;
      }
      
      return true;
    }

    public boolean input (Object value)
    {
      return false;
    }
    
    public boolean isDisposed ()
    {
      return false;
    }
  }
}
