package dsto.dfc.swt.forms;

import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import dsto.dfc.text.IStringTranslator;
import dsto.dfc.util.ITranslator;

/**
 * Pipe utility methods.
 * 
 * @author mpp
 * @version $Revision$
 */
public final class Pipes
{
  public static TextEndpoint connect (Pipe source, String property, 
                                      Text textField)
  {
    return (TextEndpoint)source.connect
      (new PropertyPipe (property)).connect
        (new TextEndpoint (textField));
  }
  
  public static TextEndpoint connect (Pipe source, String property, 
                                      StyledText textField)
  {
    return (TextEndpoint)source.connect
      (new PropertyPipe (property)).connect
        (new TextEndpoint (textField));
  }
  
  public static TextEndpoint connect (Pipe source, String property, Label label)
  {
    return (TextEndpoint) source.connect (new PropertyPipe (property)).connect (
        new TextEndpoint (label));
  }
  
  public static NumberEndpoint connect (Pipe source, String property, 
                                        Spinner spinnerField)
  {
    NumberEndpoint spinnerEndPoint = new NumberEndpoint (spinnerField);
    spinnerEndPoint.commitMode = NumberEndpoint.COMMIT_DEFAULT;
    return (NumberEndpoint)source.connect
      (new PropertyPipe (property)).connect (spinnerEndPoint);
  }
  
  public static NumberEndpoint connect (Pipe source, String property,
      Scale scaleField)
  {
    NumberEndpoint spinnerEndPoint = new NumberEndpoint (scaleField);
    spinnerEndPoint.commitMode = NumberEndpoint.COMMIT_DEFAULT;
    return (NumberEndpoint) source.connect 
      (new PropertyPipe (property)).connect (spinnerEndPoint);
  }

  public static NumberEndpoint connect (Pipe source, String property,
      Slider sliderField)
  {
    NumberEndpoint spinnerEndPoint = new NumberEndpoint (sliderField);
    spinnerEndPoint.commitMode = NumberEndpoint.COMMIT_DEFAULT;
    return (NumberEndpoint) source.connect 
      (new PropertyPipe (property)).connect (spinnerEndPoint);
  }

  public static TextEndpoint connect (Pipe source, String property, 
                                      Text textField,
                                      IStringTranslator translator)
  {
    return (TextEndpoint)source.connect
      (new PropertyPipe (property)).connect
        (new TranslatorPipe (translator)).connect
          (new TextEndpoint (textField));
  }
  
  public static TextEndpoint connect (Pipe source, String property, 
                                      Text textField,
                                      ITranslator inputTranslator,
                                      ITranslator outputTranslator)
  {
    return (TextEndpoint)source.connect
      (new PropertyPipe (property)).connect
        (new TranslatorPipe (inputTranslator, outputTranslator)).connect
          (new TextEndpoint (textField));
  }
  
  public static ButtonEndpoint connect (Pipe source, String property, 
                                        Button button)
  {
    return (ButtonEndpoint)source.connect
      (new PropertyPipe (property)).connect (new ButtonEndpoint (button));
  }
  
  public static ButtonEndpoint connect (Pipe source, String property, 
                                        Button button,
                                        ITranslator inputTranslator,
                                        ITranslator outputTranslator)
  {
    return (ButtonEndpoint)source.connect
      (new PropertyPipe (property)).connect
        (new TranslatorPipe (inputTranslator, outputTranslator)).connect
          (new ButtonEndpoint (button));
  }
  
  public static TextEndpoint connect (Pipe source, String property, Combo combo)
  {
    return (TextEndpoint)source.connect
      (new PropertyPipe (property)).connect (new TextEndpoint (combo));
  }

  public static TextEndpoint connect (Pipe source, String property, Combo combo,
                                      IStringTranslator translator)
  {
    return (TextEndpoint)source.connect
      (new PropertyPipe (property)).connect
        (new TranslatorPipe (translator)).connect
          (new TextEndpoint (combo));
  }

  public static TextEndpoint connect (Pipe source, String property, Combo combo,
                                      ITranslator inputTranslator,
                                      ITranslator outputTranslator)
  {
    return (TextEndpoint)source.connect
      (new PropertyPipe (property)).connect
        (new TranslatorPipe (inputTranslator, outputTranslator)).connect
          (new TextEndpoint (combo));
  }
}
