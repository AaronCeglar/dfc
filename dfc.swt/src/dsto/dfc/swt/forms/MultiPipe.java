package dsto.dfc.swt.forms;

import java.util.ArrayList;

/**
 * A multiplex pipe that outputs its input to a list of children.
 * 
 * @author mpp
 * @version $Revision$
 */
public class MultiPipe extends AbstractPipe implements Pipe
{
  private ArrayList pipes;
  
  /**
   * Constructor for MultiPipe.
   */
  public MultiPipe ()
  {
    this.pipes = new ArrayList (5);
  }
  
  public void dispose ()
  {
    if (pipes != null)
    {
      for (int i = 0; i < pipes.size (); i++)
      {
        Pipe pipe = (Pipe)pipes.get (i);
        
        pipe.dispose ();
      }
      
      pipes = null;
    }
  }
  
  public boolean isDisposed ()
  {
    return pipes == null;
  }
  
  public boolean input (Object value)
  {
    boolean succeeded = true;
    
    for (int i = 0; i < pipes.size (); i++)
    {
      Pipe pipe = (Pipe)pipes.get (i);
      
      succeeded &= pipe.input (value);
    }
    
    return succeeded;
  }
  
  public boolean output (Object value)
  {
    if (parent != null)
      return parent.output (value);
    else
      return true;
  }
  
  public boolean flush ()
  {
    boolean succeeded = true;
    
    for (int i = 0; i < pipes.size (); i++)
    {
      Pipe pipe = (Pipe)pipes.get (i);
      
      succeeded &= pipe.flush ();
    }
    
    return succeeded;
  }
  
  public Pipe connect (Pipe pipe)
  {
    pipe.setParent (this);
    
    pipes.add (pipe);
   
    if (errorHandler != null)
      pipe.setErrorHandler (errorHandler);

    return pipe;
  }
  
  public void setErrorHandler (PipeErrorHandler handler)
  {
    super.setErrorHandler (handler);
    
    for (int i = 0; i < pipes.size (); i++)
    {
      Pipe pipe = (Pipe)pipes.get (i);
      
      pipe.setErrorHandler (handler);
    }
  }
}
