package dsto.dfc.swt.forms;

/**
 * Base class for pipe endpoints: pipe segments that put their input into a 
 * non-pipe (eg a UI control). Endpoints cannot have child pipes and cannot
 * have values output () to them.
 *  
 * @author phillipm
 */
public abstract class PipeEndpoint extends AbstractPipe
{
  public abstract boolean flush ();

  public abstract boolean input (Object value);

  public Pipe connect (Pipe pipe)
  {
    throw new UnsupportedOperationException ("Not supported for an endpoint");
  }
  
  public boolean output (Object value)
  {
    throw new UnsupportedOperationException ("Not supported for an endpoint");
  }
}
