package dsto.dfc.swt.forms;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import dsto.dfc.swt.DFC_SWT;

/**
 * Utility methods for form creation/management.
 *
 * @author mpp
 * @version $Revision$
 */
public final class Forms
{
  private Forms ()
  {
    // zip
  }
 
  /**
   * Create a wrapping description label. Assumes that parent layout is a
   * GridLayout using 2 columns.
   */
  public static Label description (Composite parent, String text)
  {
    return description (parent, text, 60);
  }
  
  /**
   * Create a wrapping description label. Assumes that parent layout is a
   * GridLayout using 2 columns.
   * 
   * @param parent The parent component.
   * @param text The text to display.
   * @param columns The suggested number of columns for the text. If 0, the
   * column count is the same as the numer of characters in the text, ensuring
   * the text is on one line.
   */
  public static Label description (Composite parent, String text, int columns)
  {
    Label label = new Label (parent, SWT.WRAP);
    label.setText (text);
    GridData nameDescLayout = new GridData ();
    nameDescLayout.horizontalSpan = 2;
   
    if (columns == 0)
      columns = text.length ();

    nameDescLayout.widthHint = DFC_SWT.getTextUnitWidth (label) * columns;
    nameDescLayout.grabExcessHorizontalSpace = true;
    nameDescLayout.horizontalAlignment = GridData.FILL;
    label.setLayoutData (nameDescLayout);
    
    return label;
  }
  
  /**
   * Create a text field with a descriptive label to the left, and thus takes
   * two cells worth of space. The label is accessible via 
   * {@link Text#getData(String)} with the property "label".<p>
   * 
   * Assumes that the {@link Composite} this {@link Control} is being added to
   * is using the {@link GridLayout}.
   *
   * @param parent The parent component.
   * @param labelText The text of the label.
   * @return The new text field.
   * 
   * @see #text(Composite,String,int)
   * @see #getLabel(Control)
   */
  public static Text text (Composite parent, String labelText)
  {
    return text (parent, labelText, SWT.DEFAULT);
  }
  
  /**
   * Create a text field with space for the specified number of columns with a
   * descriptive label to the left, and thus takes two cells worth of space. The
   * label is accessible via {@link Text#getData(String)} with the property
   * "label".<p>
   * 
   * Assumes that the {@link Composite} this {@link Control} is being added to
   * is using the {@link GridLayout}.
   *
   * @param parent The parent component.
   * @param labelText The text of the label.
   * @param columns The suggested number of colums of text for the field.
   * @return The new text field.
   * 
   * @see #getLabel(Control)
   */
  public static Text text (Composite parent, String labelText, int columns)
  {
    return text (parent, labelText, columns, 1);
  }
  
  /**
   * Create a text field with space for the specified number of columns with a
   * descriptive label to the left, and thus takes an extra cell's worth of
   * space. The label is accessible via {@link Text#getData(String)} with the
   * property "label". The text field's resizing is governed by the
   * <code>hSpan</code> parameter, which is expected to be a value appropriate
   * for {@link GridData#horizontalSpan}.<p>
   * 
   * Assumes that the {@link Composite} this {@link Control} is being added to
   * is using the {@link GridLayout}.
   *
   * @param parent The parent component.
   * @param labelText The text of the label.
   * @param columns The suggested number of colums of text for the field.
   * @param hSpan The horizontal (GridLayout) span of the text.
   * @return The new text field.
   * 
   * @see #getLabel(Control)
   */
  public static Text text (Composite parent, String labelText,
                           int columns, int hSpan)
  {
   return text (parent, labelText, columns, hSpan, SWT.BORDER);
  }
  
  /**
   * Create a text field with space for the specified number of columns with a
   * descriptive label to the left, and thus takes an extra cell's worth of
   * space. The label is accessible via {@link Text#getData(String)} with the
   * property "label". The text field's resizing is governed by the
   * <code>hSpan</code> parameter, which is expected to be a value appropriate
   * for {@link GridData#horizontalSpan}. The style of the text field is
   * specified by <code>style</code>.<p>
   * 
   * Assumes that the {@link Composite} this {@link Control} is being added to
   * is using the {@link GridLayout}.
   *
   * @param parent The parent component.
   * @param labelText The text of the label.
   * @param columns The suggested number of columns of text for the field.
   * @param hSpan The horizontal (GridLayout) span of the text.
   * @param style The style for the text field.
   * @return The new text field.
   * 
   * @see #getLabel(Control)
   */
  public static Text text (Composite parent, String labelText,
                           int columns, int hSpan, int style)
  {
    Label label = new Label (parent, SWT.NONE);
    label.setText (labelText);
    
    Text text = new Text (parent, style);
    GridData nameFieldLayout = new GridData ();
    
    nameFieldLayout.horizontalSpan = hSpan;
    
    if (columns != SWT.DEFAULT)
      nameFieldLayout.widthHint = columns * DFC_SWT.getTextUnitWidth (text);
    
    text.setLayoutData (nameFieldLayout);
    text.setData ("label", label);
    return text;
  }
  
  /**
   * Create a label and combo box, and thus takes two cells worth of space. The
   * label is accessible via {@link Combo#getData(String)} with the property
   * "label". The style of the combo is specified by <code>style</code>.<p>
   * 
   * Assumes that the {@link Composite} this {@link Control} is being added to
   * is using the {@link GridLayout}.
   *
   * @param parent The parent control.
   * @param labelText The text for the combo label.
   * @param style The style for the combo.
   * @return The new combo box with an attached GridData as layout data.
   * 
   * @see #combo(Composite, String, int, int)
   * @see #getLabel(Control)
   */
  public static Combo combo (Composite parent, String labelText, int style)
  {
    return combo (parent, labelText, SWT.DEFAULT, style);
  }
  
  /**
   * Create a label and combo box, and thus takes two cells worth of space. The
   * label is accessible via {@link Combo#getData(String)} with the property
   * "label". The style of the combo is specified by <code>style</code>. The
   * <code>columns</code> parameter is used to set the width hint for the combo.
   * <p>
   * 
   * Assumes that the {@link Composite} this {@link Control} is being added to
   * is using the {@link GridLayout}.
   *
   * @param parent The parent control.
   * @param labelText The text for the combo label.
   * @param columns The number of columns for the combo. Can be SWT.DEFAULT.
   * @param style The style for the combo.
   * @return The new combo box with an attached GridData as layout data.
   * 
   * @see #getLabel(Control)
   */
  public static Combo combo (Composite parent, String labelText,
                             int columns, int style)
  {
    Label label = new Label (parent, SWT.NONE);
    label.setText (labelText);
    
    Combo combo = new Combo (parent, style);
    GridData nameFieldLayout = new GridData ();
    
    if (columns != SWT.DEFAULT)
      nameFieldLayout.widthHint = columns * DFC_SWT.getTextUnitWidth (combo);
    
    combo.setLayoutData (nameFieldLayout);
    
    combo.setData ("label", label);
    
    return combo;
  }
  
  /**
   * Get the label associated with a control created by the
   * {@link #text(Composite, String)} or {@link #combo(Composite, String, int)} 
   * methods.
   */
  public static Label getLabel (Control control)
  {
    return (Label)control.getData ("label");
  }
  
  /**
   * Creates a {@link Group} within the given parent with the given text and the
   * style {@link SWT#SHADOW_ETCHED_IN} Sets the layout of the {@link Group} to
   * be a {@link GridLayout}.
   * 
   * @param parent The parent of the {@link Group}.
   * @param text The text to show at the top of the {@link Group}.
   * @return A new {@link Group}.
   */
  public static Group group (Composite parent, String text)
  {
    return group (parent, text, SWT.SHADOW_ETCHED_IN);
  }
  
  /**
   * Creates a {@link Group} within the given parent with the given text and 
   * style. Sets the layout of the {@link Group} to be a {@link GridLayout}. 
   * 
   * @param parent The parent of the {@link Group}.
   * @param text   The text to show at the top of the {@link Group}.
   * @param style  The SWT style use use for the {@link Group}.
   * @return A new {@link Group}.
   */
  public static Group group (Composite parent, String text, int style)
  {
    Group group = new Group (parent, style);
    GridLayout layout = new GridLayout (1, false);
    
    layout.marginHeight = 4;
    layout.marginWidth = 4;
    
    group.setText (text);
    group.setLayout (layout);
    
    return group;
  }
  
  /**
   * Creates a {@link Button} within the given parent with the given text and
   * style.
   * 
   * @param parent The parent {@link Composite} for the {@link Button}.
   * @param text   The text to show on the {@link Button}.
   * @param style  The SWT style of the {@link Button}.
   * @return A new {@link Button}.
   */
  public static Button button (Composite parent, String text, int style)
  {
    Button button = new Button (parent, style);
    
    button.setText (text);
    
    return button;    
  }
  
  /**
   * Creates a {@link GridLayout} instance with the given number of columns and
   * margin width and height of 0, a {@link GridLayout#verticalSpacing} value of
   * 6, and sets it to be the layout of the given {@link Composite}
   * <code>panel</code>.<p>
   * 
   * <b>NB</b> There is no need to invoke <code>panel.setLayout()</code> with
   * the returned {@link GridLayout} because it's already been done.
   * 
   * @param panel The {@link Composite} for which to create and set the layout.
   * @param columns The number of columns the layout will have.
   * @return The new {@link GridLayout}, which has been set on the provided
   *         <code>panel</code>.
   */
  public static GridLayout gridLayout (Composite panel, int columns)
  {
    return gridLayout (panel, columns, 0);
  }
  
  /**
   * Creates a {@link GridLayout} instance with the given number of columns and
   * margin width and height of <code>margin</code>, a
   * {@link GridLayout#verticalSpacing} value of 6, and sets it to be the layout
   * of the given {@link Composite} <code>panel</code>.<p>
   * 
   * <b>NB</b> There is no need to invoke <code>panel.setLayout()</code> with
   * the returned {@link GridLayout} because it's already been done.
   * 
   * @param panel   The {@link Composite} for which to create and set the 
   *                layout.
   * @param columns The number of columns the layout will have.
   * @param margin  The value to use for the layout's margin width and height.
   * @return The new {@link GridLayout}, which has been set on 
   *         the provided <code>panel</code>.
   */
  public static GridLayout gridLayout (Composite panel, int columns, int margin)
  {
    GridLayout layout = new GridLayout (columns, false);

    layout.marginHeight = margin;
    layout.marginWidth = margin;
    layout.verticalSpacing = 6;
    
    panel.setLayout (layout);
    
    return layout;
  }

  public static GridLayout gridLayout (Composite panel, int columns, int margin, int vertSpacing)
  {
    GridLayout layout = new GridLayout (columns, false);

    layout.marginHeight = margin;
    layout.marginWidth = margin;
    layout.verticalSpacing = vertSpacing;
    
    panel.setLayout (layout);
    
    return layout;
  }

  
  /**
   * Creates a left-aligned {@link Label} within the parent {@link Composite}
   * <code>panel</code> with the provided text.
   * 
   * @param panel The parent for the {@link Label}.
   * @param text  The text for the {@link Label} to show.
   * @return A new {@link Label}.
   */
  public static Label label (Composite panel, String text)
  {
    return label (panel, text, SWT.LEFT);
  }
  
  /**
   * Creates a {@link Label} within the parent {@link Composite}
   * <code>panel</code> with the provided text and style.
   * 
   * @param panel The parent for the {@link Label}.
   * @param text The text for the {@link Label} to show.
   * 
   * @return A new {@link Label}.
   */
  public static Label label (Composite panel, String text, int style)
  {
    return label (panel, text, SWT.DEFAULT, style);
  }
  
  /**
   * Creates a {@link Label} within the parent {@link Composite}
   * <code>panel</code> with the provided text and style.
   * 
   * @param panel The parent for the {@link Label}.
   * @param text The text for the {@link Label} to show.
   * @param columns The suggested max number of columns or
   *          SWT.DEFAULT. This only makes sense when SWT.WRAP is
   *          used.
   * @return A new {@link Label}.
   */
  public static Label label (Composite panel, String text, int columns,
                             int style)
  {
    Label label = new Label (panel, style);
    
    label.setText (text);
    
    if (columns != SWT.DEFAULT)
    {
      GridData data = new GridData ();
      
      data.widthHint = columns * DFC_SWT.getTextUnitWidth (panel);
      label.setLayoutData (data);
    }
    
    return label;
  }

  /**
   * Creates a {@link Spinner} in the given <code>parent</code> with its
   * own {@link Label} (accessible through {@link #getLabel(Control)}).
   * Works best when used in a {@link Composite} using a {@link GridLayout}
   * with two columns.
   * 
   * @param parent The parent {@link Composite}.
   * @param text The text to put in the leading {@link Label}.
   * @param min The minimum value the {@link Spinner} should accept.
   * @param max The maximum value the {@link Spinner} should accept.
   * @param decimalPlaces The number of decimal places to show.
   * @param style The SWT style bits to apply to the {@link Spinner}.
   * @return An appropriately configured {@link Spinner}, with its own
   *         {@link Label} object (findable through 
   *         {@link #getLabel(Control)}).
   */
  public static Spinner spinner (Composite parent, String text, int min, 
                                 int max, int decimalPlaces, int style)
  {
    Label label = new Label (parent, SWT.LEFT);
    label.setText (text);

    Spinner spinner = new Spinner (parent, style);
    spinner.setMaximum (max);
    spinner.setMinimum (min);
    spinner.setDigits (decimalPlaces);
    spinner.setData ("label", label);

    return spinner;
  }

  /**
   * Creates a {@link Slider} in the given <code>parent</code> with its
   * own {@link Label} (accessible through {@link #getLabel(Control)}).
   * Works best when used in a {@link Composite} using a {@link GridLayout}
   * with two columns.
   * 
   * @param parent The parent {@link Composite}.
   * @param text The text to put in the leading {@link Label}.
   * @param min The minimum value the {@link Slider} should accept.
   * @param max The maximum value the {@link Slider} should accept.
   * @param increment The step size between min and max.
   * @param style The SWT style bits to apply to the {@link Slider}.
   * @return An appropriately configured {@link Slider}, with its own
   *         {@link Label} object (findable through 
   *         {@link #getLabel(Control)}).
   */
  public static Slider slider (Composite parent, String text, int min, 
                               int max, int increment, int style)
  {
    Label label = new Label (parent, SWT.LEFT);
    label.setText (text);

    Slider slider = new Slider (parent, style);
    slider.setMaximum (max);
    slider.setMinimum (min);
    slider.setIncrement (increment);
    slider.setData ("label", label);

    return slider;
  }

  /**
   * Creates a {@link Slider} in the given <code>parent</code> with its
   * own {@link Label} (accessible through {@link #getLabel(Control)}).
   * Works best when used in a {@link Composite} using a {@link GridLayout}
   * with two columns.
   * 
   * @param parent The parent {@link Composite}.
   * @param text The text to put in the leading {@link Label}.
   * @param min The minimum value the {@link Scale} should accept.
   * @param max The maximum value the {@link Scale} should accept.
   * @param increment The step size between min and max.
   * @param style The SWT style bits to apply to the {@link Scale}.
   * @return An appropriately configured {@link Scale}, with its own
   *         {@link Label} object (findable through 
   *         {@link #getLabel(Control)}).
   */
  public static Scale scale (Composite parent, String text, int min, 
                               int max, int increment, int style)
  {
    Label label = new Label (parent, SWT.LEFT);
    label.setText (text);

    Scale scale = new Scale (parent, style);
    scale.setMaximum (max);
    scale.setMinimum (min);
    scale.setIncrement (increment);
    scale.setData ("label", label);

    return scale;
  }
}
