package dsto.dfc.swt.forms;

import dsto.dfc.util.InvalidFormatException;
import dsto.dfc.util.ITranslator;

import dsto.dfc.text.IStringTranslator;


/**
 * A pipe that outputs a transformed version of its input using a
 * {@link ITranslator}.
 * 
 * @author mpp
 * @version $Revision$
 */
public class TranslatorPipe extends AbstractBiDiPipe implements Pipe
{
  private ITranslator inputTranslator;
  private ITranslator outputTranslator;
  
  /**
   * Create a new instance using an {@link IStringTranslator} to
   * convert to string for input and back again for output.
   * 
   * @param translator The string translator.
   * 
   * @see IStringTranslator.ToString
   * @see IStringTranslator.FromString
   */
  public TranslatorPipe (IStringTranslator translator)
  {
    this (new IStringTranslator.ToString (translator),
           new IStringTranslator.FromString (translator));
  }
  
  /**
   * Create a new instance.
   * 
   * @param inputTranslator Translator for the input direction. May be
   *          null for no input (i.e. read-only).
   * @param outputTranslator Translator for the output direction. May be
   *          null for no input (i.e. write-only).
   */
  public TranslatorPipe (ITranslator inputTranslator,
                         ITranslator outputTranslator)
  {
    this.outputTranslator = outputTranslator;
    this.inputTranslator = inputTranslator;
  }
  
  public void dispose ()
  {
    outputTranslator = null;
    inputTranslator = null;
    
    super.dispose ();
  }
  
  public boolean isDisposed ()
  {
    return outputTranslator == null && inputTranslator == null;
  }
  
  public boolean input (Object value)
  {
    try
    {
      if (inputTranslator != null)
        childPipe.input (inputTranslator.translate (value));
      
      return true;
    } catch (InvalidFormatException ex)
    {
      handlePipeError (ex.getMessage ());
      
      return false;
    }
  }

  public boolean output (Object value)
  {
    try
    {
      if (outputTranslator != null)
        parent.output (outputTranslator.translate (value));
      
      return true;
    } catch (InvalidFormatException ex)
    {
      handlePipeError (ex.getMessage ());
      
      return false; 
    }
  }
}
