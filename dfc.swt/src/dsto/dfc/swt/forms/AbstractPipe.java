package dsto.dfc.swt.forms;

/**
 * Base class for the Pipe interface. Provides parent and error handling
 * implementations.
 * 
 * @author mpp
 * @version $Revision$
 */
public abstract class AbstractPipe implements Pipe
{
  protected Pipe parent;
  protected PipeErrorHandler errorHandler;
  
  /**
   * Default implementation: does nothing.
   */
  public void dispose ()
  {
    // zip
  }
  
  public void setParent (Pipe parent)
  {
    this.parent = parent;
  }

  public Pipe getParent ()
  {
    return parent;
  }
  
  public void setErrorHandler (PipeErrorHandler errorHandler)
  {
    this.errorHandler = errorHandler;
  }
  
  protected void handlePipeError (String message)
  {
    if (errorHandler != null)
      errorHandler.handlePipeError (this, message);
  }
}
