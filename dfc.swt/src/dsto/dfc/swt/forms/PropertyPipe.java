package dsto.dfc.swt.forms;

import org.eclipse.swt.widgets.Display;

import dsto.dfc.databeans.DataObjects;
import dsto.dfc.databeans.IDataObject;
import dsto.dfc.databeans.PropertyEvent;
import dsto.dfc.databeans.PropertyListener;
import dsto.dfc.databeans.PropertyPath;

/**
 * A pipe that inputs a data object and outputs the value of one of
 * its properties. Property changes originating from a thread other
 * than the current UI thread are rescheduled to be executed later in
 * the UI thread.
 * 
 * @author mpp
 * @version $Revision$
 * 
 * @see dsto.dfc.databeans.IDataBean
 */
public class PropertyPipe
  extends AbstractBiDiPipe implements Pipe, PropertyListener
{
  private PropertyPath path;
  private IDataObject object;
  private boolean changing;
  private Display display;
  
  /**
   * @param property Property for IO (may be a property path eg "person/name").
   * 
   * @see DataObjects#getValue(IDataObject, PropertyPath)
   */
  public PropertyPipe (String property)
  {
    this.path = new PropertyPath (property);    
    this.display = Display.getCurrent ();
  }
  
  public synchronized void dispose ()
  {
    if (path != null)
    {
      if (object != null)
      {
        object.removePropertyListener (this);
        object = null;
      }
      
      childPipe.dispose ();
      path = null;
    }
  }

  public boolean isDisposed ()
  {
    return path == null;
  }
  
  public boolean input (Object value)
  {
    if (value == null)
      throw new NullPointerException ("Object cannot be null");
    
    IDataObject oldObject = object;
      
    try
    {
      this.object = (IDataObject)value;
      
      if (object != null)
        childPipe.input (DataObjects.getValue (object, path));
      else
        childPipe.input (null);
      
      if (oldObject != null)
        oldObject.removePropertyListener (this);

      if (object != null)
        object.addPropertyListener (this);
        
      return true;
    } catch (ClassCastException ex)
    {
      handlePipeError ("Value is not a data object: " + ex.getMessage ());

      return false;
    }
  }

  public boolean output (Object value)
  {
    try
    {
      changing = true;
      
      DataObjects.setValue (object, path, value);
      
      return parent.output (object);
    } catch (RuntimeException ex)
    {
      IllegalStateException ex2 =
        new IllegalStateException
          ("Error in setting value for \"" + path + "\" value = " + value);
      
      ex2.initCause (ex);
      
      throw ex2;
    } finally
    {
      changing = false;
    }
  }
  
  public synchronized void propertyValueChanged (PropertyEvent e)
  {
    if (!isDisposed () && !changing && path.equals (e.path))
    {
      if (display.getThread () == Thread.currentThread ())
        childPipe.input (e.newValue);
      else
        display.asyncExec (new InputTask (e.newValue));
    }
  }
  
  private class InputTask implements Runnable
  {
    private final Object newValue;

    public InputTask (Object newValue)
    {
      this.newValue = newValue;
    }

    public void run ()
    {
      synchronized (PropertyPipe.this)
      {
        // since this is async, pipe may have been disposed in the meantime
        if (!isDisposed () && !childPipe.isDisposed ())
          childPipe.input (newValue);
      }
    }
  }
}
