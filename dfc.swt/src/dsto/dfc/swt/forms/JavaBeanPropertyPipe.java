package dsto.dfc.swt.forms;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;

import dsto.dfc.util.Beans;

/**
 * A pipe that inputs a data bean and outputs the value of one of its
 * properties.
 * 
 * @author mpp
 * @version $Revision$
 * 
 * @see dsto.dfc.databeans.IDataBean
 */
public class JavaBeanPropertyPipe
  extends AbstractBiDiPipe implements Pipe, PropertyChangeListener
{
  private String property;
  private Object bean;
  private boolean changing;

  public JavaBeanPropertyPipe (String property)
  {
    this.property = property;
  }
  
  public void dispose ()
  {
    if (bean != null)
    {
      Beans.removeListener (PropertyChangeListener.class, bean, this);
      bean = null;
      
      childPipe.dispose ();
    }
  }
  
  public boolean isDisposed ()
  {
    return bean == null;
  }

  public boolean input (Object value)
  {
    Object oldBean = bean;
      
    this.bean = value;
    
    try
    {
      if (bean != null)
        childPipe.input (Beans.getPropertyValue (bean, property));
      else
        childPipe.input (null);
    } catch (NoSuchMethodException ex)
    {
      handlePipeError ("Bean has no \"" + property + "\" property:" + ex.getMessage ());
      
      return false;
    }
    
    if (oldBean != null)
      Beans.removeListener (PropertyChangeListener.class, oldBean, this);

    if (bean != null)
      Beans.addListener (PropertyChangeListener.class, bean, this);
      
    return true;
  }

  public boolean output (Object value)
  {
    try
    {
      changing = true;
      
      Beans.setPropertyValue (bean, property, value);
      
      return parent.output (bean);
    } catch (NoSuchMethodException ex)
    {
      handlePipeError ("Bean has no \"" + property + "\" property:" + ex.getMessage ());
      
      return false;
    } catch (InvocationTargetException ex)
    {
      handlePipeError ("Error in property set method: "  + ex.getTargetException ());
      
      return false;
    } finally
    {
      changing = false;
    }
  }
  
  public void propertyChange (PropertyChangeEvent e)
  {
    if (!changing && e.getPropertyName ().equals (property))
      childPipe.input (e.getNewValue ());
  }
}
