package dsto.dfc.swt.forms;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;

import dsto.dfc.util.Beans;

/**
 * A pipe endpoint that outputs its input to bean property (ie via set/get 
 * methods on a target object).
 * 
 * @author mpp
 * @version $Revision$
 */
public class BeanEndpoint
  extends AbstractPipe implements PropertyChangeListener
{
  private Object bean;
  private String property;
  private boolean changed;
  private boolean changing;
  
  public BeanEndpoint (Object bean, String property)
  {
    this.bean = bean;
    this.property = property;
    
    Beans.addListener (PropertyChangeListener.class, this, bean);
  }
  
  public void dispose ()
  {
    Beans.removeListener (PropertyChangeListener.class, this, bean);
    
    bean = null;
  }
  
  public boolean isDisposed ()
  {
    return bean == null;
  }
  
  public boolean flush ()
  {
    boolean succeeded = true;
    
    if (changed)
    {
      try
      {
        succeeded = parent.output (Beans.getPropertyValue (bean, property));
        
        if (succeeded)
          changed = false;
      } catch (NoSuchMethodException ex)
      {
        handlePipeError ("Bean " + bean.getClass () + " has no \"" + property + "\" property");
        
        succeeded = false;
      }
    }
    
    return succeeded;
  }

  public Pipe connect (Pipe pipe)
  {
    throw new UnsupportedOperationException ();
  }
  
  public boolean input (Object value)
  {
    try
    {
      changing = true;
      
      Beans.setPropertyValue (bean, property, value);  
    } catch (NoSuchMethodException ex)
    {
      handlePipeError ("Bean " + bean.getClass () + " has no \"" + property + "\" property");
      
      return false;
    } catch (InvocationTargetException ex)
    {
      handlePipeError ("Error in property set method: "  + ex.getTargetException ());
      
      return false;
    } finally
    {
      changing = false;
    }
    
    changed = false;
    
    return true;
  }

  public boolean output (Object value)
  {
    return true;
  }
  
  public void propertyChange (PropertyChangeEvent e)
  {
    if (!changing)
      flush ();
  }
}
