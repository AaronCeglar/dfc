package dsto.dfc.swt.forms;

import java.lang.reflect.Method;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

/**
 * A pipe endpoint that connects its input to SWT text-like widgets
 * (Text, Combo, and Spinner currently). Has the ability to batch
 * changes in the text-like widget rather than sending them through
 * individually.
 * 
 * @author mpp
 * @version $Revision$
 */
public class TextEndpoint extends PipeEndpoint implements Listener
{
  /**
   * Only commit changes on a change of focus or a default selection
   * (e.g. hitting the return key) in the text field.
   */
  public static int COMMIT_DEFAULT = 0;

  /**
   * Commit on every change to the text.
   */
  public static int COMMIT_IMMEDIATELY = 1;

  /**
   * Commit changes after a period of inactivity of {@link #delay}
   * milliseconds.
   */
  public static int COMMIT_LATER = 2;

  /**
   * Controls when to commit text changes to the parent pipe. Default:
   * COMMIT_DEFAULT. Possible values: COMMIT_DEFAULT,
   * COMMIT_IMMEDIATELY, COMMIT_LATER.
   */
  public int commitMode;

  /**
   * Used when commitMode == COMMIT_LATER to control how long a period
   * of inactivity should pass before committing changes to the
   * parent.
   */
  public int delay = 500;

  private Control textControl;
  private boolean changed;
  private boolean handleModify; // used to short-circuit our own text changes
  private FlushTask currentFlushTask;

  public TextEndpoint (Control textControl)
  {
    this.textControl = textControl;
    this.handleModify = true;
    this.commitMode = COMMIT_DEFAULT;
    
    textControl.addListener (SWT.Modify, this);
    textControl.addListener (SWT.DefaultSelection, this);
    textControl.addListener (SWT.FocusOut, this);
  }

  public void dispose ()
  {
    if (textControl != null && !textControl.isDisposed ())
    {
      textControl.removeListener (SWT.Modify, this);
      textControl.removeListener (SWT.DefaultSelection, this);
      textControl.removeListener (SWT.FocusOut, this);
    }

    textControl = null;
  }

  public boolean isDisposed ()
  {
    return textControl == null;
  }
  
  public boolean flush ()
  {
    boolean succeeded = true;

    if (changed)
    {
      succeeded = parent.output (getText ());

      if (succeeded)
        changed = false;
    }

    return succeeded;
  }

  public void flushLater ()
  {
    if (currentFlushTask != null)
      currentFlushTask.cancel (); // cancel the old one
    currentFlushTask = new FlushTask ();
    textControl.getDisplay ().timerExec (delay, currentFlushTask);
  }

  public boolean input (Object value)
  {
    if (value == null)
      setText ("");
    else
      setText (value.toString ());

    changed = false;

    return true;
  }

  public void setText (String text)
  {
    handleModify = false;

    try
    {
      if (textControl instanceof Text)
        ((Text) textControl).setText (text);
      else if (textControl instanceof Spinner)
        ((Spinner) textControl).setSelection (Integer.parseInt (text));
      else if (textControl instanceof StyledText)
        ((StyledText) textControl).setText (text);
      else if (textControl instanceof Combo)
        ((Combo) textControl).setText (text);
      else if (textControl instanceof Label)
        ((Label) textControl).setText (text);
      else if (textControl == null)
        throw new IllegalStateException ("Text endpoint is disposed");
      else
        duckSetText (text);
    } finally
    {
      handleModify = true;
    }
  }

  private void duckSetText (String text)
  {
    try
    {
      Method setText = 
        textControl.getClass ().getMethod ("setText", String.class);
      
      setText.invoke (textControl, text);
    } catch (Exception ex)
    {
      throw new IllegalStateException ("Don't know how to set text on " +
                                        textControl.getClass () + ": " + ex);
    }
  }

  public String getText ()
  {
    if (textControl instanceof Text)
      return ((Text) textControl).getText ();
    else if (textControl instanceof Spinner)
      return Integer.toString (((Spinner) textControl).getSelection ());
    else if (textControl instanceof StyledText)
      return ((StyledText) textControl).getText ();
    else if (textControl instanceof Combo)
      return ((Combo) textControl).getText ();
    else if (textControl == null)
      throw new IllegalStateException ("Text endpoint is disposed");
    else
      throw new IllegalStateException ("Don't know how to get text from "
          + textControl.getClass ());
  }

  public void handleEvent (Event e)
  {
    switch (e.type)
    {
      case SWT.FocusOut:
      case SWT.DefaultSelection:
        flush ();
        break;

      case SWT.Modify:
        if (handleModify)
        {
          changed = true;

          if (commitMode == TextEndpoint.COMMIT_IMMEDIATELY)
            flush ();
          else if (commitMode == TextEndpoint.COMMIT_LATER)
            flushLater ();
        }

        break;
    }
  }

  class FlushTask implements Runnable
  {
    private boolean isCancelled = false;

    public synchronized void cancel ()
    {
      isCancelled = true;
    }

    public synchronized boolean isCancelled ()
    {
      return isCancelled;
    }

    public void run ()
    {
      if (!isCancelled ())
        flush ();
    }
  }
}
