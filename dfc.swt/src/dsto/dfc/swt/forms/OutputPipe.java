package dsto.dfc.swt.forms;

/**
 * @author mpp
 * @version $Revision$
 */
public interface OutputPipe
{
  public void output (Object value);
  
  public void setParent (Pipe pipe);
  
  public Pipe getParent ();
}
