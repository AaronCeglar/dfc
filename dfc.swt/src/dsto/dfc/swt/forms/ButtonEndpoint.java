package dsto.dfc.swt.forms;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;

/**
 * A pipe endpoint that connects a boolean input to the selected state of a SWT
 * button (usually a checkbox or toggle button).
 * 
 * @author mpp
 * @version $Revision$
 */
public class ButtonEndpoint extends PipeEndpoint implements SelectionListener
{
  private Button button;
  private boolean selected;

  public ButtonEndpoint (Button button)
  {
    this.button = button;
    
    button.addSelectionListener (this);
  }
  
  public void dispose ()
  {
    if (button != null && !button.isDisposed ())
      button.removeSelectionListener (this);

    button = null;
    
    super.dispose();
  }
  
  public boolean isDisposed ()
  {
    return button == null;
  }

  public boolean input (Object value)
  {
    try
    {
      selected = (value == null) ? false : ((Boolean)value).booleanValue ();
      
      button.setSelection (selected);
      
      return true;
    } catch (ClassCastException ex)
    {
      handlePipeError ("\"" + value + "\" is not boolean");
      
      return false;
    }
  }

  public boolean flush ()
  {
    if (button.getSelection () != selected)
    {
      selected = !selected;
      
      return parent.output (selected ? Boolean.TRUE : Boolean.FALSE);
    } else
    {
      return true;
    }
  }
  
  public void widgetDefaultSelected (SelectionEvent e)
  {
    // zip
  }

  public void widgetSelected (SelectionEvent e)
  {
    flush ();
  }
}
