package dsto.dfc.swt.forms;

import dsto.dfc.util.Disposable;

/**
 * Defines a segment of a data flow pipe. A pipe accepts an input value
 * (via {@link #input(Object)}), optionally performs some translation, and
 * passes the (possibly translated) value as the input to one or more child
 * pipes that are connected to it (via {@link #connect(Pipe)}). The child pipes
 * may later output values back into the parent (via {@link #output(Object)}).<p>
 * 
 * Notes:
 * <ul>
 *   <li>A set of pipes always forms a tree - each pipe may only have one
 *       parent.
 *   <li>Some pipes may not not accept output values from children: eg the 
 *       multiplex pipe.
 * </ul>
 * 
 * @author mpp
 * @version $Revision$
 */
public interface Pipe extends Disposable
{
  /**
   * Cause any uncommitted changes to any pipe segments in the tree to be
   * pushed back into the pipe sequence (using {@link #output(Object)}). This
   * is designed for use by pipes connect editors such as text fields that are
   * not committed immediately: most pipes will simply forward this call on to
   * their children. 
   * 
   * @return True if the operation completed successfully.
   * 
   * @see #setErrorHandler(PipeErrorHandler) for more info on handling errors.
   */
  public boolean flush ();
  
  /**
   * Input a value into the pipe tree.
   * 
   * @return True if the operation completed successfully.
   * 
   * @see #setErrorHandler(PipeErrorHandler) for more info on handling errors.
   */
  public boolean input (Object value);
  
  /**
   * Output a value from a child into the pipe (the reverse of input ()).
   * 
   * @return True if the operation completed successfully.
   * 
   * @see #setErrorHandler(PipeErrorHandler) for more info on handling errors.
   */
  public boolean output (Object value);
  
  /**
   * Set the parent pipe. The child should push new values back up the chain
   * by calling its parent's {@link #output(Object)} method.
   */
  public void setParent (Pipe pipe);
  
  /**
   * Get the parent pipe.
   * 
   * @see #setParent(Pipe)
   */
  public Pipe getParent ();
  
  /**
   * Connect the output of this pipe to the input of a child pipe.
   */
  public Pipe connect (Pipe pipe);
  
  /**
   * Set the handler for any errors encountered while loading/unloading the 
   * pipe. This will also set the handler for all children.
   * 
   * @see PipeErrorHandler
   */
  public void setErrorHandler (PipeErrorHandler handler);
  
  /**
   * Release any resources acquired by the pipe.
   */
  public void dispose ();
  
  /**
   * Test if pipe has been disposed.
   */
  public boolean isDisposed ();
}
