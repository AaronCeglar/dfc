package dsto.dfc.swt.forms;

/**
 * @author mpp
 * @version $Revision$
 */
public interface InputPipe
{
  public void input (Object value);
}
