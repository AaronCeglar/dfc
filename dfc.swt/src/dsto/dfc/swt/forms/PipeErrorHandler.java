package dsto.dfc.swt.forms;

/**
 * @author mpp
 * @version $Revision$
 */
public interface PipeErrorHandler
{
  public void handlePipeError (Pipe source, String message);
}
