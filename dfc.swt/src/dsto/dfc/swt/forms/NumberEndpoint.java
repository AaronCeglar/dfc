/**
 * 
 */
package dsto.dfc.swt.forms;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Spinner;

/**
 * A pipe endpoint that connects its input to SWT number-like widgets
 * (Scale, Slider, and Spinner currently). Has the ability to batch
 * changes in the text-like widget rather than sending them through
 * individually.
 * 
 * @author WeberD
 * @created 25/09/2006
 * @version $Revision$
 */
public class NumberEndpoint extends PipeEndpoint implements SelectionListener
{
  /**
   * Commit changes as soon as the selection changes. May result in lots of
   * events.
   */
  public static int COMMIT_DEFAULT = 0;

  /**
   * Commit changes after a period of inactivity of {@link #delay} milliseconds.
   */
  public static int COMMIT_LATER = 1;

  /**
   * Controls when to commit changes to the parent pipe. Default:
   * COMMIT_DEFAULT. Possible values: COMMIT_DEFAULT, or COMMIT_LATER.
   */
  public int commitMode;

  /**
   * Used when <code>commitMode == COMMIT_LATER</code> to control how long a
   * period of inactivity should pass before committing changes to the parent.
   */
  public int delay;

  private Control numberControl;
  private int selection;
  private FlushTask currentFlushTask;

  public NumberEndpoint (Control scale)
  {
    this (scale, COMMIT_DEFAULT);
  }
  
  public NumberEndpoint (Control scale, int commitMode)
  {
    this.numberControl = scale;
    this.commitMode = commitMode;
    this.delay = 500;
    
    addSelectionListener (scale, this);
  }

  public void dispose ()
  {
    if (numberControl != null && !numberControl.isDisposed ())
      removeSelectionListener (numberControl, this);

    numberControl = null;

    super.dispose ();
  }
  
  public boolean isDisposed ()
  {
    return numberControl == null;
  }

  public boolean input (Object value)
  {
    try
    {
      if (value == null)
        selection = 0;
      else
        selection = ((Number) value).intValue ();

      setSelection (numberControl, selection);

      return true;
    }
    catch (ClassCastException ex)
    {
      handlePipeError ("\"" + value + "\" is not a number");

      return false;
    }
  }

  public boolean flush ()
  {
    if (getSelection (numberControl) != selection)
    {
      selection = getSelection (numberControl);

      return parent.output (new Integer (selection));
    }
    return true;
  }

  public void flushLater ()
  {
    if (currentFlushTask != null)
      currentFlushTask.cancel (); // cancel the old one
    currentFlushTask = new FlushTask ();
    numberControl.getDisplay ().timerExec (delay, currentFlushTask);
  }

  public void widgetDefaultSelected (SelectionEvent e)
  {
    // zip
  }

  public void widgetSelected (SelectionEvent e)
  {
    if (commitMode == NumberEndpoint.COMMIT_DEFAULT)
      flush ();
    else if (commitMode == NumberEndpoint.COMMIT_LATER)
      flushLater ();
  }

  private void addSelectionListener (Control widget, SelectionListener listener)
  {
    if (widget instanceof Scale)
      ((Scale) widget).addSelectionListener (listener);
    else if (widget instanceof Spinner)
      ((Spinner) widget).addSelectionListener (listener);
    else if (widget instanceof Slider)
      ((Slider) widget).addSelectionListener (listener);
  }

  private void removeSelectionListener (Control widget,
      SelectionListener listener)
  {
    if (widget instanceof Scale)
      ((Scale) widget).removeSelectionListener (listener);
    else if (widget instanceof Spinner)
      ((Spinner) widget).removeSelectionListener (listener);
    else if (widget instanceof Slider)
      ((Slider) widget).removeSelectionListener (listener);
  }

  private void setSelection (Control widget, int value)
  {
    if (widget instanceof Scale)
      ((Scale) widget).setSelection (value);
    else if (widget instanceof Spinner)
      ((Spinner) widget).setSelection (value);
    else if (widget instanceof Slider)
      ((Slider) widget).setSelection (value);
  }

  private int getSelection (Control widget)
  {
    if (widget instanceof Scale)
      return ((Scale) widget).getSelection ();
    else if (widget instanceof Spinner)
      return ((Spinner) widget).getSelection ();
    else if (widget instanceof Slider)
      return ((Slider) widget).getSelection ();

    return -1;
  }

  class FlushTask implements Runnable
  {
    private boolean isCancelled = false;

    public synchronized void cancel ()
    {
      isCancelled = true;
    }

    public synchronized boolean isCancelled ()
    {
      return isCancelled;
    }

    public void run ()
    {
      if (!isCancelled ())
        flush ();
    }
  }
}
