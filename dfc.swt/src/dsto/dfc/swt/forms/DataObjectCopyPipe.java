package dsto.dfc.swt.forms;

import dsto.dfc.databeans.DataObjects;
import dsto.dfc.databeans.IDataObject;
import dsto.dfc.databeans.PropertyEvent;
import dsto.dfc.databeans.PropertyListener;

/**
 * A pipe that inputs a data object and outputs a clone. Subsequent flush ()'es
 * will trigger an output to the parent pipe only if the object has changed.
 * This can be used to make bean property changes "transactional" ie not
 * committed until a flush ().
 * 
 * @author mpp
 * @version $Revision$
 * 
 * @see dsto.dfc.databeans.IDataBean
 */
public class DataObjectCopyPipe
  extends AbstractBiDiPipe implements Pipe, PropertyListener
{
  private IDataObject object;
  private boolean changed;
  
  public void dispose ()
  {
    if (object != null)
    {
      object.removePropertyListener (this);
      object = null;
      
      childPipe.dispose ();
    }
  }
  
  public boolean isDisposed ()
  {
    return object == null;
  }

  public boolean flush ()
  {
    boolean succeeded = true;
    
    if (changed)
    {
      IDataObject copy;
      
      if (object == null)
        copy = null;
      else
        copy = DataObjects.deepClone (object);
      
      // output a copy to avoid parent seeing any later changes from children
      succeeded = parent.output (copy);
      
      if (succeeded)
        changed = false;
    }
    
    return succeeded;
  }
  
  public boolean input (Object value)
  {
    try
    {
      setBean ((IDataObject)value);
      
      childPipe.input (object);
      
      return true;
    } catch (ClassCastException ex)
    {
      handlePipeError ("Value is not a data object: " + ex.getMessage ());
      
      return false;
    }
  }

  public boolean output (Object value)
  {
    setBean ((IDataObject)value);

    return parent.output (value);
  }
  
  public void propertyValueChanged (PropertyEvent e)
  {
    changed = true;
  }
  
  private void setBean (IDataObject value)
  {    
    IDataObject oldBean = object;
    
    if (oldBean != value)
    {  
      if (value == null)
        object = null;
      else
        object = DataObjects.deepClone (value);
      
      if (oldBean != null)
        oldBean.removePropertyListener (this);
  
      if (object != null)
        object.addPropertyListener (this);
      
      changed = false;
    }
  }
}
