package dsto.dfc.swt.forms;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import dsto.dfc.swt.icons.ValueLabel;

/**
 * A pipe endpoint that allows selection of a value from a list of radio
 * buttons.
 * 
 * @author mpp
 * @version $Revision$
 */
public class RadioEndpoint extends PipeEndpoint implements SelectionListener
{
  private Object value;
  private Button [] buttons;
  private boolean loading;
  
  /**
   * Create a new instance.
   * 
   * @param parent The parent control to place the radio buttons in.
   * @param valueLabels An array of value/label pairs corresponding to
   *          the radio button labels/data values.
   */
  public RadioEndpoint (Composite parent, ValueLabel [] valueLabels)
  {
    this.buttons = new Button [valueLabels.length];

    for (int i = 0; i < valueLabels.length; i++)
    {
      Button button = new Button (parent, SWT.RADIO);
      button.setText (valueLabels [i].label);
      button.setData (valueLabels [i].value);
      
      button.addSelectionListener (this);
      
      buttons [i] = button;
    }
  }
  
  public void dispose ()
  {
    if (buttons != null)
    {
      for (int i = 0; i < buttons.length; i++)
        buttons [i].removeSelectionListener (this);
      
      buttons = null;
    }
    
    super.dispose ();
  }
  
  public boolean isDisposed ()
  {
    return buttons == null;
  }
  
  public void focus ()
  {
    buttons [0].forceFocus ();
  }
  
  /**
   * Get the selected value.
   */
  public Object getValue ()
  {
    return value;
  }
  
  public boolean input (Object newValue)
  {
    this.value = newValue;
    
    selectValue ();
    
    return true;
  }

  private void selectValue ()
  {
    loading = true;
    
    for (int i = 0; i < buttons.length; i++)
    {
      Button button = buttons [i];
      
      button.setSelection (button.getData ().equals (value));
    }
    
    loading = false;
  }

  public boolean flush ()
  {
    return true;
  }
  
  public Button getButton (int index)
  {
    return buttons [index];
  }
  
  public void widgetSelected (SelectionEvent e)
  {
    if (loading)
      return;
      
    value = ((Button)e.getSource ()).getData ();
    
    // don't seem to need this: buttons automagically ensure only one is selected
    //    selectValue ();
    
    if (parent != null)
      parent.output (value);
  }

  public void widgetDefaultSelected (SelectionEvent e)
  {
    // zip
  }
}
