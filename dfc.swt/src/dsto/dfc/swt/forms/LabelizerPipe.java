package dsto.dfc.swt.forms;

import dsto.dfc.util.Objects;

import dsto.dfc.swt.icons.ValueLabel;

/**
 * A pipe that turns a value into a UI label and vice-versa. This can
 * be used as an in-between translator to turn enumerated values e.g.
 * ["on", "off", null] into user-presentable ones like ["On", "Off",
 * "Invalid!"]. This control's label/value mapping is compatible with
 * {@link dsto.dfc.swt.forms.RadioEndpoint}'s.
 * 
 * @author Matthew Phillips
 */
public class LabelizerPipe extends AbstractBiDiPipe
{
  /** Set to true (default) to enable ignoring "&amp;" in labels. */
  public boolean hideShortcuts;  
  public ValueLabel [] labelsAndValues;

  /**
   * Create a new instance.
   * 
   * @param labelsAndValues The control's mapping of labels to values.
   *          eg {{"On", "on"}, {"Off", "off"}, {null, "[invalid]"}}.
   *          This mapping is compatible with
   *          {@link dsto.dfc.swt.forms.RadioEndpoint}'s. In
   *          particular, the labels may have "&amp;" in them which
   *          will be ignored.
   * 
   * @see RadioEndpoint
   * @see #hideShortcuts
   */
  public LabelizerPipe (ValueLabel [] labelsAndValues)
  {
    this.labelsAndValues = labelsAndValues;
    this.hideShortcuts = true;
  }
  
  public void dispose ()
  {
    labelsAndValues = null;
    
    super.dispose ();
  }
  
  public boolean isDisposed ()
  {
    return labelsAndValues == null;
  }

  public boolean input (Object value)
  {
    for (int i = 0; i < labelsAndValues.length; i++)
    {
      ValueLabel valueLabel = labelsAndValues [i];
      
      if (Objects.objectsEqual (valueLabel.value, value))
      {
        String label;
        
        if (hideShortcuts)
          label = valueLabel.getPlainLabel ();
        else
          label = valueLabel.label;
        
        childPipe.input (label);
        
        return true;
      }
    }
    
    return false;
  }

  public boolean output (Object value)
  {
    for (int i = 0; i < labelsAndValues.length; i++)
    {
      ValueLabel labelAndValue = labelsAndValues [i];
      
      String label;
      
      if (hideShortcuts)
        label = labelAndValue.getPlainLabel ();
      else
        label = labelAndValue.label;
      
      if (Objects.objectsEqual (label, value))
      {
        childPipe.output (labelAndValue.value);
        
        return true;
      }
    }
    
    return false;
  }
}
