package dsto.dfc.swt.forms;

/**
 * Abstract base for pipes that are bi-directional (accept inputs and outputs).
 * Subclasses may just implement input () and output ().
 * 
 * @author mpp
 * @version $Revision$
 */
public abstract class AbstractBiDiPipe extends AbstractPipe implements Pipe
{
  protected Pipe childPipe;

  public Pipe connect (Pipe newChildPipe)
  {
    this.childPipe = newChildPipe;
    
    newChildPipe.setParent (this);
    
    if (errorHandler != null)
      newChildPipe.setErrorHandler (errorHandler);

    return newChildPipe;
  }
  
  /**
   * Forwarded to the child pipe.
   */
  public boolean flush ()
  {
    return childPipe.flush ();
  }
  
  /**
   * Forwarded to the child pipe.
   */
  public void dispose ()
  {
    childPipe.dispose ();
  }
  
  /**
   * Calls superclass methis and sets the error handler for the the child pipe.
   */
  public void setErrorHandler (PipeErrorHandler handler)
  {
    super.setErrorHandler (handler);
    
    if (childPipe != null)
      childPipe.setErrorHandler (handler);
  }
}
