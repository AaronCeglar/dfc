package dsto.dfc.swt;

import java.lang.reflect.Method;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.FontMetrics;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import dsto.dfc.logging.Log;

/**
 * General DFC additions to SWT.
 * 
 * @author mpp
 */
public final class DFC_SWT
{ 
  public final static RGB WHITE   = new RGB(255, 255, 255);
  public final static RGB LIGHT_GRAY = new RGB(192, 192, 192);
  public final static RGB GRAY  = new RGB(128, 128, 128);
  public final static RGB DARK_GRAY   = new RGB(64, 64, 64);
  public final static RGB BLACK   = new RGB(0, 0, 0);
  public final static RGB RED   = new RGB(255, 0, 0);
  public final static RGB PINK  = new RGB(255, 175, 175);
  public final static RGB ORANGE  = new RGB(255, 200, 0);
  public final static RGB YELLOW  = new RGB(255, 255, 0);
  public final static RGB GREEN   = new RGB(0, 255, 0);
  public final static RGB MAGENTA = new RGB(255, 0, 255);
  public final static RGB CYAN  = new RGB(0, 255, 255);
  public final static RGB BLUE  = new RGB(0, 0, 255);

  private DFC_SWT ()
  {
    // zip
  }
  
  /**
   * Method isMiddleButton.
   * @param e
   * @return boolean
   */
  public static boolean isMiddleButton (MouseEvent e)
  {
    /** @todo do this properly for 2 button mouse */
    return e.button == getMiddleButton ();
  }
  
  public static boolean isMiddleButton (Event e)
  {
    /** @todo do this properly for 2 button mouse */
    return e.button == getMiddleButton ();
  }

  /**
   * Method isMiddleButton.
   * @param e
   * @return boolean
   */
  public static boolean isLeftButton (MouseEvent e)
  {
    /** @todo do this properly for 2 button mouse */
    return e.button == getLeftButton ();
  }
  
  public static boolean isLeftButton (Event e)
  {
    /** @todo do this properly for 2 button mouse */
    return e.button == getLeftButton ();
  }
  
  /**
   * Method isMiddleButton.
   * @param e
   * @return boolean
   */
  public static boolean isRightButton (MouseEvent e)
  {
    /** @todo do this properly for 2 button mouse */
    return e.button == getRightButton ();
  }
  
  public static boolean isRightButton (Event e)
  {
    /** @todo do this properly for 2 button mouse */
    return e.button == getRightButton ();
  }
  
  public static int getLeftButton ()
  {
    return 1;
  }
  
  public static int getMiddleButton ()
  {
    return 2;
  }
  
  public static int getRightButton ()
  {
    return 3;
  }
  
  /**
   * Get the basic text unit size for a given control (ie the average size
   * of a character in the current font).
   */
  public static Point getTextUnitSize (Control control)
  {
    GC gc = new GC (control);
   
    gc.setFont (control.getFont ());
    FontMetrics fontMetrics = gc.getFontMetrics ();
    gc.dispose ();
    
    return new Point (fontMetrics.getAverageCharWidth (),
                      fontMetrics.getHeight () + fontMetrics.getAscent () +
                        fontMetrics.getLeading ());
  }
  
  /**
   * Get the basic text width for a given control (ie the average size
   * of a character in the current font).
   */
  public static int getTextUnitWidth (Control control)
  {
    GC gc = new GC (control);
   
    gc.setFont (control.getFont ());
    FontMetrics fontMetrics = gc.getFontMetrics ();
    gc.dispose ();
    
    return fontMetrics.getAverageCharWidth ();
  }
  
  /**
   * Get the basic text height for a given control (ie the height
   * of a character in the current font).
   */
  public static int getTextUnitHeight (Control control)
  {
    GC gc = new GC (control);
   
    gc.setFont (control.getFont ());
    FontMetrics fontMetrics = gc.getFontMetrics ();
    gc.dispose ();
    
    return fontMetrics.getHeight ();
  }
  
  /**
   * Recursively set a tree of controls enabled state.
   */
  public static void setEnabled (Control control, boolean enabled)
  {
    control.setEnabled (enabled);
    
    if (control instanceof Composite)
    {
      Control [] children = ((Composite)control).getChildren ();
      
      for (int i = 0; i < children.length; i++)
        setEnabled (children [i], enabled); 
    }
  }
  
  public static float [] RGBtoHSB (RGB rgb)
  {
    float [] hsbvals = new float [3];
    float hue, saturation, brightness;
    
    int cmax = (rgb.red > rgb.green) ? rgb.red : rgb.green;
    if (rgb.blue > cmax) cmax = rgb.blue;
    
    int cmin = (rgb.red < rgb.green) ? rgb.red : rgb.green;
    if (rgb.blue < cmin) cmin = rgb.blue;
  
    brightness = cmax / 255.0f;
    
    if (cmax != 0)
      saturation = ((float) (cmax - cmin)) / ((float) cmax);
    else
      saturation = 0;

    if (saturation == 0)
    {
      hue = 0;
    } else
    {
      float redc = ((float) (cmax - rgb.red)) / ((float) (cmax - cmin));
      float greenc = ((float) (cmax - rgb.green)) / ((float) (cmax - cmin));
      float bluec = ((float) (cmax - rgb.blue)) / ((float) (cmax - cmin));
      
      if (rgb.red == cmax)
        hue = bluec - greenc;
      else if (rgb.green == cmax)
        hue = 2.0f + redc - bluec;
      else
        hue = 4.0f + greenc - redc;
 
      hue = hue / 6.0f;
      
      if (hue < 0)
        hue = hue + 1.0f;
    }
    
    hsbvals [0] = hue;
    hsbvals [1] = saturation;
    hsbvals [2] = brightness;
    
    return hsbvals;
  }
  
  public static RGB HSBtoRGB (float [] hsb)
  {
    // SWT's RGB uses hue of 0 - 360, ours uses 0.0 - 1.0
    return new RGB (hsb [0] * 360, hsb [1], hsb [2]);
  }

  /**
   * Shortcut to do the SWT MessageBox dance slightly more concisely.
   * 
   * @param shell The parent shell.
   * @param style The dialog style.
   * @param title The dialog title.
   * @param message The message to display.
   * @return The result of calling {@link MessageBox#open()}.
   */
  public static int messageBox (Shell shell, int style,
                                 String title, String message)
  {
    MessageBox box = new MessageBox (shell, style);
    
    box.setText (title);
    box.setMessage (message);
    
    return box.open ();
  }

  /**
   * Shortcut to do the SWT MessageBox dance slightly more concisely.
   * Includes an error message for an exception.
   * 
   * @param shell The parent shell.
   * @param style The dialog style.
   * @param title The dialog title.
   * @param message The message to display.
   * @param ex The exception that triggered the dialog. The message
   *          will be extended with an error line.
   * @return The result of calling {@link MessageBox#open()}.
   */
  public static int messageBox (Shell shell, int style,
                                 String title, String message, Throwable ex)
  {
    return messageBox (shell, style, title,
                       message + "\n\nError was: " + ex.getMessage ());
  }

  /**
   * For some reason StyledText in SWT 3.2 has a non-public setMargins ()
   * method. This works around it until it gets fixed.
   * 
   * @param styledText
   * @param leftMargin
   * @param topMargin
   * @param rightMargin
   * @param bottomMargin
   * 
   * @todo retire when setMargins () is fixed.
   */
  public static void setMargins (StyledText styledText,
                                  int leftMargin, int topMargin,
                                  int rightMargin, int bottomMargin)
  {
    try
    {
      Method setMargins =
        styledText.getClass ().getDeclaredMethod
          ("setMargins",
           new Class [] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE});
      
      setMargins.setAccessible (true);
      
      setMargins.invoke
        (styledText,
         new Object [] {new Integer (leftMargin), new Integer (topMargin),
                        new Integer (rightMargin), new Integer (bottomMargin)});
    } catch (Exception ex)
    {
      // zip
      Log.diagnostic ("Failed to set text margins", DFC_SWT.class, ex);
    }
  }

  /**
   * Simulate a button click event.
   */
  public static void clickButton (Button button)
  {
    Event event = new Event ();
    event.widget = button;
    event.type = SWT.Selection;
    
    button.notifyListeners (SWT.Selection, event);
  }
}
