package dsto.dfc.swt.icons;

import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;

/**
 * Defined an object that can render an iconic image.
 * 
 * <p>Note: icons that may be cached using {@link dsto.dfc.swt.icons.IconCache}
 * must also implement equals () and hashCode ().
 * 
 * @author mpp
 */
public interface Icon
{
  public int getIconWidth ();
  
  public int getIconHeight ();
  
  public void paintIcon (GC gc, int x, int y);
  
  public Image createImage (Device device);
  
  public ImageData getImageData ();
}
