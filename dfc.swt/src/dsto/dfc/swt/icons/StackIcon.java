package dsto.dfc.swt.icons;

import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;

import dsto.dfc.swt.util.Geometry;

/**
 * An icon that stacks one icon on another.
 */
public class StackIcon implements Icon
{
  public static final int TOP_LEFT = Geometry.TOP_LEFT;
  public static final int MID_TOP = Geometry.MID_TOP;
  public static final int TOP_RIGHT = Geometry.TOP_RIGHT;
  public static final int MID_RIGHT = Geometry.MID_RIGHT;
  public static final int BOTTOM_RIGHT = Geometry.BOTTOM_RIGHT;
  public static final int MID_BOTTOM = Geometry.MID_BOTTOM;
  public static final int BOTTOM_LEFT = Geometry.BOTTOM_LEFT;
  public static final int MID_LEFT = Geometry.MID_LEFT;
  public static final int CENTER = Geometry.CENTER;

  protected Icon lowerIcon;
  protected Icon upperIcon;
  protected int alignment;
  protected ImageData imageData;

  /**
   * Make an icon that consists of one icon overlayed on another.
   * The upper icon is displayed centered on the lower.
   *
   * @param lowerIcon The lower icon.
   * @param upperIcon The upper icon.
   */
  public StackIcon (Icon lowerIcon, Icon upperIcon)
  {
    this (lowerIcon, upperIcon, CENTER);
  }
  
  /**
   * Make an icon that consists of one icon overlayed on another.
   *
   * @param lowerIcon The lower icon.
   * @param upperIcon The upper icon.
   * @param alignment The alignment of the lower icon relative to the
   * upper. One of the TOP_LEFT, TOP_RIGHT, CENTER, etc constants.
   */
  public StackIcon (Icon lowerIcon, Icon upperIcon, int alignment)
  {
    this.lowerIcon = lowerIcon;
    this.upperIcon = upperIcon;
    this.alignment = alignment;
  }

  public Icon getLowerIcon ()
  {
    return lowerIcon;
  }

  public void setLowerIcon (Icon newValue)
  {
    lowerIcon = newValue;
  }

  public Icon getUpperIcon ()
  {
    return upperIcon;
  }

  public void setUpperIcon (Icon newValue)
  {
    upperIcon = newValue;
  }

  public void paintIcon (GC gc, int x, int y)
  {
    Point lowerSize = new Point (lowerIcon.getIconWidth (), lowerIcon.getIconHeight ());
    Point upperSize = new Point (upperIcon.getIconWidth (), upperIcon.getIconHeight ());

    Point upperOffset =
      Geometry.offsetForAlignment (lowerSize, upperSize, alignment);

    lowerIcon.paintIcon (gc, x, y);
    upperIcon.paintIcon (gc, x + upperOffset.x, y + upperOffset.y);
  }

  public Image createImage (Device device)
  {
    return Images.createImage (device, getImageData ());
  }
  
  public ImageData getImageData ()
  {
    if (imageData == null)
    {
      imageData =
        Images.createBlankImageData
          (lowerIcon.getIconWidth (), lowerIcon.getIconHeight ());
      
      Point lowerSize = new Point (lowerIcon.getIconWidth (), lowerIcon.getIconHeight ());
      Point upperSize = new Point (upperIcon.getIconWidth (), upperIcon.getIconHeight ());
  
      Point upperOffset =
        Geometry.offsetForAlignment (lowerSize, upperSize, alignment);
  
      Images.mergeIcons (imageData, lowerIcon.getImageData (), 0, 0);
      Images.mergeIcons (imageData, upperIcon.getImageData (),
                         upperOffset.x, upperOffset.y);
      
      Images.finalizePalette (imageData.palette);
    }
    
    return imageData;
  }
  
  public int getIconWidth ()
  {
    return Math.max (lowerIcon.getIconWidth (), upperIcon.getIconWidth ());
  }

  public int getIconHeight ()
  {
    return Math.max (lowerIcon.getIconHeight (), upperIcon.getIconHeight ());
  }
  
  public boolean equals (Object obj)
  {
    if (obj instanceof StackIcon)
      return equals ((StackIcon)obj);
    else
      return false;
  }
  
  public boolean equals (StackIcon icon)
  {
    return icon.lowerIcon.equals (lowerIcon) && icon.upperIcon.equals (upperIcon);
  }
  
  public int hashCode ()
  {
    return lowerIcon.hashCode () ^ upperIcon.hashCode ();
  }
  
  public String toString()
  {
    return "StackIcon [lower=" + lowerIcon + " upper=" + upperIcon + "]";
  }
}
