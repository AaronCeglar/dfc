package dsto.dfc.swt.icons;

import java.io.Serializable;

import dsto.dfc.util.EnumerationValue;
import dsto.dfc.util.StringEnumerationValue;

/**
 * @author mpp
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public abstract class IconicEnumerationValue
  extends StringEnumerationValue implements Iconic, Serializable
{
	private static final long serialVersionUID = 1l;
	
	private Icon icon;
	
	/**
	 * To support beans serialization only.
	 */
	protected IconicEnumerationValue ()
	{
		this (null, (Icon)null);
	}
	
  public IconicEnumerationValue (String name, String iconResource)
  {
    this (name, null, Images.createIcon (iconResource));
  }

  public IconicEnumerationValue (String name, Icon icon)
  {
    this (name, null, icon);
  }
  
  public IconicEnumerationValue (String name, String text,
  															 String iconResource)
  {
  	this (name, text, Images.createIcon (iconResource));
  }
  
  public IconicEnumerationValue (String name, String text,
  															 Icon iconResource)
  {
  	super (name, text);
  	
    this.icon = iconResource;
  }

  public abstract EnumerationValue [] getEnumValues ();
  
	/**
	 */
	public Icon getIcon ()
	{
		return icon;
	}
	
	public String getIconicName ()
	{
		return toString ();
	}
}
