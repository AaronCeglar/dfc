package dsto.dfc.swt.icons;

import dsto.dfc.util.Objects;

/**
 * A value/label struct for abstracting the information displayed on
 * Button's, Label's, Item's etc. Can also have an optional icon.
 * 
 * @author Matthew Phillips
 */
public class ValueLabel
{
  /** The value associated with this label. */
  public final Object value;
  /** The user-visible label. May include an &amp; shortcut character. */
  public final String label;
  /** The (optional) icon. */
  public final Icon icon;

  /**
   * Shortcut to create a new instance with an integer value.
   * 
   * @see #ValueLabel(String, Icon, Object)
   */
  public ValueLabel (String label, int value)
  {
    this (label, new Integer (value));
  }
  
  /**
   * Shortcut to create a new instance with a boolean value.
   * 
   * @see #ValueLabel(String, Icon, Object)
   */
  public ValueLabel (String label, boolean value)
  {
    this (label, value ? Boolean.TRUE : Boolean.FALSE);
  }
  
  /**
   * Create a new instance.
   * 
   * @param label The label,
   * @param value The value associated with the label.
   */
  public ValueLabel (String label, Object value)
  {
    this (label, null, value);
  }
  
  /**
   * Create a new instance.
   * 
   * @param label The label,
   * @param icon The icon (may be null).
   * @param value The value associated with the label.
   */
  public ValueLabel (String label, Icon icon, Object value)
  {
    this.label = label;
    this.value = value;
    this.icon = icon;
  }

  /**
   * Get the plain label text, with any shortcut "&amp;"'s removed.
   */
  public String getPlainLabel ()
  {
    return label.replaceAll ("&", "");
  }

  /**
   * Get the label associated with a value.
   * 
   * @param valueLabels The array of value/label's to search.
   * @param value The value to look for.
   * @return The value associated with the label.
   * 
   * @throws IllegalArgumentException if there is no label for value.
   */
  public static String labelFor (ValueLabel [] valueLabels, Object value)
    throws IllegalArgumentException
  {
    for (int i = 0; i < valueLabels.length; i++)
    {
      if (Objects.objectsEqual (valueLabels [i].value, value))
        return valueLabels [i].label;
    }
    
    throw new IllegalArgumentException ("No label for value " + value);
  }
  
  /**
   * Get the plain text label associated with a value (see
   * {@link #getPlainLabel()}).
   * 
   * @param valueLabels The array of value/label's to search.
   * @param value The value to look for.
   * @return The value associated with the label.
   * 
   * @throws IllegalArgumentException if there is no label for value.
   */
  public static String plainLabelFor (ValueLabel [] valueLabels, Object value)
  {
    return labelFor (valueLabels, value).replaceAll ("&", "");
  }
}
