package dsto.dfc.swt.icons;


/**
 * Defines an object that has an icon + text representation.
 */
public interface Iconic
{
  public String getIconicName ();

  public Icon getIcon ();
}
