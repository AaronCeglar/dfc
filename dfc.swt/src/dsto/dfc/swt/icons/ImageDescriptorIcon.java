package dsto.dfc.swt.icons;

import java.net.URL;

import org.eclipse.jface.resource.ImageDescriptor;

import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;


/**
 * Wraps a JFace ImageDesecriptor as an Icon. You may want to use
 * {@link dsto.dfc.swt.icons.ImageResourceIcon} instead.
 * 
 * @author mpp
 */
public class ImageDescriptorIcon implements Icon
{
  private ImageDescriptor descriptor;
  private ImageData data;

  public ImageDescriptorIcon (String resource)
  {
    URL url = Images.findResource (resource);
    
    this.descriptor = ImageDescriptor.createFromURL (url);
    this.data = null;
  }
    
  public Image createImage ()
  {
    return descriptor.createImage ();
  }
  
  public Image createImage (Device device)
  {
    return descriptor.createImage (device);
  }
  
  public ImageData getImageData ()
  { 
    return descriptor.getImageData ();
  }
  
  public ImageDescriptor getDescriptor ()
  {
    return descriptor;
  }
  
  private void loadData ()
  {
    if (data == null)
      this.data = descriptor.getImageData ();
  }
  
  // Icon interface
  
  public int getIconWidth ()
  {
    loadData ();
      
    return data.width;
  }
  
  public int getIconHeight ()
  {
    loadData ();
    
    return data.height;
  }
  
  public void paintIcon (GC gc, int x, int y)
  {
    Image image = createImage ();
    
    gc.drawImage (image, x, y);
    
    image.dispose ();
  }
  
  public boolean equals (Object obj)
  {
    return obj instanceof ImageDescriptorIcon &&
           ((ImageDescriptorIcon)obj).descriptor.equals (descriptor);
  }
  
  public int hashCode ()
  {
    return descriptor.hashCode ();
  }
  
  public String toString ()
  {
    return "ImageDescriptorIcon [descriptor=\"" + descriptor + "\"]";
  }
}
