package dsto.dfc.swt.icons;

/**
 * Basic Iconic implementation.
 * 
 * @author Matthew Phillips
 */
public class SimpleIconic implements Iconic
{
  public Icon icon;
  public String text;
  
  public SimpleIconic ()
  {
    // zip
  }
  
  public SimpleIconic (Icon icon, String text)
  {
    this.icon = icon;
    this.text = text;
  }

  public String getIconicName ()
  {
    return text;
  }

  public Icon getIcon ()
  {
    return icon;
  }
}
