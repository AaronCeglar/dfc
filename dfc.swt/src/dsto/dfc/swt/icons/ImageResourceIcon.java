package dsto.dfc.swt.icons;

import java.net.URL;

import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Display;

import org.eclipse.jface.resource.ImageDescriptor;

import dsto.dfc.logging.Log;

/**
 * An icon that paints an image retrieved from a local resource (.gif, 
 * .png, etc).
 * 
 * @author mpp
 */
public class ImageResourceIcon implements Icon
{
  private URL url;
  private ImageData data;
  private String resourceName;

  /**
   * Create a new instance.
   * 
   * @param resourceName The image's resource path.
   * @throws IllegalArgumentException if Images.findResource () cannot resolve.
   */
  public ImageResourceIcon (String resourceName)
  {
    this.resourceName = resourceName;
    this.url = Images.findResource (resourceName);
  }
  
  /**
   * Shortcut to get a cached version of the icon image. The image is placed
   * in the default {@link IconCache}.
   */
  public Image getImage ()
  {
    return IconCache.getDefault ().getOrPut (this);
  }
  
  public Image createImage ()
  {
    return createImage (Display.getCurrent ());
  }
  
  public Image createImage (Device device)
  {
    return Images.createImage (device, getImageData ());
  }
  
  public ImageData getImageData ()
  {
    if (data == null)
    {
      if (url != null)
      {
        try
        {
          data = Images.loadImageData (url) [0];
        } catch (IllegalArgumentException ex)
        {
          Log.warn ("Failed to load image \"" + url + "\"", this, ex);
        }
      }
      
      if (data == null)
        data = ImageDescriptor.getMissingImageDescriptor ().getImageData ();
    }
    
    return data;
  }
  
  // Icon interface
  
  public int getIconWidth ()
  {
    if (data == null)
      getImageData ();
    
    return data.width;
  }
  
  public int getIconHeight ()
  {
    if (data == null)
      getImageData ();
    
    return data.height;
  }
  
  public void paintIcon (GC gc, int x, int y)
  {
    gc.drawImage (getImage (), x, y);
  }
  
  public boolean equals (Object obj)
  {
    return obj instanceof ImageResourceIcon && equals ((ImageResourceIcon)obj);
  }
  
  public boolean equals (ImageResourceIcon icon)
  {
    return icon.resourceName.equals (resourceName);
  }
  
  public int hashCode ()
  {
    return resourceName.hashCode ();
  }
  
  public String toString ()
  {
    return "ImageResourceIcon [resource=\"" + url.getPath () + "\"]";
  }
}
