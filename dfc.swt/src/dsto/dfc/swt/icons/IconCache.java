package dsto.dfc.swt.icons;

import java.util.HashMap;
import java.util.Iterator;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

/**
 * A system-wide cache of rendered SWT images for Icon instances. This class
 * is not thread-safe: it is assumed that all accesses come from the SWT GUI
 * thread. Icons used in the cache must implement equals () and hashCode ().
 * 
 * @author Matthew Phillips
 */
public class IconCache
{
  private static final IconCache INSTANCE = new IconCache ();
 
  protected HashMap cache;
  
  /**
   * Get the default instance.
   */
  public static IconCache getDefault ()
  {
    return INSTANCE;
  }
  
  public IconCache ()
  {
    this.cache = new HashMap ();
  }
  
  /**
   * Clear and dispose of all icons in the cache. Only call this if
   * you know all SWT clients have been disposed (i.e. after the event
   * loop has exited).
   * 
   */
  public void clear ()
  {
    for (Iterator i = cache.values ().iterator (); i.hasNext ();)
    {
      Entry entry = (Entry)i.next ();
      
      entry.image.dispose ();
    }
    
    cache.clear ();
  }

  /**
   * Put icon into the cache. If the icon is not already there, its
   * image is rendered with
   * {@link Icon#createImage(org.eclipse.swt.graphics.Device)} and
   * placed in the cache. If the icon is already there, its reference
   * count is incremented.
   * 
   * @param icon The icon to cache.
   * @return The image for icon.
   * 
   * @see #get(Icon)
   * @see #remove(Icon)
   */
  public Image put (Icon icon)
  {
    Entry entry = (Entry)cache.get (icon);

    if (entry == null)
    {
      entry = new Entry (icon.createImage (Display.getCurrent ()));

      cache.put (icon, entry);
    } else
    {
      entry.refCount++;
    }

    return entry.image;
  }
  
  /**
   * Retrieve the image for a cached icon.
   * 
   * @param icon The icon to query.
   * @return The icon image, or null if not in the cache.
   * 
   * @see #put(Icon)
   * @see #getOrPut(Icon)
   */
  public Image get (Icon icon)
  {
    Entry entry = (Entry)cache.get (icon);
    
    if (entry != null)
      return entry.image;
    else
      return null;
  }
  
  /**
   * Like get (), but put ()'s the icon if not in the cache.
   * 
   * @see #get(Icon)
   * @see #put(Icon)
   */
  public Image getOrPut (Icon icon)
  {
    Entry entry = (Entry)cache.get (icon);
    
    if (entry == null)
    {
      entry = new Entry (icon.createImage (Display.getCurrent ()));
      
      cache.put (icon, entry);    
    }
    
    return entry.image;
  }  
  
  /**
   * Reverse of put (): decrements an icon's reference count in the cache. If
   * the count falls to zero, the image is disposed, EXCEPT for 
   * ImageResourceIcon's, which remain cached permanently to avoid the overhead
   * associated with reloading them.
   */
  public void remove (Icon icon)
  {
    Entry entry = (Entry)cache.get (icon);
    
    if (entry != null)
    {
      if (entry.refCount == 1)
      {        
        if ((icon instanceof ImageResourceIcon))
        {  
          // we keep ImageResourceIcon's with zero refs to avoid loading costs
          entry.refCount = 0;
        } else
        {
          entry.image.dispose ();

          cache.remove (icon);
        }
      } else
      {
        entry.refCount--;
      } 
    }
  }
  
  private static class Entry
  {
    public Image image;
    public int refCount;
    
    public Entry (Image image)
    {
      this.image = image;
      this.refCount = 1;
    }
  }
}
