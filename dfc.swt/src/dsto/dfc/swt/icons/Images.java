package dsto.dfc.swt.icons;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;

import dsto.dfc.logging.Log;

/**
 * @author mpp
 *
 */
public final class Images
{
  /** The base path of DFC icons. */
  public static final String DFC_ICONS_PATH = "/dsto/dfc/icons";

  private static final ImageLoader loader = new ImageLoader ();
  private static final ArrayList searchPath = new ArrayList ();

  static
  {
    searchPath.add (DFC_ICONS_PATH);
  }

  private Images ()
  {
    //  zip
  }

  /**
   * Add a path to the front of the list of paths searched for relative
   * image resources. The same path may be added more than once:
   * re-adding has the same effect as a remove/add.
   */
  public static void addSearchPath (String path)
  {
    searchPath.remove (path);

    searchPath.add (0, path);
  }

  /**
   * Undo the effect of addSearchPath ()
   */
  public static void removeSearchPath (String path)
  {
    searchPath.remove (path);
  }

  public static Icon createIcon (String resource)
  {
    return new ImageResourceIcon (resource);
  }

  /**
   * Create an image from a resource.
   *
   * @param resource The resource name. If relative (does not begin
   * with "/") the resource is searched for in the current search path
   * (see {@link #addSearchPath(String)}).
   * @return The new image.
   * @see #loadImageData(URL)
   * @see #addSearchPath(String)
   */
  public static Image createImage (String resource)
  {
    return createImage (Display.getCurrent (), resource);
  }

  /**
   * Create an image from a URL.
   *
   * @param url The URL.
   * @return The new image.
   * @see #loadImageData(String)
   * @see #addSearchPath(String)
   */
  public static Image createImage (URL url)
  {
    return createImage (Display.getCurrent (), url);
  }

  /**
   * Create an image from a resource.
   *
   * @param device The image device.
   * @param resource The resource name. If relative (does not begin
   * with "/") the resource is searched for in the current search path
   * (see {@link #addSearchPath(String)}).
   * @return The new image.
   * @see #loadImageData(URL)
   * @see #addSearchPath(String)
   */
  public static Image createImage (Device device, String resource)
  {
    try
    {
      ImageData data = loadImageData (resource) [0];

      return createImage (device, data);
    } catch (IllegalArgumentException ex)
    {
      return ImageDescriptor.getMissingImageDescriptor ().createImage ();
    }
  }

  /**
   * Create an image from a URL.
   *
   * @param device The image device.
   * @param url The URL.
   * @return The new image.
   * @see #loadImageData(URL)
   * @see #addSearchPath(String)
   */
  public static Image createImage (Device device, URL url)
  {
    try
    {
      ImageData data = loadImageData (url) [0];

      return createImage (device, data);
    } catch (IllegalArgumentException ex)
    {
      return ImageDescriptor.getMissingImageDescriptor ().createImage ();
    }
  }

  public static Image createImage (Device device, ImageData data)
  {
    if (data.transparentPixel != -1)
      return new Image (device, data, data.getTransparencyMask ());
    else
      return new Image (device, data);
  }

  /**
   * Load image data from a resource.
   *
   * @param resource The resource name. If relative (does not begin
   * with "/") the resource is searched for in the current search path
   * (see {@link #addSearchPath(String)}).
   * @return The image data.
   * @throws IllegalArgumentException if the image resource is not
   * found.
   * @see #loadImageData(URL)
   */
  public static ImageData [] loadImageData (String resource)
    throws IllegalArgumentException
  {
    URL resUrl = findResource (resource);

    if (resUrl == null)
      throw new IllegalArgumentException ("No image resource named \"" +
                                          resource + "\" found");

     return loadImageData (resUrl);
  }

  /**
   * Load image data from a URL.
   *
   * @param url The URL to load from.
   * @return The image data.
   * @throws IllegalArgumentException if the image resource is not found.
   */
  public static ImageData [] loadImageData (URL url)
    throws IllegalArgumentException
  {
    InputStream resStr = null;

    try
    {
      resStr = url.openStream ();

      ImageData [] images = loader.load (resStr);
      resStr.close ();

      return images;
    } catch (IOException ex)
    {
      throw new IllegalArgumentException
        ("Failed to load image resource \"" + url + "\": " + ex);
    } finally
    {
      try
      {
      	if (resStr != null)
           resStr.close ();
      } catch (Exception ex)
      {
      	// zip
      }
    }
  }

  /**
   * Find a resource, searching the image path if the resource is
   * relative.
   */
  public static URL findResource (String resource)
  {
    ClassLoader classLoader = Thread.currentThread ().getContextClassLoader ();

    if (classLoader == null)
      classLoader = Images.class.getClassLoader ();

    URL resourceUrl = findResource (classLoader, resource);

    // todo Do we need this?
    //    if (resourceUrl == null)
    //      resourceUrl = findResource (Images.class.getClassLoader (), resource);

    return resourceUrl;
  }

  private static URL findResource (ClassLoader classLoader, String resource)
  {
    URL url = null;

    if (resource.length () == 0)
      throw new IllegalArgumentException ("Illegal empty resource name");

    if (resource.charAt (0) == '/')
    {
      url = classLoader.getResource (resource.substring (1));
    } else
    {
      for (int i = 0; i < searchPath.size () && url == null; i++)
      {
        String path = searchPath.get (i).toString ();

        if (path.charAt (0) == '/')
          path = path.substring (1);

        url = classLoader.getResource (path + '/' + resource);
      }
    }

    return url;
  }

  public static ImageDescriptor createDescriptor (String resourceName)
  {
    URL url = findResource (resourceName);

    if (url != null)
    {
      return ImageDescriptor.createFromURL (url);
    } else
    {
      Log.warn ("Failed to load image \"" + resourceName + "\"", Images.class);

      return ImageDescriptor.getMissingImageDescriptor ();
    }
  }

  /**
   * Create an 8-bit blank image data block. This is usually used to create
   * new icon data. The palette of this block must be finalized before before
   * being used to create an Image with {@link #finalizePalette(PaletteData)}.
   *
   * @see #createBlankImageData(int, int, int, PaletteData)
   */
  public static ImageData createBlankImageData (int width, int height)
  {
    RGB black = new RGB (0, 0, 0);
    RGB [] rgbs = new RGB [256];
    rgbs [0] = black; // transparency
    rgbs [1] = black; // black

    PaletteData dataPalette = new PaletteData (rgbs);

    ImageData imageData = createBlankImageData (width, height, 8, dataPalette);
    imageData.transparentPixel = 0;

    return imageData;
  }

  /**
   * Create a blank image data block with a given depth and palette.
   *
   * @see #createBlankImageData(int, int)
   */
  public static ImageData createBlankImageData (int width, int height,
                                                int depth, PaletteData palette)
  {
    ImageData imageData =
      new ImageData (width, height, depth, palette);

    // set up transparency mask
    byte [] mask = new byte [imageData.data.length];
    Arrays.fill (mask, (byte)0);
    imageData.maskData = mask;
    imageData.maskPad = imageData.scanlinePad;
    imageData.transparentPixel = 1;

    return imageData;
  }

  /**
   * Creates a transparent image sized <code>width</code> by
   * <code>height</code> in pixels.
   *
   * @param device The Device the image is to be built with.
   * @param width The width of the proposed image.
   * @param height The height of the proposed image.
   * @return A transparent image, <code>width</code> by <code>height</code> in
   *            size.
   */
  public static Image createTransparentImage (Device device, int width,
                                              int height)
  {
    RGB blackRGB = new RGB (0, 0, 0);
    RGB whiteRGB = new RGB (0xFF, 0xFF, 0xFF);
    PaletteData palette = new PaletteData (new RGB[] { blackRGB, whiteRGB });
    ImageData imageData = new ImageData (width, height, 1, palette);
    ImageData maskData = imageData;

    return new Image (device, imageData, maskData);
  }

  /**
   * Finalize an indexed palette by filling unused (null) entries with black
   * pixels.
   */
  public static void finalizePalette (PaletteData palette)
  {
    RGB black = new RGB (0, 0, 0);
    RGB [] rgbs = palette.getRGBs ();

    if (rgbs == null)
      throw new IllegalArgumentException ("Palette is not indexed");

    for (int i = 0; i < rgbs.length; i++)
    {
      if (rgbs [i] == null)
        rgbs [i] = black;
    }
  }

  /**
   * Merge a source icon image into a target, handling transparency masks.
   *
   * @param trg The target to be modified.
   * @param src The source image.
   * @param x Start x coord inside trg.
   * @param y Start y coord inside trg.
   */
  public static void mergeIcons (ImageData trg, ImageData src, int x, int y)
  {
    blit (trg, src, x, y);

    // union the transparency masks
    ImageData mask = trg.getTransparencyMask ();

    orMasks (mask, src.getTransparencyMask (), x, y);

    trg.maskData = mask.data;
    trg.transparentPixel = 1;
  }

  /**
   * Modified version of CompositeImageDescriptor.blit () from 3.0M6 that
   * actually works.
   */
  public static void blit (ImageData trg, ImageData src, int ox, int oy)
  {
    RGB [] out = trg.getRGBs ();

    PaletteData palette = src.palette;
    if (palette.isDirect)
    {
      ImageData mask = src.getTransparencyMask ();

      for (int y = 0; y < src.height; y++)
      {
        for (int x = 0; x < src.width; x++)
        {
          if (mask.getPixel (x, y) != 0)
          {
            int xx = x + ox;
            int yy = y + oy;
            if (xx >= 0 && xx < trg.width && yy >= 0
                && yy < trg.height)
            {
              int pixel = src.getPixel (x, y);

              int r = pixel & palette.redMask;
              /* JM: Changed operators from >> to >>> to shift sign bit right */
              r = (palette.redShift < 0)
                  ? r >>> -palette.redShift
                  : r << palette.redShift;
              int g = pixel & palette.greenMask;
              g = (palette.greenShift < 0)
                  ? g >>> -palette.greenShift
                  : g << palette.greenShift;
              int b = pixel & palette.blueMask;
              b = (palette.blueShift < 0)
                  ? b >>> -palette.blueShift
                  : b << palette.blueShift;

              // original code below always causes a NPE since there is no
              // palette for direct images
              // alloc (out, r, g, b);
              pixel = palette.getPixel (new RGB (r, g, b));

              trg.setPixel (xx, yy, pixel);
            }
          }
        }
      }

      return;
    }

    // map maps src pixel values to dest pixel values
    int map[] = new int[256];
    for (int i = 0; i < map.length; i++)
      map [i] = -1;

    /* JM: added code to test if the image is an icon */
    if (src.getTransparencyType () == SWT.TRANSPARENCY_MASK)
    {
      ImageData mask = src.getTransparencyMask ();
      for (int y = 0; y < src.height; y++)
      {
        for (int x = 0; x < src.width; x++)
        {
          if (mask.getPixel (x, y) != 0)
          {
            int xx = x + ox;
            int yy = y + oy;
            if (xx >= 0 && xx < trg.width && yy >= 0
                && yy < trg.height)
            {
              int pixel = src.getPixel (x, y);
              int newPixel = map [pixel];
              if (newPixel < 0)
              {
                RGB c = palette.getRGB (pixel);
                map [pixel] = newPixel = alloc (out, c.red, c.green, c.blue);
              }

              trg.setPixel (xx, yy, newPixel);
            }
          }
        }
      }
      return;
    }

    int maskPixel = src.transparentPixel;
    for (int y = 0; y < src.height; y++)
    {
      for (int x = 0; x < src.width; x++)
      {
        int pixel = src.getPixel (x, y);
        if (maskPixel < 0 || pixel != maskPixel)
        {
          int xx = x + ox;
          int yy = y + oy;
          if (xx >= 0 && xx < trg.width && yy >= 0
              && yy < trg.height)
          {

            int newPixel = map [pixel];
            if (newPixel < 0)
            {
              RGB c = palette.getRGB (pixel);
              map [pixel] = newPixel = alloc (out, c.red, c.green, c.blue);
            }

            trg.setPixel (xx, yy, newPixel);
          }
        }
      }
    }
  }

  /**
   * Logically OR two transparency masks together to form a union mask.
   */
  public static void orMasks (ImageData trg, ImageData src, int ox, int oy)
  {
    for (int y = 0; y < src.height; y++)
    {
      for (int x = 0; x < src.width; x++)
      {
        int xx = x + ox;
        int yy = y + oy;
        if (xx >= 0 && xx < trg.width && yy >= 0
            && yy < trg.height)
        {
          int trgPixel = trg.getPixel (xx, yy);

          if (trgPixel == 0)
          {
            int srcPixel = src.getPixel (x, y);

            if (srcPixel == 1)
              trg.setPixel (xx, yy, 1);
          }
        }
      }
    }
  }

  /**
   * Returns the index of a RGB entry in the given map which matches the
   * specified RGB color. If no such entry exists, a new RGB is allocated. If
   * the given array is full, the value 0 is returned (which maps to the
   * transparency value).
   */
  private static int alloc(RGB [] map, int red, int green, int blue)
  {
    int i;
    RGB c;

    // this loops starts at index 1 because index 0 corresponds to the
    // transparency value
    for (i = 1; i < map.length && (c = map [i]) != null; i++)
      if (c.red == red && c.green == green && c.blue == blue)
        return i;

    if (i < map.length - 1)
    {
      map [i] = new RGB (red, green, blue);
      return i;
    }
    return 0;
  }
}
