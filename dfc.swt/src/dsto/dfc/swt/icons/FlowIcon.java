package dsto.dfc.swt.icons;

import java.util.ArrayList;

import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;

import dsto.dfc.swt.util.Geometry;

/**
 * @author mpp
 *
 */
public class FlowIcon implements Icon
{
  public static final int CENTER = Geometry.CENTER;
  public static final int TOP = Geometry.TOP;
  public static final int BOTTOM = Geometry.BOTTOM;
  
  private static final int DEFAULT_ICON_GAP = 2;

  protected ArrayList icons = new ArrayList ();
  protected int width = -1;
  protected int height = -1;
  protected int gap = DEFAULT_ICON_GAP;
  protected int alignment = CENTER;
  protected ImageData imageData;

  public FlowIcon ()
  {
    // zip
  }

  public FlowIcon (int gap)
  {
    this.gap = gap;
  }

  public FlowIcon (Icon icon1, Icon icon2)
  {
    addIcon (icon1);
    addIcon (icon2);
  }

  private void calcSize ()
  {
    if (width == -1)
    {
      width = 0;
      height = 0;
      
      for (int i = 0; i < icons.size (); i++)
      {
        Icon icon = (Icon)icons.get (i);
  
        width += icon.getIconWidth ();
        height = Math.max (height, icon.getIconHeight ());
      }
  
      // add gaps
      if (icons.size () > 1)
        width += gap * (icons.size () - 1);
    }
  }
  
  public void setGap (int newGap)
  {
    gap = newGap;
    width = height = -1;
  }

  public int getGap ()
  {
    return gap;
  }

  public int getAlignment ()
  {
    return alignment;
  }
  
  /**
   * Set the vertical alignment of icons. One of TOP, BOTTOM or CENTER.
   * Default is CENTER.
   */
  public void setAlignment (int alignment)
  {
    this.alignment = alignment;
  }
  
  public void addIcon (Icon icon)
  {
    icons.add (icon);
    width = -1;
    height = -1;
  }

  public void removeIcon (Icon icon)
  {
    icons.remove (icon);
    width = -1;
    height = -1;
  }

  /**
   * Removes all the icons.
   */
  public void removeAllIcons ()
  {
    icons.clear();
    width = -1;
    height = -1;
  }

  public void paintIcon (GC gc, int x, int y)
  {
    int xoffset = 0;

    for (int i = 0; i < icons.size (); i++)
    {
      Icon icon = (Icon)icons.get (i);
      
      icon.paintIcon
        (gc,
         x + xoffset,
         y + Geometry.offsetForAlignment (height, icon.getIconHeight (), alignment));
 
      xoffset += icon.getIconWidth () + gap;
    }
  }
  
  public Image createImage (Device device)
  {
    return Images.createImage (device, getImageData ());
  }
  
  public ImageData getImageData ()
  {
    if (imageData == null)
    {
      if (icons.size () == 0)
        throw new IllegalStateException ("Empty flow icon");

      calcSize ();
     
      imageData = Images.createBlankImageData (width, height);

      int xoffset = 0;
      for (int i = 0; i < icons.size (); i++)
      {
        Icon icon = (Icon)icons.get (i);
      
        Images.mergeIcons (imageData, icon.getImageData (), xoffset, 0);
 
        xoffset += icon.getIconWidth () + gap;
      }
      
      Images.finalizePalette (imageData.palette);
    }
    
    return imageData;
  }

  public int getIconWidth ()
  {
    calcSize ();

    return width;
  }

  public int getIconHeight ()
  {
    calcSize ();

    return height;
  }
  
  public boolean equals (Object obj)
  {
    if (obj instanceof FlowIcon)
      return equals ((FlowIcon)obj);
    else
      return false;
  }
  
  public boolean equals (FlowIcon icon)
  {
    return icon.gap == gap && icon.icons.equals (this.icons);
  }
  
  public int hashCode ()
  {
    return icons.hashCode ();
  }
}
