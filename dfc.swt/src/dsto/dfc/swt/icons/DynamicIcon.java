package dsto.dfc.swt.icons;

import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;

/**
 * Base class for icons that draw themselves dynamically.
 * 
 * @author Matthew Phillips
 */
public abstract class DynamicIcon implements Icon
{
  /**
   * Creates a new image and calls {@link #paintIcon(GC, int, int)} to
   * fill it in. Does not take transparency into account.
   * 
   * TODO support image masks.
   */
  public Image createImage (Device device)
  {
    Image image = new Image (device, getIconWidth (), getIconHeight ());
    
    GC gc = new GC (image);
    
    paintIcon (gc, 0, 0);
    
    gc.dispose ();
    
    return image;
  }
  
  public ImageData getImageData ()
  {
    throw new UnsupportedOperationException ();
  }
  
  public abstract int getIconHeight ();

  public abstract int getIconWidth ();

  public abstract void paintIcon (GC gc, int x, int y);
}
