package dsto.dfc.swt.icons;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.RGB;

/**
 * A simple icon displaying a block of solid colour.
 * 
 * @author Matthew Phillips
 */
public class ColourIcon extends DynamicIcon implements Icon
{
  private RGB colour;
  private int width;
  private int height;

  public ColourIcon (RGB colour, int width, int height)
  {
    this.colour = colour;
    this.width = width;
    this.height = height;
  }

  public int getIconHeight ()
  {
    return height;
  }

  public int getIconWidth ()
  {
    return width;
  }

  public void paintIcon (GC gc, int x, int y)
  {
    Color background = new Color (gc.getDevice (), colour);
    
    gc.setBackground (background);
    gc.fillRectangle (x, y, width, height);
    
    background.dispose ();
  }
}
