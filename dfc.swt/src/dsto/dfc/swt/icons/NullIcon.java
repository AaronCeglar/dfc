package dsto.dfc.swt.icons;

import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;

/**
 * An empty icon.
 * 
 * @author Matthew Phillips
 */
public class NullIcon implements Icon
{
  private int width;
  private int height;

  public NullIcon (int width, int height)
  {
    this.width = width;
    this.height = height;
  }
  
  public Image createImage (Device device)
  {
    throw new UnsupportedOperationException ();
  }

  public int getIconHeight ()
  {
    return height;
  }

  public int getIconWidth ()
  {
    return width;
  }

  public ImageData getImageData ()
  {
    throw new UnsupportedOperationException ();
  }

  public void paintIcon (GC gc, int x, int y)
  {
    // zip
  }
}
