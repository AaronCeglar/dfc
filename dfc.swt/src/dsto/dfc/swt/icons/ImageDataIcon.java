package dsto.dfc.swt.icons;

import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Display;


/**
 * An icon that is created from preloaded image data.
 * 
 * @see dsto.dfc.swt.icons.ImageResourceIcon
 * 
 * @author mpp
 */
public class ImageDataIcon implements Icon
{
  private String name;
  private ImageData data;

  /**
   * Create a new instance.
   * 
   * @param name The image's logical name.
   */
  public ImageDataIcon (String name, ImageData data)
  {
    this.name = name;
    this.data = data;
  }
  
  /**
   * Shortcut to get a cached version of the icon image. The image is placed
   * in the default {@link IconCache}.
   */
  public Image getImage ()
  {
    return IconCache.getDefault ().getOrPut (this);
  }
  
  public Image createImage ()
  {
    return createImage (Display.getCurrent ());
  }
  
  public Image createImage (Device device)
  {
    return Images.createImage (device, data);
  }
  
  public ImageData getImageData ()
  {
    return data;
  }
  
  // Icon interface
  
  public int getIconWidth ()
  {
    return data.width;
  }
  
  public int getIconHeight ()
  {
    return data.height;
  }
  
  public void paintIcon (GC gc, int x, int y)
  {
    gc.drawImage (getImage (), x, y);
  }
  
  public boolean equals (Object obj)
  {
    return obj instanceof ImageDataIcon &&
           ((ImageDataIcon)obj).name.equals (name);
  }
  
  public int hashCode ()
  {
    return name.hashCode ();
  }
  
  public String toString ()
  {
    return "ImageDataIcon [name=\"" + name + "\"]";
  }
}
