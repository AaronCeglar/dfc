package dsto.dfc.swt.controls;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

/**
 * Same as the JFace InputDialog, but uses a combo box for input.
 * 
 * @author mpp
 * @version $Revision$
 * 
 * @see org.eclipse.jface.dialogs.InputDialog
 */
public class ComboInputDialog extends Dialog implements ModifyListener
{
  private String value;
  private int valueIndex;
  private Combo combo;
  private Label errorMessageLabel;
  private IInputValidator validator;
  private String dialogTitle;
  private String dialogMessage;
  private String [] comboItems;
  
  public ComboInputDialog (Shell parentShell, String dialogTitle,
                           String dialogMessage, String initialValue,
                           String [] items,
                           IInputValidator validator)
  {
    super (parentShell);
    
    this.dialogTitle = dialogTitle;
    this.dialogMessage = dialogMessage;
    this.value = initialValue;
    this.validator = validator;
    this.comboItems = items;
    this.valueIndex = -1;
  }
  
  public String getValue ()
  {
    return value;
  }
  
  public int getValueIndex ()
  {
    return valueIndex;
  }
  
  protected void configureShell (Shell newShell)
  {
    super.configureShell (newShell);
    
    newShell.setText (dialogTitle);
  }

  protected void buttonPressed (int buttonId)
  {
    if (buttonId == IDialogConstants.OK_ID)
    {
      value = combo.getText ();
      valueIndex = combo.getSelectionIndex ();
    } else
    {
      value = "";
      valueIndex = -1;
    }

    super.buttonPressed (buttonId);
  }

  protected void createButtonsForButtonBar (Composite parent)
  {
    super.createButtonsForButtonBar (parent);
    
    combo.setFocus ();
    
    if (value == null || value.length () == 0)
      getButton (OK).setEnabled (false);
  }
  
  protected Control createDialogArea (Composite parent)
  {
    Composite composite = (Composite)super.createDialogArea (parent);
  
    // create message
    Label label = new Label (composite, SWT.WRAP);
    label.setText (dialogMessage);
    GridData data =
      new GridData
        (GridData.GRAB_HORIZONTAL | GridData.GRAB_VERTICAL |
         GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_CENTER);
      
    data.widthHint =
      convertHorizontalDLUsToPixels (IDialogConstants.MINIMUM_MESSAGE_AREA_WIDTH);
      
    label.setLayoutData (data);
  
    // create combo
    combo = new Combo (composite, SWT.BORDER);
    
    combo.setLayoutData (new GridData (GridData.FILL_HORIZONTAL));
    
    combo.setItems (comboItems);
    if (value != null)
      combo.setText (value);

    combo.addModifyListener (this);
    
    // create error message
    errorMessageLabel = new Label (composite, SWT.NONE);
    errorMessageLabel.setLayoutData (new GridData (GridData.FILL_HORIZONTAL));
    
    return composite;
  }

  public void modifyText (ModifyEvent e)
  {
    validateInput ();
  }
  
  protected void validateInput ()
  {
    String errorMessage = null;

    if (validator != null)
      errorMessage = validator.isValid (combo.getText ());

    errorMessageLabel.setText (errorMessage == null ? "" : errorMessage);
    
    getButton (OK).setEnabled (errorMessage == null);
    
    errorMessageLabel.getParent ().update ();
  }
}