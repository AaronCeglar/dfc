package dsto.dfc.swt.controls;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.Layout;

import dsto.dfc.swt.commands.CommandView;
import dsto.dfc.swt.commands.Commands;

/**
 * A host for a control that exports commands in its toolbar command view.
 * The contained control's toolbar commands are shown in a toolbar at the top
 * of the hosted control. Call {@link #setControl(Control)} after creating
 * the control with this panel as parent.
 * 
 * @author mpp
 * @version $Revision$
 */
public class ToolbarPanel extends Composite
{
  private static final int GAP = 0;

  protected CoolBar toolbar;
  protected Control control;
  
  public ToolbarPanel (Composite parent, int style)
  {
    super (parent, style);
    
    toolbar = new CoolBar (this, 0);
    
    setLayout (new ToolbarLayout ());
  }
  
  public void setControl (Control control)
  {
    this.control = control;
    
    // controls may expect GridData: we kind of emulate bits of it
    control.setLayoutData (new GridData (GridData.FILL_BOTH));
    
    CommandView controlToolbarView = Commands.getToolbarView (control);
    CommandView toolbarView = Commands.getToolbarView (this);
    
    toolbarView.addView (controlToolbarView);
    
    Commands.createToolbar (toolbar, toolbarView);
  }
  
  public Control getControl ()
  {
    return control;
  }

  public CoolBar getToolbar ()
  {
    return toolbar;
  }

  public void setBounds(int x, int y, int width, int height)
  {
    super.setBounds (x, y, width, height);
    
    boolean oldLocked = toolbar.getLocked ();
    
    layout (true);
    
    // for some reason locked status gets blown away (at least on SWT 2.1.1
    // on win32), so restore it here
    toolbar.setLocked (oldLocked);
  }
  
  class ToolbarLayout extends Layout
  {
    protected Point computeSize (Composite composite, int wHint, int hHint,
                                  boolean flushCache)
    {
      Point toolbarSize = toolbar.computeSize (wHint, SWT.DEFAULT, flushCache);
      
      Point controlSize =
        new Point
          (wHint,
           hHint == SWT.DEFAULT ? SWT.DEFAULT : hHint - toolbarSize.y - GAP);
      
      GridData data = (GridData)control.getLayoutData ();

      if (data != null)
      {
        controlSize.x = Math.max (controlSize.x, data.widthHint);
        controlSize.y = Math.max (controlSize.y, data.heightHint);
      }

      controlSize = control.computeSize (controlSize.x, controlSize.y);
      
      Rectangle rect =
        computeTrim (0, 0,
                     Math.max (toolbarSize.x, controlSize.x),
                     toolbarSize.y + controlSize.y + GAP);
      
      return new Point (rect.width, rect.height);
    }
    
    protected void layout (Composite composite, boolean flushCache)
    {
      Rectangle area = getClientArea ();
  
      Point toolbarSize = toolbar.computeSize (area.width, SWT.DEFAULT, flushCache);
      
      toolbar.setBounds (area.x, area.y, area.width, toolbarSize.y);
      
      // debug: dumps item names to console
//      CoolItem [] items = toolbar.getItems();
//      for (int i = 0; i < items.length; i++)
//      {
//        ToolBar toolbar1 = (ToolBar)items [i].getControl();
//        
//        ToolItem [] toolitems = toolbar1.getItems();
//        
//        for (int j = 0; j < toolitems.length; j++)
//        {
//          System.out.println ("item = " + toolitems [j].getClass () + " " + toolitems [j].getText());
//        }
//        
//      }
      
      control.setBounds (area.x, area.y + toolbarSize.y + GAP,
                         area.width, area.height - toolbarSize.y - GAP);
    }
  }
}
