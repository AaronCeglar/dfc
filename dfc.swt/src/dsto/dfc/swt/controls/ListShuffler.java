package dsto.dfc.swt.controls;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerSorter;

import dsto.dfc.collections.BasicMonitoredList;
import dsto.dfc.collections.MonitoredCollection;

import dsto.dfc.swt.DFC_SWT;
import dsto.dfc.swt.viewers.CollectionTableContentProvider;

/**
 * Basic list editor that allows user to shuffle items between two lists.
 *  
 * @author mpp
 */
public class ListShuffler
  extends Composite implements SelectionListener, ISelectionChangedListener
{
  private TableViewer leftList;
  private TableViewer rightList;
  private Button leftButton;
  private Button rightButton;
  
  public ListShuffler (Composite parent, int style)
  {
    super (parent, style);
    
    GridLayout layout = new GridLayout (3, false);
    layout.marginWidth = 0;
    layout.marginHeight = 0;
    
    setLayout (layout);

    this.leftList = new TableViewer (this, SWT.BORDER | SWT.MULTI);
    
    Composite buttonPanel = new Composite (this, SWT.NONE);
    layout = new GridLayout (1, true);
    layout.marginWidth = 0;
    layout.marginHeight = 0;
    buttonPanel.setLayout (layout);

    this.leftButton = new Button (buttonPanel, SWT.PUSH);
    leftButton.setText ("<<");
    leftButton.setToolTipText ("Move items from right to left");
    leftButton.setEnabled (false);
    
    this.rightButton = new Button (buttonPanel, SWT.PUSH);
    rightButton.setText (">>");
    leftButton.setToolTipText ("Move items from left to right");
    rightButton.setEnabled (false);
    
    this.rightList = new TableViewer (this, SWT.BORDER | SWT.MULTI);
    
    int listWidth = DFC_SWT.getTextUnitWidth (leftList.getTable ()) * 25;
    GridData data = new GridData (GridData.FILL_BOTH);

    data.verticalSpan = 2;
    data.widthHint = listWidth;  
    leftList.getTable ().setLayoutData (data);
  
    data = new GridData (GridData.FILL_BOTH);
    data.verticalSpan = 2;
    data.widthHint = listWidth;
    rightList.getTable ().setLayoutData (data);
    
    data = new GridData ();
    data.verticalAlignment = GridData.CENTER;
    data.grabExcessVerticalSpace = true;
    buttonPanel.setLayoutData (data);
    
    data = new GridData (GridData.FILL_HORIZONTAL);
    leftButton.setLayoutData (data);
    
    data = new GridData (GridData.FILL_HORIZONTAL);
    rightButton.setLayoutData (data);
    
    leftList.setContentProvider (new CollectionTableContentProvider ());
    rightList.setContentProvider (new CollectionTableContentProvider ());
    
    leftButton.addSelectionListener (this);
    rightButton.addSelectionListener (this);
    leftList.addSelectionChangedListener (this);
    rightList.addSelectionChangedListener (this);
  }
  
  public void setLabelProvider (ILabelProvider provider)
  {
    leftList.setLabelProvider (provider);
    rightList.setLabelProvider (provider);
  }
  
  public void setContentProvider (IStructuredContentProvider provider)
  {
    leftList.setContentProvider (provider);
    rightList.setContentProvider (provider);
  }
  
  public void setSorter (ViewerSorter sorter)
  {
    leftList.setSorter (sorter);
    rightList.setSorter (sorter);
  }
  
  /**
   * Set the input collections.
   * 
   * @param allItems The set of all items that may be in either the right or
   * left lists. If this is a {@link MonitoredCollection} it is tracked for
   * changes.
   * @param items The list of items to be modified in the left list.
   */
  public void setInput (Collection allItems, Collection items)
  {
    // create a list of items not in the left list 
    BasicMonitoredList rightItems = new BasicMonitoredList ();
    
    for (Iterator i = allItems.iterator (); i.hasNext (); )
    {
    	Object item = i.next ();
    	
      if (!items.contains (item))
        rightItems.add (item);
    }
    
    leftList.setInput (items);
    rightList.setInput (rightItems);
  }
  
  public TableViewer getLeftList ()
  {
    return leftList;
  }

  public TableViewer getRightList ()
  {
    return rightList;
  }

  public Button getMoveLeftButton ()
  {
    return leftButton;
  }

  public Button getMoveRightButton ()
  {
    return rightButton;
  }

  public void widgetDefaultSelected (SelectionEvent e)
  {
    // zip
  }

  public void widgetSelected (SelectionEvent e)
  {
    TableViewer source;
    TableViewer target;

    if (e.widget == leftButton)
    {
      source = rightList;
      target = leftList;
    } else
    {
      source = leftList;
      target = rightList;
    }
    
    List selected = ((IStructuredSelection)source.getSelection ()).toList ();
    
    ((Collection)source.getInput ()).removeAll (selected);
    ((Collection)target.getInput ()).addAll (selected);
    
    target.setSelection (new StructuredSelection (selected), true);
  }

  public void selectionChanged (SelectionChangedEvent e)
  {
    leftButton.setEnabled (!rightList.getSelection ().isEmpty ());
    rightButton.setEnabled (!leftList.getSelection ().isEmpty ());
  }
}
