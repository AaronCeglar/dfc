package dsto.dfc.swt.controls;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.jface.dialogs.Dialog;

import dsto.dfc.swt.DFC_SWT;

/**
 * A dialog that presents a set of choices as a list of checkboxes or radio
 * buttons.
 * 
 * @author mpp
 * 
 * @see ChoicePanel
 */
public class ChoiceDialog extends Dialog
{
  private ChoicePanel choicePanel;
  private boolean exclusive;
  private String title;
  private String buttonsTitle;
  private String [] labels;
  private int [] values;
  private int choice;

  /**
   * Create a new instance.
   * 
   * @param parentShell The parent for the dialog.
   * @param title The title of the dialog.
   * @param buttonsTitle The title shown on the group panel containing
   *          the choices. May be null in which case the choices are
   *          not put in a Group panel.
   * @param labels The labels, one per choice.
   * @param values The values for each label (values [i] is value for
   *          selection of labels [i]).
   * @param choice The initial selected choice.
   * @param exclusive True if only one value can be chosen (will use
   *          radio list rather than checkboxes).
   */
  public ChoiceDialog (Shell parentShell, String title,
                       String buttonsTitle,
                       String [] labels, int [] values, int choice,
                       boolean exclusive)
  {
    super (parentShell);
    
    this.title = title;
    this.buttonsTitle = buttonsTitle;
    this.labels = labels;
    this.values = values;
    this.choice = choice;
    this.exclusive = exclusive;
  }

  public void setChoice (int choice)
  {
    this.choice = choice;
  }
  
  public int getChoice ()
  {
    return choice;
  }

  protected void okPressed ()
  {
    choice = choicePanel.getChoice ();
    
    super.okPressed ();
  }

  protected void configureShell (Shell shell)
  {
    super.configureShell (shell);
    
    shell.setText (title);
  }

  protected Control createDialogArea (Composite parent)
  {
    Composite panel = (Composite)super.createDialogArea (parent);
    
    choicePanel =
      new ChoicePanel (panel, buttonsTitle, labels, values, choice, exclusive);
      
    GridData data = new GridData (GridData.FILL_BOTH);
    data.widthHint = DFC_SWT.getTextUnitWidth (panel) * 45;

    choicePanel.setLayoutData (data);
    
    return panel;
  }
}
