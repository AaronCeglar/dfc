package dsto.dfc.swt.controls;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ViewerSorter;

import dsto.dfc.swt.viewers.CollectionTableContentProvider;

/**
 * A chooser that allows items to be added/removed from a collection by
 * checking/unchecking them in a checkbox list view.
 *  
 * @author phillipm
 */
public class ListCheckboxChooser
  extends Composite implements ICheckStateListener
{
  protected Collection items;
  protected CheckboxTableViewer tableView;

  public ListCheckboxChooser (Composite parent, int style, Collection allItems)
  {
    super (parent, SWT.NONE);
    
    setLayout (new FillLayout ());
    
    tableView = CheckboxTableViewer.newCheckList (this, style);
    
    tableView.setContentProvider (new ChooserProvider ());
    tableView.addCheckStateListener (this);
    
    setInput (new ArrayList ());
    
    tableView.setInput (allItems);
  }
  
  public void setInput (Collection items)
  {
    this.items = items;
    
    tableView.setCheckedElements (items.toArray ());
  }

  public Collection getInput ()
  {
    return items;
  }

  public CheckboxTableViewer getTable ()
  {
    return tableView;
  }
  
  public void setLabelProvider (ILabelProvider provider)
  {
    tableView.setLabelProvider (provider);
  }
  
  public void setSorter (ViewerSorter sorter)
  {
    tableView.setSorter (sorter);
  }
  
  public void checkStateChanged (CheckStateChangedEvent e)
  {
    if (e.getChecked ())
      items.add (e.getElement ());
    else
      items.remove (e.getElement ());
  }
  
  class ChooserProvider extends CollectionTableContentProvider
  {
    protected void insertElements (Collection elements, int startIndex)
    {
      super.insertElements (elements, startIndex);
      
      for (Iterator i = elements.iterator (); i.hasNext (); )
      {
      	Object element = i.next ();
      	
        tableView.setChecked (element, items.contains (element));
      }
    }
  }
}
