package dsto.dfc.swt.controls;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

/**
 * A pseudo SWT ToolItem that wraps a real dropdown item and a 
 * menu.
 * 
 * @author mpp
 * @version $Revision$
 */
public class MenuToolItem implements SelectionListener
{
  private ToolItem item;
  private Menu menu;
  private boolean menuAlways;
  
  public MenuToolItem (ToolBar toolbar, Menu menu, int index)
  {
    this.item = new ToolItem (toolbar, SWT.DROP_DOWN, index);
    this.menu = menu;
    this.menuAlways = true;
    
    item.addSelectionListener (this);
    
    updateIcon ();
  }
  
  public void dispose ()
  {    
    if (!item.isDisposed ())
      item.dispose ();
    
    if (!menu.isDisposed ())
      menu.dispose ();
  }
  
  /**
   * Set to true to display the menu on any click, rather than only when the 
   * down arrow is selected.
   */
  public void setMenuAlways (boolean menuOnly)
  {
    this.menuAlways = menuOnly;
  }
  
  public boolean isMenuAlways ()
  {
    return menuAlways;
  }
  
  public ToolItem getToolItem ()
  {
    return item;
  }

  private void updateIcon ()
  {
    for (int i = 0; i < menu.getItemCount (); i++)
    {
      MenuItem menuItem =  menu.getItem (i);
      
      if (menuItem == menu.getDefaultItem ())
      {
        item.setImage (menuItem.getImage ());
        return;
      }
    }
    
    for (int i = 0; i < menu.getItemCount (); i++)
    {
      MenuItem menuItem =  menu.getItem (i);
      
      if (menuItem.getImage () != null)
      {
        item.setImage (menuItem.getImage ());
        return;
      }
    }
  }

  public void widgetSelected (SelectionEvent e)
  {
    if (menuAlways || e.detail == SWT.ARROW)
    {
      Rectangle bounds = item.getBounds ();
      
      Point location = new Point (bounds.x, bounds.y + bounds.height);
      location = item.getParent ().toDisplay (location);
      
      menu.setLocation (location.x, location.y);
      menu.setVisible (true);
    } else
    {
      // MenuItem menuItem = menu.getDefaultItem ();
      /** @todo change active default menu item ?? */
    }
  }

  public void widgetDefaultSelected (SelectionEvent e)
  {
    // zip
  }
}
