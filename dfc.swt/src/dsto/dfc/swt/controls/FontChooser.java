package dsto.dfc.swt.controls;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FontDialog;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import dsto.dfc.swt.forms.Forms;

/**
 * Font chooser control showing font description plus a button to popup a 
 * font chooser dialog.
 * 
 * @author phillipm
 */
public class FontChooser extends Composite implements Listener
{
  private Text text;
  private FontData selectedFont;
  
  public FontChooser (Composite parent)
  {
    super (parent, SWT.NONE);
    
    Forms.gridLayout (this, 2);

    text = new Text (this, SWT.READ_ONLY);
    text.setLayoutData (new GridData (GridData.FILL_HORIZONTAL));
    
    Button button = new Button (this, SWT.PUSH);
    button.setText ("...");
    button.setToolTipText ("Choose a font");
    
    button.addListener (SWT.Selection, this);
    
    setSelectedFont (getFont ().getFontData () [0]);
  }

  public FontData getSelectedFont ()
  {
    return selectedFont;
  }

  public void setSelectedFont (FontData font)
  {
    selectedFont = font;
    
    String style;

    if (font.getStyle () == SWT.BOLD)
      style = "Bold";
    else if (font.getStyle () == SWT.ITALIC)
      style = "Italic";
    else if (font.getStyle () == (SWT.BOLD | SWT.ITALIC))
      style = "Bold Italic";
    else
      style = "Regular";

    StringBuilder fontText = new StringBuilder (40);

    fontText.append (font.getName ());
    fontText.append (" ").append (font.getHeight ()).append ("pt ");
    fontText.append (style);
      
    text.setText (fontText.toString ());
    getParent ().layout (true);
    
    Event event = new Event ();
    event.widget = this;
    event.type = SWT.Selection;
    event.data = selectedFont;

    notifyListeners (SWT.Selection, event);
  }

  public void handleEvent (Event event)
  {
    FontDialog dialog = new FontDialog (getShell ());
    
    dialog.setFontList (new FontData [] {selectedFont});

    FontData newFont = dialog.open ();
    
    if (newFont != null)
      setSelectedFont (newFont);
  }
}
