package dsto.dfc.swt.controls;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import dsto.dfc.swt.icons.ImageResourceIcon;

/**
 * A single line status bar that shows error, warning and info messages.
 * A separate stack of messages is maintained for each type and the current
 * displayed message is the first message at the top of the error, warning or
 * info stacks (in that order).
 * 
 * @author Matthew Phillips
 */
public class StatusBar extends Composite
{
  public static final int ERROR = 0;
  public static final int WARNING = 1;
  public static final int INFO = 2;

  private static final ImageResourceIcon [] ICONS = new ImageResourceIcon []
  {
    new ImageResourceIcon ("stop.gif"),
    new ImageResourceIcon ("warning.png"),
    new ImageResourceIcon ("info16.gif")
  };
  
  private ArrayList [] stacks;
  private CLabel label;
  
  public StatusBar (Composite parent, int style)
  {
    super (parent, style);
    
    this.label = new CLabel (this, SWT.WRAP);
    this.stacks = new ArrayList [3];
    
    for (int i = 0; i < stacks.length; i++)
      stacks [i] = new ArrayList (3);
    
    setLayout (new FillLayout ());
  }

  /**
   * Add a new message. This shortcut allows the message type to be
   * encoded in the message text.
   * 
   * @param id An ID for the message. Any other message with that ID
   *          is replaced.
   * @param message The message type and message text. The text must
   *          begin with "e:" (ERROR)"w:" (WARNING) or "i:" (INFO).
   *          The rest of the text is the message itself. Can use null
   *          as a shortcut for the equivalent of removeMessage (id).
   * 
   * @see #addMessage(String, int, String)
   * @see #removeMessage(String)
   */
  public void addMessage (String id, String message)
  {
    if (message == null)
    {
      addMessage (id, -1, message);
    } else
    {
      char typeCode = message.charAt (0);
      int type;
     
      switch (typeCode)
      {
        case 'e':
          type = ERROR; break;
        case 'w':
          type = WARNING; break;
        case 'i':
          type = INFO; break;
        default:
          throw new IllegalArgumentException ("'" + typeCode + " is not a valid type");
      }
      
      addMessage (id, type, message.substring (2));
    }
  }
  
  /**
   * Add a new message.
   * 
   * @param id An ID for the message. Any other message with that ID
   *          is replaced.
   * @param type The message type: ERROR, WARNING or INFO.
   * @param message The message text. Can use null as a shortcut for
   *          the equivalent of removeMessage (id) (type is ignored).
   * 
   * @see #addMessage(String, String)
   * @see #removeMessage(String)
   */
  public void addMessage (String id, int type, String message)
  {
    doRemoveMessage (id);
    
    if (message != null)
      stacks [type].add (0, new Item  (id, message));
    
    updateDisplay ();
  }
  
  /**
   * Remove a message.
   * 
   * @param id The ID of the message.
   * 
   * @see #addMessage(String, int, String)
   */
  public void removeMessage (String id)
  {
    doRemoveMessage (id);
    
    updateDisplay ();
  }
  
  /**
   * Get the type of the currently displayed message.
   * 
   * @return The message type (ERROR, WARNING or INFO) or -1 if no message
   * is displayed.
   */
  public int getCurrentType ()
  {
    for (int type = 0; type < stacks.length; type++)
    {
      if (!stacks [type].isEmpty ())
        return type;
    }
    
    return -1;
  }
  
  /**
   * True if the status bar is showing a message.
   */
  public boolean showingMessage ()
  {
    return getCurrentType () == -1;
  }
  
  /**
   * Update the label to show the most important message.
   */
  private void updateDisplay ()
  {
    Image image = null;
    String message = "";
    
    for (int type = 0; image == null && type < stacks.length; type++)
    {
      List stack = stacks [type];
      
      if (!stack.isEmpty ())
      {
        image = ICONS [type].getImage ();
        message = ((Item)stack.get (0)).message;
      }
    }
    
    label.setImage (image);
    label.setText (message);
  }

  private void doRemoveMessage (String id)
  {
    for (int i = 0; i < stacks.length; i++)
    {
      List stack = stacks [i];
     
      for (int j = 0; j < stack.size (); j++)
      {
        if (((Item)stack.get (j)).id.equals (id))
        {
          stack.remove (j);
          
          return;
        }
      }
    }
  }
  
  protected static class Item
  {
    public String id;
    public String message;

    public Item (String id, String message)
    {
      this.id = id;
      this.message = message;
    }
  }
}