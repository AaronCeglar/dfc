package dsto.dfc.swt.controls;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

/**
 * Allows selection of either one or many integer-based choices in a checkbox
 * or radio list.
 *  
 * @author mpp
 */
public class ChoicePanel extends Composite
{
  private Button [] buttons;
  private int [] values;
  private int choice;
  private boolean exclusive;
  
  /**
   * Create a new instance.
   * 
   * @param parent The parent.
   * @param title The title of the choice group. May be null in which
   *          case the radio buttons are not put in a Group panel.
   * @param labels The choice labels.
   * @param values The choice values.
   * @param choice The initially selected choice.
   * @param exclusive True if only one value can be chosen (will use
   *          radio list rather than checkboxes).
   * 
   * @see #getChoice()
   */
  public ChoicePanel (Composite parent, String title,
                      String [] labels, int [] values,
                      int choice,
                      boolean exclusive)
  {
    super (parent, 0);

    this.values = values;
    this.exclusive = exclusive;

    GridLayout layout = new GridLayout (1, true);
    layout.marginHeight = 0;
    layout.marginWidth = 0;
    
    setLayout (layout);

    Composite panel;
    
    if (title == null)
    {
      panel = new Composite (this, SWT.NONE);
    } else
    {
      Group group = new Group (this, SWT.NONE);
      group.setText (title);
      
      panel = group;
    }
    
    panel.setLayoutData (new GridData (GridData.FILL_BOTH));
    panel.setLayout (new GridLayout (1, true));
    
    buttons = new Button [labels.length];
    
    int buttonType = exclusive ? SWT.RADIO : SWT.CHECK;

    for (int i = 0; i < buttons.length; i++)
    {
      Button button = new Button (panel, buttonType);
      
      button.setText (labels [i]);
      
      if (exclusive)
        button.setSelection (choice == values [i]);
      else
        button.setSelection ((choice & values [i]) != 0);

      buttons [i] = button;           
    }
  }

  /**
   * Set the current choice.
   * 
   * @see #getChoice()
   */
  public void setChoice (int choice)
  {
    this.choice = choice;
    
    for (int i = 0; i < buttons.length; i++)
    {
      if (exclusive)
        buttons [i].setSelection (choice == values [i]);
      else
        buttons [i].setSelection ((choice & values [i]) != 0);
    }
  }
  
  /**
   * Get the selected choice. If exclusive, this will be one element from the
   * values array. If non-exclusive, this will be a bitwise OR of the selected
   * values from the values array.
   */
  public int getChoice ()
  {
    choice = 0;

    for (int i = 0; i < buttons.length; i++)
    {
      Button button = buttons [i];
      
      if (button.getSelection ())
        choice |= values [i];
    }
    
    return choice;
  }
  
  /**
   * Get the button at a given index (*not* for a given choice value).
   */
  public Button getButton (int index)
  {
    return buttons [index];
  }
}
