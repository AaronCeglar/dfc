package dsto.dfc.swt.controls;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

/**
 * A listener that makes the specified table's columns resize
 * automatically when the table is resized.
 * 
 * @author NguyenV3
 */
public class TableColumnResizer implements Listener
{
  protected Table table;

  public TableColumnResizer (Table table)
  {
    this.table = table;
    
    table.addListener (SWT.Resize, this);
    
    // trigger initial resize on parent shell's visibility
    table.getShell ().addListener (SWT.Show, this);
    
    table.addListener (SWT.Dispose, new Listener ()
    {
      public void handleEvent (Event event)
      {
        TableColumnResizer.this.table.getShell ().removeListener 
          (SWT.Show, TableColumnResizer.this);
      }
    });
  }

  public void handleEvent (Event event)
  {
    resizeColumns ();
  }

  private void resizeColumns ()
  {
    Rectangle area = table.getParent ().getClientArea();
    Point preferredSize = table.computeSize(SWT.DEFAULT, SWT.DEFAULT);
    int width = area.width - 2*table.getBorderWidth();
    if (preferredSize.y > area.height + table.getHeaderHeight()) {
      // Subtract the scrollbar width from the total column width
      // if a vertical scrollbar will be required
      Point vBarSize = table.getVerticalBar().getSize();
      width -= vBarSize.x;
    }
    
    // exclude the last col in resizing operations
    int numCols = table.getColumnCount ();
    
    table.setSize(area.width, area.height);
    for (int i = 0; i < numCols; i++)
    {
      TableColumn col = table.getColumn (i); 
      col.setWidth (width/numCols);
    }
  }
}