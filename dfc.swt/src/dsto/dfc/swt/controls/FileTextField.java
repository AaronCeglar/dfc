package dsto.dfc.swt.controls;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import dsto.dfc.swt.DFC_SWT;
import dsto.dfc.swt.forms.Forms;
import dsto.dfc.util.Files;

/**
 * A text control that is designed for entering a file or directory.
 * Consists of a standard text field plus a "..." button to the right
 * to popup a file or directory dialog.
 * 
 * @author phillipm
 */
public class FileTextField extends Composite implements Listener
{
  private Text fileField;
  private Button popupButton;
  private boolean selectDirectory;
  private String dialogTitle;
  private int dialogMode;
  private String dialogDirectory;
  private String defaultExtension;
  private String [] dialogExtensions;
  private String dialogDescription;

  /**
   * Create a new instance.
   * 
   * @param parent The parent component.
   * @param style The text field's style.
   */
  public FileTextField (Composite parent, int style)
  {
    super (parent, SWT.NONE);
    
    dialogTitle = "Open";
    dialogMode = SWT.OPEN;
    
    Forms.gridLayout (this, 2);
    
    fileField = new Text (this, style);
    GridData layoutData = new GridData (GridData.FILL_HORIZONTAL);
    layoutData.widthHint = DFC_SWT.getTextUnitWidth (fileField) * 35;
    
    fileField.setLayoutData (layoutData);
    
    popupButton = new Button (this, SWT.PUSH);
    popupButton.setText ("...");
    popupButton.setToolTipText ("Click to browse files");
    
    popupButton.addListener (SWT.Selection, this);
  }
  
  public void setSuggestedWidth (int columns)
  {
    GridData data = new GridData (GridData.FILL_HORIZONTAL);
    data.widthHint = DFC_SWT.getTextUnitWidth (fileField) * columns;
    fileField.setLayoutData (data);
  }
  
  public Text getTextField ()
  {
    return fileField;
  }
  
  public Button getPopupButton ()
  {
    return popupButton;
  }
  
  public void setText (String text)
  {
    fileField.setText (text);
  }
  
  public String getText ()
  {
    return fileField.getText ();
  }
  
  public void setDialogDirectory (String directory)
  {
    this.dialogDirectory = directory;
  }

  public String [] getDialogExtensions ()
  {
    return dialogExtensions;
  }
  
  public void setDialogExtensions (String [] extensions)
  {
    dialogExtensions = extensions;
  }

  public void setDefaultExtension (String extension)
  {
    defaultExtension = extension;
  }

  public String getDefaultExtension ()
  {
    return defaultExtension;
  }

  public int getDialogMode ()
  {
    return dialogMode;
  }

  /**
   * One of SWT.SAVE or SWT.OPEN. Has no effect for directory dialogs.
   */
  public void setDialogMode (int mode)
  {
    dialogMode = mode;
  }

  public void setDialogTitle (String string)
  {
    dialogTitle = string;
  }
  
  public String getDialogTitle ()
  {
    return dialogTitle;
  }
  
  public boolean isSelectDirectory ()
  {
    return selectDirectory;
  }

  public void setSelectDirectory (boolean b)
  {
    selectDirectory = b;
  }
  
  public String getDialogDescription ()
  {
    return dialogDescription;
  }
    
  /**
   * Set the description shown on the dialog. Only implemented for directory
   * dialogs.
   */
  public void setDialogDescription (String string)
  {
    dialogDescription = string;
  }

  public void handleEvent (Event e)
  {
    String newSelection = null;
    
    if (selectDirectory)
    {
      DirectoryDialog dialog = new DirectoryDialog (getShell ());      
      
      dialog.setFilterPath (fileField.getText ());
      dialog.setMessage (dialogDescription);

      newSelection = dialog.open ();
    } else
    {
      FileDialog dialog = new FileDialog (getShell (), dialogMode);
  
      dialog.setText (dialogTitle);

      if (dialogDirectory != null)
        dialog.setFilterPath (dialogDirectory);
          
      if (fileField.getText ().length () > 0)
        dialog.setFileName (fileField.getText ());
      
      if (dialogExtensions != null)
        dialog.setFilterExtensions (dialogExtensions);
      
      newSelection = dialog.open ();
      
      if (newSelection != null && defaultExtension != null)
        newSelection = Files.addDefaultExtension (newSelection, defaultExtension);
    }
    
    if (newSelection != null)
    {
      fileField.setText (newSelection);
      fileField.setFocus ();
      fileField.selectAll ();
    }
  }
}
