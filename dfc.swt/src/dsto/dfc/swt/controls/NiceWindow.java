package dsto.dfc.swt.controls;

import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.jface.window.Window;

import dsto.dfc.databeans.DataBean;
import dsto.dfc.databeans.IDataBean;

import dsto.dfc.swt.util.Geometry;

import dsto.dfc.util.Objects;

/**
 * Base class for "nice" windows that remember their last location and/or size.
 * Subclasses can set "sizeProperty" and/or "locationProperty" to be the name
 * of the properties used to store size and location (null means no save).
 * Subclasses may also override {@link #savePreferences()} to store other
 * persistent window properties.
 * 
 * @author mpp
 * @version $Revision$
 */
public class NiceWindow extends Window
{
  /** The global preferences store used by all windows that don't have an
   *  explicit preferences set.*/
  protected static final DataBean GLOBAL_PREFERENCES = new DataBean ();
  
  /** The window's preferences store. Initialized to GLOBAL_PREFERENCES if
   * none set. */
  protected IDataBean preferences;
  /** The name of the property in the preferences that stores the window
   * location (null = no save). */
  protected String sizeProperty;
  /** The name of the property in the preferences that stores the window
   * size (null = no save). */
  protected String locationProperty;
  
  public NiceWindow (Shell parentShell)
  {
    this (parentShell, GLOBAL_PREFERENCES);
  }
  
  public NiceWindow (Shell parentShell, IDataBean preferences)
  {
    super (parentShell);
    
    this.preferences = preferences;
  }
  
  protected Point getInitialSize ()
  {
    Point size = null;
    
    if (sizeProperty != null)
      size = (Point)preferences.getValue (sizeProperty);
      
    if (size == null)
      size = super.getInitialSize ();
      
    return size;
  }

  protected Point getInitialLocation (Point initialSize)
  {
    Point loc = null;
    
    if (locationProperty != null)
      loc = (Point)preferences.getValue (locationProperty);
    
    if (loc == null)
      loc = getDefaultLocation (initialSize);
    
    Rectangle clientArea = getShell ().getDisplay ().getClientArea ();
    Rectangle rect = new Rectangle (loc.x, loc.y, initialSize.x, initialSize.y);
    
    Geometry.bumpRectangle (clientArea, rect);
    
    loc.x = rect.x;
    loc.y = rect.y;
    
    if (locationProperty != null)
      preferences.setValue (locationProperty, loc, IDataBean.TRANSIENT);
    
    return loc;
  }
  
  protected Point getDefaultLocation (Point initialSize)
  {
    return super.getInitialLocation (initialSize);
  }

  public boolean close ()
  {
    if (getShell () != null && !getShell ().isDisposed ())
      savePreferences ();
    
    return super.close ();
  }

  /**
   * Override superclass version since it "optimizes" the initial position/size
   * logic away after first call. This version works correctly over an
   * open/close cycle.
   */
  protected void initializeBounds ()
  {
    Point size = getInitialSize ();
    Point location = getInitialLocation (size);
  
    getShell ().setBounds (location.x, location.y, size.x, size.y);
  }

  /**
   * Save the window preferences. Subclasses may override.
   */
  protected void savePreferences ()
  {
    Shell shell = getShell ();
    
    if (!shell.getMaximized ())
    {
      if (sizeProperty != null)
        preferences.setValue (sizeProperty, shell.getSize ());
        
      if (locationProperty != null)
      {
        Point newLocation = shell.getLocation ();
        Point oldLocation = (Point)preferences.getValue (locationProperty);
        
        if (!Objects.objectsEqual (oldLocation, newLocation))
          preferences.setValue (locationProperty, shell.getLocation ());
      }
    }
  }
}
