package dsto.dfc.swt.styles;

import dsto.dfc.swt.icons.Icon;
import dsto.dfc.swt.icons.Iconic;
import dsto.dfc.util.EnumerationValue;

/**
 * A logical display style.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public interface Style extends Iconic, EnumerationValue
{
  /**
   * The type of value for the style.
   */
  public Class getType ();

  /**
   * The user readable name of the style.
   */
  public String getText ();

  /**
   * A short (tooltip length) description of the style.
   */
  public String getDescription ();

  public Icon getIcon ();
  
  public String getIconicName ();
}
