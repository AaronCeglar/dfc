package dsto.dfc.swt.styles;

import java.io.Serializable;

import dsto.dfc.swt.icons.Icon;
import dsto.dfc.swt.icons.IconicEnumerationValue;
import dsto.dfc.swt.icons.Images;

import dsto.dfc.util.EnumerationValue;

/**
 * Base implementation of the Style interface. The intended use of
 * this class is to create a set of related styles, so this class also
 * implements the Enumeration interface. Subclasses of this class must
 * implement getEnumValues () to return all other styles in the style
 * set, and override readResolve () to call super.readResolve (). This
 * enables inheritance of the singleton deserialization logic from the
 * IconicEnumeration class.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public abstract class AbstractStyle
  extends IconicEnumerationValue implements Style, Serializable
{
  private static final long serialVersionUID = 1L;

  protected transient Class type;
  protected transient String description;

  protected AbstractStyle ()
  {
    // zip
  }

  /**
   * Creates a new <code>AbstractStyle</code> instance.
   *
   * @param name The logical unique name of the style. This is used
   * (via IconicEnumeration's deserialization) to resolve saved
   * instances to the singleton instance.
   * @param text The user-readable textual name of the style.
   * @param type The type of value for the style.
   * @param description A short (tooltip length) description of the
   * style.
   * @param iconResource The name of an icon resource for the
   * style. May be null for no icon.
   */
  public AbstractStyle (String name, String text, Class type,
                        String description, String iconResource)
  {
    this (name, text, type, description,
          iconResource == null ? null : Images.createIcon (iconResource));
  }

  /**
   * Creates a new <code>AbstractStyle</code> instance.
   *
   * @param name The logical unique name of the style. This is used
   * (via IconicEnumeration's deserialization) to resolve saved
   * instances to the singleton instance.
   * @param text The user-readable textual name of the style.
   * @param type The type of value for the style.
   * @param description A short (tooltip length) description of the
   * style.
   * @param icon The icon for the style. May be null for no icon.
   */
  public AbstractStyle (String name, String text, Class type,
                        String description, Icon icon)
  {
    super (name, text, icon);

    this.type = type;
    this.description = description;
  }

  public abstract EnumerationValue [] getEnumValues ();

  public Class getType ()
  {
    return type;
  }

  public String getDescription ()
  {
    return description;
  }

  protected Object readResolve () throws java.io.InvalidObjectException
  {
    return super.readResolve ();
  }
}
