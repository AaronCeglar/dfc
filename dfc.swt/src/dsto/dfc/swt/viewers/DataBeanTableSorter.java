package dsto.dfc.swt.viewers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableColumn;

import dsto.dfc.databeans.IDataBean;

/**
 * Enables auto sorting of a table of IDataBean's by clicking table headers.
 * Holding shift or ctrl down while clicking preserves the current sorting,
 * by adding the clicked column to the sort list rather than clearing it.
 */
public class DataBeanTableSorter extends ViewerSorter implements Listener
{
  private TableViewer tableView;
  private List defaultSortOptions;
  private List sortOptions;
  private Map comparators;
  
  /**
   * Create a new instance. 
   * 
   * @param tableView The table view to sort. The tableView's columns should be
   * fully configured and the column properties (see
   * {@link TableViewer#setColumnProperties(java.lang.String[])}) set to be the
   * data bean properties that are displayed in each column.
   * 
   * @see #addDefaultColumn(String, int)
   * @see #addColumn(String, int)
   * @see #setComparator(String, Comparator)
   */
  public DataBeanTableSorter (TableViewer tableView)
  {
    this.defaultSortOptions = Collections.EMPTY_LIST;
    this.sortOptions = new ArrayList ();
    this.comparators = Collections.EMPTY_MAP;
    this.tableView = tableView;
    
    TableColumn [] columns = tableView.getTable ().getColumns ();
    Object [] properties = tableView.getColumnProperties ();
    
    for (int i = 0; i < columns.length; i++)
    {
      TableColumn tableColumn = columns [i];
      
      tableColumn.setData ("property", properties [i]);
    
      tableColumn.addListener (SWT.Selection, this);
    }
  }
  
  /**
   * Add a column to the sort list.
   * 
   * @param property The property sort by.
   * @param order The sort order: 1 = ascending. -1 = descending.
   * 
   * @see #clearColumns()
   */
  public void addColumn (String property, int order)
  {
    sortOptions.add (new SortOption (property, order));
    
    tableView.refresh ();
  }
  
  /**
   * Add a column to the default list used when no columns are selected for
   * sorting.
   * 
   * @param property The property sort by.
   * @param order The sort order: 1 = ascending. -1 = descending.
   */
  public void addDefaultColumn (String property, int order)
  {
    if (defaultSortOptions == Collections.EMPTY_LIST)
      defaultSortOptions = new ArrayList ();
      
    defaultSortOptions.add (new SortOption (property, order));
    
    tableView.refresh ();
  }

  /**
   * Clear the sort order to the default.
   * 
   * @see #addColumn(String, int)
   */
  public void clearColumns ()
  {
    if (!sortOptions.isEmpty ())
    {
      sortOptions.clear ();
      tableView.refresh ();
    }
  }
  
  /**
   * Optionally set the comparator used to sort a given property.
   */
  public void setComparator (String property, Comparator comparator)
  {
    if (comparators == Collections.EMPTY_MAP)
      comparators = new HashMap ();
      
    comparators.put (property, comparator);
  }
  
  public boolean isSorterProperty (Object element, String property)
  {
    SortOption option = findOption (sortOptions, property);
    
    if (option == null || option.order == 0)
      option = findOption (defaultSortOptions, property);
      
    return option != null && option.order != 0;
  }
   
  public int compare (Viewer viewer, Object e1, Object e2)
  {
    IDataBean b1 = (IDataBean)e1;
    IDataBean b2 = (IDataBean)e2;
    
    int result = compareBeans (b1, b2, sortOptions);
    
    if (result == 0 && !defaultSortOptions.isEmpty ())
      result = compareBeans (b1, b2, defaultSortOptions);
      
    return result;
  }
  
  protected int compareBeans (IDataBean b1, IDataBean b2, List options)
  {
    for (int i = 0; i < options.size (); i++)
    {
      SortOption option = (SortOption)options.get (i);
      
      int result = compareBeans (b1, b2, option.property);
        
      result *= option.order;
      
      if (result != 0)
        return result;
    }
    
    return 0;
  }

  protected int compareBeans (IDataBean b1, IDataBean b2, String property)
  {
    Object v1 = b1.getValue (property);
    Object v2 = b2.getValue (property);
    
    Comparator comparator = (Comparator)comparators.get (property);
    
    int result;
    
    if (comparator == null)
    {
      if (v1 == v2)
        result = 0;
      else if (v1 == null)
        result = -1; // ie null < something
      else if (v2 == null)
        result = 1;  // ie something > null
      else
        result = ((Comparable)v1).compareTo (v2);
    } else
    {
      result = comparator.compare (v1, v2);
    }
      
    return result;
  }
  
  public void handleEvent (Event e)
  {
    String property =
      ((TableColumn)e.widget).getData ("property").toString ();
   
    SortOption option = findOption (sortOptions, property);
     
    if ((e.stateMask & (SWT.CTRL | SWT.SHIFT)) == 0)
    {
      // clear options and add new
      
      sortOptions.clear ();
      
      if (option == null)
        option = new SortOption (property);
      else if (option.order == 1)
        option.order = -1;
      else
        option = null;
        
      if (option != null)
        sortOptions.add (option);
    } else
    {
      // preserve the current sort options
      
      if (option == null)
        sortOptions.add (new SortOption (property));
      else if (option.order == 1)
        option.order = -1;
      else if (option.order == -1)
        option.order = 0;
    }
    
    tableView.refresh ();
  }

  private static SortOption findOption (List options, String property)
  {
    for (int i = 0; i < options.size (); i++)
    {
      SortOption option = (SortOption)options.get (i);
      
      if (option.property.equals (property))
        return option;
    }
    
    return null;
  }
  
  private static class SortOption
  {
    public String property;
    public int order;
    
    public SortOption (String property)
    {
      this.property = property;
      this.order = 1;
    }
    
    public SortOption (String property, int order)
    {
      this.property = property;
      this.order = order;
    }
  }
}