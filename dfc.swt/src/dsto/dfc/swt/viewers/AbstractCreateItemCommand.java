package dsto.dfc.swt.viewers;

import java.util.Collection;

import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.StructuredViewer;

import dsto.dfc.swt.commands.AbstractCommand;

/**
 * Abstract base class for commands that create new items in a collection
 * being viewed by a JFace StructuredViewer. Handles common behaviour of
 * creating then selecting new items. Subclasses can simply implement
 * {@link #createItem()}.
 * 
 * @author mpp
 * @version $Revision$
 */
public abstract class AbstractCreateItemCommand extends AbstractCommand
{
  protected StructuredViewer view;
  protected Collection items;
  
  public AbstractCreateItemCommand (StructuredViewer view)
  {
    this (view, null);
  }
  
  /**
   * Create a new instance.
   * 
   * @param view The viewer that is showing the groups.
   * @param items The collection to add new items to. It is assumed that the
   * viewer is listening for changes.
   */
  public AbstractCreateItemCommand (StructuredViewer view, Collection items)
  {
    super ("edit.New Item", NO_ICON, "edit", "Create a new item", true, '\0', 0);
    
    this.view = view;
    this.items = items;
  }

  public StructuredViewer getView ()
  {
    return view;
  }

  public Collection getItems ()
  {
    return items == null ? (Collection)view.getInput () : items;
  }

  public void execute ()
  {
    Object item = createItem ();
    
    if (item != null)
    {
      addItem (item);
      
      fireCommandExecuted (item);
    }
  }

  protected void addItem (Object item)
  {
    getItems ().add (item);
    
    view.setSelection (new StructuredSelection (item), true);
    
    view.getControl ().setFocus ();
  }
  
  /**
   * Subclasses should implement this to create a new item.
   * 
   * @return The new item, or null if no item to be added (eg dialog was
   * cancelled).
   */
  protected abstract Object createItem ();
}
