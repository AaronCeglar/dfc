package dsto.dfc.swt.viewers;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerSorter;

import dsto.dfc.swt.DFC_SWT;

/**
 * Adds click-column-to-sort behaviour to a table viewer. Clicking a
 * column sorts it if a sorter is defined. Clicking again inverts sort
 * order. Clicking a third time clears sort.<p>
 * 
 * Example use:
 * 
 * <pre>
 *   TableColumnSorter columnSorter  = new TableColumnSorter (tableView);
 *   
 *   TableColumn column = new TableColumn (tableView.getTable (), SWT.LEFT);
 *   column.setText ("Name");
 *   column.setWidth (40);
 *   
 *   columnSorter.addColumn (column, "name", new DataObjectViewerSorter ("name"));
 *   
 *   column = new TableColumn (tableView.getTable (), SWT.RIGHT);
 *   column.setText ("Age");
 *   column.setWidth (20);
 *   
 *   columnSorter.addColumn (column, "age", new DataObjectViewerSorter ("age"));
 *   
 *   ...
 *   
 *   columnSorter.setSortColumn ("name");
 *   tableView.setColumnProperties (columnSorter.getColumnProperties ());
 * </pre>
 * 
 * @author Matthew Phillips
 */
public class TableColumnSorter implements Listener
{
  public static final int UNSORTED = 0;
  public static final int ASCENDING = 1;
  public static final int DESCENDING = 2;
  
  /**
   * Allow the the table column to be unsorted
   */
  private boolean allowUnsorted;
  
  private TableViewer viewer;
  private TableColumn sortColumn;
  /* 
   * UNSORTED, ASCENDING, DESCENDING if allowUnsorted = true, otherwise
   * toggles between ASCENDING AND DESCENDING
   */
  private int ordering; 

  public TableColumnSorter (TableViewer viewer)
  {
    this.viewer = viewer;
    this.ordering = UNSORTED;
    this.sortColumn = null;
    this.allowUnsorted = false;
  }
  
  /**
   * Instantiate a TableColumnSorter, specifying whether the sort column may be 'UNSORTED'. 
   * See {@link TableColumnSorter#UNSORTED} 
   * @param viewer
   * @param allowUnsorted Specify whether the sort column can be 'UNSORTED'. Note if passed 
   * in as false, it is equivalent to calling {@link TableColumnSorter#TableColumnSorter(TableViewer)} 
   */
  public TableColumnSorter (TableViewer viewer, boolean allowUnsorted)
  {
    this (viewer);
    
    if (!allowUnsorted)
      this.ordering = ASCENDING;
    
    this.allowUnsorted = allowUnsorted; 
  }

  /**
   * Shortcut to get the array of properties associated with the
   * columns using
   * {@link #addColumn(TableColumn, String, ViewerSorter)}. This can
   * be passed into
   * {@link TableViewer#setColumnProperties(java.lang.String[])}.
   */
  public String [] getColumnProperties ()
  {
    TableColumn [] columns = viewer.getTable ().getColumns ();
    ArrayList properties = new ArrayList (columns.length);
    
    for (int i = 0; i < columns.length; i++)
    {
      String property = (String)columns [i].getData ("property");
      
      if (property != null)
        properties.add (property);
      else
        throw new IllegalArgumentException
          ("Missing property for column " + columns [i].getText ());
    }
    
    return (String [])properties.toArray (new String [properties.size ()]);
  }
  
  /**
   * Add a column with no sorter.
   * 
   * @param column The column.
   * @param property The property associated with the column.
   */
  public void addColumn (TableColumn column, String property)
  {
    addColumn (column, property, null);
  }
  
  /**
   * Add a column.
   * 
   * @param column The column.
   * @param property The property associated with the column.
   * @param sorter The sorter for the column. May be null.
   */
  public void addColumn (TableColumn column, String property, ViewerSorter sorter)
  {
    if (sorter != null)
    {
      column.setData ("sorter", sorter);
      column.addListener (SWT.Selection, this);
    }
    
    column.setData ("property", property);
  }
  
  /**
   * Shortcut to add a left-aligned column with a set property, title
   * and suggested column width.
   * 
   * @param property The property the column is displaying.
   * @param title The title of the column.
   * @param columns The suggested column width in characters.
   * 
   * @return The column item created.
   * 
   * @see #addColumn(String, String, ViewerSorter, int, int)
   */
  public TableColumn addColumn (String property, String title, int columns)
  {
    return addColumn (property, title,
                      new DataObjectViewerSorter (property), columns, SWT.LEFT);
  }
  
  /**
   * Shortcut to add a column with a set property, title
   * and suggested column width.
   * 
   * @param property The property the column is displaying.
   * @param title The title of the column.
   * @param columns The suggested column width in characters.
   * @param style The column style (SWT.LEFT, SWT.RIGHT, SWT.CENTER).
   * 
   * @return The column item created.
   * 
   * @see #addColumn(String, String, ViewerSorter, int, int)
   */
  public TableColumn addColumn (String property, String title,
                                int columns, int style)
  {
    return addColumn (property, title,
                      new DataObjectViewerSorter (property), columns, style);
  }

  /**
   * Shortcut to add a column with a set property, title, sorter and
   * suggested column width.
   * 
   * @param property The property the column is displaying.
   * @param title The title of the column.
   * @param sorter The sorter for the column (may be null).
   * @param columns The suggested column width in characters.
   * @param style The column style (SWT.LEFT, SWT.RIGHT, SWT.CENTER).
   * 
   * @return The column item created.
   */
  public TableColumn addColumn (String property, String title,
                                ViewerSorter sorter, int columns, int style)
  {
    Table table = viewer.getTable ();
    int baseUnit = DFC_SWT.getTextUnitWidth (table);
    
    TableColumn column = new TableColumn (table, style);
    column.setText (title);
    column.setWidth (columns * baseUnit);
    column.setMoveable (true);
    
    addColumn (column, property, sorter);
    
    return column;
  }

  /**
   * Set the currently sorted column to be the one displaying a given
   * property. Sort order is ASCENDING.
   * 
   * @see #setSortColumn(String, int)
   */
  public void setSortColumn (String property)
  {
    setSortColumn (property, ASCENDING);
  }
  
  /**
   * Set the currently sorted column to be the one displaying a given
   * property.
   * 
   * @param property The property for the sorted column.
   * @param newOrdering The new sort order: UNSORTED, ASCENDING, DESCENDING.
   */
  public void setSortColumn (String property, int newOrdering)
  {
    sortColumn = columnForProperty (property);
    ordering = newOrdering;
    updateSorter ();
  }

  private TableColumn columnForProperty (String property)
  {
    TableColumn [] columns = viewer.getTable ().getColumns ();
    
    for (int i = 0; i < columns.length; i++)
    {
      TableColumn column = columns [i];
      String p = (String)column.getData ("property");
      
      if (p != null && property.equals (p))
        return column;
    }
    
    throw new IllegalArgumentException ("No column for property " + property);
  }

  private void toggleSortColumn (TableColumn column)
  {
    if (column == null)
      ordering = UNSORTED;
    else if (column != sortColumn)
      ordering = ASCENDING;
    else if (allowUnsorted)
      ordering = (ordering + 1) % 3;
    else 
      ordering = ordering == ASCENDING ? DESCENDING : ASCENDING;
    
    sortColumn = column;

    updateSorter ();
  }

  private void updateSorter ()
  {
    ViewerSorter sorter;
    
    if (ordering == UNSORTED)
      sorter = null;
    else if (ordering == ASCENDING)
      sorter = sorterFor (sortColumn);
    else
      sorter = new InverseViewerSorter (sorterFor (sortColumn));
    
    viewer.setSorter (sorter);
    
    if (ordering == UNSORTED)
      sortColumn = null;
    
    Table table = viewer.getTable ();
    
    table.setSortColumn (sortColumn);
    table.setSortDirection (ordering == ASCENDING ? SWT.UP : SWT.DOWN);
  }

  private ViewerSorter sorterFor (TableColumn column)
  {
    ViewerSorter sorter = (ViewerSorter)column.getData ("sorter");
    
    if (sorter != null)
      return sorter;
    else
      throw new IllegalArgumentException ("No sorter for " + column.getText ());
  }

  public void handleEvent (Event e)
  {
    toggleSortColumn ((TableColumn)e.widget);
  }
}
