package dsto.dfc.swt.viewers;

import java.util.Iterator;
import java.util.List;

import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.Viewer;

import dsto.dfc.collections.CollectionEvent;
import dsto.dfc.collections.DfcMonitoredList;
import dsto.dfc.collections.DfcMonitoredListListener;
import dsto.dfc.collections.MonitoredCollection;
import dsto.dfc.swt.commands.AbstractCommand;

/**
 * Generic command to move items up/down in a viewer that is displaying a
 * java.util.List. If the list is a DfcMonitoredList (which supports in-place
 * movement of items) move () is used rather than remove/add.
 */
public class CmdMoveItem
  extends AbstractCommand
  implements ISelectionChangedListener, DfcMonitoredListListener, DisposeListener
{
  public static final int UP = -1;
  public static final int DOWN = 1;

  protected Viewer viewer;
  protected List items;
  protected int direction;

  /**
   * Create a new instance.
   *
   * @param viewer The viewer.
   * @param direction The direction to move the entries (MOVE_UP or
   * MOVE_DOWN).
   * 
   * @see #setInput(List)
   */
  public CmdMoveItem (Viewer viewer, int direction)
  {
    super ("edit.Move " + (direction == UP ? "Up" : "Down"),
           (direction == UP ? "arrow_up.gif" : "arrow_down.gif"),
           "move",
           "Move the selected item " + (direction == UP ? "up" : "down"),
           false, '\0', 0);

    this.viewer = viewer;
    this.direction = direction;
    
    viewer.addSelectionChangedListener (this);
    viewer.getControl ().addDisposeListener (this);
    
    inputChanged (null, (List)viewer.getInput ());

    updateEnabled ();
  }

  public void dispose ()
  {
    viewer.removeSelectionChangedListener (this);

    inputChanged (items, null);
  }

  /**
   * Notify the command that the input to the viewer has changed (there is no
   * automatic way to do this). If the items list also implements
   * {@link dsto.dfc.collections.MonitoredCollection} it is tracked and the
   * command will be correctly enabled/disabled depending on the current
   * context.
   */
  public void setInput (List newItems)
  {
    inputChanged (items, newItems);
    
    updateEnabled ();
  }
  
  protected void inputChanged (List oldInput, List newInput)
  {
    if (oldInput instanceof MonitoredCollection)
      ((MonitoredCollection)oldInput).removeCollectionListener (this);
      
    if (newInput instanceof MonitoredCollection)
      ((MonitoredCollection)newInput).addCollectionListener (this);
      
    items = newInput;
  }
  
  public void execute ()
  {
    List toMove = ((IStructuredSelection)viewer.getSelection ()).toList ();

    // handle non-monitorable list case where command is always enabled but
    // may not be possible for current selection
    if (!(items instanceof MonitoredCollection) &&
         (items.isEmpty () || !canMove (toMove)))
    {
      return;
    }

    for (int i = 0; i < toMove.size (); i++)
    {
      int oldIndex = items.indexOf (toMove.get (i));
      
      moveItem (oldIndex, oldIndex + direction);
    }
    
    fireCommandExecuted (toMove);
  }
  
  protected void moveItem (int oldIndex, int newIndex)
  {
    // NOTE: both options below swap old with new to avoid removing old and
    // causing it to be deselected in the viewer
    if (items instanceof DfcMonitoredList)
    {
      ((DfcMonitoredList)items).move (newIndex, oldIndex);
    } else
    {
      Object swapItem = items.remove (newIndex);
    
      items.add (oldIndex, swapItem);
    }
  }

  /**
   * Update the enabled status of the command given the current selected items.
   * Default is to be enabled when items is not empty and canMove () is true.
   */
  protected void updateEnabled ()
  {
    if (items instanceof MonitoredCollection)
    {
      List toMove = ((IStructuredSelection)viewer.getSelection ()).toList (); 
      
      setEnabled (!toMove.isEmpty () && canMove (toMove));
    } else
    {
      setEnabled (true);
    }
  }
  
  protected boolean canMove (List toMove)
  {    
    for (Iterator i = toMove.iterator (); i.hasNext (); )
    {
      int itemIndex = items.indexOf (i.next ()) + direction;
      
      if (itemIndex < 0 || itemIndex >= items.size ())
        return false;
    }
    
    return true;
  }

  // ISelectionListener
  
  public void selectionChanged (SelectionChangedEvent e)
  {
    updateEnabled ();
  }
  
  // CollectionListener

  public void elementsAdded (CollectionEvent e)
  {
    updateEnabled ();
  }
  
  public void elementsRemoved (CollectionEvent e)
  {
    updateEnabled ();
  }
  
  public void elementMoved (CollectionEvent e)
  {
    updateEnabled ();
  }

  // DisposeListener

  public void widgetDisposed (DisposeEvent e)
  {
    dispose ();
  }
}
