package dsto.dfc.swt.viewers;

import org.eclipse.swt.graphics.Image;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;

import dsto.dfc.databeans.IDataObject;

/**
 * Label provider for views of IDataObject's that presents a given
 * list of property values.
 * 
 * @author Matthew Phillips
 */
public class DataObjectLabelProvider extends LabelProvider
  implements ITableLabelProvider
{
  protected Object [] properties;

  public DataObjectLabelProvider (Object property)
  {
    this (new Object [] {property});
  }
  
  /**
   * Create a new instance.
   * 
   * @param properties The data object properties to be displayed.
   *          These indices of these must match the columns in the
   *          table.
   */
  public DataObjectLabelProvider (Object [] properties)
  {
    this.properties = properties;
  }
  
  public Image getColumnImage (Object element, int columnIndex)
  {
    return null;
  }

  public String getColumnText (Object element, int columnIndex)
  {
    return getText (getColumnValue (element, columnIndex));
  }

  protected Object getColumnValue (Object element, int columnIndex)
  {
    return ((IDataObject)element).getValue (properties [columnIndex]);
  }
  
  public boolean isLabelProperty (Object element, String property)
  {
    for (int i = 0; i < properties.length; i++)
    {
      if (properties [i].equals (property))
        return true;
    }
    
    return false;
  }
}
