package dsto.dfc.swt.viewers;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.graphics.Image;

import dsto.dfc.databeans.DataObjects;
import dsto.dfc.databeans.IDataBean;
import dsto.dfc.databeans.IDataObject;
import dsto.dfc.databeans.PropertyEvent;
import dsto.dfc.databeans.PropertyListener;
import dsto.dfc.swt.util.UIPropertyListener;

/**
 * A table content and label provider for IDataObject values that
 * presents a data object as a two-column property/value list. The
 * elements of this provider are the input's properties.
 *
 * @author mpp
 * @version $Revision$
 *
 * @see dsto.dfc.databeans.IDataObject
 */
public class DataObjectPropertyListProvider
  extends LabelProvider
  implements IStructuredContentProvider, ITableLabelProvider, PropertyListener
{
  private TableViewer viewer;
  private IDataObject object;
  private UIPropertyListener listener;

  @Override
  public void dispose ()
  {
    // zip
  }

  public Object [] getElements (Object inputElement)
  {
    if (inputElement instanceof IDataBean)
    {
      return ((IDataBean)inputElement).getPropertyNames ();
    } else
    {
      return DataObjects.propertySet ((IDataObject)inputElement).toArray ();
    }
  }

  public void inputChanged (Viewer newViewer, Object oldInput, Object newInput)
  {
    IDataObject oldObject = (IDataObject)oldInput;
    IDataObject newObject = (IDataObject)newInput;

    if (oldObject != null)
    {
      listener.dispose (oldObject);
      listener = null;
    }

    viewer = (TableViewer)newViewer;
    object = newObject;

    if (newObject != null)
    {
      listener =
        new UIPropertyListener (viewer.getControl ().getDisplay (), this);

      newObject.addPropertyListener (listener);
    }
  }

  // ITableLabelProvider interface

  public Image getColumnImage (Object element, int columnIndex)
  {
    return null;
  }

  public String getColumnText (Object element, int columnIndex)
  {
    if (columnIndex == 0)
    {
      return element.toString ();
    } else
    {
      if (object == null)
        return "";

      // store value here in case it changes (ticket #333)
      Object value = object.getValue (element);

      return value != null ? value.toString () : "";
    }
  }

  public void propertyValueChanged (PropertyEvent e)
  {
    if (e.path.length () == 1)
    {
      Object property = e.path.first ();

      if (e.newValue == null)
        viewer.remove (property);
      else if (e.oldValue == null)
        viewer.add (property);
      else
        viewer.update (property, null);
    }
  }
}
