package dsto.dfc.swt.viewers;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;

import dsto.dfc.util.DefaultComparator;

import dsto.dfc.databeans.IDataObject;

/**
 * Viewer sorter for sorting views of IDataObject's by a given property.
 * 
 * @author Matthew Phillips
 */
public class DataObjectViewerSorter extends ViewerSorter
{
  private Object sortProperty;
  private boolean inverted;

  public DataObjectViewerSorter (Object sortProperty)
  {
    this (sortProperty, false);
  }
  
  public DataObjectViewerSorter (Object sortProperty, boolean inverted)
  {
    this.sortProperty = sortProperty;
    this.inverted = inverted;
  }

  public int compare (Viewer viewer, Object e1, Object e2)
  {
    int cat1 = category (e1);
    int cat2 = category (e2);

    int comparison = cat1 - cat2;
    if (comparison == 0)
    {
      Object value1 = ((IDataObject)e1).getValue (sortProperty);
      Object value2 = ((IDataObject)e2).getValue (sortProperty);
      
      if (value1 instanceof String && value2 instanceof String)
        comparison = getComparator ().compare (value1, value2);
      else
        comparison = DefaultComparator.instance ().compare (value1, value2);
    }
    return inverted ? -comparison : comparison;
  }

  @Override
  public boolean isSorterProperty (Object element, String property)
  {
    return property.equals (sortProperty);
  }
}
