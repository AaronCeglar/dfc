package dsto.dfc.swt.viewers;

import java.util.Collection;
import java.util.List;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;

import dsto.dfc.swt.commands.AbstractCommand;
import dsto.dfc.util.Disposable;

/**
 * Generic "Delete Items" command for JFace Viewer's showing a Collection
 * (although could be subclassed for non Collection's). The default behaviour
 * is to remove () the selected items from the collection. Subclasses may
 * choose to override {@link #canDelete(List)}, {@link #confirmDelete},
 * {@link #deleteItems(List)} and {@link #updateEnabled()} to customize
 * behaviour.
 * 
 * @author mpp
 * @version $Revision$
 */
public class CmdDeleteItem
  extends AbstractCommand implements ISelectionChangedListener, Disposable
{
  protected Viewer viewer;
  protected Collection items;
  protected boolean confirmDelete;
  protected String confirmMessage;
  
  /**
   * Create a new instance. The items to delete from are assumed to be the
   * viewer's current input (ie from its getInput () method).
   */
  public CmdDeleteItem (Viewer viewer)
  {
    this (viewer, null);
  }
  
  /**
   * Create a new instance.
   * 
   * @param viewer The viewer.
   * @param items The items to delete from.
   */
  public CmdDeleteItem (Viewer viewer, Collection items)
  {
    super ("edit.Delete Item", "delete.gif", "edit", "Delete the selected item",
           false, 'd', SWT.CTRL | 'd');
    
    this.viewer = viewer;
    this.items = items;
    this.confirmMessage = "Really delete the selected items?";
    
    viewer.addSelectionChangedListener (this);

    updateEnabled ();
  }

  public void dispose ()
  {
    viewer.removeSelectionChangedListener (this);
  }

  public boolean isConfirmDelete ()
  {
    return confirmDelete;
  }

  /**
   * Set whether the default confirmDelete () method will ask the user to
   * confirm the operation.
   */
  public void setConfirmDelete (boolean confirmDelete)
  {
    this.confirmDelete = confirmDelete;
  }

  /**
   * Set the message that is displayed in the confirm dialog.
   */
  public void setConfirmMessage (String message)
  {
    this.confirmMessage = message;
  }
  
  public String getConfirmMessage ()
  {
    return confirmMessage;
  }
  
  public void execute ()
  {
    List toDelete = ((IStructuredSelection)viewer.getSelection ()).toList ();
    
    if (confirmDelete (toDelete))
    {
      deleteItems (toDelete);
      
      fireCommandExecuted ();
    }
  }
  
  protected Collection getItems ()
  {
    return items == null ? (Collection)viewer.getInput () : items;
  }
  
  /**
   * Delete the given items.
   */
  protected void deleteItems (List toDelete)
  {
    getItems ().removeAll (toDelete);
  }
 
  /**
   * Confirm with the user that it's OK to delete the given items. This
   * implementation pops up a yes/no dialog if confirmDelete is true.
   * 
   * @return true if the items can be safely deleted.
   */
  protected boolean confirmDelete (List toDelete)
  {
    if (confirmDelete)
    {
      MessageBox dialog =
        new MessageBox (viewer.getControl ().getShell (),
                        SWT.ICON_QUESTION | SWT.YES | SWT.NO);
      
      dialog.setText (getDisplayName ());
      dialog.setMessage (confirmMessage);

      return dialog.open () == SWT.YES;
    } else
    {
      return true;
    }
  }
  
  /**
   * Check if all the items in the list can be deleted (eg some items may be
   * undeletable).
   * 
   * @return True if all the items in the list can be deleted.
   */
  protected boolean canDelete (List toDelete)
  {
    return true;
  }
  
  /**
   * Update the enabled status of the command given the current selected items.
   * Default is to be enabled when items is not empty and canDelete () is true.
   */
  protected void updateEnabled ()
  {
    List toDelete = ((IStructuredSelection)viewer.getSelection ()).toList (); 
    
    setEnabled (!toDelete.isEmpty () && canDelete (toDelete));
  }
  
  public void selectionChanged (SelectionChangedEvent e)
  {
    updateEnabled ();
  }
}
