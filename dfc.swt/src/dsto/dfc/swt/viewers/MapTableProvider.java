package dsto.dfc.swt.viewers;

import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.graphics.Image;

/**
 * A table content and label provider for a Map instance.
 * The data in the map instance is presented as a 2 column
 * property/value list.
 * 
 * @author David Karunaratne
 */
public class MapTableProvider
  extends LabelProvider
  implements IStructuredContentProvider, ITableLabelProvider
{
  
  public Object [] getElements (Object object)
  {
    // return an an array of entries
    return ((Map)object).entrySet ().toArray ();
  }

  public void inputChanged (Viewer newViewer, Object oldInput, Object newInput)
  { 
    // nothing to do, except to do a type check.
    if (newInput != null && !(newInput instanceof Map))
      throw new IllegalArgumentException ("New input is not of type Map");
  }
  
  // ITableLabelProvider interface
  
  public Image getColumnImage (Object element, int columnIndex)
  {
    return null;
  }

  public String getColumnText (Object object, int colIndex)
  {
    Map.Entry<Object, Object> entry = (Entry<Object, Object>) object;
    return (colIndex == 0 ? entry.getKey ().toString () : entry.getValue ().toString ());
  }
}
