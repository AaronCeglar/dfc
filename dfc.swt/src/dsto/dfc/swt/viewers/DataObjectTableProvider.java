package dsto.dfc.swt.viewers;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;

import dsto.dfc.databeans.IDataObject;
import dsto.dfc.databeans.PropertyEvent;
import dsto.dfc.databeans.PropertyListener;

import dsto.dfc.swt.util.UIPropertyListener;

/**
 * Table provider that makes all property values of an input
 * IDataObject into rows. This is often used where the property values
 * are all IDataObject's e.g. for a SetDataObject or a registry,
 * coupled with a {@link DataObjectLabelProvider}.
 * 
 * @author Matthew Phillips
 */
public class DataObjectTableProvider
  implements IStructuredContentProvider, PropertyListener
{
  private UIPropertyListener propertyListener;
  private TableViewer viewer;
  private IDataObject inputObject;

  public void dispose ()
  {
    // zip
  }

  public void inputChanged (Viewer newViewer, Object oldInput, Object newInput)
  {
    IDataObject oldObject = (IDataObject)oldInput;
    IDataObject newObject = (IDataObject)newInput;
    
    viewer = (TableViewer)newViewer;

    if (oldObject != null)
    {
      oldObject.removePropertyListener (propertyListener);
      
      propertyListener.dispose ();
      propertyListener = null;
    }
    
    if (newObject != null)
    {
      propertyListener =
        new UIPropertyListener (viewer.getControl ().getDisplay (), this);
      
      newObject.addPropertyListener (propertyListener);
    }
    
    inputObject = (IDataObject)newInput;
  }

  public Object [] getElements (Object inputElement)
  {
    IDataObject object = (IDataObject)inputElement;
    ArrayList elements = new ArrayList ();
    
    for (Iterator i = object.propertyIterator (); i.hasNext ();)
      elements.add (object.getValue (i.next ()));
    
    return elements.toArray ();
  }
  
  public void propertyValueChanged (PropertyEvent e)
  {
    if (e.path.length () == 1)
    {
      if (e.oldValue != null)
        viewer.remove (e.oldValue);
      
      if (e.newValue != null)
        viewer.add (e.newValue);
    } else if (e.path.length () == 2)
    {
      Object element = inputObject.getValue (e.path.first ());
      
      /*
       * It's vaguely possible that the element has disappeared
       * shortly before this property change gets processed,
       * especially if we're using Display.asyncExec () in a
       * multi-threaded client.
       */
      if (element == null)
        return;
      
      /*
       * JFace up to and including 3.2.0 has a bug (#24521:
       * https://bugs.eclipse.org/bugs/show_bug.cgi?id=24521) that
       * means calling update () on an invisible filtered item won't
       * make it visible again. Workaround is to call add () .
       * 
       * todo replace add () with update () code when bug #24521 is fixed
       */
    
      // todo hide non-string properties?
      // todo notify of changes more than a single level in?
      if (viewer.testFindItem (element) != null)
        viewer.update (element, new String [] {e.path.last ().toString ()});
      else
        viewer.add (element);
    }
  }
}
