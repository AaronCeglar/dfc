package dsto.dfc.swt.viewers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.Viewer;

import dsto.dfc.logging.Log;
import dsto.dfc.util.Beans;

/**
 * Binds the selected entry/entries of a JFace Viewer to the value of
 * a property on a target object.
 * <p>
 * 
 * Subclasses may override
 * {@link #getTargetValue(IStructuredSelection)} and
 * {@link #translate(Object)} to customize behaviour.
 * 
 * @author mpp
 * @version $Revision$
 */
public class SelectionBinding implements ISelectionChangedListener
{
  protected static final int SINGLE = 0;
  protected static final int LIST = 1;
  protected static final int ARRAY = 2;

  protected Viewer viewer;
  protected Object target;
  protected String targetProperty;
  /** One of the TYPE_* constants. */
  protected int targetType;
  /** The container type for multi-value properties (null for single value) */
  protected Class multiValueType;
  /** The type of the property, or type of an element for multi-value properties */
  protected Class propertyType;
  
  public SelectionBinding (Viewer viewer, Object target, String targetProperty)
  {
    this (viewer, target, targetProperty, false);
  }
  
  /**
   * Create a new instance.
   * 
   * @param viewer The viewer to read the selection from.
   * @param target The target object to set the value on.
   * @param targetProperty The property (with a setter method) to copy the 
   * value to.
   * @param allowMultipleValues True if multiple values can be passed to
   * the property. If true, the property must must be compatible with either
   * java.util.List or Object [].
   */
  public SelectionBinding (Viewer viewer, Object target, String targetProperty,
                            boolean allowMultipleValues)
  {
    this.viewer = viewer;
    this.target = target;
    this.targetProperty = targetProperty;
    
    try
    {
      if (allowMultipleValues)
      {
        multiValueType =
          Beans.getPropertyClass (target.getClass (), targetProperty);
        
        if (List.class.isAssignableFrom (multiValueType))
        {  
          targetType = LIST;
          propertyType = Object.class;
        } else if (Object [].class.isAssignableFrom (multiValueType))
        {  
          targetType = ARRAY;
          propertyType = multiValueType.getComponentType ();
        } else
          throw new IllegalArgumentException
            ("Cannot handle multiple value binding for " + multiValueType);
      } else
      {
        targetType = SINGLE;
        propertyType = Beans.getPropertyClass (target.getClass (), targetProperty);
      }

      loadTargetValue ();
    } catch (NoSuchMethodException ex)
    {
      IllegalArgumentException ex2 = new IllegalArgumentException
        ("Missing property \"" + targetProperty + "\": " + ex.getMessage ());
      
      ex2.initCause (ex);
      
      throw ex2;
    } catch (InvocationTargetException ex)
    {
      Log.diagnostic ("Property write exception", this, ex.getTargetException ());
      
      throw new IllegalArgumentException
        ("Error in write of property \"" + targetProperty + "\": " +
         ex.getTargetException ());
    }
    
    viewer.addSelectionChangedListener (this);
  }
  
  public void dispose ()
  {
    viewer.removeSelectionChangedListener (this);
  }
  
  private void loadTargetValue ()
    throws NoSuchMethodException, InvocationTargetException
  {
    IStructuredSelection selection =
      (IStructuredSelection)viewer.getSelection ();
    
    Beans.setPropertyValue (target, targetProperty, getTargetValue (selection));
  }
  
  /**
   * Get the value that should be applied to the target for a given selection.
   * Default implementation returns the first element (or null) for single
   * valued properties or the List or Object [] version if multi valued.
   * Subclasses may override.
   * 
   * @see #translate(Object)
   */
  protected Object getTargetValue (IStructuredSelection selection)
  {
    if (targetType == SINGLE)
    {
      Object item = null;
      
      if (selection.size () == 1)
      {  
        item = translate (selection.getFirstElement ());
        
        if (!compatibleType (item))
          item = null;
      }
      
      return item;
    } else
    {
      List values = selection.toList ();
      ArrayList newValues = new ArrayList (values.size ());
      
      for (Iterator i = values.iterator (); i.hasNext (); )
      {
        Object newValue = translate (i.next ());
          
        if (compatibleType (newValue)) 
      	  newValues.add (newValue);
      }
      
      if (targetType == ARRAY)
      {
        Object [] newValuesArray =
         (Object [])Array.newInstance
           (multiValueType.getComponentType (), newValues.size ());
        
        newValues.toArray (newValuesArray);
        
        return newValuesArray;
      } else
      {
        return newValues;
      }
    }
  }
  
  /**
   * Test if a value is a compatible type for the property. Default is to
   * allow null and any value assignable to the property type. Subclasses
   * may override. 
   */
  protected boolean compatibleType (Object value)
  {
    return value == null ||
            propertyType.isAssignableFrom (value.getClass ());
  }

  /**
   * Translate a selected value into the form that the target object expects.
   * Default is no translation. Subclasses may override.
   */
  protected Object translate (Object selectedObject)
  {
    return selectedObject;
  }

  public void selectionChanged (SelectionChangedEvent event)
  {
    try
    {
      loadTargetValue ();
    } catch (NoSuchMethodException ex)
    {
      Log.internalError ("Missing property on " + target.getClass (), this, ex);
    } catch (InvocationTargetException ex)
    {
      Log.internalError
        ("Error in property write " + targetProperty, this, ex);
    }
  }
}
