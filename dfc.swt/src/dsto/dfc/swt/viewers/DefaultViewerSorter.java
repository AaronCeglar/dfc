package dsto.dfc.swt.viewers;

import java.text.Collator;

import org.eclipse.jface.viewers.ViewerSorter;

/**
 * Default view sorter that uses the standard text collator. This is needed
 * only because, for some reason, {@link org.eclipse.jface.viewers.ViewerSorter}
 * is abstract.
 * 
 * @author mpp
 * @version $Revision$
 */
public class DefaultViewerSorter extends ViewerSorter
{
  public DefaultViewerSorter ()
  {
    // zip
  }

  public DefaultViewerSorter (Collator collator)
  {
    super (collator);
  }
}
