package dsto.dfc.swt.viewers;

import java.util.Collection;
import java.util.Iterator;

import org.eclipse.swt.widgets.Display;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;

import dsto.dfc.collections.CollectionEvent;
import dsto.dfc.collections.DfcMonitoredListListener;
import dsto.dfc.collections.MonitoredCollection;

/**
 * An IStructuredContentProvider that loads the content of any Collection into
 * a JFace TableViewer. If the collection implements the
 * {@link dsto.dfc.collections.MonitoredCollection} and/or
 * {@link dsto.dfc.collections.DfcMonitoredList} interfaces, changes to the
 * collection are mirrored through to the table.
 * 
 * @author mpp
 */
public class CollectionTableContentProvider
  implements IStructuredContentProvider, DfcMonitoredListListener
{
  protected TableViewer table;
  
  public CollectionTableContentProvider ()
  {
    // zip
  }
  
  public void dispose ()
  {
    // zip
  }

  // IStructuredContentProvider interface
      
  public Object [] getElements (Object inputElement)
  {
    return ((Collection)inputElement).toArray ();
  }

  public void inputChanged (Viewer viewer, Object oldInput, Object newInput)
  {
    this.table = (TableViewer)viewer;
    
    if (oldInput instanceof MonitoredCollection)
      ((MonitoredCollection)oldInput).removeCollectionListener (this);
    
    if (newInput instanceof MonitoredCollection)
      ((MonitoredCollection)newInput).addCollectionListener (this);
  }
  
  protected void insertElements (Collection elements, int startIndex)
  {
    for (Iterator i = elements.iterator (); i.hasNext (); )
      table.insert (i.next (), startIndex++);
  }

  // CollectionListener interface
  
  public void elementsAdded (final CollectionEvent e)
  {
    Display display = table.getTable ().getDisplay ();
    
    if (display.getThread () == Thread.currentThread ())
    {
      insertElements (e.getElements (), e.getStartIndex ());
    } else
    {
      display.asyncExec (new Runnable ()
      {
        public void run ()
        {
          if (!table.getTable ().isDisposed ())
            insertElements (e.getElements (), e.getStartIndex ());
        }
      });
    }
  }
  
  public void elementsRemoved (final CollectionEvent e)
  {
    Display display = table.getTable ().getDisplay ();
   
    if (display.getThread () == Thread.currentThread ())
    { 
      table.remove (e.getElements ().toArray ());
    } else
    {
      display.asyncExec (new Runnable ()
      {
        public void run ()
        {
          if (!table.getTable ().isDisposed ())
            table.remove (e.getElements ().toArray ());
        }
      });
    }
  }
  
  public void elementMoved (final CollectionEvent e)
  {
    Display display = table.getTable ().getDisplay ();
    final Object element = e.getElements ().iterator ().next ();

    if (display.getThread () == Thread.currentThread ())
    { 
      table.remove (element);
      table.insert (element, e.getEndIndex ());
    } else
    {
      display.asyncExec (new Runnable ()
      {
        public void run ()
        {
          if (!table.getTable ().isDisposed ())
          {  
            table.remove (element);
            table.insert (element, e.getEndIndex ());
          }
        }
      });
    }
  }
}
