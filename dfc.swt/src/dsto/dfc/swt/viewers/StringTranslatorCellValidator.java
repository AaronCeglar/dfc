package dsto.dfc.swt.viewers;

import org.eclipse.jface.viewers.ICellEditorValidator;

import dsto.dfc.util.InvalidFormatException;

import dsto.dfc.text.IStringTranslator;
import dsto.dfc.text.StringUtility;

/**
 * Cell validator that wraps an {@link IStringTranslator}.
 * 
 * @author Matthew Phillips
 */
public class StringTranslatorCellValidator implements ICellEditorValidator
{
  private IStringTranslator translator;

  public StringTranslatorCellValidator (Class type)
    throws IllegalArgumentException
  {
    this (StringUtility.translatorFor (type));
  }
  
  public StringTranslatorCellValidator (IStringTranslator translator)
  {
    this.translator = translator;
  }

  public String isValid (Object value)
  {
    try
    {
      translator.fromString ((String)value);
      
      return null;
    } catch (InvalidFormatException ex)
    {
      return ex.getMessage ();
    }
  }
}
