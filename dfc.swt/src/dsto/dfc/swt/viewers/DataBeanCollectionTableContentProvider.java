
package dsto.dfc.swt.viewers;

import org.eclipse.swt.widgets.Display;

import org.eclipse.jface.viewers.Viewer;

import dsto.dfc.databeans.IDataObject;
import dsto.dfc.databeans.PropertyEvent;
import dsto.dfc.databeans.PropertyListener;
import dsto.dfc.databeans.PropertyPath;
import dsto.dfc.util.RunnableEventCallback;

/**
 * Extends CollectionTableContentProvider to generate table updates when
 * any contained IDataObject properties change.
 */
public class DataBeanCollectionTableContentProvider
  extends CollectionTableContentProvider implements PropertyListener
{
  public void inputChanged (Viewer viewer, Object oldInput, Object newInput)
  {
    super.inputChanged (viewer, oldInput, newInput);
    
    if (oldInput instanceof IDataObject)
      ((IDataObject)oldInput).removePropertyListener (this);
    
    if (newInput instanceof IDataObject)
      ((IDataObject)newInput).addPropertyListener (this);
  }
  
  public void propertyValueChanged (PropertyEvent e)
  {
    PropertyPath path = e.path;

    if (path.length () >= 2 && path.first () != null)
    {
      String property = path.tail ().toString ();      
      Display display = table.getControl ().getDisplay ();
        
      if (display.getThread () == Thread.currentThread ())
      {
        table.update (path.first (), new String [] {property});
      } else
      {
        // do asyncExec () if property affects display
        if (table.getLabelProvider ().isLabelProperty (path.first (), property))
          display.asyncExec (new RunnableEventCallback (this, "propertyValueChanged0", e));
      }
    }
  }
  
  protected void propertyValueChanged0 (PropertyEvent e)
  {
    PropertyPath path = e.path;
    
    if (!table.getTable ().isDisposed ())
    {
      // JFace up to and including 2.1.1 has a bug
      // (24521: https://bugs.eclipse.org/bugs/show_bug.cgi?id=24521)
      // that means calling update () on an invisible filtered item won't
      // make it visible again. Workaround is to use unsupported
      // testFindItem () call to check for this case and call add () if needed
      
      Object element = path.first ();
      
      if (table.testFindItem (element) == null)
        table.add (element);
      else
        table.update (element, new String [] {path.tail ().toString ()});
    }
  }
}
