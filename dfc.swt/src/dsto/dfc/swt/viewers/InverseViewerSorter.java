package dsto.dfc.swt.viewers;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;

/**
 * Wrapper that inverts the ordering of another sorter.
 *
 * @author Matthew Phillips
 */
public class InverseViewerSorter extends ViewerSorter
{
  private final ViewerSorter sorter;

  public InverseViewerSorter (ViewerSorter sorter)
  {
    this.sorter = sorter;
  }

  @Override
  public int category (Object element)
  {
    return sorter.category (element) * -1;
  }

  @Override
  public int compare (Viewer viewer, Object e1, Object e2)
  {
    return sorter.compare (viewer, e1, e2) * -1;
  }

  @Override
  public boolean equals (Object obj)
  {
    return obj instanceof InverseViewerSorter &&
            ((InverseViewerSorter)obj).sorter.equals (sorter);
  }

  @Override
  public int hashCode ()
  {
    return 31 + sorter.hashCode (); // based on Eclipse's auto-gen hashCode()
  }

  @Override
  public boolean isSorterProperty (Object element, String property)
  {
    return sorter.isSorterProperty (element, property);
  }
}
