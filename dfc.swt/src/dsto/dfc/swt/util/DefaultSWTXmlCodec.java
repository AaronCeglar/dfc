package dsto.dfc.swt.util;

import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.jdom.Element;

import dsto.dfc.databeans.io.XmlDecodeContext;
import dsto.dfc.databeans.io.XmlDecoder;
import dsto.dfc.databeans.io.XmlEncodeContext;
import dsto.dfc.databeans.io.XmlEncoder;
import dsto.dfc.databeans.io.XmlInput;
import dsto.dfc.databeans.io.XmlOutput;

import static java.lang.Integer.parseInt;

/**
 * XML codec for common SWT types (Point, RGB, etc).
 * 
 * @author mpp
 * @version $Revision: 1.7 $
 */
public class DefaultSWTXmlCodec implements XmlDecoder, XmlEncoder
{
  public static void register ()
  {
    DefaultSWTXmlCodec codec = new DefaultSWTXmlCodec ();
    
    XmlInput.registerGlobalXmlDecoder ("swt_point", codec);
    XmlInput.registerGlobalXmlDecoder ("swt_color", codec);
    XmlInput.registerGlobalXmlDecoder ("swt_font", codec);
    XmlOutput.registerGlobalXmlEncoder (codec);
  }
  
  public boolean shouldPreserveIdentity (XmlEncodeContext context, Object value)
  {
    return false;
  }
  
  public boolean canEncode (XmlEncodeContext context, Object value)
  {
    return value instanceof Point ||
           value instanceof RGB ||
           value instanceof FontData;
  }

  public Object decode (XmlDecodeContext context, Element element)
    throws IllegalArgumentException, ClassNotFoundException
  {
    String name = element.getName ();
    
    if (name.equals ("swt_point"))
    {
      int x = parseInt (element.getAttributeValue ("x"));
      int y = parseInt (element.getAttributeValue ("y"));
      
      return new Point (x, y);
    } else if (name.equals ("swt_color"))
    {
      int r = parseInt (element.getAttributeValue ("r"));
      int g = parseInt (element.getAttributeValue ("g"));
      int b = parseInt (element.getAttributeValue ("b"));
      
      return new RGB (r, g, b);
    } else if (name.equals ("swt_font"))
    {
      return new FontData (element.getAttributeValue ("data"));
    } else
    {
      throw new IllegalArgumentException ();
    }
  }
  
  public Element encode (XmlEncodeContext context, Object value)
  {
    if (value instanceof Point)
    {
      Point point = (Point)value;
      Element element = new Element ("swt_point");
      
      element.setAttribute ("x", Integer.toString (point.x));
      element.setAttribute ("y", Integer.toString (point.y));
      
      return element;
    } else if (value instanceof RGB)
    {
      RGB colour = (RGB)value;
      Element element = new Element ("swt_color");
      
      element.setAttribute ("r", Integer.toString (colour.red));
      element.setAttribute ("g", Integer.toString (colour.green));
      element.setAttribute ("b", Integer.toString (colour.blue));
      
      return element;
    } else if (value instanceof FontData)
    {
      Element element = new Element ("swt_font");
      
      element.setAttribute ("data", value.toString ());
      
      return element;
    } else
    {
      throw new IllegalArgumentException ();
    }
  }
}
