package dsto.dfc.swt.util;

import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;

/**
 * Basic geometrical utilities for SWT.
 * 
 * @author mpp
 */
public final class Geometry
{
  public static final int CENTER = 0;
  public static final int LEFT = 1;
  public static final int RIGHT = 2;
  public static final int TOP = LEFT;
  public static final int BOTTOM = RIGHT;
  
  public static final int TOP_LEFT = (TOP << 2) | LEFT;
  public static final int MID_TOP = (CENTER << 2) | TOP;
  public static final int TOP_RIGHT = (TOP << 2) | RIGHT;
  public static final int MID_RIGHT = (CENTER << 2) | RIGHT;
  public static final int BOTTOM_RIGHT = (BOTTOM << 2) | RIGHT;
  public static final int MID_BOTTOM = (CENTER << 2) | BOTTOM;
  public static final int BOTTOM_LEFT = (BOTTOM << 2) | LEFT;
  public static final int MID_LEFT = (CENTER << 2) | LEFT;
  

  private Geometry()
  {
    //  zip
  }

  /**
   * Calculate offset of a child region from a parent region necessary
   * to align child region.
   *
   * @param parentSize Size of parent region.
   * @param childSize Size of child region;
   * @param alignment Alignment of child relative to parent.
   */
  public static Point offsetForAlignment (Point parentSize,
					   Point childSize,
  					   int alignment)
  {
    return new Point
      (offsetForAlignment (parentSize.x, childSize.x, alignment & 0x03),
       offsetForAlignment (parentSize.y, childSize.y, alignment >> 2));
  }

  /**
   * Calculate offset from a parent length necessary to align a child length.
   *
   * @param parentLength Size of parent region.
   * @param childLength Size of child region;
   * @param alignment Alignment of child relative to parent. One of LEFT/TOP,
   * CENTER or RIGHT/BOTTOM. 
   */
  public static int offsetForAlignment (int parentLength,
                                          int childLength,
                                          int alignment)
  {
    switch (alignment)
    {
      case LEFT :
        return 0;
      case CENTER :
        return Math.max (0, (parentLength - childLength) / 2);
      case RIGHT:
        return Math.max (0, parentLength - childLength);
      default:
        throw new IllegalArgumentException ();
    }
  }
  
  /**
   * Return a rectangle's position, adjusting so it does not go outside the
   * screen.
   * 
   * @return The new location.
   */
  public static Point bumpRectangle (Rectangle windowRect)
  {
    bumpRectangle (Display.getCurrent ().getClientArea (), windowRect);
  
    return new Point (windowRect.x, windowRect.y);
  }
  
  /**
   * Ensure that childRect is in parentRect, and move it into it if not.
   */
  public static void bumpRectangle (Rectangle parentRect, Rectangle childRect)
  {
    int x_diff =
      (childRect.x + childRect.width) - (parentRect.x + parentRect.width);
    int y_diff =
      (childRect.y + childRect.height) - (parentRect.y + parentRect.height);

    /* To far right */
    if (x_diff > 0)
      childRect.x -= x_diff;

    /* To far down */
    if (y_diff > 0)
      childRect.y -= y_diff;

    /* To far left */
    if (childRect.x < parentRect.x)
      childRect.x = parentRect.x;

    /* To far up */
    if (childRect.y < parentRect.y)
      childRect.y = parentRect.y;
  }

  /**
   * Test if one rectangle fully contains another.
   * 
   * @return True if r1 fully contains r2.
   */
  public static boolean contains (Rectangle r1, Rectangle r2)
  {
    return r1.contains (r2.x, r2.y) &&
           r1.contains (r2.x + r2.width, r2.y + r2.height);
  }
}
