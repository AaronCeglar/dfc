package dsto.dfc.swt.util;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;

import dsto.dfc.logging.Log;
import dsto.dfc.util.Beans;

/**
 * Binds the value of a property from a source object to the value of a
 * property on a target object via the JFace IPropertyChangeListener interface.
 * The value of the target property will always be the value of the source
 * property.
 * 
 * @author mpp
 * @version $Revision$
 */
public class PropertyBinding implements IPropertyChangeListener
{
  private Object source;
  private Object target;
  private String sourceProperty;
  private String targetProperty;
  
  public PropertyBinding (Object source, String sourceProperty,
                          Object target, String targetProperty)
  {
    this.source = source;
    this.target = target;
    this.sourceProperty = sourceProperty;
    this.targetProperty = targetProperty;
    
    try
    {
      setTargetValue (Beans.getPropertyValue (source, sourceProperty));
    } catch (NoSuchMethodException ex)
    {
      throw new IllegalArgumentException ("Missing property: " + ex.getMessage ());
    } catch (InvocationTargetException ex)
    {
      throw new IllegalArgumentException
        ("Error in property read/write " + targetProperty + ": " +
         ex.getTargetException ());
    }
    
    if (!Beans.addListener (source, "propertyChange",
        IPropertyChangeListener.class, this))
    {
      throw new IllegalArgumentException
        ("Cannot listen to " + source.getClass ());
    }
  }
  
  public void dispose ()
  {
    Beans.removeListener (source, "propertyChange",
                          IPropertyChangeListener.class, this);
  }
  
  public void propertyChange (PropertyChangeEvent e)
  {
    try
    {
      if (e.getProperty ().equals (sourceProperty))
        setTargetValue (e.getNewValue ());
    } catch (InvocationTargetException ex)
    {
      Log.warn ("Error in property write method " + targetProperty,
                this, ex.getTargetException ());
    } catch (NoSuchMethodException ex)
    {
      throw new Error ("This cannot happen!");
    }
  }
  
  private void setTargetValue (Object value)
    throws NoSuchMethodException, InvocationTargetException
  {
    Beans.setPropertyValue (target, targetProperty, value);
  }
}
