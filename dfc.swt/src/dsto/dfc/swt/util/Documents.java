package dsto.dfc.swt.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.swt.program.Program;

import org.apache.oro.text.perl.Perl5Util;

import dsto.dfc.util.BrowserControl;

import dsto.dfc.logging.Log;
import dsto.dfc.text.StringUtility;

/**
 * General utilities for opening document files/URL's using local
 * applications or the system browser.
 * 
 * @see BrowserControl
 * 
 * @author Matthew Phillips
 */
public final class Documents
{
  private Documents ()
  {
    // zip
  }
  
  /**
   * Open a filename/URL with the system default application. Tries to
   * use the SWT {@link Program} class to open based on extension
   * (works well with filenames and file:// URL's), then falls back on
   * whatever handles HTML then falls back on {@link BrowserControl}
   * as a last resort.
   * 
   * @param name The URL/filename to open.
   * 
   * @throws IOException if the URL could not be opened or an error
   *           occurred opening it.
   */
  public static void open (String name)
    throws IOException
  {
    Program program = null;
    String extension = getExtension (name, "html");
    
    // convert to filename if possible
    name = urlToFilename (name);

    if (hasUrlPrefix (name))
    {
      // use web browser for any non-file URL        
      program = Program.findProgram ("html");  
    } else
    {
      program = Program.findProgram (extension);
      
      if (program == null)
        program = Program.findProgram ("html");
    }

    if (program != null)
    {
      Log.diagnostic ("Opening " + name + " as type \"" + program.getName () + "\"", Documents.class);
      
      if (!program.execute (name))
        throw new IOException ("Could not open URL as type \"" + program.getName () + " \"");
    } else
    {
      // for non web pages on Windows, assume starting browser is
      // pointless since it will have the same problem
      if (!extension.equals ("html") && !name.startsWith ("http") &&
          System.getProperty ("os.name", "").startsWith ("Windows"))
      {
        throw new IOException ("No program associated with URL");
      } else
      {
        // use BrowserControl as last resort
        // this also works on Windows with Firefox which doesn't show up in SWT programs lst
        openWithBrowserControl (null, name);
      }
    }
  }
  
  /**
   * Test if a name starts like a "proper" URL: 2 or more letters
   * followed by ':'
   */
  private static boolean hasUrlPrefix (String name)
  {
    for (int i = 0; i < name.length (); i++)
    {
      char c = name.charAt (i);
      
      if (c == ':')
        return i > 1;
      else if (!Character.isLetter (c))
        return false;
    }
    
    return false;
  }

  /**
   * Open a URL using {@link BrowserControl}.
   * 
   * @param command The command to execute. May be null.
   * @param url The URL.
   * 
   * @throws IOException if an error occurs.
   */
  public static void openWithBrowserControl (String command, String url)
    throws IOException
  {
    if (url.startsWith ("file:"))
      url = urlToFilename (url);
    
    try
    {
      if (command == null)
        BrowserControl.displayURL (url);
      else
        BrowserControl.displayURL (command, url);
    } catch (IOException ex)
    {
      throw new IOException ("Error opening URL: " + ex.getMessage ());
    } 
  }

  /**
   * Convert "file:" URL's to their platform filenames. Does nothing
   * for non-file URL's.
   */
  public static String urlToFilename (String url)
  { 
    if (url.startsWith ("file:"))
    {
      int start = 5;
      
      // skip any "/"'s at the start
      while (start < url.length () && url.charAt (start) == '/')
        start++;
      
      if (start >= url.length ())
      {
        return "";
      } else
      {
        // platform'ize the file separator
        char fileSep = File.separatorChar;
        char otherSep = (fileSep == '/') ? '\\' : '/';
  
        return url.substring (start).replace (otherSep, fileSep);
      }
    } else
    {
      return url;
    }
  }

  /**
   * Get the nominal extension of a URL/URI.
   * 
   * @param uri The URI.
   * @param defaultExt The extension to return when there is none (eg URL is
   * http://www.frob.com/)
   */
  public static String getExtension (String uri, String defaultExt)
  {
    // normalize \'s
    uri = uri.replace ('\\', '/');
    
    String path;
    
    // extract path from URI
    try
    {
      path = new URI (uri).getPath ();
    } catch (URISyntaxException ex)
    {
      path = null;
    }
    
    if (path == null)
      path = uri;
    
    // remove all but last element of path
    path = path.substring (path.lastIndexOf ('/') + 1);
    
    // try to find extension
    int dotIndex = path.lastIndexOf ('.');
  
    if (dotIndex != -1)
      return path.substring (dotIndex + 1);
    else
      return defaultExt;
  }

  /**
   * Convert a reference that's probably a URL or a filename into a
   * guaranteed bona fide URL. Does nothing if filename appears to be
   * a URL already. This may add "file:" as a prefix if needed or
   * expand a Windows ".url" file.
   */
  public static String makeUrl (String filename)
  {
    Perl5Util matcher = new Perl5Util ();
  
     //  strip off any extra lines that may have snuck in there
     int endOfLine = StringUtility.indexOfChar (filename, "\n\r");
    
     if (endOfLine != -1)
       filename = filename.substring (0, endOfLine);
           
    if (matcher.match ("m!^\\w+://!i", filename) || filename.startsWith ("file:"))
      return filename;
    else if (filename.toLowerCase ().endsWith (".url"))
      return urlFromWindowsLink (filename);
    else
      return "file:" + filename;
  }

  private static String urlFromWindowsLink (String filename)
  {
    String url = null;
    BufferedReader input = null;
    
    try
    {
      input = new BufferedReader (new FileReader (filename));
      
      String line;
      
      while (url == null && (line = input.readLine ()) != null)
      {
        if (line.startsWith ("URL=") && line.length () > 4)
          url = line.substring (4);
      }
    } catch (IOException ex)
    {
      url = filename;
    } finally
    {
      if (input != null)
      {  
        try
        {
          input.close ();
        } catch (IOException ex)
        {
          // zip
        }
      }
    }
    
    return url;
  }

  public static boolean looksLikeUNCFile (String name)
  {
    return name.startsWith ("\\\\") || name.startsWith ("//");
  }

  public static boolean looksLikeDOSFile (String name)
  {
    return name.length () >= 3 &&
            Character.isLetter (name.charAt (0)) &&
            name.charAt (1) == ':' &&
            (name.charAt (2) == '\\' || name.charAt (2) == '/'); 
  }
}
