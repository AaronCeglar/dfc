package dsto.dfc.swt.util;

import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;


/**
 * Provides utilities for placing windows relative to each other and
 * to the screen.
 */
public final class WindowLayout
{
  public static final int DEFAULT_DISTANCE = 30;

  public static final int HORIZ_LEFT = 1;
  public static final int HORIZ_CENTER = 2;
  public static final int HORIZ_RIGHT = 4;
  
  public static final int VERT_TOP = 8;
  public static final int VERT_CENTER = 16;
  public static final int VERT_BOTTOM = 32;
  
  public static final int CENTER = HORIZ_CENTER | VERT_CENTER;
  
  public static final int NORTH = VERT_TOP;
  public static final int SOUTH = VERT_BOTTOM;
  public static final int EAST = HORIZ_RIGHT;
  public static final int WEST = HORIZ_LEFT;
  public static final int NORTH_EAST = NORTH | EAST;
  public static final int SOUTH_EAST = SOUTH | EAST;
  public static final int SOUTH_WEST = SOUTH | WEST;
  public static final int NORTH_WEST = NORTH | WEST;

  private WindowLayout()
  {
    // zip
  }

  public static void setLocation (Shell window, int position)
  {
    window.setLocation (getLocation (window.getSize (), position));
  }
  
  /**
   * Get location needed to place window at a specified alignmnent on the screen.
   *
   * @param windowSize The size of the window to place.
   * @param position The position of the window. A bitmask of one of the
   * HORIZ_* constants with one of the VERT_* constants, eg
   * "HORIZ_TOP | VERT_CENTER".
   * 
   * @return The new location.
   */
  public static Point getLocation (Point windowSize, int position)
  {
    return getLocation (Display.getCurrent ().getClientArea (), windowSize, position);
  }
  
  /**
   * Align a child window relative to its parent.
   *
   * @param parentRect The parent window.
   * @param childSize The size of the child window.
   * @param position The child's alignment relative to parent.
   * 
   * @return The new location.
   */
  public static Point getLocation (Rectangle parentRect, Point childSize, 
                                    int position)
  {
    Rectangle childRect = new Rectangle (0, 0, childSize.x, childSize.y);

    switch (position & (HORIZ_LEFT | HORIZ_CENTER | HORIZ_RIGHT))
    {
      case HORIZ_LEFT:
        childRect.x = parentRect.x;
        break;
      case HORIZ_CENTER:
        childRect.x =
          Math.max (parentRect.x,
                    parentRect.x + (parentRect.width - childSize.x) / 2);
        break;
      case HORIZ_RIGHT:
        childRect.x =
          Math.max (parentRect.x,
                    parentRect.x + parentRect.width - childSize.x);
        break;
    }

    switch (position & (VERT_TOP | VERT_CENTER | VERT_BOTTOM))
    {
      case VERT_TOP:
        childRect.y = parentRect.y;
        break;
      case VERT_CENTER:
        childRect.y =
          Math.max (parentRect.y,
                    parentRect.y + (parentRect.height - childSize.y) / 2);
        break;
      case VERT_BOTTOM:
        childRect.y =
          Math.max (parentRect.y,
                    parentRect.y + parentRect.height - childSize.y);
        break;
    }

    return Geometry.bumpRectangle (childRect);
  }

  public static Point getLocation (Rectangle parentRect, int fromPoint,
                                    Point childSize, int toPoint)
  {
    return getLocation (parentRect, fromPoint, childSize, toPoint, SOUTH_EAST,
                        DEFAULT_DISTANCE);
  }

  /**
   * Places a window at a default distance.  See placeWindow
   * below.
   */
  public static Point getLocation (Rectangle parentRect, int fromPoint,
                                    Point childSize, int toPoint,
                                    int direction)
  {
    return getLocation (parentRect, fromPoint, childSize, toPoint, direction,
                        DEFAULT_DISTANCE);
  }

  /**
   * Get location needed to place a child window relative to a parent (or the
   * screen) using an imaginary strut connecting 8 points on the parent and
   * child window's borders.
   *
   * @param parentRect The parent window.  May be null in which case the
   * screen dimensions are used.
   * @param fromPoint The point on the parent to connect the strut.
   * A bitmask of one of the HORIZ_* constants with one of the VERT_* constants,
   * eg "HORIZ_TOP | VERT_CENTER".
   * @param childSize The child window size.
   * @param toPoint The point on the child to connect the strut. Same format as
   * fromPoint.
   * @param direction The direction of the strut.
   * @param distance The length of the strut.
   * 
   * @return The new location.
   */
  public static Point getLocation (Rectangle parentRect, int fromPoint,
                                    Point childSize, int toPoint,
                                    int direction, int distance)
  {
    if (parentRect == null)
      parentRect = Display.getCurrent ().getClientArea ();

    Point offset =
      getOffsetForPoint (parentRect.width, parentRect.height, fromPoint);
      
    Rectangle childRect = new Rectangle (parentRect.x + offset.x, parentRect.y + offset.y, childSize.x, childSize.y);

    switch (direction & (NORTH | SOUTH))
    {
      case NORTH:
        childRect.y -= distance;
        break;
      case SOUTH:
        childRect.y += distance;
        break;
    }
    
    switch (direction & (EAST | WEST))
    {
      case EAST:
        childRect.x += distance;
        break;
      case WEST:
        childRect.x -= distance;
        break;
    }

    Point toDelta = getOffsetForPoint (childSize.x, childSize.y, toPoint);

    childRect.x -= toDelta.x;
    childRect.y -= toDelta.y;

    return Geometry.bumpRectangle (childRect);
  }

  private static Point getOffsetForPoint (int width, int height, int connector)
  {
    Point point = new Point (0, 0);

    switch (connector & (HORIZ_CENTER | HORIZ_RIGHT))
    {
      case HORIZ_CENTER:
        point.x = width / 2;
        break;
      case HORIZ_RIGHT:
        point.x = width - 1;
        break;
    }
    
    switch (connector & (VERT_CENTER | VERT_BOTTOM))
    {
      case VERT_CENTER:
        point.y = height / 2;
        break;
      case VERT_BOTTOM:
        point.y = height - 1;
        break;
    }

    return point;
  }
}
