package dsto.dfc.swt.util;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;

import dsto.dfc.databeans.DataBean;
import dsto.dfc.databeans.IDataBean;
import dsto.dfc.databeans.PropertyEvent;
import dsto.dfc.databeans.PropertyListener;
import dsto.dfc.util.EventListenerList;

/**
 * Wraps a DFC IDataBean as a JFace IPreferenceStore.
 * 
 * @todo Map nested properties (eg foo/bar/name)?
 * 
 * @author mpp
 * @version $Revision$
 */
public class DataBeanPreferenceStore
  implements IPreferenceStore, PropertyListener
{
  private IDataBean bean;
  private EventListenerList listeners;
  private boolean changed;
  
  public DataBeanPreferenceStore ()
  {
    this (new DataBean ());
  }
  
  public DataBeanPreferenceStore (IDataBean bean)
  {
    this.bean = bean;
    this.listeners = new EventListenerList ();
    this.changed = false;
    
    bean.addPropertyListener (this);
  }
  
  public void dispose ()
  {
    bean.removePropertyListener (this);
  }
  
  public IDataBean getDataBean ()
  {
    return bean;
  }
  
  public void propertyValueChanged (PropertyEvent e)
  {
    //System.out.println ("changed " + e.getPath ());
    changed = true;
  
    // fire property events for local path  
    if (e.path.length () == 1)
    {
      firePropertyChangeEvent (e.path.toString (),
                               e.oldValue, e.newValue);
    }
  }

  public Object getValue (String name)
  {
    return bean.getValue (name);
  }
  
  public void setValue (String name, Object value)
  {
    bean.setValue (name, value);
  }
  
  // IPreferenceStore interface
  
  public boolean needsSaving ()
  {
    return changed;
  }
  
  public void addPropertyChangeListener (IPropertyChangeListener listener)
  {
    listeners.addListener (listener);
  }
  
  public void removePropertyChangeListener(IPropertyChangeListener listener)
  {
    listeners.removeListener (listener);
  }

  public void firePropertyChangeEvent (String name,
                                       Object oldValue,
                                       Object newValue)
  {
    if (listeners.hasListeners () &&
         (oldValue == null || !oldValue.equals (newValue)))
    {
      PropertyChangeEvent e =
        new PropertyChangeEvent (this, name, oldValue, newValue);
      
      listeners.fireEvent ("propertyChange", e);
    }
  }
  
  public boolean contains (String name)
  {
    return bean.getValue (name) != null;
  }

  public boolean getBoolean (String name)
  {
    return bean.getBooleanValue (name);
  }
  
  public boolean getDefaultBoolean (String name)
  {
    return bean.getBooleanValue (name);
  }
  
  public double getDefaultDouble (String name)
  {
    return bean.getDoubleValue (name);
  }

  public float getDefaultFloat (String name)
  {
    return bean.getFloatValue (name);
  }
  
  public int getDefaultInt (String name)
  {
    return bean.getIntValue (name);
  }
  
  public long getDefaultLong (String name)
  {
    return bean.getLongValue (name);
  }
  
  public String getDefaultString (String name)
  {
    Object value = bean.getValue (name);
    
    if (value != null)
      return value.toString ();
    else
      return "";
  }
  
  public double getDouble (String name)
  {
    return bean.getDoubleValue (name);
  }
  
  public float getFloat (String name)
  {
    return bean.getFloatValue (name);
  }
  
  public int getInt (String name)
  {
    return bean.getIntValue (name);
  }
  
  public long getLong (String name)
  {
    return bean.getLongValue (name);
  }
  
  public String getString (String name)
  {
    Object value = bean.getValue (name);
    
    if (value != null)
      return value.toString ();
    else
      return "";
  }
  
  public boolean isDefault (String name)
  {
    return bean.isTransient (name);
  }
  
  public void putValue (String name, String value)
  {
    bean.setValue (name, value);
  }
  
  public void setDefault (String name, double value)
  {
    bean.setValue (name, value, IDataBean.TRANSIENT);
  }
  
  public void setDefault (String name, float value)
  {
    bean.setValue (name, value, IDataBean.TRANSIENT);
  }
  
  public void setDefault (String name, int value)
  {
    bean.setValue (name, value, IDataBean.TRANSIENT);
  }
  
  public void setDefault (String name, long value)
  {
    bean.setValue (name, value, IDataBean.TRANSIENT);
  }
  
  public void setDefault (String name, String value)
  {
    bean.setValue (name, value, IDataBean.TRANSIENT);
  }
  
  public void setDefault (String name, boolean value)
  {
    bean.setValue (name, value, IDataBean.TRANSIENT);
  }
  
  public void setToDefault (String name)
  {
    throw new UnsupportedOperationException ();
  }
  
  public void setValue (String name, double value)
  {
    bean.setValue (name, value);
  }
  
  public void setValue (String name, float value)
  {
    bean.setValue (name, value);
  }
  
  public void setValue (String name, int value)
  {
    bean.setValue (name, value);
  }
  
  public void setValue (String name, long value)
  {
    bean.setValue (name, value);
  }
  
  public void setValue (String name, String value)
  {
    bean.setValue (name, value);
  }
  
  public void setValue (String name, boolean value)
  {
    bean.setValue (name, value);
  }
}
