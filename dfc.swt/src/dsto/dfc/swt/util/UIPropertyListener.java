package dsto.dfc.swt.util;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import dsto.dfc.util.Disposable;

import dsto.dfc.databeans.IDataObject;
import dsto.dfc.databeans.PropertyEvent;
import dsto.dfc.databeans.PropertyListener;

import static java.lang.Thread.currentThread;

/**
 * A DataObject property listener adapter that re-routes property
 * change events that occur outside the UI thread into the UI thread
 * by using Display.asyncExec ().
 * <p>
 * 
 * Threading note: clients should call dispose () (ideally
 * {@link #dispose(IDataObject)}) to remove this listener. This will
 * ensure that any async property updates in the display pipeline will
 * be cancelled rather than calling into a disposed UI element.
 * 
 * @author Matthew Phillips
 */
public class UIPropertyListener 
  implements Disposable, PropertyListener
{
  /**
   * Display is used as an interthread variable, so mark as volatile.
   * We use this rather than locking due to the high possibility of
   * deadlocking if we hold a lock while calling into client code.
   */
  protected volatile Display display;
  protected PropertyListener listener;
  protected ClassLoader contextClassLoader;
  
  /**
   * Factory to create a listener for a SWT control that is also the
   * property listener. The listener is automatically added as a
   * property listener to the object, and is disposed when the control
   * is.
   * 
   * @param control The control: must implement
   *          {@link PropertyListener}.
   * @param object The object to listen to.
   */
  public static UIPropertyListener create (Control control, 
                                           final IDataObject object)
  {
    final UIPropertyListener listener = new UIPropertyListener (control);
    
    object.addPropertyListener (listener);
    
    control.addListener (SWT.Dispose, new Listener ()
    {
      public void handleEvent (Event event)
      {
        listener.dispose (object);
      }
    });
    
    return listener;
  }
  
  /**
   * Shortcut to create a listener for a SWT control that is also the
   * property listener.
   * 
   * @param control The control: must implement {@link PropertyListener}.
   * 
   * @see #create(Control, IDataObject)
   */
  public UIPropertyListener (Control control)
  {
    this (control.getDisplay (), (PropertyListener)control);    
  }
  
  /**
   * Create a new instance.
   * 
   * @param display The display.
   * @param listener The listener that will be called by this one,
   *          guaranteed from the UI thread.
   */
  public UIPropertyListener (Display display, PropertyListener listener)
  {
    this.display = display;
    this.listener = listener;
    this.contextClassLoader = currentThread ().getContextClassLoader ();
  }

  public synchronized void propertyValueChanged (PropertyEvent e)
  {
    if (display == null || display.isDisposed ())
      return;
    
    if (display.getThread () == currentThread ())
      listener.propertyValueChanged (e);
    else
      display.asyncExec (new Callback (e));
  }
  
  public synchronized void dispose ()
  {
    display = null;
    listener = null;
  }
  
  /**
   * Remove this listener and dispose it.
   * 
   * @param object The object to remove the listener from. Removing
   *          the listener this way ensures no event can sneak in
   *          while disposing.
   */
  public synchronized void dispose (IDataObject object)
  {
    object.removePropertyListener (this);
    display = null;
    listener = null;
  }
    
  /**
   * Swaps in a specified class loader
   * @param newClassLoader
   * @return The original class loader
   */
  protected ClassLoader installClassLoader (ClassLoader newClassLoader)
  {
    ClassLoader origLoader = currentThread ().getContextClassLoader ();
    
    currentThread ().setContextClassLoader (newClassLoader);
    
    return origLoader;
  }
  
  protected void restoreClassLoader (ClassLoader origLoader)
  {
    currentThread ().setContextClassLoader (origLoader);
  }

  private class Callback implements Runnable
  {
    private final PropertyEvent e;

    public Callback (PropertyEvent e)
    {
      this.e = e;
    }
   
    public void run ()
    {
      ClassLoader oldClassLoader = installClassLoader (contextClassLoader);
      
      try
      {
        boolean disposed;
        
        synchronized (UIPropertyListener.this)
        {
          /* This may occur after client has shut down and disposed listener */
          disposed = display == null || display.isDisposed ();
        }
        
        /*
         * NB: holding the sync lock while calling listener opens up
         * the way to deadlocks. The display could be disposed between
         * the previous statement and this, but it's going to take a
         * cleverer man than me to figure out a deadlock free way to
         * handle this.
         */
        if (!disposed)
          listener.propertyValueChanged (e);
      } finally
      {
        // restore to the original class loader
        restoreClassLoader (oldClassLoader);
      }
    }
  }
}
