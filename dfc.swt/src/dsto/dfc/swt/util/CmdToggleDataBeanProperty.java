package dsto.dfc.swt.util;

import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.ToolBar;

import dsto.dfc.databeans.IDataBean;
import dsto.dfc.databeans.PropertyEvent;
import dsto.dfc.databeans.PropertyListener;
import dsto.dfc.swt.commands.AbstractCommand;
import dsto.dfc.swt.commands.CommandMenuCheckboxProvider;
import dsto.dfc.swt.commands.CommandMenuItemProvider;
import dsto.dfc.swt.commands.CommandToolbarCheckboxProvider;
import dsto.dfc.swt.commands.CommandToolbarItemProvider;
import dsto.dfc.swt.commands.CommandView;
import dsto.dfc.swt.commands.CustomMenuCommand;
import dsto.dfc.swt.commands.CustomToolbarCommand;
import dsto.dfc.util.Disposable;

/**
 * Command that toggles a boolean property of an
 * {@link dsto.dfc.databeans.IDataBean}. 
 *
 * @version $Revision$
 */
public class CmdToggleDataBeanProperty
  extends AbstractCommand
  implements CustomMenuCommand, CustomToolbarCommand,
             PropertyListener, Disposable
{
  private IDataBean bean;
  private String property;

  public CmdToggleDataBeanProperty (IDataBean bean, String property,
                                    String name,
                                    String iconName,
                                    String group, 
                                    String description,
                                    char mnemonic,
                                    int accelerator)
  {
    super (name, iconName, group, description, false, mnemonic,
           accelerator);
           
    this.bean = bean;
    this.property = property;
    
    bean.addPropertyListener (this);
  }
  
  public void dispose ()
  {
    bean.removePropertyListener (this);
  }

  public void execute ()
  {
    bean.setValue (property, !bean.getBooleanValue (property));
    
    fireCommandExecuted ();
  }

  public boolean getValue ()
  {
    return bean.getBooleanValue (property);
  }

  public void setValue (boolean newValue)
  {
    bean.setValue (property, newValue);
  }
  
  public CommandMenuItemProvider createMenuProvider
    (CommandView commandView, Menu menu, int index)
  {
    return new CommandMenuCheckboxProvider
      (commandView, this, "value", menu, index);
  }
  
  public CommandToolbarItemProvider createToolbarProvider
    (CommandView commandView, ToolBar toolbar, int index)
  {
    return new CommandToolbarCheckboxProvider
      (commandView, this, "value", toolbar, index);
  }
  
  public void propertyValueChanged (PropertyEvent e)
  {
    if (e.path.length () == 1 && e.getName ().equals (property))
      firePropertyChange ("value", e.oldValue, e.newValue);
  }
}
