package dsto.dfc.swt.persistence;

import static dsto.dfc.util.Beans.addListener;
import static dsto.dfc.util.Beans.removeListener;
import static org.eclipse.swt.SWT.CANCEL;
import static org.eclipse.swt.SWT.ICON_ERROR;
import static org.eclipse.swt.SWT.ICON_QUESTION;
import static org.eclipse.swt.SWT.NO;
import static org.eclipse.swt.SWT.YES;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import dsto.dfc.databeans.PropertyEvent;
import dsto.dfc.databeans.PropertyListener;
import dsto.dfc.util.BasicPropertyEventSource;
import dsto.dfc.util.Disposable;
import dsto.dfc.util.PropertyEventSource;

/**
 * A base implementation of Document which implements the high-level
 * bahaviour of a document without being specific to a particular persistent
 * storage mechanism.  If a model contained within this document emits either
 * property change or change events, these are used to automatically raise the
 * changed flag.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public abstract class AbstractDocument
  extends BasicPropertyEventSource
  implements Document, PropertyChangeListener, PropertyListener, ChangeListener,
  Disposable
{
  protected boolean changed;

  public abstract String getType ();

  /**
   * Return a new model instance.
   */
  protected abstract Object createModelInstance ();

  /**
   * Prompt the user for a location (probably in a persistent storage system)
   * to save the model in.  This location will be passed to {@link #readModel},
   * {@link #writeModel} and {@link #setLocation}.
   *
   * @param mode The mode the selection is being made in: either "Open" or
   * "Save".
   * @return A 'location' object, pointing to the selected storage location.
   * Null indicates no location was selected.
   */
  protected abstract Object selectLocation (String mode);

  /**
   * The abstract location in persistent storage where the document is
   * to be saved.
   */
  protected abstract void setLocation (Object newLocation);

  protected abstract Object getLocation ();

  /**
   * Read a new model object from a persistent store location.
   *
   * @param location The location the persistent store to read from.
   * @return The new model read from the store.  Returning null indicates
   * that the operation usually open ()) should be cancelled.
   * @exception IOException if an error occurred while reading the model.
   *
   * @see #selectLocation
   */
  protected abstract Object readModel (Object location)
    throws IOException;

  /**
   * Write a model object to a persistent store location.
   *
   * @param location The location the persistent store to write to.
   * @param model The model to write.
   * @exception IOException if an error occurred while writing the model.
   *
   * @see #selectLocation
   */
  protected abstract void writeModel (Object location, Object model)
    throws IOException;

  public abstract Control getClient ();

  public abstract Object getModel ();

  /**
   * Set the model contained by this document.
   *
   * <p>If the model emits either property change or change events, these
   * are used to automatically raise the changed flag.</p>
   */
  public void setModel (Object newModel)
    throws IllegalArgumentException
  {
    Object oldModel = getModel ();

    basicSetModel (newModel);

    if (oldModel != null)
      unregisterModelListener (oldModel);

    if (newModel != null)
      registerModelListener (newModel);

    setChanged (false);

    firePropertyChange ("model", oldModel, newModel);
  }

  /**
   * Subclasses should overrride this to actually change the model returned
   * by getModel ().  This is called as part of the setModel ()
   * implementation
   *
   * @exception IllegalArgumentException if the model is invalid (eg not of
   * the correct type).  If this is thrown, setModel () will revert the model
   * to the original instance and re-throw the exception.
   */
  protected abstract void basicSetModel (Object newModel)
    throws IllegalArgumentException;


  public boolean isChanged ()
  {
    return changed;
  }

  public void setChanged (boolean newValue)
  {
    boolean oldValue = changed;

    changed = newValue;

    firePropertyChange ("changed", oldValue, newValue);
  }

  public Object newModel ()
  {
    Object newModel = createModelInstance ();

    setModel (newModel);

    setChanged (false);

    return newModel;
  }

  public boolean open ()
  {
    return open (true);
  }

  public boolean open (boolean interactive)
  {
    Object location = getLocation ();

    if (interactive)
      location = selectLocation ("Open");

    if (location != null)
    {
      try
      {
        Object newModel = readModel (location);

        if (newModel != null)
        {
          setModel (newModel);

          setLocation (location);

          return true;
        } else
        {
          // readModel () returns null if operation should be cancelled

          return false;
        }
      } catch (IOException ex)
      {
        handleOpenError (ex);
      }
    }

    return false;
  }

  public boolean save ()
  {
    if (!isLocationSelected ())
      return saveAs ();
    else
      return write (getLocation ());
  }

  public boolean saveAs ()
  {
    Object location;

    if ((location = selectLocation ("Save")) != null)
      return write (location);
    else
      return false;
  }

  protected boolean write (Object location)
  {
    try
    {
      writeModel (location, getModel ());

      setChanged (false);
      setLocation (location);

      return true;
    } catch (IOException ex)
    {
      handleSaveError (ex);

      return false;
    }
  }

  public boolean checkSaveChanges ()
  {
    if (isChanged ())
    {
      String name = getName ();

      if (name == null || name.length () == 0)
        name = "<unnamed>";

      Shell parent = getClient ().getShell ();

      MessageBox dialog =
        new MessageBox (parent, ICON_QUESTION | YES | NO | CANCEL);
      dialog.setText (getType () + " Changed");
      dialog.setMessage ("The " + getType ().toLowerCase () + " \"" + name +
                         "\" has changed since it was last saved.\nDo you " +
                         "want to save it?");

      int result = dialog.open ();

      switch (result)
      {
        case YES:
          return save ();
        case NO:
          return true;
        default: // CANCEL
          return false;
      }

    } else
      return true;
  }

  protected boolean isLocationSelected ()
  {
    return getLocation () != null;
  }

  protected void handleOpenError (Throwable ex)
  {
    openErrorDialog ("Open", "Error opening document", ex);
  }

  protected void handleSaveError (Throwable ex)
  {
    openErrorDialog ("Save", "Error saving document", ex);
  }

  protected void openErrorDialog (String title, String message, Throwable ex)
  {
    if (ex != null)
      message += " (" + ex + ")";

    MessageBox dialog = new MessageBox (getClient ().getShell (), ICON_ERROR);
    dialog.setText (title);
    dialog.setMessage (message);
    dialog.open ();
  }

  protected void unregisterModelListener (Object model)
  {
    if (model instanceof PropertyEventSource)
      ((PropertyEventSource)model).removePropertyChangeListener (this);
    else
      removeListener (PropertyChangeListener.class, model, this);

    removeListener (PropertyListener.class, model, this);
    removeListener (ChangeListener.class, model, this);
  }

  protected void registerModelListener (Object model)
  {
    if (model instanceof PropertyEventSource)
      ((PropertyEventSource)model).addPropertyChangeListener (this);
    else
      addListener (PropertyChangeListener.class, model, this);

    addListener (PropertyListener.class, model, this);
    addListener (ChangeListener.class, model, this);
  }

  public void dispose ()
  {
    if (getModel () != null)
    {
      unregisterModelListener (getModel ());
      basicSetModel (null);
    }
  }

  protected void modelPropertyChanged (PropertyEvent e)
  {
    setChanged (true);
  }

  protected void modelPropertyChanged (PropertyChangeEvent e)
  {
    setChanged (true);
  }

  protected void modelStateChanged (ChangeEvent e)
  {
    setChanged (true);
  }

  // ChangeListener interface

  /**
   * ChangeListener implementation: do not override this, override
   * modelStateChanged instead.
   */
  public void stateChanged (ChangeEvent e)
  {
    modelStateChanged (e);
  }

  // PropertyChangeListener interface

  /**
   * PropertyChangeListener implementation: do not override this, override
   * modelPropertyChanged instead.
   */
  public void propertyChange (PropertyChangeEvent e)
  {
    modelPropertyChanged (e);
  }

  // PropertyListener interface

  /**
   * PropertyListener implementation: do not override this, override
   * modelPropertyChanged instead.
   */
  public void propertyValueChanged (PropertyEvent e)
  {
    modelPropertyChanged (e);
  }
}