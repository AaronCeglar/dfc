package dsto.dfc.swt.persistence;

import org.eclipse.swt.widgets.Control;

import dsto.dfc.util.PropertyEventSource;

/**
 * A document is used as a wrapper around a model to provide the typical
 * Save / Save As / Open / New / Save Changes? behaviour that a user usually
 * expects to see with persistent documents.
 *
 * @author Matthew Phillips
 * @version $Revision$
 *
 * @see AbstractDocument
 * @see FileDocument
 */
public interface Document extends PropertyEventSource
{
  /**
   * The UI client of this document.
   */
  public Control getClient ();

  /**
   * Set the model contained by the document.
   *
   * @exception IllegalArgumentException if the model is not of the
   * correct type.
   */
  public void setModel (Object newModel)
    throws IllegalArgumentException;

  /**
   * The model contained by the document.
   */
  public Object getModel ();

  /**
   * The name of the document.
   */
  public String getName ();

  /**
   * The type of document eg "Spreadsheet".
   */
  public String getType ();

  /**
   * True if the model or other part of the document has changed since
   * last saved.
   */
  public boolean isChanged ();

  public void setChanged (boolean newValue);

  /**
   * Create a new, empty model and set it as the document's model.
   *
   * @return The new model.
   */
  public Object newModel ();

  /**
   * Open a new model from a persistent source such as a file.
   *
   * @return True if a new model was successfully opened.
   * @see #save
   * @see #saveAs
   */
  public boolean open ();

  /**
   * Save the model to its original location.  If the model has not been saved
   * this is equivalent to {@link #saveAs}.
   *
   * @return True if the model was successfully saved.
   * @see #saveAs
   * @see #open
   */
  public boolean save ();

  /**
   * Save the model to a new, user selected, location.
   *
   * @return True if the model was successfully saved.
   * @see #save
   * @see #open
   */
  public boolean saveAs ();

  /**
   * If the document has been changed, check with the user whether the model
   * should be saved and optionally save it.
   *
   * @return True if it would be safe to blow away the current model
   * (with newModel() or open() for example) ie:
   *
   * <ul>
   *   <li>the document was not changed, or
   *   <li>the document was changed and the user saved it, or
   *   <li>the document was changed and the user did not want to save the
   *   changes.
   * </ul>
   *
   * @see #save
   */
  public boolean checkSaveChanges ();
}
