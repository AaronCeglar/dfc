package dsto.dfc.swt.persistence;

import static dsto.dfc.swt.persistence.SerialisedFormat.FORMAT_XML;
import static dsto.dfc.util.Files.addDefaultExtension;
import static org.eclipse.swt.SWT.SAVE;
import static org.eclipse.swt.SWT.OPEN;

import java.io.File;
import java.io.IOException;

import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import dsto.dfc.util.Files;

/**
 * A base class for documents that are file-based.  The default
 * implementations of {@link #readModel} and {@link #writeModel} use the
 * {@link Deserialiser} and {@link Serialiser} classes, but these may be
 * overridden to provide other serialisation mechanisms.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public abstract class FileDocument extends AbstractDocument
{
  protected File file;
  protected SerialisedFormat format = new SerialisedFormat (FORMAT_XML, false);

  @Override
  protected abstract Object createModelInstance ();

  /**
   * The default extension to apply to files with no explicit extension.
   */
  protected abstract String getDefaultExtension ();

  @Override
  public abstract String getType ();

  public File getFile ()
  {
    return file;
  }

  public void setFile (File newValue)
  {
    setLocation (newValue);
  }

  /**
   * Customize a file chooser before it is displayed. May be overridden.
   */
  protected void customizeChooser (String mode, FileDialog chooser)
  {
    chooser.setText (mode + " " + getType ());

    if (file != null)
    {
      chooser.setFilterPath (file.getAbsoluteFile ().getParent ());
      chooser.setFileName (file.getName ());
      String[] extensions =
        new String[] { "*.*", "*." + getDefaultExtension () };
      chooser.setFilterExtensions (extensions);
    }
  }

  protected void restoreDefaultFormat ()
  {
    format.setType (FORMAT_XML);
    format.setCompressed (false);
  }

  // AbstractDocument implementation

  @Override
  protected void setLocation (Object newValue)
  {
    File oldValue = file;
    String oldName = getName ();

    file = (File)newValue;

    firePropertyChange ("file", oldValue, newValue);
    firePropertyChange ("name", oldName, getName ());
  }

  @Override
  protected Object getLocation ()
  {
    return file;
  }

  @Override
  protected Object selectLocation (String mode)
  {
    
    Shell window = getClient ().getShell ();
    
    FileDialog chooser =null;
    if(mode.equals ("Open"))
      chooser = new FileDialog (window, OPEN);
    else
      chooser = new FileDialog (window, SAVE);  
    
    customizeChooser (mode, chooser);

    String filename = chooser.open ();
    if (filename != null)
    {
      File actualFile =
        new File (addDefaultExtension (filename, getDefaultExtension ()));

      if (mode.equals ("Save"))
        restoreDefaultFormat ();

      return actualFile;
    } else
      return null;
  }

  /**
   * Read a model using {@link Deserialiser}.  May be overridden to enable
   * alternative deserialisation mechanism.
   */
  @Override
  protected Object readModel (Object location) throws IOException
  {
    File fileLocation = (File)location;

    try
    {
      return Deserialiser.read (fileLocation, format);
    } catch (ClassNotFoundException ex)
    {
      throw new IOException ("Failed to resolve class in archive: " +
                              ex.getMessage ());
    }
  }

  /**
   * Write a model using {@link Serialiser}.  May be overridden to enable
   * alternative serialisation mechanism.
   */
  @Override
  protected void writeModel (Object location, Object theModel)
    throws IOException
  {
    File fileLocation = (File)location;

    Serialiser.save (theModel, fileLocation.getPath (), format);
  }

  @Override
  public Object newModel ()
  {
    setFile (null);

    restoreDefaultFormat ();

    return super.newModel ();
  }

  /**
   * Returns the file name without extension if one is set.
   */
  public String getName ()
  {
    if (file != null)
      return Files.removeExtension (file.getName ());
    else
      return null;
  }
}