package dsto.dfc.swt.persistence;

/**
 * Stores format options for serialisation.
 *
 * @author Matthew Phillips
 * @version $Revision$
 *
 * @see Deserialiser
 */
public class SerialisedFormat
{
  public static final String FORMAT_XML = "XML";
  public static final String FORMAT_DBXML = "DBXML";
  public static final String FORMAT_BINARY = "Binary";

  private boolean compressed;
  private String type;

  public SerialisedFormat ()
  {
    this.compressed = false;
    this.type = FORMAT_DBXML;
  }

  public SerialisedFormat (String type, boolean compressed)
  {
    this.type = type;
    this.compressed = compressed;
  }

  public String getType ()
  {
    return type;
  }

  public void setType (String newType)
  {
    type = newType;
  }

  public void setCompressed (boolean newCompressed)
  {
    compressed = newCompressed;
  }

  public boolean isCompressed ()
  {
    return compressed;
  }
}
