package dsto.dfc.swt.persistence;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

/**
 * Default serialiser that uses Java serialisation.
 *
 * @author mpp
 * @version $Revision$
 */
public class DefaultSerialiser implements ISerialiser
{
  private static final DefaultSerialiser INSTANCE = new DefaultSerialiser ();

  public static DefaultSerialiser getInstance ()
  {
    return INSTANCE;
  }

  public void serialise (OutputStream output, Object object)
    throws IOException
  {
    ObjectOutputStream objectOutput = new ObjectOutputStream (output);

    objectOutput.writeObject (object);

    objectOutput.flush ();
  }

  public Object deserialise (InputStream input)
    throws IOException, ClassNotFoundException
  {
    ObjectInput objectInput;

    // it's a serialised object stream
    if (input instanceof ObjectInput)
      objectInput = (ObjectInput)input;
    else
      objectInput = new ObjectInputStream (input);

    Object readObject = objectInput.readObject ();
    
    objectInput.close ();

    return readObject;
  }
}
