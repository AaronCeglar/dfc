package dsto.dfc.swt.persistence;

import static dsto.dfc.swt.persistence.SerialisedFormat.FORMAT_BINARY;
import static dsto.dfc.swt.persistence.SerialisedFormat.FORMAT_DBXML;
import static dsto.dfc.swt.persistence.SerialisedFormat.FORMAT_XML;
import static java.io.File.createTempFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;

import fr.dyade.koala.xml.kbml.KBMLSerializer;

/**
 * Support for serialising objects in various formats.  Includes
 * support XML (KBML) and Java object serialisation and adds optional
 * compression.
 *
 * @author Matthew.
 * @version $Revision$
 *
 * @see Deserialiser
 */
public final class Serialiser
{
  private Serialiser ()
  {
    // zip
  }

  /**
   * Serialise an object using a given format.
   *
   * @param object The object to serialise.
   * @param file The file to serialise to.
   * @param format The format to use.
   */
  public static void save (Object object, String file,
                           SerialisedFormat format)
    throws FileNotFoundException, IOException
  {
    if (format.getType ().equals (FORMAT_DBXML))
      saveAsDBXML (object, file, format.isCompressed ());

    else if (format.getType ().equals (FORMAT_XML))
      saveAsKBML (object, file, format.isCompressed ());

    else
      saveAsBinary (object, file, format.isCompressed ());
  }

  public static void saveAsBinary (Object object, String fileName)
    throws FileNotFoundException, IOException
  {
    saveAsBinary (object, fileName, false);
  }

  public static void saveAsBinary (Object object, String fileName,
                                   boolean compress)
    throws FileNotFoundException, IOException
  {
    File file = new File (fileName);
    File tempFile = createTempFile ("ser", null, file.getParentFile ());

    OutputStream stream =
      new BufferedOutputStream (new FileOutputStream (tempFile));

    try
    {
      saveAsBinary (object, stream, compress);
    } finally
    {
      stream.close ();
    }

    if (file.exists ())
      file.delete ();

    tempFile.renameTo (file);
  }

  public static void saveAsBinary (Object object, OutputStream stream)
    throws IOException
  {
    saveAsBinary (object, stream, false);
  }

  public static void saveAsBinary (Object object, OutputStream stream,
                                   boolean compress)
    throws IOException
  {
    OutputStream output = compress ? new GZIPOutputStream (stream) : stream;

    ObjectOutputStream objectOutput = new ObjectOutputStream (output);

    objectOutput.writeObject (object);

    output.flush ();

    if (compress)
      ((GZIPOutputStream) output).finish ();
  }

  public static void saveAsKBML (Object object, String fileName)
    throws FileNotFoundException, IOException
  {
    saveAsKBML (object, fileName, false);
  }

  public static void saveAsKBML (Object object, String fileName,
                                 boolean compress)
    throws FileNotFoundException, IOException
  {
    File file = new File (fileName);
    File tempFile = createTempFile ("ser", null, file.getParentFile ());

    FileOutputStream fileStream = new FileOutputStream (tempFile);

    // only buffer non-compressed streams
    OutputStream stream =
      compress ? fileStream : new BufferedOutputStream (fileStream);

    try
    {
      saveAsKBML (object, stream, compress);
    } finally
    {
      stream.close ();
    }

    if (file.exists ())
      file.delete ();

    tempFile.renameTo (file);
  }

  public static void saveAsKBML (Object object, OutputStream stream)
    throws IOException
  {
    saveAsKBML (object, stream, false);
  }

  public static void saveAsKBML (Object object, OutputStream stream,
                                 boolean compress)
    throws IOException
  {
    OutputStream output = compress ? new GZIPOutputStream (stream) : stream;

    KBMLSerializer serialiser = new KBMLSerializer (output);

    serialiser.writeKBMLStartTag ();
    serialiser.writeBean (object);
    serialiser.writeKBMLEndTag ();
    serialiser.flush ();

    if (compress)
      ((GZIPOutputStream) output).finish ();
  }

  public static void saveAsDBXML (Object object, String fileName)
    throws FileNotFoundException, IOException
  {
    saveAsDBXML (object, fileName, false);
  }

  public static void saveAsDBXML (Object object, String fileName, boolean compress)
  throws FileNotFoundException, IOException
{
    File file = new File (fileName);
    File tempFile = createTempFile ("ser", null, file.getParentFile ());

    FileOutputStream fileStream = new FileOutputStream (tempFile);

    // only buffer non-compressed streams
    OutputStream stream =
      compress ? fileStream : new BufferedOutputStream (fileStream);

    try
    {
      saveAsDBXML (object, stream, compress);
    } finally
    {
      stream.close ();
    }

    if (file.exists ())
      file.delete ();

    tempFile.renameTo (file);
  }

  public static void saveAsDBXML (Object object, OutputStream stream)
  throws IOException
{
    saveAsDBXML (object, stream, false);
}

  public static void saveAsDBXML (Object object, OutputStream stream, boolean compress)
    throws IOException
  {
    OutputStream output = compress ? new GZIPOutputStream (stream) : stream;

    DbXmlSerialiser serialiser = new DbXmlSerialiser ();
    serialiser.serialise (output, object);

    if (compress)
      ((GZIPOutputStream)output).finish ();
  }

  /**
   * Open a compressed output stream.  Ensure a call to {@link
   * #finishOutput} is made before closing or the stream will be
   * invalid.
   */
  public static OutputStream openCompressedOutput (OutputStream output)
    throws IOException
  {
    return new GZIPOutputStream (output);
  }

  /**
   * Call this to finalize compressed output streams.  Has no effect
   * on normal streams.
   */
  public static void finishOutput (OutputStream output)
    throws IOException
  {
    if (output instanceof GZIPOutputStream)
      ((GZIPOutputStream)output).finish ();
  }

  /**
   * Write an object to an output stream using the appropriate writer.
   *
   * @param object The object to write.
   * @param output The stream.
   * @param format The format.  Only the type is used.
   * @exception IOException if an error occurs
   */
  public static void writeObject (Object object, OutputStream output,
                                  SerialisedFormat format)
    throws IOException
  {
    if (format.getType ().equals (FORMAT_DBXML))
    {
      DbXmlSerialiser serialiser = new DbXmlSerialiser ();
      serialiser.serialise (output, object);

    } else if (format.getType ().equals (FORMAT_XML))
    {
      KBMLSerializer serialiser = new KBMLSerializer (output);

      serialiser.writeKBMLStartTag ();
      serialiser.writeBean (object);
      serialiser.writeKBMLEndTag ();
      serialiser.flush ();

    } else if (format.getType ().equals (FORMAT_BINARY))
    {
      ObjectOutput objectOutput;

      if (output instanceof ObjectOutput)
        objectOutput = (ObjectOutput)output;
      else
        objectOutput = new ObjectOutputStream (output);

      objectOutput.writeObject (object);

    } else
    {
      throw new IOException ("Unrecognised format: " + format.getType ());
    }
  }
}
