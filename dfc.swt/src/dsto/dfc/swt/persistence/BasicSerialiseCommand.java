package dsto.dfc.swt.persistence;

import static dsto.dfc.swt.persistence.SerialisedFormat.FORMAT_DBXML;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;

import dsto.dfc.swt.commands.BasicFileSaveCommand;

/**
 * Base implementation for commands that save objects using Serialiser. The UI
 * is customized by initFileChooser(). The saveFile () method will usually call
 * save () to save an object using the options specified in the chooser.
 */
public class BasicSerialiseCommand extends BasicFileSaveCommand
{
  public BasicSerialiseCommand ()
  {
    this (null);
  }

  public BasicSerialiseCommand (Control owner)
  {
    super (owner);
  }

  /**
   * Save an object using the settings in the chooser.
   *
   * @param object The object to save.
   * @param chooser The chooser.
   */
  protected void save (Object object, FileDialog chooser)
    throws FileNotFoundException, IOException
  {
    save (object, chooser, new File (chooser.getFileName ()));
  }

  /**
   * Save an object to a specified file, using the option settings in the
   * chooser.
   *
   * @param object The object to save.
   * @param chooser The chooser.
   * @param file The file to save to.
   */
  protected void save (Object object, FileDialog chooser, File file)
    throws FileNotFoundException, IOException
  {
    SerialisedFormat format =
      new SerialisedFormat (FORMAT_DBXML, false);
    Serialiser.save (object, file.getCanonicalPath (), format);
  }
}
