package dsto.dfc.swt.persistence;

import dsto.dfc.swt.commands.BasicFileSaveAsCommand;


/**
 * Basic command to execute the saveAs() method on a {@link Document}.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class BasicDocumentSaveAsCommand extends BasicFileSaveAsCommand
{
  protected Document document;

  public BasicDocumentSaveAsCommand (Document document)
  {
    super (document.getClient ());
    this.document = document;
  }

  @Override
  public void execute ()
  {
    document.saveAs ();
  }

  @Override
  public String getDescription ()
  {
    return "Save the document to a new location";
  }
}
