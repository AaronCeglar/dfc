package dsto.dfc.swt.persistence;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.xml.sax.SAXException;

import fr.dyade.koala.xml.kbml.KBMLDeserializer;
import fr.dyade.koala.xml.kbml.KBMLSerializer;

/**
 * Serialiser that uses KBML IO.
 *
 * @author mpp
 * @version $Revision$
 */
public class KbmlSerialiser implements ISerialiser
{
  public void serialise (OutputStream output, Object object) throws IOException
  {
    KBMLSerializer serialiser = new KBMLSerializer (output);

    serialiser.writeKBMLStartTag ();
    serialiser.writeBean (object);
    serialiser.writeKBMLEndTag ();

    serialiser.flush ();
  }

  public Object deserialise (InputStream input)
    throws IOException, ClassNotFoundException
  {
    try
    {
      KBMLDeserializer deserialiser = new KBMLDeserializer (input);

      return deserialiser.readBean ();
    } catch (SAXException ex)
    {
      if (ex.getException () != null)
      {
        ex.getException ().printStackTrace ();

        throw new IOException ("Exception during XML parsing: " + ex.getException ());
      } else
      {
        throw new IOException (ex.getMessage ());
      }
    } catch (InstantiationException ex)
    {
      throw new IOException ("Error instantiating class: " + ex);
    } catch (IllegalAccessException ex)
    {
      throw new IOException ("Error accessing class: " + ex);
    }
  }
}
