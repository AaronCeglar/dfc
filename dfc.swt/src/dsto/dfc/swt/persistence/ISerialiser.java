package dsto.dfc.swt.persistence;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Defines an object that can serialise/deserialise an object to a stream.
 *
 * @author mpp
 */
public interface ISerialiser
{
  public void serialise (OutputStream output, Object object)
    throws IOException;

  public Object deserialise (InputStream input)
    throws IOException, ClassNotFoundException;
}
