package dsto.dfc.logging;

import javax.servlet.ServletContext;

/**
 * Directs DFC log events to a servlet's logger.
 * 
 * @author Matthew Phillips
 */
public class ServletLogger implements LogListener
{
  private ServletContext context;
  private LogFilter filter;
  
  public ServletLogger (ServletContext context)
  {
    this.context = context;
    this.filter = new LogFilter ();
    
    Log.addLogListener (this);
  }

  public void dispose ()
  {
    Log.removeLogListener (this);    
  }

  public void setEnabled (int type, boolean enabled)
  {
    filter.setEnabled (type, enabled);
  }
  
  public void messageReceived (LogEvent e)
  {
    if (filter.select (e))
    {
      String message = "[" + Log.getTypeString (e.getType ()) + "]: " + e.getMessage ();
      
      if (e.getException () == null)
        context.log (message);
      else
        context.log (message, e.getException ());
    }
  }
}
