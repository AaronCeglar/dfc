package dsto.dfc.servlets;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import dsto.dfc.util.MissingParameterException;

import dsto.dfc.databeans.IDataObject;

/**
 * General servlet support routines.
 * 
 * @author Matthew Phillips
 */
public final class ServletSupport
{
  private ServletSupport ()
  {
    // zip
  }

  /**
   * Shortcut to extract an integer parameter from a servlet request.
   * 
   * @param request The request.
   * @param name The parameter name.
   * @param defaultValue The value to return if name is not defined or invalid.
   */
  public static int getParam (ServletRequest request,
                                String name, int defaultValue)
  {
    String param = request.getParameter (name);
    int value = defaultValue;
    
    if (param != null)
    {
      try
      {
        value = Integer.parseInt (param);
      } catch (NumberFormatException ex)
      {
        // zip
      }
    }
    
    return value;
  }
  
  /**
   * Get a non-empty parameter value.
   * 
   * @param request The request.
   * @param param The parameter name.
   * @return The parameter value. This will be non-null and non-empty.
   * 
   * @throws MissingParameterException if the parameter is either null or the
   *           empty string.
   */
  public static String getNonEmptyParam (ServletRequest request,
                                          String param)
    throws MissingParameterException
  {
    String value = getParam (request, param).trim ();
    
    if (value.length () > 0)
      return value;
    else
      throw new MissingParameterException (param + " cannot be empty");
  }
  
  /**
   * Get an optional non-empty parameter value. An empty parameter value is
   * treated as non existent.
   * 
   * @param request The request.
   * @param param The parameter name.
   * @return The parameter value. This will either be null or non-empty.
   */
  public static String getOptionalNonEmptyParam (ServletRequest request,
                                                   String param)
  {
    String value = request.getParameter (param);
    
    if (value == null || value.trim ().length () == 0)
      return null;
    else
      return value;
  }
  
  /**
   * Get an a required parameter value.
   * 
   * @param request The request.
   * @param param The parameter name.
   * @return The parameter value. This will be be non-null.
   * 
   * @throws MissingParameterException if the parameter is not present.
   */
  public static String getParam (ServletRequest request,
                                       String param)
    throws MissingParameterException
  {
    String value = request.getParameter (param);
    
    if (value != null)
      return value;
    else
      throw new MissingParameterException ("Missing " + param);
  }
  
  public static String getParam (ServletRequest request,
                                   String param, String defaultValue)
  {
    String value = request.getParameter (param);
    
    return value == null || value.trim ().length () == 0 ? defaultValue : value;
  }
  
  /**
   * Generate a HTML name="field" value="value" pair.
   * 
   * @param field The field name.
   * @param values The value set. The value is read using values.getValue
   *          (field).
   * @return The HTML string.
   * 
   * @see #htmlNameValue(String, Object)
   */
  public static String htmlNameValue (String field, IDataObject values)
  {
    return htmlNameValue (field, values.getValue (field));
  }
  
  /**
   * Generate a HTML name="field" value="value" pair.
   * 
   * @param field The field name.
   * @param value The value. If null, the value= part is left out.
   * @return The HTML string.
   */
  public static String htmlNameValue (String field, Object value)
  {
    return "name=\"" + field + "\" " + htmlValueAttr (value);
  }
  
  public static String htmlValueAttr (Object value)
  {
    return htmlAttr ("value", value);
  }
  
  /**
   * Generate a HTML attr="value" statement.
   * 
   * @param attr The attribute name.
   * @param value The value. If null, "" is returned..
   * @return The HTML string.
   */
  public static String htmlAttr (String attr, Object value)
  {
    if (value == null)
      return "";
    else
      return attr + "=\"" + toHTML (value.toString ()) + "\"";
  }
  
  /**
   * Generate a HTML &lt;a href="...">blah&lt;/a> value.
   * 
   * @param text The link text.
   * @param url The URL. May be null in which case just the text is returned.
   */
  public static String htmlLink (String text, String url)
  {
    if (url == null)
      return text;
    else
      return "<a href=\"" + url + "\">" + text + "</a>";
  }
  
  /**
   * Generate a HTML <code>checked="true"</code> if the parameter is true or
   * <code>""</code> if not.
   */
  public static String htmlChecked (boolean checked)
  {
    return checked ? "checked=\"true\"" : "";
  }
  
  /**
   * Escape a string for use in HTML.
   * Courtesy of http://www.rgagnon.com/javadetails/java-0306.html.
   * 
   * @param string The string to escape. May be null.
   * 
   * @return The escaped string.
   */
  public static String toHTML (String string)
  {
    if (string == null)
      return null;
    
    StringBuffer sb = new StringBuffer (string.length ());
    // true if last char was blank
    boolean lastWasBlankChar = false;
    int len = string.length ();
    char c;

    for (int i = 0; i < len; i++)
    {
      c = string.charAt (i);
      if (c == ' ')
      {
        // blank gets extra work,
        // this solves the problem you get if you replace all
        // blanks with &nbsp;, if you do that you lose
        // word breaking
        if (lastWasBlankChar)
        {
          lastWasBlankChar = false;
          sb.append ("&nbsp;");
        } else
        {
          lastWasBlankChar = true;
          sb.append (' ');
        }
      } else
      {
        lastWasBlankChar = false;
        //
        // HTML Special Chars
        if (c == '"')
          sb.append ("&quot;");
        else if (c == '&')
          sb.append ("&amp;");
        else if (c == '<')
          sb.append ("&lt;");
        else if (c == '>')
          sb.append ("&gt;");
        else if (c == '\n')
          // Handle Newline
          sb.append ("<br/>");
        else
        {
          int ci = 0xffff & c;
          if (ci < 160)
            // nothing special only 7 Bit
            sb.append (c);
          else
          {
            // Not 7 Bit use the unicode system
            sb.append ("&#");
            sb.append (Integer.toString (ci));
            sb.append (';');
          }
        }
      }
    }
    
    return sb.toString ();
  }
  
  /**
   * Post an error by lodging it as the "error" attribute on the
   * current session.
   * 
   * @param request The request that the error happened in.
   * @param message The error message.
   * 
   * @see #getError(HttpServletRequest)
   * @see #clearError(HttpServletRequest)
   */
  public static void postError (HttpServletRequest request, String message)
  {
    request.getSession (true).setAttribute ("error", message);
  }
  
  /**
   * Get the current error (if any) for the session.
   * 
   * @param request The request that the error happened in.
   * 
   * @return The error message or null for none.
   * 
   * @see #postError(HttpServletRequest, String)
   * @see #clearError(HttpServletRequest)
   */
  public static String getError (HttpServletRequest request)
  {
    if (request.getSession () != null)
      return (String)request.getSession ().getAttribute ("error");
    else
      return null;
  }
  
  /**
   * Clear the current error (if any) for the session.
   * 
   * @param request The request that the error happened in.
   * 
   * @see #postError(HttpServletRequest, String)
   * @see #getError(HttpServletRequest)
   */
  public static void clearError (HttpServletRequest request)
  {
    if (request.getSession () != null)
      request.getSession ().removeAttribute ("error");
  }
}