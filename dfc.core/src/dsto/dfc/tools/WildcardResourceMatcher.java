/*
 * JarX JAR archive creator.
 *
 * Copyright (c) 2000, 2005 Matthew Phillips, DSTO.
 * <matthew.phillips@dsto.defence.gov.au>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License (http://www.gnu.org/copyleft/gpl.html)
 * for full details.
 *
 * $Id$
 */
package dsto.dfc.tools;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * A resource matcher that matches via wilcard (file glob-like).
 * Globbing is actually implemented using regular expressions, so it's
 * not quite compatible.
 *
 * @version $Revision$
 */
public final class WildcardResourceMatcher implements ResourceMatcher
{
  private String expression;
  private Pattern pattern;

  public WildcardResourceMatcher (String expression)
    throws IllegalArgumentException
  {
    try
    {
      if (expression.indexOf ('*') != -1)
        this.pattern = Pattern.compile (makeRegex (expression));

      this.expression = expression;
    } catch (PatternSyntaxException ex)
    {
      throw new IllegalArgumentException
        ("Invalid regular expression: " + ex.getMessage ());
    }
  }

  public boolean matches (String resourceName)
  {
    if (pattern != null)
      return pattern.matcher (resourceName).matches ();
    else
      return expression.equals (resourceName);
  }

  private static String makeRegex (String wildcardExpr)
  {
    StringBuilder regex = new StringBuilder ();

    for (int i = 0; i < wildcardExpr.length (); i++)
    {
      char c = wildcardExpr.charAt (i);

      if (c == '*')
        regex.append (".*");
      else if (c == '.')
        regex.append ("\\.");
      else
        regex.append (c);
    }

    return regex.toString ();
  }
}
