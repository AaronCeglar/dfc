/*
 * JarX JAR archive creator.
 *
 * Copyright (c) 2000 Matthew Phillips, DSTO.
 * <matthew.phillips@dsto.defence.gov.au>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License (http://www.gnu.org/copyleft/gpl.html)
 * for full details.
 *
 * $Id$
 */
package dsto.dfc.tools;

/**
 * Defines a class that matches resource names.
 *
 * @version $Revision$
 */
public interface ResourceMatcher
{
  public boolean matches (String resourceName);
} 
