/*
 * JarX JAR archive creator.
 *
 * Copyright (c) 2000 Matthew Phillips, DSTO.
 * <matthew.phillips@dsto.defence.gov.au>
 *
 * This class was inspired by ZipLock written by Karl Moss and Stuart
 * D. Gathman and contains some code derived from the original class
 * (most notably getClassResources ()).  Original source is available
 * from http://www.bmsi.com/java.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License (http://www.gnu.org/copyleft/gpl.html)
 * for full details.
 *
 * -------------------------------------------------------------------
 *
 * Original copyright notice follows (as requested by the authors of
 * ZipLock)
 *
 * Enhanced by Stuart D. Gathman from an original progam named
 * RollCall.  Copyright (c) 1998 Business Management Systems, Inc.
 *
 * Original Copyright (c) 1998 Karl Moss. All Rights Reserved.
 *
 * -------------------------------------------------------------------
 *
 * $Id$
 */
package dsto.dfc.tools;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * <p>A JAR file generator tool that operates by taking a one or more
 * 'root' resource patterns (eg the main class of an application or a
 * set of library package names) and generates a JAR containing the
 * root resources plus any referenced resources.<p>
 *
 * <p><b>NOTE:</b> All resources must be named using the resource
 * syntax: ie most importantly classes are NOT referenced by their
 * logical class name.  For example, the logical class name
 * com.frobsoft.stuff.Widget should be referred to by the resource
 * name "com/frobsoft/stuff/Widget.class".<p>
 *
 * TODO:
 * <ul>
 *   <li>Warn when a root resource is not found.
 * </ul>
 *
 * @author Matthew Phillips <matthew.phillips@dsto.defence.gov.au>
 * @version $Revision$
 * @see JarXTask for a Ant build task that uses JarX.
 */
public class JarX
{
  private static final String [] SYSTEM_PACKAGES = new String []
  {
    "java.", "javax.accessibility", "javax.naming", "javax.rmi", "javax.net",
    "javax.sound", "javax.swing", "javax.transaction", "sun.", "org.omg",
    "com.sun.corba", "com.sun.image", "com.sun.java", "com.sun.javadoc",
    "com.sun.jdi", "com.sun.jndi", "com.sun.media", "com.sun.naming",
    "com.sun.net.ssl", "com.sun.org.omg", "com.sun.rmi.rmid", "com.sun.rsajca",
    "com.sun.rsasign", "com.sun.tools", "javax.media.j3d",
    "javax.vecmath", "com.sun.j3d.utils", "com.sun.j3d.audioengines",
    "com.sun.j3d.utils", "com.sun.j3d.loaders.lw3d",
    "com.sun.j3d.loaders.objectfile",
    "com.sun.j3d.loaders.Loader",
    "com.sun.j3d.loaders.Scene",
    "com.sun.j3d.loaders.LoaderBase",
    "com.sun.j3d.loaders.SceneBase",
    "com.sun.j3d.loaders.IncorrectFormatException",
    "com.sun.j3d.loaders.ParsingErrorException"
  };

  private final String [] SYSTEM_CLASSPATH = getSystemClassPath ();

  /** True if JarX should create a JAR-compatible archive. */
  private boolean useJarFormat = true;

  /** The JAR manifest file to use as the base of the archive manifest. */
  private File manifestFile = null;

  /** The JAR manifest.  Only used if useJarFormat == true. */
  private Manifest manifest = null;

  /** The (optional) main class name specified in the JAR manifest
      'Main-Class' attribute. */
  private String mainClassName = null;

  /** The archive file that is being written to. */
  private File archiveFile;

  /** The archive stream being written to. */
  private ZipOutputStream archiveStream;

  /** List of ResourceMatcher's for excluding resources. */
  private ArrayList excludes = new ArrayList ();

  /** List of resource names (String's) to use as roots for resource
      set. */
  private ArrayList roots = new ArrayList ();

  /** List of ResourceMatcher's to use as resource patterns to include
      in roots. This is kept seperate from roots, because, if this
      list is empty, JarX can use a slighlty faster resource scan
      approach driven by scanning the roots list. */
  private ArrayList rootMatchers = new ArrayList ();

  /** The total list of resources added to the JAR. */
  private HashSet resources = new HashSet ();

  /** Open ZIP file cache: map ZIP file name to ZipFile. */
  private HashMap openZips = new HashMap ();

  /** The path used to find resources. */
  private String [] classPath = SYSTEM_CLASSPATH;

  /** True if findClassResources () should try to find used resources
      via hints from calls to getResource (). */
  private boolean autoFindResources = false;

  // used by findClassResources ()
  private final IntList classInfo = new IntList (); // which names are classes
  private final IntList stringTbl = new IntList (); // which entries are Strings
  private final Vector methInfo = new Vector ();

  /**
   * Create an instance of JarX writing to a ZIP archive.
   *
   * @param archiveFile The file to write the ZIP archive to.
   */
  public JarX (File archiveFile)
  {
    this (archiveFile, true);
  }

  /**
   * Create an instance of JarX writing to a ZIP or JAR archive.
   *
   * @param archiveFile The file to write the ZIP archive to.
   * @param useJarFormat True if the archive should be JAR-compatible.
   *
   * @see #setUseJarFormat
   */
  public JarX (File archiveFile, boolean useJarFormat)
  {
    this.archiveFile = archiveFile;
    this.useJarFormat = useJarFormat;

    setupDefaultExcludes ();
  }

  /**
   * Get the JVM system class path as an array of String's.
   */
  public static String [] getSystemClassPath ()
  {
    String path = System.getProperty ("java.class.path");
    String pathsep = System.getProperty ("path.separator");

    StringTokenizer tok = new StringTokenizer (path, pathsep);

    String [] classPath = new String [tok.countTokens ()];

    for (int i = 0; tok.hasMoreTokens (); i++)
      classPath [i] = tok.nextToken ();

    return classPath;
  }

  /**
   * Generate the resource name for a logical class name.
   *
   * @param className The name of a class eg "com.frobsoft.Widget".
   * @return The resource name of the class eg
   * "com/frobsoft/Widget.class".
   */
  public static String classToResourceName (String className)
  {
    return className.replace ('.', '/') + ".class";
  }

  /**
   * Set whether to generate a JAR-compatible archive (generate a
   * manifest and use a java.util.JarOutputStream).
   *
   * @see #setManifestFile
   * @see #setMainClassName
   */
  public void setUseJarFormat (boolean useJarFormat)
  {
    this.useJarFormat = useJarFormat;
  }

  /**
   * Set the file to use as the base of the JAR manifest.  Implicitly
   * sets useJarFormat to true.
   *
   * @see #setUseJarFormat
   */
  public void setManifestFile (File newManifestFile)
  {
    this.manifestFile = newManifestFile;
    this.useJarFormat = true;
  }

  /**
   * Set the name (not the resource name) of the class to specify as
   * the main application in the JAR manifest (the "Main-Class"
   * attribute).  This class should define a 'static void main (String
   * [])' method.  Implicitly sets useJarFormat to true and adds the
   * class to the root resource set if not already added.
   *
   * <p>Example: setMainClassName ("com.frobsoft.Main").
   *
   * @see #setUseJarFormat
   */
  public void setMainClassName (String mainClassName)
  {
    this.mainClassName = mainClassName;
    this.useJarFormat = true;

    // add resource to roots if not already added
    String resourceName = classToResourceName (mainClassName);

    if (!roots.contains (resourceName))
      roots.add (resourceName);
  }

  /**
   * Set the path used to find resources.
   *
   * @param newClassPath The new class path.  Must contain either
   * directory paths or ZIP/JAR file names.
   */
  public void setClassPath (String [] newClassPath)
  {
    classPath = newClassPath;
  }

  /**
   * Clear the exclude rule set.
   *
   * @see #addExcludes(java.util.List)
   */
  public void clearExcludes ()
  {
    excludes.clear ();
  }

  /**
   * Convenience version of addExcludes.
   *
   * @param newExcludes An array of exclude expressions.
   *
   * @see #addExcludes(java.util.List) for full description.
   */
  public void addExcludes (String [] newExcludes)
  {
    addExcludes (Arrays.asList (newExcludes));
  }

  /**
   * Add simple string or wildcard rules to the exclude set.  The
   * default exclude set exludes the java and javax packages plus the
   * sun and com.sun packages ("java/*", "javax/*",
   * "com/sun/*". sun/*").
   *
   * @param newExcludes A list of strings to used as exclude wildcard
   * patterns, where "*" can be used as a kleene star operator.
   *
   * @see #addExcludeRegexps(java.util.List) - you can use regular
   * expressions too!
   */
  public void addExcludes (List newExcludes)
  {
    for (int i = 0; i < newExcludes.size (); i++)
      excludes.add (new WildcardResourceMatcher ((String)newExcludes.get (i)));
  }

  /**
   * Convenience version of addExcludes.
   *
   * @param newExcludes A list of regular expressions to add to the
   * exclude set.
   *
   * @see #addExcludeRegexps(java.util.List)
   */
  public void addExcludeRegexps (String [] newExcludes)
    throws IllegalArgumentException
  {
    addExcludeRegexps (Arrays.asList (newExcludes));
  }

  /**
   * Add a set of regular expression rules to the exclude set.  The
   * regular expressions syntax is the Perl 5 syntax supported by
   * OROMatcher (http://jakarta.apache.org/oro/index.html).
   *
   * @param newExcludes A list of regular expression strings.
   * @exception IllegalArgumentException if one of the regular
   * expressions has invalid syntax.
   */
  public void addExcludeRegexps (List newExcludes)
    throws IllegalArgumentException
  {
    for (int i = 0; i < newExcludes.size (); i++)
      excludes.add (new RegexpResourceMatcher ((String)newExcludes.get (i)));
  }

  /**
   * Convenience version of addExcludeMatchers.
   *
   * @see #addExcludeMatchers(java.util.List)
   */
  public void addExcludeMatchers (ResourceMatcher [] newExcludes)
  {
    excludes.addAll (Arrays.asList (newExcludes));
  }

  /**
   * Add a list of ResourceMatcher's to the excludes list.
   *
   * @param newExcludes A list of ResourceMatcher instances.
   *
   * @see ResourceMatcher
   * @see RegexpResourceMatcher
   * @see WildcardResourceMatcher
   */
  public void addExcludeMatchers (List newExcludes)
  {
    excludes.addAll (newExcludes);
  }

  /**
   * Convenience version of addRoots.
   *
   * @see #addRoots(java.util.List)
   */
  public void addRoots (String [] newRoots)
  {
    addRoots (Arrays.asList (newRoots));
  }

  /**
   * Add a list of resource names as roots of the resource tree to
   * archive.  The resource names may contain the '*' as a wildcard
   * (kleene star) operator.
   *
   * @param newRoots A list of resource names.
   *
   * @see #addRootRegexps(java.util.List)
   * @see WildcardResourceMatcher
   */
  public void addRoots (List newRoots)
  {
    // TODO: handle case where user has specified a class name rather than
    // a resource name
    for (int i = 0; i < newRoots.size (); i++)
    {
      String newRoot = (String)newRoots.get (i);

      // if using wildcard operator, add to the rootMatchers list
      if (newRoot.indexOf ('*') != -1)
        rootMatchers.add (new WildcardResourceMatcher (newRoot));
      else
        roots.add (newRoot);
    }
  }

  /**
   * Convenience version of addRootRegexps.
   *
   * @see #addRootRegexps(java.util.List)
   */
  public void addRootRegexps (String [] regexps)
  {
    addRootRegexps (Arrays.asList (regexps));
  }

  /**
   * Add a list of regular expressions to the root resources list.
   *
   * @param regexps a value of type 'List'
   */
  public void addRootRegexps (List regexps)
  {
    for (int i = 0; i < regexps.size (); i++)
      rootMatchers.add (new RegexpResourceMatcher ((String)regexps.get (i)));
  }

  /**
   * Convenience version of addRootMatchers.
   *
   * @see #addRootMatchers(java.util.List)
   */
  public void addRootMatchers (ResourceMatcher [] newResourceMatchers)
  {
    rootMatchers.addAll (Arrays.asList (newResourceMatchers));
  }

  /**
   * Add a list of ResourceMatcher's to the root resources list.
   *
   * @param newResourceMatchers A list of ResourceMatcher instances.
   *
   * @see ResourceMatcher
   * @see RegexpResourceMatcher
   * @see WildcardResourceMatcher
   */
  public void addRootMatchers (List newResourceMatchers)
  {
    rootMatchers.addAll (newResourceMatchers);
  }

  /**
   * Set to true if JarX should include resources guessed by looking
   * at calls to Class.getResource().
   */
  public void setAutoFindResources (boolean newValue)
  {
    autoFindResources = newValue;
  }

  /**
   * Start archive creation.
   *
   * @exception IOException if an error ocurred while creating the
   * archive.
   */
  public void run () throws IOException
  {
    // open archive
    FileOutputStream archiveOutput = new FileOutputStream (archiveFile);

    try
    {
      if (useJarFormat)
      {
        // handle JAR archive setup

        if (manifestFile != null)
          manifest = new Manifest (new FileInputStream (manifestFile));
        else
          manifest = new Manifest ();

        Attributes mainAttrs = manifest.getMainAttributes ();
        mainAttrs.putValue ("Manifest-Version", "1.0");

        if (mainClassName != null)
          mainAttrs.putValue ("Main-Class", mainClassName);

        archiveStream = new JarOutputStream (archiveOutput);
      } else
        archiveStream = new ZipOutputStream (archiveOutput);

      // if no pattern-based root matchers are registered, use direct
      // scan method.
      if (rootMatchers.isEmpty ())
        runDirect ();
      else
        runFullScan ();

      if (useJarFormat)
      {
        // write manifest to archive

        ZipEntry manifestEntry = new ZipEntry (JarFile.MANIFEST_NAME);
        archiveStream.putNextEntry (manifestEntry);
        manifest.write (new BufferedOutputStream (archiveStream));
        archiveStream.closeEntry ();
      }

      archiveStream.close ();

      closeOpenZips ();
    } catch (IOException ex)
    {
      // catch IO exceptions, delete invalid archive file and rethrow
      // exception.

      try
      {
        if (archiveStream != null)
          archiveStream.close ();
      } catch (IOException ex2)
      {
        // ignore, just wanted to make sure archiveStream was closed
        // if possible.
      }

      archiveFile.delete ();

      closeOpenZips ();

      throw ex;
    }
  }


  public void dispose ()
  {
    try
    {
      archiveStream.close ();
    } catch (IOException ex)
    {
      // don't care
    }

    closeOpenZips ();
  }

  // Implementation methods

  protected void setupDefaultExcludes ()
  {
    excludes.add
      (new RegexpResourceMatcher ("CVS/"));
  }

  /**
   * Tests if a resource is in the root include set directly by name.
   */
  protected boolean isResourceIncludedByName (String resourceName)
  {
    // scan roots
    for (int i = 0; i < roots.size (); i++)
    {
      if (roots.get (i).equals (resourceName))
        return true;
    }

    return false;
  }

  /**
   * Tests if a resource is in the root include set by matching a pettern.
   */
  protected boolean isResourceIncludedByMatch (String resourceName)
  {
    // scan rootMatchers
    for (int i = 0; i < rootMatchers.size (); i++)
    {
      ResourceMatcher matcher = (ResourceMatcher)rootMatchers.get (i);

      if (matcher.matches (resourceName))
        return true;
    }

    return false;
  }

  /**
   * Test if a resource is matched by the excludes set.
   */
  protected boolean isResourceExcludedByMatch (String resourceName)
  {
    for (int i = 0; i < excludes.size (); i++)
    {
      ResourceMatcher matcher = (ResourceMatcher)excludes.get (i);

      if (matcher.matches (resourceName))
        return true;
    }

    return false;
  }

  /**
   * Test if a resource has been stored in the archive.
   */
  protected boolean isResourceStored (String resourceName)
  {
    return resources.contains (resourceName);
  }

  /**
   * Test if a resource should be stored ie (a) is not already stored
   * (b) is included in the root resource set and (c) is not in the
   * excludes set.
   *
   * @param resourceName a value of type 'String'
   * @return a value of type 'boolean'
   *
   * @see #isSystemClass(String)
   * return true;#shouldIncludeResource
   */
  protected boolean shouldStoreResource (String resourceName)
  {
    return !isResourceStored (resourceName) &&
           shouldIncludeResource (resourceName);
  }

  /**
   * Test if a resource found by scanning the classpath should be
   * included using the rules: (a) resource must not be a system class
   * and (b) resource must EITHER be included directly by name OR
   * matched by a pattern and not excluded by a pattern (ie exclude
   * patterns override include patterns, but not inclusion directly by
   * name).
   *
   * @param resourceName a value of type 'String'
   * @return a value of type 'boolean'
   */
  protected boolean shouldIncludeResource (String resourceName)
  {
    return !isSystemClass (resourceName) &&
           (isResourceIncludedByName (resourceName) ||
             (isResourceIncludedByMatch (resourceName) &&
              !isResourceExcludedByMatch (resourceName)));
  }

  /**
   * Test if a resource imported by another resource should be
   * included using the rules: (a) resource must not be a system class
   * and (b) resource must either be included directly by name OR not
   * be excluded by a pattern.
   *
   * @param resourceName a value of type 'String'
   * @return a value of type 'boolean'
   */
  protected boolean shouldIncludeImportedResource (String resourceName)
  {
    return !isSystemClass (resourceName) &&
            (isResourceIncludedByName (resourceName) ||
             !isResourceExcludedByMatch (resourceName));
  }

  /**
   * Test if a path name refers to a ZIP archive.  Currently returns
   * true if the path has a '.zip' or '.jar' extension.
   */
  protected boolean isZipArchive (String path)
  {
    String lPath = path.toLowerCase ();

    return lPath.endsWith (".zip") || lPath.endsWith (".jar");
  }

  /**
   * Run the archive operation in 'direct' mode: scan each root
   * resource in roots and add it via addResourceFromClasspath().
   * This method should be slightly faster than that used by
   * runFullScan() but will ignore any resources specified via
   * patterns in in rootMatchers.
   *
   * @see #runFullScan
   * @see #addResourceFromClasspath
   */
  protected void runDirect ()
  {
    for (int i = 0; i < roots.size (); i++)
    {
      String root = (String)roots.get (i);

      try
      {
        if (shouldStoreResource (root))
          addResourceFromClasspath (root);
      } catch (IOException ex)
      {
        warn ("Error adding resource '" + root + "': " + ex);
      }
    }
  }

  /**
   * Run the archive operation in 'full scan' mode: scan all resources
   * in the classpath looking for resource names that match those in
   * roots and rootMatchers.
   *
   * @see #runDirect
   * @see #scanArchive
   * @see #scanDirectory
   */
  protected void runFullScan ()
  {
    for (int i = 0; i < classPath.length; i++)
    {
      String path = classPath [i];

      if (isZipArchive (path))
        scanArchive (path);
      else
        scanDirectory (new File (path), "");
    }
  }

  /**
   * Scan a ZIP archive for resources that match those in the root
   * set.
   *
   * @param archivePath The path to a ZIP-format archive file.
   */
  protected void scanArchive (String archivePath)
  {
    try
    {
      ZipFile zipFile = findOrOpenZip (archivePath);

      for (Enumeration e = zipFile.entries (); e.hasMoreElements (); )
      {
        ZipEntry entry = (ZipEntry)e.nextElement ();

        // if not a directory and not already archived...
        if (!entry.isDirectory () &&
            shouldStoreResource (entry.getName ()))
        {
          addResourceFromZip (entry.getName (), zipFile);
        }
      }
    } catch (IOException ex)
    {
      warn ("Invalid archive found in classpath: " +
            archivePath + " (" + ex.getMessage () + ")");
    }
  }

  /**
   * Scan a directory and all its subdirectories for resources that
   * match those in the root set.
   *
   * @param directory The directory to scan.
   * @param resourcePath The resource name of the directory (ie its
   * relative path from the root of the scan).
   */
  protected void scanDirectory (File directory, String resourcePath)
  {
    if (directory.exists () && directory.isDirectory ())
    {
      File [] files = directory.listFiles ();

      for (int i = 0; i < files.length; i++)
      {
        File file = files [i];
        String newResourcePath;

        if (resourcePath.length () == 0)
          newResourcePath = file.getName ();
        else
          newResourcePath = resourcePath + '/' + file.getName ();

        if (file.isDirectory ())
        {
          scanDirectory (file, newResourcePath);
        } else if (shouldStoreResource (newResourcePath))
        {
          try
          {
            addResourceFromFile (newResourcePath, file);
          } catch (IOException ex)
          {
            warn ("Failed to add file '" + file.getAbsolutePath () +
                  "': " + ex.getMessage ());
          }
        }
      }
    } else
    {
      warn ("Invalid directory found on classpath: " +
            directory.getAbsolutePath ());
    }
  }

  /**
   * Add a resource stored in a ZIP archive (via addResource()).
   *
   * @param resourceName The name of the resource.
   * @param zipFile The ZIP file to read from.
   *
   * @exception IOException if an IO error occurs.
   * @see #addResource
   */
  protected void addResourceFromZip (String resourceName, ZipFile zipFile)
    throws IOException
  {
    byte [] resourceData = readResourceDataFromZip (resourceName, zipFile);

    addResource (resourceName, resourceData);
  }

  /**
   * Add a resource stored in a file (via addResource()).
   *
   * @param resourceName The name of the resource.
   * @param file The file to read from.
   *
   * @exception IOException if an IO error occurs.
   * @see #addResource
   */
  protected void addResourceFromFile (String resourceName, File file)
    throws IOException
  {
    byte [] resourceData = readResourceDataFromFile (resourceName, file);

    addResource (resourceName, resourceData);
  }

  /**
   * Add a resource (via addResource()) found by searching the
   * classpath.
   *
   * @param resourceName The resource name.
   *
   * @exception IOException if an IO error occurs, including if the
   * resource is not found.
   * @see #setClassPath
   * @see #addResource
   */
  protected void addResourceFromClasspath (String resourceName)
    throws IOException
  {
    for (int i = 0; i < classPath.length; i++)
    {
      try
      {
        byte [] resourceData = readResourceData (classPath [i], resourceName);

        addResource (resourceName, resourceData);

        return;
      } catch (IOException ex)
      {
        // ignore exception and allow loop to continue (yes, dodgy)
      }
    }

    // failed to find resource: throw exception
    throw new IOException ("Resource '" + resourceName +
                           "' not found in classpath");
  }

  /**
   * Add a resource, and all the resources it references in the case
   * of classes, to the archive.
   *
   * @param resourceName The name of the resource.
   * @param resourceData The resource data.
   *
   * @exception IOException if an IO exception occurs while writing
   * the resource.
   * @see #findClassResources
   */
  protected void addResource (String resourceName, byte [] resourceData)
    throws IOException
  {
    storeResource (resourceName, resourceData);

    // if a class, add its imported resources
    if (resourceName.endsWith (".class"))
    {
      String [] classResources =
        findClassResources (resourceName, resourceData);

      for (int i = 0; i < classResources.length; i++)
      {
        String classResource = classResources [i];

        if (!isResourceStored (classResource) &&
            shouldIncludeImportedResource (classResource))
        {
          try
          {
            addResourceFromClasspath (classResource);
          } catch (IOException ex)
          {
            warn ("Failed to add resource " + classResource +
                  " used by " + resourceName + " (" + ex.getMessage () + ")");
          }
        }
      }
    }
  }

  /**
   * Read the data associated with a resource from either a ZIP
   * archive or a file.
   *
   * @param path The path to a ZIP archive or a file.
   * @param resourceName The name of the resource.
   * @return The resource data.
   *
   * @exception IOException if the data could not be read.
   * @see #readResourceDataFromFile
   * @see #readResourceDataFromZip
   */
  protected byte [] readResourceData (String path, String resourceName)
    throws IOException
  {
    byte [] resourceData = null;

    // If the path is a zip or jar file, look inside for the
    // resource
    if (isZipArchive (path))
    {
      ZipFile zipFile = findOrOpenZip (path);

      resourceData = readResourceDataFromZip (resourceName, zipFile);
    } else
    {
      // not a zip or jar file: look for the resource in a file
      String fullName = path;

      // put in the directory separator if necessary
      if (!path.endsWith (File.separator))
        fullName += File.separatorChar;

      fullName += resourceName.replace ('/', File.separatorChar);

      resourceData =
        readResourceDataFromFile (resourceName, new File (fullName));
    }

    return resourceData;
  }

  /**
   * Read resource data from a file.
   *
   * @param resourceName The name of the resource.
   * @param resourceFile The file to read from.
   * @return The resource data.
   * @exception IOException if the data could not be read.
   */
  protected byte [] readResourceDataFromFile (String resourceName,
                                              File resourceFile)
    throws IOException
  {
    // check to make sure the file exists and it is a file
    if (resourceFile.exists () && resourceFile.isFile ())
    {
      // create an input stream and read the file

      FileInputStream resourceFileStream = new FileInputStream (resourceFile);
      long length = resourceFile.length ();
      byte [] resourceData = new byte [(int)length];
      DataInputStream ds = new DataInputStream (resourceFileStream);

      ds.readFully (resourceData);

      ds.close ();

      return resourceData;
    } else
    {
      throw new IOException ("Resource file '" +
                              resourceFile.getCanonicalPath () +
                             "' not found");
    }
  }

  /**
   * Read resource data from a ZIP archive.
   *
   * @param resourceName The name of the resource.
   * @param zipFile The ZIP archive to read from.
   * @return The resource data.
   * @exception IOException if the data could not be read.
   */
  protected byte [] readResourceDataFromZip (String resourceName,
                                             ZipFile zipFile)
    throws IOException
  {
    ZipEntry entry = zipFile.getEntry (resourceName);

    if (entry == null)
    {
      // entry not found, throw a wobbly
      throw new IOException ("Resource '" + resourceName +
                             "' not found in '" + zipFile + "'");
    }

    InputStream resourceStream = zipFile.getInputStream (entry);
    DataInputStream ds = new DataInputStream (resourceStream);

    // the number of bytes available
    int len = (int) entry.getSize ();

    // read the resource data
    byte [] resourceData = new byte [len];
    ds.readFully (resourceData);
    ds.close ();

    return resourceData;
  }

  /**
   * Store a resource in the output archive.
   *
   * @param resourceName The resource name.
   * @param resourceData The resource data.
   * @exception IOException if an IO error occurred.
   */
  protected void storeResource (String resourceName, byte [] resourceData)
    throws IOException
  {
    // sanity check that resource not stored more than once
    if (isResourceStored (resourceName))
      throw new Error ("Resource '" + resourceName + "' is already stored");

    resources.add (resourceName);

    ZipEntry entry = new ZipEntry (resourceName);
    entry.setSize (resourceData.length);

    // write the resource entry and data
    archiveStream.putNextEntry (entry);
    archiveStream.write (resourceData, 0, resourceData.length);
    archiveStream.closeEntry ();

    // do manifest entry
    if (useJarFormat)
    {
      if (manifest.getAttributes (resourceName) == null)
      {
        Attributes attrs = new Attributes ();
        manifest.getEntries ().put (resourceName, attrs);
      }
    }
  }

  /**
   * Find an already opened ZIP archive (cached in openZips) or open
   * it.
   *
   * @param zipFileName The file name of the ZIP archive.
   * @return The ZIP archive opened for reading.
   *
   * @exception IOException if an IO error occurred.
   * @see #closeOpenZips
   */
  protected ZipFile findOrOpenZip (String zipFileName)
    throws IOException
  {
    ZipFile zipFile = (ZipFile)openZips.get (zipFileName);

    if (zipFile == null)
    {
      zipFile = new ZipFile (zipFileName);

      openZips.put (zipFileName, zipFile);
    }

    return zipFile;
  }

  /**
   * Close any ZIP archives opened via findOrOpenZip().
   *
   * @see #findOrOpenZip
   */
  protected void closeOpenZips ()
  {
    for (Iterator i = openZips.values ().iterator (); i.hasNext (); )
    {
      ZipFile zipFile = (ZipFile)i.next ();

      try
      {
        zipFile.close ();
      } catch (IOException ex)
      {
        error ("Error while closing ZIP file '" +
                zipFile.getName () + "': " + ex);
      }
    }

    openZips.clear ();
  }

  /**
   * Log an error.
   */
  protected static void error (String message)
  {
    System.err.println ("JarX: Error: " + message);
  }

  /**
   * Log a warning.
   */
  protected static void warn (String message)
  {
    System.err.println ("JarX: Warning: " + message);
  }

  /**
   * Log a message.
   */
  protected static void log (String message)
  {
    System.err.println ("JarX: " + message);
  }

  /**
   * Return a list of resources referenced by a class.  If
   * autoFindResources is true, will also scan for resources
   * referenced via calls to Class.getResource().
   *
   * <p>The logic in this method is shamelessly copied from the
   * original in ZipLock: see class doc for author acknowledgements
   * and contact info.
   *
   * @param className The class resource name.
   * @param classData The class resource data.
   * @return An array of resources used by the class.
   * @exception IOException if an IO error occurs.
   */
  protected String [] findClassResources (String className,
                                           byte [] classData)
    throws IOException
  {
    // Create a DataInputStream using the buffer. This will make
    // reading the buffer very easy

    ByteArrayInputStream bais =
      new ByteArrayInputStream (classData);

    DataInputStream in = new DataInputStream (bais);

    // Read the magic number. It should be 0xCAFEBABE

    int magic = in.readInt();
    if (magic != 0xCAFEBABE)
      throw new IOException ("Invalid magic number in " + className);

    // Validate the version numbers

    in.readShort(); // minor
    short major = in.readShort();

    if (major < 45)
    {
      // The VM specification defines 3 as the minor version and 45 as
      // the major version for 1.1
      throw new IOException ("Invalid version number in " + className + " major == " + major);
    }

    // Get the number of items in the constant pool

    short count = in.readShort();

    // Track which CP entries are classes and String contants
    classInfo.removeAll();
    stringTbl.removeAll();

    // Keep a list of method references
    methInfo.removeAllElements();

    // Initialize the constant pool handle table
    HandleTable cp = new HandleTable(count);    // Constant Pool

    // Now walk through the constant pool looking for entries we are
    // interested in.  Others can be ignored, but we need to
    // understand the format so they can be skipped.
    readcp:
    for (int i = 1; i < count; i++)
    {
      // Read the tag
      byte tag = in.readByte();
      switch (tag) {
      case 7:  // CONSTANT_Class
        // Save the constant pool index for the class name
        short nameIndex = in.readShort();
        classInfo.add(nameIndex);
        cp.put(i,nameIndex);
        break;
      case 10: // CONSTANT_Methodref
        short clazz = in.readShort();   // class
        short nt = in.readShort();      // name and type
        methInfo.addElement(new Pair(clazz,nt));
        break;
      case 9:  // CONSTANT_Fieldref
      case 11: // CONSTANT_InterfaceMethodref
        // Skip past the structure
        in.skipBytes(4);
        break;
      case 8:  // CONSTANT_String
        // Skip past the string index
        short strIndex = in.readShort();
        stringTbl.add(strIndex);
        break;
      case 3:  // CONSTANT_Integer
      case 4:  // CONSTANT_Float
        // Skip past the data
        in.skipBytes(4);
        break;
      case 5:  // CONSTANT_Long
      case 6:  // CONSTANT_Double
        // Skip past the data
        in.skipBytes(8);

        // As dictated by the Java Virtual Machine specification,
        // CONSTANT_Long and CONSTANT_Double consume two constant pool
        // entries.
        i++;

        break;
      case 12: // CONSTANT_NameAndType
        int name = in.readShort();
        int sig = in.readShort();
        cp.put(i,name,sig);
        break;
      case 1:  // CONSTANT_Utf8
        String s = in.readUTF();
        cp.put(i, s);
        break;
      default:
        warn ("Unknown constant tag (" + tag + "@" + i + " of " + count +
              ") in " + className);
        break readcp;
      }
    }

    // We're done with the buffer and input streams

    in.close();

    Vector v = new Vector();    // collect resources used by this class

    // Walk through our vector of class name index values and get the
    // actual class names

    // Copy the actual class names so tables can get reused
    int [] ia = classInfo.elements ();

    for (int i = 0; i < ia.length; i++)
    {
      int idx = ia[i];
      String s = cp.getString(idx);
      if (s == null) continue;

      // Look for arrays. Only process arrays of objects
      if (s.startsWith("["))
      {
        // Strip off all of the array indicators
        while (s.startsWith("["))
          s = s.substring(1);

        // Only use the array if it is an object. If it is,
        // the next character will be an 'L'
        if (!s.startsWith("L"))
          continue;

        // Strip off the leading 'L' and trailing ';'
        s = s.substring(1, s.length() - 1);
      }
      v.addElement(s + ".class");
    }

    if (autoFindResources)
    {
      // examine methods used for calls to getResource*()
      boolean resourceUsed = false;
      Pair [] p = new Pair[methInfo.size()];
      methInfo.copyInto(p);

      for (int i = 0; i < p.length; ++i)
      {
        try
        {
          String clazz = cp.getString (cp.getInt(p[i].a));

          if ("java/lang/Class".equals(clazz))
          {
            Pair nt = cp.getPair(p[i].b);
            String name = cp.getString(nt.a);

            if (name.startsWith("getResource"))
            {
              resourceUsed = true;
              break;
            }
          }
        } catch (IndexOutOfBoundsException x)
        {
          // dodgy, but I copied this code and can't see if we need it or not
        }
      }

      if (resourceUsed)
      {
        /* string constants might be resource file names Those that
           aren't will get ignored when the resulting path is not
           found. */
        int pos = className.lastIndexOf('/');
        String res = className.substring(0,pos+1);
        ia = stringTbl.elements();

        for (int i = 0; i < ia.length; ++i)
        {
          String s = res + cp.getString(ia[i]);
          v.addElement(s);
        }
      }
    }

    String[] a = new String[v.size()];
    v.copyInto(a);

    return a;
  }

  /**
   * Test if a given class name is a 'system' class, ie a class
   * provided by the JRE or a core extension.
   */
  protected static boolean isSystemClass (String className)
  {
    className = className.replace ('/', '.');

    for (int i = 0; i < SYSTEM_PACKAGES.length; i++)
    {
      if (className.startsWith (SYSTEM_PACKAGES [i]))
        return true;
    }

    return false;
  }

  /**
   * Used by findClassResources ().
   */
  static final class IntList
  {
    private int[] a = new int[8];
    private int size = 0;

    public final int size ()
    {
      return size;
    }

    public void add (int i)
    {
      if (size >= a.length)
      {
        int[] na = new int[a.length * 2];
        System.arraycopy(a,0,na,0,size);
        a = na;
      }

      a[size++] = i;
    }

    public void removeAll ()
    {
      size = 0;
    }

    public int [] elements ()
    {
      int[] na = new int[size];

      System.arraycopy (a,0,na,0,size);

      return na;
    }
  }

  /**
   * Used by findClassResources ().
   */
  private static final class Pair
  {
    final int a, b;

    public Pair (int a,int b)
    {
      this.a = a;
      this.b = b;
    }
  }

  /**
   * Used by findClassResources ().
   */
  private static final class HandleTable
  {
    private Object[] tbl;

    public HandleTable(int n)
    {
      tbl = new Object[n];
    }

    public void put(int i,Object name)
    {
      tbl[i] = name;
    }

    public void put(int i,int val)
    {
      put(i,new Integer(val));
    }

    public void put (int i,int a,int b)
    {
      put(i,new Pair(a,b));
    }

    public String getString(int i)
    {
      if (i >= tbl.length)
        return null;
      else
        return (String)tbl[i];
    }

    public int getInt(int i)
    {
      return ((Integer)tbl[i]).intValue();
    }

    public Pair getPair(int i)
    {
      return (Pair)tbl[i];
    }
  }
}
