package dsto.dfc.tools;

import java.io.IOException;
import java.io.Reader;

import org.apache.tools.ant.filters.BaseParamFilterReader;
import org.apache.tools.ant.types.Parameter;
import org.apache.tools.ant.types.Parameterizable;

/**
 * An Ant file filter that prefixes a set piece of text (eg copyright header)
 * to file. Use the "prefix" paramter to set the text to be appended. 
 * 
 * @author mpp
 */
public class PrefixFilter
  extends BaseParamFilterReader
  implements Parameterizable
{
  private int cursor;
  private String prefix;

  public PrefixFilter (Reader in)
  {
    super (in);
    
    prefix = "";
    cursor = 0;
  }

  public int read () throws IOException
  {
    if (!getInitialized ())
    {
      Parameter [] parameters = getParameters ();
      for (int i = 0; i < parameters.length; i++)
      {
        if (parameters [i].getName().equals ("prefix"))
          prefix = parameters [i].getValue ();
      }
      
      setInitialized (true);
    }
    
    if (cursor >= prefix.length ())
      return super.read ();
    else
      return prefix.charAt (cursor++);
  }
}
