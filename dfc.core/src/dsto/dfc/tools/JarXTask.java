/*
 * JarXTask - Ant task wrapper for JarX JAR creator
 *
 * Copyright (c) 2000 Matthew Phillips, DSTO.
 * <matthew.phillips@dsto.defence.gov.au>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License (http://www.gnu.org/copyleft/gpl.html)
 * for full details.
 *
 * $Id$
 */
package dsto.dfc.tools;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.Path;
import org.apache.tools.ant.types.Reference;

/**
 * An Ant build task wrapper for JarX.
 *
 * <p>Attributes:
 * <pre>
 *    archiveFile:    The archive file (required).
 *    manifest:       The manifest file (optional, default = none).
 *    mainClass:      The main application class (optional, default = none).
 *    useJarFormat:   True to generate a JAR (optional, default = true).
 *    classpath:      The classpath to search for resources (optional,
 *                    default = system classpath).  May also be specified
 *                    as a nested &lt;classpath&gt; element.
 *    includes:       Comma separated list of included resources (required,
 *                    may include '*' as wildcard).  May also use nested
 *                    include &lt;include name="..."/&gt; form.
 *    includeRegexps: Comma separated list of regular expression resource
 *                    include patterns (optional).  May also use nested
 *                    include &lt;include regexp="..." /&gt; form.
 *    excludes:       Comma separated list of excluded resources (optional,
 *                    may include '*' as wildcard).  May also use nested
 *                    exclude &lt;exlude name="..." /&gt; form.
 *    excludeRegexps: Comma separated list of regular expression resource
 *                    exclude patterns (optional). May also use nested
 *                    exclude &lt;exclude regexp="..." /&gt; form.
 * </pre>
 */
public class JarXTask extends Task
{
  private File archiveFile;
  private File manifestFile = null;
  private String mainClass = null;
  private boolean useJarFormat = true;
  private ArrayList includes = new ArrayList ();
  private ArrayList includeRegexps = new ArrayList ();
  private ArrayList excludes = new ArrayList ();
  private ArrayList excludeRegexps = new ArrayList ();
  private Path classPath = null;

  public JarXTask ()
  {
    setTaskName ("jarx");
  }

  public void setArchive (File newFile)
  {
    archiveFile = newFile;
  }

  public void setManifest (File newFile)
  {
    manifestFile = newFile;
  }

  public void setMainClass (String mainClass)
  {
    this.mainClass = mainClass;
  }

  public void setUseJarFormat (boolean useJarFormat)
  {
    this.useJarFormat = useJarFormat;
  }

  public void setIncludes (String newIncludes)
  {
    String includesArray [] = split (newIncludes, ",");

    includes.addAll (Arrays.asList (includesArray));
  }

  public PatternArgument createInclude ()
  {
    return new PatternArgument (includes, includeRegexps);
  }

  public void setIncludeRegexps (String newRegexps)
  {
    String regexpsArray [] = split (newRegexps, ",");

    includeRegexps.addAll (Arrays.asList (regexpsArray));
  }

  public void setExcludes (String newExcludes)
  {
    String excludesArray [] = split (newExcludes, ",");

    excludes.addAll (Arrays.asList (excludesArray));
  }

  public PatternArgument createExclude ()
  {
    return new PatternArgument (excludes, excludeRegexps);
  }

  public void setExcludeRegexps (String newRegexps)
  {
    String regexpsArray [] = split (newRegexps, ",");

    excludeRegexps.addAll (Arrays.asList (regexpsArray));
  }

  public void setClasspath (Path newPath)
  {
    if (classPath == null)
    {
      classPath = newPath;
    } else
    {
      classPath.append (newPath);
    }
  }

  public Path getClasspath ()
  {
    return classPath;
  }

  public void setClasspathRef (Reference r)
  {
    createClasspath ().setRefid (r);
  }

  public Path createClasspath ()
  {
    if (classPath == null)
    {
      classPath = new Path (project);
    }

    return classPath.createPath ();
  }

  public void execute () throws BuildException
  {
    if (archiveFile == null)
      throw new BuildException ("No archive specified");

    JarX jarx = new JarX (archiveFile, useJarFormat);

    if (manifestFile != null)
      jarx.setManifestFile (manifestFile);

    if (mainClass != null)
      jarx.setMainClassName (mainClass);

    if (classPath == null)
      classPath = Path.systemClasspath;

    // class path
    jarx.setClassPath (classPath.list ());

    // includes
    jarx.addRoots (includes);
    jarx.addRootRegexps (includeRegexps);

    // excludes
    jarx.addExcludes (excludes);
    jarx.addExcludeRegexps (excludeRegexps);

    try
    {
      log ("Creating " + archiveFile);
      jarx.run ();
    } catch (Exception ex)
    {
      throw new BuildException ("JarX encountered an error: " + ex, ex);
    }
  }

  public static String [] split (String str, String delimeter)
  {
    ArrayList elements = new ArrayList ();
    int start = 0;
    int end = -1;

    while (start < str.length ())
    {
      end = str.indexOf (delimeter, start);

      if (end == -1)
        end = str.length ();

      elements.add (str.substring (start, end));

      start = end + 1;
    }

    String [] elementArray = new String [elements.size ()];
    elements.toArray (elementArray);

    return elementArray;
  }

  public final class PatternArgument
  {
    private List namesList, regexpsList;

    public PatternArgument (List namesList, List regexpsList)
    {
      this.namesList = namesList;
      this.regexpsList = regexpsList;
    }

    public void setName (String name)
    {
      namesList.add (name);
    }

    public void setRegexp (String regexp)
    {
      regexpsList.add (regexp);
    }
  }
}
