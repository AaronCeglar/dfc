package dsto.dfc.reflection;

/**
 * A delegate pointer to a method. Similar to a java.lang.Method, but
 * allows pluggable instances and composition.
 * 
 * @author Matthew Phillips
 */
public interface IDelegate
{
  public static final Object [] EMPTY_ARGS = new Object [0];
  
  /**
   * Invoke the delegate with the given argument list.
   * 
   * @param args The arguments.
   * 
   * @throws InvokeException if a checked exception occurs.
   */
  public void invoke (Object... args)
    throws InvokeException;
}
