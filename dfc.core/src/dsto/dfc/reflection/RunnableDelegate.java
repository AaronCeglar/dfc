package dsto.dfc.reflection;

import static dsto.dfc.reflection.IDelegate.EMPTY_ARGS;

/**
 * Adapts a delegate to the Runnable interface.
 * 
 * @author Matthew Phillips
 */
public class RunnableDelegate implements Runnable
{
  private IDelegate delegate;
  private Object [] args;

  public RunnableDelegate (IDelegate delegate)
  {
    this (delegate, EMPTY_ARGS);
  }

  public RunnableDelegate (IDelegate delegate, Object... args)
  {
    this.delegate = delegate;
    this.args = args;
  }

  public void run ()
  {
    delegate.invoke (args);
  }
}
