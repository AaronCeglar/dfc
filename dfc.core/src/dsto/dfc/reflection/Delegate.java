package dsto.dfc.reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import dsto.dfc.util.Objects;

/**
 * A delegate to a method in a class instance. Supports
 * semi-lambda-like ability to pre-specify some arguments at creation
 * for appending to the set called with invoke().
 * 
 * @author Matthew Phillips
 */
public class Delegate implements IDelegate, Runnable
{
  private static final Class [] EMPTY_ARG_TYPES = new Class [0];
  
  private Method method;
  private Object [] baseArgs;
  private Object target;

  public Delegate (Object target, String methodName)
    throws IllegalArgumentException
  {
    this (target, methodName, EMPTY_ARG_TYPES, EMPTY_ARGS);
  }
  
  public Delegate (Object target, String methodName,
                   Object... args)
    throws IllegalArgumentException
  {
    this (target, methodName, argTypes (args), args);
  }
  
  /**
   * Create a delegate by giving a name and an argument type list.
   * 
   * @param target The target object.
   * @param methodName The name of the method to call.
   * @param argTypes The argument types of the method.
   * 
   * @throws IllegalArgumentException
   */
  public Delegate (Object target, String methodName,
                   Class... argTypes)
    throws IllegalArgumentException
  {
    this (target, methodName, argTypes, EMPTY_ARGS);
  }
  
  /**
   * Create an instance, fully specifying method and argument list.
   * 
   * @param target The target object.
   * @param methodName The name of the method to call.
   * @param argTypes The argument types of the method.
   * @param args Any arguments to pre-specify.
   * 
   * @throws IllegalArgumentException
   */
  public Delegate (Object target, String methodName,
                   Class [] argTypes, Object [] args)
    throws IllegalArgumentException
  {
    this.baseArgs = args == null ? EMPTY_ARGS : args;
    this.target = target;
    this.method = Objects.findMethod (target.getClass (), methodName, argTypes);
    
    if (method == null)
    {
      throw new IllegalArgumentException
        ("Target does not have a method named " + methodName +
         " with " + argTypes.length + " arguments");
    }
    
    method.setAccessible (true);
  }

  @Override
  public String toString ()
  {
    return "Delegate [" + target.getClass ().getName () + "." +  
                          method.getName () + "]";
  }
  
  public Object target ()
  {
    return target;
  }
  
  public void run ()
  {
    invoke (EMPTY_ARGS);
  }
  
  /**
   * Call the method pointed to by the delegate with the default
   * parameter set.
   */
  public void invoke (Object... args)
    throws InvokeException
  {
    if (args == null)
      throw new NullPointerException ("Args cannot be null");
    
    try
    {
      method.invoke (target, concatArgs (baseArgs, args));
    } catch (Throwable ex)
    {
      // unwrap a InvocationTargetException(s)
      while (ex instanceof InvocationTargetException)
        ex = ((InvocationTargetException)ex).getTargetException ();

      if (ex instanceof InvokeException)
      {
        // don't wrap a wrapped exception
        throw (InvokeException)ex;
      } else
      {
      
        throw new InvokeException ("Error in delegate method", ex);
      }
    }
  }

  private static Object [] concatArgs (Object [] args1, Object [] args2)
  {
    if (args1.length == 0)
    {
      return args2;
    } else if (args2.length == 0)
    {
      return args1;
    } else
    {
      Object [] newArgs = new Object [args1.length + args2.length];
      
      System.arraycopy (args1, 0, newArgs, 0, args1.length);
      System.arraycopy (args2, 0, newArgs, args1.length, args2.length);
      
      return newArgs;
    }
  }

  private static Class [] argTypes (Object [] args)
  {
    if (args == null)
      return EMPTY_ARG_TYPES;
    
    Class [] argTypes = new Class [args.length];
    
    for (int i = 0; i < args.length; i++)
      argTypes [i] = args [i].getClass ();
    
    return argTypes;
  }
}