package dsto.dfc.reflection;

/**
 * Exception that wraps checked exceptions inside a delegate
 * invocation.
 * 
 * @author Matthew Phillips
 */
public class InvokeException extends RuntimeException
{
  public InvokeException (String message)
  {
    super (message);
  }

  public InvokeException (Throwable cause)
  {
    super (cause);
  }

  public InvokeException (String message, Throwable cause)
  {
    super (message, cause);
  }
}
