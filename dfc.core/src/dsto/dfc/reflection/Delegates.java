package dsto.dfc.reflection;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.emptyList;

import static dsto.dfc.reflection.IDelegate.EMPTY_ARGS;

/**
 * A set of delegates for notification.
 * 
 * @author Matthew Phillips
 */
public class Delegates
{
  private List<IDelegate> delegates;

  public Delegates ()
  {
    this.delegates = emptyList ();
  }
  
  public void add (IDelegate delegate)
  {
    ArrayList<IDelegate> newDelegates = new ArrayList<IDelegate> (delegates);
    
    newDelegates.add (delegate);
    
    delegates = newDelegates;
  }
  
  public void remove (IDelegate delegate)
  {
    ArrayList<IDelegate> newDelegates = new ArrayList<IDelegate> (delegates);
    
    newDelegates.remove (delegate);
    
    delegates = newDelegates;
  }

  /**
   * Invoke all delegates with an empty argument list.
   */
  public void fire ()
    throws InvokeException
  {
    fire (EMPTY_ARGS);
  }
  
  /**
   * Invoke all delegates with the given argument list.
   */
  public void fire (Object... args)
    throws InvokeException
  {
    if (delegates.size () > 0)
    {
      for (IDelegate delegate : delegates)
        delegate.invoke (args);
    }
  }
}
