package dsto.dfc.test;

import static dsto.dfc.logging.Log.warn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Runs a server on a socket that accepts messages and responds appropriately
 * with the use of an {@link AbstractProtocol} instance.
 *
 * @author WeberD
 * @created 2/12/2004
 * @version $Revision$
 */
public class ProtocolServer extends Thread
{
  protected int port;
  protected boolean running;
  protected Object lock = new Object ();
  protected AbstractProtocol protocol;
  protected List spawnedThreads = new ArrayList ();
  protected ServerSocket serverSocket;
  protected boolean shuttingDown;
  protected boolean autoNewline = true;

  public ProtocolServer (int aPort, AbstractProtocol aProtocol)
  {
    this.port = aPort;
    this.protocol = aProtocol;
    this.setDaemon (true);
  }

  public void setAutoNewline (boolean value)
  {
    this.autoNewline = value;
  }

  public void startServer ()
  {
    synchronized (lock)
    {
      running = true;
    }
    super.start ();
  }

  public void stopServer () throws IOException
  {
    synchronized (lock)
    {
      running = false;
    }
    for (Iterator it = spawnedThreads.iterator (); it.hasNext ();)
    {
      try
      {
        ((Thread) it.next ()).join ();
      }
      catch (InterruptedException e)
      {
        e.printStackTrace ();
      }
    }
    if (serverSocket != null)
    {
      try
      {
        shuttingDown = true;
        serverSocket.close ();
      }
      catch (SocketException e)
      {
        System.err.println ("Ignoring expected SocketException: "
            + e.getMessage ());
        /*
         * serverSocket.close() throws a SocketException if it's blocked on a
         * call to accept() - IGNORE
         */
      }
      catch (IOException e)
      {
        // e.printStackTrace (System.err);
        throw e;
      }
    }
  }

  private boolean stillRunning ()
  {
    synchronized (lock)
    {
      return running;
    }
  }

  @Override
  public void run ()
  {
    try
    {
      serverSocket = new ServerSocket (port);
      while (stillRunning ())
      {
        try
        {
          ServerThread spawnedThread = new ServerThread (serverSocket.accept ());
          spawnedThreads.add (spawnedThread);
          spawnedThread.start ();
        }
        catch (SocketException e1)
        {
          if (!shuttingDown)
            warn ("While waiting for new connections", ProtocolServer.this, e1);
          /*
           * We expect a SocketException to be thrown if the serverSocket is
           * closed while it's still listening on the port (accept()). You can't
           * stop it listening without killing it, which throws this exception.
           * The sentinel (shuttingDown) See the java.net.ServerSocket
           * documentation.
           */
        }
        cleanUpThreads (spawnedThreads);
      }

      serverSocket.close ();
      serverSocket = null;
    }
    catch (IOException e)
    {
      System.err.println ("CGateEventServer could not listen on port: " + port);
      e.printStackTrace ();
    }
  }

  /**
   * Removes dead threads from <code>threads</code>.
   *
   * @param threads The list to remove dead threads from.
   */
  private void cleanUpThreads (List threads)
  {
    List toRemove = new ArrayList ();
    for (Iterator it = threads.iterator (); it.hasNext ();)
    {
      Thread thread = (Thread) it.next ();
      if (!thread.isAlive ())
        toRemove.add (thread);
    }

    threads.removeAll (toRemove);
  }

  class ServerThread extends Thread
  {
    private Socket client;

    public ServerThread (Socket clientSocket)
    {
      super ("ProtocolServer.ServerThread");
      this.client = clientSocket;
    }

    @Override
    public void run ()
    {
      try
      {
        PrintWriter out = new PrintWriter (client.getOutputStream (), true);
        BufferedReader in =
          new BufferedReader (new InputStreamReader (client.getInputStream ()));

        String inputLine = null;

        String welcomeMessage = protocol.getWelcomeMessage ();
        if (welcomeMessage == null)
          welcomeMessage = protocol.process (inputLine); // get a welcome message

        write (out, welcomeMessage);

        while ((inputLine = in.readLine ()) != null)
        {
          write (out, protocol.process (inputLine));
          if (protocol.isSigningOff ())
            break;
        }

        in.close ();
        out.close ();
        client.close ();
      }
      catch (IOException e)
      {
        System.err.println ("This may just be the server being shut down"
            + " - check at:");
        StackTraceElement[] stackTrace = e.getStackTrace ();
        System.err.println (stackTrace[stackTrace.length - 1]);
        // e.printStackTrace ();
      }
    }

    private void write (PrintWriter out, String message)
    {
      if (autoNewline)
        out.println (message);
      else
        out.print (message);
    }
  }
}