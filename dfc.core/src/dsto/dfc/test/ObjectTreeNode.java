package dsto.dfc.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Used by Stringifier to represent a tree of objects.
 * 
 * @version $Revision$
 */
final class ObjectTreeNode
{
  public ObjectTreeNode parent;
  public ObjectTreeNode alias;
  public String name;
  public String text;
  public int id;
  public List children;

  public ObjectTreeNode (String text)
  {
    this (null, text, -1);
  }
  
  public ObjectTreeNode (ObjectTreeNode node)
  {
    this (null, null, -1);
    
    this.alias = node;
  }

  public ObjectTreeNode (String name, String text, int id)
  {
    this.parent = null;
    this.name = name;
    this.text = text;
    this.id = id;
    this.children = Collections.EMPTY_LIST;
  }

  public void addChild (ObjectTreeNode child)
  {
    if (children == Collections.EMPTY_LIST)
      children = new ArrayList ();
      
    children.add (child);
    
    child.parent = this;
  }

  public boolean isAlias ()
  {
    return alias != null;
  }
}
