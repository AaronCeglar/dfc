package dsto.dfc.test;

import java.awt.event.WindowEvent;

import javax.swing.JFrame;

/**
 * Base class for test frames.
 *
 * @version $Revision$
 */
public class TestFrame extends JFrame
{
  public TestFrame()
  {
    try 
    {
      jbInit();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception
  {
    this.setTitle("Test");
    this.setSize (600, 400);
    this.addWindowListener(new java.awt.event.WindowAdapter()
    {
      public void windowClosing(WindowEvent e)
      {
        this_windowClosing(e);
      }
    });
  }

  protected void this_windowClosing (WindowEvent e)
  {
    System.exit (0);
  }
}
 
