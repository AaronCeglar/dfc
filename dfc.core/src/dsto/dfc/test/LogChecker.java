package dsto.dfc.test;

import java.util.ArrayList;
import java.util.Iterator;

import junit.framework.AssertionFailedError;

import dsto.dfc.logging.Log;
import dsto.dfc.logging.LogEvent;
import dsto.dfc.logging.LogListener;

/**
 * JUnit utility class that listens to DFC log events and logs any
 * errors/warnings and can be used to fail a test if any are logged.
 * <p>
 * 
 * Example:
 * 
 * <pre>
 *      protected void setUp ()
 *      {
 *        checker = new LogChecker ();
 *        ...
 *      }
 *    
 *      public void tearDown ()
 *      {
 *        checker.assertOk ();
 *        ...
 *      }
 * </pre>
 * 
 * @author Matthew Phillips
 */
public class LogChecker implements LogListener
{
  private ArrayList logErrors;
  
  public LogChecker ()
  {
    logErrors = new ArrayList ();
    
    Log.addLogListener (this);
  }
  
  public synchronized void messageReceived (LogEvent e)
  {
    int type = e.getType ();
    
    if (type == Log.ALARM || type == Log.WARNING || type == Log.INTERNAL_ERROR)
      logErrors.add (e);
  }
  
  /**
   * If any log errors/warnings have been detected since construction,
   * throw an AssertionFailedError exception. This also disposes of
   * the checker, and should only be called once.
   */
  public synchronized void assertOk ()
    throws AssertionFailedError
  {
    Log.removeLogListener (this);
    
    if (!logErrors.isEmpty ())
    {
      StringBuilder errorText = new StringBuilder ();
      
      for (Iterator i = logErrors.iterator (); i.hasNext ();)
      {
        LogEvent message = (LogEvent)i.next ();
        
        if (errorText.length () > 0)
          errorText.append ("\n");
          
        errorText.append (Log.toLogString (message));        
      }
      
      throw new AssertionFailedError ("Log errors/warnings detected: " + errorText);
    }
  }
}
