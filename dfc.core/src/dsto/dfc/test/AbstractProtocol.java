package dsto.dfc.test;

import dsto.dfc.databeans.DataBean;

/**
 * A protocol framework to be used with {@link ProtocolServer}.
 *
 * @author WeberD
 * @created 3/12/2004
 * @version $Revision$
 */
public abstract class AbstractProtocol extends DataBean
{
  private String response;

  /**
   * Returns true if the last response of this protocol is to sign off from the
   * exchange.
   *
   * @return A boolean representing the state of the message exchange.
   */
  public boolean isSigningOff ()
  {
    return getSignOffMessage ().equals (getResponse ());
  }

  /**
   * Gets the sign-off message of this protocol.
   *
   * @return The sign-off message of this protocol as a String.
   */
  public abstract String getSignOffMessage ();

  /**
   * Template method to ensure {@link #getResponse()} is set.
   *
   * @param input The line of input received by the protocol object.
   * @return The response appropriate to the 'input'.
   */
  public String process (String input)
  {
    setResponse (processInput (input));
    return getResponse ();
  }

  /**
   * This method is called for the protocol to respond to a given line of input,
   * <code>inputLine</code>. This method is called once with a null parameter to
   * allow a welcome message to be displayed.
   *
   * @param inputLine The line of input to respond to.
   * @return The protocol's response to <code>inputLine</code>.
   */
  public abstract String processInput (String inputLine);

  /**
   * Sets the message for the protocol to respond with.
   *
   * @param message The message to set as the protocol's response.
   */
  protected void setResponse (String message)
  {
    this.response = message;
  }

  /**
   * Gets the most recent response of the protocol.
   *
   * @return Returns the last message set for the protocol to respond with.
   */
  protected String getResponse ()
  {
    return response;
  }

  /**
   * Should be overridden with a method return an empty String if not used. If
   * it's not used, then {@link #processInput(String)} should be prepared to
   * accept a <code>null</code> parameter value.
   *
   * @return The welcome message of the protocol, e.g. asking for a username.
   */
  public String getWelcomeMessage ()
  {
    return null;
  }
}
