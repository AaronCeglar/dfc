package dsto.dfc.test;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Dummy test property change listener for unit tests. Used to see if a 
 * particular property change is heard from a source.<p>
 * 
 * Example use:
 * <pre>
 *   MyJavaBean            bean     = new MyJavaBean ();
 *   DummyPropertyListener listener = new DummyPropertyListener ();
 *   bean.addPropertyChangeListener (listener);
 *   
 *   bean.setProperty ("name", newValue);
 *   
 *   assertTrue (listener.heardIt ("name"));
 *   listener.reset ();
 * </pre>
 * 
 * @author  weberd
 * @created 20/03/2003
 * @version $Revision$
 */
public class DummyPropertyListener implements PropertyChangeListener
{
  public static final String EMPTY_STRING = "";
  
  private String  propertyName = EMPTY_STRING; // ensures lack of NPE
  private boolean heardIt;
  private PropertyChangeEvent event;
  
  /**
   * Replies true if a property change event of the <i>name</i> property was
   * heard.
   * 
   * @param name The name of the property we want to know was fired.
   * @return True if the appropriate event was heard and false if either no
   *         event was heard or if the event heard was of a different property.
   */
  public boolean heardIt (String name)
  {
    return heardIt && propertyName.equals (name);
  }

  /**
   * Returns either an empty string or the name of the last property the change
   * event of which was heard. <p>
   * 
   * <b>NB</b> If {@link #reset()} has been called, the value returned will be
   * an empty string.
   */
  public String getPropertyName ()
  {
    return propertyName;
  }
  
  /**
   * Returns the last event heard by the listener. If no event has been heard
   * or the listener has been reset then null is returned.
   * 
   * @return The last event heard or null, if no event was heard or reset was
   *         called.
   */
  public PropertyChangeEvent getEvent ()
  {
    return event;
  }
  
  /**
   * Resets the listener so it is ready to hear new events.
   */
  public void reset ()
  {
    heardIt      = false;
    propertyName = EMPTY_STRING;
    event        = null;
  }

  /**
   * Invoked when a bean being listened to fires a property change event.
   */
  public void propertyChange (PropertyChangeEvent evt)
  {
    heardIt      = true;
    propertyName = evt.getPropertyName ();
    event        = evt;
  }
}
