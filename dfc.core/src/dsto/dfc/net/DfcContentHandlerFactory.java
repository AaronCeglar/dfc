package dsto.dfc.net;

import java.net.ContentHandler;
import java.net.ContentHandlerFactory;
import java.net.URLConnection;
import java.util.HashMap;

/**
 * A generic URL ContentHandlerFactory that allows handlers to
 * dynamically associated with their MIME type.
 *
 * <p>NOTE: Use {@link DfcFileNameMap} to map URL's by their file
 * extensions to a given MIME type.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class DfcContentHandlerFactory implements ContentHandlerFactory
{
  private static HashMap handlers = new HashMap ();
  private static boolean registered = false;

  public DfcContentHandlerFactory ()
  {
    // zip
  }

  /**
   * Register this class as the system-wide content handler factory.
   *
   * @exception Error if something else is already registered as the
   * system content handler factory.
   */
  public static void registerFactory () throws Error
  {
    if (!registered)
    {
      URLConnection.setContentHandlerFactory (new DfcContentHandlerFactory ());
      registered = true;
    }
  }

  /**
   * Register a handler with a MIME type.
   *
   * @param mimeType The MIME type.
   * @param handlerPkg The base package of the handler class, minus
   * the major.minor part.  Eg if handler is dfc.handlers.text.plain,
   * handlerPkg should be dfc.handlers.  This package is added to the
   * system-wide handlers property.
   * @param handler The handler for the given MIME type.
   */
  public static void registerHandler (String mimeType,
                                      String handlerPkg,
                                      ContentHandler handler)
  {
    registerFactory ();

    String handlersProperty =
      System.getProperty ("java.content.handler.pkgs");

    handlers.put (mimeType, handler);

    if (handlersProperty == null)
    {
      System.setProperty ("java.content.handler.pkgs", handlerPkg);
    } else if (handlersProperty.indexOf (handlerPkg) == -1)
    {
      System.setProperty ("java.content.handler.pkgs",
                          handlersProperty + "|" + handlerPkg);
    }
  }

  public ContentHandler createContentHandler (String mimeType)
  {
    return (ContentHandler)handlers.get (mimeType);
  }
}
