package dsto.dfc.net;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;

import java.io.IOException;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;

import static java.net.NetworkInterface.getNetworkInterfaces;
import static java.util.Collections.sort;

/**
 * General host info utilities.
 *
 * @author Matthew Phillips
 */
public final class Localhost
{
  private Localhost ()
  {
    // zip
  }

  /**
   * Short cut to get the short version of the local host name (ie without the
   * domain name).
   *
   * @return The host's name.
   *
   * @throws IOException if an error occurs accessing network adapter info.
   * @throws NoHostNameException if no host name can be found.
   *
   * @see #getNetworkAddress()
   */
  public static String getName () throws IOException, NoHostNameException
  {
    String fullName = getNetworkAddress ().getHostName ();

    checkIsHostName (fullName);

    int dotIndex = fullName.indexOf ('.');

    if (dotIndex == -1)
      return fullName;
    else
      return fullName.substring (0, dotIndex);
  }

  /**
   * Short cut to get the local host's default domain name.
   *
   * @return The host's domain.
   *
   * @throws IOException if an error occurs accessing network adapter info.
   * @throws NoHostNameException if no domain can be found.
   *
   * @see #getNetworkAddress()
   */
  public static String getDomain () throws IOException, NoHostNameException
  {
    String fullName = getNetworkAddress ().getHostName ();

    checkIsHostName (fullName);

    int dotIndex = fullName.indexOf ('.');

    if (dotIndex != -1)
      return fullName.substring (dotIndex + 1);
    else
      throw new NoHostNameException ("No domain for host " + fullName);
  }

  /**
   * Throw exception if hostname looks like an IP rather than a name.
   *
   * @param hostname
   * @throws NoHostNameException is the hostname is passed in a raw IP address
   */
  private static void checkIsHostName (String hostname)
    throws NoHostNameException
  {
    if (isRawIP (hostname))
      throw new NoHostNameException ("Cannot get host name for IP " + hostname);
  }

  /**
   * Checks if the string looks like a raw IPv4 or 6 IP address
   *
   * @param addr
   * @return true if the address is IPv4 or 6 form
   */
  private static boolean isRawIP (String addr)
  {
    boolean isIPv4 = isIPv4 (addr);
    boolean isIPv6 = isIPv6 (addr);
    return (isIPv4 || isIPv6);
  }

  static boolean isIPv6 (String addr)
  {
    return addr.matches ("(\\w{1,4}:){7}\\w{1,4}");
  }

  static boolean isIPv4 (String addr)
  {
    return addr.matches ("(\\d{1,3}.){3}\\d{1,3}");
  }
  
  /**
   * Return the "real" network address of the current host (ie a non-loopback
   * interface).
   * <p>
   *
   * @throws IOException if an error occurs accessing network adapter info.
   * @throws NoHostNameException if no non-loopback address can be found.
   */
  public static InetAddress getNetworkAddress ()
    throws IOException, NoHostNameException
  {
    InetAddress address = sortByPreferredIP (getInetAddresses ()).get (0);

    if (address.isLoopbackAddress ())
      throw new NoHostNameException ("Only loopback network address"
          + " is available");

    return address;
  }

  /**
   * Finds all the {@link java.net.InetAddress InetAddresses} available on this
   * computer.
   *
   * @return A list of valid {@link java.net.InetAddress InetAddresses}.
   * @throws SocketException If there's an issue interrogating local network
   *           interfaces.
   */
  private static List<InetAddress> getInetAddresses () throws SocketException
  {
    List<InetAddress> iaList = new ArrayList<InetAddress> ();
    Enumeration<NetworkInterface> nis = getNetworkInterfaces ();
    while (nis.hasMoreElements ())
    {
      Enumeration<InetAddress> addrs = nis.nextElement ().getInetAddresses ();
      while (addrs.hasMoreElements ())
        iaList.add (addrs.nextElement ());
    }
    return iaList;
  }

  /**
   * Sorts a list of {@link java.net.InetAddress InetAddresses} so that
   * preferred IP addresses are listed first. The preferred list is:
   * <ol>
   * <li> Public IPv4 addresses
   * <li> Public IPv6 addresses
   * <li> Loopback addresses (IPv4 before IPv6)
   * <li> Other local addresses
   * <li> Link local address
   * <li> Other addresses
   * </ol>
   *
   * @param addrs A list of {@link java.net.InetAddress InetAddresses}.
   * @return The parameter <code>addrs</code> for builder pattern convenience.
   */
  private static List<InetAddress> sortByPreferredIP (List<InetAddress> addrs)
  {
    sort (addrs, new Comparator<InetAddress> ()
    {
      public int compare (InetAddress ia1, InetAddress ia2)
      {
        int cat1 = category (ia1);
        int cat2 = category (ia2);

        if (cat1 != cat2)
          return cat1 - cat2;
        else
          return ia1.getHostAddress ().compareTo (ia2.getHostAddress ());
      }

      public int category (InetAddress address)
      {
        String ip = address.getHostAddress ();

        if (address.isLinkLocalAddress ())
          return 7;
        else if (address.isAnyLocalAddress ())
          return 6;
        else if (isIPv6 (ip) && address.isLoopbackAddress ())
          return 5;
        else if (isIPv4 (ip) && address.isLoopbackAddress ())
          return 4;
        else if (address.isSiteLocalAddress ()) // private or NAT address
          return 3;
        else if (isIPv6 (ip))
          return 2;
        if (isIPv4 (ip))
          return 1;
        else
          return 10;
      }
    });

    //    trace ("DFC.Localhost: available addresses (sorted):", Localhost.class);
    //    for (InetAddress addr : addrs)
    //      trace ("- " + addr, Localhost.class);

    return addrs;
  }
}
