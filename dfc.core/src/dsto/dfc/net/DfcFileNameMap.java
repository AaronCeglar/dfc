package dsto.dfc.net;

import java.io.File;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.HashMap;

import dsto.dfc.util.Files;

/**
 * A file name map that allows dynamic mapping of file extensions to
 * MIME types.  Daisy chains to the default system file map if no
 * mapping is found.
 *
 * <p>This class is intended to be used to connect file URL's to their
 * content handler, usually via {@link DfcContentHandlerFactory}.
 *
 * @version $Revision$
 */
public class DfcFileNameMap implements FileNameMap
{
  /** The original system file map */
  private static FileNameMap systemMap;
  /** Map extension (without '.') to MIME type. */
  private static HashMap extensionMap = new HashMap ();
  private static boolean registered = false;

  public DfcFileNameMap ()
  {
    // zip
  }

  /**
   * Register this class as the system wide file map.  May be called
   * more than once.
   */
  public static void registerMap ()
  {
    if (!registered)
    {
      systemMap = URLConnection.getFileNameMap ();
      URLConnection.setFileNameMap (new DfcFileNameMap ());
      registered = true;
    }
  }

  /**
   * Associate a file extension with a MIME type.
   *
   * @param extension The extension (without ".").
   * @param mimeType The MIME type for the extension.
   */
  public static void registerTypeForExtension (String extension,
                                               String mimeType)
  {
    // in case client forgets to register
    registerMap ();

    extensionMap.put (extension, mimeType);
  }

  public String getContentTypeFor (String fileName)
  {
    File file = new File (fileName);
    String extension = Files.getExtension (file);

    String contentType = (String)extensionMap.get (extension);

    // no type found, defer to system default map
    if (contentType == null)
      contentType = systemMap.getContentTypeFor (fileName);

    return contentType;
  }
}
