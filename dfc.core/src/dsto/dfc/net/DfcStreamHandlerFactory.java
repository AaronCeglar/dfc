package dsto.dfc.net;

import java.net.URL;
import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;
import java.util.HashMap;

/**
 * Generic implementation of {@link java.net.URLStreamHandlerFactory}
 * that supports dynamic registration of protocol handlers.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class DfcStreamHandlerFactory implements URLStreamHandlerFactory
{
  private static final DfcStreamHandlerFactory INSTANCE =
    new DfcStreamHandlerFactory ();
  private static boolean registered = false;

  private HashMap handlers = new HashMap ();

  private DfcStreamHandlerFactory ()
  {
    // zip
  }

  /**
   * Get the singleton instance of this class.
   */
  public static DfcStreamHandlerFactory getInstance ()
  {
    return INSTANCE;
  }

  /**
   * Register a handler for a given protocol.
   *
   * @param protocol The protocol to handle (ie the "xxx:" part of the
   * URL).
   * @param handler The instance that will be returned as the handler
   * for the protocol.
   * @exception UnsupportedOperationException if registration fails
   * (probably because something else has already registered itself as
   * a handler factory).
   */
  public void registerHandler (String protocol, URLStreamHandler handler)
    throws UnsupportedOperationException
  {
    maybeRegisterFactory ();

    handlers.put (protocol, handler);
  }

  // URLStreamHandlerFactory interface

  public URLStreamHandler createURLStreamHandler (String protocol)
  {
    return (URLStreamHandler)handlers.get (protocol);
  }

  /**
   * Register the singleton instance of this class as the system-wide
   * URL handler factory.
   *
   * @exception UnsupportedOperationException if another factory is
   * already registered.
   */
  protected static void maybeRegisterFactory ()
    throws UnsupportedOperationException
  {
    if (!registered)
    {
      try
      {
        URL.setURLStreamHandlerFactory (getInstance ());
      } catch (Error ex)
      {
        // can't set factory because someone else already has
        throw new UnsupportedOperationException
          ("Can't install URL handler factory: another factory is already registered (" + ex + ")");
      }
    }

    registered = true;
  }
}
