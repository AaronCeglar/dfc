package dsto.dfc.net;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;

import dsto.dfc.text.StringUtility;

/**
 * Defines a set of URL paths to search for resources.  Analogous to a
 * Java classpath, but allows any URL in the path.  Convenience
 * methods as as {@link #addPathEntry} and {@link #resolveToFile}
 * allow this to be used solely as a file-based search path also.<p>
 *
 * Symbolic constant expansion is provided inside path entries via a
 * property set (see {@link #setSymbols}).
 *
 * @version $Revision$
 * @author Matthew
 */
public class URLSearchPath
{
  private ArrayList searchPath;
  private Properties symbols;

  public URLSearchPath ()
  {
    this.searchPath = new ArrayList ();
    this.symbols = new Properties ();
  }

  /**
   * Get the search path as a list of URL's.
   */
  public List getEntries ()
  {
    return searchPath;
  }

  /**
   * Shortcut to add a file to the search path.
   *
   * @see #addPathEntry(URL)
   */
  public void addPathEntry (String file)
  {
    try
    {
      addPathEntry (new File (file).toURI ().toURL ());
    } catch (MalformedURLException ex)
    {
      throw new Error (ex.getMessage ());
    }
  }

  /**
   * Add an entry to the set of URL's in the search path.  The URL may
   * contain symbolic references (see {@link #setSymbols}).
   */
  public void addPathEntry (URL entry)
  {
    searchPath.add (entry);
  }

  /**
   * Remove an entry to the set of URL's in the search path.
   */
  public void removeEntry (URL entry)
  {
    searchPath.remove (entry);
  }

  /**
   * Convenience method to resolve to a file from a file URL-based
   * search path. Do not use this if any non "file:" URL's are in the
   * path.
   * 
   * @param path The path to resolve. If path is absolute or exists
   *          relative to the current directory, it is returned
   *          without searching paths.
   * @return An absolute path.
   * 
   * @exception NoSuchElementException if path could not be resolved.
   */
  public String resolveToFile (String path)
    throws NoSuchElementException
  {
    File file = new File (path);

    if (file.isAbsolute ())
      return path;
    else if (file.exists ())
    {
      return file.getAbsolutePath ();
    } else
    {
      path = path.replace ('\\', '/');

      URL url = resolveToURL (path);

      if (url.getProtocol ().equals ("file"))
      {
        return url.getPath ();
      } else
      {
        throw new NoSuchElementException ();
      }
    }
  }

  /**
   * Resolve a relative path to a URL by searching the path.
   *
   * @param path The relative path name of the resource to resolve.
   * @return The full URL for path.
   * @exception NoSuchElementException if the path could not be
   * resolved to anm existing URL.
   */
  public URL resolveToURL (String path) throws NoSuchElementException
  {
    // ignore any initial '/': all paths are relative
    if (path.length () > 0 && path.charAt (0) == '/')
      path = path.substring (1);

    // for each entry in the search path...
    for (int i = 0; i < searchPath.size (); i++)
    {
      URL entry = (URL)searchPath.get (i);

      try
      {
        entry = expandSymbols (entry);
        URL url = new URL (entry, path);

        if (exists (url))
          return url;
      } catch (MalformedURLException ex)
      {
        // ignore: this URL was not valid
      }
    }

    throw new NoSuchElementException ("Failed to resolve " + path);
  }

  /**
   * Shortcut to convert a file path relative to this search path.
   *
   * @param file The path to convert.
   * @return The file relative to this search path if possible (ie if a
   * matching search prefix was found).
   *
   * @see #unresolveToPath(URL)
   */
  public String unresolveToPath (String file)
  {
    try
    {
      return unresolveToPath (new File (file).toURI ().toURL ());
    } catch (MalformedURLException ex)
    {
      return null;
    }
  }

  /**
   * Convert a URL to a path relative to this search path.
   *
   * @param url The URL to unresolve.  The first matching URL in the
   * path that is a prefix to the URL is used.
   * @return The relative path, or null if none could be found.
   */
  public String unresolveToPath (URL url)
  {
    String urlStr = url.toExternalForm ();

    // for each entry in the search path...
    for (int i = 0; i < searchPath.size (); i++)
    {
      URL entry = (URL)searchPath.get (i);
      String entryStr = entry.toExternalForm ();

      if (urlStr.startsWith (entryStr))
      {
        String relativePath = urlStr.substring (entryStr.length ());

        return relativePath;
      }
    }

    return null;
  }

  public Properties getSymbols ()
  {
    return symbols;
  }

  /**
   * Set symbol mappings for expansion of symbolic names within search
   * path entries.  Entries in the search path may reference these
   * symbolic names using the ${property_name} syntax within the URL.
   * See {@link StringUtility#expandSymbols} for more information on
   * the expansion rules.
   */
  public void setSymbols (Properties aSymbols)
  {
    symbols = aSymbols;
  }

  protected URL expandSymbols (URL url) throws MalformedURLException
  {
    String urlStr = url.toExternalForm ();

    if (urlStr.indexOf ("${") != -1)
    {
      String newUrlStr = StringUtility.expandSymbols (urlStr, symbols);

      return new URL (newUrlStr);
    } else
      return url;
  }

  protected static boolean containsSymbol (URL url)
  {
    return url.toExternalForm ().indexOf ("${") != -1;
  }

  protected static boolean exists (URL url)
  {
    boolean result = false;

    try
    {
      if (url.getProtocol ().equals ("http"))
      {
        HttpURLConnection connection = (HttpURLConnection)url.openConnection ();

        // check for response
        int response = connection.getResponseCode ();
        connection.disconnect ();

        // found if reponse == 2xx (ie HTTP success)
        result = (response >= 200 && response < 300);
      } else if (url.getProtocol ().equals ("file"))
      {
        return new File (url.getPath ()).exists ();
      } else
      {
        // fall back to trying to open a stream to the URL
        InputStream stream = url.openStream ();
        stream.close ();
        result = true;
      }
    } catch (IOException ex)
    {
      // assume IO error means not found
      result = false;
    }

    return result;
  }
}
