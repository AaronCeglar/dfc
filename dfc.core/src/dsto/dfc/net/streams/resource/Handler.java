package dsto.dfc.net.streams.resource;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

import dsto.dfc.net.DfcStreamHandlerFactory;

/**
 * Handler for URL's using the "resource:" protocol.  Searches the
 * current Java resource path (using {@link Class#getResource} for the
 * URL contents.
 *
 * @version $Revision$
 */
public class Handler extends URLStreamHandler
{
  private static final Handler INSTANCE = new Handler ();

  public Handler ()
  {
    // zip
  }

  /**
   * Get the singleton instance of this class.  Other instances may be
   * safely created as well.
   */
  public static Handler getInstance ()
  {
    return INSTANCE;
  }

  /**
   * Register this class as the handler for the "resource:" URL
   * protocol. May be called more than once.
   */
  public static void register () throws UnsupportedOperationException
  {
    String oldHandlers = System.getProperty ("java.protocol.handler.pkgs");

    // check whether already registered
    if (oldHandlers != null && oldHandlers.indexOf ("dsto.dfc.net.streams") != -1)
      return;

    // tack this package onto protocol handlers list
    if (oldHandlers != null)
    {
      System.setProperty ("java.protocol.handler.pkgs",
                          oldHandlers + "|dsto.dfc.net.streams");
    } else
    {
      System.setProperty ("java.protocol.handler.pkgs",
                          "dsto.dfc.net.streams");
    }

    // register with DFC handler factory also
    DfcStreamHandlerFactory.getInstance ().registerHandler
      ("resource", getInstance ());
  }

  // URLStreamHandler interface

  public URLConnection openConnection (URL url) throws IOException
  {
    URL resourceURL = getClass ().getResource (url.getPath ());

    if (resourceURL == null)
      throw new IOException ("Could not find resource " + url);

    return resourceURL.openConnection ();
  }
}
