package dsto.dfc.net.streams.object;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import dsto.dfc.util.Beans;

/**
 * The URLConnection implementation used by {@link Handler}.

 * @version $Revision$
 */
public class ObjectURLConnection extends URLConnection
{
  private Object object;

  public ObjectURLConnection (URL url)
  {
    super (url);
  }

  /**
   * 'Connect' by trying to instantiate the path of the URL as a Java
   * class.
   */
  public void connect () throws IOException
  {
    if (connected)
      return;

    String className = getURL ().getPath ();
    Class objectClass;

    try
    {
      objectClass = Class.forName (className);

      // use singleton instance if possible...
      if (Beans.isSingletonClass (objectClass))
        object = Beans.getSingletonInstance (objectClass);
      else
        object = objectClass.newInstance ();
    } catch (ClassNotFoundException ex)
    {
      throw new IOException ("Class " + className + " not found");
    } catch (InstantiationException ex)
    {
      throw new IOException ("Failed to instantiate class " + className +
                             " (" + ex + ")");
    } catch (IllegalAccessException ex)
    {
      throw new IOException ("Could not access constructor for class " +
                             className + " (" + ex + ")");
    }

    connected = true;
  }

  public Object getContent () throws IOException
  {
    connect ();

    return object;
  }
}
