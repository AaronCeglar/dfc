package dsto.dfc.net.streams.search;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.HashMap;
import java.util.NoSuchElementException;

import dsto.dfc.net.DfcStreamHandlerFactory;
import dsto.dfc.net.URLSearchPath;

/**
 * A URLStreamHandler that acts as a proxy for a set of named
 * URLSearchPath's.  This allows use of indirect "search:" URL's which
 * can be resolved dynamically by searching a set of base URL's
 * (defined by a {@link dsto.dfc.net.URLSearchPath}.  The name of the
 * search path is selected via the host part of the URL: eg
 * <code>search://[search_path]/[relative_path]</code>.
 *
 * @version $Revision$
 * @author Matthew
 */
public class Handler extends URLStreamHandler
{
  private static final Handler INSTANCE = new Handler ();

  private static boolean registered = false;
  private static HashMap searchPaths = new HashMap ();

  private Handler ()
  {
    // zip
  }

  public static Handler getInstance ()
  {
    return INSTANCE;
  }

  /**
   * Register this class as the handler for the "search:" URL
   * protocol. May be called more than once.
   *
   * @exception UnsupportedOperationException if part of the
   * registration fails (probably because another URL factory is
   * already installed).  This does not necessarily mean that the
   * handler is not installed at all since two methods are used
   * (factory and registration via the java.protocol.handler.pkgs
   * system property).
   */
  public static void registerHandler ()
    throws UnsupportedOperationException
  {
    if (registered)
      return;

    String oldHandlers = System.getProperty ("java.protocol.handler.pkgs");

    // tack dsto.dfc.net.streams.search package onto protocol handlers
    // list
    if (oldHandlers != null)
    {
      System.setProperty ("java.protocol.handler.pkgs",
                          oldHandlers + "|dsto.dfc.net.streams.search");
    } else
    {
      System.setProperty ("java.protocol.handler.pkgs",
                          "dsto.dfc.net.streams.search");
    }

    DfcStreamHandlerFactory.getInstance ().registerHandler
      ("search", getInstance ());

    registered = true;
  }

  /**
   * The search path registered for a given name.  Null if no path is
   * registered.
   */
  public static URLSearchPath getSearchPath (String searchPathName)
  {
    return (URLSearchPath)searchPaths.get (searchPathName);
  }

  /**
   * Register a search path for a given name.  Auto registers this
   * class as the "search:" protocol handler if necessary.
   *
   * @param searchPathName The search path name.  URL's of the form
   * <code>search://[name]/...</code> will be resolved against
   * <code>searchPath</code>.
   * @param searchPath The search path to associated with
   * <code>searchPathName</code>
   */
  public static void registerSearchPath (String searchPathName,
                                         URLSearchPath searchPath)
  {
    registerHandler ();

    searchPaths.put (searchPathName, searchPath);
  }

  public URLConnection openConnection (URL url) throws IOException
  {
    String searchPathName = url.getHost ();
    String path = url.getPath ();
    URLSearchPath searchPath = getSearchPath (searchPathName);

    // check if search path defined
    if (searchPath == null)
    {
      throw new IOException ("Unregistered search path \"" +
                             searchPathName + "\"");
    }

    try
    {
      URL resolvedURL = searchPath.resolveToURL (path);

      return resolvedURL.openConnection ();
    } catch (NoSuchElementException ex)
    {
      throw new IOException ("Failed to resolve \"" + url + "\"");
    }
  }
}
