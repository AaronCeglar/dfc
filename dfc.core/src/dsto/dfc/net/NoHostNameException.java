package dsto.dfc.net;

import java.io.IOException;

/**
 * @author Matthew Phillips
 */
public class NoHostNameException extends IOException
{
  public NoHostNameException ()
  {
    super ();
  }

  public NoHostNameException (String s)
  {
    super (s);
  }
}
