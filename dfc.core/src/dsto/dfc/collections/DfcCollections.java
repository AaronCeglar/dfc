package dsto.dfc.collections;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import dsto.dfc.util.DefaultComparator;

import static java.lang.System.arraycopy;

/**
 * Miscellaneous utilities for use with the Java Collections classes.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public final class DfcCollections
{
  private DfcCollections ()
  {
    // zip
  }

  /**
   * Find the index that an item should have in a sorted list to maintain the
   * sorted order.  The items in the list are assumed to be Comparable.
   *
   * @param items A sorted list of items.
   * @param item The item.
   */
  public static int findSortedIndex (List items, Object item)
  {
    return findSortedIndex (items, item, DefaultComparator.instance ());
  }

  /**
   * Find the index that an item should have in a sorted list to maintain the
   * sorted order.
   *
   * @param items A sorted list of items.
   * @param item The item.
   * @param comparator The comparator used to order the list.
   */
  public static int findSortedIndex (List items, Object item,
                                     Comparator comparator)
  {
    for (int i = 0; i < items.size (); i++)
    {
      if (comparator.compare (item, items.get (i)) <= 0)
        return i;
    }

    return items.size ();
  }
  
  /**
   * Compare the contents of two arrays. The contents must implement the 
   * Comparable interface.
   * 
   * @param a1 The first array.
   * @param a2 The second array.
   * @return &lt; 0 if a1 &lt; a2, 0 if a1 == a2, &gt; 0 if a1 &gt; a2.
   * @throws IllegalArgumentException if any of the elements in a1 or a2 are
   * found not to be {@link Comparable}.
   */
  public static int compareArrays (Object [] a1, Object [] a2)
    throws IllegalArgumentException
  {
    if (a1.length == a2.length)
    {
      try
      {
        for (int i = 0; i < a1.length; i++)
        {
          int result = ((Comparable)a1 [i]).compareTo (a2 [i]);
          
          if (result != 0)
            return result;
        }
        
        return 0;
      } catch (ClassCastException ex)
      {
        throw new IllegalArgumentException
          ("Non-comparable array entry: " + ex.getMessage ());
      }
    } else
    {
      return a1.length - a2.length;
    }
  }
  
  /**
   * Test whether two int arrays are equal.
   */
  public static boolean arraysEqual (int [] a1, int [] a2)
  {
    if (a1 == a2)
      return true;
    
    if (a1 == null || a2 == null)
      return false;

    if (a1.length == a2.length)
    {
      for (int i = 0; i < a1.length; i++)
        if (a1 [i] != a2 [i])
          return false;
          
      return true;
    }
    
    return false;
  }
  
  public static boolean arraysEqual (Object [] a1, Object [] a2)
  {
    if (a1 == a2)
      return true;
    
    if (a1 == null || a2 == null)
      return false;

    if (a1.length == a2.length)
    {
      for (int i = 0; i < a1.length; i++)
        if (!a1 [i].equals (a2 [i]))
          return false;
          
      return true;
    }
    
    return false;
  }
  
  /**
   * Test if one collection contains any element from another.
   * 
   * @return True if any element of coll is in source.
   * 
   * @see #containsAny(Collection, Collection, Comparator)
   */
  public static boolean containsAny (Collection source, Collection coll)
  {
    for (Iterator i = coll.iterator (); i.hasNext (); )
    {
      if (source.contains (i.next ()))
        return true;
    }
    
    return false;
  }
  
  /**
   * Test if one collection contains any element from another.
   * 
   * @return True if any element of coll is in source, using comparator
   * to test for equality.
   */
  public static boolean containsAny (Collection source, Collection coll,
                                        Comparator comparator)
  {
    for (Iterator i = coll.iterator (); i.hasNext (); )
    {
      Object item = i.next ();
      
      for (Iterator j = source.iterator (); j.hasNext (); )
      {
        if (comparator.compare (item, j.next ()) == 0)
          return true;
      }
    }
    
    return false;
  }
  
  /**
   * Deep clone an array. This is equivalent to Arrays.copyOf () in
   * Java 6.
   * 
   * @param source The array to copy
   * @param newLength The length of the copied array. If the array is
   *          extended, the new elements are set to 0.
   * 
   * @return A copy of the source array.
   */
  public static int [] cloneArray (int [] source, int newLength)
  {
    int [] copy = new int [newLength];
    
    arraycopy (source, 0, copy, 0, newLength);
    
    return copy;
  }
}