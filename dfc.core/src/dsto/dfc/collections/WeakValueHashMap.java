package dsto.dfc.collections;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 * A hashmap that has weak values in the same way as
 * {@link java.util.WeakHashMap} has weak keys.
 *
 * Original author: Chris Wood
 */
public class WeakValueHashMap extends AbstractMap implements Map
{
  protected final Map m_hash;

  protected final ReferenceQueue m_queue = new ReferenceQueue ();

  protected Set m_entrySet;

  /**
   * Constructs a new, empty <code>WeakValueHashMap</code> with the
   * given initial capacity and the given load factor.
   *
   * @param initialCapacity The initial capacity of the
   *          <code>WeakValueHashMap</code>
   * @param loadFactor The load factor of the
   *          <code>WeakValueHashMap</code>
   */
  public WeakValueHashMap (int initialCapacity, float loadFactor)
  {
    m_hash = new HashMap (initialCapacity, loadFactor);
  }

  /**
   * Constructs a new, empty <code>WeakValueHashMap</code> with the
   * given initial capacity and the default load factor, which is
   * <code>0.75</code>.
   *
   * @param initialCapacity The initial capacity of the
   *          <code>WeakValueHashMap</code>
   */
  public WeakValueHashMap (int initialCapacity)
  {
    m_hash = new HashMap (initialCapacity);
  }

  /**
   * Constructs a new, empty <code>WeakValueHashMap</code> with the
   * default initial capacity and the default load factor, which is
   * <code>0.75</code>.
   */
  public WeakValueHashMap ()
  {
    m_hash = new HashMap ();
  }

  /**
   * Constructs a new <code>WeakValueHashMap</code> with the same
   * mappings as the specified <tt>Map</tt>. The
   * <code>WeakValueHashMap</code> is created with an initial
   * capacity of twice the number of mappings in the specified map or
   * 11 (whichever is greater), and a default load factor, which is
   * <tt>0.75</tt>.
   *
   * @param t the map whose mappings are to be placed in this map.
   * @since 1.3
   */
  public WeakValueHashMap (Map t)
  {
    this (Math.max (2 * t.size (), 11), 0.75f);
    putAll (t);
  }

  /**
   * Remove all invalidated entries from the map, that is, remove all
   * entries whose keys have been discarded. This method should be
   * invoked once by each public mutator in this class. We don't
   * invoke this method in public accessors because that can lead to
   * surprising ConcurrentModificationExceptions.
   */
  protected void processQueue ()
  {
    WeakValue wv;
    while ((wv = (WeakValue)m_queue.poll ()) != null)
    {
      m_hash.remove (wv.getKey ());
    }
  }

  /**
   * Returns a <code>Set</code> view of the mappings in this map.
   *
   * @return The set of entries in the map. This method creates an
   *         empty set if the member is null.
   */
  @Override
  public Set entrySet ()
  {
    if (m_entrySet == null)
    {
      m_entrySet = new EntrySet ();
    }
    return m_entrySet;
  }

  /**
   * Returns <code>true</code> if this map contains a mapping for
   * the specified key.
   *
   * @param key The key whose presence in this map is to be tested.
   * @return True if the internal map contains the key, false
   *         otherwise.
   */
  @Override
  public boolean containsKey (Object key)
  {
    return m_hash.containsKey (key);
  }

  /**
   * Returns the value to which this map maps the specified
   * <code>key</code>. If this map does not contain a value for
   * this key, then return <code>null</code>.
   *
   * @param key The key whose associated value, if any, is to be
   *          returned
   * @return The value for the spacified key.
   */
  @Override
  public Object get (Object key)
  {
    WeakValue val = (WeakValue)m_hash.get (key);
    if (val == null)
    {
      return null;
    }
    return val.get ();
  }

  /**
   * Updates this map so that the given <code>key</code> maps to the
   * given <code>value</code>. If the map previously contained a
   * mapping for <code>key</code> then that mapping is replaced and
   * the previous value is returned.
   *
   * @param key The key that is to be mapped to the given
   *          <code>value</code>
   * @param value The value to which the given <code>key</code> is
   *          to be mapped
   *
   * @return The previous value to which this key was mapped, or
   *         <code>null</code> if if there was no mapping for the
   *         key
   */
  @Override
  public Object put (Object key, Object value)
  {
    processQueue ();
    return m_hash.put (key, new WeakValue (key, value, m_queue));
  }

  /**
   * Removes the mapping for the given <code>key</code> from this
   * map, if present.
   *
   * @param key The key whose mapping is to be removed
   *
   * @return The value to which this key was mapped, or
   *         <code>null</code> if there was no mapping for the key
   */
  @Override
  public Object remove (Object key)
  {
    processQueue ();
    return m_hash.remove (key);
  }

  /**
   * Removes all mappings from this map.
   */
  @Override
  public void clear ()
  {
    processQueue ();
    m_hash.clear ();
  }

  /* weak value. Entered into map. */
  private static class WeakValue extends WeakReference
  {
    private Object m_key;

    public WeakValue (Object k, Object val, ReferenceQueue q)
    {
      super (val, q);
      m_key = k;
    }

    public Object getKey ()
    {
      return m_key;
    }
  }

  protected static boolean valEquals (Object o1, Object o2)
  {
    return (o1 == null) ? (o2 == null) : o1.equals (o2);
  }

  /* Internal class for entries */
  private class Entry implements Map.Entry
  {
    private Map.Entry m_ent;
    /*
     * Strong reference to value, so that the GC will leave it alone
     * as long as this Entry exists
     */
    private Object m_value;

    Entry (Map.Entry ent, Object value)
    {
      m_ent = ent;
      m_value = value;
    }

    public Object getKey ()
    {
      return m_ent.getKey ();
    }

    public Object getValue ()
    {
      return m_value;
    }

    public Object setValue (Object value)
    {
      m_value = value;
      return m_ent.setValue (new WeakValue (m_ent.getKey (), value,
          m_queue));
    }

    @Override
    public boolean equals (Object o)
    {
      if (!(o instanceof Map.Entry))
      {
        return false;
      }
      Map.Entry e = (Map.Entry)o;
      return (valEquals (getKey (), e.getKey ()) && valEquals (
                                                               m_value,
                                                               e
                                                                   .getValue ()));
    }

    @Override
    public int hashCode ()
    {
      Object k;
      return ((((k = getKey ()) == null) ? 0 : k.hashCode ()) ^ ((m_value == null)
          ? 0
          : m_value.hashCode ()));
    }

  }

  /* Internal class for entry sets */
  class EntrySet extends AbstractSet
  {
    private Set m_hashEntrySet = m_hash.entrySet ();

    public Set getEntrySet ()
    {
      return m_hashEntrySet;
    }

    @Override
    public Iterator iterator ()
    {
      return new Iterator ()
      {
        Iterator hashIterator = getEntrySet ().iterator ();
        Entry next = null;

        public boolean hasNext ()
        {
          while (hashIterator.hasNext ())
          {
            Map.Entry ent = (Map.Entry)hashIterator.next ();
            WeakValue wv = (WeakValue)ent.getValue ();
            Object v = null;
            if ((wv != null) && ((v = wv.get ()) == null))
            {
              /* Weak value has been cleared by GC */
              continue;
            }
            next = new Entry (ent, v);
            return true;
          }
          return false;
        }

        public Object next ()
        {
          if ((next == null) && !hasNext ())
          {
            throw new NoSuchElementException ();
          }
          Entry e = next;
          next = null;
          return e;
        }

        public void remove ()
        {
          hashIterator.remove ();
        }
      };
    }

    @Override
    public boolean isEmpty ()
    {
      return !(iterator ().hasNext ());
    }

    @Override
    public int size ()
    {
      int j = 0;
      for (Iterator i = iterator (); i.hasNext (); i.next ())
      {
        j++;
      }
      return j;
    }

    @Override
    public boolean remove (Object o)
    {
      processQueue ();
      if (!(o instanceof Map.Entry))
      {
        return false;
      }
      Map.Entry e = (Map.Entry)o;
      Object ek = e.getKey ();
      WeakValue wv = (WeakValue)m_hash.get (ek);
      if ((wv == null)
          ? m_hash.containsKey (ek)
          : (wv.get () != null))
      {
        m_hash.remove (ek);
        return true;
      }
      return false;
    }

    @Override
    public int hashCode ()
    {
      int h = 0;
      for (Iterator i = getEntrySet ().iterator (); i.hasNext ();)
      {
        Map.Entry ent = (Map.Entry)i.next ();
        Object k = ent.getKey ();
        if (k == null)
        {
          continue;
        }
        WeakValue wv = (WeakValue)ent.getValue ();
        Object v;
        h += (k.hashCode () ^ ((wv == null || (v = wv.get ()) == null)
            ? 0
            : v.hashCode ()));
      }
      return h;
    }
  }
}

// alternate implementation that requires a separate thread to reap
// dead instances.

///*
// * This code is shamelessly cut from the original from geotools.org
// * (http://modules.geotools.org/). Original license below.
// *
// * Geotools 2 - OpenSource mapping toolkit
// * (C) 2003, Geotools Project Managment Committee (PMC)
// * (C) 2001, Institut de Recherche pour le Developpement
// *
// * This library is free software; you can redistribute it and/or
// * modify it under the terms of the GNU Lesser General Public
// * License as published by the Free Software Foundation; either
// * version 2.1 of the License, or (at your option) any later
// version.
// *
// * This library is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// * Lesser General Public License for more details.
// *
// * You should have received a copy of the GNU Lesser General Public
// * License along with this library; if not, write to the Free
// Software
// * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
// */
//
//import java.util.AbstractMap;
//import java.util.Arrays;
//import java.util.Map;
//import java.util.Set;
//import java.util.WeakHashMap;
//
//import java.lang.ref.ReferenceQueue;
//import java.lang.ref.WeakReference;
//
//import dsto.dfc.util.Objects;
//
//import dsto.dfc.logging.Log;
//
//
///**
// * A hashtable-based {@link Map} implementation with <em>weak values</em>. An entry in a
// * <code>WeakValueHashMap</code> will automatically be removed when its value is no longer
// * in ordinary use. This class is similar to the standard {@link WeakHashMap} class provided
// * is J2SE, except that weak references are hold on values instead of keys.
// * <br><br>
// * The <code>WeakValueHashMap</code> class is thread-safe.
// *
// * @version $Id$
// * @author Martin Desruisseaux
// *
// * @see WeakHashMap
// * @see WeakHashSet
// */
//public class WeakValueHashMap extends AbstractMap {
//    /**
//     * Minimal capacity for {@link #table}.
//     */
//    private static final int MIN_CAPACITY = 7;
//
//    /**
//     * Load factor. Control the moment
//     * where {@link #table} must be rebuild.
//     */
//    private static final float LOAD_FACTOR = 0.75f;
//
//    /**
//     * An entry in the {@link WeakValueHashMap}. This is a weak reference
//     * to a value together with a strong reference to a key.
//     */
//    private final class Entry extends WeakReference implements Map.Entry {
//        /**
//         * The key.
//         */
//        Object key;
//
//        /**
//         * The next entry, or <code>null</code> if there is none.
//         */
//        Entry next;
//
//        /**
//         * Index for this element in {@link #table}. This index
//         * must be updated at every {@link #rehash} call.
//         */
//        int index;
//
//        /**
//         * Construct a new weak reference.
//         */
//        Entry(final Object key, final Object value, final Entry next, final int index) {
//            super(value, WeakCollectionCleaner.DEFAULT.referenceQueue);
//            this.key   = key;
//            this.next  = next;
//            this.index = index;
//        }
//
//        /**
//         * Returns the key corresponding to this entry.
//         */
//        public Object getKey() {
//            return key;
//        }
//
//        /**
//         * Returns the value corresponding to this entry.
//         */
//        public Object getValue() {
//            return get();
//        }
//
//        /**
//         * Replaces the value corresponding to this entry with the specified value.
//         */
//        public Object setValue(final Object value) {
//            if (value != null) {
//                throw new UnsupportedOperationException();
//            }
//            Object old = get();
//            clear();
//            return old;
//        }
//
//        /**
//         * Clear the reference.
//         */
//        public void clear() {
//            super.clear();
//            removeEntry(this);
//            key = null;
//        }
//
//        /**
//         * Compares the specified object with this entry for equality.
//         */
//        public boolean equals(final Object other) {
//            if (other instanceof Map.Entry) {
//                final Map.Entry that = (Map.Entry) other;
//                return Objects.objectsEqual (this.getKey(),   that.getKey()) &&
//                       Objects.objectsEqual (this.getValue(), that.getValue());
//            }
//            return false;
//        }
//
//        /**
//         * Returns the hash code value for this map entry.
//         */
//        public int hashCode() {
//            final Object val = get();
//            return (key==null ? 0 : key.hashCode()) ^
//                   (val==null ? 0 : val.hashCode());
//        }
//    }
//
//    /**
//     * Table of weak references.
//     */
//    private Entry[] table;
//
//    /**
//     * Number of non-nul elements in {@link #table}.
//     */
//    private int count;
//
//    /**
//     * The next size value at which to resize. This value should
//     * be <code>{@link #table}.length*{@link #loadFactor}</code>.
//     */
//    private int threshold;
//
//    /**
//     * The timestamp when {@link #table} was last rehashed. This information
//     * is used to avoid too early table reduction. When the garbage collector
//     * collected a lot of elements, we will wait at least 20 seconds before
//     * rehashing {@link #table}. Too early table reduction leads to many cycles
//     * like "reduce", "expand", "reduce", "expand", etc.
//     */
//    private long lastRehashTime;
//
//    /**
//     * Number of millisecond to wait before to rehash
//     * the table for reducing its size.
//     */
//    private static final long HOLD_TIME = 20*1000L;
//
//    /**
//     * Construct a <code>WeakValueHashMap</code>.
//     */
//    public WeakValueHashMap() {
//        table = new Entry[MIN_CAPACITY];
//        threshold = Math.round(table.length*LOAD_FACTOR);
//        lastRehashTime = System.currentTimeMillis();
//    }
//
//    /**
//     * Invoked by {@link Entry} when an element has been collected
//     * by the garbage collector. This method will remove the weak reference
//     * from {@link #table}.
//     */
//    protected synchronized void removeEntry(final Entry toRemove) {
////        assert valid() : count;
//        final int i = toRemove.index;
//        // Index 'i' may not be valid if the reference 'toRemove'
//        // has been already removed in a previous rehash.
//        if (i < table.length) {
//            Entry prev = null;
//            Entry e = table[i];
//            while (e != null) {
//                if (e == toRemove) {
//                    if (prev != null) {
//                        prev.next = e.next;
//                    } else {
//                        table[i] = e.next;
//                    }
//                    count--;
////                    assert valid();
//
//                    // If the number of elements has dimunished
//                    // significatively, rehash the table.
//                    if (count <= threshold/4) {
//                        rehash(false);
//                    }
//                    // We must not continue the loop, since
//                    // variable 'e' is no longer valid.
//                    return;
//                }
//                prev = e;
//                e = e.next;
//            }
//        }
////        assert valid();
//        /*
//         * If we reach this point, its mean that reference 'toRemove' has not
//         * been found. This situation may occurs if 'toRemove' has already been
//         * removed in a previous run of {@link #rehash}.
//         */
//    }
//
//    /**
//     * Rehash {@link #table}.
//     *
//     * @param augmentation <code>true</code> if this method is invoked
//     *        for augmenting {@link #table}, or <code>false</code> if
//     *        it is invoked for making the table smaller.
//     */
//    private void rehash(final boolean augmentation) {
////        assert Thread.holdsLock(this);
////        assert valid();
//        final long currentTime = System.currentTimeMillis();
//        final int capacity = Math.max(Math.round(count/(LOAD_FACTOR/2)), count+MIN_CAPACITY);
//        if (augmentation ? (capacity<=table.length) :
//                           (capacity>=table.length || currentTime-lastRehashTime<HOLD_TIME))
//        {
//            return;
//        }
//        lastRehashTime = currentTime;
//        final Entry[] oldTable = table;
//        table     = new Entry[capacity];
//        threshold = Math.round(capacity*LOAD_FACTOR);
//        for (int i=0; i<oldTable.length; i++) {
//            for (Entry old=oldTable[i]; old!=null;) {
//                final Entry e=old;
//                old = old.next; // On retient 'next' tout de suite car sa valeur va changer...
//                final Object key = e.key;
//                if (key != null) {
//                    final int index=(key.hashCode() & 0x7FFFFFFF) % table.length;
//                    e.index = index;
//                    e.next  = table[index];
//                    table[index] = e;
//                } else {
//                    count--;
//                }
//            }
//        }
////        final Logger logger = Logger.getLogger("org.geotools.util");
////        final Level   level = Level.FINEST;
////        if (logger.isLoggable(level)) {
////            final LogRecord record = new LogRecord(level, "Rehash from " + oldTable.length +
////                                                                  " to " +    table.length);
////            record.setSourceMethodName(augmentation ? "canonicalize" : "remove");
////            record.setSourceClassName("WeakValueHashMap");
////            logger.log(record);
////        }
////        assert valid();
//    }
//
//    /**
//     * Check if this <code>WeakValueHashMap</code> is valid. This method counts the
//     * number of elements and compare it to {@link #count}. If the check fails,
//     * the number of elements is corrected (if we didn't, an {@link AssertionError}
//     * would be thrown for every operations after the first error,  which make
//     * debugging more difficult). The set is otherwise unchanged, which should
//     * help to get similar behaviour as if assertions hasn't been turned on.
//     */
////    private boolean valid() {
////        int n=0;
////        for (int i=0; i<table.length; i++) {
////            for (Entry e=table[i]; e!=null; e=e.next) {
////                n++;
////            }
////        }
////        if (n!=count) {
////            count = n;
////            return false;
////        } else {
////            return true;
////        }
////    }
//
//    /**
//     * Returns the number of key-value mappings in this map.
//     */
//    public synchronized int size() {
////        assert valid();
//        return count;
//    }
//
//    /**
//     * Returns <code>true</code> if this map maps one or more keys to this value.
//     *
//     * @param value value whose presence in this map is to be tested.
//     * @return <code>true</code> if this map maps one or more keys to this value.
//     */
//    public synchronized boolean containsValue(final Object value) {
//        return super.containsValue(value);
//    }
//
//    /**
//     * Returns <code>true</code> if this map contains a mapping for the specified key.
//     *
//     * @param key key whose presence in this map is to be tested.
//     * @return <code>true</code> if this map contains a mapping for the specified key.
//     * @throws NullPointerException If key is <code>null</code>.
//     */
//    public boolean containsKey(final Object key) {
//        return get(key) != null;
//    }
//
//    /**
//     * Returns the value to which this map maps the specified key. Returns
//     * <code>null</code> if the map contains no mapping for this key.
//     *
//     * @param  key Key whose associated value is to be returned.
//     * @return The value to which this map maps the specified key.
//     * @throws NullPointerException if the key is <code>null</code>.
//     */
//    public synchronized Object get(final Object key) {
////        assert WeakCollectionCleaner.DEFAULT.isAlive();
////        assert valid() : count;
//        final int index = (key.hashCode() & 0x7FFFFFFF) % table.length;
//        for (Entry e=table[index]; e!=null; e=e.next) {
//            if (key.equals(e.key)) {
//                return e.get();
//            }
//        }
//        return null;
//    }
//
//    /**
//     * Implementation of {@link #put} and {@link #remove} operations.
//     */
//    private synchronized Object intern(final Object key, final Object value) {
////        assert WeakCollectionCleaner.DEFAULT.isAlive();
////        assert valid() : count;
//        /*
//         * Check if <code>obj</code> is already contained in this
//         * <code>WeakValueHashMap</code>. If yes, clear it.
//         */
//        Object oldValue = null;
//        final int hash = key.hashCode() & 0x7FFFFFFF;
//        int index = hash % table.length;
//        for (Entry e=table[index]; e!=null; e=e.next) {
//            if (key.equals(e.key)) {
//                oldValue = e.get();
//                e.clear();
//            }
//        }
//        if (value != null) {
//            if (count >= threshold) {
//                rehash(true);
//                index = hash % table.length;
//            }
//            table[index] = new Entry(key, value, table[index], index);
//            count++;
//        }
////        assert valid();
//        return oldValue;
//    }
//
//    /**
//     * Associates the specified value with the specified key in this map.
//     * The value is associated using a {@link WeakReference}.
//     *
//     * @param  key key with which the specified value is to be associated.
//     * @param  value value to be associated with the specified key.
//     * @return previous value associated with specified key, or <code>null</code>
//     *         if there was no mapping for key.
//     *
//     * @throws NullPointerException if the key or the value is <code>null</code>.
//     */
//    public Object put(final Object key, final Object value) {
//        if (value == null) {
//            throw new NullPointerException("Null value not allowed");
//            // TODO: localize this message.
//        }
//        return intern(key, value);
//    }
//
//    /**
//     * Removes the mapping for this key from this map if present.
//     *
//     * @param key key whose mapping is to be removed from the map.
//     * @return previous value associated with specified key, or <code>null</code>
//     *         if there was no entry for key.
//     */
//    public Object remove(final Object key) {
//        return intern(key, null);
//    }
//
//    /**
//     * Removes all of the elements from this map.
//     */
//    public synchronized void clear() {
//        Arrays.fill(table, null);
//        count = 0;
//    }
//
//    /**
//     * Returns a set view of the mappings contained in this map.
//     * Each element in this set is a {@link java.util.Map.Entry}.
//     * The current implementation thrown {@link UnsupportedOperationException}.
//     *
//     * @return a set view of the mappings contained in this map.
//     */
//    public Set entrySet() {
//        throw new UnsupportedOperationException();
//    }
//
//
//    private static class WeakCollectionCleaner extends Thread
//    {
//      /**
//       * The default thread.
//       */
//      public static final WeakCollectionCleaner DEFAULT = new WeakCollectionCleaner();
//
//      /**
//       * List of reference collected by the garbage collector.
//       * Those elements must be removed from {@link #table}.
//       */
//      public final ReferenceQueue referenceQueue = new ReferenceQueue();
//
//      /**
//       * Construct and stard a new thread as a daemon. This thread will be stoped
//       * most of the time.  It will run only some few nanoseconds each time a new
//       * {@link WeakReference} is enqueded.
//       */
//      private WeakCollectionCleaner() {
//          super("WeakCollectionCleaner");
//          setDaemon(true);
//          start();
//      }
//
//      /**
//       * Loop to be run during the virtual machine lifetime.
//       */
//      public void run() {
//          while (true) {
//              try {
//                  // Block until a reference is enqueded.
//                  // Note: To be usefull, the clear() method must have
//                  //       been overriden in Reference subclasses.
//                  referenceQueue.remove().clear();
//              } catch (InterruptedException exception) {
//                  // Somebody doesn't want to lets
//                  // us sleep... Go back to work.
//              } catch (Throwable ex)
//              {
//                Log.internalError ("Exception in WeakCollectionCleaner", this, ex);
//              }
//          }
//      }
//    }
//}
