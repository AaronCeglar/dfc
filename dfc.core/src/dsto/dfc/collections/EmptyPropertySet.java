package dsto.dfc.collections;

import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * An empty, unmodifiable property set.
 *
 * @version $Revision$
 */
public class EmptyPropertySet implements PropertySet, Serializable, Cloneable
{
	public Set getPropertyNames ()
  {
    return Collections.EMPTY_SET;
  }
	
	public List getPropertyValues (String propertyName)
  {
    return Collections.EMPTY_LIST;
  }

  public Object getPropertyValue (String propertyName)
  {
    return null;
  }
	
	public void setPropertyValues (String propertyName, List values)
   throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException
      ("EmptyPropertySet may not be modified");
  }

	public void setPropertyValue (String propertyName, Object value)
   throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException
      ("EmptyPropertySet may not be modified");
  }
	
	public void removeProperty (String propertyName)
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException
      ("EmptyPropertySet may not be modified");
	}

	public boolean hasProperty (String propertyName)
  {
    return false;
  }
	
	public void removeAll () throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException
      ("EmptyPropertySet may not be modified");
  }
	
	public void addPropertyValue (String propertyName, Object value)
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException
      ("EmptyPropertySet may not be modified");
  }

  public void addPropertyValues (String propertyName, List values)
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException
      ("EmptyPropertySet may not be modified");
  }

  public boolean removePropertyValue (String propertyName, Object value)
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException
      ("EmptyPropertySet may not be modified");
  }

  public boolean isModifiable ()
  {
    return false;
  }

  public Object clone () throws CloneNotSupportedException
  {
    return super.clone ();
  }

  public void addPropertyChangeListener (PropertyChangeListener l)
  {
    // zip: no properties change
  }

  public void removePropertyChangeListener (PropertyChangeListener l)
  {
//  zip: no properties change
  }
}
