package dsto.dfc.collections;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;

import dsto.dfc.util.Copyable;
import dsto.dfc.util.Singleton;

public class EmptyCollection
  implements Collection, Singleton, Copyable, Serializable
{
  public static final EmptyCollection INSTANCE = new EmptyCollection();

  protected EmptyCollection()
  {
    // zip
  }

  public int size()
  {
    return 0;
  }

  public boolean isEmpty()
  {
    return true;
  }

  public boolean contains(Object o)
  {
    return false;
  }

  public Iterator iterator()
  {
    return EmptyIterator.INSTANCE;
  }

  public Object[] toArray()
  {
    Object[] temp = new Object[0];
    return temp;
  }

  public Object[] toArray(Object[] a)
  {
    return a;
  }

  public boolean add(Object o)
  {
    throw new UnsupportedOperationException();
  }

  public boolean remove(Object o)
  {
    throw new UnsupportedOperationException();
  }

  public boolean containsAll(Collection c)
  {
    return false;
  }

  public boolean addAll(Collection c)
  {
    throw new UnsupportedOperationException();
  }

  public boolean removeAll(Collection c)
  {
    throw new UnsupportedOperationException();
  }

  public boolean retainAll(Collection c)
  {
    throw new UnsupportedOperationException();
  }

  public void clear()
  {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean equals(Object o)
  {
    return false;
  }

  @Override
  public int hashCode ()
  {
    return 1;
  }

  @Override
  public Object clone ()
  {
    return this;
  }

  private Object readResolve ()
  {
    return INSTANCE;
  }
}