package dsto.dfc.collections;

import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * An unmodifiable property set containing a fixed set of values.
 *
 * @version $Revision$
 */
public class FixedPropertySet implements PropertySet, Serializable, Cloneable
{
  private FixedArraySetList propertyNames;
  private Object [] propertyValues;

  /**
   * Create a new property set of fixed values.
   *
   * @param propertyNames The names of the properties in the set.
   * @param propertyValues The values for the properties in the set.
   * Arrays will be returned as multiple values for the property.
   */
  public FixedPropertySet (String [] propertyNames, Object [] propertyValues)
  {
    this.propertyNames = new FixedArraySetList (propertyNames);
    this.propertyValues = propertyValues;
  }

  /**
   * Create a new fixed value property set by copying a source
   * property set.
   */
  public FixedPropertySet (PropertySet set)
  {
    if (set instanceof FixedPropertySet)
    {
      FixedPropertySet fixedSet = (FixedPropertySet)set;

      this.propertyNames = fixedSet.propertyNames;
      this.propertyValues = fixedSet.propertyValues;
    } else
    {
      Set propertyNameSet = set.getPropertyNames ();
      String [] names = new String [propertyNameSet.size ()];
      Object [] values = new Object [propertyNameSet.size ()];
      
      for (int i = 0; i < names.length; i++)
      {
        List valueList = set.getPropertyValues (names [i]);

        if (valueList.size () == 1)
          values [i] = valueList.get (0);
        else
          values [i] = valueList.toArray ();
      }

      this.propertyNames = new FixedArraySetList (names);
      this.propertyValues = values;
    }
  }

  public Set getPropertyNames ()
  {
    return propertyNames;
  }
  
  public List getPropertyValues (String propertyName)
  {
    int index = propertyNames.indexOf (propertyName);

    if (index != -1)
    {
      Object value = propertyValues [index];

      if (value instanceof Object [])
        return Arrays.asList ((Object [])value);
      else
        return Arrays.asList (new Object [] {value});
    } else
      return null;
  }

  public Object getPropertyValue (String propertyName)
  {
    int index = propertyNames.indexOf (propertyName);

    if (index != -1)
    {
      Object value = propertyValues [index];

      if (value instanceof Object [])
        return ((Object [])(value)) [0];
      else
        return value;
    } else
      return null; 
  }
  
  public void setPropertyValues (String propertyName, List values)
   throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException
      ("EmptyPropertySet may not be modified");
  }

  public void setPropertyValue (String propertyName, Object value)
   throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException
      ("EmptyPropertySet may not be modified");
  }
  
  public void removeProperty (String propertyName)
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException
      ("EmptyPropertySet may not be modified");
  }

  public boolean hasProperty (String propertyName)
  {
    return propertyNames.contains (propertyName);
  }
  
  public void removeAll () throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException
      ("EmptyPropertySet may not be modified");
  }
  
  public void addPropertyValue (String propertyName, Object value)
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException
      ("EmptyPropertySet may not be modified");
  }

  public void addPropertyValues (String propertyName, List values)
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException
      ("EmptyPropertySet may not be modified");
  }

  public boolean removePropertyValue (String propertyName, Object value)
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException
      ("EmptyPropertySet may not be modified");
  }

  public boolean isModifiable ()
  {
    return false;
  }

  public Object clone () throws CloneNotSupportedException
  {
    return super.clone ();
  }

  public void addPropertyChangeListener (PropertyChangeListener l)
  {
    //  zip: no properties change
  }

  public void removePropertyChangeListener (PropertyChangeListener l)
  {
    //  zip: no properties change
  }
}
