package dsto.dfc.collections;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import dsto.dfc.util.Objects;

/**
 * An implementation of a MonitoredMap, wrapping a backing Map
 * instance.<p>
 *
 * NOTE: modifying entrySet (), values () and keys () does not
 * currently fire collection events: only the "top-level" modifiers
 * such as put (), get (), remove (), etc are monitored.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class BasicMonitoredMap
  extends BasicCollectionEventSource implements MonitoredMap
{
  protected Map backingMap;

  public BasicMonitoredMap ()
  {
    this (new HashMap ());
  }

  public BasicMonitoredMap (Map backingMap)
  {
    this.backingMap = backingMap;
  }

  public Object clone () throws CloneNotSupportedException
  {
    BasicMonitoredMap copy = (BasicMonitoredMap)super.clone ();
    
    copy.backingMap = (Map)Objects.cloneObject (backingMap);
    
    return copy;
  }
  
  /**
   * Make an empty clone of this collection (ie a copy with none of the
   * original elements).
   */
  protected BasicMonitoredMap emptyClone () throws CloneNotSupportedException
  {
    try
    {
      BasicMonitoredMap clone = (BasicMonitoredMap)super.clone ();

      clone.backingMap = backingMap.getClass ().newInstance ();

      return clone;
    } catch (Exception ex)
    {
      throw new CloneNotSupportedException ("Failed to create nested collection: " + ex);
    }
  }
  
  public int size ()
  {
    return backingMap.size ();
  }

  public boolean isEmpty ()
  {
    return backingMap.isEmpty ();
  }

  public boolean containsKey (Object key)
  {
    return backingMap.containsKey (key);
  }

  public boolean containsValue (Object value)
  {
    return backingMap.containsValue (value);
  }

  public Object get (Object key)
  {
    return backingMap.get (key);
  }

  // Modification Operations

  public Object put (Object key, Object value)
  {
    Object oldValue = backingMap.remove (key);

    if (oldValue != null)
      fireElementRemoved (new MyEntry (key, oldValue));

    backingMap.put (key, value);

    fireElementAdded (new MyEntry (key, value));

    return oldValue;
  }

  public Object remove (Object key)
  {
    Object oldValue = backingMap.remove (key);

    if (oldValue != null)
      fireElementRemoved (new MyEntry (key, oldValue));

    return oldValue;
  }

  public void putAll (Map t)
  {
    Iterator i = t.entrySet ().iterator ();

    while (i.hasNext ())
    {
      Entry e = (Entry)i.next();

      put (e.getKey (), e.getValue ());
    }
  }

  public void clear ()
  {
    Iterator i = entrySet ().iterator ();

    while (i.hasNext ())
    {
      Entry e = (Entry)i.next();

      remove (e.getKey ());
    }
  }

  public Set keySet ()
  {
    return backingMap.keySet ();
  }

  public Collection values ()
  {
    return backingMap.values ();
  }

  public Set entrySet ()
  {
    return backingMap.entrySet ();
  }

  protected final class MyEntry implements Map.Entry
  {
    private Object key;
    private Object value;

    public MyEntry (Object key, Object value)
    {
      this.key = key;
      this.value = value;
    }

    public Object getKey ()
    {
      return key;
    }

    public Object getValue ()
    {
      return value;
    }

    public Object setValue (Object value)
    {
      return put (key, value);
    }
  }
}
