package dsto.dfc.collections;

import java.util.NoSuchElementException;

/**
 * A cursor is a more flexible way to move through a collection than an
 * Iterator. A cursor can be moved rapidly back and forth by any number of
 * units. It also remembers the current item it is pointing to.
 * 
 * @author Matthew Phillips
 */
public interface Cursor
{
  /**
   * Move the cursor left (negative units) or right (positive units). This may
   * cause the cursor to move outside the collection: if so, it stops just one
   * unit to the left or right of the sequence and returns the actual number of
   * items it moved past. A subsequent move of one unit in the other direction
   * would put the cursor back to the first/last entry.
   * 
   * @param units
   *          The number of units to move.
   * @return The number of elements actually moved past, which may be less than
   *         the requested units if the cursor has moved outside the collection.
   *         For example, if the cursor was at the last element of the
   *         collection and a move of 1 unit was requested, this returns zero.
   */
  public int move (int units);

  /**
   * @return The current element the cursor is pointing to.
   * 
   * @throws NoSuchElementException
   *           If the cursor is not inside the collection.
   */
  public Object current () throws NoSuchElementException;

  /**
   * @return True if the cursor is valid is within the collection).
   */
  public boolean inside ();
}