package dsto.dfc.collections;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Iterator that scans though a range of numbers.
 */
public class NumberIterator implements Iterator
{
  private int upper;
  private int i;
  
  public NumberIterator (int lower, int upper)
  {
    this.upper = upper;
    this.i = lower;
  }

  public boolean hasNext ()
  {
    return i < upper;
  }

  public Object next ()
  {
    if (hasNext ())
    {
      return new Integer (i++);
    } else
    {
      throw new NoSuchElementException ();
    }
  }

  public void remove ()
  {
    throw new UnsupportedOperationException ();
  }
}