package dsto.dfc.collections;

import java.util.Enumeration;
import java.util.Iterator;

/**
 * Wraps a legacy Enumeration in an Iterator.
 *
 * @see dsto.dfc.collections.IteratorEnumeration
 * 
 * @version $Revision$
 */
public final class EnumerationIterator implements Iterator
{
  Enumeration enumeration;

  public EnumerationIterator (Enumeration enumeration)
  {
    this.enumeration = enumeration;
  }

  public boolean hasNext ()
  {
    return enumeration.hasMoreElements ();
  }

  public Object next ()
  {
    return enumeration.nextElement ();
  }

  public void remove () throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException ("deletion not supported");
  }
}

