package dsto.dfc.collections;

import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Set;

/**
 * A set of (name, value) pairs.
 *
 * @version $Revision$
 */
public interface PropertySet
{
  public static final PropertySet EMPTY_PROPERTY_SET = new EmptyPropertySet ();

  /**
   * The names of defined properties.
   *
   * @return An immutable set of strings.
   *
   * @roseuid 373A5A0103DB
   */
  public Set getPropertyNames ();

  /**
   * The values of a property.
   *
   * @param propertyName The name of the property.
   * @return The values for propertyName, or null if no values are
   * defined.
   *
   * @roseuid 373A5A2103A5
   */
  public List getPropertyValues (String propertyName);

  /**
   * Get the value of a property.  If more than one value exists, the
   * last added value is returned.
   *
   * @roseuid 373A5A3F0236
   */
  public Object getPropertyValue (String propertyName);

  /**
   * Set the values of a property.
   *
   * @exception UnsupportedOperationException if property values
   * cannot be changed.
   *
   * @roseuid 373A5A880321
   */
  public void setPropertyValues (String propertyName, List values)
   throws UnsupportedOperationException;

  public void setPropertyValue (String propertyName, Object value)
   throws UnsupportedOperationException;

  /**
   * Remove all values of a property (ie undefine it).
   *
   * @exception UnsupportedOperationException if property values
   * cannot be changed.
   *
   * @roseuid 373A5AA102D7
   */
  public void removeProperty (String propertyName)
    throws UnsupportedOperationException;

  /**
   * Remove the first occurrence of a value from a list of property values.
   *
   * @return True if the property set was changed (ie a value was removed).
   * @exception UnsupportedOperationException if property values
   * cannot be changed.
   */
  public boolean removePropertyValue (String propertyName, Object value)
    throws UnsupportedOperationException;

  /**
   * True if a property has any defined values.
   *
   * @roseuid 373A5AB60056
   */
  public boolean hasProperty (String propertyName);

  /**
   * Undefine all properties in the set
   *
   * @exception UnsupportedOperationException if property values
   * cannot be changed.
   *
   * @roseuid 373A5AC60316
   */
  public void removeAll () throws UnsupportedOperationException;

  /**
   * Add a values to the end of the list of property values.
   *
   * @exception UnsupportedOperationException if property values
   * cannot be changed.
   *
   * @roseuid 373A5BCD0061
   */
  public void addPropertyValue (String propertyName, Object value)
    throws UnsupportedOperationException;

  /**
   * True if property set may be changed.
   */
  public boolean isModifiable ();

  /**
   * Add a set of values to a property.
   *
   * @exception UnsupportedOperationException if property values
   * cannot be changed.
   */
  public void addPropertyValues (String propertyName, List values)
    throws UnsupportedOperationException;

  public Object clone () throws CloneNotSupportedException;

  public void addPropertyChangeListener (PropertyChangeListener l);

  public void removePropertyChangeListener (PropertyChangeListener l);
}
