package dsto.dfc.collections;

import java.util.ArrayList;
import java.util.List;

/**
 * Extended MonitoredList implementation supporting move operations.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class DfcMonitoredList extends BasicMonitoredList
{
  /**
   * Create a new instance using an ArrayList for storage.
   */
  public DfcMonitoredList ()
  {
    this (new ArrayList ());
  }

  public DfcMonitoredList (List backingList)
  {
    super (backingList);
  }

  /**
   * Move an element from one index to another, firing an
   * elementMoved event to any registered DfcMonitoredListListener's.
   */
  public void move (int fromIndex, int toIndex)
    throws IndexOutOfBoundsException
  {
    List list = getBackingList ();
    
    Object element = list.remove (fromIndex);

    list.add (toIndex, element);

    fireElementMoved (element, fromIndex, toIndex);
  }

  public void addDfcMonitoredListListener (DfcMonitoredListListener l)
  {
    addCollectionListener (l);
  }

  public void removeDfcMonitoredListListener (DfcMonitoredListListener l)
  {
    removeCollectionListener (l);
  }

  protected void fireElementMoved (Object element,
                                   int oldIndex, int newIndex)
  {
    if (collectionListeners != null)
    {
      CollectionEvent e =
        new CollectionEvent (this, element, oldIndex, newIndex);
      ArrayList listeners = collectionListeners;
      int count = listeners.size ();

      for (int i = 0; i < count; i++)
      {
        Object l = listeners.get (i);

        if (l instanceof DfcMonitoredListListener)
          ((DfcMonitoredListListener)l).elementMoved (e);
      }
    }
  }
}