package dsto.dfc.collections;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/**
 * Exactly like HashSet except uses an IdentityHashMap as backing, thus
 * comparing objects using == instead of equals ().
 * 
 * @author mpp
 * @version $Revision$
 */
public class IdentityHashSet
  extends AbstractSet
  implements Set, Cloneable, java.io.Serializable
{
  private transient IdentityHashMap map;

  // Dummy value to associate with an Object in the backing Map
  private static final Object PRESENT = new Object ();

  /**
   * Constructs a new, empty set; the backing <tt>IdentityHashMap</tt> instance has
   * default capacity and load factor, which is <tt>0.75</tt>.
   */
  public IdentityHashSet ()
  {
    map = new IdentityHashMap ();
  }

  /**
   * Constructs a new set containing the elements in the specified
   * collection.  The capacity of the backing <tt>IdentityHashMap</tt> instance is
   * twice the size of the specified collection or eleven  (whichever is
   * greater), and the default load factor  (which is <tt>0.75</tt>) is used.
   *
   * @param c the collection whose elements are to be placed into this set.
   */
  public IdentityHashSet (Collection c)
  {
    map = new IdentityHashMap (Math.max (2 * c.size (), 11));
    addAll (c);
  }

  /**
   * Constructs a new, empty set; the backing <tt>IdentityHashMap</tt> instance has
   * the specified initial capacity and the specified load factor.
   *
   * @param      initialCapacity   the initial capacity of the hash map.
   * @param      loadFactor        the load factor of the hash map.
   * @throws     IllegalArgumentException if the initial capacity is less
   *             than zero, or if the load factor is nonpositive.
   */
  public IdentityHashSet (int initialCapacity, float loadFactor)
  {
    map = new IdentityHashMap (initialCapacity, loadFactor);
  }

  /**
   * Constructs a new, empty set; the backing <tt>IdentityHashMap</tt> instance has
   * the specified initial capacity and default load factor, which is
   * <tt>0.75</tt>.
   *
   * @param      initialCapacity   the initial capacity of the hash table.
   * @throws     IllegalArgumentException if the initial capacity is less
   *             than zero.
   */
  public IdentityHashSet (int initialCapacity)
  {
    map = new IdentityHashMap (initialCapacity);
  }

  /**
   * Returns an iterator over the elements in this set.  The elements
   * are returned in no particular order.
   *
   * @return an Iterator over the elements in this set.
   */
  public Iterator iterator ()
  {
    return map.keySet ().iterator ();
  }

  /**
   * Returns the number of elements in this set  (its cardinality).
   *
   * @return the number of elements in this set  (its cardinality).
   */
  public int size ()
  {
    return map.size ();
  }

  /**
   * Returns <tt>true</tt> if this set contains no elements.
   *
   * @return <tt>true</tt> if this set contains no elements.
   */
  public boolean isEmpty ()
  {
    return map.isEmpty ();
  }

  /**
   * Returns <tt>true</tt> if this set contains the specified element.
   *
   * @param o element whose presence in this set is to be tested.
   * @return <tt>true</tt> if this set contains the specified element.
   */
  public boolean contains (Object o)
  {
    return map.containsKey (o);
  }

  /**
   * Adds the specified element to this set if it is not already
   * present.
   *
   * @param o element to be added to this set.
   * @return <tt>true</tt> if the set did not already contain the specified
   * element.
   */
  public boolean add (Object o)
  {
    return map.put (o, PRESENT) == null;
  }

  /**
   * Removes the given element from this set if it is present.
   *
   * @param o object to be removed from this set, if present.
   * @return <tt>true</tt> if the set contained the specified element.
   */
  public boolean remove (Object o)
  {
    return map.remove (o) == PRESENT;
  }

  /**
   * Removes all of the elements from this set.
   */
  public void clear ()
  {
    map.clear ();
  }

  /**
   * Returns a shallow copy of this <tt>identityHashSet</tt> instance: the elements
   * themselves are not cloned.
   *
   * @return a shallow copy of this set.
   */
  public Object clone ()
  {
    try
    {
      IdentityHashSet newSet =  (IdentityHashSet) super.clone ();
      newSet.map =  (IdentityHashMap) map.clone ();
      return newSet;
    } catch  (CloneNotSupportedException e)
    {
      throw new InternalError ();
    }
  }

  /**
   * Save the state of this <tt>identityHashSet</tt> instance to a stream  (that is,
   * serialize this set).
   *
   * @serialData The capacity of the backing <tt>IdentityHashMap</tt> instance
   *        (int), and its load factor  (float) are emitted, followed by
   *       the size of the set  (the number of elements it contains)
   *        (int), followed by all of its elements  (each an Object) in
   *             no particular order.
   */
  private synchronized void writeObject (java.io.ObjectOutputStream s)
    throws java.io.IOException
  {
    // Write out any hidden serialization magic
    s.defaultWriteObject ();

    // Write out IdentityHashMap capacity and load factor
    s.writeInt (map.capacity ());
    s.writeFloat (map.loadFactor ());

    // Write out size
    s.writeInt (map.size ());

    // Write out all elements in the proper order.
    for  (Iterator i = map.keySet ().iterator (); i.hasNext ();)
      s.writeObject (i.next ());
  }

  /**
   * Reconstitute the <tt>identityHashSet</tt> instance from a stream  (that is,
   * deserialize it).
   */
  private synchronized void readObject (java.io.ObjectInputStream s)
    throws java.io.IOException, ClassNotFoundException
  {
    // Read in any hidden serialization magic
    s.defaultReadObject ();

    // Read in IdentityHashMap capacity and load factor and create backing IdentityHashMap
    int capacity = s.readInt ();
    float loadFactor = s.readFloat ();
    map = new IdentityHashMap (capacity, loadFactor);

    // Read in size
    int size = s.readInt ();

    // Read in all elements in the proper order.
    for  (int i = 0; i < size; i++)
    {
      Object e = s.readObject ();
      map.put (e, PRESENT);
    }
  }
}
