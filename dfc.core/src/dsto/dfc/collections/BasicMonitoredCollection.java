package dsto.dfc.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import java.io.Serializable;

import dsto.dfc.util.Copyable;
import dsto.dfc.util.Objects;

/**
 * Basic implementation of MonitoredCollection that acts as a wrapper
 * around a Collection instance.
 *
 * @version $Revision$
 */
public class BasicMonitoredCollection
  implements MonitoredCollection, Serializable, Copyable
{
  private static final long serialVersionUID = 1L;

  protected Collection collection;
  protected transient ArrayList collectionListeners;

  public BasicMonitoredCollection (Collection collection)
  {
    this.collection = collection;
  }

  // Collection interface

  public boolean add (Object element)
  {
    boolean added = collection.add (element);

    if (added)
      fireElementsAdded (new CollectionEvent (this, element));

    return added;
  }

  public boolean remove (Object element)
  {
    boolean removed = collection.remove (element);

    if (removed)
      fireElementsRemoved (new CollectionEvent (this, element));

    return removed;
  }

  public boolean addAll (Collection elements)
  {
    boolean added = collection.addAll (elements);

    if (added)
      fireElementsAdded (new CollectionEvent (this, elements));

    return added;
  }

  public boolean removeAll (Collection elements)
  {
    boolean removed = collection.removeAll (elements);

    if (removed)
      fireElementsRemoved (new CollectionEvent (this, elements));

    return removed;
  }

  public boolean retainAll (Collection elements)
  {
    // TODO: implement
    throw new UnsupportedOperationException ("retainAll () not implemented");
  }

  public void clear ()
  {
    if (collection.size () > 0)
    {
      Collection origCollection = collection;

      // make it appear that collection has been cleared
      collection = Collections.EMPTY_LIST;

      fireElementsRemoved (new CollectionEvent (this, origCollection));

      // actually clear collection now and restore reference
      origCollection.clear ();
      collection = origCollection;
    }
  }

  public int size ()
  {
    return collection.size ();
  }

  public boolean isEmpty ()
  {
    return collection.isEmpty ();
  }

  public boolean contains (Object element)
  {
    return collection.contains (element);
  }

  public Iterator iterator ()
  {
    return new CollectionIteratorWrapper ();
  }

  public Object [] toArray()
  {
    return collection.toArray ();
  }

  public Object [] toArray (Object [] array)
  {
    return collection.toArray (array);
  }

  public boolean containsAll (Collection elements)
  {
    return collection.containsAll (elements);
  }

  public Object clone () throws CloneNotSupportedException
  {
    BasicMonitoredCollection clone = (BasicMonitoredCollection)super.clone();
    clone.collection = (Collection)Objects.cloneObject (collection);
    clone.collectionListeners = null;

    return clone;
  }
  
  /**
   * Make an empty clone of this collection (ie a copy with none of the
   * original elements).
   */
  protected BasicMonitoredCollection emptyClone ()
    throws CloneNotSupportedException
  {
    try
    {
      BasicMonitoredCollection clone = (BasicMonitoredCollection)super.clone ();

      clone.collection = collection.getClass ().newInstance ();
      clone.collectionListeners = null;

      return clone;
    } catch (Exception ex)
    {
      throw new CloneNotSupportedException ("Failed to create nested collection: " + ex);
    }
  }

  // Events

  public synchronized void removeCollectionListener (CollectionListener l)
  {
    if (collectionListeners != null && collectionListeners.contains(l))
    {
      ArrayList v = (ArrayList)collectionListeners.clone();
      v.remove (l);
      collectionListeners = v;
    }
  }

  public synchronized void addCollectionListener (CollectionListener l)
  {
    ArrayList v =
      collectionListeners == null ? new ArrayList(2) : (ArrayList)collectionListeners.clone();
      
    if (!v.contains (l))
    {
      v.add (l);
      collectionListeners = v;
    }
  }

  protected void fireElementsAdded (CollectionEvent e)
  {
    if (collectionListeners != null)
    {
      ArrayList listeners = collectionListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
        ((CollectionListener) listeners.get (i)).elementsAdded (e);
    }
  }

  protected void fireElementsRemoved (CollectionEvent e)
  {
    if (collectionListeners != null)
    {
      ArrayList listeners = collectionListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
        ((CollectionListener) listeners.get (i)).elementsRemoved(e);
    }
  }
  
  public String toString ()
  {
    return collection.toString ();
  }

  /**
   * Wraps an iterator to catch remove () operations.
   */
  protected class CollectionIteratorWrapper implements Iterator
  {
    protected Iterator iterator;
    protected Object current;
    
    public CollectionIteratorWrapper ()
    {
      this (collection.iterator ());
    }
    
    public CollectionIteratorWrapper (Iterator iterator)
    {
      this.iterator = iterator;
    }
    
    public boolean hasNext ()
    {
      return iterator.hasNext ();
    }
    
    public Object next ()
    {
      current = iterator.next ();
      
      return current;
    }
    
    public void remove ()
    {
      iterator.remove ();
      
      if (current != null)
      {
        fireElementsRemoved (new CollectionEvent (BasicMonitoredCollection.this, current));
        
        current = null;
      }
    }
  }
}
