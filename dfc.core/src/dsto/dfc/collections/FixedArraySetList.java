package dsto.dfc.collections;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 * A wrapper around an array that makes it appear as both an
 * unmodifiable set and an unmodifiable list.
 *
 * @version $Revision$
 */
public class FixedArraySetList
  extends AbstractList implements List, Set
{
  Object [] values;

  /**
   * Create a set backed by an array of values.  The values must not
   * contain duplicates.
   */
  public FixedArraySetList (Object [] values)
  {
    this.values = values;
  }

  public Object get (int index)
  {
    return values [index];
  }

  public int indexOf (Object o)
  {
    for (int i = 0; i < values.length; i++)
    {
      if (values [i].equals (o))
        return i;
    }

    return -1;
  }

  public int lastIndexOf (Object o)
  {
    for (int i = values.length - 1; i >= 0; i--)
    {
      if (values [i].equals (o))
        return i;
    }

    return -1;
  }

  public ListIterator listIterator ()
  {
    return new UnmodifiableArraySetListIterator ();
  }

  public ListIterator listIterator (int index)
  {
    return new UnmodifiableArraySetListIterator (index);
  }

  public Iterator iterator ()
  {
    return listIterator ();
  }

  public int size ()
  {
    return values.length;
  }

  public boolean contains (Object o)
  {
    for (int i = 0; i < values.length; i++)
    {
      if (values [i].equals (o))
        return true;
    }

    return false;
  }

  public Object [] toArray ()
  {
    Object [] newArray = new Object [values.length];

    System.arraycopy (values, 0, newArray, 0, values.length);

    return newArray;
  }

  private final class UnmodifiableArraySetListIterator implements ListIterator
  {
    private int index;

    public UnmodifiableArraySetListIterator ()
    {
      this (0);
    }

    public UnmodifiableArraySetListIterator (int index)
    {
      this.index = index;
    }

    public boolean hasNext ()
    {
      return index < values.length;
    }

    public boolean hasPrevious ()
    {
      return index > 0 && values.length > 0;
    }

    public Object next ()
    {
      if (hasNext ())
        return values [index++];
      else
        throw new NoSuchElementException ();
    }
    
    public Object previous ()
    {
      if (hasPrevious ())
        return values [--index];
      else
        throw new NoSuchElementException ();
    }

    public int nextIndex ()
    {
      return index;
    }

    public int previousIndex ()
    {
      return index - 1;
    }

    public void remove ()
    {
      throw new UnsupportedOperationException ();
    }

    public void add (Object o)
    {
      throw new UnsupportedOperationException ();
    }

    public void set (Object o)
    {
      throw new UnsupportedOperationException ();
    }
  }
}
