package dsto.dfc.collections;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Basic implementation of MonitoredSet that acts as a wrapper around
 * a Set instance.
 *
 * @version $Revision$
 */
public class BasicMonitoredSet
  extends BasicMonitoredCollection implements MonitoredSet
{
  /**
   * Create a new instance with a backing HashSet.
   */
  public BasicMonitoredSet ()
  {
    this (new HashSet ());
  }

  /**
   * Create an instance from a set of entries. If entries is not a Set, it
   * is wrapped in one.
   */
  public BasicMonitoredSet (Collection entries)
  {
    super (entries instanceof Set ? entries : new HashSet (entries));
  }
  
  public BasicMonitoredSet (Set set)
  {
    super (set);
  }
}
