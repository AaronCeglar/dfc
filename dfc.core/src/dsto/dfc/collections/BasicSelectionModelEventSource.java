package dsto.dfc.collections;

import java.util.Collection;
import java.util.Vector;

/**
 * Basic class to maintain a set of SelectionModelListener's.
 *
 * @version $Revision$
 */
public class BasicSelectionModelEventSource
{
  private Object source;
  private transient Vector selectionModelListeners;

  public BasicSelectionModelEventSource ()
  {
    this.source = this;
  }

  public BasicSelectionModelEventSource (Object source)
  {
    this.source = source;
  }

  public synchronized void addSelectionModelListener(SelectionModelListener l)
  {
    Vector v = selectionModelListeners == null ? new Vector(2) : (Vector) selectionModelListeners.clone();
    if(!v.contains(l))
    {
      v.addElement(l);
      selectionModelListeners = v;
    }
  }

  public synchronized void removeSelectionModelListener(SelectionModelListener l)
  {
    if(selectionModelListeners != null && selectionModelListeners.contains(l))
    {
      Vector v = (Vector) selectionModelListeners.clone();
      v.removeElement(l);
      selectionModelListeners = v;
    }
  }

  public void fireElementsAdded (Collection elements)
  {
    fireElementsAdded (new SelectionModelEvent (source, elements));
  }

  public void fireElementAdded (Object element)
  {
    fireElementsAdded (new SelectionModelEvent (source, element));
  }

  public void fireElementsRemoved (Collection elements)
  {
    fireElementsRemoved (new SelectionModelEvent (source, elements));
  }

  public void fireElementRemoved (Object element)
  {
    fireElementsRemoved (new SelectionModelEvent (source, element));
  }

  private void fireElementsAdded (SelectionModelEvent e)
  {
    if (selectionModelListeners != null)
    {
      Vector listeners = selectionModelListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((SelectionModelListener) listeners.elementAt(i)).elementsAdded(e);
      }
    }
  }

  private void fireElementsRemoved(SelectionModelEvent e)
  {
    if(selectionModelListeners != null)
    {
      Vector listeners = selectionModelListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((SelectionModelListener) listeners.elementAt(i)).elementsRemoved(e);
      }
    }
  }
}
