package dsto.dfc.collections;

import java.util.Set;

/**
 * Defines a Set that may be monitored for changes via the
 * CollectionListener interface.
 *
 * @version $Revision$
 */
public interface MonitoredSet extends MonitoredCollection, Set
{
  // zip: this is a marker interface
}
