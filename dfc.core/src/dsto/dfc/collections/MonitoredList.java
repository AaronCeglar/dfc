package dsto.dfc.collections;

import java.util.List;

/**
 * Defines a List that may be monitored for changes via the
 * CollectionListener interface.
 *
 * @version $Revision$
 */
public interface MonitoredList extends MonitoredCollection, List
{
  // zip: this is a marker interface
}
