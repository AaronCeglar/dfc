package dsto.dfc.collections;

import java.util.Collection;
import java.util.Collections;
import java.util.EventObject;
import java.util.Iterator;

/**
 * @version $Revision$
 */
public class CollectionEvent extends EventObject
{
  private Collection elements;
  private int startIndex;
  private int endIndex;

  /**
   * Copy constructor useful for re-firing an event from a wrapped collection.
   *
   * @param source The source for this event.
   * @param srcEvent The wrapped event: all fields except source will beb
   * copied.
   */
  public CollectionEvent (Object source, CollectionEvent srcEvent)
  {
    this (source, srcEvent.getElements (),
          srcEvent.getStartIndex (), srcEvent.getEndIndex ());
  }

  public CollectionEvent (Object source, Collection elements)
  {
    super (source);
    this.elements = elements;
    this.startIndex = -1;
    this.endIndex = -1;
  }

  /**
   * Create an event when only one element has been added/removed.
   */
  public CollectionEvent (Object source, Object element)
  {
    this (source, Collections.singleton (element));
  }

  /**
   * Create an event where an indexed element has changed.
   */
  public CollectionEvent (Object source, Object element, int index)
  {
    this (source, Collections.singleton (element), index, index);
  }

  /**
   * Create an event where a number of indexed elements have changed.
   *
   * @param source The source of the event.
   * @param elements A collection of contiguous elements that changed.
   * @param startIndex The start index of the changed elements.
   * @param endIndex The end index of the changed elements (inclusive).
   */
  public CollectionEvent (Object source, Collection elements,
                          int startIndex, int endIndex)
  {
    super (source);

    this.elements = elements;
    this.startIndex = startIndex;
    this.endIndex = endIndex;
  }

  /**
   * Create an event where an indexed element has moved (eg see
   * {@link DfcMonitoredList}).
   */
  public CollectionEvent (Object source, Object element,
                          int startIndex, int endIndex)
  {
    this (source, Collections.singleton (element), startIndex, endIndex);
  }

  /**
   * Shortcut to get source as a Collection.
   */
  public Collection getSourceAsCollection ()
  {
    return (Collection)getSource ();
  }

  public int getElementCount ()
  {
    return elements.size ();
  }

  public Collection getElements ()
  {
    return elements;
  }

  public Iterator iterator ()
  {
    return elements.iterator ();
  }

  /**
   * The start index of the changed elements (if not coming from an
   * indexed collection, this will be -1).
   */
  public int getStartIndex ()
  {
    return startIndex;
  }

  /**
   * The end index of the changed elements (if not coming from an
   * indexed collection, this will be -1).
   */
  public int getEndIndex ()
  {
    return endIndex;
  }
}
