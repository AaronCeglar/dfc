package dsto.dfc.collections;

import java.util.HashMap;

/**
 * A map-like container that maps from classes to values. The search for
 * a value for a class first looks at the class, then it superclasses, and
 * then at its interfaces. If none of the specific classes and interfaces
 * match, then the mapping for java.lang.Object is returned.
 * 
 * @author mpp
 * @version $Revision$
 */
public class TypeMap
{
  private final HashMap map;
  
  /**
   * Constructor for TypeMap.
   */
  public TypeMap ()
  {
    this.map = new HashMap ();
  }

  public TypeMap (TypeMap copy)
  {
    this.map = new HashMap (copy.map);
  }
  
  public void put (Class type, Object value)
  {
    put (type.getName (), value);
  }
  
  public void put (String type, Object value)
  {
    map.put (type, value);
  }
  
  public Object get (Class type)
  {
    Object value = findByClass (type);
    
    if (value == null)
      value = findByInterface (type);
      
    if (value == null)
      value = map.get ("java.lang.Object");
      
    return value;
  }
  
  public void remove (Class type)
  {
    remove (type.getName ());
  }
  
  public void remove (String typeName)
  {
    map.remove (typeName);
  }
  
  private Object findByClass (Class type)
  {
    Object value;
    
    while (type != null && !type.equals (Object.class))
    {
      value = map.get (type.getName ());
      
      if (value != null)
        return value;
        
      type = type.getSuperclass ();
    }
    
    return null;
  }

  private Object findByInterface (Class type)
  {
    Object value;
    
    while (type != null && !type.equals (Object.class))
    {
      Class [] interfaces = type.getInterfaces ();
      
      for (int i = 0; i < interfaces.length; i++)
      {
        Class interfaceType = interfaces [i];

        value = map.get (interfaceType.getName ());
        
        if (value != null)
          return value;
      }
      
      type = type.getSuperclass ();
    }
    
    /** @todo search super interfaces */
    
    return null;
  }
}
