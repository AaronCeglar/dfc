package dsto.dfc.collections;

import dsto.dfc.util.Copyable;



/**
 *  Convenience data storage class for pairing objects.  
 *
 * @author   Robbe WT Stewart
 *
 * @version  $Revision$
 **/
public final class Pair
  implements Copyable 
{
  /** The first field of the pair. **/
  public Object first;

  /** The second field of the pair. **/
  public Object second;

  private int hash;


  
  public Pair(Object a, Object b)
  {
    first = a;
    second = b;
    hash = 0;
  }

  
  public Object clone()
    throws CloneNotSupportedException
  {
    Pair me = (Pair)super.clone();
    me.first = ((Copyable)first).clone();
    me.second = ((Copyable)second).clone();

    return me;
  }

  
  public int hashCode()
  {
    if (hash == 0)
    {
      int result = 17;
      result = 37 * result + (null == first ? 0 : first.hashCode());
      result = 37 * result + (null == second ? 0 : second.hashCode());

      hash = result;
    }
    return hash;    
  }


  public boolean equals(Object obj)
  {
    if (!(obj instanceof Pair))
      return false;

    Pair pair = (Pair)obj;

    return (null == first ? null == pair.first : first.equals(pair.first)) &&
      (null == second ? null == pair.second : second.equals(pair.second));
  }

}

// -- EOF
