package dsto.dfc.collections;

/**
 * A small footprint Map-like collection that provides basic put/get/remove
 * functionality for small maps that don't change much. Lookup cost and memory
 * usage scales linearly with entry count.
 * 
 * @author mpp
 * @version $Revision$
 */
public class MicroMap
{
  private Object [] keys;
  private Object [] values;
  private int count;

  /**
   * Create a map with a default initial capacity before resize.
   */  
  public MicroMap ()
  {
    this (8);
  }
  
  /**
   * Create a map with a given initial capacity before resize.
   */
  public MicroMap (int initialCapacity)
  {
    this.count = 0;
    this.keys = new Object [initialCapacity];
    this.values = new Object [initialCapacity];
  }
  
  /**
   * Remove all entries.
   */
  public void clear ()
  {
    this.count = 0;
  }
  
  /**
   * Add/overwrite a value.
   * 
   * @param key The value key.
   * @param value The value.
   * @return The old value for key, or null if none.
   */
  public Object put (Object key, Object value)
  {
    int index = findIndex (key);
    
    if (index == -1)
    {
      checkExpand ();
      
      keys [count] = key;
      values [count] = value;
      
      count++;
      
      return null;
    } else
    {
      Object oldValue = values [index];
      
      values [index] = value;
      
      return oldValue;
    }
  }

  /**
   * Remove a value.
   * 
   * @param key The value key.
   * @return The old value, or null if none.
   */
  public Object remove (Object key)
  {
    int index = findIndex (key);
    
    if (index != -1)
    {
      Object oldValue = values [index];
      
      count--;
      
      if (index < count)
      {
        System.arraycopy (keys, index + 1, keys, index, count - index);
        System.arraycopy (values, index + 1, values, index, count - index);
      }
      
      return oldValue;
    } else
    {
      return null;
    }
  }
  
  /**
   * Find the value with matchinf key.
   * 
   * @param key The key to search for.
   * @return The value, or null if not found.
   */
  public Object get (Object key)
  {
    int index = findIndex (key);
    
    if (index == -1)
      return null;
    else
      return values [index];
  }
  
  /**
   * Test if the map contains a key.
   */
  public boolean containsKey (Object key)
  {
    return findIndex (key) != -1;
  }

  /**
   * The number of entries in the map.
   */
  public int size ()
  {
    return count;
  }
  
  /**
   * Get the key for an index (0..size ()).
   */
  public Object getKey (int index)
  {
    return keys [index];
  }

  /**
   * Get the value for an index (0..size ()).
   */  
  public Object getValue (int index)
  {
    return values [index];
  }
  
  /**
   * Find the index for a key, or -1 if not found.
   */
  public int findIndex (Object key)
  {
    for (int i = 0; i < count; i++)
    {
      if (keys [i].equals (key))
        return i;
    }
    
    return -1;
  }
  
  /**
   * Check if arrays are full and, if so, expand by factor of 2.
   */
  private void checkExpand ()
  {
    if (count == keys.length)
    {
      int newCapacity = keys.length * 2;
      
      Object [] newKeys = new Object [newCapacity];
      Object [] newValues = new Object [newCapacity];
      
      System.arraycopy (keys, 0, newKeys, 0, keys.length);
      System.arraycopy (values, 0, newValues, 0, keys.length);
      
      this.keys = newKeys;
      this.values = newValues;
    }
  }
}
