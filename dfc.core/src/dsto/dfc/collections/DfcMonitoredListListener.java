package dsto.dfc.collections;

/**
 * Defines a listener on a DfcMonitoredList.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public interface DfcMonitoredListListener extends CollectionListener
{
  /**
   * Fired when an element in the list has moved (changed indexes).
   *
   * @param e The move event.  startIndex is the element's old index,
   * endIndex is the new index.
   */
  public void elementMoved (CollectionEvent e);
}



