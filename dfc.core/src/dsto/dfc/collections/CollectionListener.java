package dsto.dfc.collections;

import java.util.EventListener;

/**
 * Defines a listener for add/remove events on a collection.
 *
 * @version $Revision$
 */
public interface CollectionListener extends EventListener
{  
  public void elementsAdded (CollectionEvent e);

  public void elementsRemoved (CollectionEvent e);
}
