package dsto.dfc.collections;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Iterates through items in an array. Implements both java.util.Iterator and
 * the DFC {@link dsto.dfc.collections.Cursor}iterator interface.
 * 
 * @author Matthew Phillips
 */
public class ArrayIterator implements Iterator, Cursor
{
  private Object [] items;
  private int index;

  /**
   * Create a new instance.
   * 
   * @param items The items to iterate.
   */
  public ArrayIterator (Object [] items)
  {
    this (items, 0);
  }
  
  /**
   * Create a new instance.
   * 
   * @param items
   *          The items to iterate.
   * @param index
   *          The start index. May be "outside" the sequence (null iteration).
   */
  public ArrayIterator (Object [] items, int index)
  {
    this.items = items;
    this.index = index;
  }

  public boolean inside ()
  {
    return index < items.length && index >= 0;
  }
  
  public Object current () throws NoSuchElementException
  {
    if (inside ())
      return items [index];
    else
      throw new NoSuchElementException ("Cursor not inside array");
  }
  
  public int move (int units)
  {
    int distance;
    int newIndex = index + units;
    
    if (newIndex < 0)
    {
      distance = -index;
      
      index = -1;
    } else if (newIndex >= items.length)
    {
      distance = items.length - index - 1;
      
      index = items.length;
    } else
    {
      distance = units;
      
      index = newIndex;
    }
    
    return distance;
  }

  public boolean hasNext ()
  {
    return inside ();
  }

  public Object next ()
  {
    move (1);
    
    return current ();
  }

  /**
   * Not supported.
   */
  public void remove ()
  {
    throw new UnsupportedOperationException ();
  }
}
