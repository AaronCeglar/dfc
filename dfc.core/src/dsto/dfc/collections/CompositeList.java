package dsto.dfc.collections;

import java.util.AbstractList;
import java.util.List;

import dsto.dfc.util.Copyable;
import dsto.dfc.util.Disposable;
import dsto.dfc.util.Objects;

/**
 * A virtual list composed of a series of sub-List's laid end to end.
 * Tracks change events on any MonitoredList that is used as a sublist and
 * re-fires the appropriate event.<p>
 *
 * NOTE: Any sub-list's that may change size after being added must implement
 * MonitoredList or else this class will become out of sync.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class CompositeList
  extends AbstractList
  implements MonitoredList, CollectionListener, Disposable, Copyable
{
  protected List [] lists;
  protected int totalSize;
  protected BasicCollectionEventSource listeners =
    new BasicCollectionEventSource (this);

  public CompositeList ()
  {
    this.lists = new List [0];
    this.totalSize = 0;
  }

  public Object clone () throws CloneNotSupportedException
  {
    CompositeList copy = (CompositeList)super.clone ();

    copy.listeners = new BasicCollectionEventSource (copy);

    for (int i = 0; i < lists.length; i++)
      copy.lists [i] = (List)Objects.cloneObject (lists [i]);

    return copy;
  }

  public void dispose ()
  {
    if (lists != null)
    {
      for (int i = 0; i < lists.length; i++)
      {
        List list = lists [i];

        if (list instanceof MonitoredList)
          ((MonitoredList)list).removeCollectionListener (this);

        if (list instanceof Disposable)
          ((Disposable)list).dispose ();
      }

      lists = null;
      totalSize = -1;
    }
  }

  public void addList (List list)
  {
    addList (list, lists.length);
  }
  
  public void addList (List list, int index)
  {
    List [] newLists = new List [lists.length + 1];

    System.arraycopy (lists, 0, newLists, 0, index);
    newLists [index] = list;
    System.arraycopy (lists, index, newLists, index + 1, lists.length - index);

    lists = newLists;
    totalSize += list.size ();

    if (list instanceof MonitoredList)
      ((MonitoredList)list).addCollectionListener (this);

    if (!list.isEmpty ())
      listeners.fireElementsAdded (list, index, index + list.size () - 1);
  }

  public void removeList (int index)
  {
    List list = lists [index];
    List [] newLists = new List [lists.length - 1];

    System.arraycopy (lists, 0, newLists, 0, index);
    System.arraycopy (lists, index + 1, newLists, index, lists.length - index - 1);

    lists = newLists;
    totalSize -= list.size ();

    if (list instanceof MonitoredList)
      ((MonitoredList)list).removeCollectionListener (this);

    if (!list.isEmpty ())
      listeners.fireElementsRemoved (list, index, index + list.size () - 1);
  }

  public int indexOfList (List list)
  {
    for (int i = 0; i < lists.length; i++)
    {
      if (lists [i] == list)
        return i;
    }

    return -1;
  }

  public int absoluteIndex (List list, int index)
  {
    int absIndex = index;

    for (int i = 0; i < lists.length; i++)
    {
      List l = lists [i];

      if (l == list)
        return absIndex;

      absIndex += l.size ();
    }

    return absIndex;
  }

  // List interface

  public Object get (int index)
  {
    for (int i = 0; i < lists.length; i++)
    {
      List list = lists [i];

      if (index < list.size ())
        return list.get (index);
      else
        index -= list.size ();
    }

    throw new IndexOutOfBoundsException ();
  }

  public int size ()
  {
    return totalSize;
  }

  public void addCollectionListener (CollectionListener l)
  {
    listeners.addCollectionListener (l);
  }

  public void removeCollectionListener (CollectionListener l)
  {
    listeners.removeCollectionListener (l);
  }

  // CollectionListener interface

  public void elementsAdded (CollectionEvent e)
  {
    int startIndex = absoluteIndex ((List)e.getSource (), e.getStartIndex ());
    int endIndex = startIndex + (e.getEndIndex () - e.getStartIndex ());

    totalSize += e.getElementCount ();
    
    listeners.fireElementsAdded (e.getElements (), startIndex, endIndex);
  }

  public void elementsRemoved (CollectionEvent e)
  {
    int startIndex = absoluteIndex ((List)e.getSource (), e.getStartIndex ());
    int endIndex = startIndex + (e.getEndIndex () - e.getStartIndex ());

    totalSize -= e.getElementCount ();
    
    listeners.fireElementsRemoved (e.getElements (), startIndex, endIndex);
  }
}
