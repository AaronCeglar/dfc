package dsto.dfc.collections;

import java.util.EventListener;

/**
 * Defines a listener to a SelectionModel.
 *
 * @version $Revision$
 */
public interface SelectionModelListener extends EventListener
{
  public void elementsAdded (SelectionModelEvent e);

  public void elementsRemoved (SelectionModelEvent e);
} 
