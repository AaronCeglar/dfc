package dsto.dfc.collections;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * A set that counts add's and remove's on values.  If a value is
 * added N times, then it must be removed N times to be deleted from
 * the set.
 *
 * @version $Revision$
 */
public class CountedSet implements MonitoredSet
{
  private static final Integer ONE = new Integer (1);
  private static final Integer TWO = new Integer (2);
  private static final Integer THREE = new Integer (3);
  private static final Integer FOUR = new Integer (4);
  private static final Integer FIVE = new Integer (5);

  private Map mapValueToCount = new HashMap ();
  private BasicCollectionEventSource listeners =
    new BasicCollectionEventSource (this);

  public CountedSet ()
  {
    // zip
  }

  /**
   * Constructor to allow users to get CountedSet to use their Map
   * implementation. The data must be structured with the map key being the
   * data-object being stored, and the map value being an Integer object of
   * the appropriate value (eg. Integer(3) would indicate that the map knows
   * about three of the corresponding data-objects).
   *
   * <p> The reason behind this constructor is so that users can implement
   * Maps with they own comparator implementations rather than relying on
   * HashMap's and TreeMap's implementations.
   */
  public CountedSet (Map dataMap)
  {
    mapValueToCount = dataMap;
  }

  public CountedSet (Collection data)
  {
    addAll (data);
  }

  // MonitoredSet interface

  public void addCollectionListener (CollectionListener l)
  {
    listeners.addCollectionListener (l);
  }

  public void removeCollectionListener (CollectionListener l)
  {
    listeners.removeCollectionListener (l);
  }

  // Set interface

  public int size ()
  {
    return mapValueToCount.size ();
  }

  public boolean isEmpty ()
  {
    return mapValueToCount.isEmpty ();
  }

  public boolean contains (Object o)
  {
    return mapValueToCount.containsKey (o);
  }

  public Iterator iterator ()
  {
    return mapValueToCount.keySet ().iterator ();
  }

  public Object [] toArray()
  {
   return  mapValueToCount.keySet ().toArray ();
  }

  public Object [] toArray (Object [] array)
  {
    return mapValueToCount.keySet ().toArray (array);
  }

  public boolean add (Object o)
  {
    Integer count = (Integer)mapValueToCount.get (o);

    if (count != null)
    {
      mapValueToCount.put (o, increment (count));

      return false;
    } else
    {
      mapValueToCount.put (o, ONE);

      listeners.fireElementAdded (o);

      return true;
    }
  }

  public boolean remove (Object o)
  {
    Integer count = (Integer)mapValueToCount.get (o);

    if (count != null)
    {
      if (count.intValue () == 1)
      {
        mapValueToCount.remove (o);

        listeners.fireElementRemoved (o);

        return true;
      } else
      {
        mapValueToCount.put (o, decrement (count));

        return false;
      }
    } else
    {
      // not in set
      return false;
    }
  }

  public boolean containsAll (Collection c)
  {
    return mapValueToCount.keySet ().containsAll (c);
  }

  public boolean addAll (Collection c)
  {
    boolean changed = false;

    for (Iterator i = c.iterator (); i.hasNext (); )
    {
      if (add (i.next ()))
        changed = true;
    }

    return changed;
  }

  public boolean retainAll (Collection c)
  {
    boolean changed = false;

    for (Iterator i = iterator (); i.hasNext (); )
    {
      Object value = i.next ();

      if (!c.contains (value))
      {
        if (remove (value))
          changed = true;
      }
    }

    return changed;
  }

  public boolean removeAll (Collection c)
  {
    boolean changed = false;

    for (Iterator i = c.iterator (); i.hasNext (); )
    {
      if (remove (i.next ()))
        changed = true;
    }

    return changed;
  }

  /**
   * Clears the set regardless of value counts.
   */
  public void clear ()
  {
    Collection removedValues = mapValueToCount.keySet ();

    mapValueToCount = new HashMap ();

    listeners.fireElementsRemoved (removedValues);
  }

  @Override
  public boolean equals (Object o)
  {
    if (o instanceof Set)
      return mapValueToCount.keySet ().equals (o);
    else
      return super.equals (o);
  }

  @Override
  public int hashCode ()
  {
    return mapValueToCount.keySet ().hashCode ();
  }

  @Override
  public String toString ()
  {
    return mapValueToCount.keySet ().toString ();
  }

  /**
   * Return an Integer plus 1.  Uses predefined instances for 0-5.
   */
  protected static Integer increment (Integer i)
  {
    switch (i.intValue ())
    {
      case 0: return ONE;
      case 1: return TWO;
      case 2: return THREE;
      case 3: return FOUR;
      case 4: return FIVE;
      default: return new Integer (i.intValue () + 1);
    }
  }

  /**
   * Return an Integer minus 1.  Uses predefined instances for 0-5.
   */
  protected static Integer decrement (Integer i)
  {
    switch (i.intValue ())
    {
      case 2: return ONE;
      case 3: return TWO;
      case 4: return THREE;
      case 5: return FOUR;
      case 6: return FIVE;
      default: return new Integer (i.intValue () - 1);
    }
  }
}
