package dsto.dfc.collections;

import java.io.Serializable;

import dsto.dfc.util.Copyable;

public class EmptyMonitoredCollection extends EmptyCollection
    implements
      MonitoredCollection,
      Copyable,
      Serializable
{
  public static final EmptyMonitoredCollection EMPTY_INSTANCE = new EmptyMonitoredCollection ();

  private EmptyMonitoredCollection ()
  {
    // zip
  }

  public void addCollectionListener (CollectionListener l)
  {
    //zip
  }

  public void removeCollectionListener (CollectionListener l)
  {
    // zip
  }

  public Object clone ()
  {
    return this;
  }

  private Object readResolve ()
  {
    return EMPTY_INSTANCE;
  }
}