package dsto.dfc.collections;

import java.io.Serializable;
import java.util.Collection;
import java.util.Vector;

/**
 * Basic class to maintain a set of CollectionListener's.
 *
 * @version $Revision$
 */
public class BasicCollectionEventSource implements Serializable
{
  private static final long serialVersionUID = -2287356568541419875L;

  private Object source;
  private transient Vector collectionListeners;

  public BasicCollectionEventSource ()
  {
    this.source = this;
  }

  public BasicCollectionEventSource (Object source)
  {
    this.source = source;
  }

  protected Object clone () throws CloneNotSupportedException
  {
    BasicCollectionEventSource copy = (BasicCollectionEventSource)super.clone ();
    
    copy.collectionListeners = null;
    
    return copy;
  }
  
  public synchronized void addCollectionListener (CollectionListener l)
  {
    if (l == null)
      throw new NullPointerException ("Null listener");
    
    Vector v = collectionListeners == null ? new Vector(2) : (Vector) collectionListeners.clone();
    if(!v.contains(l))
    {
      v.addElement(l);
      collectionListeners = v;
    }
  }

  public synchronized void removeCollectionListener(CollectionListener l)
  {
    if (collectionListeners != null && collectionListeners.contains(l))
    {
      Vector v = (Vector) collectionListeners.clone();
      v.removeElement(l);

      if (v.isEmpty ())
        collectionListeners = null;
      else
        collectionListeners = v;
    }
  }

  public void fireElementsAdded (Collection elements)
  {
    if (collectionListeners != null)
      fireElementsAdded (new CollectionEvent (source, elements));
  }

  public void fireElementAdded (Object element)
  {
    if (collectionListeners != null)
      fireElementsAdded (new CollectionEvent (source, element));
  }

  public void fireElementsAdded (Collection elements,
                                 int startIndex, int endIndex)
  {
    if (collectionListeners != null)
      fireElementsAdded (new CollectionEvent (source, elements,
                                              startIndex, endIndex));
  }

  public void fireElementAdded (Object element, int index)
  {
    if (collectionListeners != null)
      fireElementsAdded (new CollectionEvent (source, element, index));
  }

  public void fireElementsRemoved (Collection elements)
  {
    if (collectionListeners != null)
      fireElementsRemoved (new CollectionEvent (source, elements));
  }

  public void fireElementRemoved (Object element)
  {
    if (collectionListeners != null)
      fireElementsRemoved (new CollectionEvent (source, element));
  }

  public void fireElementsRemoved (Collection elements,
                                   int startIndex, int endIndex)
  {
    if (collectionListeners != null)
      fireElementsRemoved (new CollectionEvent (source, elements,
                                                startIndex, endIndex));
  }

  public void fireElementRemoved (Object element, int index)
  {
    if (collectionListeners != null)
      fireElementsRemoved (new CollectionEvent (source, element, index));
  }

  public void fireElementsAdded (CollectionEvent e)
  {
    if (collectionListeners != null)
    {
      Vector listeners = collectionListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((CollectionListener) listeners.elementAt(i)).elementsAdded(e);
      }
    }
  }

  public void fireElementsRemoved (CollectionEvent e)
  {
    if(collectionListeners != null)
    {
      Vector listeners = collectionListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((CollectionListener) listeners.elementAt(i)).elementsRemoved(e);
      }
    }
  }
}
