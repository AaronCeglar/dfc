package dsto.dfc.collections;

import java.util.Enumeration;
import java.util.Iterator;

/**
 * Adapts an java.util.Iterator to the java.util.Enumeration interface.
 *
 * @see dsto.dfc.collections.EnumerationIterator
 * 
 * @author Matthew Phillips
 */
public class IteratorEnumeration implements Enumeration
{
  private Iterator iterator;

  public IteratorEnumeration (Iterator iterator)
  {
    this.iterator = iterator;
  }

  public boolean hasMoreElements ()
  {
    return iterator.hasNext ();
  }

  public Object nextElement ()
  {
    return iterator.next ();
  }
}
