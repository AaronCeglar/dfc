package dsto.dfc.collections;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import dsto.dfc.util.Disposable;

/**
 * A superset of a number of subsets.<p>
 * 
 * NOTE: This class will currently not serialize correctly.
 * 
 * @author mpp
 * @version $Revision$
 */
public class SuperSet
  extends BasicMonitoredSet implements CollectionListener, Disposable
{
  private HashSet subsets = new HashSet ();
  
  /**
   * Constructor for SuperSet.
   */
  public SuperSet ()
  {
    super ();
    
    this.subsets = new HashSet ();
  }
  
  public SuperSet (Set set)
  {
    this ();
    
    addSubset (set);
  }
  
  public void dispose ()
  {
    if (subsets != null)
    {
      for (Iterator i = subsets.iterator (); i.hasNext (); )
      {
        Object set = i.next ();
        
        if (set instanceof MonitoredCollection)
          ((MonitoredCollection)set).removeCollectionListener (this);
      }
      
      subsets = null;
    }
  }
  
  public void addSubset (Set subset)
  {
    if (subsets.add (subset))
    {
      addAll (subset);
      
      if (subset instanceof MonitoredCollection)
        ((MonitoredCollection)subset).addCollectionListener (this);
    }      
  }
  
  /**
   * NOTE: this may be significantly slower than addSubset ()
   */
  public void removeSubset (Set subset)
  {
    if (!subsets.remove (subset))
      return;

    if (subset instanceof MonitoredCollection)
      ((MonitoredCollection)subset).removeCollectionListener (this);
        
    removeAllSubsetElements (subset, subset);
  }
  
  public void elementsAdded (CollectionEvent e)
  {
    addAll (e.getElements ());
  }

  public void elementsRemoved (CollectionEvent e)
  {
    removeAllSubsetElements ((Set)e.getSource (), e.getElements ());
  }
  
  private void removeAllSubsetElements (Set subset, Collection elements)
  {
    for (Iterator i = elements.iterator (); i.hasNext (); )
    {
      Object entry = i.next ();
      boolean inOtherSet = false;
      
      for (Iterator j = subsets.iterator (); j.hasNext () && !inOtherSet; )
      {
        Set s = (Set)j.next ();
        
        if (s != subset && s.contains (entry))
          inOtherSet = true;
      }
      
      if (!inOtherSet)
        super.remove (entry);
    }
  }
  
  /**
   * Remove an element from all subsets. If the subsets are not
   * MonitoredCollection's, this is will not correctly fire an element removed
   * event.
   */
  public boolean remove (Object element)
  {
    boolean removed = false;
    
    for (Iterator i = subsets.iterator (); i.hasNext (); )
    {
      Set set = (Set)i.next ();
      
      if (set.remove (element))
        removed = true;
    }
    
    if (removed)
      super.remove (element);

    return removed;
  }

  /**
   * Remove all elements from each subset individually. If the subsets are not
   * MonitoredCollection's, this is will not correctly fire an element removed
   * event.
   */
  public boolean removeAll (Collection elements)
  {
    boolean removed = false;
    
    for (Iterator i = elements.iterator (); i.hasNext (); )
    {
      if (remove (i.next ()))
        removed = true;
    }
    
    return removed;
  }
}
