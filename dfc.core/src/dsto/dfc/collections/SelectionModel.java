package dsto.dfc.collections;

import java.util.Collection;
import java.util.Iterator;

/**
 * Defines a model of selected elements.
 *
 * @version $Revision$
 */
public interface SelectionModel
{
  public boolean isSelected (Object element);

  public void select (Object element);

  public void deselect (Object element);

  public void selectAll (Collection elements);

  public void deselectAll (Collection elements);

  public void deselectAll ();

  public Iterator iterator ();

  public void addSelectionModelListener (SelectionModelListener l);

  public void removeSelectionModelListener (SelectionModelListener l);
}
