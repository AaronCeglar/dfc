package dsto.dfc.collections;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

/**
 * Support for PropertySet's.
 *
 * @version $Revision$
 */
public final class PropertySets
{
  private static final String INDENT_1 = "  ";
  private static final String INDENT_2 = "    ";

  private PropertySets ()
  {
    // zip
  }

  /**
   * Get a value from a property set, returning a default value if
   * none exists in the set.
   */
  public static Object getPropertyValue (PropertySet set,
                                         String propertyName,
                                         Object defaultValue)
  {
    if (set.hasProperty (propertyName))
      return set.getPropertyValue (propertyName);
    else
      return defaultValue;
  }

  /**
   * Get a value from a property set, returning the default value in a
   * second set if none exists in first the set (ie second set
   * contains default values for the first).
   */
  public static Object getPropertyValue (PropertySet set1,
                                         PropertySet set2,
                                         String propertyName)
  {
    if (set1.hasProperty (propertyName))
      return set1.getPropertyValue (propertyName);
    else
      return set2.getPropertyValue (propertyName);
  }

  /**
   * Return an instance of a property set that cannot be modified.
   */
  public static PropertySet unmodifiablePropertySet (PropertySet propertySet)
  {
    if (propertySet instanceof UnmodifiablePropertySet)
      return propertySet;
    else
      return new UnmodifiablePropertySet (propertySet);
  }

  public static void xmlOutput (OutputStream stream, PropertySet set)
  {
    xmlOutput (new PrintWriter (stream), set); 
  }

  /**
   * Print a PropertySet in XML form.  No quoting of values is done,
   * so XML is not guaranteed to be well-formed.
   */
  public static void xmlOutput (PrintWriter writer, PropertySet set)
  {
    Iterator i = set.getPropertyNames ().iterator ();

    writer.println ("<property_set>");

    while (i.hasNext ())
    {
      String propertyName = (String)i.next ();

      writer.print (INDENT_1);
      writer.println ("<property name='" + propertyName + "'>");

      // output values
      List values = set.getPropertyValues (propertyName);

      for (int j = 0; j < values.size (); j++)
      {
        Object value = values.get (j);
        String valueType = value.getClass ().getName ();

        // remove "class " from start of class name
        valueType = valueType.substring (valueType.indexOf (" ") + 1);

        writer.print (INDENT_2);
        writer.print ("<value type='" + valueType + "'>");
        writer.print (value.toString ());
        writer.println ("</value>");
      }

      writer.print (INDENT_1);
      writer.println ("</property>");
    }

    writer.println ("</property_set>");
    writer.flush ();
  }
}
