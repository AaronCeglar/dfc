package dsto.dfc.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import dsto.dfc.util.Copyable;

/**
 * Basic implementation of MonitoredList that acts as a wrapper around
 * a List instance.
 *
 * @version $Revision$
 */
public class BasicMonitoredList
  extends BasicMonitoredCollection implements MonitoredList, Copyable
{
  private static final long serialVersionUID = 5437245980617046795L;
	
  /**
   * Default constructor: uses an ArrayList as backing list.
   */
  public BasicMonitoredList ()
  {
    this (new ArrayList ());
  }

  public BasicMonitoredList (List list)
  {
    super (list);
  }

  /**
   * Get the list backing this monitored list.  Be very careful in
   * modifying this list, since any changes will not be seen by
   * listeners.
   */
  public List getBackingList ()
  {
    return (List)collection;
  }

  // Collection interface

  public boolean add (Object element)
  {
    int index = collection.size ();
    boolean added = collection.add (element);

    if (added)
      fireElementsAdded (new CollectionEvent (this, element, index));

    return added;
  }

  public boolean remove (Object element)
  {
    List list = getBackingList ();
    
    int index = list.indexOf (element);

    if (index != -1 && list.remove (index) != null)
    {
      fireElementsRemoved (new CollectionEvent (this, element, index));

      return true;
    } else
    {
      return false;
    }
  }

  public boolean addAll (Collection elements)
  {
    int index = collection.size ();
    boolean added = collection.addAll (elements);

    if (added)
      fireElementsAdded
        (new CollectionEvent (this, elements, index, index + elements.size () - 1));

    return added;
  }

  public boolean removeAll (Collection elements)
  {
    boolean changed = false;

    for (Iterator i = elements.iterator (); i.hasNext (); )
    {
      if (remove (i.next ()))
        changed = true;
    }

    return changed;
  }

  public void clear ()
  {
    List list = getBackingList ();
    
    if (list.size () > 0)
    {
      List origList = list;

      // make it appear that collection has been cleared
      collection = Collections.EMPTY_LIST;

      fireElementsRemoved (new CollectionEvent (this, origList, 0, origList.size () - 1));

      // actually clear collection now and restore reference
      origList.clear ();
      collection = origList;
    }
  }

  // List interface

  public Object get (int index)
  {
    return getBackingList ().get (index);
  }

  public Object set (int index, Object element)
  {
    List list = getBackingList ();
    
    Object oldElement = list.remove (index);

    fireElementsRemoved (new CollectionEvent (this, oldElement, index));

    list.add (index, element);

    fireElementsAdded (new CollectionEvent (this, element, index));

    return oldElement;
  }

  public void add (int index, Object object)
  {
    getBackingList ().add (index, object);

    fireElementsAdded (new CollectionEvent (this, object, index));
  }

  public boolean addAll (int index, Collection elements)
  {
    if (getBackingList ().addAll (index, elements))
    {
      fireElementsAdded (new CollectionEvent (this, elements, index));

      return true;
    } else
    {
      return false;
    }
  }

  public Object remove (int index)
  {
    Object oldElement = getBackingList ().remove (index);

    fireElementsRemoved (new CollectionEvent (this, oldElement, index));

    return oldElement;
  }

  public int indexOf (Object object)
  {
    return getBackingList ().indexOf (object);
  }

  public int lastIndexOf (Object object)
  {
    return getBackingList ().lastIndexOf (object);
  }

  public ListIterator listIterator ()
  {
    return new ListIteratorWrapper (0);
  }

  public ListIterator listIterator (int index)
  {
    return new ListIteratorWrapper (index);
  }

  public List subList (int i1, int i2)
  {
    return getBackingList ().subList (i1, i2);
  }
  
  protected class ListIteratorWrapper
    extends CollectionIteratorWrapper implements ListIterator
  {
    public ListIteratorWrapper (int index)
    {
      super (getBackingList ().listIterator (index));
    }

    public void add (Object o)
    {
      ((ListIterator)iterator).add (o);
    }
    
    public boolean hasPrevious ()
    {
      return ((ListIterator)iterator).hasPrevious ();
    }
    
    public int nextIndex ()
    {
      return ((ListIterator)iterator).nextIndex ();
    }
    
    public Object previous ()
    {
      current = ((ListIterator)iterator).previous ();
      
      return current;
    }
    
    public int previousIndex ()
    {
      return ((ListIterator)iterator).previousIndex ();
    }
    
    public void set (Object o)
    {
      // XXX this may be bogus (untested)
      fireElementsRemoved (new CollectionEvent (BasicMonitoredList.this, current, nextIndex ()));

      ((ListIterator)iterator).set (o);

      fireElementsAdded (new CollectionEvent (BasicMonitoredList.this, o, nextIndex ()));
    
    }
  }
}
