package dsto.dfc.collections;

import java.util.Collection;

/**
 * Defines a Collection that may be monitored for changes via the
 * CollectionListener interface.
 *
 * @version $Revision$
 */
public interface MonitoredCollection extends Collection
{
  public void addCollectionListener (CollectionListener l);

  public void removeCollectionListener (CollectionListener l);
}
