package dsto.dfc.collections;

import java.util.Collection;
import java.util.Collections;
import java.util.EventObject;

/**
 * The event fired by SelectionModel's to indicate the selection has
 * changed.
 *
 * @version $Revision$
 */
public class SelectionModelEvent extends EventObject
{
  private Collection elements;

  public SelectionModelEvent (SelectionModel source, Collection elements)
  {
    super (source);
    this.elements = elements;
  }

  /**
   * Create an event when only one element has been added/removed.
   */
  public SelectionModelEvent (Object source, Object element)
  {
    super (source);
    this.elements = Collections.singleton (element);
  }

  public int getElementCount ()
  {
    return elements.size ();
  }

  public Collection getElements ()
  {
    return elements;
  }
}
