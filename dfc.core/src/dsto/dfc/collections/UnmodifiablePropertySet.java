package dsto.dfc.collections;

import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * An unmodifiable wrapper around a PropertySet instance.
 *
 * @version $Revision$
 */
public class UnmodifiablePropertySet
  implements PropertySet, Serializable, Cloneable
{
  private PropertySet propertySet;

  public UnmodifiablePropertySet (PropertySet propertySet)
  {
    this.propertySet = propertySet;
  }

  public Set getPropertyNames ()
  {
    return propertySet.getPropertyNames ();
  }
  
  public List getPropertyValues (String propertyName)
  {
    return propertySet.getPropertyValues (propertyName);
  }

  public Object getPropertyValue (String propertyName)
  {
    return propertySet.getPropertyValue (propertyName);
  }
  
  public void setPropertyValues (String propertyName, List values)
   throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException ("property set is immutable");
  }

  public void setPropertyValue (String propertyName, Object value)
   throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException ("property set is immutable");
  }
  
  public void removeProperty (String propertyName)
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException ("property set is immutable");
  }

  public boolean removePropertyValue (String propertyName, Object value)
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException ("property set is immutable");
  }

  public boolean hasProperty (String propertyName)
  {
    return propertySet.hasProperty (propertyName);
  }
  
  public void removeAll () throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException ("property set is immutable");
  }
  
  public void addPropertyValue (String propertyName, Object value)
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException ("property set is immutable");
  }

  public void addPropertyValues (String propertyName, List values)
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException ("property set is immutable");
  }

  public boolean isModifiable ()
  {
    return false;
  }

  public Object clone () throws CloneNotSupportedException
  {
    return propertySet.clone ();
  }

  public void addPropertyChangeListener (PropertyChangeListener l)
  {
    propertySet.addPropertyChangeListener (l);
  }

  public void removePropertyChangeListener (PropertyChangeListener l)
  {
    propertySet.removePropertyChangeListener (l);
  }
}
