package dsto.dfc.collections;

import java.util.Map;

/**
 * Defines a slightly frankensteinish Map that may be monitored for
 * changes via the CollectionListener interface. Map's are not
 * Collection's, but the changes made to them can be thought of as
 * adding/removing Map.Entry's. Thus, the elements in
 * CollectionEvent.getElements () are of type Map.Entry.
 *
 * @version $Revision$
 */
public interface MonitoredMap extends Map
{
  public void addCollectionListener (CollectionListener l);

  public void removeCollectionListener (CollectionListener l);
}
