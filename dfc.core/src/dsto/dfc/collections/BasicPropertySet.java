package dsto.dfc.collections;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * An implementation of PropertySet using HashMap and ArrayList.
 *
 * @version $Revision$
 */
public class BasicPropertySet implements PropertySet, Serializable, Cloneable
{
  private HashMap properties = new HashMap ();
  private transient PropertyChangeSupport propertyListeners;

  /**
   * @roseuid 373BAFC70283
   */
  public Set getPropertyNames ()
  {
    return Collections.unmodifiableSet (properties.keySet ());
  }

  /**
   * @roseuid 373BAFC702B5
   */
  public List getPropertyValues (String propertyName)
  {
    Object value = properties.get (propertyName);

    if (value != null)
    {
      if (value instanceof List)
        return Collections.unmodifiableList ((List)value);
      else
      {
        List values = new ArrayList ();
        values.add (value);

        return values;
      }
    } else
      return null;
  }

  /**
   * @roseuid 373BAFC70337
   */
  public Object getPropertyValue (String propertyName)
  {
    Object value = properties.get (propertyName);

    if (value != null)
    {
      if (value instanceof List)
        return ((List)value).get (0);
      else
        return value;
    } else
      return null;
  }

  public void addPropertyValues (String propertyName, List values)
  {
    Object value = properties.get (propertyName);

    if (value == null)
    {
      // no existing values

      List newValues;

      // if only one value in the new values, store it only.
      if (values.size () == 1 && !(values.get (0) instanceof List))
      {
        properties.put (propertyName, values.get (0));
        newValues = new ArrayList (1);
        newValues.add (values.get (0));
      } else
      {
        newValues = new ArrayList (values);
        properties.put (propertyName, newValues);
      }

      firePropertyChange (propertyName, null, newValues);
    } else
    {
      // values exist

      ArrayList newValues;
      ArrayList oldValues;

      if (value instanceof List)
      {
        // values exist and are already in list form, add new values.

        newValues = (ArrayList)value;
        oldValues = new ArrayList (newValues);

        newValues.addAll (values);
      } else
      {
        // a single value exists, convert to list form and add old
        // value, new values.

        oldValues = new ArrayList (1);
        oldValues.add (value);

        newValues = new ArrayList ();
        newValues.add (value);
        newValues.addAll (values);

        properties.put (propertyName, newValues);
      }

      firePropertyChange (propertyName, oldValues, newValues);
    }
  }

  public void setPropertyValues (String propertyName, List values)
  {
    // read old value
    ArrayList oldValues = getValueAsList (propertyName);
    ArrayList newValues;

    // if only one value (which is not a list, since this would cause
    // confusion), then just store the value.
    if (values.size () == 1 && !(values.get (0) instanceof List))
    {
      newValues = new ArrayList (1);
      newValues.add (values.get (0));

      properties.put (propertyName, values.get (0));
    } else
    {
      newValues = new ArrayList (values);
      properties.put (propertyName, newValues);
    }

    firePropertyChange (propertyName, oldValues, newValues);
  }

  public void setPropertyValue (String propertyName, Object value)
  {
    // read old value
    ArrayList oldValues = getValueAsList (propertyName);
    ArrayList newValues = new ArrayList (1);

    newValues.add (value);

    // if value is not a list (since this would cause confusion), then
    // just store the value.
    if (!(value instanceof List))
      properties.put (propertyName, value);
    else
      properties.put (propertyName, newValues);

    firePropertyChange (propertyName, oldValues, newValues);
  }

  /**
   * @roseuid 373BAFC8007B
   */
  public void removeProperty (String propertyName)
  {
    ArrayList oldValues = getValueAsList (propertyName);

    properties.remove (propertyName);

    firePropertyChange (propertyName, oldValues, null);
  }

  public boolean removePropertyValue (String propertyName, Object value)
    throws UnsupportedOperationException
  {
    Object oldValue = properties.get (propertyName);
    boolean result = false;

    if (oldValue != null)
    {
      if (oldValue instanceof List)
      {
        List oldValues = (List)oldValue;

        if (oldValues.contains (value))
        {
          result = true;

          ArrayList newValues = new ArrayList (oldValues);
          newValues.remove (value);

          if (newValues.size () == 1)
            properties.put (propertyName, newValues.get (0));
          else
            properties.put (propertyName, newValues);

          firePropertyChange (propertyName, oldValues, newValues);
        }
      } else
      {
        // it's a single value, check if it's the same as the remove value
        if (oldValue.equals (value))
        {
          properties.remove (propertyName);

          result = true;

          firePropertyChange (propertyName,
                              Collections.singletonList (oldValue), null);
        }
      }
    }

    return result;
  }

  /**
     @roseuid 373BAFC80108
   */
  public boolean hasProperty (String propertyName)
  {
    return properties.get (propertyName) != null;
  }

  /**
   * @roseuid 373BAFC8018A
   */
  public void removeAll ()
  {
    Iterator i = properties.keySet ().iterator ();

    while (i.hasNext ())
      removeProperty ((String)i.next ());
  }

  /**
   * @roseuid 373BAFC801BC
   */
  public void addPropertyValue (String propertyName, Object newValue)
  {
    Object value = properties.get (propertyName);

    // if no exisiting values and new value is not a List, then add
    // directly.
    if (value == null && !(newValue instanceof List))
    {
      properties.put (propertyName, newValue);

      ArrayList newValues = new ArrayList (1);
      newValues.add (newValue);

      firePropertyChange (propertyName, null, newValues);
    } else
    {
      ArrayList oldValues;
      ArrayList newValues;

      if (value instanceof List)
      {
        newValues = (ArrayList)value;
        oldValues = new ArrayList (newValues);

        // values exist and are already in list form, add new value.
        newValues.add (newValue);
      } else
      {
        // a single value exists, convert to list form and add old
        // value, new values.
        oldValues = new ArrayList (1);
        oldValues.add (value);
        newValues = new ArrayList ();

        newValues.add (value);
        newValues.add (newValue);

        properties.put (propertyName, newValues);
      }

      firePropertyChange (propertyName, oldValues, newValues);
    }
  }

  public boolean isModifiable ()
  {
    return true;
  }

  public void addPropertyChangeListener (PropertyChangeListener l)
  {
    if (propertyListeners == null)
      propertyListeners = new PropertyChangeSupport (this);

    propertyListeners.addPropertyChangeListener (l);
  }

  public void removePropertyChangeListener (PropertyChangeListener l)
  {
    if (propertyListeners != null)
      propertyListeners.removePropertyChangeListener (l);
  }

  public Object clone () throws CloneNotSupportedException
  {
    BasicPropertySet newObject = (BasicPropertySet)super.clone ();

    newObject.propertyListeners = null;
    newObject.properties = (HashMap)properties.clone ();

    return newObject;
  }

  protected void firePropertyChange (String propertyName,
                                     Object oldValue, Object newValue)
  {
    if (propertyListeners != null)
      propertyListeners.firePropertyChange (propertyName, oldValue, newValue);
  }

  /**
   * Get the value of a property as a List, handling the case where a
   * single value for property has been stored directly.
   */
  protected ArrayList getValueAsList (String propertyName)
  {
    Object value = properties.get (propertyName);

    if (value != null && !(value instanceof ArrayList))
    {
      ArrayList values = new ArrayList (1);
      values.add (value);

      return values;
    } else
      return (ArrayList)value;
  }
}
