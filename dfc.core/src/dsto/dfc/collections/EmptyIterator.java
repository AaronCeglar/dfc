package dsto.dfc.collections;

import java.util.Iterator;
import java.util.NoSuchElementException;

import dsto.dfc.util.Singleton;

public class EmptyIterator implements Iterator, Singleton
{
  public static final EmptyIterator INSTANCE = new EmptyIterator();

  private EmptyIterator ()
  {
    // zip
  }

  public boolean hasNext()
  {
    return false;
  }

  public Object next()
  {
    throw new NoSuchElementException();
  }

  public void remove()
  {
    throw new UnsupportedOperationException();
  }
  
  public static <T> Iterator<T> iterator ()
  {
    return INSTANCE;
  }
}
