package dsto.dfc.util;

/**
 * Extends the java.lang.Cloneable interface to guarantee that clone () is
 * public, rather than protected as defined by Object.
 *
 * @version $Revision$
 */
public interface Copyable extends Cloneable
{
  /**
   * Create a completely separate copy of this object and any mutable
   * objects owned by the object.<p>
   *
   * NOTE: subclasses should not remove the CloneNotSupportedException
   * throws clause if they themselves support subclassing:
   * removing the CloneNotSupportedException declaration stops
   * subclasses from indicating that a clone was not possible.
   *
   * @exception CloneNotSupportedException if the clone failed or is not
   * possible.
   */
  public Object clone () throws CloneNotSupportedException;
}