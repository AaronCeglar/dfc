package dsto.dfc.util;

import java.util.Comparator;

/**
 * Default comparator instance that will compare anything thrown at it
 * in a semi-sensible way.
 * 
 * @version $Revision$
 */
public class DefaultComparator implements Comparator
{
  private static final DefaultComparator INSTANCE = new DefaultComparator ();

  public DefaultComparator ()
  {
    // zip
  }

  public static DefaultComparator instance ()
  {
    return INSTANCE;
  }

  public int compare (Object o1, Object o2)
  {
    if (o1 == o2)
      return 0;
    else if (o1 == null)
      return -1;
    else if (o2 == null)
      return 1;
    else if (o1.getClass () == o2.getClass ())
    {
      // for objects of same class, use Comparable or fall back on toString ()
      if (o1 instanceof String)
        return ((String)o1).compareToIgnoreCase ((String)o2);
      else if (o1 instanceof Comparable)
        return ((Comparable)o1).compareTo (o2);
      else
        return o1.toString ().compareToIgnoreCase (o2.toString ());
    } else
    {
      // for objects of different class, use class name compare
      return o1.getClass ().getName ().compareTo (o2.getClass ().getName ());
    }
  }

  public boolean equals (Object obj)
  {
    return obj instanceof DefaultComparator;
  }
  
  public int hashCode ()
  {
    return 42;
  }
}