package dsto.dfc.util;

import java.util.Comparator;

/**
 * Compares objects using their stringified (toString ()) form.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class StringComparator implements Comparator
{
  private static final StringComparator INSTANCE = new StringComparator (true);
  private static final StringComparator CASE_INSENSITIVE_INSTANCE = new StringComparator (false);

  private boolean caseSensitive;

  public StringComparator ()
  {
    this (true);
  }

  public StringComparator (boolean caseSensitive)
  {
    this.caseSensitive = caseSensitive;
  }

  public static StringComparator getInstance ()
  {
    return INSTANCE;
  }

  public static StringComparator getCaseInsensitiveInstance ()
  {
    return CASE_INSENSITIVE_INSTANCE;
  }

  public int compare (Object o1, Object o2)
  {
    if (caseSensitive)
      return o1.toString ().compareTo (o2.toString ());
    else
      return o1.toString ().compareToIgnoreCase (o2.toString ());
  }

  @Override
  public boolean equals (Object obj)
  {
    return obj instanceof StringComparator &&
           ((StringComparator)obj).caseSensitive == caseSensitive;
  }

  @Override
  public int hashCode ()
  {
    return 17 + (caseSensitive ? 1 : 2);
  }
}