package dsto.dfc.util;

import java.util.ArrayList;
import java.util.Iterator;

import dsto.dfc.logging.Log;

import static java.lang.System.currentTimeMillis;

import static dsto.dfc.logging.Log.diagnostic;

/**
 * A lightweight task scheduler thread similar to java.util.Timer. The reason
 * for this class' existence is that the Java Timer class does not allow
 * {@link Task Tasks} to be rescheduled, meaning you can't create a task,
 * schedule it and then reschedule for some later time.
 * <p>
 *
 * Threading implementation note: while a task is executing, the scheduler
 * thread owns its monitor, but not the scheduler monitor so the task is not
 * forced to block access to the scheduler by other threads. So, to avoid the
 * possibility of deadlock if the task <b>does</b> decide to acquire the
 * scheduler monitor (most commonly by calling {@link #schedule(Task)} to
 * reschedule itself), when the scheduler needs both its own monitor and a
 * task's, it always acquires the task's first, then its own.
 *
 * @author Matthew Phillips
 */
public class Scheduler extends Thread
{
  protected ArrayList<Task> tasks;
  protected Task currentTask;
  protected int workers;
  protected boolean stopRequested;

  public Scheduler ()
  {
    super ("Scheduler");

    this.tasks = new ArrayList<Task> ();
    this.currentTask = null;
  }

  public synchronized int getTaskCount ()
  {
    return tasks.size ();
  }

  private synchronized boolean isIdle ()
  {
    return (tasks.size () == 0) && (currentTask == null);
  }

  /**
   * Waits, via a 100 millisecond polling loop, for this scheduler to
   * became idle, that is, the scheduler has no more tasks pending run
   * and that it is not currently running a task.
   */
  public void waitForIdle ()
  {
    while (!isIdle ())
    {
      try
      {
        sleep (100);
      } catch (InterruptedException e)
      {
        Thread.currentThread ().interrupt ();
      }
    }
  }

  /**
   * Schedule a task to run immediately.
   *
   * @see #schedule(Task, long)
   */
  public void schedule (Task task)
  {
    schedule (task, 0);
  }

  /**
   * Schedule a task to run with a given delay. If the task is currently
   * running, this blocks until the task finishes.
   *
   * @param task The feed.
   * @param delay Delay in millis. Zero or less implies immediate run.
   *
   * @see #unschedule(Task)
   *
   * @throws IllegalArgumentException If the task is already scheduled. Use
   *           unschedule () first if you want to reschedule,
   */
  public void schedule (Task task, long delay)
    throws IllegalArgumentException
  {
    if (stopRequested)
      throw new IllegalArgumentException ("Scheduler is shut down");

    // see note in class doc about why the double sync blocks
    synchronized (task)
    {
      synchronized (this)
      {
        if (task.runAt != 0)
          throw new IllegalArgumentException
            ("Cannot schedule already scheduled task");

        if (delay < 0)
          delay = 0;

        synchronized (task)
        {
          task.runAt = currentTimeMillis () + delay;

          insertTask (task);
        }

        if (!isAlive ())
          start ();

        notify ();
      }
    }
  }

  /**
   * Unschedule a task. If the task is running, this will block until
   * it finishes. This will do nothing if the task is not scheduled.
   *
   * @param task The feed.
   * @return True if at least one instance was unscheduled.
   *
   * @see #schedule(Task)
   */
  public boolean unschedule (Task task)
  {
    // see note in class doc about why the double sync blocks
    synchronized (task)
    {
      if (task.runAt == 0)
        return false;

      synchronized (this)
      {
        boolean removed = false;

        // == based removal. ArrayList.remove(Object) uses .equals()
        for (Iterator i = tasks.iterator (); i.hasNext (); )
        {
          Task t = (Task) i.next ();

          if (t == task)
          {
            i.remove ();

            removed = true;
          }
        }

        task.runAt = 0;

        return removed;
      }
    }
  }

  /**
   * Shutdown at the next opportunity. Blocks until any currently running tasks
   * have exited and disposes any not already running.
   */
  public synchronized void shutdown ()
  {
    stopRequested = true;

    try
    {
      if (isAlive ())
      {
        // wake up scheduler thread if needed
        notifyAll ();

        // wait for scheduler thread to exit
        // should be using join () but does not work on IKVM.NET (never returns)
        // see end of run () for corresponding notify ()
        wait (20000);
      }

      // dispose any extant tasks
      // (copy before iterate to avoid changes in list by task disposal)
      for (Iterator<Task> i = new ArrayList (tasks).iterator (); i.hasNext (); )
      {
        Task task = i.next ();
        
        synchronized (task)
        {
          task.scheduler = this;
          task.dispose ();
          task.scheduler = null;
        }
      }
      tasks.clear ();
    } catch (InterruptedException ex)
    {
      diagnostic ("Scheduler shutdown interrupted while waiting for " +
      		  "tasks to exit", this, ex);

      currentThread ().interrupt ();
    }
  }

  /**
   * Returns the time at which the last scheduled task will be run in
   * milliseconds since midnight, January 1, 1970, UTC. Used to schedule tasks a
   * period of time after the last known task will be run.
   * <p>
   *
   * <b>NB</b> Use {@link #getTaskCount()} to check if any tasks are currently
   * scheduled. If no tasks are scheduled this method will throw an
   * IllegalStateException because it doesn't make sense to get the run time of
   * a non-existent task.
   *
   * @return The time at which the last known task is scheduled to run.
   * @throws IllegalStateException if there are no tasks scheduled.
   */
  public synchronized long findLastRunAt ()
  {
    if (tasks.isEmpty ())
      throw new IllegalStateException ("Scheduler task list is empty");
    else
      return tasks.get (tasks.size () - 1).runAt;
  }

  @Override
  public void run ()
  {
    while (!stopRequested)
    {
      Task task;

      synchronized (this)
      {
        task = removeNextRunnable ();
      }

      // note: task exec is deliberately run out of
      // scheduler-synchronized block to avoid forcing it to hold the
      // monitor and possibly running the risk of deadlock.
      try
      {
        if (task != null)
        {
          synchronized (task)
          {
            task.scheduler = this;
            this.currentTask = task;

            task.run ();

            this.currentTask = null;
            task.scheduler = null;
          }
        }
      } catch (RuntimeException ex)
      {
        Log.alarm ("Unhandled exception in task", this, ex);
      }

      synchronized (this)
      {
        if (!stopRequested)
        {
          long nextRunAt = findNextRunAt ();

          try
          {
            long delay = nextRunAt - currentTimeMillis ();

            if (delay > 0)
              wait (delay);
          } catch (InterruptedException ex)
          {
            stopRequested = true;
          }
        }
      }
    }

    // Log.trace ("Exiting scheduler", this);

    // part of workaround for IKVM.NET join () bug
    synchronized (this)
    {
      // let any waiters know we're terminating
      notify ();
    }
  }

  private long findNextRunAt ()
  {
    if (tasks.isEmpty ())
      return Long.MAX_VALUE;
    else
      return tasks.get (0).runAt;
  }

  private Task removeNextRunnable ()
  { 
    if (tasks.isEmpty ()) 
      return null;

    Task task = tasks.get (0);

    if (task.runAt > currentTimeMillis ())
    {
      return null;
    } else
    {
      tasks.remove (0);

      task.runAt = 0;

      return task;
    }
  }

  private void insertTask (Task task)
  {
    int insertAt = -1;

    for (int i = 0; insertAt == -1 && i < tasks.size (); i++)
    {
      Task t = tasks.get (i);

      if (t.runAt > task.runAt)
        insertAt = i;
    }

    if (insertAt == -1)
      tasks.add (task);
    else
      tasks.add (insertAt, task);
  }

  /**
   * Base class for tasks that can be scheduled.
   */
  public abstract static class Task implements Runnable
  {
    /** Time that task will next be run. 0 => not currently scheduled. */
    protected long runAt;
    /**
     * The scheduler instance running the task. Only valid during run () and
     * dispose ().
     */
    protected Scheduler scheduler;

    public abstract void run ();

    /**
     * Called when the task is in the scheduling queue (but not currently
     * running) and the scheduler is being shut down. After this has been
     * called, the task will not get another chance to run in the current
     * scheduler.
     * <p>
     *
     * The default implementation calls run () if the task is scheduled to be
     * run now, does nothing if not. Subclasses may override as needed.
     */
    public void dispose ()
    {
      if (runAt <= currentTimeMillis ())
        run ();
    }

    public final boolean scheduled ()
    {
      return runAt != 0;
    }
  }
}
