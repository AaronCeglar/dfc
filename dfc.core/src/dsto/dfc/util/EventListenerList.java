package dsto.dfc.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventObject;
import java.util.List;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import dsto.dfc.logging.Log;

/**
 * A generic list of event listeners.
 * 
 * @author mpp
 * @version $Revision: 1.18 $
 */
public class EventListenerList
{
  private transient List listeners;
  private transient int nestingLevel;

  public EventListenerList ()
  {
    this.listeners = Collections.EMPTY_LIST;
  }
  
  public synchronized void addListener (Object listener)
  {
    // use copy-on-write to avoid interfering with any concurrent
    // event firing loop
    ArrayList newListeners = new ArrayList (listeners);
    newListeners.add (listener);
    
    listeners = newListeners;
  }
  
  /**
   * Add listener to the front of the list, so it will get events
   * before all other current listeners. Since depending on listener
   * order is usually a dodgy practice (and someone else could always
   * use this call), use at your own risk.
   */
  public synchronized void addFirstListener (Object listener)
  {
    // use copy-on-write to avoid interfering with any concurrent
    // event firing loop
    ArrayList newListeners = new ArrayList (listeners);
    newListeners.add (0, listener);
    
    listeners = newListeners;
  }
  
  public synchronized void removeListener (Object listener)
  {
    ArrayList newListeners = new ArrayList (listeners);
    
    // remove from list using == rather than equals ()
    for (int i = newListeners.size () - 1; i >= 0; i--)
    {
      if (newListeners.get (i) == listener)
      {
        newListeners.remove (i);
        break;
      }
    }
    
    listeners = newListeners;
  }
  
  public synchronized boolean hasListeners ()
  {
    return !listeners.isEmpty ();
  }
  
  public Collection getListeners ()
  {
    return listeners;
  }
  
  /**
   * Return the number of listeners in the list.
   */
  public int size ()
  {
    return listeners.size ();
  }
  
  /**
   * Get the current nesting level of the event chain. This can be
   * used to determine whether a recursive or nested event is
   * triggered by an event listener. Level == 0 when no event is being
   * fired, level == 1 when an event is initially being fired, level >
   * 1 for nested events.
   */
  public int getEventNestingLevel ()
  {
    return nestingLevel;    
  }
  
  /**
   * Fire an event of a given type. The type of listener interface is derived
   * from the event class name.
   * 
   * @param eventType The name of the method on the listener class eg
   * "propertyChanged" or "mouseDown".
   * @param event The event object to pass to listeners. The listener interface
   * is derived from the event's class name: eg MouseEvent => MouseListener.
   * 
   * @see #fireEvent(Class, String, EventObject)
   */
  public void fireEvent (String eventType, EventObject event)
  {
    if (listeners.isEmpty ())
      return;

    try
    {
      // derive listener class name from event name
      String listenerClassName = event.getClass ().getName ();
      listenerClassName =
        listenerClassName.substring
          (0, listenerClassName.length () - 5) + "Listener";
      
      Class listenerClass = Class.forName (listenerClassName);
      
      fireEvent (listenerClass, eventType, event);
      
    } catch (ClassNotFoundException ex)
    {
      throw new IllegalArgumentException ("No listener class: " + ex);
    }
  }
  
  /**
   * Fire an event of a given type.
   * 
   * @param listenerClass The event listener interface class (eg
   * PropertyChangeListener.class).
   * @param eventType The name of the method on the listener class eg
   * "propertyChanged" or "mouseDown".
   * @param event The event object to pass to listeners.
   * 
   * @see #fireEvent(String, EventObject)
   */
  public void fireEvent (Class listenerClass,
                         String eventType,
                         EventObject event)
  {
    List localListeners = listeners;
    
    if (localListeners.isEmpty ())
      return;
    
    try
    {
      nestingLevel++;
      
      Method listenerMethod =
        listenerClass.getMethod (eventType, new Class [] {event.getClass ()});

      Object [] args = new Object [] {event};
      
      for (int i = 0; i < localListeners.size (); i++)
      {
        Object listener = localListeners.get (i);
        
        if (listenerClass.isAssignableFrom (listener.getClass ()))
          listenerMethod.invoke (listener, args);
      }

    } catch (InvocationTargetException ex)
    {
      // log exception, but cannot sensibly re-throw since we don't want
      // to force clients to handle any Throwable
      Log.alarm ("Error in event listener", this, ex.getTargetException ());      
    } catch (Exception ex)
    {
      // this indicates some sort of reflection exception
      
      throw new IllegalArgumentException ("Invalid event: " + ex);
    } finally
    {
      nestingLevel--;
    }
  }
}
