package dsto.dfc.util;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Base implementation of PropertyEventSource.  In order to provide
 * property change notification classes may either simply extend this
 * class (use by inheritance) or create a standalone instance and
 * forward methods to it (use by composition) with the event source
 * set to be the containing instance.<p>

 * NOTE: in the use by composition case, it is the client clone ()
 * method's responsibility to update the event source correctly, eg
 * via a call to {@link #setPropertyChangeSource}.<p>
 *
 * This class also implements the {@link dsto.dfc.util.JavaBean} marker
 * interface.
 * 
 * @version $Revision$
 */
public class BasicPropertyEventSource
  implements PropertyEventSource, JavaBean, Serializable, Copyable
{
  private static final long serialVersionUID = 2832147823827880924L;

  private Object source;        // null => this instance is source
  private transient ArrayList listeners;

  public BasicPropertyEventSource ()
  {
    this.source = null;
  }

  public BasicPropertyEventSource (Object source)
  {
    this.source = source;
  }

  protected void setPropertyChangeSource (Object newSource)
  {
    this.source = newSource;
  }

  protected Object getPropertyChangeSource ()
  {
    return source == null ? this : source;
  }

  public synchronized void addPropertyChangeListener (PropertyChangeListener l)
  {
    // create new listeners list to avoid possibly clashing
    // when a listener is added/removed during event firing.
    if (listeners != null)
      listeners = new ArrayList (listeners);
    else
      listeners = new ArrayList ();

    listeners.add (l);
  }

  public synchronized void removePropertyChangeListener (PropertyChangeListener l)
  {
    if (listeners != null)
    {
      // if removing last element, null listener list
      if (listeners.size () == 1)
      {
        if (listeners.get (0) == l)
          listeners = null;
      } else
      {
        // clone and remove to avoid possibly clashing
        // when a listener is added/removed during event firing.
        listeners = (ArrayList)listeners.clone ();
        listeners.remove (l);
      }
    }
  }

  public Object clone () throws CloneNotSupportedException
  {
    BasicPropertyEventSource newObject =
      (BasicPropertyEventSource)super.clone ();

    newObject.source = null;
    newObject.listeners = null;

    return newObject;
  }

  public void firePropertyChange (String propertyName,
                                  Object oldValue, Object newValue)
  {
    if (listeners != null &&
        oldValue != newValue &&
         (oldValue == null || newValue == null || !oldValue.equals (newValue)))
    {
      PropertyChangeEvent e =
        new PropertyChangeEvent (getPropertyChangeSource (), propertyName,
                                 oldValue, newValue);

      doFirePropertyChange (e);
    }
  }

  public void firePropertyChange (String propertyName, boolean newValue)
  {
    if (listeners != null)
    {
      doFirePropertyChange
        (new PropertyChangeEvent
          (getPropertyChangeSource (), propertyName,
           new Boolean (!newValue), new Boolean (newValue)));
    }
  }

  public void firePropertyChange (String propertyName,
                                  boolean oldValue, boolean newValue)
  {
    if (listeners != null && oldValue != newValue)
    {
      doFirePropertyChange
        (new PropertyChangeEvent
         (getPropertyChangeSource (), propertyName,
          new Boolean (oldValue), new Boolean (newValue)));
    }
  }

  public void firePropertyChange (String propertyName,
                                  int oldValue, int newValue)
  {
    if (listeners != null && oldValue != newValue)
    {
      doFirePropertyChange
        (new PropertyChangeEvent
         (getPropertyChangeSource (), propertyName,
          new Integer (oldValue), new Integer (newValue)));
    }
  }

  public void firePropertyChange (String propertyName,
                                  float oldValue, float newValue)
  {
    if (listeners != null && oldValue != newValue)
    {
      doFirePropertyChange
        (new PropertyChangeEvent
         (getPropertyChangeSource (), propertyName,
          new Float (oldValue), new Float (newValue)));
    }
  }

  public void firePropertyChange (String propertyName,
                                  double oldValue, double newValue)
  {
    if (listeners != null && oldValue != newValue)
    {
      doFirePropertyChange
        (new PropertyChangeEvent
         (getPropertyChangeSource (), propertyName,
          new Double (oldValue), new Double (newValue)));
    }
  }

  public void firePropertyChange (String propertyName,
                                  char oldValue, char newValue)
  {
    if (listeners != null && oldValue != newValue)
    {
      doFirePropertyChange
        (new PropertyChangeEvent
         (getPropertyChangeSource (), propertyName,
          new Character (oldValue), new Character (newValue)));
    }
  }

  private void doFirePropertyChange (PropertyChangeEvent e)
  {
    List theListeners = listeners;

    for (int j = 0; j < theListeners.size (); j++)
      ((PropertyChangeListener)theListeners.get (j)).propertyChange (e);

  }
}

