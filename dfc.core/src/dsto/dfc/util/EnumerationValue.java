package dsto.dfc.util;

/**
 * Defines a value that is part of an enumerated set of values.
 *
 * @version $Revision$
 */
public interface EnumerationValue
{
  /**
   * Get the list of values within the enumeration.
   */
  public EnumerationValue [] getEnumValues ();
}
