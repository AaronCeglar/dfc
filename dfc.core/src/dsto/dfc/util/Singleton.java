package dsto.dfc.util;

/**
 * <p>A marker interface to identify classes that have only one
 * instance per VM.  A class implementing this interface should also
 * allow access to the its instance by providing at least one of:</p>
 *
 * <ul>
 *
 *   <li>A method with the signature
 *   <code>public static Object getInstance ()</code></li>
 *
 *   <li>A method with the signature
 *   <code>public static Object getSingletonInstance ()</code></li>
 *
 *   <li>A static member variable of the form
 *   <code>public static final <i>[classname]</i>INSTANCE</code><li>
 *
 * </ul>
 *
 * @author Matthew
 * @version $Revision$
 *
 * @see Beans#getSingletonInstance
 */
public interface Singleton
{
  // marker interface
}
