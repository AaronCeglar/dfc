package dsto.dfc.util;

import java.util.Timer;
import java.util.TimerTask;

import java.lang.reflect.Method;

import dsto.dfc.logging.Log;

/**
 * Used to deliver an event from java.util.Timer. This can be used to shunt an
 * event from a one thread to be re-delivered in another.
 * 
 * <p>Example usage:
 * 
 * <pre>
 * public void propertyValueChanged (PropertyChangeEvent e)
 * {
 *   Timer timer = java.util.Timers.getTimer (false);
 *   timer.schedule (new TimerEventCallback (this, "propertyValueChanged", e), 0);
 * }
 * </pre>
 * 
 * or (shorter and equivalently)
 * 
 * <pre>
 *   TimerEventCallback.schedule (this, "propertyValueChanged", e);
 * </pre>
 * 
 * @author phillipm
 */
public class TimerCallback extends TimerTask
{
  private Object target;
  private Object param;
  private Method method;

  /**
   * Create a new instance.
   * 
   * @param target The target object to invoke the event on.
   * @param methodName The name of the event handling method on target.
   * @param param The parameter object.
   * 
   * @throws IllegalArgumentException if method does not exist.
   * 
   * @see #schedule(Object, String, Object)
   */
  public TimerCallback (Object target, String methodName, Object param)
    throws IllegalArgumentException
  {
    this.target = target;
    this.param = param;
    
    try
    {
      method = target.getClass ().getDeclaredMethod (methodName, new Class [] {param.getClass ()});
      method.setAccessible (true);
    } catch (NoSuchMethodException ex)
    {
      throw new IllegalArgumentException ("Method " + methodName + " does not exist");
    }
  }

  /**
   * Shortcut to schedule an event in the shared non-daemon timer available
   * from {@link Timers#getTimer(boolean)}.
   * 
   * @param target The target object to invoke the event on.
   * @param methodName The name of the event handling method on target.
   * @param param The parameter object.
   * 
   * @throws IllegalArgumentException if method does not exist.
   * 
   * @see #schedule(Object, String, Object, long)
   */
  public static void schedule (Object target, String methodName, Object param)
    throws IllegalArgumentException
  {
    schedule (target, methodName, param, 0L);
  }
  
  /**
   * Shortcut to schedule an event in the shared daemon timer available
   * from {@link Timers#getTimer(boolean)}.
   * 
   * @param target The target object to invoke the event on.
   * @param methodName The name of the event handling method on target.
   * @param param The parameter object.
   * @param delayMillis Delay in milliseconds.
   * 
   * @throws IllegalArgumentException if method does not exist.
   * 
   * @see #schedule(Object, String, Object)
   */
  public static void schedule (Object target, String methodName,
                                 Object param, long delayMillis)
    throws IllegalArgumentException
  {
    Timer timer = Timers.getTimer (true);
    timer.schedule (new TimerCallback (target, methodName, param), delayMillis);
  }
  
  public void run ()
  {
    try
    {
      method.invoke (target, new Object [] {param});
    } catch (Exception ex)
    {
      Log.alarm
        ("Event callback generated exception", this, Objects.unwrapException (ex));
    }
  }
}
