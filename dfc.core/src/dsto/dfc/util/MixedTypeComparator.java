package dsto.dfc.util;

import java.util.Comparator;

import static java.lang.Math.abs;

/**
 * A comparator that tries to intelligently order various mixed value
 * types. For cases where two compared values are the same type: uses
 * case-insenstive mode if both values are strings, otherwise tries to
 * use Comparable interface on either value, and then gives up with an
 * error. For cases where values are different types, it orders the
 * types into ordered categories of: numbers, strings, and everything
 * else categorised by hash code, This should work on collections of
 * strings, numbers and mutually comparable objects fairly well.
 * 
 * @author Matthew Phillips
 */
public class MixedTypeComparator implements Comparator<Object>
{
  public static final MixedTypeComparator INSTANCE = new MixedTypeComparator ();
  
  public int compare (Object o1, Object o2)
  {
    if (o1 == o2)
      return 0;
    else if (o1 == null)
      return -1;
    else if (o2 == null)
      return 1;
    else if (o1 instanceof String && o2 instanceof String)
      return String.CASE_INSENSITIVE_ORDER.compare (o1.toString (), o2.toString ());
    else if (o1.getClass () == o2.getClass ())
    {
      if (o1 instanceof Comparable)
        return ((Comparable)o1).compareTo (o2);
      else
        throw new IllegalArgumentException
          ("Don't know how to compare two instances of " + o1.getClass ());
    } else
    {
      return category (o1.getClass ()) - category (o2.getClass ());
    }
  }

  private int category (Class type)
  {
    if (Number.class.isAssignableFrom (type))
      return 0;
    else if (type == String.class)
      return 1;
    else
      return abs (type.getName ().hashCode ()) + 2;
  }
}
