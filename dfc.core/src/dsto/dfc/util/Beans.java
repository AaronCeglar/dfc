package dsto.dfc.util;

import java.beans.PropertyDescriptor;
import java.beans.PropertyEditor;
import java.beans.PropertyEditorManager;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;

import dsto.dfc.text.StringUtility;

/**
 * Utilities for messing with JavaBeans.
 *
 * @version $Revision$
 */
public final class Beans
{
  private static final HashMap propertyEditorCache = new HashMap ();

  private Beans ()
  {
    // zip
  }

  /**
   * Test if a given class is a JavaBean. The class is considered a bean
   * if it has at least one readable bean property, ie one public, non-static,
   * non-void getXXX () method. This method does not use the Beans introspector
   * and hence will not recognise beans that use non-standard property
   * accessors published via BeanInfo.
   *
   * @param aClass The class to test
   */
  public static boolean isBean (Class aClass)
  {
    Method [] methods = aClass.getMethods ();

    for (int i = 0; i < methods.length; i++)
    {
      Method method = methods [i];

      if (method.getName ().startsWith ("get") &&
          (method.getModifiers () & Modifier.STATIC) == 0 &&
          method.getParameterTypes ().length == 0 &&
          !(method.getReturnType ().equals (Void.TYPE) ||
            method.getReturnType ().equals (Void.class)))
      {
        return true;
      }
    }

    return false;
  }

  /**
   * Get the method used to write a given JavaBean property. This includes
   * setter methods that are private/protected.
   *
   * @param beanClass The class of JavaBean to analyse.
   * @param propertyName The name of the property.
   *
   * @return the property setter method, or null if none found.
   *
   * @see #getPropertyWriteMethod(Class,String,Class)
   * @see #getPropertyReadMethod(Class, String)
   */
  public static Method getPropertyWriteMethod (Class beanClass,
                                                String propertyName)
  {
    String methodName = "set" + StringUtility.capitaliseFirst (propertyName);

    return Objects.findMethod (beanClass, methodName);
  }

  /**
   * Get the method used to read a JavaBean property. This includes getter
   * methods that are private/protected.
   *
   * @param beanClass The class of the bean.
   * @param propertyName The property name.
   * @return The method used to access the property.
   * @exception NoSuchMethodException if no method for that property
   * could be resolved.
   *
   * @see #getPropertyWriteMethod(Class, String)
   */
  public static Method getPropertyReadMethod (Class beanClass,
                                                String propertyName)
    throws NoSuchMethodException
  {
    String methodName = StringUtility.capitaliseFirst (propertyName);

    // try "get"better pattern
    Method method = Objects.findMethod (beanClass, "get" + methodName);

    // try "is" getter pattern
    if (method == null)
      method = Objects.findMethod (beanClass, "is" + methodName);

    if (method != null)
      return method;
    else
      throw new NoSuchMethodException
        ("Property read method for \"" + propertyName + "\" not found on " + beanClass);
  }

  /**
   * Attempt to resolve a JavaBean's method for writing a property
   * value.
   *
   * @param targetClass The class of JavaBean to analyse.
   * @param propertyName The name of the property.
   * @param valueClass The type of the property.
   * @return The 'set' method that sets the property value, or null if
   * no such method could be found.
   *
   * @see #getPropertyWriteMethod(Class,String)
   */
  public static Method getPropertyWriteMethod (Class targetClass,
                                               String propertyName,
                                               Class valueClass)
  {
    String methodName = "set" + StringUtility.capitaliseFirst (propertyName);
    Method method = null;

    // try valueClass as param and all superclasses/superinterfaces
    method = Objects.findMethod (targetClass, methodName, valueClass);

    if (method == null)
    {
      // try the equivalent primitive value (if any)

      Class primitiveClass = objectToPrimitive (valueClass);

      if (primitiveClass != valueClass)
        method = Objects.getMethod (targetClass, methodName, primitiveClass);
    }

    return method;
  }

  /**
   * True if a JavaBean has a writable property with a given name.
   *
   * @param targetClass The JavaBean class.
   * @param propertyName The property name.
   * @param valueClass The type of property value.
   * @return True if the JavaBean has such a writeable property.
   * @see #getPropertyWriteMethod
   * @see #setPropertyValue
   */
  public static boolean hasWritableProperty (Class targetClass,
                                             String propertyName,
                                             Class valueClass)
  {
    return getPropertyWriteMethod (targetClass, propertyName, valueClass) != null;
  }

  /**
   * Similar to {@link #hasWritableProperty(Class, String, Class)} except the
   * type of the property is not checked. This is a little faster than
   * the full version.
   *
   * @param targetClass The class to check.
   * @param propertyName The name of the property.
   * @return True if there is a property write method.
   */
  public static boolean hasWritableProperty (Class targetClass,
                                             String propertyName)

  {
    String methodName = "set" + StringUtility.capitaliseFirst (propertyName);
    Method [] methods = targetClass.getMethods ();

    for (int i = 0; i < methods.length; i++)
    {
      Method method = methods [i];

      if (method.getName ().equals (methodName) &&
          (method.getModifiers () & Modifier.STATIC) == 0 &&
          method.getParameterTypes ().length == 1 &&
          (method.getReturnType ().equals (Void.TYPE) ||
             method.getReturnType ().equals (Void.class)))
      {
        return true;
      }
    }

    return false;
  }

  /**
   * Set the value of a writeable JavaBean property.  Note: in the case of
   * setting property value to null, resolution of the correct write method
   * may behave unexpectedly if there are multiple overloaded 'set' methods
   * (this would be a bad idea anyway, right?).
   *
   * @param target The JavaBean.
   * @param propertyName The name of the property.
   * @param propertyValue The new value of the property.
   * @exception NoSuchMethodException if the property does not exist.
   * @exception InvocationTargetException if the property setter method threw
   * an exception.
   */
  public static void setPropertyValue (Object target,
                                       String propertyName,
                                       Object propertyValue)
    throws NoSuchMethodException, InvocationTargetException
  {
    Debug.assertTrue (target != null,
                      "setPropertyValue called with null target (propertyName = " +
                      propertyName + ")",
                      Beans.class);

    Method method = null;

    if (propertyValue != null && target != null)
    {
      method = getPropertyWriteMethod (target.getClass (),
                                       propertyName,
                                       propertyValue.getClass ());
    } else if (target != null)
    {
      method = getPropertyWriteMethod (target.getClass (),
                                       propertyName);
    }

    if (method != null)
    {
      try
      {
        method.setAccessible (true);
        method.invoke (target, new Object [] {propertyValue});
      } catch (IllegalAccessException ex)
      {
        NoSuchMethodException ex2 = new NoSuchMethodException
          ("no permission to write property '" + propertyName + "'");

        ex2.initCause (ex);

        throw ex2;
      }
    } else
    {
      throw new NoSuchMethodException ("no property named: " + propertyName);
    }
  }

  /**
   * Get the value of a given property.
   *
   * @param target The object to read the value from.
   * @param property The property name.
   * @return The value of the property.
   * @exception NoSuchMethodException if the property does not exist
   * or an error occurred during the property method invocation.
   */
  public static Object getPropertyValue (Object target, String property)
    throws NoSuchMethodException
  {
    Method method = getPropertyReadMethod (target.getClass (), property);

    try
    {
      return method.invoke (target, (Object [])null);
    } catch (Exception ex)
    {
      throw new NoSuchMethodException
        ("exception in method invocation: " + ex);
    }
  }

  /**
   * Get the type of a property.
   *
   * @param beanClass The class of the JavaBean.
   * @param property The property name.
   * @return The class of the given property.
   * @exception NoSuchMethodException if the property does not exist.
   */
  public static Class getPropertyClass (Class beanClass, String property)
    throws NoSuchMethodException
  {
    Method method = getPropertyReadMethod (beanClass, property);

    return method.getReturnType ();
  }

  /**
   * Convert a class representing a primitive type (eg 'int') to its
   * equivalent Object-based type (eg 'java.lang.Integer').
   *
   * @param theClass The primitive class.
   * @return The equivalent Object-based class, or simply theClass if
   * theClass is not a primitive class.
   */
  public static Class primitiveToObject (Class theClass)
  {
    if (theClass.isPrimitive ())
    {
      if (theClass.equals (Integer.TYPE))
        return Integer.class;
      else if (theClass.equals (Long.TYPE))
      	return Long.class;
      else if (theClass.equals (Float.TYPE))
        return Float.class;
      else if (theClass.equals (Double.TYPE))
        theClass = Double.class;
      else if (theClass.equals (Short.TYPE))
        theClass = Short.class;
      else if (theClass.equals (Character.TYPE))
        theClass = Character.class;
      else if (theClass.equals (Boolean.TYPE))
        theClass = Boolean.class;
      else if (theClass.equals (Byte.TYPE))
        theClass = Byte.class;
      else
        throw new RuntimeException ("Unknown class : " + theClass);
    }

    return theClass;
  }

  public static Class objectToPrimitive (Class theClass)
  {
    if (!theClass.isPrimitive ())
    {
      if (theClass.equals (Integer.class))
        return Integer.TYPE;
      if (theClass.equals (Long.class))
        return Long.TYPE;
      else if (theClass.equals (Float.class))
        return Float.TYPE;
      else if (theClass.equals (Double.class))
        return Double.TYPE;
      else if (theClass.equals (Short.class))
        return Short.TYPE;
      else if (theClass.equals (Character.class))
        return Character.TYPE;
      else if (theClass.equals (Boolean.class))
        return Boolean.TYPE;
    }

    return theClass;
  }

  /**
   * Like Class.forName () but also handles the primitive classes "int",
   * "float", etc.
   *
   * @param name The class name.
   * @return The Class with the associated name.
   *
   * @throws ClassNotFoundException if class is not defined.
   */
  public static Class classForName (String name)
    throws ClassNotFoundException
  {
    try
    {
      return Class.forName (name);
    } catch (ClassNotFoundException ex)
    {
      if (name.equals ("int"))
        return Integer.TYPE;
      if (name.equals ("long"))
        return Long.TYPE;
      else if (name.equals ("float"))
        return Float.TYPE;
      else if (name.equals ("double"))
        return Double.TYPE;
      else if (name.equals ("short"))
        return Short.TYPE;
      else if (name.equals ("char"))
        return Character.TYPE;
      else if (name.equals ("boolean"))
        return Boolean.TYPE;
      else
        throw new ClassNotFoundException ("Class not found: " + name);
    }
  }

  /**
   * Test if a class is a singleton class type (see {@link Singleton} for
   * details on what constitues a singleton class).
   */
  public static boolean isSingletonClass (Class theClass)
  {
    return Singleton.class.isAssignableFrom (theClass);
  }

  /**
   * Get the global instance of a singleton class.  See {@link Singleton} for
   * details on what constitues a singleton class.
   *
   * @param theClass The class to examine.  For convenience, theClass does
   * not have to implement the Singleton interface, it just has to have either
   * a static getSingletonInstance() method or a static INSTANCE variable.
   * @exception IllegalArgumentException if theClass is not a singleton class.
   */
  public static Object getSingletonInstance (Class theClass)
    throws IllegalArgumentException
  {
    // try getSingletonInstance method
    try
    {
      Method method =
        theClass.getDeclaredMethod ("getSingletonInstance", (Class [])null);

      if (Modifier.isStatic (method.getModifiers ()))
      {
        boolean oldAccessible = method.isAccessible ();
        method.setAccessible (true);
        Object instance = method.invoke (null, (Object [])null);
        method.setAccessible (oldAccessible);

        return instance;
      }
    } catch (NoSuchMethodException ex)
    {
      // zip
    } catch (SecurityException ex)
    {
      // zip
    } catch (IllegalAccessException ex)
    {
      // zip
    } catch (InvocationTargetException ex)
    {
      throw new IllegalArgumentException
        (theClass.getName () + ".getSingletonInstance () threw exception: " +
         ex.getTargetException ());
    }

    // try getInstance method
    try
    {
      Method method = theClass.getDeclaredMethod ("getInstance", (Class [])null);

      if (Modifier.isStatic (method.getModifiers ()))
      {
        boolean oldAccessible = method.isAccessible ();
        method.setAccessible (true);
        Object instance = method.invoke (null, (Object [])null);
        method.setAccessible (oldAccessible);

        return instance;
      }
    } catch (NoSuchMethodException ex)
    {
      // zip
    } catch (SecurityException ex)
    {
      // zip
    } catch (IllegalAccessException ex)
    {
      // zip
    } catch (InvocationTargetException ex)
    {
      throw new IllegalArgumentException
        (theClass.getName () + ".getInstance () threw exception: " +
         ex.getTargetException ());
    }

    // try INSTANCE static variable
    try
    {
      Field field = theClass.getDeclaredField ("INSTANCE");

      if (Modifier.isStatic (field.getModifiers ()))
      {
        boolean oldAccessible = field.isAccessible ();
        field.setAccessible (true);
        Object instance = field.get (null);
        field.setAccessible (oldAccessible);

        return instance;
      }
    } catch (NoSuchFieldException ex)
    {
      // zip
    } catch (SecurityException ex)
    {
      // zip
    } catch (IllegalAccessException ex)
    {
      // zip
    }

    throw new IllegalArgumentException ("Class " + theClass.getName () +
                                        " is not a singleton");
  }

  /**
   * Add an event listener to a JavaBean using reflection.
   *
   * @param listenerInterface The event listener interface (eg
   * PropertyChangeListener).
   * @param source The event source bean.
   * @param listener The event listener (must implement
   * listenerInterface).
   *
   * @return True if the add succeeded.
   */
  public static boolean addListener (Class listenerInterface,
                                     Object source,
                                     Object listener)
  {
    Class sourceClass =
      source instanceof Class ? (Class)source : source.getClass ();

    // work out the method to call from the interface name
    String interfaceName = listenerInterface.getName ();
    int dotIndex = interfaceName.lastIndexOf ('.');

    if (dotIndex != -1)
      interfaceName = interfaceName.substring (dotIndex + 1);

    try
    {
      Method addListenerMethod =
        sourceClass.getMethod ("add" + interfaceName,
                               new Class [] {listenerInterface});

      addListenerMethod.invoke (source, new Object [] {listener});
    } catch (NoSuchMethodException ex)
    {
      return false;
    } catch (InvocationTargetException ex)
    {
      return false;
    } catch (IllegalAccessException ex)
    {
      return false;
    }

    return true;
  }

  /**
   * Remove an event listener to a JavaBean using reflection.
   *
   * @param listenerInterface The event listener interface (eg
   * PropertyChangeListener).
   * @param source The event source bean.
   * @param listener The event listener (must implement
   * listenerInterface).
   *
   * @return True if the remove succeeded.
   */
  public static boolean removeListener (Class listenerInterface,
                                        Object source,
                                        Object listener)
  {
    Class sourceClass =
      source instanceof Class ? (Class)source : source.getClass ();

    // work out the method to call from the interface name
    String interfaceName = listenerInterface.getName ();
    int dotIndex = interfaceName.lastIndexOf ('.');

    if (dotIndex != -1)
      interfaceName = interfaceName.substring (dotIndex + 1);

    try
    {
      Method removeListenerMethod =
        sourceClass.getMethod ("remove" + interfaceName,
                                      new Class [] {listenerInterface});

      removeListenerMethod.invoke (source, new Object [] {listener});
    } catch (NoSuchMethodException ex)
    {
      return false;
    } catch (InvocationTargetException ex)
    {
      return false;
    } catch (IllegalAccessException ex)
    {
      return false;
    }

    return true;
  }

  public static PropertyEditor findPropertyEditor (Class type)
  {
    PropertyEditor editor =
      (PropertyEditor)propertyEditorCache.get (type);

    if (editor == null)
    {
      if (!propertyEditorCache.containsKey (type))
      {
        editor = PropertyEditorManager.findEditor (type);

        propertyEditorCache.put (type, editor);
      }
    }

    return editor;
  }

  /**
   * Find a property descriptor with a given name.
   *
   * @param properties The properties to search.
   * @param name The name to find.
   * @return The descriptor or null if not found.
   */
  public static PropertyDescriptor findProperty (PropertyDescriptor [] properties,
                                                 String name)
  {
    for (int i = 0; i < properties.length; i++)
    {
      PropertyDescriptor d = properties [i];

      if (d.getName ().equals (name))
        return d;
    }

    return null;
  }

  /**
   * Convert an object to its stringified value using (if possible) its
   * PropertyEditor.  If no editor is available, toString () is used.
   *
   * @param propertyValue The value to convert.
   * @return The stringified version of propertyValue.
   *
   * @see #convertStringToProperty
   */
  public static String convertPropertyToString (Object propertyValue)
  {
    if (propertyValue == null)
      return "null";

    PropertyEditor editor = findPropertyEditor (propertyValue.getClass ());

    if (editor == null)
    {
      return propertyValue.toString ();
    } else
    {
      editor.setValue (propertyValue);

      return editor.getAsText ();
    }
  }

  /**
   * Convert a stringfied object to an instance by using (if possible) its
   * PropertyEditor.  If no property editor is available, but a constructor
   * taking a single string argument is, then this will be used instead.
   *
   * @param propertyType The type of the new property value.
   * @param propertyText The stringified property value
   * @return A new instance of propertyType made by parsing propertyText.
   * @throws IllegalArgumentException If propertyText could not be converted
   * to an object (eg format error or no converter was found).
   *
   * @see #convertPropertyToString
   */
  public static Object convertStringToProperty (Class propertyType,
                                                String propertyText)
    throws IllegalArgumentException
  {
    PropertyEditor editor = findPropertyEditor (propertyType);

    if (editor == null)
    {
      // try constructor
      try
      {
        Constructor constructor =
          propertyType.getConstructor (new Class [] {String.class});

        return constructor.newInstance (new Object [] {propertyText});
      } catch (Exception ex)
      {
        Throwable ex2;

        // unwrap an InvocationTargetException
        if (ex instanceof InvocationTargetException)
          ex2 = ((InvocationTargetException)ex).getTargetException ();
        else
          ex2 = ex;

        throw new IllegalArgumentException
          ("Could not convert \"" + propertyText + "\" to " + propertyType +
           " using constructor (" + ex2.getMessage () + ")");
      }
    } else
    {
      editor.setAsText (propertyText);

      return editor.getValue ();
    }
  }

  /**
   * Dynamically add a listener to an event type using reflection.
   *
   * @param source The object to add the listener to.
   * @param eventType The event type eg "change" for ChangeListener,
   * "propertyChange" for PropertyChangeListener.
   * @param listenerClass The listener interface class eg
   * PropertyChangeListener.class for "propertyChange" events.
   * @param listener The listener class.  Must implement the interface
   * defined by listenerClass.
   * @return True if the add succeeded.
   */
  public static boolean addListener (Object source, String eventType,
                                     Class listenerClass, Object listener)
  {
    try
    {
      Method addMethod =
        findEventListenerMethod (source.getClass (), "add", eventType, listenerClass);

      addMethod.invoke (source, new Object [] {listener});

      return true;
    } catch (NoSuchMethodException ex)
    {
      return false;
    } catch (InvocationTargetException ex)
    {
      return false;
    } catch (IllegalAccessException ex)
    {
      return false;
    }
  }



  /**
   * Dynamically remove a listener to an event type using reflection.
   *
   * @param source The object to remove the listener from.
   * @param eventType The event type eg "change" for ChangeListener,
   * "propertyChange" for PropertyChangeListener.
   * @param listenerClass The listener interface class eg
   * PropertyChangeListener.class for "propertyChange" events.
   * @param listener The listener class.  Must implement the interface
   * defined by listenerClass.
   * @return True if the remove succeeded.
   */
  public static boolean removeListener (Object source, String eventType,
                                        Class listenerClass, Object listener)
  {
    try
    {
      Method removeMethod =
        findEventListenerMethod (source.getClass (), "remove", eventType, listenerClass);

      removeMethod.invoke (source, new Object [] {listener});

      return true;
    } catch (NoSuchMethodException ex)
    {
      return false;
    } catch (InvocationTargetException ex)
    {
      return false;
    } catch (IllegalAccessException ex)
    {
      return false;
    }
  }



  protected static Method findEventListenerMethod (Class sourceClass,
                                                   String prefix,
                                                   String eventType,
                                                   Class listenerClass)
    throws NoSuchMethodException
  {
    eventType = StringUtility.capitaliseFirst (eventType);
    String methodName = prefix + eventType + "Listener";

    return sourceClass.getMethod (methodName, new Class [] {listenerClass});
  }
}
