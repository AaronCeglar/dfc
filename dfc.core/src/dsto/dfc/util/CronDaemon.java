package dsto.dfc.util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import dsto.dfc.logging.Log;

/**
 * A simple cron-like daemon thread that notifies clients at regular
 * intervals.
 *
 * @version $Revision$
 */
public class CronDaemon extends Thread
{
  private long resolution = 10000;
  private ArrayList entries = new ArrayList ();

  public CronDaemon ()
  {
    setDaemon (true);
  }

  public void setResolution (long newValue)
  {
    resolution = newValue;
  }

  public long getResolution ()
  {
    return resolution;
  }

  public void addEntry (ActionListener listener,
                        long initialInterval)
  {
    addEntry (listener, initialInterval, -1);
  }

  public void addEntry (ActionListener listener,
                        long initialInterval, long repeatInterval)
  {
    synchronized (entries)
    {
      entries.add (new Entry (listener, initialInterval, repeatInterval));
    }

    synchronized (this)
    {
      this.notify ();
    }
  }

  public void removeEntry (ActionListener listener)
  {
    synchronized (entries)
    {
      Entry entry = null;

      for (int i = 0; entry == null && i < entries.size (); i++)
      {
        Entry e = (Entry)entries.get (i);

        if (e.listener == listener)
          entry = e;
      }

      if (entry != null)
        entries.remove (entry);
    }
  }

  public void shutDown ()
  {
    synchronized (entries)
    {
      entries.clear ();
    }
  }

  public void run ()
  {
    for (;;)
    {
      synchronized (entries)
      {
        ActionEvent event =
          new ActionEvent (this, ActionEvent.ACTION_PERFORMED, "CronDaemon");

        for (int i = 0; i < entries.size (); i++)
        {
          Entry entry = (Entry)entries.get (i);
          long currentTime = System.currentTimeMillis ();

          if (entry.nextWake <= currentTime)
          {
            entry.listener.actionPerformed (event);

            if (entry.interval == -1)
              entries.remove (entry);
            else
              entry.nextWake = currentTime + entry.interval;
          }
        }
      }

      try
      {
        if (entries.size () == 0)
        {
          synchronized (this)
          {
            this.wait ();
          }
        } else
        {
          sleep (resolution);
        }
      } catch (InterruptedException ex)
      {
        Log.alarm ("Cron daemon interrupted (exiting)", this, ex);

        break;
      }
    }
  }

  private static final class Entry
  {
    public long nextWake;
    public long interval;
    public ActionListener listener;

    public Entry (ActionListener listener, long initialInterval,
                  long interval)
    {
      this.listener = listener;
      this.interval = interval;
      this.nextWake = System.currentTimeMillis () + initialInterval;
    }
  }
}