package dsto.dfc.util;

import java.util.Timer;

/**
 * General routines for the java.util.Timer class including timer pool factory
 * methods.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public final class Timers
{
  private static Timer daemonTimer = null;
  private static Timer nondaemonTimer = null;

  private Timers ()
  {
    // zip
  }

  /**
   * Get a timer from the global timer pool.<p>
   * 
   * NOTE: if you create a non-daemon timer, you need to call dispose () on this
   * class for the application to exit normally.
   *
   * @param isDaemon True if the timer should be a daemon (see Timer class for
   * more info).
   * @return A shared timer instance.
   */
  public synchronized static Timer getTimer (boolean isDaemon)
  {
    if (isDaemon)
    {
      if (daemonTimer == null)
        daemonTimer = new Timer (true);

      return daemonTimer;
    } else
    {
      if (nondaemonTimer == null)
        nondaemonTimer = new Timer (false);

      return nondaemonTimer;
    }
  }
  
  public synchronized static void dispose ()
  {
    if (nondaemonTimer != null)
    {
      nondaemonTimer.cancel ();
      nondaemonTimer = null;
    }
    
    if (daemonTimer != null)
    {
      daemonTimer.cancel ();
      daemonTimer = null;
    }
  }
 
  /**
   * Reset (clear) the shared timers. This may be needed if the timer threads
   * have been killed (eg on applet restart). Unfortunately there is no way
   * to automatically detect when a Timer's thread has been killed.
   */ 
  public synchronized static void reset ()
  {
    daemonTimer = null;
    nondaemonTimer = null; 
  }
}