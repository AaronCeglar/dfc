package dsto.dfc.util;

import java.io.InvalidObjectException;
import java.io.Serializable;

/**
 * Base class for enumerated types.  Provides sensible default for equals() as
 * well as intelligent serialization support via a readResolve () that resolves
 * serialized enum values to their equivalent singleton instance in
 * getEnumValues ().  Superclasses that wish to use this feature must provide
 * a readResolve() implementation that calls super.readResolve().
 *
 * @version $Revision$
 */
public abstract class AbstractEnumerationValue
  implements EnumerationValue, Serializable
{
  private static final long serialVersionUID = 1L;

  public abstract EnumerationValue [] getEnumValues ();

  @Override
  public boolean equals (Object o)
  {
    return o != null &&
           o.getClass ().equals (getClass ()) &&
           toString ().equals (o.toString ());
  }

  @Override
  public int hashCode ()
  {
    int seed = 17;
    return seed + getClass ().hashCode () + toString ().hashCode ();
  }

  public int findIndex ()
  {
    return findIndex (this);
  }

  public static int findIndex (EnumerationValue value)
  {
    Object [] values = value.getEnumValues ();

    for (int i = 0; i < values.length; i++)
    {
      if (values [i].equals (value))
        return i;
    }

    return -1;
  }

  /**
   * Serialization support: resolves instance written to stream to the
   * equivalent instance in getEnumValues () using equals ().  Superclasses
   * that wish to use this feature must override this method and call
   * super.readResolve ().
   *
   * @exception InvalidObjectException if value could not be resolved
   * to a valid entry in the list of enum values.
   */
  protected Object readResolve () throws InvalidObjectException
  {
    Object [] values = getEnumValues ();

    for (int i = 0; i < values.length; i++)
    {
      if (values [i].equals (this))
        return values [i];
    }

    throw new InvalidObjectException
      ("Value \"" + toString () + "\" is not a valid enumeration entry");
  }
}
