package dsto.dfc.util;

public final class Math
{
  private Math ()
  {
    // zip
  }
  
  /**
   * Constrain value to be within a range.
   * 
   * @param min The min vaule.
   * @param value The value to compare.
   * @param max The max value.
   * 
   * @return A value >= min and &lt;= max. 
   */
  public static int range (int min, int value, int max)
  {
    if (value < min)
      return min;
    else if (value > max)
      return max;
    else
      return value;
  }
}
