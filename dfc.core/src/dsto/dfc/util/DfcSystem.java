package dsto.dfc.util;

import dsto.dfc.util.plugins.BootStrapLoader;

/**
 * DFC system utilities.
 *
 * @version $Revision$
 */
public final class DfcSystem
{
  private static final String [] systemPackages;

  static
  {
    systemPackages = new String []
    {
      "java.", "javax.accessibility", "javax.naming", "javax.rmi",
      "javax.sound", "javax.swing", "javax.transaction", "sun.", "org.omg",
      "com.sun.corba", "com.sun.image", "com.sun.java", "com.sun.javadoc",
      "com.sun.jdi", "com.sun.jndi", "com.sun.media", "com.sun.naming",
      "com.sun.org.omg", "com.sun.rmi.rmid", "com.sun.rsajca",
      "com.sun.rsasign", "com.sun.tools", "javax.media.j3d",
      "javax.vecmath", "com.sun.j3d.utils", "com.sun.j3d.audioengines",
      "com.sun.j3d.utils", "com.sun.j3d.loaders.lw3d",
      "com.sun.j3d.loaders.objectfile",
      "com.sun.j3d.loaders.Loader",
      "com.sun.j3d.loaders.Scene",
      "com.sun.j3d.loaders.LoaderBase",
      "com.sun.j3d.loaders.SceneBase",
      "com.sun.j3d.loaders.IncorrectFormatException",
      "com.sun.j3d.loaders.ParsingErrorException"
    };
  }

  private DfcSystem ()
  {
    // zip
  }

  /**
   * Test if a given class name is a 'system' class, ie a class
   * provided by the JRE or a core extension. Used primarily by
   * {@link BootStrapLoader} to determine whether to let the system
   * classloader handle loading a class.
   */
  public static boolean isSystemClass (String className)
  {
    className = className.replace ('/', '.');

    for (int i = 0; i < systemPackages.length; i++)
    {
      if (className.startsWith (systemPackages [i]))
        return true;
    }

    return false;
  }
}