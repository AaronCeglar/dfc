package dsto.dfc.util;

import java.beans.PropertyChangeListener;

/**
 * Interface for classes that fire PropertyChangeEvent's.
 *
 * @version $Revision$
 */
public interface PropertyEventSource
{
  public void addPropertyChangeListener (PropertyChangeListener l);

  public void removePropertyChangeListener (PropertyChangeListener l);
}

