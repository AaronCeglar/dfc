package dsto.dfc.util;

/**
 * Marker interface for JavaBean's.
 * 
 * @author mpp
 * @version $Revision$
 */
public interface JavaBean
{
  // marker interface
}
