package dsto.dfc.util;

import java.lang.reflect.Method;

import dsto.dfc.logging.Log;

/**
 * Used to deliver an event from a Runnable. This can be used to shunt an event
 * from a non-UI thread to be re-delivered in the UI thread.
 * 
 * <p>Example usage for SWT:
 * 
 * <pre>
 * public void propertyValueChanged (PropertyChangeEvent e)
 * {
 *   Display display = table.getDisplay ();
 *      
 *   if (display.getThread () == Thread.currentThread ())
 *   {
 *     table.update (e.path.first (), null);
 *   } else
 *   {
 *     display.asyncExec (new RunnableEventCallback (this, "propertyValueChanged", e));
 *   }
 * }
 * </pre>
 * 
 * @author phillipm
 */
public class RunnableEventCallback implements Runnable
{
  private Object event;
  private Object target;
  private Method method;

  /**
   * Create a new instance.
   * 
   * @param target The target object to invoke the event on.
   * @param methodName The name of the event handling method on target.
   * @param event The event object.
   * 
   * @throws IllegalArgumentException if method does not exist.
   */
  public RunnableEventCallback (Object target, String methodName, Object event)
  {
    this.target = target;
    this.event = event;
    
    method = Objects.findMethod (target.getClass (), methodName);
    
    if (method == null)
      throw new IllegalArgumentException ("Method " + methodName + " does not exist");
    
    method.setAccessible (true);
  }

  public void run ()
  {
    try
    {
      method.invoke (target, new Object [] {event});
    } catch (Exception ex)
    {
      Log.alarm
        ("Event callback generated exception", this, Objects.unwrapException (ex));
    }
  }
}
