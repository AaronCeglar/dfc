package dsto.dfc.util;

/**
 * Abstract base class for objects with a name that (at least partially) defines
 * their identity. Provides toString (), equals (), hashCode () and compareTo ().
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public abstract class AbstractNamedObject implements Comparable
{
  @SuppressWarnings ("unused")
  private static final long serialVersionUID = 1L;

  public abstract String getName ();

  public int hashCode ()
  {
    return getName ().hashCode ();
  }

  public String toString ()
  {
    return getName ();
  }

  public boolean equals (Object o)
  {
    if (o instanceof AbstractNamedObject)
    {
      return (o.getClass ().equals (getClass ())) &&
             ((AbstractNamedObject)o).getName ().equals (getName ());
    } else
    {
      return false;
    }
  }

  public int compareTo (Object o)
  {
    return getName ().compareTo (((AbstractNamedObject)o).getName ());
  }
}