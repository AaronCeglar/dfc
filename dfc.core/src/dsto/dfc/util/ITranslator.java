package dsto.dfc.util;

/**
 * Defines an object that translates objects from one form to another.
 * 
 * @see dsto.dfc.text.IStringTranslator
 * 
 * @version $Revision$
 */
public interface ITranslator
{
  public Object translate (Object object)
    throws InvalidFormatException;
}
