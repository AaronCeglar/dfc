package dsto.dfc.util;

import java.io.Serializable;

/**
 * Base class for objects with a name that (at least partially) defines
 * their identity. Provides toString (), equals (), hashCode (), compareTo ()
 * plus a name property.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class NamedObject
  extends AbstractNamedObject implements Comparable, Serializable
{
  private static final long serialVersionUID = 1L;

  protected String name;

  public NamedObject (String name)
  {
    this.name = name;
  }

  public String getName ()
  {
    return name;
  }

  public void setName (String newName)
  {
    name = newName;
  }

  public int hashCode ()
  {
    return getName ().hashCode ();
  }

  public String toString ()
  {
    return getName ();
  }

  public boolean equals (Object o)
  {
    if (o instanceof NamedObject)
    {
      return (o.getClass ().equals (getClass ())) &&
             ((NamedObject)o).getName ().equals (getName ());
    } else
    {
      return false;
    }
  }

  public int compareTo (Object o)
  {
    return getName ().compareTo (((NamedObject)o).getName ());
  }
}