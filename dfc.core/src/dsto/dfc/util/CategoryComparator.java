package dsto.dfc.util;

import java.text.Collator;
import java.util.Comparator;

/**
 * Comparator that sorts by a major category and then by a Collator.
 * Subclasses may override {@link #compare} to define new categories.
 * 
 * @author mpp
 * @version $Revision$
 */
public class CategoryComparator implements Comparator
{
  protected Collator collator;
  
  public CategoryComparator ()
  {
    this (Collator.getInstance ());
  }
  
  public CategoryComparator (Collator collator)
  {
    this.collator = collator;
  }
  
  public int compare (Object o1, Object o2)
  {
    int cat1 = category (o1);
    int cat2 = category (o2);

    if (cat1 != cat2)
      return cat1 - cat2;
    else
      return collator.compare (o1, o2);
  }
  
  public int category (Object value)
  {
    return 0;
  }
}
