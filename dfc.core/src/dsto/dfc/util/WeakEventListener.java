package dsto.dfc.util;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import dsto.dfc.databeans.PropertyEvent;
import dsto.dfc.databeans.PropertyListener;
import dsto.dfc.logging.Log;

/**
 * A 'weak' event listener proxy that does not preclude garbage
 * collection of the listener.  Similar in concept to a weak
 * reference, this class acts as a proxy to a contained listener.  The
 * contained listener is held with a weak reference, allowing it to be
 * garbage collected if the listener registration is the only thing
 * pointing to it.<p>
 *
 * This can be especially useful when transient classes (such as
 * commands) need to listen to global or static classes.<p>
 *
 * Example - add a property change listener to a bean:
 * <pre>
 *
 * WeakEventListener.createListener
 *  (PropertyChangeListener.class, bean, listener);
 *
 * </pre>
 *
 * @see Beans#addListener
 *
 * @author Matthew
 * @version $Revision$
 */
public class WeakEventListener
  extends WeakReference implements InvocationHandler
{
  /** Cleans up GC's WeakEventListener's. */
  protected static CleanupThread cleanupThread;

  // debug: private static int counter = 0;

  /** The thing being listened to. */
  protected Object source;
  /** The event listener interface (eg PropertyChangeListener). */
  protected Class listenerInterface;
  protected Object proxy;
  // debug: private int id = counter++;

  /**
   * Create a proxy weak event listener.  Use {@link #getProxy} to
   * retrieve the actual proxy instance of listenerInterface.  Most clients
   * will use {@link #createListener} instead to automatically create a
   * WeakEventListener instance and register its proxy with the event
   * source.
   *
   * @param listenerInterface The event listener interface (eg
   * PropertyChangeListener).
   * @param source The event source (ie the thing being listened to).
   * @param listener The actual listener instance (must implement the
   * listener interface).
   */
  public WeakEventListener (Class listenerInterface,
                            Object source, Object listener)
  {
    super (listener, getCleanupThread ().getRefQueue ());

    this.listenerInterface = listenerInterface;
    this.source = source;
    this.proxy = createProxy ();

    // debug: System.out.println ("Create weak listener id = " + id);
  }

  protected Object createProxy ()
  {
    // use non-dynamic proxy for known interfaces: allows basic use of weak
    // listener use with static compiler (ie no dynamic classes).
    if (listenerInterface.equals (StateChangeListener.class) ||
        listenerInterface.equals (PropertyListener.class))
    {
      return new WeakProxy ();
    } else
    {
      return Proxy.newProxyInstance (getClass ().getClassLoader (),
                                     new Class [] {listenerInterface},
                                     this);
    }
  }

  /**
   * Manually dispose the obvject and remove the proxy listener.
   */
  public void dispose ()
  {
    if (proxy != null)
    {
      Beans.removeListener (listenerInterface, source, proxy);
      
      proxy = null;
      source = null;
      listenerInterface = null;
    }
  }

  /**
   * Shortcut to automatically create a WeakEventListener instance and
   * register its proxy with the event source.
   *
   * @param listenerInterface The event listener interface (eg
   * PropertyChangeListener).
   * @param source The event source (ie the thing being listened to).
   * @param listener The actual listener instance (must implement the
   * listener interface).
   *
   * @return The weak event listener, or null if source does not support
   * the event type.
   */
  public static WeakEventListener createListener (Class listenerInterface,
                                                  Object source,
                                                  Object listener)
  {
    WeakEventListener proxyListener =
      new WeakEventListener (listenerInterface, source, listener);

    boolean result =
      Beans.addListener (listenerInterface, source,
                               proxyListener.getProxy ());

    return result ? proxyListener : null;
  }
  
  /**
   * The actual proxy created to receive calls to the listener
   * interface.
   */
  public Object getProxy ()
  {
    return proxy;
  }

  /**
   * Get or create the cleanup thread instance.
   */
  protected static CleanupThread getCleanupThread ()
  {
    if (cleanupThread == null)
    {
      cleanupThread = new CleanupThread ();
      cleanupThread.start ();
    }

    return cleanupThread;
  }

  // InvocationHandler implementation

  public Object invoke (Object proxyInstance, Method method, Object [] args)
    throws Throwable
  {
    // handle equality and hash code here
    if (method.getName ().equals ("equals") ||
        method.getName ().equals ("hashCode"))
    {
      /** @todo this is a bit dodgy: it will backfire if the proxied
       *  object has an equals() or hashCode() with different
       *  arguments
       */

      return method.invoke (this, args);
    } else
    {
      // forward method call to proxied object
      Object listener = get ();

      if (listener != null)
      {
        return method.invoke (listener, args);
      } else
      {
        // listener has been GC'd: remove

        // debug: System.out.println ("event call triggers garbage collect id = " + id);

        dispose ();

        return null;
      }
    }
  }
  
  /**
   * Proxy for common DFC listener types so we don't have to use a dynamically
   * generated one.
   */
  private class WeakProxy implements StateChangeListener, PropertyListener
  {
    public WeakProxy ()
    {
      // zip
    }
    
    public void stateChanged (StateChangeEvent e)
    {
      StateChangeListener listener = (StateChangeListener)get ();

      if (listener != null)
        listener.stateChanged (e);
      else
        dispose ();
    }

    public void propertyValueChanged (PropertyEvent e)
    {
      PropertyListener listener = (PropertyListener)get ();

      if (listener != null)
        listener.propertyValueChanged (e);
      else
        dispose ();
    }
  }
  
  /**
   * Unregisters weak listeners created for listeners that have been
   * GC'd.
   */
  private static final class CleanupThread extends Thread
  {
    private ReferenceQueue queue = new ReferenceQueue ();

    public CleanupThread ()
    {
      setDaemon (true);
    }

    public ReferenceQueue getRefQueue ()
    {
      return queue;
    }

    public void run ()
    {
      try
      {
        for (;;)
        {
          WeakEventListener wev = (WeakEventListener)queue.remove ();

          // debug: System.out.println ("GC'ing weak listener for id = " + wev.id);

          wev.dispose ();
        }
      } catch (InterruptedException ex)
      {
        /*
         * This will happen in an applet context, log (in case this is
         * a problem) and null out dead thread reference to ensure
         * applet reload works OK.
         */
        Log.diagnostic ("WeakEventListener cleanup thread interrupted",
                        this, ex);
        
        cleanupThread = null;
      }
    }
  }
}
