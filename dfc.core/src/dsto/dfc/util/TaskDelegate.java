package dsto.dfc.util;

import dsto.dfc.reflection.IDelegate;
import dsto.dfc.reflection.InvokeException;
import dsto.dfc.util.Scheduler.Task;


/**
 * A delegate that calls a wrapped delegate as a Task on the supplied 
 * scheduler
 * 
 * @author karunard
 *
 */
public class TaskDelegate extends Task implements IDelegate
{
  private IDelegate delegate;
  private Scheduler aScheduler;
  
  public TaskDelegate(Scheduler scheduler, IDelegate delegate)
  {
    this.aScheduler = scheduler;
    this.delegate = delegate;
  }
  
  @Override
  public String toString ()
  {
    return "TaskDelegate [" + delegate + "]";
  }

  public void run ()
  {
    delegate.invoke (IDelegate.EMPTY_ARGS);
  }

  public void invoke (Object... args) throws InvokeException
  {
    aScheduler.schedule (this);
  }
}
