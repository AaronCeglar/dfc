package dsto.dfc.util;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Basic support class for managing ClassInstantiation listeners.
 *
 * @see ClassInstantiationListener
 * @version $Revision$
 */
public class BasicClassInstantiationEventSource
  implements Serializable, Copyable
{
  private transient ArrayList listeners;

  public BasicClassInstantiationEventSource ()
  {
    // zip
  }

  public synchronized void addClassInstantiationListener (ClassInstantiationListener l)
  {
    if (listeners == null)
      listeners = new ArrayList ();

    listeners.add (l);
  }

  public synchronized void removeClassInstantiationListener (ClassInstantiationListener l)
  {
    if (listeners != null)
      listeners.remove (l);
  }

  public Object clone () throws CloneNotSupportedException
  {
    BasicClassInstantiationEventSource newObject =
      (BasicClassInstantiationEventSource)super.clone ();

    newObject.listeners = null;

    return newObject;
  }

  public void fireClassInstantiated (Object newInstance)
  {
    if (listeners != null)
    {
      ClassInstantiationEvent e = new ClassInstantiationEvent (newInstance);
      ClassInstantiationListener [] theListeners;

      synchronized (this)
      {
        theListeners = new ClassInstantiationListener [listeners.size ()];
        theListeners = (ClassInstantiationListener [])listeners.toArray (theListeners);
      }

      for (int i = 0; i < theListeners.length; i++)
       theListeners [i].instanceCreated (e);
    }
  }
}
