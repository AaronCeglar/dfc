package dsto.dfc.util;

import java.io.Serializable;

/**
 * Base class for enumerated types that have a string representation.
 * Provides sensible defaults for toString(), hashCode() and equals().
 * Also provides intelligent serialization support via a readResolve ()
 * that resolves serialized enum values to their equivalent instance in
 * getEnumValues ().  Superclasses that wish to use this feature must
 * provide a readResolve() implementation that calls super.readResolve().
 *
 * @version $Revision$
 */
public abstract class StringEnumerationValue
  extends AbstractEnumerationValue implements EnumerationValue, Serializable
{
  private static final long serialVersionUID = 592576433733544422L;

  protected String name;
  protected transient String text;

  /**
   * For beans compliance only.
   */
  public StringEnumerationValue ()
  {
    this.name = "<unset>";
    this.text = null;
  }

  /**
   * Create a new string enumeration value.
   *
   * @param name The human readable name of the value.
   *
   * @see #StringEnumerationValue(String,String)
   */
  public StringEnumerationValue (String name)
  {
    this.name = name;
    this.text = null;
  }

  /**
   * Create a new string enumeration value.
   *
   * @param name The logical name of the value.
   * @param text The human readable text for the value.  The text may
   * change without affecting the identity of the value, which is determined
   * by the name property.
   *
   * @see #StringEnumerationValue(String,String)
   */
  public StringEnumerationValue (String name, String text)
  {
    this.name = name;
    this.text = text;
  }

  public abstract EnumerationValue [] getEnumValues ();

  /**
   * Find the enumeration value with a given name.
   *
   * @param enumName The name to find.
   * @return The matching value, or null if not found.
   */
  public EnumerationValue findValue (String enumName)
  {
    int index = findIndex (enumName);

    if (index == -1)
      return null;
    else
      return getEnumValues () [index];
  }

  /**
   * Find the index of an enum value with a given name.
   *
   * @param enumName The name to find.
   * @return The index of the matching value in the enum values array, or -1
   * if not found.
   */
  public int findIndex (String enumName)
  {
    Object [] values = getEnumValues ();

    for (int i = 0; i < values.length; i++)
    {
      StringEnumerationValue value = (StringEnumerationValue)values [i];

      if (value.getName ().equals (enumName))
        return i;
    }

    return -1;
  }

  public String getName ()
  {
    return name;
  }

  /**
   * Use only during construction.
   */
  public void setName (String newValue)
  {
    name = newValue;
  }

  /**
   * Get the human readable text for the value.
   */
  public String getText ()
  {
    return text;
  }

  public String toString ()
  {
    return text != null ? text : name;
  }

  public int hashCode ()
  {
    return name.hashCode ();
  }

  public boolean equals (Object o)
  {
    if (null == o)
      return false;

    if (o.getClass ().equals (getClass ()))
    {
      StringEnumerationValue s = (StringEnumerationValue)o;

      return name.equals (s.name);
    } else
    {
      return false;
    }
  }
}
