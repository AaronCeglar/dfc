package dsto.dfc.util;

import dsto.dfc.logging.Log;

/**
 * Debugging utilities.
 *
 * @author Matthew
 * @version $Revision$
 */
public final class Debug
{
  private static boolean exceptionOnFailure = true;

  private Debug ()
  {
    // zip
  }

  public static void setExceptionOnFailure (boolean newValue)
  {
    exceptionOnFailure = newValue;
  }

  public static boolean isExceptionOnFailure ()
  {
    return exceptionOnFailure;
  }

  /**
   * @deprecated Use assert (condition, message, source) instead
   * @see #assertTrue(boolean,String,Object)
   */
  public static void assertTrue (boolean condition, String message)
  {
    assertTrue (condition, message, Debug.class);
  }

  public static void assertTrue (boolean condition, String message, Object source)
    throws AssertionFailedException
  {
    if (condition == false)
    {
      Log.internalError ("Assertion failed: " + message, source);

      if (exceptionOnFailure)
        throw new AssertionFailedException (message, source);
    }
  }
}
