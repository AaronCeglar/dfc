package dsto.dfc.util;

import java.util.ArrayList;

import dsto.dfc.logging.Log;

/**
 * Allows a thread to be multiplexed into several scheduled Runnable's. This is
 * essentially Matthew's simple version of java.util.Timer that (a) allows tasks
 * to re-schedule themselves inside their run () method (why did Sun actively
 * block that?) and (b) allows managed shutdown-and-join (no way to tell when a
 * cancelled Timer has finished running tasks).
 * 
 * @author Matthew Phillips
 */
public class MultiplexedThread extends Thread
{
  private ArrayList entries;
  private boolean shuttingDown;

  public MultiplexedThread ()
  {
    this.entries = new ArrayList ();
  }
  
  /**
   * Requests shutdown of the thread. If a runnable is executing it is
   * completed.
   */
  public void shutdown ()
  {
    shuttingDown = true;
  }
  
  /**
   * Schedule a runnable for execution.
   * 
   * @param runnable The runnable.
   * @param time The time to run at.
   */
  public synchronized void schedule (Runnable runnable, long time)
  {
    entries.add (new Entry (runnable, time));
  }
  
  public void run ()
  {
    while (!shuttingDown)
    {
      synchronized (this)
      {
        for (int i = entries.size () - 1; i >=0 && !shuttingDown; i--)
        {
          Entry entry = (Entry)entries.get (i);
          
          if (entry.time <= System.currentTimeMillis ())
          {
            entries.remove (i);
            
            try
            {
              entry.runnable.run ();
            } catch (Throwable ex)
            {
              Log.alarm ("Error in runnable", this, ex);
            }
          }
        }
      }
      
      try
      {
        sleep (10);
      } catch (InterruptedException ex)
      {
        Log.alarm ("Error in thread", this, ex);
      }
    }
  }
  
  private static class Entry
  {
    public long time;
    public Runnable runnable;

    public Entry (Runnable runnable, long time)
    {
      this.runnable = runnable;
      this.time = time;
    }
  }
}
