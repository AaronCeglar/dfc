package dsto.dfc.util;

/**
 * A unique object that only equals itself.  The hashCode () and
 * toString () methods may optionally be delegated to a proxy 'key'
 * object.
 *
 * @version $Revision$
 */
public class UniqueObject
{
  private Object key;
  
  public UniqueObject ()
  {
    this.key = null;
  }

  public UniqueObject (Object key)
  {
    this.key = key;
  }

  /**
   * Delegated to the key's equals () method.  Always returns false if
   * there is no key.
   */
  public boolean keyEquals (Object o)
  {
    if (key != null)
      return key.equals (o);
    else
      return false;
  }

  /**
   * Only true of other object is the same instance.
   */
  public boolean equals (Object o)
  {
    return this == o;
  }

  /**
   * Delegated to key (or superclass if no key).
   */
  public int hashCode ()
  {
    if (key != null)
      return key.hashCode ();
    else
      return super.hashCode ();
  }

  /**
   * Delegated to key (or superclass if no key).
   */
  public String toString ()
  {
    if (key != null)
      return key.toString ();
    else
      return super.toString ();
  }
} 
