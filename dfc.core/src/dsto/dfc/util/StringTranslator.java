package dsto.dfc.util;

import dsto.dfc.text.IStringTranslator;

/**
 * Translates any value to a string using toString ().
 * 
 * @author mpp
 * @version $Revision$
 */
public class StringTranslator implements ITranslator, IStringTranslator
{
  public static final StringTranslator INSTANCE = new StringTranslator ();

  public Object translate (Object object) throws IllegalArgumentException
  {
    return object == null ? null : object.toString ();
  }
  
  public Object fromString (String string)
    throws InvalidFormatException
  {
    return string;
  }
  
  public String toString (Object value)
    throws IllegalArgumentException
  {
    return value.toString ();
  }
}
