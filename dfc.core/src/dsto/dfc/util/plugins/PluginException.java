package dsto.dfc.util.plugins;

/**
 * The Exception thrown when an error occurs loading a plugin.
 *
 * @version $Revision$
 */
public class PluginException extends Exception
{
  private String plugin;
  private Throwable subException;

  public PluginException (String plugin, String message)
  {
    super (message);

    this.plugin = plugin;
    this.subException = null;
  }

  /**
   * Constructor.
   *
   * @param plugin The name of the plugin
   * @param subException The exception that triggered the error.
   */
  public PluginException (String plugin, Throwable subException)
  {
    super ("Failed to initialise plugin " + plugin + ": " + subException);

    this.plugin = plugin;
    this.subException = subException;
  }

  /**
   * The name of the plugin.
   */
  public String getPlugin ()
  {
    return plugin;
  }

  /**
   * The sub-exception.  May be null.
   */
  public Throwable getSubException ()
  {
    return subException;
  }
}
