package dsto.dfc.util.plugins;

import dsto.dfc.logging.Log;
import dsto.dfc.commandline.Option;

/**
 * A commmand line option class that enables plugin loading via a
 * "-plugin" (or just "-p") command line switch.
 *
 * @version $Revision$
 */
public class PluginOption extends Option
{
  /**
   * The no argument constructor uses '-p' or "--plugin" or "-plugin"
   * as the form for the option.
   */
  public PluginOption ()
  {
    this ('p', "plugin", true);
  }

  public PluginOption (char shortForm, String longForm, boolean wantsValue)
  {
    super (shortForm, longForm, true);
  }

  protected Object parseValue( String arg )
  {
    return arg;
  }

  /**
   * Set the name of the plugin to load to value, and load the the plugin.
   *
   * @param value the name of the plugin to load and initialise.
   */
  protected void setValue (String value)
  {
    super.setValue (value);

    PluginManager pluginManager = PluginManager.INSTANCE;

    try
    {
      pluginManager.addPlugin (value);
    } catch (PluginException ex)
    {
      if (ex.getSubException () == null)
        Log.alarm ("Error loading plugin", this, ex);
      else
        Log.alarm ("Exception in plugin " + ex.getPlugin () + ": " + ex.getMessage (), this, ex.getSubException ());
    }
  }
}
