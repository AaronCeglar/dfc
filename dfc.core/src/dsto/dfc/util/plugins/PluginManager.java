package dsto.dfc.util.plugins;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;

import dsto.dfc.logging.Log;
import dsto.dfc.net.URLSearchPath;
import dsto.dfc.util.Files;

/**
 * Manages dynamic loading of plugin classes contained in a JAR file.
 * A plugin JAR (usually with a ".jpi" extension) must have a
 * "Plugin-Class" property in its manifest specifying the plugin
 * initialisation class.  When loaded, the plugin class's<p>
 *
 * <pre>
 *   public static void initPlugin ()
 * </pre><p>
 *
 * method is called.  This method may perform any initialisation
 * required by the plugin.<p>
 *
 * <b>NOTE</b>: For plugins to work, the system must be using {@link
 * DfcClassLoader} to load the application.  See {@link
 * BootStrapLoader} for one way to accomplish this.
 *
 * @version $Revision$
 */
public class PluginManager
{
  public static final String PLUGIN_EXTENSION = "jpi";
  public static final FilenameFilter PLUGIN_FILENAME_FILTER =
    new PluginFilenameFilter ();
  public static final PluginManager INSTANCE = new PluginManager();

  protected ArrayList plugins = new ArrayList ();
  protected URLSearchPath searchPath = new URLSearchPath ();

  protected PluginManager ()
  {
    // zip
  }

  public void addSearchPathEntry (String directory)
  {
    searchPath.addPathEntry (directory);
  }

  /**
   * Find all plugins currently available on the plugin search path.
   */
  public String [] listPlugins ()
  {
    ArrayList pluginsList = new ArrayList ();
    List entries = searchPath.getEntries ();

    for (int i = 0; i < entries.size (); i++)
    {
      URL entry = (URL)entries.get (i);

      if (entry.getProtocol ().equals ("file"))
      {
        File directory = new File (entry.getPath ());
        File [] files = directory.listFiles (PLUGIN_FILENAME_FILTER);

        for (int j = 0; j < files.length; j++)
        {
          pluginsList.add (Files.removeExtension (files [j].getName ()));
        }
      }
    }

    String [] pluginArray = new String [pluginsList.size ()];
    pluginsList.toArray (pluginArray);

    return pluginArray;
  }

  /**
   * Get the loaded plugin names.
   */
  public String [] getPlugins ()
  {
    String [] pluginArray = new String [plugins.size ()];

    plugins.toArray (pluginArray);

    return pluginArray;
  }

  /**
   * Get the logical name for a plugin.  The logical name is the
   * plugin path name, minus any directories and the extension.
   */
  public static String getPluginName (String path)
  {
    String pluginName = path;

    // remove extension if any
    if (pluginName.endsWith ("." + PLUGIN_EXTENSION))
      pluginName = pluginName.substring (0, pluginName.lastIndexOf ('.'));

    // remove directories
    pluginName = pluginName.replace ('\\', '/');
    pluginName = pluginName.substring (pluginName.lastIndexOf ('/') + 1);

    return pluginName;
  }

  /**
   * Test whether a plugin is loaded.
   *
   * @param path A plugin path or logical name.
   * @return True if plugin is logical.
   */
  public boolean isPluginLoaded (String path)
  {
    return plugins.contains (getPluginName (path));
  }

  /**
   * Load a plugin and initialise it.  If plugin has already been loaded this
   * has no effect.
   *
   * @param path The path pointing to the plugin file.
   * @return The logical name by which the plugin is known.  This is the
   * plugin file name, minus any directories and the extension.
   *
   * @see #isPluginLoaded
   * @see #getPluginName
   */
  public String addPlugin (String path) throws PluginException
  {
    // sanity check to make sure the app is being loaded via the DFC
    // BootStrapLoader (ie we can dynamically add the plugin JAR to
    // the classpath).
    if (!(getClass ().getClassLoader () instanceof DfcClassLoader))
    {
      throw new PluginException
        (path, "Cannot add plugins when not using DfcClassLoader to " +
         "load application: plugin " + path + " not loaded");
    }

    String pluginName = getPluginName (path);

    if (isPluginLoaded (pluginName))
      return pluginName;

    String absPath = path;

    try
    {
      // add plugin extension if omitted
      if (!absPath.endsWith ("." + PLUGIN_EXTENSION))
        absPath = absPath + "." + PLUGIN_EXTENSION;

      absPath = searchPath.resolveToFile (absPath);

    } catch (NoSuchElementException ex)
    {
      throw new PluginException
        (pluginName, "Failed to find plugin with path \"" + absPath + "\"");
    }

    DfcClassLoader.INSTANCE.addJarFile (absPath);

    try
    {
      Log.info ("Starting " + pluginName + " plugin...", this);

      initPluginClass (absPath);

      Log.info ("Loaded " + pluginName + " plugin", this);
    } catch (Throwable ex)
    {
      ex = stripWrapperExceptions (ex);
      
      // directly throw a plugin exception generated by the plugin's init
      // method
      if (ex instanceof PluginException)
        throw (PluginException)ex;
      else
        throw new PluginException (pluginName, ex);
    }

    plugins.add (pluginName);

    return pluginName;
  }
  
  private static Throwable stripWrapperExceptions (Throwable ex)
  {
    Throwable subex = ex;
    
    for (;;)
    {
      if (subex instanceof InvocationTargetException)
      {
        subex = ((InvocationTargetException)subex).getTargetException ();
      } else if (subex instanceof ExceptionInInitializerError)
      {
        subex = ((ExceptionInInitializerError)subex).getException ();
      } else
      {
        break;
      }
    }
    
    return subex;
  }

  /**
   * Add a new entry to the plugin classpath.
   */
  public void addToPluginClasspath (String newPathEntry)
  {
    DfcClassLoader.INSTANCE.addJarFile (newPathEntry);
  }

  protected void initPluginClass (String path)
    throws IOException, IllegalAccessException, NoSuchMethodException,
           InvocationTargetException, ClassNotFoundException
  {
    Class c = null;

    c = Class.forName (getPluginClassName (path));
    Method m = c.getMethod ("initPlugin", (Class [])null);
    m.setAccessible (true);
    m.invoke (null, (Object [])null);
  }

  /**
   * Returns the name of the jar file plugin class, or null if no
   * "Plugin-Class" manifest attributes was defined.
   */
  public String getPluginClassName (String path) throws IOException
  {
    JarFile jarFile = new JarFile (new File (path));

    Attributes attr = jarFile.getManifest ().getMainAttributes ();
    
    jarFile.close ();

    return attr != null ? attr.getValue ("Plugin-Class") : null;
  }

  private static final class PluginFilenameFilter implements FilenameFilter
  {
    public PluginFilenameFilter ()
    {
      // zip
    }
    
    public boolean accept (File dir, String name)
    {
      return name.endsWith ("." + PLUGIN_EXTENSION);
    }
  }
}
