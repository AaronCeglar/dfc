package dsto.dfc.util.plugins;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Properties;

/**
 * BootStrapLoader loads and runs a main class, using {@link
 * DfcClassLoader} as the class loader.  This ensures DfcClassLoader
 * is the class loader for the application, which enables dynamic
 * additions to the classpath.<p>
 *
 * You should specify the main class as the value of the
 * "bootstrap.mainclass" property either in a property resource called
 * "/system.properties" (which must be accessible via
 * Class.getResourceAsStream ()) or on the command line using
 * "-Dbootstrap.mainclass=...".  As a side-effect, all properties in
 * system.properties are copied into the system-wide set (ie
 * accessible via System.getProperty ()).<p>
 *
 * eg: <code>bootstrap.mainclass = dsto.frob.Main</code><p>
 *
 * BootStrapLoader will load dsto.frob.Main and call its main ()
 * method as usual.
 *
 * @author Mofeed
 * @version $Revision$
 */
public class BootStrapLoader
{
  public static void main (String [] args)
  {
    DfcClassLoader dfcClassLoader = DfcClassLoader.INSTANCE;
    String mainClass;

    InputStream propertiesStr =
      BootStrapLoader.class.getResourceAsStream ("/system.properties");

    try
    {
      if (propertiesStr != null)
      {
        Properties newProps = new Properties ();

        newProps.load (propertiesStr);

        // load properties in system.properties into system-wide set
        Enumeration prop = newProps.elements ();
        Enumeration propName = newProps.propertyNames ();

        while (propName.hasMoreElements () && prop.hasMoreElements ())
        {
          String property = (String)propName.nextElement ();

          if (System.getProperty (property) == null)
            System.setProperty (property, (String)prop.nextElement ());
        }
      }
    } catch (IOException ex)
    {
      System.err.println ("ERROR: Bootstrap loader encountered an error loading system.properties: " + ex);

      System.exit (1);
    }

    mainClass = System.getProperty ("bootstrap.mainclass");

    if (mainClass == null)
    {
      System.err.println
        ("ERROR: bootstrap.mainclass is not defined.");

      System.err.println
        ("Either system.properties resource is missing or no -Dbootstrap.mainclass on command line.");

      System.exit (2);
    }

    try
    {
      Class cl = dfcClassLoader.loadClass(mainClass);
      Method m = cl.getMethod("main", new Class[] {args.getClass()});

      m.invoke(null, new Object[] {args});
    }catch (ClassNotFoundException e)
    {
      System.err.println ("ERROR: Bootstrap loader failed to load main class " + mainClass);

      System.exit (3);
    } catch (Exception e)
    {
      System.err.println ("ERROR: Bootstrap loader encountered an error while loading main class " + mainClass);

      e.printStackTrace (System.err);

      System.exit (4);
    }
  }
}
