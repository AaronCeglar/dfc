package dsto.dfc.util.plugins;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import dsto.dfc.util.DfcSystem;

/**
 * A class loader that supports dynamic addition to the classpath, including
 * JAR/ZIP files and directories. To avoid various problems, it does not load
 * any class files that are considered to be part of the system (ie java.*,
 * javax.* etc).
 * <p>
 *
 * When the DfcClassLoader is first started, it scans the system class path, and
 * adds all of the paths into its own search path.
 * <p>
 *
 * NOTE that this loader does not currently respect the order in which items are
 * added to the classpath, so if the same class is in several locations which
 * one gets loaded is not defined.
 *
 * @see BootStrapLoader
 *
 * @version $Revision$
 */
public class DfcClassLoader extends ClassLoader
{
  public static final DfcClassLoader INSTANCE = new DfcClassLoader ();

  private ArrayList pathsToCheck = new ArrayList ();
  private ArrayList jarFileArray = new ArrayList ();
  private ArrayList zipFileArray = new ArrayList ();

  private HashMap archiveCacheMap = new HashMap ();

  /**
   * Constructor is protected so as to make it a singleton. Use
   * DfcClassLoader.INSTANCE.
   */
  protected DfcClassLoader ()
  {
    StringTokenizer stringTok =
      new StringTokenizer (System.getProperty ("java.class.path"),
                           File.pathSeparator);

    while (stringTok.hasMoreElements ())
    {
      String path = stringTok.nextToken ();

      if (path.endsWith (".jar") || path.endsWith (".jpi"))
        addJarFile (path);
      else if (path.endsWith (".zip"))
        addZipFile (path);
      else
        addPath (path);
    }

    // Add the current directory to the class path, as some classes
    // assume that this is done.
    addPath (".");
  }

  /**
   * Adds a path to a jar file to the end of the search path.
   *
   * @param jarFilePath The path to the Jar file that you want added to the
   *          classloader path.
   */
  public void addJarFile (String jarFilePath)
  {
    String path;
    File file = new File (jarFilePath);

    if (!file.exists ())
    {
      System.err
        .println ("DFC Class Loader: Warning: File on class path does not exist: " +
                  jarFilePath);
      return;
    }

    path = file.getAbsolutePath ();

    jarFileArray.add (path);
  }

  /**
   * Adds a path to a zip file to the end of the search path.
   *
   * @param zipFilePath The path to the Zip file that you want added to the
   *          classloader path.
   */
  public void addZipFile (String zipFilePath)
  {
    String path;
    File file = new File (zipFilePath);

    if (!file.exists ())
    {
      System.err
        .println ("DFC Class Loader: Warning: File on class path does not exist: " +
                  zipFilePath);
      return;
    }

    path = file.getAbsolutePath ();

    zipFileArray.add (path);
  }

  /**
   * addPath adds a directory path to the search path. The path is added to the
   * end of the search path.
   *
   * @param dirPath The path to the directory that you want added to the
   *          classloader path.
   */
  public void addPath (String dirPath)
  {
    String newPath;
    newPath = (new File (dirPath)).getAbsolutePath ();
    pathsToCheck.add (newPath);
  }

  /**
   * Loads the class with the specified name. This method searches for classes
   * in the same manner as the loadClass(String, boolean) method. Calling this
   * method is equivalent to calling loadClass(name, false)
   *
   * @param name The name of the class to try to load.
   */
  @Override
  public Class loadClass (String name) throws ClassNotFoundException
  {
    return loadClass (name, false);
  }

  /**
   * Loads the class with the specified name. The default implementation of this
   * method searches for classes in the following order:
   *
   * 1 Call findLoadedClass(String) to check if the class has already been
   * loaded.
   *
   * 2 Call the findClass(String) method to find the class.
   *
   * 3 Call the loadClass method on the parent class loader. If that can't find
   * the class then you have a problem, b'cos it means that it is no-where to be
   * found.
   *
   * If the class was found using the above steps, and the resolve flag is true,
   * this method will then call the resolveClass(Class) method on the resulting
   * class object.
   *
   * @param name The name of the class to try to load.
   * @param resolve Flag set if you would like the class loader to attempt to
   *          resolve the class.
   * @exception ClassNotFoundException Thrown if the class couldn't be found.
   */
  @Override
  public synchronized Class loadClass (String name, boolean resolve)
    throws ClassNotFoundException
  {
    if (DfcSystem.isSystemClass (name))
      return getClass ().getClassLoader ().loadClass (name);

    if (name.endsWith ("DfcClassLoader"))
      return getClass ();

    Class cl = findLoadedClass (name);

    if (cl != null)
    {
      if (resolve)
        resolveClass (cl);
      return cl;
    }

    try
    {
      cl = findClass (name);
    } catch (SecurityException se)
    {
      cl = this.getClass ().getClassLoader ().loadClass (name);
    }

    if (resolve)
      resolveClass (cl);

    return cl;
  }

  /**
   * Look for a class in the defined Jar files, Zip files and directories.
   *
   * @param name The name of the class to load.
   * @returns The class that you wanted.
   * @exception ClassNotFoundException if the class couldn't be found.
   */
  @Override
  protected Class findClass (String name) throws ClassNotFoundException
  {
    Class cl = null;

    cl = checkJarFilesForClass (name);
    if (cl != null)
      return cl;

    cl = checkZipFilesForClass (name);
    if (cl != null)
      return cl;

    cl = checkPathsForClass (name);
    if (cl != null)
      return cl;

    throw new ClassNotFoundException (name);
  }

  /**
   * Searchs all of the registered jar files for the class.
   *
   * @param name The name of the class to load.
   * @return The Class that was being looked for.
   */
  private Class checkJarFilesForClass (String name)
  {
    String path = name.replace ('.', '/').concat (".class");
    Iterator i = jarFileArray.iterator ();
    JarFile jarFile;
    ZipEntry zipEntry;
    String jarFileName;

    while (i.hasNext ())
    {
      jarFileName = (String) i.next ();
      jarFile = (JarFile) archiveCacheMap.get (jarFileName);

      if (jarFile == null)
      {
        try
        {
          jarFile = new JarFile (jarFileName);
          archiveCacheMap.put (jarFileName, jarFile);
        } catch (IOException e)
        {
          e.printStackTrace ();
          continue;
        }
      }
      zipEntry = jarFile.getEntry (path);
      if (zipEntry == null)
        continue;
      try
      {
        InputStream inputStream = jarFile.getInputStream (zipEntry);
        DataInputStream dataInputStream = new DataInputStream (inputStream);
        int classLen = (int) zipEntry.getSize ();
        byte[] b = new byte[classLen];
        dataInputStream.readFully (b);
        return defineClass (name, b, 0, classLen);
      } catch (IOException e)
      {
        e.printStackTrace ();
        continue;
      }
    }
    return null;
  }

  /**
   * Searchs all of the registered zip files for the class.
   *
   * @param name The name of the class to load.
   * @return The Class that was being looked for.
   */
  private Class checkZipFilesForClass (String name)
  {
    String path = name.replace ('.', '/').concat (".class");
    Iterator i = zipFileArray.iterator ();
    ZipFile zipFile;
    ZipEntry zipEntry;
    String zipFileName;

    while (i.hasNext ())
    {
      zipFileName = (String) i.next ();

      zipFile = (ZipFile) archiveCacheMap.get (zipFileName);

      if (zipFile == null)
      {
        try
        {
          zipFile = new ZipFile (zipFileName);
          archiveCacheMap.put (zipFileName, zipFile);
        } catch (IOException e)
        {
          e.printStackTrace ();
          continue;
        }
      }
      zipEntry = zipFile.getEntry (path);
      if (zipEntry == null)
        continue;
      try
      {
        InputStream inputStream = zipFile.getInputStream (zipEntry);
        DataInputStream dataInputStream = new DataInputStream (inputStream);
        int classLen = (int) zipEntry.getSize ();
        byte[] b = new byte[classLen];
        dataInputStream.readFully (b);
        return defineClass (name, b, 0, classLen);
      } catch (IOException e)
      {
        e.printStackTrace ();
        continue;
      }
    }
    return null;
  }

  /**
   * Searchs all the registered directories for the class.
   *
   * @param name The name of the class to load.
   * @return The Class that was being looked for.
   */
  private Class checkPathsForClass (String name)
  {
    String classFileName = name.replace ('.', '/').concat (".class");
    Iterator i = pathsToCheck.iterator ();
    String pathToCheck;

    while (i.hasNext ())
    {
      pathToCheck = (String) i.next ();
      String fullPath = pathToCheck + "/" + classFileName;
      File classFile = new File (fullPath);
      if (!classFile.exists ())
        continue;
      try
      {
        InputStream inputStream = new FileInputStream (classFile);
        DataInputStream dataInputStream = new DataInputStream (inputStream);
        int classLen = (int) classFile.length ();
        byte[] b = new byte[classLen];
        dataInputStream.readFully (b);
        dataInputStream.close ();
        return defineClass (name, b, 0, classLen);
      } catch (IOException e)
      {
        e.printStackTrace ();
        continue;
      }
    }
    return null;
  }

  /**
   * Finds the resource with the given name. Ssearches jar files, zip files, and
   * then directories in that order.
   *
   * @param name The name of the resource.
   * @return a URL for reading the resource, or null if the resource couldn't be
   *         found.
   */
  @Override
  protected URL findResource (String name)
  {
    URL url = null;

    url = checkJarFilesForResource (name);
    if (url != null)
      return url;

    url = checkZipFilesForResource (name);
    if (url != null)
      return url;

    url = checkPathsForResource (name);

    return url;
  }

  /**
   * Searchs all the registered jar files for the resource.
   *
   * @param resource The name of the resource.
   * @return a URL for reading the resource, or null if the resource couldn' be
   *         found.
   */
  private URL checkJarFilesForResource (String resource)
  {
    JarFile jarFile;
    ZipEntry zipEntry;
    Iterator i = jarFileArray.iterator ();

    while (i.hasNext ())
    {
      String jarFilePath = (String) i.next ();

      jarFile = (JarFile) archiveCacheMap.get (jarFilePath);
      if (jarFile == null)
      {
        try
        {
          jarFile = new JarFile (jarFilePath);
          archiveCacheMap.put (jarFilePath, jarFile);
        } catch (IOException e)
        {
          e.printStackTrace ();
          continue;
        }
      }
      zipEntry = jarFile.getEntry (resource);
      if (zipEntry == null)
      {
        if (resource.charAt (1) == '/')
          zipEntry =
            jarFile.getEntry (resource.substring (1, resource.length ()));

        if (zipEntry == null)
          continue;
      }
      try
      {
        if (jarFilePath.startsWith ("/"))
          return new URL ("jar", "", "file:" + jarFilePath + "!/" + resource);
        else
          return new URL ("jar", "", "file:/" + jarFilePath + "!/" + resource);
      } catch (MalformedURLException e)
      {
        e.printStackTrace ();
      }
    }
    return null;
  }

  /**
   * Searchs all the registered zip files for the resource.
   *
   * @param resource The name of the resource.
   * @return a URL for reading the resource, or null if the resource couldn' be
   *         found.
   */
  private URL checkZipFilesForResource (String resource)
  {
    ZipFile zipFile;
    ZipEntry zipEntry;
    Iterator i = zipFileArray.iterator ();

    while (i.hasNext ())
    {
      String zipFilePath = (String) i.next ();
      zipFile = (ZipFile) archiveCacheMap.get (zipFilePath);
      if (zipFile == null)
      {
        try
        {
          zipFile = new ZipFile (zipFilePath);
          archiveCacheMap.put (zipFilePath, zipFile);
        } catch (IOException e)
        {
          e.printStackTrace ();
          continue;
        }
      }
      zipEntry = zipFile.getEntry (resource);
      if (zipEntry == null)
      {
        if (resource.charAt (1) == '/')
          zipEntry =
            zipFile.getEntry (resource.substring (1, resource.length ()));

        if (zipEntry == null)
          continue;
      }
      try
      {
        if (zipFilePath.startsWith ("/"))
          return new URL ("jar", "", "file:" + zipFilePath + "!/" + resource);
        else
          return new URL ("jar", "", "file:/" + zipFilePath + "!/" + resource);
      } catch (MalformedURLException e)
      {
        e.printStackTrace ();
      }
    }
    return null;
  }

  /**
   * Searchs all the registered directories for the resource.
   *
   * @param resource The name of the resource.
   * @return a URL for reading the resource, or null if the resource couldn' be
   *         found.
   */
  private URL checkPathsForResource (String resource)
  {
    String fileName = resource.replace ('.', '/');
    Iterator i = pathsToCheck.iterator ();
    URL url = null;

    while (i.hasNext ())
    {
      String path = (String) i.next ();
      String fullPath = path + "/" + fileName;
      File resourceFile = new File (fullPath);
      if (resourceFile.exists ())
      {
        try
        {
          url = resourceFile.toURI ().toURL ();
        } catch (MalformedURLException e)
        {
          e.printStackTrace ();
          continue;
        }
        return url;
      }
    }
    return null;
  }
}
