package dsto.dfc.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Reader;

/**
 * General file utilities
 *
 * @author Matthew
 * @version $Revision$
 */
public final class Files
{
  private static final int IN_TEXT = 0;
  private static final int IN_COMMENT = 1;
  private static final int IN_BACKSLASH = 2;
  private static final int EOL = 3;
  private static final int IN_NOTHING = 4;

  private Files ()
  {
    // zip
  }

  /**
   * Get the extension from a file's name.
   *
   * @param file The file to examine.
   * @return The extension, or "" if none.
   */
  public static String getExtension (File file)
  {
    return getExtension (file.getName ());
  }

  public static String getExtension (String name)
  {
    int dotIndex = name.lastIndexOf ('.');

    if (dotIndex == -1)
      return "";
    else
      return name.substring (dotIndex + 1, name.length ());
  }

  public static String removeExtension (File file)
  {
    return removeExtension (file.toString ());
  }

  public static String removeExtension (String fileName)
  {
    int dotIndex = fileName.lastIndexOf ('.');

    if (dotIndex == -1)
      return fileName;
    else
      return fileName.substring (0, dotIndex);
  }

  /**
   * Add a default extension to a file if it does not already have
   * one.
   *
   * @param file The file to examine.
   * @param extension The extension to add.
   * @return The modified file.
   */
  public static File addDefaultExtension (File file, String extension)
  {
    if (getExtension (file).equals (""))
      return new File (file.getPath () + "." + extension);
    else
      return file;
  }

  public static String addDefaultExtension (String file, String extension)
  {
    if (getExtension (file).equals (""))
      return file + "." + extension;
    else
      return file;
  }

  /**
   * Copy one file to another.
   *
   * @see #copyFile(String, String)
   */
  public static void copyFile (File srcFile, File trgFile)
    throws IOException
  {
    copyFile (srcFile.getPath (), trgFile.getPath ());
  }

  /**
   * Copy one file to another.
   *
   * @param srcFile The source file.
   * @param trgFile The target file.
   *
   * @throws IOException if an error occurs.
   */
  public static void copyFile (String srcFile, String trgFile)
    throws IOException
  {
    FileInputStream input = null;
    FileOutputStream output = null;

    byte [] buff = new byte [4096];

    try
    {
      input = new FileInputStream (srcFile);
      output = new FileOutputStream (trgFile);

      for (;;)
      {
        int bytesRead = input.read (buff);

        if (bytesRead == -1)
          break;

        output.write (buff, 0, bytesRead);
      }
    } catch (IOException ex)
    {
      // blow away partially copied file
      new File (trgFile).delete ();

      throw ex;
    } finally
    {
      if (input != null)
        input.close ();

      if (output != null)
        output.close ();
    }
  }

  /**
   * Read all of the content supplied by a Reader into a String.
   *
   * @param reader The reader.
   * @return The entire contents of reader as a String.
   * @throws IOException if an error occurs while reading.
   *
   * @see #readStringBuilder(Reader)
   */
  public static String readString (Reader reader)
    throws IOException
  {
    return readStringBuilder (reader).toString ();
  }

  /**
   * Read all of the content supplied by a Reader into a StringBuilder.
   *
   * @param reader The reader.
   * @return The entire contents of reader as a StringBuilder.
   * @throws IOException if an error occurs while reading.
   */
  public static StringBuilder readStringBuilder (Reader reader)
    throws IOException
  {
    char [] buff = new char[4096];
    StringBuilder string = new StringBuilder ();

    for (;;)
    {
      int len = reader.read (buff);

      if (len == -1)
        break;

      string.append (buff, 0, len);
    }

    return string;
  }

  /**
   * Read the next line from a "script-like stream", where script-like
   * streams are line-oriented text streams with "#" comments and \ as
   * a line-continuation operator, similar to Bourne shell scripts and
   * Java .property files. White space at the start and end of the
   * line is ignored, as are lines starting with "#".
   *
   * @param input The input stream.
   * @return The next line read (will never be "") or null if at end
   *         of stream.
   *
   * @throws IOException if an error occurs reading the stream.
   */
  public static String readScriptLine (Reader input)
    throws IOException
  {
    int c;
    int state = IN_NOTHING;
    StringBuilder line = new StringBuilder ();

    while (state != EOL && (c = input.read ()) != -1)
    {
      switch (c)
      {
        case '#':
          if (state == IN_NOTHING)
            state = IN_COMMENT;
          else if (state != IN_COMMENT)
            line.append ((char)c);
          break;
        case '\\':
          if (state == IN_TEXT || state == IN_NOTHING)
            state = IN_BACKSLASH;
          else if (state == IN_BACKSLASH)
            line.append ('\\');
          break;
        case '\n':
        case '\r':
          if (state == IN_TEXT)
            state = EOL;
          else if (state == IN_COMMENT)
            state = IN_NOTHING;
          else if (state == IN_BACKSLASH)
            state = IN_NOTHING;
          break;
        default:
          if ((state == IN_NOTHING && !Character.isWhitespace ((char)c)))
          {
            state = IN_TEXT;
          } else if (state == IN_BACKSLASH)
          {
            line.append ('\\');
            state = IN_TEXT;
          }

          if (state == IN_TEXT)
            line.append ((char)c);
      }
    }


    if (line.length () == 0)
      return null;
    else
      return line.toString ().trim ();
  }
}
