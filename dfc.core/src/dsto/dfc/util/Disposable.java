package dsto.dfc.util;

/**
 * Defines an object that supports explicit destruction.  Yes, this is
 * a nasty C++ like thing, and should be used very carefully.  It is
 * the usual way for a client to support explicit de-registration of
 * its bean listeners.
 *
 * @version $Revision$
 */
public interface Disposable
{
  /**
   * Dispose of the object (unregister listeners, close open resources
   * etc).  It should be safe to call this method more than once.
   *
   * Note for beans that support client event listeners: if there are
   * listeners registered when this is called, this method should do
   * nothing.
   */
  public void dispose ();
}
