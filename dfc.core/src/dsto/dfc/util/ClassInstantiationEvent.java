package dsto.dfc.util;

import java.util.EventObject;

/**
 * Defines a class instantiation event.
 *
 * @see ClassInstantiationListener
 * @author Matthew Phillips
 * @version $Revision$
 */
public class ClassInstantiationEvent extends EventObject
{
  /**
   * Creates a new <code>ClassInstantiationEvent</code> instance.
   *
   * @param newInstance The new instance of the class.  Becomes the
   * source of the event.
   */
  public ClassInstantiationEvent (Object newInstance)
  {
    super (newInstance);
  }
}
