package dsto.dfc.util;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.UndeclaredThrowableException;

import dsto.dfc.databeans.DataObjects;
import dsto.dfc.databeans.IDataObject;


/**
 * Utility methods for manipulating objects.
 *
 * @version $Revision: 1.20 $
 */
public final class Objects
{
  private Objects ()
  {
    // zip
  }

  /**
   * Test for equality, handling shortcut when o1 == o2 and when one or both
   * of the values are null.
   * 
   * @see #objectsEqualEx(Object, Object)
   */
  public static boolean objectsEqual (Object o1, Object o2)
  {
    return o1 == o2 || (o1 != null && o2 != null && o1.equals (o2));
  }
  
  /**
   * Extended version of objectsEqual () that does deep equality checking of
   * arrays.
   * 
   * @see #objectsEqual(Object, Object)
   */
  public static boolean objectsEqualEx (Object o1, Object o2)
  {
    if (o1 == o2 || (o1 != null && o1.equals (o2)))
    {
      return true;
    } else if (o1 != null && o2 != null)
    {
      // deep test for array equality
      Class cls = o1.getClass ();
      
      if (cls == o2.getClass () && cls.isArray ())
      {
        int length = Array.getLength (o1);
        
        if (length == Array.getLength (o2))
        {
          for (int i = 0; i < length; i++)
          {
            if (!objectsEqual (Array.get (o1, i), Array.get (o2, i)))
              return false;
          }
          
          return true;
        }
      }      
    }
    
    return false;
  }
  
  /**
   * Attempts to clone an object instance where generic cloning is
   * required but only an Object reference (with protected clone ()
   * access) is held.  If the object supports the Copyable
   * interface, this is used to directly invoke clone (), otherwise
   * reflection is used.  Standard Java classes (like String, Integer,
   * Color etc) which inexplicably do not support cloning are fudged
   * by using their constructors. {@link IDataObject}'s are cloned using
   * {@link DataObjects#deepClone(IDataObject)}.
   *
   * @exception CloneNotSupportedException if object is not cloneable
   * or clone () threw an exception (eg clone () is protected).
   * 
   * @see #simpleClone(Object)
   */
  public static Object cloneObject (Object object)
    throws CloneNotSupportedException
  {
    if (object == null)
      return null;

    if (object instanceof Copyable)
    {
      return ((Copyable)object).clone ();
    } else if (object instanceof String)
    {
      // String is not cloneable: fudge it
      return new String (object.toString ());
    } else if (object instanceof Integer)
    {
      return new Integer (((Integer)object).intValue ());
    } else if (object instanceof Boolean)
    {
      return new Boolean (((Boolean)object).booleanValue ());
    } else if (object instanceof IDataObject)
    {
      return DataObjects.deepClone ((IDataObject)object);
    } else if (object.getClass ().getName ().equals ("java.awt.Color"))
    {
      // NOTE: above uses class name rather than instanceof to avoid 
      // loading Color (and AWT support libs) if it's not being used.
      java.awt.Color color = (java.awt.Color)object;
      return new java.awt.Color (color.getRed (), color.getGreen (),
                                 color.getBlue (), color.getAlpha ());
    } else if (object.getClass ().equals (Collections.EMPTY_LIST.getClass ()))
    {
      return Collections.EMPTY_LIST;
    } else if (object.getClass ().equals (Collections.EMPTY_SET.getClass ()))
    {
      return Collections.EMPTY_SET;
    } else if (object.getClass ().equals (Collections.EMPTY_MAP.getClass ()))
    {
      return Collections.EMPTY_MAP;
    } else
    {
      return simpleClone (object);
    }
  }

  /**
   * Simple way to clone objects via dyanamic invokation of the clone () method.
   * 
   * @see #cloneObject(Object) 
   */
  public static Object simpleClone (Object object)
    throws CloneNotSupportedException
  {
    try
    {
      Method method = object.getClass ().getMethod ("clone", (Class [])null);
      return method.invoke (object, (Object [])null);
    } catch (InvocationTargetException ex)
    {
      throw new CloneNotSupportedException
        ("object's clone () threw exception: " + ex.getTargetException ());
    } catch (IllegalArgumentException ex)
    {
      // will not happen
      return null;
    } catch (ExceptionInInitializerError ex)
    {
      throw new CloneNotSupportedException
        ("exception during object's clone () init:" + ex);
    } catch (NoSuchMethodException ex)
    {
      throw new CloneNotSupportedException ("clone not accessible for " + object.getClass ());
    } catch (IllegalAccessException ex)
    {
      throw new CloneNotSupportedException ("clone not accessible for " + object.getClass ());
    }
  }

  /**
   * Deep clone a collection by cloning the collection itself and then
   * cloning each entry. The entries in ordered collections in the new
   * collection are added in the order defined by the default iterator.
   *
   * @param collection The collection to clone.
   * @return A deep copy of the collection.
   */
  public static Collection cloneCollection (Collection collection)
    throws CloneNotSupportedException
  {
    Collection copy = (Collection)cloneObject (collection);
    copy.clear ();

    for (Iterator i = collection.iterator (); i.hasNext (); )
      copy.add (cloneObject (i.next ()));

    return copy;
  }

  /**
   * Clone an object graph by serializing and then deserializing it. All
   * objects in the graph must implement java.io.Serializable.
   * 
   * @param object The object to clone.
   * @return A copy of object.
   * @throws CloneNotSupportedException if an error occurs while serializing or
   * serializing.
   */
  public static Object cloneBySerialization (Object object)
    throws CloneNotSupportedException
  {
    try
    {
      ByteArrayOutputStream data = new ByteArrayOutputStream (4 * 1024);
      ObjectOutputStream output = new ObjectOutputStream (data);
        
      output.writeObject (object);
      
      output.close ();
      
      ObjectInputStream input =
        new ObjectInputStream (new ByteArrayInputStream (data.toByteArray ()));
        
      Object copy = input.readObject ();
      
      input.close ();
      
      return copy;
    } catch (IOException ex)
    {
      throw new CloneNotSupportedException
        ("Failed to clone by serialization: " + ex);
    } catch (ClassNotFoundException ex)
    {
      // surely this this cannot happen...
      throw new CloneNotSupportedException
        ("Failed to find a class during clone: " + ex);
    }
  }
  
  /**
   * Unwrap one of Java's various exceptions that exist to wrap another one
   * (ie hide the real source of the problem).
   */
  public static Throwable unwrapException (Throwable ex)
  {
    boolean unwrapped = false;
    
    while (!unwrapped)
    {      
      // unwrap an InvocationTargetException
      if (ex instanceof InvocationTargetException)
        ex = ((InvocationTargetException)ex).getTargetException ();
      else if (ex instanceof UndeclaredThrowableException)
        ex = ((UndeclaredThrowableException)ex).getUndeclaredThrowable ();
      else
        unwrapped = true;
    }

    return ex;
  }
  
  /**
   * Shortcut to get the value of an int field from an object using reflection.
   * 
   * @param obj The object.
   * @param fieldName The field name. May be private.
   * @return The int value of field.
   * @throws SecurityException If the security manager bounces us.
   * @throws NoSuchFieldException Guess
   * @throws IllegalArgumentException If the field is not an int.
   */
  public static int getIntField (Object obj, String fieldName)
    throws SecurityException, NoSuchFieldException,
            IllegalArgumentException
  {
    Field field = obj.getClass ().getField (fieldName);
    field.setAccessible (true);
    
    try
    {
      return field.getInt (obj);
    } catch (IllegalAccessException ex)
    {
      throw new Error ("Should not be possible");
    }
  }

  /**
   * Shortcut to get the value of a long field from an object using reflection.
   * 
   * @param obj The object.
   * @param fieldName The field name. May be private.
   * @return The long value of field.
   * @throws SecurityException If the security manager bounces us.
   * @throws NoSuchFieldException Guess
   * @throws IllegalArgumentException If the field is not a long.
   */
  public static long getLongField (Object obj, String fieldName)
    throws SecurityException, NoSuchFieldException,
           IllegalArgumentException
  {
    Field field = obj.getClass ().getField (fieldName);
    field.setAccessible (true);
    
    try
    {
      return field.getLong (obj);
    } catch (IllegalAccessException ex)
    {
      throw new Error ("Should not be possible");
    }
  }

  /**
   * Instantiate a value of the given class.
   */
  public static Object instantiate (Class type)
    throws IllegalArgumentException
  {
    try
    {
      Constructor constuctor = type.getDeclaredConstructor ((Class [])null);
      
      if ((constuctor.getModifiers () & Modifier.PUBLIC) == 0)
        constuctor.setAccessible (true);
      
      return constuctor.newInstance ((Object [])null);
    } catch (InstantiationException ex)
    {
      IllegalArgumentException ex2 =
        new IllegalArgumentException (type + " cannot be instantiated: " + ex);
      
      ex2.initCause (ex);
      
      throw ex2;
    } catch (IllegalAccessException ex)
    {
      IllegalArgumentException ex2 =
        new IllegalArgumentException ("Cannot access constructor for " + type);
      
      ex2.initCause (ex);
      
      throw ex2;
    } catch (InvocationTargetException ex)
    {
      IllegalArgumentException ex2 =
        new IllegalArgumentException ("Exception in constructor for " + type);
      
      ex2.initCause (ex.getTargetException ());
      
      throw ex2;
    } catch (SecurityException ex)
    {
      IllegalArgumentException ex2 =
        new IllegalArgumentException
          ("Failed to make non-public constructor for " + type.getName () + " accessible");

      ex2.initCause (ex);

      throw ex2;
    } catch (NoSuchMethodException ex)
    {
      throw new IllegalArgumentException ("No default constructor for " + type);
    }
  }

  /**
   * Get a method using reflection, returning null if not found.
   */
  public static Method getMethod (Class theClass,
                                   String methodName,
                                   Class param)
  {
    Class [] params = new Class [] {param};
  
    try
    {
      return theClass.getMethod (methodName, params);
    } catch (NoSuchMethodException ex)
    {
      return null;
    }
  }

  /**
   * Search for a method with a given name on a class.
   */
  private static Method findMethodForClass (Class targetClass, String methodName)
  {
    Method [] methods = targetClass.getDeclaredMethods ();
  
    for (int i = 0; i < methods.length; i++)
    {
      Method method = methods [i];
  
      if (method.getName ().equals (methodName))
        return method;
    }
    
    return null;
  }

  /**
   * Search for a method taking a parameter compatible with a given
   * parameter class.  Searches the class, its superclasses and all
   * interfaces.
   */
  public static Method findMethod (Class targetClass,
                                      String methodName,
                                      Class paramClass)
  {
    // try this param class
    Method method = getMethod (targetClass, methodName, paramClass);
  
    if (method == null)
    {
      // try superclasses of param class
      if (!paramClass.isInterface () &&
          paramClass.getSuperclass () != null)
      {
        method = findMethod (targetClass, methodName,
                                          paramClass.getSuperclass ());
      }
  
      // try interfaces of param class
      if (method == null)
      {
        Class [] interfaces = paramClass.getInterfaces ();
  
        for (int i = 0; method == null && i < interfaces.length; i++)
        {
          method = findMethod (targetClass, methodName,
                                            interfaces [i]);
        }
      }
    }
  
    return method;
  }

  /**
   * Search the superclass/interface tree of a given class for a method with
   * a given name. 
   */
  public static Method findMethod (Class targetClass, String methodName)
  {
    // try this param class
    Method method = findMethodForClass  (targetClass, methodName);
  
    if (method == null)
    {
      // try interfaces of param class
      Class [] interfaces = targetClass.getInterfaces ();
  
      for (int i = 0; method == null && i < interfaces.length; i++)
        method = findMethod (interfaces [i], methodName);
    }
    
    if (method == null && targetClass.getSuperclass () != null)
    {  
      // try superclasses
      method = findMethod (targetClass.getSuperclass (), methodName);
    }
  
    return method;
  }
  
  public static Method findMethod (Class type,
                                   String methodName,
                                   Class [] argTypes)
  {
    Method [] methods = type.getDeclaredMethods ();
    Method method;
    
    for (int i = 0; i < methods.length; i++)
    {
      method = methods [i];
      
      if (methodMatches (method, methodName, argTypes))
        return method;
    }
    
    // try superclasses
    for (Class superClass = type.getSuperclass ();
         superClass != null;
         superClass = superClass.getSuperclass ())
    {
       if ((method = findMethod (superClass, methodName, argTypes)) != null)
         return method;
    }
    
    return null;
  }

  private static boolean methodMatches (Method method, String methodName,
                                        Class [] argTypes)
  {
    if (method.getName ().equals (methodName))
    {
      Class [] methodArgs = method.getParameterTypes ();
      
      if (methodArgs.length == argTypes.length)
      {
        for (int i = 0; i < methodArgs.length; i++)
        {
          if (!methodArgs [i].isAssignableFrom (argTypes [i]))
            return false;
        }
        
        return true;
      }
    }
    
    return false;
  }

  /**
   * Return just the name (minus the package) of an object's class.
   */
  public static String className (Object object)
  { 
    return className (object.getClass ());
  }
  
  /**
   * Return just the name (minus the package) of a class.
   */
  public static String className (Class<?> type)
  {
    String name = type.getName ();
    
    return name.substring (name.lastIndexOf ('.') + 1);
  }
}
