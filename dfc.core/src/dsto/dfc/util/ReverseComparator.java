package dsto.dfc.util;

import java.util.Comparator;

/**
 * Revereses the normal order of a Comparator.
 * 
 * @author Matthew Phillips
 */
public class ReverseComparator implements Comparator
{
  private Comparator comparator;
  
  public ReverseComparator (Comparator comparator)
  {
    this.comparator = comparator;
  }

  public int compare (Object o1, Object o2)
  {
    int cmp = comparator.compare (o1, o2);
    /*
     * We can't simply return -cmp, as -Integer.MIN_VALUE == Integer.MIN_VALUE.
     */
    return -(cmp | (cmp >>> 1));
  }
}
