package dsto.dfc.util;

import java.util.EventObject;

/**
 * An event fired by an object to indicate its state has changed in some way.
 * 
 * @author mpp
 * @version $Revision$
 */
public class StateChangeEvent extends EventObject
{
  public StateChangeEvent (Object source)
  {
    super (source);
  }
}
