package dsto.dfc.util;

/**
 * Used to indicate that a missing parameter value was detected. This is mostly
 * useful for methods that use dynamic parameter lists.
 * 
 * @author Matthew Phillips
 */
public class MissingParameterException extends IllegalArgumentException
{

  public MissingParameterException ()
  {
    super ();
  }

  public MissingParameterException (String s)
  {
    super (s);
  }

}
