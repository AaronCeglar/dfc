package dsto.dfc.util;

/**
 * Indicates an illegal format has been detected.
 * <p>
 * 
 * This is a "legacy" exception used primarily by the DFC Swing forms
 * library and the dfc.text library. It is similar to the newer
 * {@link InvalidFormatException} but carries a "source" reference to
 * help in UI binding and is unchecked to avoid having to wrap the
 * exception in cases where the input value is known to be valid. New
 * code should probably use InvalidFormatException.
 * 
 * @see InvalidFormatException
 * 
 * @version $Revision$
 */
public class IllegalFormatException extends RuntimeException
{
  private Object source;
  private Object value;
  private Class form;

  /**
   * Generate a message indicating that a value could not be converted
   * to a given class.
   */
  public IllegalFormatException (Object source, Object value, Class form)
  {
    super ("Illegal value \"" + (value == null ? "null" : value) + "\"");

    this.source = source;
    this.value = value;
    this.form = form;
  }

  public IllegalFormatException (Object source, Object value, Class form,
                                 Throwable ex)
  {
    super ("Illegal value \"" +
           (value == null ? "null" : value) + "\": " + ex.getMessage ());

    this.source = source;
    this.value = value;
    this.form = form;
  }

  public IllegalFormatException (Object source, String message,
                                 Object value, Class form)
  {
    super (message);

    this.source = source;
    this.value = value;
    this.form = form;
  }

  /**
   * Useful for rethrowing an IllegalFormatException with a new source.
   */
  public IllegalFormatException (Object source, IllegalFormatException ex)
  {
    super (ex.getMessage ());

    this.source = source;
    this.value = ex.getValue ();
    this.form = ex.getForm ();
  }

  public Object getSource ()
  {
    return source;
  }

  public Object getValue ()
  {
    return value;
  }

  public Class getForm ()
  {
    return form;
  }
}
