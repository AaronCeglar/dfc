package dsto.dfc.util;

import java.util.EventListener;

/**
 * Defines a listener for class instantiation events.  Class
 * instantiation events may be fired by a class whenever a new
 * instance is created, allowing clients to perform dynamic
 * customisation actions on each new instance of a given class.  The
 * addClassInstantiationListener () and
 * removeClassInstantiationListener () methods will be class-wide
 * (static).
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public interface ClassInstantiationListener extends EventListener
{
  public void instanceCreated (ClassInstantiationEvent e);
}
