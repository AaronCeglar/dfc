package dsto.dfc.util;

import java.io.IOException;

/**
 * Used to indicate an invalid format, eg while parsing a document and
 * finding an invalid field value. Extends IOException to make it
 * easier for clients to treat format errors in the same way as any
 * other failure to read data.
 * 
 * @author mpp
 * @version $Revision$
 * 
 * @see IllegalFormatException
 */
public class InvalidFormatException extends IOException
{
  public InvalidFormatException (String message)
  {
    this (message, null);
  }

  public InvalidFormatException (String message, Throwable ex)
  {
    super (message);
    
    if (ex != null)
      initCause (ex);
  }
}
