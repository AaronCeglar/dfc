package dsto.dfc.util;

/**
 * Thrown to indicate a debugging assertion has failed.
 * 
 * @see dsto.dfc.util.Debug
 * @author Matthew
 * @version $Revision$
 */
public class AssertionFailedException extends Error
{
  private Object source;
  
  public AssertionFailedException (String message, Object source)
  {
    super (message);
  }

  public Object getSource ()
  {
    return source;
  }
} 
