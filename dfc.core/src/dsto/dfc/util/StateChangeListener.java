package dsto.dfc.util;

import java.util.EventListener;

/**
 * Defines a listener to state change events. State change events are fired by
 * objects to indicate they have changed state in some way.
 * 
 * @author mpp
 * @version $Revision$
 */
public interface StateChangeListener extends EventListener
{
  public void stateChanged (StateChangeEvent e);
}
