package dsto.dfc.util;

import java.util.Properties;

/**
 * Simple extension of Java Properties class to add convenience
 * methods.
 *
 * @version $Revision$
 */
public class DfcProperties extends Properties
{
  public DfcProperties ()
  {
    super ();
  }

  public DfcProperties (Properties defaults)
  {
    super (defaults);
  }

  public boolean getBooleanProperty (String name, boolean defaultValue)
  {
    String value = getProperty (name);

    if (value != null)
      return value.trim ().equalsIgnoreCase ("TRUE");
    else
      return defaultValue;
  }

  public void setBooleanProperty (String name, boolean value)
  {
    setProperty (name, value ? "TRUE" : "FALSE");
  }

  public int getIntProperty (String name, int defaultValue)
  {
    String value = getProperty (name);

    if (value != null)
    {
      try
      {
        return Integer.parseInt (value);
      } catch (NumberFormatException ex)
      {
        return defaultValue;
      }
    } else
      return defaultValue;
  }

  public void setIntProperty (String name, int value)
  {
    setProperty (name, Integer.toString (value));
  }
} 
