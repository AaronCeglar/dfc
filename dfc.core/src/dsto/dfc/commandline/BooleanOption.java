package dsto.dfc.commandline;

/**
 * A simple kind of option that handles toggled options. ie either on or off.
 */
public class BooleanOption extends Option
{
  public BooleanOption( char shortForm, String longForm )
  {
    super(shortForm, longForm, false);
  }

  public Object getValue ()
  {
    if (value == null)
      return Boolean.FALSE;

    return Boolean.TRUE;
  }
}
