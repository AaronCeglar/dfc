package dsto.dfc.commandline;

/**
 * Thrown when the parsed command-line contains an option that is not
 * recognised. <code>getMessage()</code> returns an error string suitable
 * for reporting the error to the user (in English).
 */
public class UnknownOptionException extends Exception
{
  private String optionName = null;

  public UnknownOptionException (String optionName)
  {
    super ("Unknown option '" + optionName + "'");
    this.optionName = optionName;
  }

  public String getOptionName()
  {
    return this.optionName;
  }
}