package dsto.dfc.commandline;

/**
 * Thrown when an illegal or missing value is given by the user for
 * an option that takes a value. <code>getMessage()</code> returns
 * an error string suitable for reporting the error to the user (in
 * English).
 */
public class IllegalOptionValueException extends Exception
{
  private Option option;
  private String value;

  public IllegalOptionValueException (Option opt, String value)
  {
    super ("Illegal value '" + value + "' for option -" +
           opt.shortForm () + "/--" + opt.longForm ());
    this.option = opt;
    this.value = value;
  }

  public Option getOption ()
  {
    return this.option;
  }

  public String getValue ()
  {
    return this.value;
  }
}