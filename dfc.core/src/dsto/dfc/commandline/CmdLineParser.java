package dsto.dfc.commandline;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Vaguely GNU-compatible command-line options parser. Has short (-v) and
 * long-form (--verbose) option support, and also allows options with
 * associated values (-d 2, --debug 2, --debug=2).
 */
public class CmdLineParser
{
  private String[] remainingArgs = null;
  private Hashtable options = new Hashtable(10);
  private ArrayList optionArray = new ArrayList();

  public CmdLineParser()
  {
    // zip
  }

  /**
   * Add an option as one of the possible legal options to use. This adds the
   * longForm and shortForm version of the option.
   *
   * @param opt The option to be added as a legal Option
   */
  public final void addOption( Option opt )
  {
    this.options.put("-" + opt.shortForm(), opt);
    this.options.put("-" + opt.longForm(), opt);
    this.options.put("--" + opt.longForm(), opt);
    this.optionArray.add(opt);
  }

  /**
   * Get an array of all the possible options.
   */
  public final Option[] getOptions ()
  {
    Option[] temp = new Option[optionArray.size()];
    for (int idx=0 ; idx < temp.length; idx++)
    {
      temp[idx] = (Option)optionArray.get(idx);
    }
    return temp;
  }

  /**
   * Return the arguments that do not correspond to a particular option.
   */
  public final String[] getRemainingArgs()
  {
    return this.remainingArgs;
  }

  /**
   * This is the meat of the command line parser. This does all the parsing of the
   * command line arguements and sets the right values.
   *
   * @param argv The array of strings that you want parsed.
   */
  public void parse( String[] argv )
    throws IllegalOptionValueException, UnknownOptionException
  {
    Vector otherArgs = new Vector();
    int position = 0;
    while ( position < argv.length )
    {
      String curArg = argv[position];
      if ( curArg.startsWith("-") )
      {
        if ( curArg.equals("--") ) // end of options
        {
          position += 1;
          break;
        }
        String valueArg = null;

        // Handle --option=value or -option=value
        int equalsPos = curArg.indexOf("=");
        if ( equalsPos != -1 )
        {
          valueArg = curArg.substring(equalsPos+1);
          curArg = curArg.substring(0,equalsPos);
        }

        Option opt = (Option)this.options.get(curArg);
        if ( opt == null )
          throw new UnknownOptionException(curArg);

        if ( opt.wantsValue() )
        {
          if ( valueArg == null )
          {
            position += 1;
            if ( position < argv.length )
            {
              valueArg = argv[position];
            }
          }
          opt.getValue(valueArg);
          opt.setValue(valueArg);
        }
        else
        {
          opt.getValue(null);
        }
        position += 1;
      }
      else
      {
        break;
      }
    }
    for ( ; position < argv.length; ++position )
    {
      otherArgs.addElement(argv[position]);
    }

    this.remainingArgs = new String[otherArgs.size()];
    int i = 0;
    for (Enumeration e = otherArgs.elements(); e.hasMoreElements(); ++i)
      this.remainingArgs[i] = (String)e.nextElement();
  }
}