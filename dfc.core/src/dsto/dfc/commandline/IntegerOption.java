package dsto.dfc.commandline;

/**
 * This type of option handles Integer arguments.
 */
public class IntegerOption extends Option
{
  public IntegerOption (char shortForm, String longForm)
  {
    super (shortForm, longForm, true);
  }

  /**
   * @exception IllegalOptionValueException thrown if arg is not an
   *              Integer.
   */
  protected Object parseValue (String arg)
    throws IllegalOptionValueException
  {
    try
    {
      return new Integer (arg);
    } catch (NumberFormatException e)
    {
      throw new IllegalOptionValueException (this, arg);
    }
  }

  public Object getValue ()
  {
    Object obj = null;
    try
    {
      obj = parseValue (value);
    } catch (Exception e)
    {
      // todo should be handling this better
      e.printStackTrace ();
    }
    
    return obj;
  }
}
