package dsto.dfc.commandline;

import java.util.ArrayList;
import java.util.Properties;

/**
 * Parser for "-Dproperty=value" command line arguments that emulates the
 * processing that the "java" command does. This is useful for allowing -D
 * options in the application's argument list when modifying the command that
 * launched the VM isn't possible.
 * 
 * @author Matthew Phillips
 */
public final class PropertyParser
{
  private PropertyParser ()
  {
    // zip
  }

  /**
   * Parse the "-Dproperty=value" arguments from a list of arguments into
   * the system property set (ie System.getProperties ()).
   * 
   * @param args The arguments.
   * @return The arguments that aren't -D switches.
   * 
   * @see #parseOptions(String[], Properties)
   */
  public static String [] parseOptions (String []  args)
  {
    return parseOptions (args, System.getProperties ());
  }
  
  /**
   * Parse the "-Dproperty=value" arguments from a list of arguments into a
   * property set.
   * 
   * @param args The arguments.
   * @param properties The property set to modify.
   * @return The arguments that aren't -D switches.
   */
  public static String [] parseOptions (String []  args,
                                        Properties properties)
  {
    ArrayList otherOptions = new ArrayList (args.length);
    
    for (int i = 0; i < args.length; i++)
    {
      String arg = args [i];
      
      if (arg.startsWith ("-D"))
      {
        String property = "";
        String value = "";

        int equalsIndex = arg.indexOf ('=');
        
        if (equalsIndex == -1)
          equalsIndex = arg.length ();
        
        if (arg.length () > 2)
          property = arg.substring (2, equalsIndex).trim ();
          
        if (equalsIndex < arg.length ())
          value = arg.substring (equalsIndex + 1).trim ();
        
        if (property.length () > 0)
        {
          if (value.length () == 0)
            properties.remove (property);
          else
            properties.put (property, value);
        }
      } else
      {
        otherOptions.add (arg);
      }
    }
    
    String [] otherOptionsArray = new String [otherOptions.size ()];
    otherOptions.toArray (otherOptionsArray);
    
    return otherOptionsArray;
  }
}
