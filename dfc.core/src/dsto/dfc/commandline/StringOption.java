package dsto.dfc.commandline;

/**
 * Title:        Command Line Parser
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:
 * @author Mofeed Shahin (Based on code by Steve Purcell)
 * @version 1.0
 */

/**
 * This is the simplist type of Option, which simply handles string arguements.
 */
public class StringOption extends Option
{
  public StringOption( char shortForm, String longForm )
  {
    super(shortForm, longForm, true);
  }
  protected Object parseValue( String arg )
  {
    return arg;
  }

  public Object getValue ()
  {
    return value;
  }
}
