package dsto.dfc.commandline;

/**
* Representation of a command-line option
*/
public abstract class Option
{
  private String shortForm = null;
  private String longForm = null;
  private boolean wantsValue = false;
  protected String value = null;

  /**
   * @param shortForm A single character representation of the option. eg 'f' for "-f"
   * @param longForm A string representation of the option. eg "file" for "--file"
   * @param wantsValue If this is set to true then the option requires a value after it. eg "--file blah.txt"
   */
  protected Option( char shortForm, String longForm, boolean wantsValue )
  {
    if ( longForm == null )
      throw new IllegalArgumentException("null arg forms not allowed");
    this.shortForm = new String(new char[]{shortForm});
    this.longForm = longForm;
    this.wantsValue = wantsValue;
  }

  /**
   * return the shortForm representation of this option.
   */
  public String shortForm()
  {
    return this.shortForm;
  }

  /**
   * return the longForm representation of this option.
   */
  public String longForm()
  {
    return this.longForm;
  }

  /**
  * Tells whether or not this option wants a value
  */
  public boolean wantsValue()
  {
    return this.wantsValue;
  }

  /**
   * Returns the value of this option if the option actually has a value associated
   * with it, or else it returns Boolean.TRUE.
   *
   * @exception IllegalOptionValueException This gets thrown if the arg is null.
   */
  public final Object getValue( String arg )
    throws IllegalOptionValueException
  {
    if ( this.wantsValue )
    {
      if ( arg == null )
      {
        throw new IllegalOptionValueException(this,"");
      }
      return this.parseValue(arg);
    }
    else
    {
      return Boolean.TRUE;
    }
  }

  public Object getValue ()
  {
    return value;
  }

  protected void setValue (String value)
  {
    this.value = value;
  }

  /**
  * Override to extract and convert an option value passed on the
  *  command-line
  */
  protected Object parseValue( String arg )
    throws IllegalOptionValueException
  {
    return null;
  }
}