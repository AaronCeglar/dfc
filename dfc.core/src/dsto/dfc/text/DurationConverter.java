package dsto.dfc.text;

import java.text.DecimalFormat;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import dsto.dfc.util.InvalidFormatException;

import dsto.dfc.text.IStringTranslator;

/**
 * Converts time durations between string form and seconds form (eg between
 * "2 minutes" and 120).
 * 
 * @author mpp
 * @version $Revision$
 */
public class DurationConverter implements IStringTranslator
{
  private static final DecimalFormat NUMBER_FORMAT = new DecimalFormat ("00");
  
  private static final int MINUTE_SECS = 60;
  private static final int HOUR_SECS = 60 * MINUTE_SECS;
  private static final int DAY_SECS = 24 * HOUR_SECS;
  private static final int WEEK_SECS = 7 * DAY_SECS;
  private static final int MONTH_SECS = 4 * WEEK_SECS;
  
  private static Pattern pattern;
  
  /** Singleton instance: useful if using as a
   * {@link dsto.dfc.util.ITranslator}. */
  public static final DurationConverter INSTANCE = new DurationConverter ();
  
  private DurationConverter ()
  {
    // don't allow it to be instantiated
  }
  
  public Object fromString (String string)
    throws InvalidFormatException
  {
    return new Integer (convert (string));
  }
  
  public String toString (Object value)
    throws IllegalArgumentException
  {
    if (value instanceof Integer)
      return convert (((Integer)value).intValue ());
    else
      throw new IllegalArgumentException ("Input must be a number");
  }

  //
//  /**
//   * {@link ITranslator} interface implementation. If object is a Number,
//   * converts to an String, otherwise converts object.toString () to a
//   * number.
//   */
//  public Object translate (Object object)
//    throws InvalidFormatException
//  {
//    if (object instanceof Number)
//      return convert (((Number)object).intValue ());
//    else
//      return new Integer (convert (object.toString ()));
//  }

  public static int convert (String durationExpr)
    throws InvalidFormatException
  {
    durationExpr = durationExpr.trim ();
    
    if (durationExpr.length () == 0)
      throw new InvalidFormatException ("Duration is empty");

    if (pattern == null)
    {
      try
      {
        pattern =
          Pattern.compile
            ("\\s*((\\d+) *([smhdw][^\\d\\s]*)|(\\d+):(\\d+)(:(\\d+))?|(.+))",
             Pattern.CASE_INSENSITIVE);
  
      } catch (PatternSyntaxException ex)
      {
        throw new Error ("Bad pattern: " + ex);
      }
    }

    long duration = 0;
    Matcher matcher = pattern.matcher (durationExpr);
//    PatternMatcherInput input = new PatternMatcherInput (durationExpr);
    
    while (matcher.matches ())
    {
      MatchResult result = matcher.toMatchResult ();
      long subDuration = 0;
      
      if (result.group (2) != null)
      {
        subDuration = parseInt (result.group (2));
        
        String unit = result.group (3);
        
        if (unit.startsWith ("mo"))
          subDuration *= MONTH_SECS;
        else if (unit.startsWith ("m"))
          subDuration *= MINUTE_SECS;
        else if (unit.startsWith ("h"))
          subDuration *= HOUR_SECS;
        else if (unit.startsWith ("d"))
          subDuration *= DAY_SECS;
        else if (unit.startsWith ("w"))
          subDuration *= WEEK_SECS;
        
      } else if (result.group (4) != null)
      {
        subDuration = parseInt (result.group (4)) * HOUR_SECS;
        subDuration += parseInt (result.group (5)) * MINUTE_SECS;
        
        if (result.group (7) != null)
          subDuration += parseInt (result.group (7));
        
      } else
      {
        String invalidStr = result.group (8);
        String message;
        
        if (Character.isDigit (invalidStr.charAt (0)))
          message = "Time units are \"secs\", \"mins\", \"hours\", \"days\" or \"weeks\"";
        else
          message = "Number of seconds, weeks, etc missing";

        throw new InvalidFormatException (message);
      }
      
      duration += subDuration;
    }

    // sanity check
    if (duration > Integer.MAX_VALUE)
      throw new InvalidFormatException
        ("Duration \"" + durationExpr + "\" is too large");
    
    return (int)duration;
  }

  private static int parseInt (String string)
    throws InvalidFormatException
  {
    try
    {
      return Integer.parseInt (string);
    } catch (NumberFormatException ex)
    {
      throw new InvalidFormatException (ex.getMessage ());
    }
  }

  public static String convert (int durationSecs)
  {
    StringBuffer duration = new StringBuffer ();
    int secs = durationSecs;
    
    int months = secs / MONTH_SECS;
    secs %= MONTH_SECS;
    int weeks = secs / WEEK_SECS;
    secs %= WEEK_SECS;
    int days = secs / DAY_SECS;
    secs %= DAY_SECS;
    int hours = secs / HOUR_SECS;
    secs %= HOUR_SECS;
    int mins = secs / MINUTE_SECS;
    secs %= MINUTE_SECS;

    if (durationSecs < MINUTE_SECS)
    {
      if (secs == 1)
        duration.append (secs).append (" second");
      else
        duration.append (secs).append (" seconds");
    } else if (durationSecs < HOUR_SECS)
    {
      if (mins == 1)
        duration.append (mins).append (" minute");
      else
        duration.append (mins).append (" minutes");
    } else if (durationSecs < DAY_SECS)
    {
      // use "x hour(s)" form if minutes is 0
      if (mins == 0)
      {
        duration.append (hours);
        
        if (hours == 1)
          duration.append (" hour");
        else
          duration.append (" hours");
      } else
      {
        duration.append (NUMBER_FORMAT.format (hours));
        duration.append (":");
        duration.append (NUMBER_FORMAT.format (mins));
      }
    } else
    {
      if (months == 1)
        duration.append (months).append (" month ");
      else if (months > 1)
        duration.append (months).append (" months ");

      if (weeks == 1)
        duration.append (weeks).append (" week ");
      else if (weeks > 1)
        duration.append (weeks).append (" weeks ");

      if (days != 0)
      {
        if (days == 1)
          duration.append (days).append (" day ");
        else
          duration.append (days).append (" days ");
      }

      if (mins != 0 || hours != 0)
      {
        duration.append (NUMBER_FORMAT.format (hours));
        duration.append (":");
        duration.append (NUMBER_FORMAT.format (mins));
      }
    }

    return duration.toString ().trim ();
  }
}
