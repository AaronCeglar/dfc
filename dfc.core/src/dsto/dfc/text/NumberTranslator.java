package dsto.dfc.text;

import java.security.InvalidParameterException;

import dsto.dfc.util.InvalidFormatException;

/**
 * Translates numbers to/from strings.
 * 
 * @author mpp
 * @version $Revision$
 */
public class NumberTranslator implements IStringTranslator
{
  public static final NumberTranslator BYTE = new NumberTranslator (Byte.class);
  public static final NumberTranslator INTEGER = new NumberTranslator (Integer.class);
  public static final NumberTranslator SHORT = new NumberTranslator (Short.class);
  public static final NumberTranslator LONG = new NumberTranslator (Long.class);
  public static final NumberTranslator FLOAT = new NumberTranslator (Float.class);
  public static final NumberTranslator DOUBLE = new NumberTranslator (Double.class);
  
  private Class numberClass;
  
  public NumberTranslator (Class numberClass)
  {
    this.numberClass = numberClass;
    
    if (!Number.class.isAssignableFrom (numberClass))
      throw new InvalidParameterException (numberClass + " is not a number");
  }
  
  public Object fromString (String object)
    throws InvalidFormatException
  {
    try
    {
      if (numberClass.equals (Integer.class))
        return new Integer (object);
      else if (numberClass.equals (Float.class))
        return new Float (object);
      else if (numberClass.equals (Double.class))
        return new Double (object);
      else if (numberClass.equals (Long.class))
        return new Long (object);
      else if (numberClass.equals (Short.class))
        return new Short (object);
      else if (numberClass.equals (Byte.class))
        return new Byte (object);
      else
        throw new Error (numberClass + " is not a number");
    } catch (NumberFormatException ex)
    {
      throw new InvalidFormatException (notNumericMessage (object));
    }
  }
  
  private String notNumericMessage (Object value)
  {
    String name = numberClass.getName ();
    String prefix = numberClass == Integer.class ? "an " : "a ";
    
    return "\"" + value + "\" is not " + prefix +
            name.substring (name.lastIndexOf ('.') + 1).toLowerCase ();
  }

  public String toString (Object o)
    throws IllegalArgumentException
  {
    return o.toString ();
  }
}
