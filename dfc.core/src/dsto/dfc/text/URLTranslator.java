package dsto.dfc.text;

import java.net.MalformedURLException;
import java.net.URL;

import dsto.dfc.util.InvalidFormatException;

public class URLTranslator implements IStringTranslator
{

  public Object fromString (String string)
    throws InvalidFormatException
  {
    try
    {
      return new URL (string);
    } catch (MalformedURLException ex)
    {
      throw new InvalidFormatException ("Invalid URL: " + ex.getMessage ());
    }
  }

  public String toString (Object value)
    throws IllegalArgumentException
  {
    return value.toString ();
  }
}
