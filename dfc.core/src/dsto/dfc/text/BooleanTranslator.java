package dsto.dfc.text;

import dsto.dfc.util.InvalidFormatException;

/**
 * Converts boolean to/from string format. Allowable is "true",
 * "false", "yes" and "no" in any case.
 * 
 * @author Matthew Phillips
 */
public class BooleanTranslator implements IStringTranslator
{
  public Object fromString (String string)
    throws InvalidFormatException
  {
    string = string.toLowerCase ();
    
    if (string.equals ("true") || string.equals ("yes"))
      return Boolean.TRUE;
    else if (string.equals ("false") || string.equals ("no"))
      return Boolean.FALSE;
    else
      throw new InvalidFormatException
        ("\"" + string + "\" is not a valid boolean");
  }

  public String toString (Object value)
    throws IllegalArgumentException
  {
    if (value instanceof Boolean)
      return value.toString ();
    else
      throw new IllegalArgumentException ("Not a boolean");
  }
}
