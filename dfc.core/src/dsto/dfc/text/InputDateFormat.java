package dsto.dfc.text;

import java.text.SimpleDateFormat;

/**
 * An date format that makes for use as an input format. Supports dates like
 * "Sep 17, 2004 12:00:01", and shorter forms.<p>
 * 
 * Examples:<p>
 * 
 * <pre>
 *   25/12/2004 12:00:00 (and shorter)
 *   Sep 16, 2004 12:00:00 (and shorter)
 * </pre>

 * @author Matthew Phillips
 */
public class InputDateFormat extends MultiDateFormat
{
  protected static SimpleDateFormat[] format =
  {
    new SimpleDateFormat ("dd/MM/yyyy HH:mm:ss"),
    new SimpleDateFormat ("dd/MM/yyyy HH:mm"),
    new SimpleDateFormat ("dd/MM/yyyy"),
    new SimpleDateFormat ("dd/MM"),

    new SimpleDateFormat ("dd MMM yyyy HH:mm:ss"),
    new SimpleDateFormat ("dd MMM yyyy HH:mm"),
    new SimpleDateFormat ("dd MMM yyyy"),
    new SimpleDateFormat ("dd MMM"),
    
    new SimpleDateFormat ("MMM dd yyyy KK:mm:ssa"),
    new SimpleDateFormat ("MMM dd yyyy HH:mm:ss"),
    new SimpleDateFormat ("MMM dd yyyy HH:mm"),
    new SimpleDateFormat ("MMM dd yyyy"),
    new SimpleDateFormat ("MMM dd"),

    new SimpleDateFormat ("MMM dd KK:mm:ssa"),
    new SimpleDateFormat ("MMM dd HH:mm:ss"),
    new SimpleDateFormat ("MMM dd HH:mm"),

    new SimpleDateFormat ("KK:mm:ssa"),
    new SimpleDateFormat ("HH:mm:ss"),
    new SimpleDateFormat ("KK:mma"),
    new SimpleDateFormat ("HH:mm"),
    
    new SimpleDateFormat ("KKa"),
  };
  
  static
  {
    for (int i = 0; i < format.length; i++)
      format [i].setLenient (false);
  }

  public InputDateFormat ()
  {
    super (format);
  }
}