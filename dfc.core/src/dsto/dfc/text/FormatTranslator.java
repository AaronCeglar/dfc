package dsto.dfc.text;

import java.text.Format;
import java.text.ParseException;

import dsto.dfc.util.InvalidFormatException;

/**
 * A string translator implemented by a Java {@link Format} instance.
 * 
 * @author Matthew Phillips
 */
public class FormatTranslator implements IStringTranslator
{
  private Format format;

  public FormatTranslator (Format format)
  {
    this.format = format;
    
  }
  public Object fromString (String string)
    throws InvalidFormatException
  {
    try
    {
      return format.parseObject (string);
    } catch (ParseException ex)
    {
      InvalidFormatException ex2 = new InvalidFormatException ("Invalid format: " + ex.getMessage ());
      
      ex2.initCause (ex);
      
      throw ex2;
    }
  }

  public String toString (Object value)
    throws IllegalArgumentException
  {
    return format.format (value);
  }
}
