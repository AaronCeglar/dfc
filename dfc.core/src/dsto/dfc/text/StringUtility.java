package dsto.dfc.text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.net.URL;

import dsto.dfc.util.InvalidFormatException;
import dsto.dfc.util.StringTranslator;

/**
 * Utilities for manipulating strings.
 *
 * @version $Revision$
 */
public final class StringUtility
{
  public static final String ILLEGAL_CHARACTERS = "^:?[]/\\=+<>;\",*|\u0000";
  public static final List<String> ILLEGAL_FILENAMES;
  static 
  {
    List<String> illegals = 
      new ArrayList (Arrays.asList ("CON", "NIL", "PRN", "AUX"));
    for (int i = 1; i < 10; i++)
    {
      illegals.add ("COM" + i);
      illegals.add ("LPT" + i);
    }
    ILLEGAL_FILENAMES = illegals;
  }
  
  private static Map translators;

  private StringUtility ()
  {
    // zip
  }

  public static String [] split (String str)
  {
    return split (str, " ");
  }

  /**
   * Split a string into an array of substrings using a delimiter.<p>
   *
   * <b>NB</b> This is different to (JDK1.4+)
   * {@link java.lang.String#split(java.lang.String) String.split(String)}
   * for which the delimiter parameter is, in fact, a <em>regular
   * expression</em>.
   *
   * @see #join(Object[], String)
   * @see java.lang.String#split(java.lang.String)
   */
  public static String [] split (String str, String delimiter)
  {
    ArrayList elements = new ArrayList ();
    int start = 0;
    int end = -1;

    while (start < str.length ())
    {
      end = str.indexOf (delimiter, start);

      if (end == -1)
        end = str.length ();

      elements.add (str.substring (start, end));

      start = end + 1;
    }

    String [] elementArray = new String [elements.size ()];
    elements.toArray (elementArray);

    return elementArray;
  }

  /**
   * Splits a {@link java.lang.String String} up using the delimiter chars in
   * <code>delimiters</code> into an array of {@link java.lang.String Strings}.
   * The delimiters are included in the {@link java.lang.String Strings} by
   * default.
   *
   * @param string The {@link java.lang.String String} to split up.
   * @param delimiters The delimiter chars to use.
   * @return An array of {@link java.lang.String Strings}, which include the
   *         delimiter chars.
   * @see #split(String, char[], boolean)
   */
  public static String[] split (String string, char[] delimiters)
  {
    return split (string, delimiters, true);
  }


  /**
   * Splits a {@link java.lang.String String} up using the delimiter chars in
   * <code>delimiters</code> into an array of {@link java.lang.String Strings}.
   * The delimiters are included in the {@link java.lang.String Strings} if
   * <code>includeDelimiters</code> is true.
   *
   * @param string The {@link java.lang.String String} to split up.
   * @param delimiters The delimiter chars to use.
   * @param includeDelimiters If true, the delimiters are included in the
   *                          resulting {@link java.lang.String Strings}.
   * @return An array of {@link java.lang.String Strings}, which may or may not
   *         include the delimiter chars depending on if
   *         <code>includeDelimiters</code> is true or false.
   * @see #split(String, char[])
   */
  public static String[] split (String string, char[] delimiters,
                                boolean includeDelimiters)
  {
    List substrings = new ArrayList ();
    StringBuilder buffer = new StringBuilder ();

    for (int i = 0; i < string.length (); i++)
    {
      char c = string.charAt (i);
      if (! in (c, delimiters))
        buffer.append (c);
      else
      {
        if (includeDelimiters)
          buffer.append (c);
        substrings.add (buffer.toString ());
        buffer.delete (0, buffer.length ());
      }
    }
    return (String[]) substrings.toArray (new String[]{});
  }

  /**
   * Checks to see if <code>c</code> is in the array <code>arr</code>.
   *
   * @param c The char to search for in the array.
   * @param arr The array to search for the char in.
   * @return True if <code>c</code> is in <code>arr</code> and false otherwise.
   */
  private static boolean in (char c, char[] arr)
  {
    for (int i = 0; i < arr.length; i++)
      if (arr[i] == c)
        return true;
    return false;
  }

  /**
   * Join an array of objects into single string.
   *
   * @param objects The objects to join.
   * @param delimiter The string to add between each pair of strings.
   *
   * @see #split
   */
  public static String join (Object[] objects, String delimiter)
  {
    StringBuilder joinedObjects = new StringBuilder (100);

    for (int i = 0; i < objects.length; i++)
    {
      if (i != 0)
        joinedObjects.append (delimiter);

      joinedObjects.append (objects [i]);
    }

    return joinedObjects.toString ();
  }

  /**
   * Join an array of objects into single string.
   *
   * @param strings The objects to join.
   * @param delimiter The string to add between each pair of strings.
   *
   * @see #split
   */
  public static String join (List strings, String delimiter)
  {
    Object[] objectsArray = new Object[strings.size ()];
    strings.toArray (objectsArray);

    return StringUtility.join (objectsArray, delimiter);
  }
  
  /**
   * Concatenates a number of Objects ({@link #toString()} will be called
   * on them unless they're null) together, as a convenient way to
   * build a String. E.g.
   * <pre>
   *   String url = concat ("http://", host, ':', port, "/path/to/", dir);
   * </pre>
   * 
   * @param values The values to concatenate (typically Strings).
   * @return A bigger String made up of the values given.
   */
  public static String concat (Object... values)
  {
    StringBuilder sb = new StringBuilder ();

    for (Object o : values)
      sb.append (o == null ? "null" : o);

    return sb.toString ();
  }

  /**
   * Capitalise the first character of a string.
   */
  public static String capitaliseFirst (String str)
  {
    if (str.length () == 0)
      return str;
    else if (str.length () == 1)
      return str.toUpperCase ();
    else
    {
      StringBuilder buffer = new StringBuilder ();
      buffer.append (Character.toUpperCase (str.charAt (0)));
      buffer.append (str.substring (1));

      return buffer.toString ();
    }
  }

  /**
   * Replace all occurrences of one string with another.
   *
   * @param srcString The string to be transformed.
   * @param replaceStr The string to replace.
   * @param replaceWithStr The string to replace all occurrences of
   * replaceStr.
   * @return The transformed string.
   */
  public static String replace (String srcString,
                                String replaceStr,
                                String replaceWithStr)
  {
    StringBuilder result = null;

    int start = 0;
    int end = 0;

    do
    {
      end = srcString.indexOf (replaceStr, start);

      if (end != -1)
      {
        if (result == null)
          result = new StringBuilder (100);

        result.append (srcString.substring (start, end));
        result.append (replaceWithStr);

        start = end + replaceStr.length ();
      } else if (result != null)
      {
        result.append (srcString.substring (start, srcString.length ()));
      }
    } while (start < srcString.length () && end != -1);

    if (result == null)
      return srcString;
    else
      return result.toString ();
  }

  /**
   * Expand symbolic variables embedded within a string. Each
   * occurrence of a variable such as ${var} (alternatively can use
   * @{var}) in the string will be expanded to the result of
   * <code>symbols.get ("var")</code> or "" if var is not defined in
   * the property set.
   *
   * @param str The string to expand.
   * @param symbols The symbol definitions.
   * @return The expanded string.
   */
  public static String expandSymbols (String str, Map symbols)
  {
    return expandSymbols (str, symbols, "@$", true);
  }

  /**
   * Expand symbolic variables embedded within a string. Each
   * occurrence of a variable such as ${var} in the string will be
   * expanded to the result of <code>symbols.get ("var")</code> or ""
   * if var is not defined in the property set.
   *
   * @param str The string to expand.
   * @param symbols The symbol definitions.
   * @param symbolPrefixes The set of character prefixes that
   *          variables may begin with (eg "$%").
   * @param useDelimeters True if {}'s are allowed as symbol name
   *          delimeters, allowing multi-character names. When true,
   *          single character symbols may still be specified without
   *          {}'s.
   * @return The expanded string.
   */
  public static String expandSymbols (String str,
                                      Map symbols,
                                      String symbolPrefixes,
                                      boolean useDelimeters)
  {
    // only run expand if at least one symbol is present
    if (indexOfChar (str, symbolPrefixes) == -1)
      return str;

    StringBuilder buffer = new StringBuilder (str.length () * 2);

    int index = 0;      // current index within str
    int symStart = -1;  // start of next symbol (>= index)
    int symEnd = -1;    // end of next symbol (> symStart)

    for (;;)
    {
      symStart = indexOfChar (str, symbolPrefixes, index);

      if (symStart != -1)
      {
        boolean multiCharSymbol;

        // append text between index and start of symbol
        buffer.append (str.substring (index, symStart));

        // handle delimeter
        symStart++;

        if (useDelimeters &&
            symStart < str.length () &&
            str.charAt (symStart) == '{')
        {
          multiCharSymbol = true;

          symStart++;

          // find end of symbol
          symEnd = str.indexOf ('}', symStart);
        } else
        {
          multiCharSymbol = false;

          // symbol is a single character
          symEnd = symStart + 1;
        }

        try
        {
          String symbol = str.substring (symStart, symEnd);
          String value = (String)symbols.get (symbol);

          if (value != null)
          {
            buffer.append (value);
          } else
          {
            // no value for symbol: uncomment below to add it unchanged
            // buffer.append ("${").append (symbol).append ('}');
          }

          // advance to end of symbol
          if (multiCharSymbol)
            index = symEnd + 1;
          else
            index = symEnd;

        } catch (IndexOutOfBoundsException ex)
        {
          // unterminated or empty symbol

          // append symbol char as text
          buffer.append (symbolPrefixes.charAt (0));

          // add delimeter as text
          if (multiCharSymbol)
            buffer.append ('{');

          index = symStart;
        }
      } else
      {
        // no symbol start found, add remaining characters and exit
        buffer.append (str.substring (index));
        break;
      }
    }

    return buffer.toString ();
  }

  /**
   * Return the first character index in a string that is one of a specified
   * set of characters.
   *
   * @param str The string to search.
   * @param chars The set of characters to match.
   * @return The first index of one of chars in str, or -1 if no match.
   * @see #indexOfChar(String, String, int)
   */
  public static int indexOfChar (String str, String chars)
  {
    return indexOfChar (str, chars, 0);
  }

  /**
   * Return the first character index in a string that is one of a specified
   * set of characters.
   *
   * @param str The string to search.
   * @param chars The set of characters to match.
   * @param start The starting index to search from.
   * @return The first index of one of chars in str, or -1 if no match.
   */
  public static int indexOfChar (String str, String chars, int start)
  {
    for (int i = start; i < str.length (); i++)
    {
      for (int j = 0; j < chars.length (); j++)
      {
        if (chars.charAt (j) == str.charAt (i))
          return i;
      }
    }

    return -1;
  }

  /**
   * Turn a string like "HelloThereFrodo" into "Hello There Frodo".
   */
  public static String wordifyCaps (String str)
  {
    StringBuilder buff = new StringBuilder ();

    for (int i = 0; i < str.length (); i++)
    {
      char c = str.charAt (i);

      if (i != 0 && Character.isUpperCase (c))
        buff.append (' ');

      buff.append (c);
    }

    return buff.toString ();
  }

    /**
   * Test of string contains only hexadecimal characters (0-9,a-f).
   */
  public static boolean isHexString (String string)
  {
    for (int i = 0; i < string.length (); i++)
    {
      char c = Character.toLowerCase (string.charAt (i));

      if (!((c >= '0' && c <= '9') || (c >= 'a' || c <= 'f')))
        return false;
    }

    return true;
  }

  /**
   * Escape a given set of special characters in a string using '\' as the
   * escape character.
   *
   * @param str The string to escape.
   * @param chars The set of special characters to escape (eg "'). The escape
   * character itself is implicitly part of this set.
   *
   * @return The escaped string, or the original string if no special
   * characters were present.
   *
   * @see #escapeCharacters(String, String, char)
   */
  public static String escapeCharacters (String str, String chars)
  {
    return escapeCharacters (str, chars, '\\');
  }

  /**
   * Escape a given set of special characters in a string.
   *
   * @param str The string to escape.
   * @param chars The set of special characters to escape (eg "'). The escape
   * character itself is implicitly part of this set.
   * @param escapeChar The escape character (eg '\').
   *
   * @return The escaped string, or the original string if no special
   * characters were present.
   */
  public static String escapeCharacters (String str, String chars, char escapeChar)
  {
    StringBuilder result = null;

    int index = 0;
    char chr;

    while (index < str.length ())
    {
      chr = str.charAt (index);

      if (chr == escapeChar || chars.indexOf (chr) != -1)
      {
        if (result == null)
        {
          result = new StringBuilder (str.length () * 2);

          // copy string up until now
          result.append (str.substring (0, index));
        }

        result.append (escapeChar);
      }

      if (result != null)
        result.append (chr);

      index++;
    }

    if (result == null)
      return str;
    else
      return result.toString ();
  }

  /**
   * Truncate a string to a given max length.
   *
   * @param string The string to truncate. May be null.
   * @param maxLength The max length of the returned string. If longer, the
   * string is truncated with "...". The minimum string length will never be
   * less than 3.
   */
  public static String truncate (String string, int maxLength)
  {
    if (string == null)
      return null;
    else if (string.length () <= maxLength)
      return string;
    else if (maxLength > 3)
      return string.substring (0, maxLength - 3) + "...";
    else
      return "...";
  }

  /**
   * Capitalises all characters preceeded by a space, plus the first character
   * of <code>sentence</code>.
   *
   * @param title The sentence, each word of which to capitalise.
   * @return A copy of <code>sentence</code>, but with each word capitalised.
   */
  public static String capitaliseAllWords (String title)
  {
    if (title.length () == 0)
      return "";

    StringBuilder sb = new StringBuilder ();
    sb.append ((""+title.charAt (0)).toUpperCase ());
    for (int i = 1; i < title.length (); i++)
    {
      sb.append (title.charAt (i));
      if (sb.charAt (i - 1) == ' ')
        sb.setCharAt (i, (""+title.charAt (i)).toUpperCase ().charAt (0));
    }

    return sb.toString ();
  }

  /**
   * Generate a string containing count number of character c.
   */
  public static String characters (char c, int count)
  {
    StringBuilder str = new StringBuilder (count);

    for (int i = count; i > 0; i--)
      str.append (c);

    return str.toString ();
  }
  
  /**
   * Pads the given String, <code>init</code>, to the given length with
   * spaces, or returns the String intact.
   * 
   * @param init The String to start with.
   * @param length The desired length.
   * @return The String padded out to <code>length</code> with spaces if it
   *         is shorter than <code>length</code> or just the String itself.
   */
  public static String pad (String init, int length)
  {
    return pad (init, length, " ");
  }
  
  /**
   * Pads the given String, <code>init</code>, to the given length with
   * copies of the given <code>pattern</code>, or returns the String intact.
   * If the pattern is <code>null</code> or has length 0, a single space is
   * used. If the multiples of copies of the pattern needed for the padding
   * is not a whole number (e.g. one character's space needs padding but the
   * pattern is two character's long), then the pattern will be truncated to
   * ensure the returned String is the right length.
   * 
   * @param init The String to start with.
   * @param length The desired length.
   * @param pattern The filler pattern.
   * @return The String padded out to <code>length</code> with repeated copies
   *         of the <code>pattern</code> if it is shorter than
   *         <code>length</code> or just the String itself.
   */
  public static String pad (String init, int length, String pattern)
  {
    if (init.length () >= length)
      return init;
    
    if (pattern == null || pattern.length () == 0)
      pattern = " ";

    StringBuilder sb = new StringBuilder (init);
    while (sb.length () < length)
      sb.append (pattern);
    
    if (sb.length () != length)
      sb.setLength (length);

    return sb.toString ();
  }

  /**
   * Uses {@link #translatorFor(Class)} to convert a string to a given
   * value.
   *
   * @param type The target type to convert to.
   * @param string The string to convert.
   *
   * @return an instance of type created by parsing the string.
   *
   * @throws InvalidFormatException if an error occurs parsing.
   */
  public static Object fromString (Class type, String string)
    throws InvalidFormatException
  {
    return translatorFor (type).fromString (string);
  }

  /**
   * Uses {@link #translatorFor(Class)} to convert a value to a
   * string. This may be different from simply calling toString()
   * depending on which translator is registered for the value class.
   *
   * @param value The value to covert.
   *
   * @return The string value.
   *
   * @throws IllegalArgumentException if value has no associated
   *           translator.
   */
  public static String toString (Object value)
    throws IllegalArgumentException
  {
    return translatorFor (value.getClass ()).toString (value);
  }

  /**
   * Shortcut to test whether a translator is registered for a given
   * type.
   *
   * @see #translatorFor(Class)
   */
  public static boolean hasTranslatorFor (Class type)
  {
    synchronized (StringUtility.class)
    {
      if (translators == null)
        initTranslators ();

      return translators.containsKey (type);
    }
  }

  /**
   * Get a registered string translator for a given type.
   *
   * @param type The type to look for.
   *
   * @return The translator for type.
   *
   * @throws IllegalArgumentException if there is no translator for
   *           type.
   *
   * @see #hasTranslatorFor(Class)
   */
  public static IStringTranslator translatorFor (Class type)
    throws IllegalArgumentException
  {
    IStringTranslator translator;

    synchronized (StringUtility.class)
    {
      if (translators == null)
        initTranslators ();

      translator = (IStringTranslator)translators.get (type);
    }

    if (translator != null)
      return translator;
    else
      throw new IllegalArgumentException ("No string translator for " + type);
  }

  private static void initTranslators ()
  {
    translators = new HashMap ();

    translators.put (Byte.class, NumberTranslator.BYTE);
    translators.put (Short.class, NumberTranslator.SHORT);
    translators.put (Integer.class, NumberTranslator.INTEGER);
    translators.put (Long.class, NumberTranslator.LONG);
    translators.put (Float.class, NumberTranslator.FLOAT);
    translators.put (Double.class, NumberTranslator.DOUBLE);
    translators.put (String.class, new StringTranslator ());
    translators.put (Date.class, new FormatTranslator (new InputDateFormat ()));
    translators.put (URL.class, new URLTranslator ());
    translators.put (Boolean.class, new BooleanTranslator ());
  }

  /**
   * Convert the given string into a multi-line word-wrapped string.
   * The function does not cut the words so the tooltip created will
   * generally be slightly wider than the given width.
   *
   * @param string The string to word wrap
   * @param width  The interval used to wrap words
   *
   * @return A string containing a word wrapped version of the given string.
   */
  public static String wrappedHtml (String string, int width)
  {
    StringBuilder buff = new StringBuilder ("<html>");

    int from = 0;
    int to = 0;
    boolean firstLine = true;

    while (from < string.length ())
    {
      // Make sure the "to" index isn't out of bound
      to = from + Math.min (string.length () - from, width);

      // Increment the "to" index at the end of the next word
      to = Math.max (to, string.indexOf (" ", to));

      if (!firstLine)
        buff.append ("<br>");

      firstLine = false;

      buff.append (string.substring (from, to));

      from = to;
    }

    buff.append ("</html>");

    return buff.toString ();
  }

  /**
   * Creates a new filename based on the one provided with illegal filename
   * characters replaced with "_" and illegal filenames modified. The aim is
   * for cross-platform compatibility of filenames.<p>
   *
   * <strong>NB</strong> This is intended for filenames only, not including
   * directory paths.<p>
   *
   * <strong>NB</strong> Does not cater for filename length - that's your
   * own problem.
   *
   * @see "http://support.grouplogic.com/?p=1607"
   * @see #ILLEGAL_CHARACTERS
   * @see #ILLEGAL_FILENAMES
   *
   * @param filename The filename to start with.
   * @return A file-system friendly version of <code>filename</code>.
   */
  public static String cleanFilename (String filename)
  {
    return cleanFilename (filename, "_");
  }

  /**
   * Creates a new filename based on the one provided with illegal filename
   * characters replaced with the replacement String and illegal filenames
   * modified. The aim is for cross-platform compatibility of filenames.<p>
   *
   * <strong>NB</strong> This is intended for filenames only, not including
   * directory paths.<p>
   *
   * <strong>NB</strong> Does not cater for filename length - that's your
   * own problem.
   *
   * @see "http://support.grouplogic.com/?p=1607"
   * @see #ILLEGAL_CHARACTERS
   * @see #ILLEGAL_FILENAMES
   *
   * @param filename The filename to start with.
   * @param replacement The String to use in place of illegal characters.
   * @return A file-system friendly version of <code>filename</code>.
   * 
   * @throws IllegalArgumentException if <code>replacement</code> contains
   *                                  illegal characters.
   */
  public static String cleanFilename (String filename, String replacement)
  {
    // guard against stupidity
    for (int i = 0; i < replacement.length (); i++)
    {
      char c = replacement.charAt (i);
      if (ILLEGAL_CHARACTERS.indexOf (c) != -1)
      {
        String message = "Character " + c + 
          " in replacement String is also an invalid filename character.";
        throw new IllegalArgumentException (message);
      }
    }

    StringBuilder sb = new StringBuilder (filename.length ());
    for (int i = 0; i < filename.length (); i++)
    {
      char c = filename.charAt (i);
      if (ILLEGAL_CHARACTERS.indexOf (c) != -1)
        sb.append (replacement);
      else
        sb.append (c);
    }

    String soFar = sb.toString ();
    for (String bad : ILLEGAL_FILENAMES)
    {
      if (soFar.equals (bad))
      {
        sb.append (replacement);
        break;
      }
    }

    return sb.toString ();
  }

  public static int count (String string, String pattern)
  {
    int count = 0;
    Pattern p = Pattern.compile (pattern);
    Matcher m = p.matcher (string);

    while (m.find ())
      count++;
    
    return count;
  }
}
