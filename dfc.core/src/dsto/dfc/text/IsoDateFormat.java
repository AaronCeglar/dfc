package dsto.dfc.text;

import java.text.SimpleDateFormat;

/**
 * An ISO8601 (ish) date format. Supports dates like
 * "2001-02-01 10:00:00 UTC+8", and shorter forms.<p>
 * 
 * Examples:<p>
 * 
 * <pre>
 *   "2001"
 *   "2001-01-02"
 *   "2001-01-02 10:14:01" (uses local timezone)
 *   "2001-01-02 10:14:01 UTC+9:30"
 * </pre>

 * See http://www.iso.org/iso/en/prods-services/popstds/datesandtime.html.
 * 
 * @author Matthew Phillips
 */
public class IsoDateFormat extends MultiDateFormat
{
  private static final ThreadLocal instance = new ThreadLocal ()
  {
    protected Object initialValue ()
    {
      return new IsoDateFormat ();
    }
  };
  
 /**
  * Create a table of possible formats for W3C ISO8601 Profile
  */
  private static SimpleDateFormat [] createFormat ()
  {
    SimpleDateFormat [] format = new SimpleDateFormat []
    {
      new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss.S Z"),
      new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss.S zzz" ),
      new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss.S" ),
      new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss Z" ),
      new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss zzz" ),
      new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" ),
      new SimpleDateFormat( "yyyy-MM-dd HH:mm Z" ),
      new SimpleDateFormat( "yyyy-MM-dd HH:mm zzz" ),
      new SimpleDateFormat( "yyyy-MM-dd HH:mm" ),
      new SimpleDateFormat( "yyyy-MM-dd" ),
      new SimpleDateFormat( "yyyy-MM" ),
      new SimpleDateFormat( "yyyy" )
    };
    
    /*
     * strict (don't allow hour 25 and other curiosities).
     */
    for ( int i = 0; i < format.length; i++ )
      format[ i ].setLenient( false );
    
    return format;
  }
  
  /**
   * Get a shared, thread-local instance. This should not be cached,
   * since this instance is local to the current thread.
   */
  public static IsoDateFormat get ()
  {
    return (IsoDateFormat)instance.get ();
  }
  
  /**
   * Create a new instance. Consider using {@link #get()} instead.
   */
  public IsoDateFormat ()
  {
    super (createFormat ());
  }
}