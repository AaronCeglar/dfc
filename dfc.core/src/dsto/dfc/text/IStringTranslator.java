package dsto.dfc.text;

import dsto.dfc.util.ITranslator;
import dsto.dfc.util.InvalidFormatException;

/**
 * Interface for translators that convert to/from string format.
 * 
 * @see dsto.dfc.util.ITranslator
 * @see FormatTranslator
 * 
 * @author Matthew Phillips
 */
public interface IStringTranslator
{
  /**
   * Convert a value to a string.
   * 
   * @param value The value to convert. Null is not guaranteed to be
   *          valid.
   * @return The string equivalent.
   * 
   * @throws IllegalArgumentException if value is an object type
   *           understood by the converter.
   */
  public String toString (Object value)
    throws IllegalArgumentException;
  
  /**
   * Convert/parse a string into the object type supported by this
   * converter.
   * 
   * @param string The string to convert.
   * @return The converted object.
   * 
   * @throws InvalidFormatException if the string is not in a format
   *           parsable by the converter.
   */
  public Object fromString (String string)
    throws InvalidFormatException;
  
  /**
   * Adapts a IStringTranslator to the one-way {@link ITranslator}
   * interface. Uses {@link IStringTranslator#fromString(String)} for
   * translation.
   * 
   * @see ToString
   */
  public static class FromString implements ITranslator
  {
    private IStringTranslator translator;
    
    public FromString (IStringTranslator translator)
    {
      this.translator = translator;  
    }
    
    public Object translate (Object object)
      throws InvalidFormatException
    {
      if (object instanceof String)
        return translator.fromString ((String)object);
      else
        throw new InvalidFormatException (object + " is not a string");
    }
  }
  
  /**
   * Adapts a IStringTranslator to the one-way {@link ITranslator}
   * interface. Uses {@link IStringTranslator#toString(Object)} for
   * translation.
   * 
   * @see FromString
   */
  public static class ToString implements ITranslator
  {
    private IStringTranslator translator;
    
    public ToString (IStringTranslator translator)
    {
      this.translator = translator;  
    }
    
    public Object translate (Object object)
      throws InvalidFormatException
    {
      try
      {
        return translator.toString  (object);
      } catch (IllegalArgumentException ex)
      {
        throw new InvalidFormatException (object + " is not a valid format", ex);
      }
    }
  }
}
