package dsto.dfc.databeans;

import java.util.EventObject;

/**
 * The information included as part of an IDataObject property change
 * notification.
 * 
 * @author mpp
 * @version $Revision$
 */
public class PropertyEvent extends EventObject
{
  public final PropertyPath path;
  public final Object oldValue;
  public final Object newValue;
  public final boolean transientProperty;
  
  /**
   * Create a new PropertyEvent.
   * 
   * @param source The source object that the property is a part of.
   * @param property The property name.
   * @param oldValue The old value of the property.
   * @param newValue The new value of the property.
   * @param transientProperty True if the property is transient (ie need not
   * be persisted across sessions). For example, IDataBean's set this to true
   * when a default property is changed.
   * 
   * @see #PropertyEvent(IDataObject, PropertyPath, Object, Object, boolean)
   */
  public PropertyEvent (IDataObject source, Object property,
                        Object oldValue, Object newValue,
                        boolean transientProperty)
  {
    this (source, new PropertyPath (property), oldValue, newValue, transientProperty);
  }
  
  /**
   * Create a new non-transient PropertyEvent.
   * 
   * @param source The source object that the property is a part of.
   * @param path The path to the property value.
   * @param oldValue The old value of the property.
   * @param newValue The new value of the property.
   * 
   * @see #PropertyEvent(IDataObject, PropertyPath, Object, Object, boolean)
   */                      
  public PropertyEvent (IDataObject source, PropertyPath path,
                        Object oldValue, Object newValue)
  {
    this (source, path, oldValue, newValue, false);
  }
  
  /**
   * Create a new PropertyEvent.
   * 
   * @param source The source object that the property is a part of.
   * @param path The path to the property value.
   * @param oldValue The old value of the property.
   * @param newValue The new value of the property.
   * @param transientProperty True if the property is transient (ie need not
   * be persisted across sessions). For example, IDataBean's set this to true
   * when a default property is changed.
   */
  public PropertyEvent (IDataObject source, PropertyPath path,
                        Object oldValue, Object newValue,
                        boolean transientProperty)
  {
    super (source);
    
    this.path = path;
    this.oldValue = oldValue;
    this.newValue = newValue;
    this.transientProperty = transientProperty;
  }

  public IDataObject getObject ()
  {
    return (IDataObject)getSource ();
  }
  
  /**
   * The name of the property that changed.
   * 
   * @see PropertyPath#toString()
   */
  public String getName ()
  {
    return path.toString ();
  }
  
  public boolean isSameProperty (String property)
  {
    return path.isSamePath (property);
  }
  
  public String toString ()
  {
    StringBuilder str = new StringBuilder ();
    
    str.append ("PropertyEvent [");
    str.append ("property=\"").append (path).append ("\"");
    str.append (" src=").append (source.getClass ().getName ()).append ('@').append (System.identityHashCode (source));
    str.append (" old=[").append (oldValue).append ("] new=[").append (newValue);
    str.append ("]]");
    
    return str.toString ();
  }
}
