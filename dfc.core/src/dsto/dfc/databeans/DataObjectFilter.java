package dsto.dfc.databeans;

import java.util.Iterator;

/**
 * An adapter that allows one data object to contain a filtered subset
 * of properties from another data object.
 * 
 * @see DataObjectView
 * 
 * @author Matthew Phillips
 */
public class DataObjectFilter implements PropertyListener
{
  private IDataObject source;
  private IDataObject target;
  private Selector selector;

  /**
   * Create a new instance.
   * 
   * @param source The soucce object to be read from.
   * @param target The target object to write filtered properties to.
   * @param selector The selector that decided which properties are
   *          copied to target.
   */
  public DataObjectFilter (IDataObject source, IDataObject target, Selector selector)
  {
    this.source = source;
    this.target = target;
    this.selector = selector;
    
    source.addPropertyListener (this);
    
    for (Iterator i = source.propertyIterator (); i.hasNext ();)
    {
      Object property = i.next ();
      Object value = source.getValue (property);
      
      updateTargetProperty (new PropertyPath (property), null, value);
    }
  }

  public void dispose ()
  {
    source.removePropertyListener (this);
  }
  
  private void updateTargetProperty (PropertyPath property,
                                     Object oldValue, Object newValue)
  {
    Object root = property.first ();
    Object newSourceRootValue = source.getValue (root);
    Object newTargetRootValue;
    
    if (newSourceRootValue == null ||
        !selector.select (property, oldValue, newValue))
    {
      newTargetRootValue = null;
    } else
    {
      newTargetRootValue = newSourceRootValue;
    }
    
    target.setValue (root, newTargetRootValue,
                       source.isTransient (property) ?
                           IDataObject.TRANSIENT_OVERRIDE :
                           IDataObject.PERSISTENT_OVERRIDE);
  }

  public void propertyValueChanged (PropertyEvent e)
  {
    updateTargetProperty (e.path, e.oldValue, e.newValue);
  }
  
  public interface Selector
  {
    /**
     * Controls which properties are added to the target object.
     * 
     * @param property The path to the property that has just changed.
     * @param oldValue The old value.
     * @param newValue The new value.
     * @return True if the property at the root of the path should
     *         appear in the filtered object.
     */
    boolean select (PropertyPath property, Object oldValue, Object newValue);
  }
}
