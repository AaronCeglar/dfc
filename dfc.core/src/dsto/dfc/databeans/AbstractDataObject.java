package dsto.dfc.databeans;

import static dsto.dfc.util.Objects.className;

import java.util.Collection;
import java.util.Iterator;

import dsto.dfc.util.EventListenerList;
import dsto.dfc.util.Objects;

/**
 * Abstract base class for IDataObject's. Provides event listener
 * support and support for listening to child objects and re-firing
 * property changes.
 * <p>
 *
 * NOTE: this class implements java.lang.Cloneable only in order to be
 * able to invoke clone () internally - the clone () method is left
 * protected. Use
 * {@link dsto.dfc.databeans.DataObjects#deepClone(IDataObject)} to
 * clone a tree of objects.
 *
 * @author mpp
 * @version $Revision$
 */
public abstract class AbstractDataObject
  implements IDataObject, IDataObjectChildListener, Cloneable
{
  protected EventListenerList listeners;

  public AbstractDataObject ()
  {
    this.listeners = new EventListenerList ();
  }

  public IDataObject shallowClone ()
  {
    try
    {
      AbstractDataObject copy = (AbstractDataObject)super.clone ();

      copy.listeners = new EventListenerList ();

      return copy;
    } catch (CloneNotSupportedException ex)
    {
      throw new UnsupportedOperationException ("Cannot clone: " + ex);
    }
  }

  public abstract TypeInfo getTypeInfo ();

  /**
   * Synonym for setValue (name, value, PERSISTENT_OVERRIDE).
   */
  public void setValue (Object name, Object value)
  {
    setValue (name, value, PERSISTENT_OVERRIDE);
  }

  public abstract void setValue (Object name, Object value, int mode);

  public void setValue (Object name, boolean value)
  {
    setValue (name, value ? Boolean.TRUE : Boolean.FALSE);
  }

  public boolean getBooleanValue (Object name)
  {
    Boolean value = (Boolean)getValue (name);

    return value == null ? false : value.booleanValue ();
  }

  public void setValue (Object name, int value)
  {
    setValue (name, new Integer (value));
  }

  public int getIntValue (Object name)
  {
    Integer value = (Integer)getValue (name);

    return value == null ? 0 : value.intValue ();
  }

  public void setValue (Object name, long value)
  {
    setValue (name, new Long (value));
  }

  public long getLongValue (Object name)
  {
    Long value = (Long)getValue (name);

    return value == null ? 0 : value.longValue ();
  }

  public void setValue (Object name, float value)
  {
    setValue (name, new Float (value));
  }

  public float getFloatValue (Object name)
  {
    Float value = (Float)getValue (name);

    return value == null ? 0f : value.floatValue ();
  }

  public void setValue (Object name, double value)
  {
    setValue (name, new Double (value));
  }

  public double getDoubleValue (Object name)
  {
    Double value = (Double)getValue (name);

    return value == null ? 0d : value.doubleValue ();
  }

  public void setValue (Object name, char value)
  {
    setValue (name, new Character (value));
  }

  public char getCharValue (Object name)
  {
    Character value = (Character)getValue (name);

    return value == null ? 0 : value.charValue ();
  }

  // set/get values using String names

  public void setValue (String name, boolean value)
  {
    setValue (name, value ? Boolean.TRUE : Boolean.FALSE);
  }

  public boolean getBooleanValue (String name)
  {
    Boolean value = (Boolean)getValue (name);

    return value == null ? false : value.booleanValue ();
  }

  public void setValue (String name, short value)
  {
    setValue (name, new Short (value));
  }

  public short getShortValue (String name)
  {
    Short value = (Short)getValue (name);

    return value == null ? 0 : value.shortValue ();
  }

  public void setValue (String name, int value)
  {
    setValue (name, new Integer (value));
  }

  public int getIntValue (String name)
  {
    Integer value = (Integer)getValue (name);

    return value == null ? 0 : value.intValue ();
  }

  public void setValue (String name, long value)
  {
    setValue (name, new Long (value));
  }

  public long getLongValue (String name)
  {
    Long value = (Long)getValue (name);

    return value == null ? 0 : value.longValue ();
  }

  public void setValue (String name, float value)
  {
    setValue (name, new Float (value));
  }

  public float getFloatValue (String name)
  {
    Float value = (Float)getValue (name);

    return value == null ? 0f : value.floatValue ();
  }

  public void setValue (String name, double value)
  {
    setValue (name, new Double (value));
  }

  public double getDoubleValue (String name)
  {
    Double value = (Double)getValue (name);

    return value == null ? 0d : value.doubleValue ();
  }

  public void setValue (String name, char value)
  {
    setValue (name, new Character (value));
  }

  public char getCharValue (String name)
  {
    Character value = (Character)getValue (name);

    return value == null ? 0 : value.charValue ();
  }

  public IDataBean getBeanValue (String name)
  {
    return (IDataBean)getValue (name);
  }

  public IDataObject getObjectValue (String name)
  {
    return (IDataObject)getValue (name);
  }

  public String getStringValue (String name)
  {
    Object value = getValue (name);

    if (value == null)
      return null;
    else
      return value.toString ();
  }

  public void addPropertyListener (PropertyListener l)
  {
    listeners.addListener (l);
  }

  /**
   * Add listener to the front of the list, so it will get events
   * before all other current listeners. Since depending on listener
   * order is usually a dodgy practice (and someone else could always
   * use this call), use at your own risk.
   */
  public void addFirstPropertyListener (PropertyListener l)
  {
    listeners.addFirstListener (l);
  }

  public void removePropertyListener (PropertyListener l)
  {
    listeners.removeListener (l);
  }

  public Collection getPropertyListeners ()
  {
    return listeners.getListeners ();
  }

  /**
   * Register a child value of this object. The child value will be monitored
   * for property changes if it is an IDataObject and
   * {@link #childPropertyChanged(Object, PropertyEvent)} called whenever
   * it changes.
   *
   * @param property The name of the child.
   * @param value The child value.
   *
   * @see DataObjects#registerChildValue(IDataObjectChildListener, Object, Object)
   * @see #unregisterValue(Object)
   */
  protected void registerValue (Object property, Object value)
  {
    DataObjects.registerChildValue (this, property, value);
  }

  /**
   * Undo the effect of {@link #registerValue(Object, Object)}.
   *
   * @see DataObjects#unregisterChildValue(IDataObjectChildListener, Object)
   */
  protected void unregisterValue (Object value)
  {
    DataObjects.unregisterChildValue (this, value);
  }

  /**
   * Invoked when a child property value has changed.
   *
   * @param childProperty The property name on this object that the child
   * is accessible by.
   * @param e The change event from the child.
   *
   * @see #registerValue(Object, Object)
   */
  public void childPropertyChanged (Object childProperty, PropertyEvent e)
  {
    firePropertyChangedEvent (childProperty, e.path,
                              e.oldValue, e.newValue,
                              e.transientProperty);
  }

  public void firePropertyChangedEvent (Object property,
                                        PropertyPath basePath,
                                        Object oldValue,
                                        Object newValue,
                                        boolean isTransient)
  {
    if (listeners.hasListeners () && !Objects.objectsEqualEx (oldValue, newValue))
    {
      PropertyEvent e = new PropertyEvent (this, basePath.prepend (property),
                                           oldValue, newValue, isTransient);

      listeners.fireEvent (PropertyListener.class, "propertyValueChanged", e);
    }
  }

  public void firePropertyChangedEvent (Object property,
                                        Object oldValue,
                                        Object newValue,
                                        boolean isTransient)
  {
    if (listeners.hasListeners () && !Objects.objectsEqualEx (oldValue, newValue))
    {
      PropertyEvent e = new PropertyEvent (this, new PropertyPath (property),
                                           oldValue, newValue, isTransient);

      listeners.fireEvent (PropertyListener.class, "propertyValueChanged", e);
    }
  }

  public void firePropertyChangedEvent (PropertyPath path,
                                        Object oldValue, Object newValue,
                                        boolean isTransient)
  {
    if (listeners.hasListeners () && !Objects.objectsEqualEx (oldValue, newValue))
    {
      PropertyEvent e = new PropertyEvent (this, path, oldValue, newValue, isTransient);

      listeners.fireEvent (PropertyListener.class, "propertyValueChanged", e);
    }
  }

  /**
   * Shortcut to fire property changed event when a boolean value has
   * definitely changed.
   */
  public void firePropertyChangedEvent (Object property,
                                        boolean newValue,
                                        boolean isTransient)
  {
    firePropertyChangedEvent (property, !newValue, newValue, isTransient);
  }

  public void firePropertyChangedEvent (Object property,
                                        boolean oldValue, boolean newValue,
                                        boolean isTransient)
  {
    if (listeners.hasListeners () && oldValue != newValue)
    {
      PropertyEvent e =
        new PropertyEvent (this, property,
                           (oldValue ? Boolean.TRUE : Boolean.FALSE),
                           (newValue ? Boolean.TRUE : Boolean.FALSE),
                           isTransient);

      listeners.fireEvent (PropertyListener.class, "propertyValueChanged", e);
    }
  }

  public void firePropertyChangedEvent (Object property,
                                        int oldValue, int newValue,
                                        boolean isTransient)
  {
    if (listeners.hasListeners () && oldValue != newValue)
    {
      PropertyEvent e =
        new PropertyEvent (this, property,
                           new Integer (oldValue),
                           new Integer (newValue),
                           isTransient);

      listeners.fireEvent (PropertyListener.class, "propertyValueChanged", e);
    }
  }

  /**
   * Generates a human-readable rendering of the object's properties.
   */
  @Override
  public String toString ()
  {
    StringBuilder str = new StringBuilder ();

    String className = className (getClass ());

    str.append (className).append (" [");

    boolean first = true;

    for (Iterator i = propertyIterator(); i.hasNext ();)
    {
      Object property = i.next ();
      Object value = getValue (property);
      boolean nonDataObject = !(value instanceof IDataObject);

      if (!first)
        str.append (' ');
      else
        first = false;

      str.append (property);
      str.append ("=");

      if (nonDataObject)
        str.append ('"');

      str.append (value);

      if (nonDataObject)
        str.append ('"');
    }

    str.append (']');

    return str.toString ();
  }
}
