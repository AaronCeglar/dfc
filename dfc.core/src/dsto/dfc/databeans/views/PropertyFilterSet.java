package dsto.dfc.databeans.views;

import dsto.dfc.databeans.IDataObject;
import dsto.dfc.databeans.PropertyPath;

/**
 * An automatically-generated set of PropertyFilter's for a given
 * property. For each distinct value of the property, a filter will
 * appear in this set that selects that value.
 * 
 * @author Matthew Phillips
 */
public class PropertyFilterSet extends AbstractPropertyFilterSet
{
  public PropertyFilterSet (IDataObject source, String property)
  {
    super (source, property);
  }
  
  @Override
  protected void addFiltersFor (Object value)
  {
    addFilter (value, new PropertyFilter (property, value));
  }
  
  @Override
  protected void removeFiltersFor (Object value)
  {
    removeFilter (value);
  }

  @Override
  protected void updateFilters (PropertyPath path,
                                Object oldValue, Object newValue)
  {
    // zip
  }
}
