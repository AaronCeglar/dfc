package dsto.dfc.databeans.views;

import dsto.dfc.databeans.IDataObject;
import dsto.dfc.databeans.IFilter;
import dsto.dfc.databeans.SimpleDataObject;

import static dsto.dfc.util.Objects.objectsEqual;

/**
 * An IFilter for selecting from data objects with a given property
 * value.
 * 
 * @author Matthew Phillips
 */
public class PropertyFilter
  extends SimpleDataObject implements IFilter<IDataObject>
{
  public Object property;
  public Object value;

  public PropertyFilter (Object property, Object value)
  {
    this.property = property;
    this.value = value;
  }

  public boolean include (IDataObject object)
  {
    return objectsEqual (value, object.getValue (property));
  }
}
