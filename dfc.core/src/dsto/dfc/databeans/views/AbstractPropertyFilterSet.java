package dsto.dfc.databeans.views;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import dsto.dfc.util.Disposable;

import dsto.dfc.databeans.DataBean;
import dsto.dfc.databeans.IDataObject;
import dsto.dfc.databeans.IFilter;
import dsto.dfc.databeans.PropertyEvent;
import dsto.dfc.databeans.PropertyListener;
import dsto.dfc.databeans.PropertyPath;

/**
 * Base class for automatically-generated sets of IFilter's for
 * filtering against values of a IDataObject property. Typically, for
 * each distinct value of the property, a filter will appear in this
 * set that selects that value, but this may be customised by
 * subclasses. This can be useful as the underlying model for
 * filtering views.
 * 
 * @see PropertyFilterSet
 * @see MultiValuePropertyFilterSet
 * 
 * @author Matthew Phillips
 */
public abstract class AbstractPropertyFilterSet
  extends DataBean implements PropertyListener, Disposable
{
  protected String property;
  protected IDataObject source;
  protected Map<Object,Integer> counts;
  
  public AbstractPropertyFilterSet (IDataObject source, String property)
  {
    this.source = source;
    this.property = property;
    this.counts = new HashMap<Object,Integer> ();
    
    for (Iterator i = source.propertyIterator (); i.hasNext (); )
    {
      IDataObject item = (IDataObject)source.getValue (i.next ());
      
      addItem (item);
    }
    
    source.addPropertyListener (this);
  }
  
  /**
   * True if a filter exists for a given value.
   * 
   * @see #filterFor(Object)
   */
  public boolean hasFilterFor (Object value)
  {
    return counts.containsKey (value);
  }
  
  /**
   * Get the filter for a given value.
   * @param value The value.
   * @return The filter.
   * 
   * @throws IllegalArgumentException if no filter exists for the value.
   */
  public IFilter<IDataObject> filterFor (Object value)
    throws IllegalArgumentException
  {
    IFilter<IDataObject> filter = (IFilter<IDataObject>)getValue (value);
    
    if (filter == null)
      throw new IllegalArgumentException ("No filter for " + value);
    
    return filter;
  }
  
  /**
   * Add the filters for a given item in the source set.
   */
  protected void addItem (IDataObject item)
  {
    Object value = item.getValue (property);
    
    if (value != null)
      addFiltersFor (value);
  }

  /**
   * Remove the filters for a given item in the source set.
   */
  protected void removeItem (IDataObject item)
  {
    Object value = item.getValue (property);
    
    if (value != null)
      removeFiltersFor (value);
  }
  
  /**
   * Add filters for a given property value.
   */
  protected abstract void addFiltersFor (Object value);
  
  /**
   * Remove filters for a given property value.
   */
  protected abstract void removeFiltersFor (Object value);

  /**
   * Update filters for a given property value when the value itself
   * has changed (i.e. generated a property change event).
   * 
   * @param path The propery path that changed.
   * @param oldValue The old value at path.
   * @param newValue The new value at path.
   */
  protected abstract void updateFilters (PropertyPath path,
                                         Object oldValue,
                                         Object newValue);
  
  /**
   * Add a filter for a value.
   */
  protected void addFilter (Object value, IFilter<IDataObject> filter)
  {
    Integer count = counts.get (value);
    
    counts.put (value, count == null ? 0 : count++);
    
    if (count == null)
      setValue (value, filter);
  }

  /**
   * Remove a filter for a value.
   */
  protected void removeFilter (Object value)
  {
    int count = counts.get (value);
    
    if (count == 1)
    {
      counts.remove (value);
      
      setValue (value, null);
    } else
    {
      counts.put (value, count--);
    }
  }
  
  public void dispose ()
  {
    source.removePropertyListener (this);
  }
  
  public void propertyValueChanged (PropertyEvent e)
  {
    if (e.path.length () == 1)
    {
      if (e.oldValue != null)
        removeItem ((IDataObject)e.oldValue);
      
      if (e.newValue != null)
        addItem ((IDataObject)e.newValue);
    } else if (e.path.length () == 2 && e.path.last ().equals (property))
    {
      if (e.oldValue != null)
        removeFiltersFor (e.oldValue);
      
      if (e.newValue != null)
        addFiltersFor (e.newValue);
    } else if (e.path.length () == 2 && e.path.element (1).equals (property))
    {
      updateFilters (e.path, e.oldValue, e.newValue);
    }
  }
}
