package dsto.dfc.databeans.views;

import java.util.Iterator;

import dsto.dfc.util.Disposable;

import dsto.dfc.databeans.IDataObject;
import dsto.dfc.databeans.PropertyListener;
import dsto.dfc.databeans.PropertyPath;

/**
 * A property filter that that works when property values are
 * themselves IDataObject's (typically a set of some kind). For
 * example, if the value of the "tags" property for items in the
 * source set is a SetDataObject containing string tags, this property
 * set could be used to generate filters for all distinct values of
 * all the tags used in the source objects.
 * 
 * @author Matthew Phillips
 */
public class MultiValuePropertyFilterSet
  extends AbstractPropertyFilterSet implements PropertyListener, Disposable
{
  public MultiValuePropertyFilterSet (IDataObject source, String property)
  {
    super (source, property);
  }
  
  @Override
  protected void addFiltersFor (Object value)
  {
    IDataObject object = (IDataObject)value;
    
    for (Iterator i = object.propertyIterator (); i.hasNext (); )
      addFilterFor (object.getValue (i.next ()));
  }

  protected void addFilterFor (Object value)
  {
    addFilter (value, new Filter (property, value));
  }
  
  @Override
  protected void removeFiltersFor (Object value)
  {
    IDataObject object = (IDataObject)value;
    
    for (Iterator i = object.propertyIterator (); i.hasNext (); )
      removeFilter (object.getValue (i.next ()));
  }

  @Override
  protected void updateFilters (PropertyPath path,
                                Object oldValue, Object newValue)
  {
    if (oldValue != null)
      removeFilter (oldValue);
    
    if (newValue != null)
      addFilterFor (newValue);
  }
  
  static class Filter extends PropertyFilter
  {
    public Filter (Object property, Object value)
    {
      super (property, value);
    }

    @Override
    public boolean include (IDataObject object)
    {
      Object subObject = object.getValue (property);
     
      return subObject instanceof IDataObject &&
             ((IDataObject)subObject).getValue (value) != null;
    }
  }
}
