package dsto.dfc.databeans;

import java.util.Arrays;
import java.util.Iterator;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;

import dsto.dfc.logging.Log;
import dsto.dfc.util.Beans;
import dsto.dfc.util.Objects;

/**
 * Direct IDataBean adapter for a JavaBean. Does not handle default properties
 * or the fancy stuff that JavaDataBean provides.
 * 
 * @author mpp
 * @version $Revision$
 */
public class SimpleJavaDataBean
  extends AbstractDataBean implements PropertyChangeListener
{
  private Object bean;
  private PropertyDescriptor [] properties;
  private boolean listening;
  
  /**
   * Constructor for DataBeanAdapter.
   */
  public SimpleJavaDataBean (Object bean)
    throws IllegalArgumentException
  {
    this.bean = bean;
    
    findProperties ();
    
    this.listening = Beans.addListener (PropertyChangeListener.class, bean, this);
    
    /** @todo add listeners to property values. */
  }
  
  // todo this has not been tested
  public IDataObject shallowClone () throws UnsupportedOperationException
  {
    try
    {
      SimpleJavaDataBean copy = (SimpleJavaDataBean)super.shallowClone ();
      
      if (bean != this)
        copy.bean = Objects.cloneObject (bean);
      else
        copy.bean = copy;
      
      copy.listening =
        Beans.addListener (PropertyChangeListener.class, copy.bean, copy);
      
      return copy;
    } catch (CloneNotSupportedException ex)
    {
      throw new UnsupportedOperationException ("Clone not supported: " + ex);
    }
  }
  
  private void findProperties ()
    throws IllegalArgumentException
  {
    try
    {
      BeanInfo info = Introspector.getBeanInfo (bean.getClass ());
      properties = info.getPropertyDescriptors ();
    } catch (IntrospectionException ex)
    {
      throw new IllegalArgumentException ("Failed to introspect bean: " + ex);
    }
  }
  
  private PropertyDescriptor findProperty (String name)
    throws IllegalArgumentException
  {
    for (int i = 0; i < properties.length; i++)
    {
      PropertyDescriptor d = properties [i];
      
      if (d.getName ().equals (name))
        return d;
    }
    
    throw new IllegalArgumentException ("No property named \"" + name + "\"");
  }
  
  public void dispose ()
  {
    if (listening)
      Beans.removeListener (PropertyChangeListener.class, bean, this);
  }
  
  public Object getBean ()
  {
    return bean;
  }
  
  // IDataBean interface
  
  /**
   * The default implementation uses
   * {@link TypeRegistry#peekTypeInfo(Class)}to see whether any type
   * info is registered for the class.
   */
  public TypeInfo getTypeInfo ()
  {
    return TypeRegistry.peekTypeInfo (getClass ());
  }
  
  public String [] getPropertyNames ()
  {
    String [] names = new String [properties.length];
    
    for (int i = 0; i < properties.length; i++)
      names [i] = properties [i].getName ();
    
    return names;
  }

  public Iterator propertyIterator ()
  {
    return Arrays.asList (properties).iterator ();
  }

  public void setValue (String name, Object value, int mode)
  {
    throw new UnsupportedOperationException ();
  }
  
  public Object getDefaultValue (String name)
  {
    return null;
  }
  
  public boolean isTransient (Object name)
  {
    return false;
  }
  
  public void setToDefaultValue (String name)
  {
    throw new UnsupportedOperationException ();
  }
  
  public void setValue (Object name, Object value)
  {
    setValue (name, value, PERSISTENT_OVERRIDE);
  }
  
  public void setValue (Object name, Object value, int mode)
  {
    if ((mode & TRANSIENT) != 0)
      throw new IllegalArgumentException ("Transient properties not supported");

    try
    {
      PropertyDescriptor property = findProperty (name.toString ());
      
      Object oldValue = property.getReadMethod ().invoke (bean, (Object [])null);
      
      if (oldValue == null || (mode & OVERRIDE) != 0)
      {
        property.getWriteMethod ().invoke (bean, new Object [] {value});
        
        if (!listening)
          firePropertyChangedEvent (name, oldValue, value, false);
      }        
    } catch (InvocationTargetException ex)
    {
      Log.internalError ("Exception while setting property value", this, ex);

      throw new IllegalArgumentException
        ("Error while writing property \"" + name + "\": " + ex.getTargetException ());
    } catch (Exception ex)
    {
      throw new IllegalArgumentException
        ("Error while writing property \"" + name + "\": " + ex);
    }
  }

  public Object getValue (Object name)
    throws IllegalArgumentException
  {
    try
    {
      PropertyDescriptor property = findProperty (name.toString ());
      
      return property.getReadMethod ().invoke (bean, (Object [])null);
    } catch (InvocationTargetException ex)
    {
      Log.internalError ("Exception while setting property value", this, ex);

      throw new IllegalArgumentException
        ("Error while reading property \"" + name + "\": " + ex.getTargetException ());
    } catch (Exception ex)
    {
      throw new IllegalArgumentException
        ("Error while reading property \"" + name + "\": " + ex);
    }
  }
  
  public void propertyChange (PropertyChangeEvent e)
  {
    firePropertyChangedEvent
      (e.getPropertyName (), e.getOldValue (), e.getNewValue (), false);
  }
}
