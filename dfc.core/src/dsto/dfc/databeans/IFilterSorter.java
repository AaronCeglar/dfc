package dsto.dfc.databeans;

/**
 * Convenience interface for classes that are both filters and sorters.
 * 
 * @see DataObjectView
 */
public interface IFilterSorter<T> extends IFilter<T>, ISorter<T>
{
  // zip
}