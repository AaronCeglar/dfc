package dsto.dfc.databeans;


import static dsto.dfc.databeans.DataObjects.objectLinkToString;

import java.util.Iterator;

/**
 * A link to an IDataObject that also provides a facade to the object
 * (i.e. exposes the same property set).
 *
 * @see dsto.dfc.databeans.DataBeanLink
 *
 * @author mpp
 * @version $Revision$
 */
public class DataObjectLink
  extends AbstractDataObject
  implements IDataObject, IDataObjectLink, PropertyListener
{
  protected IDataObject object;
  protected boolean isSoftLink;

  /**
   * Create a link with no target. Use
   * {@link #setLinkTarget(IDataObject)} before use or Bad Things will
   * occur.
   */
  public DataObjectLink ()
  {
    this (null);
  }

  public DataObjectLink (IDataObject object)
  {
    this (object, true);
  }

  public DataObjectLink (IDataObject object, boolean isSoftLink)
  {
    this.object = object;
    this.isSoftLink = isSoftLink;

    initListener ();
  }

  public void dispose ()
  {
    if (object != null)
    {
      object = null;

      disposeListener ();
    }
  }

  protected void initListener ()
  {
    if (object != null)
    {
      object.addPropertyListener (this);
    }
  }

  protected void disposeListener ()
  {
    if (object != null)
    {
      object.removePropertyListener (this);
    }
  }

  public IDataObject getLinkTarget ()
  {
    return object;
  }

  /**
   * Set the object being pointed to by this link. Not recommended to
   * be used outside of initialisation.
   */
  public void setLinkTarget (IDataObject target)
  {
    disposeListener ();

    this.object = target;

    initListener ();
  }

  public boolean isSoftLink ()
  {
    return isSoftLink;
  }

  public void setSoftLink (boolean newValue)
  {
    if (listeners.size () > 0)
      throw new IllegalStateException ("Cannot change link type while object has listeners");

    this.isSoftLink = newValue;
  }

  @Override
  public String toString ()
  {
    return objectLinkToString (this);
  }

  @Override
  public void setValue (Object name, Object value)
  {
    object.setValue (name, value);
  }

  @Override
  public void setValue (Object name, Object value, int mode)
  {
    object.setValue (name, value, mode);
  }

  public Object getValue (Object name)
  {
    return object.getValue (name);
  }

  public Iterator propertyIterator ()
  {
    if(object == null)
      return null;
    return object.propertyIterator ();
  }

  @Override
  public IDataObject shallowClone () throws UnsupportedOperationException
  {
    return super.shallowClone ();
  }

  public boolean isTransient (Object name)
  {
    return object.isTransient (name);
  }

  @Override
  public TypeInfo getTypeInfo ()
  {
    return object.getTypeInfo ();
  }

  // PropertyListener interface

  public void propertyValueChanged (PropertyEvent e)
  {
    // re-fire event as if from this bean
    firePropertyChangedEvent (e.path, e.oldValue, e.newValue,
                              e.transientProperty);
  }
}
