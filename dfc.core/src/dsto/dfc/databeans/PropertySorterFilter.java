package dsto.dfc.databeans;

import dsto.dfc.util.DefaultComparator;

/**
 * A {@link DataObjectView} sorter/filter that filters/sorts on
 * property values of data objects.
 * 
 * @author Matthew Phillips
 */
public class PropertySorterFilter implements IFilterSorter
{
  protected String property;

  public PropertySorterFilter (String property)
  {
    this.property = property;
    
  }
  
  public boolean include (Object value)
  {
    return getValue (value) != null;
  }

  public boolean affectsOrder (PropertyPath path)
  {
    return path.first ().equals (property);
  }

  public int compare (Object o1, Object o2)
  {
    return DefaultComparator.instance ().compare (getValue (o1), getValue (o2));
  }

  private Object getValue (Object value)
  {
    if (value instanceof IDataObject)
      return ((IDataObject)value).getValue (property);
    else
      return null;
  }
}
