package dsto.dfc.databeans;

/**
 * Abstract base implementation of IDataBean. Provides default value primitive
 * set/get wrappers.
 * 
 * @author mpp
 * @version $Revision$
 */
public abstract class AbstractDataBean
  extends AbstractDataObject implements IDataBean
{  
  public void setValue (String name, boolean value, int mode)
  {
    setValue (name, value ? Boolean.TRUE : Boolean.FALSE, mode);
  }
    
  public void setValue (String name, int value, int mode)
  {
    setValue (name, new Integer (value), mode);
  }
  
  public void setValue (String name, short value, int mode)
  {
    setValue (name, new Short (value), mode);
  }
  
  public void setValue (String name, long value, int mode)
  {
    setValue (name, new Long (value), mode);
  }
  
  public void setValue (String name, float value, int mode)
  {
    setValue (name, new Float (value), mode);
  }

  public void setValue (String name, double value, int mode)
  {
    setValue (name, new Double (value), mode);
  }
  
  public void setValue (String name, char value, int mode)
  {
    setValue (name, new Character (value), mode);
  }
}
