package dsto.dfc.databeans;

/**
 * A sorter that makes it easier when sorting items in category, value
 * order. Subclasses define numeric categories for values that are
 * used as the first order sorting criteria. Values for equal
 * categories are sorted by value as usual.
 * 
 * @author Matthew Phillips
 */
public abstract class CategorySorter<T> implements ISorter<T>
{  
  public int compare (T o1, T o2)
  {
    int cat1 = category (o1);
    int cat2 = category (o2);
    
    if (cat1 == cat2)
      return compareValues (o1, o2);
    else
      return cat1 - cat2;
  }

  public abstract boolean affectsOrder (PropertyPath path);

  /**
   * Compare two values that are in the same category. This will
   * usually be the same as {@link #compare(Object, Object)} in a
   * non-category sorter.
   */
  protected abstract int compareValues (T item1, T item2);

  /**
   * Generate a numeric category for a given item. Items are sorted in
   * category, value order.
   */
  protected abstract int category (T item);
}
