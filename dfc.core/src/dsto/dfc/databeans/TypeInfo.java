package dsto.dfc.databeans;

/**
 * Defines metadata for a given data object type, acting in a similar
 * role as a Java class. The key difference is that metadata can be
 * attached dynamically to type info, allowing such things as icons,
 * suggested editors, etc to be attached to types.
 * <p>
 * This object is used in two slightly different ways depending on
 * whether it is metdata for "primitive" (non-IDataObject) or
 * "complex" (IDataObject) instances.
 * <p>
 * For complex objects, the {@link #type} field is a template that
 * describes the known properties of this type. For each known
 * property of an instance, the template has a property => TypeInfo
 * mapping i.e. getValue (property) yeilds the TypeInfo for values of
 * that property.
 * <p>
 * For primitive objects, the "type" field is not used, instead the
 * class of the object is held in {@link #valueClass}. If this field
 * is java.lang.Object, it indicates any value can be present.
 * 
 * @see dsto.dfc.databeans.TypeRegistry
 * 
 * @author Matthew Phillips
 */
public class TypeInfo extends FancyDataObject
{
  /**
   * The property name used to store the default value of a given
   * type.
   */
  public static final String DEFAULT_VALUE = "defaultValue";
  
  /**
   * A data object that defines the "type" as a mapping of known
   * properties to {@link TypeInfo} descriptions. This will be null
   * for non-IDataObject types (see <code>valueClass</code>).
   */
  public final IDataObject type;
  
  /**
   * Defines the class for instances of this type. This is only used
   * for non-IDataObject types that are not defined by
   * <code>type</code>.
   */
  public final Class valueClass;
  
  /**
   * The logical ID of this type. If two TypeInfo instances have the
   * same ID, then they are the same logical type. For now every
   * instance has its own unique ID, but in future we might want to
   * allow different instances with subtle changes (eg suggested
   * editor) that are stil the same type via their ID.
   */
  public final String id;
  
  /**
   * Create a new IDataObject-based type.
   */
  public TypeInfo ()
  {
    this.id = createId ();
    this.type = new DataBean ();
    this.valueClass = null;
  }

  /**
   * Create a new non-IDataObject type.
   * 
   * @param valueClass The class that instances of this type conform to.
   */
  public TypeInfo (Class valueClass)
  {
    this.id = createId ();
    this.valueClass = valueClass;
    this.type = null;
  }
  
  /**
   * Shortcut create type info for integer values with given max/min
   * values.
   * 
   * @param minValue The min value: mapped to minValue custom property
   *          on the type info.
   * @param maxValue The max value: mapped to maxValue custom property
   *          on the type info. 
   */
  public TypeInfo (int minValue, int maxValue)
  {
    this (Integer.class);
    
    setValue ("minValue", minValue);
    setValue ("maxValue", maxValue);
  }
  
  protected String createId ()
  {
    return String.valueOf (System.identityHashCode (this));
  }
  
  /**
   * Test if instances of this type are IDataObject-based.
   */
  public boolean isDataObject ()
  {
    return type != null;
  }

  /**
   * Add type info for the value of a given property.
   * 
   * @param propertyName The property name.
   * @param propertyType The type info describing values of the property.
   * 
   * @see #addPropertyInfo(Object, String)
   * @see #getPropertyInfo(Object)
   */
  public void addPropertyInfo (Object propertyName, TypeInfo propertyType)
  {
    type.setValue (propertyName, propertyType);
  }
  
  /**
   * Add type info for the value of a given property.
   * 
   * @param propertyName The property name.
   * @param propertyType The logical name of the type info.
   *          {@link TypeRegistry#getTypeInfo(String)}is used to
   *          resolve this to the registered TypeInfo.
   * 
   * @see #addPropertyInfo(Object, TypeInfo)
   * @see #getPropertyInfo(Object)
   * 
   * @throws IllegalArgumentException if propertyType is not a
   *           registered type info.
   */
  public void addPropertyInfo (Object propertyName, String propertyType)
  {
    type.setValue (propertyName, TypeRegistry.getTypeInfo (propertyType));
  }

  /**
   * Get type info for values of a given property.
   * 
   * @param property The property name,
   * 
   * @return The type info describing the property values, or null if
   *         none registered.
   * 
   * @see #addPropertyInfo(Object, TypeInfo)
   * 
   * @throws IllegalArgumentException if this is not type info for a
   *           IDataBean-based type.
   */
  public TypeInfo getPropertyInfo (Object property)
  {
    if (type == null)
      throw new IllegalArgumentException ("Cannot get property info for a primitive type");
    
    return (TypeInfo)type.getValue (property);
  }
  
  public boolean equals (Object obj)
  {
    if (obj instanceof TypeInfo)
      return equals ((TypeInfo)obj);
    else
      return false;
  }
  
  public boolean equals (TypeInfo info)
  {
    return id.equals (info.id);
  }
  
  public int hashCode ()
  {
    return id.hashCode ();
  }
}
