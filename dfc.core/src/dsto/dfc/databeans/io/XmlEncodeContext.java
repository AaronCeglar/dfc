package dsto.dfc.databeans.io;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jdom.Element;

import dsto.dfc.collections.IdentityHashMap;

/**
 * Contains context state during an XML encoding operation. XmlEncoder clients
 * receive an instance via
 * {@link XmlEncoder#encode(XmlEncodeContext, Object)}.
 * 
 * @author mpp
 * @version $Revision$
 */
public class XmlEncodeContext
{
  private List xmlEncoders;
  private List textEncoders;
  private IdentityHashMap objectToElement;
  private HashSet elementIds;
  private Map typeAliases;
  private Map properties;
  private List idCallbacks;
  
  protected XmlEncodeContext (List xmlEncoders,
                               List textEncoders,
                               Map typeAliases)
  {
    this.xmlEncoders = xmlEncoders;
    this.textEncoders = textEncoders;
    this.objectToElement = new IdentityHashMap ();
    this.elementIds = new HashSet ();
    this.typeAliases = typeAliases;
    this.properties = Collections.EMPTY_MAP;
    this.idCallbacks = new ArrayList ();
  }
  
  protected void doPostEncode ()
  {
    if (idCallbacks != null && !idCallbacks.isEmpty ())
    {
      for (Iterator i = idCallbacks.iterator (); i.hasNext ();)
      {
        IdCallbackEntry entry = (IdCallbackEntry)i.next ();
        
        if (!doIdCallback (entry.callback, entry.element, entry.value, entry.extraData))
        {
          throw new IllegalArgumentException
            ("Object " + entry.value + " (" + entry.value.getClass () +
             ") referenced by " + entry.element + " was not defined");
        }
      }
      
      idCallbacks = null;
    }
  }
  
  /**
   * Set all custom properties. Used by {@link XmlOutput}. 
   * 
   * @see #setProperty(Object, Object)
   */
  protected void setProperties (Map properties)
  {
    this.properties = properties;
  }
  
  /**
   * Define a custom property that will be accessible during this session
   * via {@link #getProperty(Object)}.
   * 
   * @see XmlOutput#setProperty(Object, Object)
   */
  public void setProperty (Object name, Object value)
  {
    if (properties == Collections.EMPTY_MAP)
      properties = new HashMap ();

    properties.put (name, value);
  }
  
  /**
   * Get a custom property defined by {@link #setProperty(Object, Object)}.
   */
  public Object getProperty (Object name)
  {
    return properties.get (name);
  }
  
  /**
   * Mediates the encoding of a value. This method should be used in
   * preference to a direct call to the registered XmlEncoder for a value,
   * since it manages generation of id/refid for values that need their
   * identity preserved.
   * 
   * @param value The value to encode.
   * @return The element representing the encoded value.
   * @throws IllegalArgumentException if the value could not be encoded.
   */
  public Element encode (Object value)
  {
    // check if reference to existing
    Element existingElement = findRegisteredElement (value);
    Element element;
    
    if (existingElement != null)
    {
      element = new Element (existingElement.getName ());
      
      element.setAttribute ("ref", existingElement.getAttributeValue ("id"));
    } else
    {
      XmlEncoder encoder = getXmlEncoder (value);

      element = encoder.encode (this, value);
      
      if (value != null && encoder.shouldPreserveIdentity (this, value))
        registerElement (element, value);
    }
    
    return element;
  }
  
  /**
   * Get the registered XML encoder for a given value.
   */
  public XmlEncoder getXmlEncoder (Object value)
  {
    for (int i = xmlEncoders.size () - 1; i >= 0; i--)
    {
      XmlEncoder encoder = (XmlEncoder)xmlEncoders.get (i);
      
      if (encoder.canEncode (this, value))
        return encoder;
    }
    
    throw new IllegalStateException ("No default encoder found for " + value);
  }
  
  /**
   * Get the registered text encoder for a given value.
   * 
   * @throws IllegalStateException if no encoder found.
   * 
   * @see #peekTextEncoder(Object)
   */
  public TextEncoder getTextEncoder (Object value)
    throws IllegalStateException
  {
    TextEncoder encoder = peekTextEncoder (value);
    
    if (encoder != null)
      return encoder;
    else
      throw new IllegalStateException ("No default encoder found for " + 
                                       value.getClass ());
  }

  /**
   * Lookup the registered text encoder for a given value.
   * 
   * @return The encoder, or null.
   * 
   * @see #getTextEncoder(Object)
   */

  public TextEncoder peekTextEncoder (Object value)
  {
    for (int i = textEncoders.size () - 1; i >= 0; i--)
    {
      TextEncoder encoder = (TextEncoder)textEncoders.get (i);
      
      if (encoder.canEncode (this, value))
        return encoder;
    }
    
    return null;
  }

  /**
   * Get the alias for a given type.
   * 
   * @return The alias, or null if no registered alias.
   */
  public String getTypeAlias (Class valueClass)
  {
    return (String)typeAliases.get (valueClass.getName ());
  }
  
  /**
   * Get the encoded type name for a class, taking type aliases and 
   * array encoding into account.
   * 
   * @see XmlDecodeContext#decodeType(String)
   */
  public String encodeType (Class valueClass)
  
  {
    int arrayDimension = 0;
    boolean isArray = false;
    
    if (valueClass.isArray ())
    {
      isArray = true;
      arrayDimension = findArrayDimension (valueClass);
      valueClass = findArrayType (valueClass);
    }

    String alias = (String)typeAliases.get (valueClass.getName ());
    String name;
    
    if (alias != null)
      name = alias;
    else
      name = valueClass.getName ();
   
    if (isArray)
    {
      // add array brackets
      StringBuilder arrayName = new StringBuilder (name.length () + 10);
      
      arrayName.append (name).append (' ');

      String brackets = valueClass.isPrimitive () ? "[]" : "{}";
      
      
      for (int i = arrayDimension; i >= 0; i--)
        arrayName.append (brackets);
      
      name = arrayName.toString ();
    }

    return name;
  }
  
  /**
   * Find an array's declared type e.g. String [] [] returns String.class.
   */
  private static Class findArrayType (Class arrayClass)
  {
    while (arrayClass.isArray ())
      arrayClass = arrayClass.getComponentType ();
    
    return arrayClass;
  }

  /**
   * Return the dimension of an array class. Dimension of int [] ==
   * 1, dimension of int [] [] == 2, etc.
   */
  private static int findArrayDimension (Class arrayClass)
  {
    int dimension = 0;
    
    while (arrayClass.isArray ()) 
    {
      dimension++;
      arrayClass = arrayClass.getComponentType ();
    } 
    
    return dimension - 1;
  }

  /**
   * Register a document element as representing a given value. This
   * registration may be used later if an ID is needed for the
   * element.
   * <p>
   * 
   * Clients do not need to call this, since
   * {@link #encode(Object)} does this automatically, but may
   * choose to do so early if they contain child values that may
   * reference the value.
   */
  public void registerElement (Element element, Object value)
  {
    objectToElement.put (value, element);
  }
    
  /**
   * Find the element for a value if it has been previously registered with
   * {@link #registerElement(Element,Object)}. If the registered element does
   * not yet have an ID, one is allocated and attached as the "id" attribute
   * on the element.
   * 
   * @param value The value.
   * @return The element registered as representing the value, or null if none
   * has been registered.
   */
  private Element findRegisteredElement (Object value)
  {
    Element element = (Element)objectToElement.get (value);
    
    if (element != null)
    {
      String id = element.getAttributeValue ("id");
      
      if (id == null)
      {
        id = createElementId (element, value);
        
        element.setAttribute ("id", id);
      }
    }
    
    return element;
  }

  /**
   * Create a unique ID based on an element's path in the document.
   */
  private String createElementId (Element element, Object value)
  {
    StringBuilder baseId = new StringBuilder ();
    
    // build id by walking up path in XML tree, using name or type attribute
    for (Element e = element; e.getParent () != null; e = (Element)e.getParent ())
    {
      String s = e.getAttributeValue ("name", (String)null);
      
      if (s == null)
      {
        String type = e.getAttributeValue ("type", (String)null);

        // if type looks like a class, use shortened version        
        if (type != null && type.indexOf ('.') != -1)
          s = type.substring (type.lastIndexOf ('.') + 1);
      }

      if (s != null)
      {
        if (baseId.length () == 0)
          baseId.insert (0, s);
        else
          baseId.insert (0, s + '.');
      }
    }

    if (baseId.length () == 0)
    {
      // if element is not in XML hierachy or we didn't find any names,
      // use the value's class name
      String valueClass = encodeType (value.getClass ());
      baseId.append (valueClass.substring (valueClass.lastIndexOf ('.') + 1));
    }
      
    String id = baseId.toString ();
    
    // if not unique, add serial numbers until it is
    for (int serial = 0; elementIds.contains (id); serial++)
      id = baseId.toString () + serial;
    
    elementIds.add (id);
    
    return id;
  }

  /**
   * Try to invoke an registered ID callnaback.
   * 
   * @return True if the callback was possible i.e. an ID was available.
   */
  private boolean doIdCallback (IdCallback callback,
                                  Element element,
                                  Object value,
                                  Object extraData)
  {
    Element existingElement = findRegisteredElement (value);
    
    if (existingElement != null)
    {
      callback.idForObjectCallback
        (this, element, existingElement.getAttributeValue ("id"), extraData);
      
      return true;
    } else
    {
      return false;
    }
  }

  /**
   * Register a callback to be invoked when an ID becomes defined for
   * another encoded value. This can be used if a client wants to
   * refer to a value that may not have been encoded yet. When an ID
   * is defined the client will be notified via
   * {@link IdCallback#idForObjectCallback}.
   * The callback will be invoked immediately if an ID is already
   * available.
   * 
   * @param callback The callback target.
   * @param element The element param to be passed to the callback.
   * @param value The value for which an ID is needed.
   * @param extraData Any extra data to be passed to the callback (may
   *          be null).
   * 
   * @see IdCallback#idForObjectCallback
   * @see XmlDecodeContext#callbackWithObjectForId
   */
  public void callbackWithIdForObject (IdCallback callback,
                                       Element element,
                                       Object value,
                                       Object extraData)
  {
    if (!doIdCallback (callback, element, value, extraData))
    {
      idCallbacks.add (new IdCallbackEntry (callback, element,
                                            value, extraData));
    }
  }
  
  /**
   * Callback interface for clients that wish to use forward references.
   */
  public interface IdCallback
  {
    /**
     * Called by encode context when an ID becomes available for a given object.
     * 
     * See {@link XmlEncodeContext#callbackWithIdForObject(IdCallback, Element, Object, Object)}.
     * 
     * @param context The current encoding context.
     * @param element The element passed to callbackWithIdForObject ().
     * @param id The ID for the requested value.
     * @param extraData The extraData passed to callbackWithIdForObject ().
     */
    public void idForObjectCallback (XmlEncodeContext context,
                                      Element element,
                                      String id,
                                      Object extraData);
    
  }
  
  /**
   * Used to store forward reference callbacks.
   */
  private static class IdCallbackEntry
  {
    public IdCallback callback;
    public Element element;
    public Object value;
    public Object extraData;

    public IdCallbackEntry (IdCallback callback,
                            Element element,
                            Object value,
                            Object extraData)
    {
      this.callback = callback;
      this.element = element;
      this.value = value;
      this.extraData = extraData;
    }
  }
}
