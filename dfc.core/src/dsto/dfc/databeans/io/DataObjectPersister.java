package dsto.dfc.databeans.io;

import java.util.TimerTask;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import dsto.dfc.databeans.IDataObject;
import dsto.dfc.databeans.PropertyEvent;
import dsto.dfc.databeans.PropertyListener;
import dsto.dfc.logging.Log;
import dsto.dfc.util.Files;
import dsto.dfc.util.Timers;

/**
 * A java.util.Timer task that saves a data object at periodic intervals when
 * it has been modified.
 * 
 * @see dsto.dfc.databeans.io.DataObjectPersister2
 */
public class DataObjectPersister
  extends TimerTask implements PropertyListener
{
  private static final long DEFAULT_SAVE_INTERVAL = 30 * 1000;
  private static final long DEFAULT_MAX_SAVE_INTERVAL = 3 * 60 * 1000;
  
  protected IDataObject object;
  protected File file;
  protected XmlOutput xmlOutput;
  protected boolean modified;
  protected long lastModified;
  protected long nextSave;
  
  protected long saveInterval;
  protected long maxSaveInterval;
  
  public DataObjectPersister (IDataObject object, String filename)
  {
    this (object, new File (filename));
  }
  
  public DataObjectPersister (IDataObject object, File file)
  {
    this.object = object;
    this.file = file;
    this.xmlOutput = new XmlOutput ();
    this.saveInterval = DEFAULT_SAVE_INTERVAL;
    this.maxSaveInterval = DEFAULT_MAX_SAVE_INTERVAL;
    
    file = file.getAbsoluteFile ();
    
    file.getParentFile ().mkdirs ();
    
    object.addPropertyListener (this);
    
    lastModified = System.currentTimeMillis ();
    nextSave = Long.MAX_VALUE;
    
    Timers.getTimer (true).schedule (this, 10 * 1000, 10 * 1000);
  }
  
  public synchronized void dispose ()
  {
    cancel ();
    
    object.removePropertyListener (this);
    
    if (modified)
      save ();
  }
  
  public XmlOutput getXmlOutput ()
  {
    return xmlOutput;
  }
  
  public int getMaxSaveInterval ()
  {
    return (int)(maxSaveInterval / 1000L);
  }
  
  /**
   * Set the maximum the persister will allow a modified object to remain
   * unsaved.
   * 
   * @param maxSaveInterval interval in seconds.
   */
  public void setMaxSaveInterval (int maxSaveInterval)
  {
    this.maxSaveInterval = maxSaveInterval * 1000L;
  }
  
  public int getSaveInterval ()
  {
    return (int)(saveInterval / 1000L);
  }
  
  /**
   * Set the interval that must pass without modifications before a modified
   * object is saved.
   * 
   * @param saveInterval The interval in seconds.
   */
  public void setSaveInterval (int saveInterval)
  {
    this.saveInterval = saveInterval * 1000L;
  }
  
  public synchronized void run ()
  {
    try
    {
      if (modified && System.currentTimeMillis () >= nextSave)
        save ();
    } catch (RuntimeException ex)
    {
      Log.alarm ("Error in persister task", this, ex);
    }
  }
  
  public synchronized void setModified (boolean value)
  {
    if (modified != value)
    {
      modified = value;
      
      if (modified)
        lastModified = System.currentTimeMillis ();
    }
  }
  
  public synchronized void propertyValueChanged (PropertyEvent e)
  {
    if (!e.transientProperty)
    {
      Log.trace ("Trigger save on property modify: " + e.getName () + ", old=" + e.oldValue + ", new=" + e.newValue, this);
      
      long now = System.currentTimeMillis ();
      
      setModified (true);
      
      // if longer than MAX_INTERVAL since last save, do it at next
      // opportunity, otherwise schedule save at SAVE_INTERVAL millis from now
      if (now - lastModified >= maxSaveInterval)
        nextSave = now;
      else
        nextSave = now + saveInterval;      
    }
  }

  private void save ()
  {
    try
    {
      // write to temp file to avoid leaving corrupted file on error
      File tempFile = File.createTempFile ("persister", null, file.getParentFile ());
      Writer writer = new BufferedWriter (new FileWriter (tempFile));

      try
      {        
        xmlOutput.write (writer, object, true);
      } finally
      {
        writer.close ();
      }
      
      if (file.exists ())
        file.delete ();
      
      if (!tempFile.renameTo (file))
      {
        Log.diagnostic
          ("Failed to rename " + tempFile + " to " + file + ": trying copy", this);
        
        Files.copyFile (tempFile, file);
      }
      
      Log.diagnostic ("Saved \"" + file + "\"", this);
    
      nextSave = Long.MAX_VALUE;
      modified = false;
    } catch (IOException ex)
    {
      Log.alarm ("Failed to save \"" + file + "\"", this, ex);
    }
  }
}
