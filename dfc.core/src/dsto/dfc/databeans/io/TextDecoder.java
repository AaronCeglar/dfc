package dsto.dfc.databeans.io;

/**
 * An object that can decode the text representation of an object into an
 * instance.
 * 
 * @author mpp
 * @version $Revision$
 */
public interface TextDecoder
{
  /**
   * Decode a text value.
   * 
   * @param type The type of value expected.
   * @param text The text form of the value.
   * @return Object The decoded value.
   * @throws IllegalArgumentException if the value cannot be decoded.
   */
  public Object decode (Class type, String text)
    throws IllegalArgumentException;
}
