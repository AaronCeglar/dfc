package dsto.dfc.databeans.io;

/**
 * An object that can encode another object into text form.
 * 
 * @author mpp
 * @version $Revision$
 */
public interface TextEncoder
{
  /**
   * Test if this codec can encode a given object.
   * 
   * @param context The current encode context.
   * @param value The value. May be null.
   * 
   * @return True if this encoder should be used for value.
   */
  public boolean canEncode (XmlEncodeContext context, Object value);
  
  /**
   * Encode a value into text.
   * 
   * @param value The value to encode.
   * @return String The encoded value.
   * @throws IllegalArgumentException if the value cannot be encoded.
   */
  public String encode (Object value)
    throws IllegalArgumentException;
}
