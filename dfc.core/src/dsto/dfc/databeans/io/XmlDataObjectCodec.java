package dsto.dfc.databeans.io;

import java.util.Arrays;
import java.util.Iterator;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;

import org.jdom.Attribute;
import org.jdom.Element;

import dsto.dfc.util.Beans;
import dsto.dfc.util.JavaBean;
import dsto.dfc.util.Objects;

import dsto.dfc.databeans.DataBean;
import dsto.dfc.databeans.IDataBean;
import dsto.dfc.databeans.IDataObject;
import dsto.dfc.databeans.JavaDataBean;

/**
 * XML encoder/decoder for IDataObject's, IDataBean's and vanilla
 * JavaBean's. This handles any object that:
 * 
 * <ul>
 * <li>Implements IDataBean. Includes special handling for
 * JavaDataBean's.
 * <li>Implements IDataObject.</li>
 * <li>Implements the dsto.dfc.util.JavaBean marker interface.
 * </ul>
 * 
 * @author Matthew Phillips
 */
public class XmlDataObjectCodec implements XmlEncoder, XmlDecoder
{
  public boolean shouldPreserveIdentity (XmlEncodeContext context,
                                         Object value)
  {
    return true;
  }

  public boolean canEncode (XmlEncodeContext context, Object value)
  {
    return value instanceof IDataObject || value instanceof JavaBean;
  }
  
  public Element encode (XmlEncodeContext context, Object value)
  {
    Element element;
    
    if (value instanceof IDataBean)
      element = encodeDataBean (context, (IDataBean)value);
    else if (value instanceof IDataObject)
      element = encodeDataObject (context, (IDataObject)value);
    else
      element = encodeJavaBean (context, value);
    
    return element;
  }

  /**
   * Encode an IDataObject.
   */
  protected Element encodeDataObject (XmlEncodeContext context, IDataObject object)
  {
    Element element = new Element ("object");
    context.registerElement (element, object);
    
    element.setAttribute ("type", context.encodeType (object.getClass ())); 
    
    for (Iterator i = object.propertyIterator (); i.hasNext (); )
    {
      Object property = i.next ();
      
      if (object.isTransient (property))
        continue;
    
      encodeDataObjectProperty (context, element, object, property);
    }
    
    return element;
  }
  
  protected void encodeDataObjectProperty (XmlEncodeContext context,
                                           Element element,
                                           IDataObject object,
                                           Object property)
    throws IllegalArgumentException
  {
    Element valueElement = context.encode (object.getValue (property));
  
    // write "nameType" if not a string
    if (!(property instanceof String))
    {
      valueElement.getAttributes ().add
        (0, new Attribute ("nameType",
                           context.encodeType (property.getClass ())));
    }
  
    // add property text encoding as first attribute
    TextEncoder encoder = context.getTextEncoder (property);
    
    valueElement.getAttributes ().add
      (0, new Attribute ("name", encoder.encode (property)));
  
    element.addContent (valueElement);
  }

  /**
   * Encode a JavaBean
   */
  protected Element encodeJavaBean (XmlEncodeContext context, Object bean)
  {
    Element element = new Element ("bean");
    context.registerElement (element, bean);
    
    try
    {
      element.setAttribute ("type", context.encodeType (bean.getClass ())); 
      
      // encode properties
      PropertyDescriptor [] properties =
        Introspector.getBeanInfo (bean.getClass ()).getPropertyDescriptors (); 
      
      for (int i = 0; i < properties.length; i++)
      {
        PropertyDescriptor property = properties [i];
      
        // skip non-read/write properties
        if (property.getWriteMethod () == null ||
            property.getReadMethod () == null)
          continue;
          
        Element valueElement =
          context.encode (property.getReadMethod ().invoke
                           (bean, (Object [])null));
      
        // add name as first attribute
        valueElement.getAttributes ().add
          (0, new Attribute ("name", property.getName ()));
      
        element.addContent (valueElement);
      }
    } catch (IntrospectionException ex)
    {
      throw new IllegalArgumentException
        ("Failed to get properties for " + bean.getClass () + ": " + ex);
    } catch (IllegalAccessException ex)
    {
      throw new IllegalArgumentException
        ("Could not access property for bean " + bean.getClass () + ": " + ex);
    } catch (InvocationTargetException ex)
    {
      throw new IllegalArgumentException
        ("Property accessor for " + bean.getClass () + " threw exception: " +
         ex.getTargetException ());
    }
    
    return element;
  }
                                 
  /**
   * Encode an IDataBean.
   */
  protected Element encodeDataBean (XmlEncodeContext context, IDataBean bean)
  {
    Element element = new Element ("bean");
    context.registerElement (element, bean);
    
    // do wrapped bean
    if (bean.getClass ().equals (JavaDataBean.class))
    {
      element.setAttribute
        ("wrappedBean",
         context.encodeType (((JavaDataBean)bean).getBean ().getClass ())); 
    }
    
    // do type if not a DataBean or JavaDataBean
    if (!bean.getClass ().equals (DataBean.class) &&
        !bean.getClass ().equals (JavaDataBean.class))
    {
      element.setAttribute
        ("type",
         context.encodeType (bean.getClass ())); 
    }
    
    encodeDataBeanProperties (context, bean, element);
    
    return element;
  }
  
  protected void encodeDataBeanProperties (XmlEncodeContext context,
                                            IDataBean bean, Element element)
  {
    String [] properties = bean.getPropertyNames ();
    Arrays.sort (properties);
    
    for (int i = 0; i < properties.length; i++)
    {
      String property = properties [i];
      
      if (!bean.isTransient (property))
      {
        Element valueElement = context.encode (bean.getValue (property));

        // add name as first attribute
        valueElement.getAttributes ().add (0, new Attribute ("name", property));

        element.addContent (valueElement);
      }
    }
  }

  public Object decode (XmlDecodeContext context, Element element)
    throws IllegalArgumentException, ClassNotFoundException
  {
    Class valueType =
      context.decodeType (element.getAttributeValue ("type"));
    
    if (element.getName ().equals ("bean"))
    {
      if (valueType == null || IDataBean.class.isAssignableFrom (valueType))
        return decodeDataBean (context, element, valueType);
      else
        return decodeJavaBean (context, element, valueType);
    } else
    {
      return decodeDataObject (context, element, valueType);
    }
  }
  
  protected IDataObject decodeDataObject (XmlDecodeContext context,
                                          Element element,
                                          Class valueType)
    throws ClassNotFoundException, IllegalArgumentException
  {
    IDataObject object = (IDataObject)Objects.instantiate (valueType);

    context.registerValue (element, object);
    
    for (Iterator i = element.getChildren ().iterator (); i.hasNext (); )
      decodeDataObjectProperty (context, (Element)i.next (), object);
    
    return object;
  }
  
  /**
   * Decode the property of a data object
   * 
   * @param context
   * @param child
   * @param object
   * @return The property decoded.
   * @throws IllegalArgumentException
   * @throws ClassNotFoundException
   */
  protected Object decodeDataObjectProperty (XmlDecodeContext context,
                                           Element child,
                                           IDataObject object)
    throws IllegalArgumentException, ClassNotFoundException
  {
    String childName = child.getAttributeValue ("name");
    
    if (childName == null)
      throw new IllegalArgumentException ("Child has no name");
  
    String childNameType = child.getAttributeValue ("nameType");
    Class childNameClass;
    
    if (childNameType != null)
      childNameClass = context.decodeType (childNameType);
    else
      childNameClass = String.class;
  
    TextDecoder nameDecoder = context.getTextDecoder (childNameClass);
    
    if (nameDecoder == null)
      throw new IllegalArgumentException ("No text decoder for nameType " + childNameType);
              
    Object property = nameDecoder.decode (childNameClass, childName);
    
    setObjectValue (object, property, context.decode (child));
    
    return property;
  }

  /**
   * Decode an IDataBean.
   */
  protected Object decodeDataBean (XmlDecodeContext context,
                                   Element element, Class valueType)
    throws ClassNotFoundException, IllegalArgumentException
  {
    Class wrappedType =
      context.decodeType (element.getAttributeValue ("wrappedBean"));
    IDataBean bean;

    if (valueType != null)
      bean = (IDataBean)Objects.instantiate (valueType);
    else if (wrappedType != null)
      bean = new JavaDataBean (Objects.instantiate (wrappedType));
    else
      bean = new DataBean ();
    
    context.registerValue (element, bean);
    
    for (Iterator i = element.getChildren ().iterator (); i.hasNext (); )
    {
      Element child = (Element)i.next ();
     
      String childName = child.getAttributeValue ("name");
      
      if (childName == null)
        throw new IllegalArgumentException ("Child has no name");
       
      setObjectValue (bean, childName, context.decode (child));
    }
    
    return bean;
  }
  
  /**
   * Called by data object decoders to set property values. Subclasses
   * may override to hook changes.
   */
  protected void setObjectValue (IDataObject object, 
                                  Object property, Object value)
  {
    object.setValue (property, value);
  }

  protected Object decodeJavaBean (XmlDecodeContext context,
                                   Element element, Class valueType)
    throws ClassNotFoundException, IllegalArgumentException
  {    
    try
    {
      Object bean = Objects.instantiate (valueType);
      
      context.registerValue (element, bean);
      
      BeanInfo info = Introspector.getBeanInfo (bean.getClass ());
      PropertyDescriptor [] properties = info.getPropertyDescriptors (); 
      
      for (Iterator i = element.getChildren ().iterator (); i.hasNext (); )
      {
        Element child = (Element)i.next ();
      
        String childName = child.getAttributeValue ("name");
        
        if (childName == null)
          throw new IllegalArgumentException ("Child has no name");
      
        PropertyDescriptor property =
          Beans.findProperty (properties, childName);
      
        if (property == null)
          throw new IllegalArgumentException
            ("Property \"" + childName + "\" is not defined for " +
             valueType);
          
        property.getWriteMethod ().invoke
          (bean, new Object [] {context.decode (child)});
      }
      
      return bean;
    } catch (IntrospectionException ex)
    {
      throw new IllegalArgumentException
        ("Failed to get properties for " + valueType.getClass () + ": " + ex);
    } catch (IllegalAccessException ex)
    {
      throw new IllegalArgumentException
        ("Could not access property for bean " + valueType.getClass () + ": " + ex);
    } catch (InvocationTargetException ex)
    {
      throw new IllegalArgumentException
        ("Property accessor for " + valueType.getClass () + " threw exception: " +
         ex.getTargetException ());
    }
  }
}
