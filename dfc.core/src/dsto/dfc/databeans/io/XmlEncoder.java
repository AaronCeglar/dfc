package dsto.dfc.databeans.io;

import org.jdom.Element;

/**
 * An encoder that can generate an XML representation of an object.
 * 
 * @author mpp
 * @version $Revision$
 */
public interface XmlEncoder
{
  /**
   * Return true if the identity of the given value should be preserved. If
   * true, any instances of the object after the first will be written out as
   * references to the original rather than a copy.
   */
  public boolean shouldPreserveIdentity (XmlEncodeContext context, Object value);
  
  /**
   * Test if this codec can encode a given object.
   * 
   * @param context The current encode context.
   * @param value The value. May be null.
   * 
   * @return True if this encoder should be used for value.
   */
  public boolean canEncode (XmlEncodeContext context, Object value);
  
  /**
   * Generate an XML representation of an object.
   * 
   * @param context The encoding context.
   * @param value The value to be encoded.
   * @return The JDOM element created for the value.
   */
  public Element encode (XmlEncodeContext context, Object value);
}
