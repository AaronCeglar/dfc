package dsto.dfc.databeans.io;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jdom.Element;

import dsto.dfc.util.Objects;

import dsto.dfc.databeans.IDataObject;
import dsto.dfc.databeans.ListDataObject;
import dsto.dfc.databeans.MapDataObject;
import dsto.dfc.databeans.SetDataObject;

/**
 * Handles XML encode/decode of the Java collections types (Set, Map,
 * List, or generic Collection). Also has special handling for
 * IDataObject-compatible collections (eg SetDataObject,
 * ListDataObject, etc).
 * 
 * @author Matthew Phillips
 */
public class XmlCollectionsCodec implements XmlEncoder, XmlDecoder
{
  public boolean shouldPreserveIdentity (XmlEncodeContext context,
                                         Object value)
  {
    return true;
  }

  public boolean canEncode (XmlEncodeContext context, Object value)
  {
    return value instanceof Collection || value instanceof Map;
  }

  public Element encode (XmlEncodeContext context, Object value)
  {
    Element element;
    
    if (value instanceof Set)
      element = encodeSet (context, (Set)value);
    else if (value instanceof Map)
      element = encodeMap (context, (Map)value);
    else
      element = encodeCollection (context, (Collection)value);
    
    return element;
  }

  public Object decode (XmlDecodeContext context, Element element)
    throws IllegalArgumentException, ClassNotFoundException
  {
    String name = element.getName ();
    Object value;
    
    if (name.equals ("list"))
      value = decodeCollection (context, element, ArrayList.class);
    else if (name.equals ("set"))
      value = decodeCollection (context, element, HashSet.class);
    else if (name.equals ("map"))
      value = decodeMap (context, element);
    else if (name.equals ("collection"))
      value = decodeCollection (context, element, null);
    else
      throw new IllegalArgumentException ("Can't decode " + name);
    
    return value;
  }

  protected Element encodeMap (XmlEncodeContext context, Map map)
  {
    Element element = new Element ("map");
    context.registerElement (element, map);
    
    encodeCollectionType (context, element, map, HashMap.class);
    
    IDataObject mapDataObject = null;
    
    if (map instanceof IDataObject)
      mapDataObject = (IDataObject)map;
      
    // encode entries
    for (Iterator i = map.entrySet ().iterator (); i.hasNext (); )
    {
      Map.Entry entry = (Map.Entry)i.next ();
      
      if (mapDataObject != null && mapDataObject.isTransient (entry.getKey ()))
        continue;
        
      element.addContent (context.encode (entry.getKey ()));
      element.addContent (context.encode (entry.getValue ()));
    }
    
    return element;
  }
  
  protected Element encodeSet (XmlEncodeContext context, Set set)
  {
    Element element = new Element ("set");
    context.registerElement (element, set);
    
    encodeCollectionType (context, element, set, HashSet.class);
    
    // read contents via IDataObject interface or Set interface as fallback
    if (set instanceof IDataObject)
    {
      IDataObject setDataObject = (IDataObject)set;
      
      for (Iterator i = setDataObject.propertyIterator (); i.hasNext (); )
      {
        Object property = i.next ();
        
        if (setDataObject.isTransient (property))
          continue;
      
        element.addContent (context.encode (setDataObject.getValue (property)));
      }
    } else
    {
      // encode contents
      for (Iterator i = set.iterator (); i.hasNext (); )
        element.addContent (context.encode (i.next ()));
    }
    
    return element;
  }
  
  /**
   * Encode any java.util.Collection, with some extra special handling
   * for List's.
   */
  protected Element encodeCollection (XmlEncodeContext context,
                                       Collection collection)
  {
    Element element;
    Class defaultType;
    
    if (collection instanceof List)
    {
      element = new Element ("list");
      defaultType = ArrayList.class;
    } else
    {
      element = new Element ("collection");
      defaultType = null;
    }
    
    context.registerElement (element, collection);
    
    encodeCollectionType (context, element, collection, defaultType);

    // encode contents
    for (Iterator i = collection.iterator (); i.hasNext (); )
      element.addContent (context.encode (i.next ()));
    
    return element;
  }
  
  /**
   * Add the type and unmodifiable/synchronized attributes for a
   * collection.
   * 
   * @param context The context.
   * @param element The element to build on.
   * @param collection The collection.
   * @param defaultType The default non-IDataObject type for the
   *          collection. If the collection is not an IDataObject and
   *          this is the same as the collection's actual type, the
   *          type attribute will be omitted.
   */
  protected void encodeCollectionType (XmlEncodeContext context,
                                       Element element,
                                       Object collection,
                                       Class defaultType)
  {
    String className = collection.getClass ().getName ();
    
    // check for internal collection types
    if (className.startsWith ("java.util.Collections$"))
    {
      className = className.substring (22, className.length ());
      
      if (className.startsWith ("Synchronized"))
      {
        element.setAttribute ("synchronized", "true");
      } else if (className.startsWith ("Unmodifiable") ||
                 className.startsWith ("Singleton") ||
                 className.startsWith ("Empty"))
      {
        element.setAttribute ("unmodifiable", "true");
      } else
      {
        throw new IllegalArgumentException
          ("Don't know how to encode collection type " +
           collection.getClass ().getName ());
      }
    } else
    {
      // write type if not one of the standard dynamic types and 
      if (!(collection.getClass ().equals (ListDataObject.class) ||
            collection.getClass ().equals (SetDataObject.class) ||
            collection.getClass ().equals (MapDataObject.class)))
      {
        // collection type is not the default type
        
        if (defaultType == null || !collection.getClass ().equals (defaultType))
        {
          element.setAttribute ("type",
                                context.encodeType (collection.getClass ()));
        } else
        {
          // signal non-dynamic (in the DataObject sense) type 
          element.setAttribute ("dynamic", "false");
        }
      }
    }
  }
  
  protected Object decodeMap (XmlDecodeContext context, Element element)
    throws ClassNotFoundException, IllegalArgumentException
  {
    boolean dynamic =
      element.getAttributeValue ("dynamic", "true").equals ("true");
    
    String valueTypeName = element.getAttributeValue ("type");
    Map map;
    
    if (valueTypeName == null)
    {
      if (dynamic)
        map = new MapDataObject ();
      else
        map = new HashMap ();
    } else
    {
      map = (Map)Objects.instantiate (context.decodeType (valueTypeName));
    }

    context.registerValue (element, map);
    
    // add entries of map
    for (Iterator i = element.getChildren ().iterator (); i.hasNext (); )
    {
      Object key = context.decode ((Element)i.next ());
      Object value = context.decode ((Element)i.next ());
      
      map.put (key, value);
    }
    
    return map;
  }
  
  /**
   * Decode a java.util.Collection.
   */
  protected Object decodeCollection (XmlDecodeContext context,
                                     Element element, Class defaultType)
    throws ClassNotFoundException, IllegalArgumentException
  {
    String name = element.getName ();
    List children = element.getChildren ();
    int size = children.size ();
    
    /** The collection wrapper will be the Collections wrapper instance if
     * the unmodifiable/synchronized options are used. It will point to
     * collection otherwise. */
    Collection wrapper = null;
    /** The actual collection underlying the wrapper. */
    Collection collection;
    
    boolean unmodifiable =
      element.getAttributeValue ("unmodifiable", "false").equals ("true");
    boolean synced =
      element.getAttributeValue ("synchronized", "false").equals ("true");
    boolean dynamic =
      element.getAttributeValue ("dynamic", "true").equals ("true");
    
    if (size == 0 && unmodifiable)
    {
      // use one of the empty collections
      if (name.equals ("set"))
        collection = Collections.EMPTY_SET;
      else
        collection = Collections.EMPTY_LIST;
          
      wrapper = collection;
    } else
    {
      // decide on the type of collection
      String valueTypeName = element.getAttributeValue ("type");
      Class valueType;
      
      if (dynamic && valueTypeName == null)
      {
        if (name.equals ("list"))
          valueType = ListDataObject.class;
        else if (name.equals ("set"))
          valueType = SetDataObject.class;
        else
          throw new IllegalArgumentException ("Cannot handle dynamic " + name);
      } else if (valueTypeName == null)
      {
        valueType = defaultType;
      } else
      {
        valueType = context.decodeType (valueTypeName);
      }
        
      // instantiate collection
      if (valueType.equals (ArrayList.class))
        collection = new ArrayList (size); // small memory optimization
      else
        collection = (Collection)Objects.instantiate (valueType);
      
      // build a wrapper if needed
      wrapper = collection;

      if (synced)
      {
        if (name.equals ("set"))
          wrapper = Collections.synchronizedSet ((Set)wrapper);
        else
          wrapper = Collections.synchronizedList ((List)wrapper);
      }
      
      if (unmodifiable)
      {
        if (name.equals ("set"))
          wrapper = Collections.unmodifiableSet ((Set)wrapper);
        else
          wrapper = Collections.unmodifiableList ((List)wrapper);
      }

      context.registerValue (element, wrapper);
      
      // add contents of collection
      for (Iterator i = children.iterator (); i.hasNext (); )
        collection.add (context.decode ((Element)i.next ()));
    }
    
    return wrapper;
  }
}
