package dsto.dfc.databeans.io;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.jdom.Element;

/**
 * XML encoder/decoder for URL's and URI's.
 * 
 * @author Matthew Phillips
 */
public class XmlURICodec implements XmlEncoder, XmlDecoder
{
  public boolean shouldPreserveIdentity (XmlEncodeContext context,
                                         Object value)
  {
    return false;
  }

  public boolean canEncode (XmlEncodeContext context, Object value)
  {
    return value instanceof URL || value instanceof URI;
  }

  public Element encode (XmlEncodeContext context, Object value)
  {
    if (value instanceof URL)
      return encodeUrl ((URL)value);
    else
      return encodeUri ((URI)value);
  }
  
  public Object decode (XmlDecodeContext context, Element element)
    throws IllegalArgumentException, ClassNotFoundException
  {
    try
    {
      if (element.getName ().equals ("url"))
        return decodeUrl (element);
      else
        return decodeUri (element);
      
    } catch (Exception ex)
    {
      IllegalArgumentException ex2 = new IllegalArgumentException ("Invalid URL/URI");
      
      ex2.initCause (ex);
      
      throw ex2;
    }
  }

  private Element encodeUri (URI uri)
  {
    Element element = new Element ("uri");
    
    element.setAttribute ("value", uri.toString ());
    
    return element;
  }

  private Element encodeUrl (URL url)
  {
    Element element = new Element ("url");
    
    element.setAttribute ("value", url.toExternalForm ());
    
    return element;
  }

  private URI decodeUri (Element element)
    throws URISyntaxException
  {
    return new URI (element.getAttributeValue ("value"));
  }
  
  private URL decodeUrl (Element element)
    throws MalformedURLException
  {
    return new URL (element.getAttributeValue ("value"));
  }
}
