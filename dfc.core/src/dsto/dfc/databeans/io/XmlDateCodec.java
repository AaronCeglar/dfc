package dsto.dfc.databeans.io;

import java.util.Date;

import java.text.ParseException;

import org.jdom.Element;

import dsto.dfc.text.IsoDateFormat;

/**
 * XML codec for java.util.Date using ISO date format
 * 
 * @author Matthew Phillips
 */
public class XmlDateCodec implements XmlEncoder, XmlDecoder
{
  public boolean shouldPreserveIdentity (XmlEncodeContext context,
                                         Object value)
  {
    return false;
  }

  public boolean canEncode (XmlEncodeContext context, Object value)
  {
    return value instanceof Date;
  }
  
  public Element encode (XmlEncodeContext context, Object value)
  {
    Element element = new Element ("date");
    
    element.setAttribute ("value", IsoDateFormat.get ().format ((Date)value));
    
    return element;
  }
  
  public Object decode (XmlDecodeContext context, Element element)
    throws IllegalArgumentException, ClassNotFoundException
  {
    try
    {
      return IsoDateFormat.get ().parse (element.getAttributeValue ("value"));
    } catch (ParseException ex)
    {
      throw new IllegalArgumentException ("Invalid date: " + ex.getMessage ());
    }
  }
}
