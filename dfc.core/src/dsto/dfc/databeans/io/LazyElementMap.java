package dsto.dfc.databeans.io;

import java.util.HashMap;

import dsto.dfc.logging.Log;

/**
 * Provides a mapping from an XML element type to a decoder for that type.
 * Decoders may be either be XmlDecoder instances, or a class name for which an
 * instance is lazily instantiated on demand.
 * 
 * @see dsto.dfc.databeans.io.LazyTypeMap
 * 
 * @author mpp
 * @version $Revision$
 */
public final class LazyElementMap
{
  private final HashMap map;
  
  public LazyElementMap ()
  {
    this.map = new HashMap ();
  }

  public LazyElementMap (LazyElementMap t)
  {
    this.map = new HashMap (t.map);
  }
  
  public void put (String type, XmlDecoder decoder)
  {
    map.put (type, decoder);
  }
  
  public void put (String type, String decoderClassName)
  {
    map.put (type, decoderClassName);
  }
  
  public void remove (String type)
  {
    map.remove (type);
  }
  
  public XmlDecoder get (String type)
  {
    Object value = map.get (type);
    
    if (value instanceof String)
    {
      try
      {
        Class valueClass = Class.forName (value.toString ());
        
        value = valueClass.newInstance ();
        
        map.put (type, value);
      } catch (Exception ex)
      {
        // stop further errors
        map.remove (type);

        Log.internalError
          ("Exception while instantiating " + value.getClass (), this, ex);
          
        throw new RuntimeException
         ("Exception while instantiating " + value.getClass () + ": " + ex);
      }
    }
    
    return (XmlDecoder)value;
  }
}
