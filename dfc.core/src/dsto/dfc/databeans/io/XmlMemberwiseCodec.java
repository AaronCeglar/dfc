package dsto.dfc.databeans.io;

import java.util.Iterator;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.jdom.Attribute;
import org.jdom.Element;

import dsto.dfc.util.Objects;

/**
 * An XML codec that encodes/decodes objects by reading their data
 * fields.
 * <p>
 * 
 * Example usage:
 * 
 * <pre>
 * SerializeXmlCodec.register (&quot;person&quot;, &quot;org.app.Person&quot;);
 * </pre>
 * 
 * @author phillipm
 */
public class XmlMemberwiseCodec implements XmlEncoder, XmlDecoder
{
  private String name;
  private Class type;
  
  /**
   * Register this codec for a given object type.
   * 
   * @param name The XML tag name to use (eg "person").
   * @param type The type of object (eg Person.class).
   */
  public static void register (String name, Class type)
  {
    XmlMemberwiseCodec codec = new XmlMemberwiseCodec (name, type);

    XmlOutput.registerGlobalXmlEncoder (codec);
    XmlInput.registerGlobalXmlDecoder (name, codec);
  }

  public XmlMemberwiseCodec (String name, Class type)
  {
    this.name = name;
    this.type = type;
  }

  public boolean shouldPreserveIdentity (XmlEncodeContext context, Object value)
  {
    return true;
  }

  public boolean canEncode (XmlEncodeContext context, Object value)
  {
    return value != null && type.isAssignableFrom (value.getClass ());
  }
  
  public Element encode (XmlEncodeContext context, Object value)
  {
    Element element = new Element (name);
    
    context.registerElement (element, value);
    
    encodeFields (context, element, value);
    
    return element;
  }

  public Object decode (XmlDecodeContext context, Element element)
    throws IllegalArgumentException, ClassNotFoundException
  {
    Object object = Objects.instantiate (type);
    
    context.registerValue (element, object);
    
    decodeFields (context, element, object);
    
    return object;
  }
  
  /**
   * Encode the fields of an object into an XML element.
   */
  public static void encodeFields (XmlEncodeContext context,
                                     Element element, Object object)
    throws IllegalArgumentException
  {
    try
    {
      // walk up inheritance heirachy...
      for (Class c = object.getClass (); c != Object.class; c = c.getSuperclass ())
      {
        // encode fields
        Field [] fields = c.getDeclaredFields ();

        for (int i = 0; i < fields.length; i++)
        {
          Field field = fields [i];
          
          if ((field.getModifiers () & (Modifier.STATIC | Modifier.TRANSIENT)) != 0)
            continue;
        
          field.setAccessible (true);
          
          Element valueElement = context.encode (field.get (object));
          
          valueElement.getAttributes ().add
            (0, new Attribute ("name", field.getName ()));
        
          element.addContent (valueElement);
        }
      }
    } catch (SecurityException ex)
    {
      throw new IllegalArgumentException
        ("Security manager veto on changing field access: " + ex);
    } catch (IllegalAccessException ex)
    {
      throw new IllegalArgumentException
        ("Veto on field access: " + ex);
    }
  }
  
  /**
   * Decode the fields of an object from an XML element.
   */
  public static void decodeFields (XmlDecodeContext context,
                                     Element element, Object object)
    throws IllegalArgumentException, ClassNotFoundException
  {
    Class valueType = object.getClass ();
    String name = null;
    
    try
    {
      for (Iterator i = element.getChildren ().iterator (); i.hasNext (); )
      {
        Element child = (Element)i.next ();
      
        name = child.getAttributeValue ("name");
        
        if (name == null)
          throw new IllegalArgumentException ("Child has no name");
                  
        Object childValue = context.decode (child);
      
        // @todo check efficiency here: do we need to cache resolved fields?
        Field field = null;
        
        for (Class c = valueType; c != Object.class; c = c.getSuperclass ())
        {
          try
          {
            field = c.getDeclaredField (name);
            field.setAccessible (true);
        
            field.set (object, childValue);
          } catch (NoSuchFieldException ex)
          {
            // zip
          }
        }
        
        if (field == null)
          throw new IllegalArgumentException ("Non-existent field " + name);
      }
    } catch (SecurityException ex)
    {
      throw new IllegalArgumentException
        ("Security veto on property: " + ex);
    } catch (IllegalAccessException ex)
    {
      throw new IllegalArgumentException
        ("Illegal access to property: " + ex);
    }
  }
}
