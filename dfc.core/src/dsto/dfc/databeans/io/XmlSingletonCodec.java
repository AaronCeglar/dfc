package dsto.dfc.databeans.io;

import org.jdom.Element;

import dsto.dfc.util.Beans;
import dsto.dfc.util.Singleton;

/**
 * An XML encoder/decoder that handles singleton objects as defined by
 * {@link dsto.dfc.util.Beans#isSingletonClass(Class)}.
 * 
 * @author mpp
 * @version $Revision$
 * 
 * @see dsto.dfc.util.Singleton
 */
public class XmlSingletonCodec implements XmlDecoder, XmlEncoder
{
  public boolean shouldPreserveIdentity (XmlEncodeContext context, Object value)
  {
    return false;
  }

  public boolean canEncode (XmlEncodeContext context, Object value)
  {
    return value instanceof Singleton;
  }
  
  public Element encode (XmlEncodeContext context, Object value)
  {
    Element element = new Element ("singleton");
    
    String type = context.encodeType (value.getClass ());
    
    element.setAttribute ("type", type);
    
    return element;
  }

  public Object decode (XmlDecodeContext context, Element element)
    throws IllegalArgumentException, ClassNotFoundException
  {
    String type = element.getAttributeValue ("type");
    
    return Beans.getSingletonInstance (context.decodeType (type));
  }
}
