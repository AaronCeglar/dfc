package dsto.dfc.databeans.io;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jdom.Element;

import dsto.dfc.collections.TypeMap;
import dsto.dfc.text.StringUtility;

/**
 * Stores context information (eg ID -> value mapping) during an XML decode
 * session. Also mediates decoding via {@link #decode(Element)}.<p>
 * 
 * XmlDecoder clients receive an instance via
 * {@link XmlDecoder#decode(XmlDecodeContext, Element)}.
 * 
 * @author mpp
 * @version $Revision$
 */
public class XmlDecodeContext
{
  private LazyElementMap xmlDecoders;
  private TypeMap textDecoders;
  private Map idToValue;
  private Map typeAliases;
  private Map properties;
  private List idCallbacks;
  
  protected XmlDecodeContext (LazyElementMap xmlDecoders, TypeMap textDecoders,
                               Map typeAliases)
  {
    this.xmlDecoders = xmlDecoders;
    this.textDecoders = textDecoders;
    this.idToValue = new HashMap ();
    this.typeAliases = typeAliases;
    this.properties = Collections.EMPTY_MAP;
    this.idCallbacks = new ArrayList ();
  }

  protected void doPostDecode ()
  {
    if (!idCallbacks.isEmpty ())
    {
      for (Iterator i = idCallbacks.iterator (); i.hasNext ();)
      {
        IdCallbackEntry entry = (IdCallbackEntry)i.next ();
        
        if (!doIdCallback (entry.callback, entry.element,
                           entry.id, entry.extraData))
        {
          throw new IllegalArgumentException
            ("No value for ID " + entry.id + " was defined");
        }
      }
    }
  }
  
  /**
   * Set all custom properties. Used by {@link XmlInput}. 
   * 
   * @see #setProperty(Object, Object)
   */
  protected void setProperties (Map properties)
  {
    this.properties = properties;
  }
  
  /**
   * Define a custom property that will be accessible during this session
   * via {@link #getProperty(Object)}.
   * 
   * @see XmlInput#setProperty(Object, Object)
   */
  public void setProperty (Object name, Object value)
  {
    if (properties == Collections.EMPTY_MAP)
      properties = new HashMap ();

    properties.put (name, value);
  }
  
  /**
   * Get a custom property defined by {@link #setProperty(Object, Object)}.
   */
  public Object getProperty (Object name)
  {
    return properties.get (name);
  }
  
  /**
   * Mediates the decoding of an element. This method should be used in
   * preference to a direct call to the registered XmlDecoder for the element
   * since it adds automatic management of id/refid.
   * 
   * @param element The element to decode.
   * @return The decoded value.
   * @throws ClassNotFoundException if a class referenced by the element tree
   * is not found.
   * @throws IllegalArgumentException if there is a logical error in the element
   * tree.
   */
  public Object decode (Element element)
    throws ClassNotFoundException, IllegalArgumentException
  {
    String ref = element.getAttributeValue ("ref");
    
    if (ref != null)
    {
      return getValue (ref);
    } else
    {
      XmlDecoder decoder = getXmlDecoder (element.getName ());
        
      Object value = decoder.decode (this, element);
      
      // register an ID if present
      registerValue (element, value);
        
      return value;
    }
  }
  
  /**
   * Get the decoder registered for a given element type. Clients will not
   * usually need to call this: it is used by {@link #decode(Element)}.
   * 
   * @param elementName The element name (eg from element.getName ()).
   * @return XmlDecoder The decoder registered for the element.
   * @throws IllegalArgumentException if there is no decoder for the element.
   */
  public XmlDecoder getXmlDecoder (String elementName)
    throws IllegalArgumentException
  {
    XmlDecoder decoder = xmlDecoders.get (elementName);
    
    if (decoder != null)
      return decoder;
    else
      throw new IllegalArgumentException 
        ("Don't know how to decode \"" + elementName + "\" elements");
  }
  
  /**
   * Get the text decoder for a given value type.
   */
  public TextDecoder getTextDecoder (Class type)
  {
    return (TextDecoder)textDecoders.get (type);
  }
  
  /**
   * Get a value registered with a given ID. Clients will not usually need
   * to call this: it is used by {@link #decode(Element)}.
   * 
   * @param id The value ID.
   * @return The value.
   * @exception IllegalArgumentException if no value for ID.
   * 
   * @see #registerValue(String, Object)
   */
  public Object getValue (String id)
    throws IllegalArgumentException
  {
    Object value = idToValue.get (id);
    
    if (value != null)
      return value;
    else
      throw new IllegalArgumentException ("No element with ID \"" + id + "\"");
  }
  
  /**
   * Register a value for an ID if one is specified on an element. This
   * is called automatically by {@link #decode(Element)} after invoking
   * an XmlDecoder, but a decoder may choose to call this as soon as it creates
   * its value so that any child decoders can reference it.
   * 
   * @param element The element. If the element has no "id" attribute, this
   * call does nothing.
   * @param value The value to register.
   */
  public void registerValue (Element element, Object value)
  {
    String id = element.getAttributeValue ("id");
      
    // register an ID if present
    if (id != null)
      registerValue (id, value);
  }
  
  /**
   * Register a value with a given ID. Clients will not usually need
   * to call this: it is used by {@link #decode(Element)}.
   * 
   * @param id The ID.
   * @param value The value.
   * @exception IllegalArgumentException if ID is already in use by another
   * value.
   * 
   * @see #getValue(String)
   */
  public void registerValue (String id, Object value)
    throws IllegalArgumentException
  {
    Object oldValue = idToValue.get (id);
    
    if (oldValue == null)
      idToValue.put (id, value);
    else if (oldValue != value)
      throw new IllegalArgumentException ("Object ID \"" + id + "\" is already in use");
  }
  
  /**
   * Get the type name for an alias.
   * 
   * @param alias The alias.
   * @return The type (class) name for the alias, or null if it does not have
   * one.
   * 
   * @see #aliasToType(String)
   */
  public String getAliasType (String alias)
  {
    return (String)typeAliases.get (alias);
  }
  
  /**
   * Map an alias to it's a type value if it has one. Similar to
   * {@link #getAliasType(String)} except it returns the parameter if no alias
   * mapping is found.
   * 
   * @param alias The alias/type name.
   * @return The type value for typeName. If there is no type value for the
   * alias, the alias is returned.
   */
  public String aliasToType (String alias)
  {
    String typeName = (String)typeAliases.get (alias);
    
    if (typeName != null)
      return typeName;
    else
      return alias;
  }
  
  /**
   * Decode an encoded type into the original Class.
   * 
   * @see XmlEncodeContext#encodeType(Class)
   */
  public Class decodeType (String typeName)
    throws ClassNotFoundException
  {
    if (typeName == null)
      return null;

    if (typeName.endsWith ("[]"))
    {
      int dimension = arrayDimension (typeName, '[');
      
      typeName =
        StringUtility.characters ('[', dimension) +
          primitiveTypeSymbol (stripArrayBrackets (typeName, dimension));
    } else if (typeName.endsWith ("{}"))
    {
      int dimension = arrayDimension (typeName, '{');
      
      typeName =
        StringUtility.characters ('[', dimension) +
          'L' + aliasToType (stripArrayBrackets (typeName, dimension)) + ';';
    } else
    {
      typeName = aliasToType (typeName);
    }
    
    return Class.forName (typeName);
  }

  /**
   * Remove the brackets used to encode arrays.
   * 
   * @param typeName The encoded type name.
   * @param dimension The mult-dimensionality of the array.
   * 
   * @see #arrayDimension(String, char)
   */
  private String stripArrayBrackets (String typeName, int dimension)
  {
    return typeName.substring (0, typeName.length () - ((dimension * 2) + 1));
  }

  /**
   * Find the multi-dimensionality of an array as encoded as the numbe
   * of bracket pairs.
   * 
   * @param typeName The encoded type name.
   * @param bracket The leading bracket character
   */
  private static int arrayDimension (String typeName, char bracket)
  {
    int dimension = 0;
    
    for (int i = typeName.length () - 1; i >= 0; i--)
    {
      if (typeName.charAt (i) == bracket)
        dimension++;
    }
    
    return dimension;
  }

  /**
   * The primitive type symbol for a given primitive type.
   * 
   * See the "Type Signatures" section in the JNI docs
   * (http://java.sun.com/j2se/1.4.2/docs/guide/jni/spec/types.html#wp16437)
   * for more info on the single-character symbol primitives.
   */
  private static char primitiveTypeSymbol (String typeName)
  {
    // boolean and long both have special values, the rest are just first char
    if (typeName.equals ("boolean"))
      return 'Z';
    else if (typeName.equals ("long"))
      return 'J';
    else
      return Character.toUpperCase (typeName.charAt (0));
  }

  private boolean doIdCallback (IdCallback callback,
                                  Element element,
                                  String id,
                                  Object extraData)
  {
    Object existingValue = idToValue.get (id);
    
    if (existingValue != null)
    {
      callback.objectForIdCallback
        (this, element, existingValue, extraData);
      
      return true;
    } else
    {
      return false;
    }
  }
  
  /**
   * Register a callback to be invoked when a value becomes available
   * for a given ID. This can be used if a client wants to refer to an
   * ID for a value that may not have been decoded yet. When an value
   * is defined the client will be notified via
   * {@link IdCallback#objectForIdCallback(XmlDecodeContext, Element, Object, Object)}.
   * The callback will be invoked immediately if an ID is already
   * available.
   * 
   * @param callback The callback target.
   * @param element The element param to be passed to the callback.
   * @param id The id for which a value is needed.
   * @param extraData Any extra data to be passed to the callback (may
   *          be null).
   * 
   * @see IdCallback#objectForIdCallback(XmlDecodeContext, Element, Object, Object)
   * @see XmlDecodeContext#callbackWithObjectForId(IdCallback,
   *      Element, String, Object)
   */
  public void callbackWithObjectForId
    (IdCallback callback, Element element, String id, Object extraData)
  {
    if (!doIdCallback (callback, element, id, extraData))
    {
      idCallbacks.add (new IdCallbackEntry (callback, element,
                                            id, extraData));
    }
  }
  
  /**
   * Callback interface for clients that wish to use forward references.
   */
  public interface IdCallback
  {
    /**
     * Called by decode context when an value becomes available for a given ID
     * .
     * See {@link XmlDecodeContext#callbackWithObjectForId(IdCallback, Element, String, Object)}.
     * 
     * @param context The current decode context.
     * @param element The element param passed to callbackWithObjectForId ().
     * @param object The object that is registered with the requested ID.
     * @param extraData The extra data param passed to callbackWithObjectForId ().
     */
    public void objectForIdCallback (XmlDecodeContext context,
                                      Element element,
                                      Object object,
                                      Object extraData);
  }
 
  /**
   * Used to store forward reference callbacks.
   */
  private static class IdCallbackEntry
  {
    public IdCallback callback;
    public Element element;
    public String id;
    public Object extraData;

    public IdCallbackEntry (IdCallback callback,
                            Element element,
                            String id,
                            Object extraData)
    {
      this.callback = callback;
      this.element = element;
      this.id = id;
      this.extraData = extraData;
    }
  }
}
