package dsto.dfc.databeans.io;

import java.util.HashMap;
import java.util.Map;


/**
 * XML input/output support.
 * 
 * @author mpp
 * @version $Revision$
 */
public final class XmlIO
{
  private static final HashMap globalTypeToAlias;
  private static final HashMap globalAliasToType;
  
  static
  {
    globalTypeToAlias = new HashMap ();
    globalAliasToType = new HashMap ();
    
    addTypeAlias ("string", "java.lang.String");
    addTypeAlias ("int", "java.lang.Integer");
    addTypeAlias ("boolean", "java.lang.Boolean");
    addTypeAlias ("long", "java.lang.Long");
    addTypeAlias ("short", "java.lang.Short");
    addTypeAlias ("float", "java.lang.Float");
    addTypeAlias ("double", "java.lang.Double");
    addTypeAlias ("char", "java.lang.Character");
    addTypeAlias ("byte", "java.lang.Byte");
  }
  
  private XmlIO ()
  {
    // zip
  }

  /**
   * @see #addTypeAlias(String,String)
   */
  public static void addTypeAlias (String alias, Class type)
  {
    addTypeAlias (alias, type.getName ());
  }
  
  /**
   * Add a global type alias. This can be used to provide shortened names for
   * types eg "java.lang.String" becomes "string".
   * 
   * @param alias The type alias.
   * @param type The type for the alias.
   * 
   * @see #getAliasToType()
   * @see #getTypeToAlias()
   */
  public static void addTypeAlias (String alias, String type)
  {
    globalAliasToType.put (alias, type);
    globalTypeToAlias.put (type, alias); 
  }
  
  public static String getType (String classType)
  {
    return (String)globalTypeToAlias.get (classType);
  }
  
  /**
   * Get a map that maps type names to their aliases.
   * 
   * @see #addTypeAlias(String, String)
   */
  public static Map getTypeToAlias ()
  {
    return globalTypeToAlias;
  }

  /**
   * Get a map that maps aliases to their type names.
   * 
   * @see #addTypeAlias(String, String)
   */  
  public static Map getAliasToType ()
  {
    return globalAliasToType;
  }
}
