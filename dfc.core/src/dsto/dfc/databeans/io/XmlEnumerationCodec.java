package dsto.dfc.databeans.io;

import org.jdom.Element;

import dsto.dfc.util.EnumerationValue;
import dsto.dfc.util.Objects;
import dsto.dfc.util.StringEnumerationValue;

/**
 * Coder/decoder for enumerated values based on
 * {@link dsto.dfc.util.StringEnumerationValue}.
 * 
 * @author mpp
 * @version $Revision$
 */
public class XmlEnumerationCodec implements XmlDecoder, XmlEncoder
{
  public boolean shouldPreserveIdentity (XmlEncodeContext context, Object value)
  {
    return false;
  }

  public boolean canEncode (XmlEncodeContext context, Object value)
  {
    return value instanceof StringEnumerationValue;
  }
  
  public Object decode (XmlDecodeContext context, Element element)
    throws IllegalArgumentException, ClassNotFoundException
  {
    Class type = context.decodeType (element.getAttributeValue ("type"));
    String enumName = element.getAttributeValue ("value");
    StringEnumerationValue value =
      (StringEnumerationValue)Objects.instantiate (type);
    
    EnumerationValue enumValue = value.findValue (enumName);
    
    if (enumValue == null)
    {
      throw new IllegalArgumentException
        ("\"" + enumName + "\" is not valid for enumeration " +
         element.getAttributeValue ("type"));
    }
      
    return enumValue;
  }

  public Element encode (XmlEncodeContext context, Object value)
  {
    Element element = new Element ("enum");
    
    element.setAttribute ("value", ((StringEnumerationValue)value).getName ());
    element.setAttribute ("type", context.encodeType (value.getClass ()));
    
    return element;
  }
}
