package dsto.dfc.databeans.io;

import java.util.List;

import org.jdom.Element;

import dsto.dfc.databeans.DataBeanLink;
import dsto.dfc.databeans.DataObjectLink;
import dsto.dfc.databeans.IDataBean;
import dsto.dfc.databeans.IDataObject;
import dsto.dfc.databeans.IDataObjectLink;

/**
 * XML codec for the standard IDataObjectLink implementations:
 * DataObjectLink and DataBeanLink.
 * 
 * todo merge support for weak links in here.
 * 
 * @author mpp
 * @version $Revision$
 */
public class XmlLinkCodec
  implements XmlEncoder, XmlDecoder,
               XmlEncodeContext.IdCallback,
               XmlDecodeContext.IdCallback
{
  public boolean shouldPreserveIdentity (XmlEncodeContext context, Object value)
  {
    return true;
  }
  
  public boolean canEncode (XmlEncodeContext context, Object value)
  {
    return value instanceof DataObjectLink ||
            value instanceof DataBeanLink;
  }

  public Element encode (XmlEncodeContext context, Object value)
  {
    IDataObjectLink link = (IDataObjectLink)value;
    
    Element element = new Element ("link");

    if (link.isSoftLink ())
    {
      // default link is DataBeanLink
      if (!(link instanceof IDataBean))
        element.setAttribute ("type", "object");

      context.callbackWithIdForObject (this, element, link.getLinkTarget (), null);
    } else
    {
      // encode hard links inline
      element.addContent (context.encode (link.getLinkTarget ()));
    }
    
    return element;
  }

  public Object decode (XmlDecodeContext context, Element element)
    throws IllegalArgumentException, ClassNotFoundException
  {
    IDataObjectLink link;
    
    // if a hard link with inline target...
    if (element.getContentSize () > 0)
    {
      List children = element.getChildren ();
      
      if (children.size () != 1)
        throw new IllegalArgumentException
          ("Link must have only one child: " + children.size ());
      
      Object linkedValue =
        context.decode ((Element)children.get (0));
      
      if (linkedValue instanceof IDataBean)
        link = new DataBeanLink ((IDataBean)linkedValue, false);
      else
        link = new DataObjectLink ((IDataObject)linkedValue, false);
    } else
    {
      // create an empty link instance and register for fix up
      if (element.getAttributeValue ("type", "bean").equals ("object"))
        link = new DataObjectLink ();
      else
        link = new DataBeanLink ();
      
      context.callbackWithObjectForId
        (this, element, XmlUtil.getAttribute (element, "target"), link);
    }
    
    return link;
  }
  
  public void idForObjectCallback (XmlEncodeContext context,
                                    Element element,
                                    String id, Object extraData)
  {
    element.setAttribute ("target", id);
  }
  
  public void objectForIdCallback (XmlDecodeContext context,
                                    Element element,
                                    Object object, Object extraData)
  {
    if (extraData instanceof DataBeanLink)
      ((DataBeanLink)extraData).setLinkTarget ((IDataBean)object);
    else
      ((DataObjectLink)extraData).setLinkTarget ((IDataObject)object);
  }
}
