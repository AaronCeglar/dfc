package dsto.dfc.databeans.io;

import java.util.List;

import org.jdom.Element;

import dsto.dfc.databeans.PropertyPath;

/**
 * XML codec for {@link dsto.dfc.databeans.PropertyPath} objects.
 * 
 * @author Matthew Phillips
 */
public class XmlPropertyPathCodec implements XmlDecoder, XmlEncoder
{
  public boolean shouldPreserveIdentity (XmlEncodeContext context,
                                         Object value)
  {
    return false;
  }

  public boolean canEncode (XmlEncodeContext context, Object value)
  {
    return value instanceof PropertyPath;
  }
  
  public Element encode (XmlEncodeContext context, Object value)
  {
    PropertyPath path = (PropertyPath)value;
    Element root = new Element ("propertypath");
    
    for (int i = 0; i < path.length(); i++)
      root.addContent (context.encode (path.element (i)));
    
    return root;
  }
  
  public Object decode (XmlDecodeContext context, Element element)
    throws IllegalArgumentException, ClassNotFoundException
  {
    List children = element.getChildren ();
    Object [] elements = new Object [children.size ()];
    
    for (int i = 0; i < elements.length; i++)
      elements [i] = context.decode ((Element)children.get (i));

    return new PropertyPath (elements);
  }
}
