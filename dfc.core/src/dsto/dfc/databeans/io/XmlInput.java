package dsto.dfc.databeans.io;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

/**
 * Manages decoding of data bean objects from XML.<p>
 * 
 * Basic usage:
 * <pre>
 *   Reader reader = new FileReader ("file.dbxml");
 * 
 *   Object decodedObject = xmlInput.read (reader);
 *    
 *   reader.close ();
 * </pre>
 * 
 * @author mpp
 * @version $Revision$
 */
public class XmlInput
{
  private static final LazyElementMap globalXmlDecoders;
  private static final LazyTypeMap globalTextDecoders;
  
  private static String defaultXmlReaderClass;
  
  /** Maps element names to the XmlDecoder for that element type. */
  private LazyElementMap xmlDecoders;
  /** Maps object types to the TextDecoder for that type. */
  private LazyTypeMap textDecoders;
  /** Maps type aliases (eg "string") to type names (eg "java.lang.String"). */
  private Map typeAliases;
  /** The JDOM document builder. */
  private SAXBuilder builder;
  private int majorVersion;
  private int minorVersion;
  private Map properties;
  private boolean multiObject;
  private XmlDecodeContext context;
  
  static
  {
    XmlDefaultDecoder defaultDecoder = new XmlDefaultDecoder ();
    XmlCollectionsCodec collectionsDecoder = new XmlCollectionsCodec ();
    XmlDataObjectCodec dataObjectCodec = new XmlDataObjectCodec ();
    
    globalXmlDecoders = new LazyElementMap ();
    globalXmlDecoders.put ("bean", dataObjectCodec);
    globalXmlDecoders.put ("object", dataObjectCodec);
    globalXmlDecoders.put ("list", collectionsDecoder);
    globalXmlDecoders.put ("set", collectionsDecoder);
    globalXmlDecoders.put ("map", collectionsDecoder);
    globalXmlDecoders.put ("collection", collectionsDecoder);
    globalXmlDecoders.put ("array", defaultDecoder);
    globalXmlDecoders.put ("string", defaultDecoder);
    globalXmlDecoders.put ("int", defaultDecoder);
    globalXmlDecoders.put ("short", defaultDecoder);
    globalXmlDecoders.put ("float", defaultDecoder);
    globalXmlDecoders.put ("double", defaultDecoder);
    globalXmlDecoders.put ("long", defaultDecoder);
    globalXmlDecoders.put ("boolean", defaultDecoder);
    globalXmlDecoders.put ("char", defaultDecoder);
    globalXmlDecoders.put ("byte", defaultDecoder);
    globalXmlDecoders.put ("null", defaultDecoder);
    globalXmlDecoders.put ("date", new XmlDateCodec ());
    
    XmlURICodec uriCodec = new XmlURICodec ();
    
    globalXmlDecoders.put ("url", uriCodec);
    globalXmlDecoders.put ("uri", uriCodec);
    
    globalXmlDecoders.put ("enum",      "dsto.dfc.databeans.io.XmlEnumerationCodec");
    globalXmlDecoders.put ("singleton", "dsto.dfc.databeans.io.XmlSingletonCodec");
    globalXmlDecoders.put ("link",      "dsto.dfc.databeans.io.XmlLinkCodec");
    globalXmlDecoders.put ("alias",     "dsto.dfc.databeans.io.XmlAliasCodec");
    globalXmlDecoders.put ("propertypath", "dsto.dfc.databeans.io.XmlPropertyPathCodec");
        
    globalTextDecoders = new LazyTypeMap ();
    globalTextDecoders.put (Object.class, new TextDefaultCodec ());
  }
  
  /**
   * Set the default SAX driver class that will be used by JDOM. Use null
   * for system default. 
   */
  public static void setDefaultXmlReaderClass (String className)
  {
    defaultXmlReaderClass = className;
  }
  
  public static String getDefaultXmlReaderClass ()
  {
    return defaultXmlReaderClass;
  }
  
  /**
   * Create a new instance using the default XML reader.
   * Use {@link #read(Reader)} to read objects.
   */
  public XmlInput ()
  {
    this (defaultXmlReaderClass);
  }
  
  /**
   * Create a new instance. Use {@link #read(Reader)} to read objects.
   * 
   * @param xmlReaderClass The class name of a SAX XMLReader.
   */
  public XmlInput (String xmlReaderClass)
  {
    this.builder = xmlReaderClass == null ? 
      new SAXBuilder (false) : new SAXBuilder (xmlReaderClass, false);
    this.builder = new SAXBuilder (false);
    this.xmlDecoders = globalXmlDecoders;
    this.textDecoders = globalTextDecoders;
    this.typeAliases = XmlIO.getAliasToType ();
    this.properties = Collections.EMPTY_MAP;
  }
  
  /**
   * Define a custom property that will be accessible during this session
   * via {@link #getProperty(Object)} and
   * {@link XmlDecodeContext#getProperty(Object)}.
   */
  public void setProperty (Object name, Object value)
  {
    if (properties == Collections.EMPTY_MAP)
      properties = new HashMap ();

    properties.put (name, value);
  }
  
  /**
   * Get a custom property defined by {@link #setProperty(Object, Object)}.
   */
  public Object getProperty (Object name)
  {
    return properties.get (name);
  }
  
  /**
   * See {@link #setMultiObject(boolean)}.
   */
  public boolean isMultiObject ()
  {
    return multiObject;
  }

  /**
   * If set to true (default = false), then multiple elements may be
   * decoded by calling the decode () methods multiple times while
   * sharing the same context. Reusing the decode context means that
   * if an object decoded by one decode () call references objects
   * defined in another call, the links will resolve. This intended
   * for the case where you might wish to have two object trees
   * encoded separately into two XML documents.
   * <p>
   * 
   * IMPORTANT: If this option is enabled, it is up to the client to
   * call {@link #finished()} when the decode session is complete,
   * otherwise outstanding links may not be resolved.
   */
  public void setMultiObject (boolean newValue)
  {
    this.multiObject = newValue;
  }

  /**
   * Create and configure a context for decoding based on this input's
   * settings.
   */
  private void maybeCreateContext ()
  {
    if (context == null)
    {
      context =
        new XmlDecodeContext (xmlDecoders, textDecoders, typeAliases);
      
      context.setProperties (properties);
    }
  }
  
  /**
   * Signal reading is finished. Only needed if you enable multi
   * object decoding (@link #setMultiObjectDecode(boolean)}, otherwise
   * this is done automatically after each decode () call.
   */
  public void finished ()
  {
    if (context != null)
    {
      context.doPostDecode ();
      
      context = null;
    }
  }

  /**
   * Read an object from an XML stream. Reads a serialized JDOM document using
   * {@link #readDocument(Reader)} and then decodes the document using
   * {@link #read(Document)}. If a DBXML header is present, the major/minor
   * version numbers are read and the first child of the header is read.
   * 
   * @param stream The stream to read from.
   * @return The deserialized value.
   * @throws JDOMException If the JDOM document builder encounters an error.
   * @throws ClassNotFoundException If a class referenced by the serialized
   * stream cannot be found.
   */
  public Object read (Reader stream)
    throws JDOMException, IOException, ClassNotFoundException
  {
    Document doc = readDocument (stream);

    return read (doc);
  }

  /**
   * Read a document from an XML stream using the JDOM SAX reader.
   * 
   * @param stream The stream to read from.
   * @return The document.
   * @throws JDOMException If the JDOM SAX document builder encounters an error.
   * 
   * @see #readElement(Reader)
   * @see #read(Reader)
   */
  public Document readDocument (Reader stream)
    throws JDOMException, IOException
  {
    Document doc = builder.build (stream);
    
    Element element = doc.getRootElement ();
    
    if (element.getName ().equals ("dbxml"))
    {
      String version = element.getAttributeValue ("version", "0.0");
      
      int dotIndex = version.indexOf ('.');
      
      majorVersion = Integer.parseInt (version.substring (0, dotIndex));
      minorVersion = Integer.parseInt (version.substring (dotIndex + 1));
    }

    return doc;
  }
  
  /**
   * Shortcut to read the XML and return the root element.
   */
  public Element readElement (Reader reader)
    throws JDOMException, IOException
  {
    return readDocument (reader).getRootElement ();
  }
  
  /**
   * Shortcut to read an object from a DBXML file.
   */
  public Object readFile (String filename)
    throws ClassNotFoundException, IOException, IllegalArgumentException
  {
    Reader reader = new BufferedReader (new FileReader (filename));
    
    try
    {
      return read (readDocument (reader));
    } catch (JDOMException ex)
    {
      throw new IOException ("XML parse error: " + ex.getMessage ());
    } finally
    {
      reader.close ();
    }
  }
  
  /**
   * Read an object from a JDOM document.
   * 
   * @param document The document to read from.
   * @return The decoded value.
   * @throws IllegalArgumentException if an error is found in the structure
   * of the document.
   * @throws ClassNotFoundException If a class referenced by the document
   * cannot be found.
   * 
   * @see #read(Element)
   * @see #readDocument(Reader)
   */
  public Object read (Document document)
    throws IllegalArgumentException, ClassNotFoundException
  {    
    Element root = document.getRootElement (); 
    
    if (root == null)
      throw new IllegalArgumentException ("Document has no DBXML code");
    
    // skip into dbxml content if needed
    if (root.getName ().equals ("dbxml"))
      root = (Element)root.getChildren ().get (0);
    
    return read (root);
  }

  /**
   * Read an object from a JDOM root element.
   * 
   * @param root The root element to read from.
   * @return The decoded value.
   * @throws IllegalArgumentException if an error is found in the structure
   * of the document.
   * @throws ClassNotFoundException If a class referenced by the document
   * cannot be found.
   * 
   * @see #read(Document)
   * @see #read(Reader)
   */
  public Object read (Element root)
    throws IllegalArgumentException, ClassNotFoundException
  {    
    maybeCreateContext ();

    Object value = context.decode (root);
    
    if (!multiObject)
      finished ();
    
    return value;
  }
  
  public int getMajorVersion ()
  {
    return majorVersion;
  }

  public int getMinorVersion ()
  {
    return minorVersion;
  }

  /**
   * Register an XmlDecoder for this input object.
   * 
   * @param elementType The element type. The decoder will be used to decode
   * all element's that have this name.
   * @param decoder The decoder.
   * 
   * @see #registerGlobalXmlDecoder(String, XmlDecoder)
   */
  public void registerXmlDecoder (String elementType, XmlDecoder decoder)
  {
    if (xmlDecoders == globalXmlDecoders)
    {
      synchronized (globalXmlDecoders)
      {
        xmlDecoders = new LazyElementMap (globalXmlDecoders);
      }
    }
      
    xmlDecoders.put (elementType, decoder);
  }
  
  /**
   * Look up a decoder registered with {@link #registerXmlDecoder(String, XmlDecoder)}.
   */
  public XmlDecoder getXmlDecoder (String elementType)
  {
    if (xmlDecoders == null)
      return null;
    else
      return xmlDecoders.get (elementType);
  }
  
  /**
   * Register a TextDecoder for this input object.
   * 
   * @param valueType The value type. The decoder will be used to decode
   * all element's that are compatible with this type as defined by
   * {@link dsto.dfc.collections.TypeMap}.
   * @param decoderClassName The decoder class name. The decoder class will be 
   * instantiated when/if an object of the specified type is encountered.
   * 
   * @see #registerGlobalTextDecoder(String, String)
   */
  public void registerTextDecoder (String valueType, String decoderClassName)
  {
    if (textDecoders == globalTextDecoders)
    {
      synchronized (globalTextDecoders)
      {
        textDecoders = new LazyTypeMap (globalTextDecoders);
      }
    }
      
    textDecoders.put (valueType, decoderClassName);
  }
  
  /**
   * Register a TextDecoder for this input object.
   * 
   * @param valueType The value type. The decoder will be used to decode
   * all element's that are compatible with this type as defined by
   * {@link dsto.dfc.collections.TypeMap}.
   * @param decoder The decoder.
   * 
   * @see #registerGlobalTextDecoder(String, String)
   */
  public void registerTextDecoder (String valueType, TextDecoder decoder)
  {
    if (textDecoders == globalTextDecoders)
    {
      synchronized (globalTextDecoders)
      {
        textDecoders = new LazyTypeMap (globalTextDecoders);
      }
    }
      
    textDecoders.put (valueType, decoder);
  }
  
  /**
   * Register a global XmlDecoder. All XmlInput instances created after this
   * call will use the registered decoder by default.
   * 
   * @param elementType The element type. The decoder will be used to decode
   * all element's that have this name.
   * @param decoder The decoder.
   * 
   * @see #registerXmlDecoder(String, XmlDecoder)
   */
  public static void registerGlobalXmlDecoder (String elementType,
                                               XmlDecoder decoder)
  {
    synchronized (globalXmlDecoders)
    {
      globalXmlDecoders.put (elementType, decoder);
    }
  }
  
  /**
   * Register a TextDecoder for this input object. All XmlInput instances
   * created after this call will use the registered decoder by default.
   * 
   * @param valueType The value type. The decoder will be used to decode
   * all element's that are compatible with this type as defined by
   * {@link dsto.dfc.collections.TypeMap}.
   * @param decoderClassName The decoder class name. The decoder class will be 
   * instantiated when/if an object of the specified type is encountered.
   * 
   * @see #registerTextDecoder(String, TextDecoder)
   */
  public static void registerGlobalTextDecoder (String valueType,
                                                String decoderClassName)
  {
    synchronized (globalTextDecoders)
    {
      globalTextDecoders.put (valueType, decoderClassName);
    }
  }
  
  /**
   * Register a TextDecoder for this input object. All XmlInput instances
   * created after this call will use the registered decoder by default.
   * 
   * @param valueType The value type. The decoder will be used to decode
   * all element's that are compatible with this type as defined by
   * {@link dsto.dfc.collections.TypeMap}.
   * @param decoder The decoder.
   * 
   * @see #registerTextDecoder(String, String)
   */
  public static void registerGlobalTextDecoder (String valueType,
                                                TextDecoder decoder)
  {
    synchronized (globalTextDecoders)
    {      
      globalTextDecoders.put (valueType, decoder);
    }
  }
}
