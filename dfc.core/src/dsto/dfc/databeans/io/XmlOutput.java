package dsto.dfc.databeans.io;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;

import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format.TextMode;

/**
 * Manages encoding of data bean objects to XML.
 * 
 * Usage:
 * <pre>
 *   Writer writer = new FileWriter ("file.dbxml");
 *
 *   XmlOutput xmlOutput = new XmlOutput ();
 * 
 *   xmlOutput.write (writer, object);
 * 
 *   writer.close ();
 * </pre>
 * @author mpp
 * @version $Revision$
 */
public class XmlOutput
{
  private static final List globalXmlEncoders;
  private static final List globalTextEncoders;

  private XMLOutputter xmlOutput;
  /** List of XmlEncoder's, in ascending order of priority (later ones override earlier). */
  private List xmlEncoders;
  /** List of TextEncoder's, in ascending order of priority (later ones override earlier). */
  private List textEncoders;
  /** Maps type names (eg "java.lang.String" to a type alias (eg "string"). */
  private Map typeAliases;
  private int majorVersion;
  private int minorVersion;
  private Map properties;
  private XmlEncodeContext context;
  private boolean multiObject;
  
  static
  {
    globalXmlEncoders = new ArrayList ();
    globalXmlEncoders.add (new XmlDefaultEncoder ());
    globalXmlEncoders.add (new XmlDateCodec ());
    globalXmlEncoders.add (new XmlDataObjectCodec ());
    globalXmlEncoders.add (new XmlCollectionsCodec ());
    globalXmlEncoders.add (new XmlEnumerationCodec ());
    globalXmlEncoders.add (new XmlURICodec ());
    globalXmlEncoders.add (new XmlPropertyPathCodec ());
    globalXmlEncoders.add (new XmlSingletonCodec ());
    globalXmlEncoders.add (new XmlLinkCodec ());
    globalXmlEncoders.add (new XmlAliasCodec ());
    
    globalTextEncoders = new ArrayList ();
    globalTextEncoders.add (new TextDefaultCodec ());
  }
  
  /**
   * Constructor for XmlOutput. Use {@link #write(Writer, Object)} to
   * serialize objects.
   */
  public XmlOutput ()
  {
    this (getPrettyFormat ());
  }
  
  /**
   * Constructor for XmlOutput. Use {@link #write(Writer, Object)} to
   * serialize objects.
   * 
   * @param indent The characters to use for indenting.
   * @param addNewlines If true, align tags using newlines.
   * 
   * @see XmlOutput#XmlOutput(Format)
   */
  public XmlOutput (String indent, boolean addNewlines)
  {
    this (createFormat (indent, addNewlines));    
  }

  /**
   * Constructor for XmlOutput. Use {@link #write(Writer, Object)} to
   * serialize objects.
   * 
   * @param format The JDOM formatting options.
   */
  public XmlOutput (Format format)
  {
    this.xmlOutput = new XMLOutputter (format);
    this.xmlEncoders = globalXmlEncoders;
    this.textEncoders = globalTextEncoders;
    this.typeAliases = XmlIO.getTypeToAlias ();
    this.properties = Collections.EMPTY_MAP;
  }

  private static Format createFormat (String indent, boolean addNewlines)
  {
    Format format;
    
    if (addNewlines)
      format = getPrettyFormat ();
    else
      format = getCompactFormat ();
    
    format.setIndent (indent);
    
    return format;
  }

  /**
   * Define a custom property that will be accessible during this session
   * via {@link #getProperty(Object)} and
   * {@link XmlEncodeContext#getProperty(Object)}.
   */
  public void setProperty (Object name, Object value)
  {
    if (properties == Collections.EMPTY_MAP)
      properties = new HashMap ();

    properties.put (name, value);
  }
  
  /**
   * Get a custom property defined by {@link #setProperty(Object, Object)}.
   */
  public Object getProperty (Object name)
  {
    return properties.get (name);
  }
  
  /**
   * See {@link #setMultiObject(boolean)}.
   */
  public boolean isMultiObject ()
  {
    return multiObject;
  }

  /**
   * If set to true (default = false), then multiple elements may be
   * decoded by calling the decode () methods multiple times while
   * sharing the same context. Reusing the context means that if an
   * object read by one read () call references objects defined in
   * another call, the links will resolve. This intended for the case
   * where you might wish to represent an object tree in more than one
   * XML document.
   * <p>
   * 
   * IMPORTANT: If this option is enabled, it is up to the client to
   * call {@link #finished()} when the decode session is complete,
   * otherwise outstanding links may not be resolved.
   */
  public void setMultiObject (boolean newValue)
  {
    this.multiObject = newValue;
  }
  
  /**
   * Signal encoding is finished. Only needed if you enable multi
   * object encoding (@link #setMultiObjectEncode(boolean)}, otherwise
   * this is done automatically after each encode () call.
   */
  public void finished ()
  {
    if (context != null)
    {
      context.doPostEncode ();
      
      context = null;
    }
  }
  
  /**
   * Create and configure a context for decoding based on this output's
   * settings.
   */
  private void maybeCreateContext ()
  {
    if (context == null)
    {
      context = 
        new XmlEncodeContext (xmlEncoders, textEncoders, typeAliases);
      
      context.setProperties (properties);
    }
  }
  
  /**
   * Serialize an object to a stream with dbxml header.
   * 
   * @param stream The stream to write to.
   * @param object The object to serialize.
   * @return Element The JDOM element that was written to the stream.
   * @throws IOException if an error occurs writing to the stream.
   * 
   * @see #write(Writer,Object)
   */
  public Element write (OutputStream stream, Object object)
    throws IOException
  {
    OutputStreamWriter writer = new OutputStreamWriter (stream);
    
    Element root = write (writer, object);
    
    writer.flush ();

    return root;
  }
  
  /**
   * Serialize an object to a stream with dbxml header.
   * 
   * @param stream The stream to write to.
   * @param object The object to serialize.
   * @return Element The JDOM element that was written to the stream.
   * @throws IOException if an error occurs writing to the stream.
   * 
   * @see #write(Writer,Object,boolean)
   */
  public Element write (Writer stream, Object object)
    throws IOException
  {
    return write (stream, object, true);
  }
  
  /**
   * Serialize an object to a stream. This is a wrapper around
   * {@link #write(Object)} to encode the object to a JDOM document and
   * {@link #writeXml(Writer, Element)} to write the document to the stream. The
   * header (if requested) is created with {@link #createHeader()}.
   * 
   * @param stream The stream to write to.
   * @param object The object to serialize.
   * @param includeHeader True if the DBXML header should be included.
   * @return Element The JDOM element that was written to the stream (the XML
   * encoded version of object).
   * @throws IOException if an error occurs writing to the stream.
   * 
   * @see #write(Object)
   */
  public Element write (Writer stream, Object object, boolean includeHeader)
    throws IOException
  {
    Element objectElement = write (object);
    
    if (includeHeader)
    {
      Element root = createHeader ();
      
      root.addContent (objectElement);
       
      writeXml (stream, root);
    } else
    {
      writeXml (stream, objectElement);
    }
    
    return objectElement;
  }

  /**
   * Write a JDOM element tree to string,
   * 
   * @param root The document root.
   * @throws IOException if an error occurs writing element.
   * 
   * @see #writeXml(Writer, Element)
   */
  public String writeXml (Element root)
    throws IOException
  {
    StringWriter writer = new StringWriter ();
        
    writeXml (writer, root);
    
    return writer.toString ();
  }
  
  /**
   * Write a JDOM element tree to a stream.
   * 
   * @param writer The stream.
   * @param root The document root.
   * @throws IOException if an error occurs writing to the stream.
   * 
   * @see #write(Writer, Object)
   */
  public void writeXml (Writer writer, Element root)
    throws IOException
  {
    xmlOutput.output (root, writer);
  }
    
  /**
   * Encode an object to a JDOM element.
   * 
   * @param object The object.
   * 
   * @return Element the encoded document.
   */
  public Element write (Object object)
  {
    maybeCreateContext ();

    Element element = context.encode (object);
    
    if (!multiObject)
      finished ();
    
    return element;
  }
  
  /**
   * Shortcut to encode an object into an XML string,
   */
  public String writeToString (Object object)
    throws IOException
  {
    StringWriter writer = new StringWriter ();
    
    xmlOutput.output (write (object), writer);
    
    return writer.toString ();
  }

  /**
   * Method createHeaderElement.
   * @return Element
   */
  public Element createHeader ()
  {
    Element header = new Element ("dbxml");
    
    if (majorVersion > 0 || minorVersion > 0)
    {
      header.setAttribute ("version",
                           Integer.toString (majorVersion) + '.' +
                           Integer.toString (minorVersion));
    }
    
    return header;
  }

  /**
   * Register an XmlEncoder for this output object.
   * 
   * @param encoder The encoder.
   * 
   * @see #registerGlobalXmlEncoder(XmlEncoder)
   */
  public void registerXmlEncoder (XmlEncoder encoder)
  {
    if (xmlEncoders == globalXmlEncoders)
    {
      synchronized (globalXmlEncoders)
      {
        xmlEncoders = new ArrayList (globalXmlEncoders);
      }
    }
    
    xmlEncoders.add (encoder);
  }
  
  /**
   * Register a TextEncoder for this output object.
   * 
   * @param encoder The encoder.
   * 
   * @see #registerGlobalTextEncoder(TextEncoder)
   */
  public void registerTextEncoder (TextEncoder encoder)
  {
    if (textEncoders == globalTextEncoders)
    {
      synchronized (globalTextEncoders)
      {
        textEncoders = new ArrayList (globalTextEncoders);
      }
    }
      
    textEncoders.add (encoder);
  }
  
  /**
   * Register a global XmlEncoder. All XmlOutput instances created after this
   * call will use the registered encoder by default.
   * 
   * @param encoder The encoder.
   * 
   * @see #registerGlobalXmlEncoder(XmlEncoder)
   */
  public static void registerGlobalXmlEncoder (XmlEncoder encoder)
  {
    synchronized (globalXmlEncoders)
    {
      globalXmlEncoders.add (encoder);
    }
  }
  
  /**
   * Register a global TextEncoder. All XmlOutput instances created after this
   * call will use the registered encoder by default.
   * 
   * @param encoder The encoder.
   * 
   * @see #registerTextEncoder(TextEncoder)
   */
  public static void registerGlobalTextEncoder (TextEncoder encoder)
  {
    synchronized (globalTextEncoders)
    {
      globalTextEncoders.add (encoder);
    }
  }

  public int getMajorVersion ()
  {
    return majorVersion;
  }

  public int getMinorVersion ()
  {
    return minorVersion;
  }

  public void setMajorVersion (int majorVersion)
  {
    this.majorVersion = majorVersion;
  }

  public void setMinorVersion (int minorVersion)
  {
    this.minorVersion = minorVersion;
  }

  public void setVersion (int majorVersion, int minorVersion)
  {
    this.majorVersion = majorVersion;
    this.minorVersion = minorVersion;
  }
  
  private static Format getPrettyFormat ()
  {
    Format format = Format.getPrettyFormat ();

    // important, otherwise empty strings get blown away
    format.setTextMode (TextMode.PRESERVE);
    
    return format;
  }
  
  private static Format getCompactFormat ()
  {
    Format format = Format.getCompactFormat ();

    // important, otherwise empty strings get blown away
    format.setTextMode (TextMode.PRESERVE);
    
    return format;
  }
}
