package dsto.dfc.databeans.io;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.jdom.Element;

/**
 * General XML codec for AWT data structures. Currently handles RGB
 * colours (java.awt.Color in default colour space), java.awt.Point, 
 * java.awt.geom.Point2D family (Point2D.Float and Point2D.Double), 
 * java.awt.Rectangle, and the java.awt.geom.Rectangle2D family 
 * (Rectangle2D.Float and Rectangle2D.Double).
 * 
 * @author Matthew Phillips
 */
public class XmlAwtCodec implements XmlEncoder, XmlDecoder
{
  public static void register ()
  {
    XmlAwtCodec codec = new XmlAwtCodec ();
    
    XmlInput.registerGlobalXmlDecoder ("colour", codec);
    XmlInput.registerGlobalXmlDecoder ("point", codec);
    XmlInput.registerGlobalXmlDecoder ("rectangle", codec);
    XmlOutput.registerGlobalXmlEncoder (codec);
  }

  public boolean shouldPreserveIdentity (XmlEncodeContext context,
                                         Object value)
  {
    return false;
  }

  public boolean canEncode (XmlEncodeContext context, Object value)
  {
    return value instanceof Color || 
           value instanceof Point || 
           value instanceof Point2D || 
           value instanceof Rectangle2D;
  }
  
  public Element encode (XmlEncodeContext context, Object value)
  {
    Element element;

    if (value instanceof Color)
      element = encodeColor ((Color)value);
    else if (value instanceof Point)
      element = encodePoint ((Point)value);
    else if (value instanceof Point2D)
      element = encodePoint2d ((Point2D)value);
    else if (value instanceof Rectangle)
      element = encodeRectangle ((Rectangle)value);
    else if (value instanceof Rectangle2D)
      element = encodeRectangle2d ((Rectangle2D)value);
    else
      throw new IllegalArgumentException ("Cannot encode " + value);

    return element;
  }

  private Element encodeColor (Color colour)
  {
    Element element = new Element ("colour");
    
    element.setAttribute ("r", Integer.toString (colour.getRed ()));
    element.setAttribute ("g", Integer.toString (colour.getGreen ()));
    element.setAttribute ("b", Integer.toString (colour.getBlue ()));
    
    return element;
  }
  
  private Element encodePoint (Point point)
  {
    Element element = new Element ("point");
    
    element.setAttribute ("x", Integer.toString (point.x));
    element.setAttribute ("y", Integer.toString (point.y));
    
    return element;
  }
  
  private Element encodePoint2d (Point2D point2d)
  {
    Element element = new Element ("point");
    element.setAttribute ("precision", point2d instanceof Point2D.Double ? "double" : "single");
    
    element.setAttribute ("x", Double.toString (point2d.getX ()));
    element.setAttribute ("y", Double.toString (point2d.getY ()));
    
    return element;
  }

  private Element encodeRectangle (Rectangle rectangle)
  {
    Element element = new Element ("rectangle");

    element.setAttribute ("x", Integer.toString (rectangle.x));
    element.setAttribute ("y", Integer.toString (rectangle.y));
    element.setAttribute ("width", Integer.toString (rectangle.width));
    element.setAttribute ("height", Integer.toString (rectangle.height));

    return element;
  }
  
  private Element encodeRectangle2d (Rectangle2D rectangle2d)
  {
    Element element = new Element ("rectangle");

    element.setAttribute ("precision", rectangle2d instanceof Rectangle2D.Double ? "double" : "single");
    
    element.setAttribute ("x", Double.toString (rectangle2d.getX ()));
    element.setAttribute ("y", Double.toString (rectangle2d.getY ()));
    element.setAttribute ("width", Double.toString (rectangle2d.getWidth ()));
    element.setAttribute ("height", Double.toString (rectangle2d.getHeight ()));

    return element;
  }

  public Object decode (XmlDecodeContext context, Element element)
    throws IllegalArgumentException
  {
    if (element.getName ().equals ("colour"))
      return decodeColor (element);
    else if (element.getName ().equals ("point"))
      return decodePoint (element);
    else if (element.getName ().equals ("rectangle"))
      return decodeRectangle (element);
    else
      throw new IllegalArgumentException ("Cannot decode " + element.getName ());
  }

  private Object decodeRectangle (Element element)
  {
    String precision = element.getAttributeValue ("precision");
    
    if (precision == null)
    {
      return new Rectangle (getIntAttribute (element, "x"),
                            getIntAttribute (element, "y"),
                            getIntAttribute (element, "width"),
                            getIntAttribute (element, "height"));
    } else if (precision.equals ("single"))
    {
      return new Rectangle2D.Float (getFloatAttribute (element, "x"),
                                    getFloatAttribute (element, "y"),
                                    getFloatAttribute (element, "width"),
                                    getFloatAttribute (element, "height"));
    } else if (precision.equals ("double"))
    {
      return new Rectangle2D.Double (getDoubleAttribute (element, "x"),
                                     getDoubleAttribute (element, "y"),
                                     getDoubleAttribute (element, "width"),
                                     getDoubleAttribute (element, "height"));
    } else
    {
      throw new IllegalArgumentException
        ("Invalid precision for rectangle: \"" + precision + "\"");
    }
  }
  
  private Object decodeColor (Element element) throws NumberFormatException
  {
    return new Color (getIntAttribute (element, "r"),
                      getIntAttribute (element, "g"),
                      getIntAttribute (element, "b"));
  }
  
  private Object decodePoint (Element element)
  {
    String precision = element.getAttributeValue ("precision");
    
    if (precision == null)
    {
      return new Point (getIntAttribute (element, "x"),
                        getIntAttribute (element, "y"));
    } else if (precision.equals ("single"))
    {
      return new Point2D.Float (getFloatAttribute (element, "x"),
                                getFloatAttribute (element, "y"));
    } else if (precision.equals ("double"))
    {
      return new Point2D.Double (getDoubleAttribute (element, "x"),
                                 getDoubleAttribute (element, "y"));
    } else
    {
      throw new IllegalArgumentException
        ("Invalid precision for point: \"" + precision + "\"");
    }
  }
  
  private static int getIntAttribute (Element element, String attribute)
    throws NumberFormatException
  {
    return Integer.parseInt (element.getAttributeValue (attribute));
  }
  
  private static float getFloatAttribute (Element element, String attribute)
    throws NumberFormatException
  {
    return Float.parseFloat (element.getAttributeValue (attribute));
  }
  
  private static double getDoubleAttribute (Element element, String attribute)
    throws NumberFormatException
  {
    return Double.parseDouble (element.getAttributeValue (attribute));
  }
}
