package dsto.dfc.databeans.io;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jdom.Attribute;
import org.jdom.Element;

import dsto.dfc.text.StringUtility;

/**
 * General utility methods for use with serialized data object XML
 * documents.
 * 
 * @author phillipm
 */
public final class XmlUtil
{
  private XmlUtil ()
  {
    // zip
  }
  
  public static Element followNamePath (Element root, String path)
  {
    String [] names = StringUtility.split (path, "/");
    
    for (int i = 0; i < names.length; i++)
    {
    	root = findElement (root.getChildren (), "name", names [i]);
      
      if (root == null)
        return null;
    }
    
    return root;
  }
  
  public static Element findElement (Collection elements,
                                     String attr, String value)
  {
    for (Iterator i = elements.iterator (); i.hasNext (); )
    {
      Element element = (Element)i.next ();
      String elementName = element.getAttributeValue (attr);
      
      if (elementName != null && elementName.equals (value))
        return element;
    }
    
    return null;
  }
  
  /**
   * Finds the first {@link org.jdom.Element Elements} of the given type 
   * (<i>elementType</i>) in the list of Elements provided, and returns
   * it.
   * 
   * @param elements The list of Elements to check.
   * @param elementType The type of Elements to look for.
   * @return The first <i>elementType</i> typed Element found in <i>elements</i>.
   */
  public static Element findElement (List elements, String elementType)
  {
    List hits = findElements (elements, elementType);
    if (hits.size () > 0)
      return (Element) hits.get (0);
    else
      return null;
  }

  /**
   * Finds all the {@link org.jdom.Element Elements} of the given type 
   * (<i>elementType</i>) in the list of Elements provided, and returns
   * them in a list in order of discovery.
   * 
   * @param elements The list of Elements to check.
   * @param elementType The type of Elements to look for.
   * @return A list of <i>elementType</i> typed Elements.
   */
  public static List findElements (List elements, String elementType)
  {
    ArrayList hits = new ArrayList ();
    for (Iterator it = elements.iterator (); it.hasNext (); )
    {
      Element element = (Element) it.next ();
      
      if (element.getName ().equals (elementType))
        hits.add (element);
    }
    return hits;
  }
  

  public static int getAttribute (Element element, String name, int defValue)
  {
    String value = element.getAttributeValue (name);
    
    return value == null ? defValue : Integer.parseInt (value);
  }

  public static boolean getAttribute (Element element, String name, boolean defValue)
  {
    String value = element.getAttributeValue (name);
    
    return value == null ? defValue : Boolean.valueOf (value).booleanValue ();
  }

  /**
   * Get the value of a required boolean attribute.
   * 
   * @param element The element to read from.
   * @param name The attribute name.
   * @return The boolean attribute value.
   * 
   * @throws IllegalArgumentException if attribute is missing or not a boolean.
   */
  public static boolean getBooleanAttribute (Element element, String name)
    throws IllegalArgumentException
  {
    String value = getAttribute (element, name);
    
    if (value.equalsIgnoreCase ("true"))
      return true;
    else if (value.equalsIgnoreCase ("false"))
      return false;
    else
      throw new IllegalArgumentException ("Invalid boolean option: \"" + value + "\"");
  }
  
  /**
   * Get the value of a required attribute.
   * 
   * @param element The element to read from.
   * @param name The attribute name.
   * @return The attribute value.
   * 
   * @throws IllegalArgumentException if attribute is missing.
   */
  public static String getAttribute (Element element, String name)
    throws IllegalArgumentException
  {
    String value = element.getAttributeValue (name);
    
    if (value == null)
      throw new IllegalArgumentException ("Missing required attribute \"" + name + "\"");
    
    return value;
  }

  /**
   * Turn the attributes of an element into a java.util.Map.
   */
  public static Map getAttributesAsMap (Element element)
  {
    HashMap map = new HashMap ();
    
    for (Iterator i = element.getAttributes().iterator (); i.hasNext ();)
    {
      Attribute attr = (Attribute)i.next ();
      
      map.put (attr.getName (), attr.getValue ());
    }
    
    return map;
  }
}
