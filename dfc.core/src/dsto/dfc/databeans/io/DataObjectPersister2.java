package dsto.dfc.databeans.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import dsto.dfc.databeans.IDataObject;
import dsto.dfc.databeans.PropertyEvent;
import dsto.dfc.databeans.PropertyListener;
import dsto.dfc.logging.Log;
import dsto.dfc.util.Files;
import dsto.dfc.util.Scheduler;
import dsto.dfc.util.Scheduler.Task;

/**
 * A task that saves a data object at periodic intervals when it has
 * been modified. This mostly replaces the original
 * {@link DataObjectPersister}class, which is only really useful if
 * you want to use java.util.Timer.
 * <p>
 * 
 * NOTE: This task differs from the original
 * {@link DataObjectPersister} in that it uses the DFC
 * {@link dsto.dfc.util.Scheduler}'s ability to re-schedule and thus
 * avoids being in the task queue unless a change actually needs to be
 * saved. This allows the persister to be automatically GC'd with the
 * target object, or shut down cleanly when the scheduler is, removing
 * the need to explictly dispose () this object.
 */
public class DataObjectPersister2
  extends Scheduler.Task implements PropertyListener
{
  private static final long DEFAULT_SAVE_INTERVAL = 30 * 1000; 
  private static final long DEFAULT_MAX_SAVE_INTERVAL = 3 * 60 * 1000;
  private static final long CLEANUP_INTERVAL = 24 * 60 * 60 * 1000;
  
  public static final String BACKUP_REGEX = "-\\d{8}-\\d{9}.dbxml";
  private static final DateFormat DATE_FORMAT = new SimpleDateFormat ("yyyyMMdd-HHmmssSSS");
  private static final long DEFAULT_MAX_BACKUP_INTERVAL = 60 * 60 * 1000;
  
  protected static final int BACKUP_DAILY = 0;
  protected static final int BACKUP_WEEKLY = 1;
  protected static final int BACKUP_MONTHLY = 2;
  
  /**
   * Set to true to force the persister to save on exit (dispose ())
   * rather than rely on the modified flag. Default is false.
   */
  public boolean forceSaveOnExit;
  
  protected Scheduler theScheduler;
  protected IDataObject object;
  protected File file;
  protected XmlOutput xmlOutput;
  protected boolean modified;
  protected long firstUnsavedChange;
  protected long lastBackedUp;
  protected long saveInterval;
  protected long maxSaveInterval;
  protected long maxBackupInterval;
  protected boolean backup;
  
  public DataObjectPersister2 (Scheduler scheduler, IDataObject object, 
                               String filename)
  {
    this (scheduler, object, new File (filename));
  }
  
  public DataObjectPersister2 (Scheduler scheduler, 
                               IDataObject object, File file)
  {
    this (scheduler, object, file, true);
  }
  
  public DataObjectPersister2 (Scheduler scheduler, IDataObject object, 
                               File file, boolean backup)
  {
    this (scheduler, object, file, DEFAULT_SAVE_INTERVAL, 
          DEFAULT_MAX_SAVE_INTERVAL, DEFAULT_MAX_BACKUP_INTERVAL, backup);
  }
  
  public DataObjectPersister2 (Scheduler scheduler, IDataObject object, 
                               File file, long saveInterval, 
                               long maxSaveInterval, 
                               long maxBackupInterval, boolean backup)
  {
    this.theScheduler = scheduler;
    this.object = object;
    this.file = file.getAbsoluteFile ();
    this.xmlOutput = new XmlOutput ();
    this.saveInterval = saveInterval;
    this.maxSaveInterval = maxSaveInterval;
    this.maxBackupInterval = maxBackupInterval;
    this.forceSaveOnExit = false;
    
    this.backup = backup;
    this.lastBackedUp = getLastBackupDate ();
    
    if (backup)
    {
      // ensure there is at least one backup made, even if no property change
      scheduler.schedule (this, maxBackupInterval);
      
      // schedule cleanup task
      scheduler.schedule (new CleanupTask (), CLEANUP_INTERVAL);
    }
    
    object.addPropertyListener (this);
  }
  
  public synchronized void dispose ()
  {
    object.removePropertyListener (this);    

    theScheduler.unschedule (this);
    
    if (forceSaveOnExit)
      Log.trace ("Forcing save of " + file, this);
    
    if (modified || forceSaveOnExit)
      save ();
  }
  
  public XmlOutput getXmlOutput ()
  {
    return xmlOutput;
  }
  
  public int getMaxSaveInterval ()
  {
    return (int)(maxSaveInterval / 1000L);
  }
  
  /**
   * Set the maximum the persister will allow a modified object to remain
   * unsaved.
   * 
   * @param maxSaveInterval interval in seconds.
   */
  public void setMaxSaveInterval (int maxSaveInterval)
  {
    this.maxSaveInterval = maxSaveInterval * 1000L;
  }
  
  public int getSaveInterval ()
  {
    return (int)(saveInterval / 1000L);
  }
  
  /**
   * Set the interval that must pass without modifications before a modified
   * object is saved.
   * 
   * @param saveInterval The interval in seconds.
   */
  public void setSaveInterval (int saveInterval)
  {
    this.saveInterval = saveInterval * 1000L;
  }
  
  public void run ()
  {
    try
    {
      save ();
    } 
    catch (RuntimeException ex)
    {
      Log.alarm ("Error in persister task when saving to " + file.getName (), this, ex);
    }
  }
  
  public synchronized void setModified (boolean value)
  {
    if (modified != value)
    {
      modified = value;
      
      if (modified)
        firstUnsavedChange = System.currentTimeMillis ();
    }
  }
  
  public synchronized void propertyValueChanged (PropertyEvent e)
  {
    if (!e.transientProperty)
    {
      Log.trace ("Trigger save on property modify: " + e.getName () + 
                 ", old=" + e.oldValue + ", new=" + e.newValue, this);
      
      long now = System.currentTimeMillis ();
      
      setModified (true);
      
      // if longer than MAX_INTERVAL since last save, do it at next
      // opportunity, otherwise schedule save at SAVE_INTERVAL millis from now
      
      if (scheduled ())
      {
        // only allow reschedule forwards if it doesn't allow us to go
        // outside the max interval
        if (now - firstUnsavedChange < maxSaveInterval)
        {
          theScheduler.unschedule (this);
          theScheduler.schedule (this, saveInterval);
        }
      } else
      {
        theScheduler.schedule (this, saveInterval);
      }
    }
  }
  
  private void save ()
  {
    try
    {
      file.getParentFile ().mkdirs ();
      
      // write to temp file to avoid leaving corrupted file on error
      File tempFile = 
        File.createTempFile ("persister", null, file.getParentFile ());
      tempFile.deleteOnExit ();
      
      Writer writer = new BufferedWriter (new FileWriter (tempFile));

      try
      {        
        xmlOutput.write (writer, object, true);
      } finally
      {
        writer.close ();
      }
      
      if (file.exists ())
      {
        if (backup)
        {
          long waitToBackup = nextBackupAt ();
          if (waitToBackup == 0)
          {
            File backupFile = 
              new File (file.getParentFile (), getBackupFilename (file)); 
            if (!file.renameTo (backupFile))
            {
              Log.warn ("Failed to backup " + file.getName () + 
                        " to " + backupFile.getName (), this);
              file.delete ();
            }
            else
            {
              lastBackedUp = System.currentTimeMillis ();
              Log.diagnostic ("Backed up to " + backupFile.getName (), this);
            }
          }
          else
          {
            // schedule a backup to occur in the future
            theScheduler.unschedule (this);
            theScheduler.schedule (this, waitToBackup);
          }
        }
        else
        {
          file.delete ();
        }
      }
      
      if (!tempFile.renameTo (file))
      {
        Log.trace
          ("Failed to rename " + tempFile + 
           " to " + file + ": trying copy", this);
        
        Files.copyFile (tempFile, file);
      }
      
      Log.diagnostic ("Saved \"" + file + "\"", this);
    
      modified = false;
    } catch (IOException ex)
    {
      Log.alarm ("Failed to save \"" + file + "\"", this, ex);
    }
  }
  
  protected String getBackupFilename (File f)
  {
    String filename = f.getName ();
    String ext = Files.getExtension (filename);
    String filenameNoExt = Files.removeExtension (filename);
    String datestr = DATE_FORMAT.format (new Date ());
    return filenameNoExt + "-" +  datestr + "." + ext;
  }
  
  /**
   * Returns the date of the most recent backup
   * 
   * @return Returns the date of the most recent backup, returns 0 is
   *         no backups are found.
   */
  private long getLastBackupDate ()
  {
    File[] backups = getBackups ();
    return backups != null && backups.length > 0 ? 
             backups[0].lastModified () : 0;
  }
  
  /**
   * Returns an array of back up files for the persistance file sorted
   * by last modified in desceding order, ie the most recent backup
   * file is at index 0;
   */
  protected File[] getBackups ()
  {
    // filter on files which 'look like' backup files
    File[] files = file.getParentFile ().listFiles (new FileFilter ()
    {
      public boolean accept (File f)
      {
        return f.getName ().matches (getBackupFileRegex (file));
      }
    });
    
    if (files != null)
    {
      // sort files by their last modified date
      System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
      Arrays.sort (files, new Comparator<File> ()
      {
        public int compare (File f1, File f2)
        {
          return (int) (f2.lastModified () - f1.lastModified ());
        }
      });
    }
    
    return files;
  }
  
  protected String getBackupFileRegex (File f)
  {
    return Files.removeExtension (f.getName ()) + BACKUP_REGEX;
  }
  
  private long nextBackupAt ()
  {
    long now = System.currentTimeMillis ();
    long elapsed = (now - lastBackedUp);
    long duration = maxBackupInterval - elapsed;
    return Math.max (0, duration);
  }
  
  
  /**
   * Cleans up old backups according to the following rules
   * Preserve a trail of backups, daily backups for 1 week
   * weekly backups of 1 month and monthly backups for 1 year.
   * Delete all other backups.
   * 
   * @author KarunarD
   *
   */
  protected class CleanupTask extends Task
  {
    
    protected void keep (List<File> dontDelete, File f)
    {
      long oneDay = 24 * 60 * 60 * 1000;
      
      
      if (dontDelete.size () == 0)
      {
        // add most recent
        dontDelete.add (f);
        return;
      }

      int backupCandidate;
      File mostRecent = dontDelete.get (0);
      long elapsed = mostRecent.lastModified () - f.lastModified ();
      if (elapsed < 7 * oneDay)
        backupCandidate = BACKUP_DAILY;
      else if (elapsed < 31 * oneDay)
        backupCandidate = BACKUP_WEEKLY;
      else if (elapsed < 356 * oneDay)
        backupCandidate = BACKUP_MONTHLY;
      else 
        return; // greater than a year -> delete.
      
      File lastKept = dontDelete.get (dontDelete.size () - 1);
      long sinceLastKept = lastKept.lastModified () - f.lastModified ();
      
      switch (backupCandidate)
      {
        case BACKUP_DAILY:
          if (sinceLastKept > oneDay)
            dontDelete.add (f);
          
          break;
        case BACKUP_WEEKLY:
          if (sinceLastKept > 7 * oneDay)
            dontDelete.add (f);
           
          break;
        case BACKUP_MONTHLY:
          if (sinceLastKept > 31 * oneDay)
            dontDelete.add (f);
      }
    }
    
    public void run ()
    {
      // get list of backups
      List<File> dontDelete = new ArrayList<File> ();
      File[] files = getBackups ();
      
      if (files == null)
        return; // none found or IO error
      
      for (File bf : files)
        keep (dontDelete, bf);
      
      for (File bf : files)
        if (!dontDelete.contains (bf))
          bf.delete ();
    }
  }
}
