package dsto.dfc.databeans.io;

import dsto.dfc.collections.TypeMap;
import dsto.dfc.logging.Log;

/**
 * Extends TypeMap to add lazy creation of the value mapped to a class.
 * If a value retrieved by get () is a string, then it is treated as a class
 * name, instantiated using the default constructor, and substituted as the
 * mapped value. This can be used to defer loading of codecs that are specific
 * to a given environment eg AWT, SWT, etc.
 * 
 * @see dsto.dfc.databeans.io.LazyElementMap
 * 
 * @author mpp
 * @version $Revision$
 */
public final class LazyTypeMap extends TypeMap
{
  public LazyTypeMap ()
  {
    // zip
  }
  
  public LazyTypeMap (LazyTypeMap map)
  {
    super (map);
  }
  
  public Object get (Class type)
  {
    Object value = super.get (type);
    
    if (value instanceof String)
    {
      try
      {
        Class valueClass = Class.forName (value.toString ());
        
        value = valueClass.newInstance ();
        
        put (type, value);
      } catch (Exception ex)
      {
        // stop further errors
        remove (type);

        Log.internalError
          ("Exception while instantiating " + value.getClass (), this, ex);
          
        throw new RuntimeException
         ("Exception while instantiating " + value.getClass () + ": " + ex);
      }
    }
    
    return value;
  }
}
