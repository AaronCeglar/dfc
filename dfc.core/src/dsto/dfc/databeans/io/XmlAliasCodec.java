package dsto.dfc.databeans.io;

import org.jdom.Element;

import dsto.dfc.databeans.DataBeanWeakRef;
import dsto.dfc.databeans.DataObjectWeakRef;
import dsto.dfc.databeans.IDataBean;
import dsto.dfc.databeans.IDataObject;

/**
 * 
 * 
 * @author mpp
 * @version $Revision$
 */
public class XmlAliasCodec implements XmlEncoder, XmlDecoder
{
  public boolean shouldPreserveIdentity (XmlEncodeContext context, Object value)
  {
    return true;
  }
  
  public boolean canEncode (XmlEncodeContext context, Object value)
  {
    return value instanceof DataBeanWeakRef || value instanceof DataObjectWeakRef;
  }

  public Element encode (XmlEncodeContext context, Object value)
  {
    Element element = new Element ("alias");
    
    Element aliasElement = context.encode (value);
    
    element.addContent (aliasElement);
    
    return element;
  }

  public Object decode (XmlDecodeContext context, Element element)
    throws IllegalArgumentException, ClassNotFoundException
  {
    Element aliasElement = (Element)element.getChildren ().get (0);
    
    Object aliasValue = context.decode (aliasElement);
    
    if (aliasValue instanceof IDataBean)
      return new DataBeanWeakRef ((IDataBean)aliasValue);    
    else
      return new DataObjectWeakRef ((IDataObject)aliasValue);    
  }
}
