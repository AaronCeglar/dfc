package dsto.dfc.databeans.io;

import org.jdom.Element;

/**
 * An object that can decode an XML representation into a value.
 * 
 * @author mpp
 * @version $Revision$
 */
public interface XmlDecoder
{
  /**
   * Decode an XML representation into a value.
   * 
   * @param context The decode context.
   * @param element The element to decode.
   * @return The new object.
   * @throws IllegalArgumentException if an error is found in the structure
   * of the document.
   * @throws ClassNotFoundException if a referenced class cannot be found.
   */
  public Object decode (XmlDecodeContext context, Element element)
    throws IllegalArgumentException, ClassNotFoundException;
}
