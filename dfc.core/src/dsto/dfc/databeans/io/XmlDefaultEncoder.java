package dsto.dfc.databeans.io;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.jdom.Element;
import org.jdom.Verifier;

import dsto.dfc.databeans.IDataObject;

/**
 * Default XML fallback encoder. This encoder can encode any object
 * that (in order of preference):
 *
 * <ul>
 *   <li>Is an array.
 *   <li>Has a defined TextEncoder (see
 *      {@link XmlOutput#registerTextEncoder(TextEncoder)}
 *   <li>Is null</li>
 * </li>
 *
 * @see XmlDefaultDecoder
 *
 * @author mpp
 * @version $Revision$
 */
public class XmlDefaultEncoder implements XmlEncoder
{
  public boolean shouldPreserveIdentity (XmlEncodeContext context, Object value)
  {
    if (value instanceof String || value instanceof Number ||
        value instanceof Boolean || value instanceof Character || value == null)
    {
      return false;
    } else
    {
      return true;
    }
  }

  public boolean canEncode (XmlEncodeContext context, Object value)
  {
    return getElementName (context, value) != null;
  }

  private static String getElementName (XmlEncodeContext context, Object value)
  {
    if (value == null)
      return "null";
    else if (value instanceof List)
      return "list";
    else if (value instanceof Set)
      return "set";
    else if (value instanceof Map)
      return "map";
    else if (value instanceof Collection)
      return "collection";
    else if (value instanceof IDataObject)
      return "object";
    else if (value.getClass ().isArray ())
      return "array";
    else
    {
      String alias = context.getTypeAlias (value.getClass ());

      if (alias != null)
        return alias;
      else if (context.peekTextEncoder (value.getClass ()) != null)
        return "string";
      else
        return null;
    }
  }

  public Element encode (XmlEncodeContext context, Object value)
  {
    String name = getElementName (context, value);
    Element element = new Element (name);

    if (name.equals ("array"))
    {
      context.registerElement (element, value);

      encodeArray (context, element, value);
    } else if (value != null)
    {
      encodeText (context, element, value);
    }

    return element;
  }

  /**
   * Encode an array.
   */
  protected void encodeArray (XmlEncodeContext context, Element element,
                              Object array)
  {
    Class componentType = array.getClass ().getComponentType ();

    if (componentType != Object.class)
      element.setAttribute ("type", context.encodeType (array.getClass ()));

    // encode contents
    int len = Array.getLength (array);

    /*
     * If array a list of primitives, encode as comma-delimited string
     * value. Do not encode char's: they may contain characters that
     * aren't valid in attribute values.
     */
    if (len > 0 &&
        componentType.isPrimitive () &&
        !componentType.equals (Character.TYPE))
    {
      StringBuilder str = new StringBuilder (len * 4);

      for (int i = 0; i < len; i++)
      {
        if (i != 0)
          str.append (',');

        str.append (Array.get (array, i).toString ());
      }

      element.setAttribute ("value", str.toString ());
    } else
    {
      for (int i = 0; i < len; i++)
        element.addContent (context.encode (Array.get (array, i)));
    }
  }

  /**
   * Encode an object as text using a TextEncoder.
   */
  protected void encodeText (XmlEncodeContext context, Element element,
                             Object value)
  {
    TextEncoder encoder = context.getTextEncoder (value);

    if (encoder == null)
      throw new IllegalArgumentException
        ("No text encoder for " + value.getClass ());

    String textValue = encoder.encode (value);

    // if text is not XML-compatible, tunnel in binary form
    if (Verifier.checkCharacterData (textValue) != null)
    {
      element.setAttribute ("binary", "true");

      try
      {
        textValue = Base64.encodeBase64String (textValue.getBytes ("UTF-8"));
      } catch (UnsupportedEncodingException ex)
      {
        throw new Error ("UTF-8 not supported!");
      }
    }

    // If longer than 20 characters put text into element, otherwise use attribute
    if (textValue.length () >= 20)
      element.setText (textValue);
    else
      element.setAttribute ("value", textValue);

    // need to include type attribute if we are stringifying a non-string
    if (element.getName ().equals ("string") && !(value instanceof String))
      element.setAttribute ("type", context.encodeType (value.getClass ()));
  }
}

