package dsto.dfc.databeans.io;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Generic text encoder/decoder that uses toString () to encode and the
 * single String argument constructor to decode.
 * 
 * @author mpp
 * @version $Revision$
 */
public class TextDefaultCodec implements TextEncoder, TextDecoder
{
  private static final Class [] STRING_PARAM = new Class [] {String.class};
  
  public boolean canEncode (XmlEncodeContext context, Object value)
  {
    return value instanceof String ||
            value instanceof Number || 
            value instanceof Boolean ||
            value instanceof Character ||
            value.getClass ().isPrimitive ();
  }
  
  public String encode (Object value)
    throws IllegalArgumentException
  {
    return value.toString ();
  }
  
  public Object decode (Class type, String text)
    throws IllegalArgumentException
  {
    if (type.equals (String.class))
    {
      return text;
    } else if (type.equals (Boolean.class))
    {
      return text.equalsIgnoreCase ("true") ? Boolean.TRUE : Boolean.FALSE;
    } else if (type.equals (Character.class))
    {
      if (text.length () == 1)
        return new Character (text.charAt (0));
      else
        throw new IllegalArgumentException
          ("Character data must be one character long: \"" + text + "\"");
    } else
    {
      try
      {
        Constructor constructor = type.getConstructor (STRING_PARAM);
        
        return constructor.newInstance (new Object [] {text});
      } catch (InvocationTargetException ex)
      {      
        throw new IllegalArgumentException
          ("Error converting \"" + text + "\" to a " + type.getName () +
             ": " + ex.getTargetException ());
      } catch (Exception ex)
      {
        throw new IllegalArgumentException
          ("Don't know how to create a " + type.getName () + " from a string (" + ex + ")");
      }
    }
  }
}
