package dsto.dfc.databeans;

import java.util.Collections;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import dsto.dfc.util.Objects;

/**
 * Default implementation of IDataBean using Java collections classes for
 * storage.
 *
 * @author mpp
 * @version $Revision: 1.22 $
 */
public class DataBean extends AbstractDataBean
{
  protected HashMap values;
  protected HashSet transientProperties;
  protected Set alwaysTransient;
  protected TypeInfo typeInfo;

  public DataBean ()
  {
    this ((TypeInfo)null);
  }

  /**
   * Create a databean initialised from the contents of a java.util.Map. Keys
   * of the map must all be Strings, as required by IDataBean.
   */
  public DataBean (Map map)
  {
    this ();

    for (Iterator i = map.entrySet ().iterator (); i.hasNext ();)
    {
      Map.Entry entry = (Map.Entry)i.next ();

      setValue (entry.getKey (), entry.getValue ());
    }
  }

  /**
   * Create an instance copied from a java.util.Dictionary. Keys
   * of the dictionary must all be Strings, as required by IDataBean.
   */
  public DataBean (Dictionary dictionary)
  {
    this ();

    for (Enumeration i = dictionary.keys (); i.hasMoreElements ();)
    {
      Object key = i.nextElement ();

      setValue (key, dictionary.get (key));
    }
  }
  
  /**
   * Creates an instance copied from an IDataObject. Properties
   * of the IDataObject must all be Strings, as required by IDataBean.
   */
  public DataBean (IDataObject object)
  {
    this ();

    for (Iterator<Object> it = object.propertyIterator (); it.hasNext (); )
    {
      Object key = it.next ();
      
      setValue (key, object.getValue (key));
    }
  }
  
  /**
   * Create an instance with a specified type.
   */
  public DataBean (TypeInfo type)
  {
    this.typeInfo = type;
    this.values = new HashMap ();
    this.transientProperties = new HashSet ();
    this.alwaysTransient = Collections.EMPTY_SET;
  }

  @Override
  public TypeInfo getTypeInfo ()
  {
    return typeInfo;
  }

  public void setTypeInfo (TypeInfo typeInfo)
  {
    this.typeInfo = typeInfo;
  }

  /**
   * Flag a property as always transient regardless of any changes that would
   * normally make it non-transient.
   */
  public void setAlwaysTransient (String name)
  {
    if (alwaysTransient == Collections.EMPTY_SET)
      alwaysTransient = new HashSet ();

    alwaysTransient.add (name);
  }

  @Override
  public IDataObject shallowClone () throws UnsupportedOperationException
  {
    DataBean copy = (DataBean)super.shallowClone ();

    copy.values = new HashMap (values.size ());
    copy.transientProperties = new HashSet (transientProperties.size ());

    // always transient needs to be copied
    if (alwaysTransient != Collections.EMPTY_SET)
      copy.alwaysTransient = new HashSet (alwaysTransient);

    return copy;
  }

  /**
   * Gets the value associated with the key 'name'. If 'name' is not a String,
   * however, a ClassCastException is thrown, as IDataBean requires all keys
   * to be Strings.
   * 
   * @param name The name of the property in question (must be a String)
   * @return The value associated with 'name'.
   */
  public Object getValue (Object name)
  {
    checkProperty (name);

    return values.get (name);
  }

  @Override
  public void setValue (Object name, Object value, int mode)
  {
    checkProperty (name);
    
    Object oldValue = values.get (name);

    // nullop if value already set and no override
    if (oldValue != null && (mode & OVERRIDE) == 0)
      return;

    if (!Objects.objectsEqualEx (oldValue, value))
    {
      // handle always transient
      if (alwaysTransient.contains (name))
        mode |= TRANSIENT;

      if (value == null)
        values.remove (name);
      else
        values.put (name, value);

      unregisterValue (oldValue);
      registerValue (name, value);

      if (value == null || (mode & TRANSIENT) == 0)
        transientProperties.remove (name);
      else
        transientProperties.add (name);

      firePropertyChangedEvent (name, oldValue, value, (mode & TRANSIENT) != 0);
    }
  }

  public boolean isTransient (Object name)
  {
    return transientProperties.contains (name) || alwaysTransient.contains (name);
  }

  public Iterator<Object> propertyIterator ()
  {
    return values.keySet ().iterator ();
  }

  public String [] getPropertyNames ()
  {
    String [] names = new String [values.size ()];

    // todo opt this generates a LOT of unneeded keyset's: use an iterator
    values.keySet ().toArray (names);

    return names;
  }

  @Override
  public void childPropertyChanged (Object childProperty, PropertyEvent e)
  {
    // rewrite event as transient if an always transient property is changing
    if (!e.transientProperty && alwaysTransient.contains (childProperty))
      e = new PropertyEvent (e.getObject (), e.path, e.oldValue, e.newValue, true);

    if (!e.transientProperty)
      transientProperties.remove (childProperty);

    super.childPropertyChanged (childProperty, e);
  }

  /**
   * Checks that the property provided is a String and fails with a
   * ClassCastException if it isn't.
   * 
   * @param property The property, which should be a String
   * @throws ClassCastException if 'property' is not a String.
   */
  protected void checkProperty (Object property)
  {
    if (!(property instanceof String))
      throw new ClassCastException ("Databeans do not support non-String" +
      		                        " properties: " + property + " (" + 
                                    property.getClass () + ")");
  }
}
