package dsto.dfc.databeans;

import static dsto.dfc.databeans.IFilter.NULL_FILTER;
import static dsto.dfc.databeans.ISorter.NULL_SORTER;

import java.util.ArrayList;
import java.util.Iterator;

import dsto.dfc.collections.NumberIterator;
import dsto.dfc.util.Disposable;

/**
 * Creates a dynamically sortable, filterable list view of a data
 * object. The list view is itself a data object: The properties of
 * this object are integer indexes. The source object is usually a
 * collection type object such as a registry but can be any
 * IDataObject.
 *
 * @see DataObjectFilter
 *
 * @author Matthew Phillips
 */
public class DataObjectView
  extends AbstractDataObject implements Disposable, PropertyListener, Iterable
{
  private IDataObject source;
  private ISorter sorter;
  private IFilter filter;
  private ArrayList values;

  /**
   * Create a default sorter.
   *
   * @see ISorter#DEFAULT_SORTER
   */
  public static final <T> ISorter<T> defaultSorter ()
  {
    return ISorter.DEFAULT_SORTER;
  }

  public DataObjectView (IDataObject source)
  {
    this (source, NULL_FILTER, NULL_SORTER);
  }

  /**
   * Shortcut to create a new instance with a combined filter/sorter.
   *
   * @param source The source object to read from.
   * @param filterSorter Combined filter sorter.
   */
  public DataObjectView (IDataObject source, IFilterSorter filterSorter)
  {
    this (source, filterSorter, filterSorter);
  }

  public DataObjectView (IDataObject source, IFilter filter)
  {
    this (source, filter, NULL_SORTER);
  }

  public DataObjectView (IDataObject source, ISorter sorter)
  {
    this (source, NULL_FILTER, sorter);
  }

  /**
   * Create a new instance.
   *
   * @param source The source object to read from.
   * @param filter Any values of source that are included by the
   *          fliter appear in this view. May be null for no
   *          filtering.
   * @param sorter Values in the view are kept sorted using this. May
   *          be null for default sorting.
   */
  public DataObjectView (IDataObject source, IFilter filter, ISorter sorter)
  {
    this.source = source;
    this.sorter = sorter == null ? NULL_SORTER : sorter;
    this.filter = filter == null ? NULL_FILTER : filter;
    this.values = new ArrayList ();

    for (Iterator i = source.propertyIterator (); i.hasNext ();)
    {
      Object property = i.next ();
      Object value = source.getValue (property);

      maybeInsert (value);
    }

    source.addPropertyListener (this);
  }

  public void dispose ()
  {
    source.removePropertyListener (this);

    for (Iterator i = values.iterator (); i.hasNext ();)
      unregisterValue (i.next ());
  }

  /**
   * Set the view filter.
   */
  public void setFilter (IFilter newFilter)
  {
    if (!newFilter.equals (filter))
    {
      this.filter = newFilter;

      refilter ();
    }
  }

  public IFilter getFilter ()
  {
    return filter;
  }

  /**
   * Force the view to refilter its contents.
   */
  public void refilter ()
  {
    for (Iterator i = source.propertyIterator (); i.hasNext ();)
    {
      Object property = i.next ();
      Object value = source.getValue (property);

      if (filter.include (value))
      {
        if (!contains (value))
          insert (value);
      } else
      {
        delete (value);
      }
    }
  }

  public void propertyValueChanged (PropertyEvent e)
  {
    if (e.path.length () == 1)
    {
      if (e.oldValue != null)
        delete (e.oldValue);

      if (e.newValue != null)
        maybeInsert (e.newValue);
    } else
    {
      maybeInsert (source.getValue (e.path.first ()));
    }
  }

  private void maybeInsert (Object value)
  {
    if (!contains (value) && filter.include (value))
      insert (value);
  }

  private boolean contains (Object value)
  {
    return values.contains (value);
  }

  private void insert (Object newValue)
  {
    add (sortedIndexOf (newValue), newValue);

    // NOTE: using null property since indices change
    registerValue (null, newValue);
  }

  private void delete (Object oldValue)
  {
    int index = values.indexOf (oldValue);

    if (index != -1)
      delete (index);
  }

  private void delete (int index)
  {
    unregisterValue (remove (index));
  }

  private int sortedIndexOf (Object newValue)
  {
    for (int i = 0; i < values.size (); i++)
    {
      if (sorter.compare (newValue, values.get (i)) <= 0)
        return i;
    }

    return values.size ();
  }

  @Override
  public void childPropertyChanged (Object notUsed, PropertyEvent e)
  {
    int index = values.indexOf (e.getSource ());
    
    if (index > -1)
    {
      super.childPropertyChanged (new Integer (index), e);
      
      Object value = values.get (index);
      
      if (!filter.include (value))
      {
        delete (index);
      } else if (sorter.affectsOrder (e.path))
      {
        remove (values.indexOf (value));
        add (sortedIndexOf (value), value);
      }
    }
  }

  private Integer add (int index, Object newValue)
  {
    Integer property = new Integer (index);
    values.add (index, newValue);

    firePropertyChangedEvent (property, null, newValue, false);

    return property;
  }

  private Object remove (int index)
  {
    Object oldValue = values.remove (index);

    firePropertyChangedEvent (new Integer (index), oldValue, null, false);

    return oldValue;
  }

  @Override
  public TypeInfo getTypeInfo ()
  {
    return null;
  }

  @Override
  public void setValue (Object name, Object value, int mode)
  {
    throw new UnsupportedOperationException ("Cannot modify view");
  }

  public Object getValue (Object name)
  {
    if (name instanceof Integer)
    {
      return getValue (((Integer)name).intValue ());
    } else
    {
      return null;
    }
  }

  public Object getValue (int index)
  {
    if (index >= 0 && index < values.size ())
      return values.get (index);
    else
      return null;
  }

  public boolean isTransient (Object name)
  {
    return false;
  }

  public Iterator propertyIterator ()
  {
    return new NumberIterator (0, values.size ());
  }

  public int size ()
  {
    return values.size ();
  }

  public Iterator iterator ()
  {
    return values.iterator ();
  }

  /**
   * The sorted index of value in the view (-1 if value is not shown
   * in the view).
   */
  public int indexOf (Object value)
  {
    return values.indexOf (value);
  }
}
