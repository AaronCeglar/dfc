package dsto.dfc.databeans;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Iterator;

import dsto.dfc.collections.IteratorEnumeration;


/**
 * Adapts a data object as a java.util.Dictionary.
 * 
 * @author Matthew Phillips
 */
public class DataObjectDictionary extends Dictionary
{
  private IDataObject object;

  public DataObjectDictionary (IDataObject object)
  {
    this.object = object;
  }

  /**
   * NOTE: this is relatively inefficient since it iterates through
   * the property set on each call.
   */
  public int size ()
  {
    int size = 0;
    
    for (Iterator i = object.propertyIterator (); i.hasNext (); i.next ())
      size++;
    
    return size;
  }

  public boolean isEmpty ()
  {
    return size () == 0;
  }

  public Enumeration elements ()
  {
    return new IteratorEnumeration (new PropertyValueIterator (object));
  }

  public Enumeration keys ()
  {
    return new IteratorEnumeration (object.propertyIterator ());
  }

  public Object get (Object key)
  {
    return object.getValue (key);
  }

  public Object remove (Object key)
  {
    return put (key, null);
  }

  public Object put (Object key, Object value)
  {
    Object oldValue = object.getValue (key); 
      
    object.setValue (key, value);
    
    return oldValue;
  }
}
