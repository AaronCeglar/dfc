package dsto.dfc.databeans;

import dsto.dfc.util.WeakEventListener;

/**
 * An weak reference alias for an IDataObject. The wrapped object is
 * listened to using a weak listener, allowing the alias to be GC'd
 * when no other references to it are held.
 * 
 * @author mpp
 * @version $Revision$
 */
public class DataObjectWeakRef
  extends DataObjectLink
  implements IDataObject, IDataObjectLink, PropertyListener
{
  private WeakEventListener listener;
  
  public DataObjectWeakRef (IDataObject object)
  {
    this (object, true);
  }
  
  public DataObjectWeakRef (IDataObject object, boolean isSoftLink)
  {
    super (object, isSoftLink);
  }
    
  protected void initListener ()
  {
    listener =
      WeakEventListener.createListener (PropertyListener.class, object, this);
  }
  
  
  protected void disposeListener ()
  {
    listener.dispose ();
    listener = null;
  }

  public IDataObject getLinkTarget ()
  {
    return object;
  }  
}
