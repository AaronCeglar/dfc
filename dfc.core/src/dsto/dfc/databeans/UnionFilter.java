package dsto.dfc.databeans;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * An IFilter that is a union of several sub filters. Includes items
 * if any sub filter matches.
 * 
 * @author Matthew Phillips
 */
public class UnionFilter<T> implements IFilter<T>, Iterable<IFilter<T>>
{
  private Set<IFilter<T>> filters;

  public UnionFilter ()
  {
    this.filters = new HashSet<IFilter<T>> ();
  }
  
  public boolean include (T value)
  {
    for (IFilter<T> filter : filters)
    {
      if (filter.include (value))
        return true;
    }
    
    return false;
  }

  public void add (IFilter<T> filter)
  {
    filters.add (filter);
  }
  
  public void remove (IFilter<T> filter)
  {
    filters.remove (filter);
  }

  public void clear ()
  {
    filters.clear ();
  }

  public boolean contains (IFilter<T> filter)
  {
    return filters.contains (filter);
  }
  
  public Iterator<IFilter<T>> iterator ()
  {
    return filters.iterator ();
  }
}
