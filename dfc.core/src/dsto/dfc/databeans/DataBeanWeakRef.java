package dsto.dfc.databeans;

import dsto.dfc.util.WeakEventListener;

/**
 * A weak reference alias for an IDataBean. The wrapped databean is
 * listened to using a weak listener, allowing the alias to be GC'd
 * when no other references to it are held.
 * 
 * @author mpp
 * @version $Revision$
 */
public class DataBeanWeakRef
  extends DataBeanLink implements IDataBean, PropertyListener
{
  private WeakEventListener listener;

  public DataBeanWeakRef (IDataBean bean)
  {
    this (bean, true);
  }
  
  public DataBeanWeakRef (IDataBean bean, boolean isSoftLink)
  {
    super (bean, isSoftLink);
  }

  protected void initListener ()
  {
    listener =
      WeakEventListener.createListener (PropertyListener.class, bean, this);
  }
  
  protected void disposeListener ()
  {
    listener.dispose ();
    
    listener = null;
  }
}
