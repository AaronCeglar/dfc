package dsto.dfc.databeans;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import dsto.dfc.collections.BasicMonitoredSet;
import dsto.dfc.collections.CollectionEvent;
import dsto.dfc.collections.CollectionListener;
import dsto.dfc.util.EventListenerList;

/**
 * An extension of the DFC monitored set that supports access via the
 * IDataObject interface. Any IDataObject's in the set are automatically
 * monitored and the appropriate property events fired. The property values
 * of the set are the values themselves.
 * 
 * @author mpp
 * @version $Revision$
 */
public class SetDataObject
  extends BasicMonitoredSet
  implements IDataObject, IDataObjectChildListener, CollectionListener
{
  protected HashSet transientProperties;
  private transient boolean changingTransient;
  private EventListenerList listeners;
  
  public SetDataObject ()
  {
    this (new HashSet ());
  }
  
  public SetDataObject (Set set)
  {
    super (set);
    
    this.listeners = new EventListenerList ();
    
    addCollectionListener (this);
    
    if (!set.isEmpty ())
    {
      for (Iterator i = set.iterator (); i.hasNext (); )
        registerValue (i.next ());
    }
  }

  public TypeInfo getTypeInfo ()
  {
    return null;
  }
  
  public Collection getPropertyListeners ()
  {
    return listeners.getListeners ();
  }

  protected void registerValue (Object value)
  {
    DataObjects.registerChildValue (this, null, value);
  }
  
  protected void unregisterValue (Object value)
  {
    DataObjects.unregisterChildValue (this, value);
  }
  
  // IDataObject interface
  
  public IDataObject shallowClone () throws UnsupportedOperationException
  {
    try
    {
      SetDataObject copy = (SetDataObject)emptyClone ();
      
      if (transientProperties != null)
        copy.transientProperties = new HashSet (transientProperties);
      
      copy.listeners = new EventListenerList ();
      copy.addCollectionListener (copy);
      
      return copy;    
    } catch (CloneNotSupportedException ex)
    {
      throw new UnsupportedOperationException ("Cannot clone: " + ex);
    }
  }

  /**
   * This is a little weird for sets, where nominally name and value must be
   * the same. However, operations like deep cloning assume that they can 
   * do "setValue (oldProperty, newValue)" since properties are immutable,
   * so we allow this even though technically we shouldn't. Note that
   * setValue (X, null) removes X from the set as would likely be
   * expected.
   */
  public void setValue (Object name, Object value)
  {
    setValue (name, value, PERSISTENT_OVERRIDE);
  }
  
  public void setValue (Object name, Object value, int mode)
  {
    // checks for illegal setValue () calls (disabled due to issue above)
    //    if (value != null && !Objects.objectsEqual (name, value))
    //      throw new UnsupportedOperationException
    //        ("Set requires name and value to be the same: " + name + " != " + value);
   
    if ((mode & TRANSIENT) != 0)
    {
      if (transientProperties == null)
        transientProperties = new HashSet ();

      if (value != null)
        transientProperties.add (name);
      
      changingTransient = true;
    }
    
    try
    {
      if (value == null)
        remove (name);
      else if ((mode & OVERRIDE) != 0 || !contains (name))
        add (value);
    } finally
    {
      changingTransient = false;
    }
  }

  public Object getValue (Object name)
  {
    if (contains (name))
      return name;
    else
      return null;
  }

  public boolean isTransient (Object name)
  {
    return transientProperties != null && transientProperties.contains (name);
  }
  
  public Iterator propertyIterator ()
  {
    return iterator ();
  }
  
  public void addPropertyListener (PropertyListener l)
  {
    listeners.addListener (l);
  }

  public void removePropertyListener (PropertyListener l)
  {
    listeners.removeListener (l);
  }

  // CollectionListener interface
  
  public void elementsAdded (CollectionEvent e)
  {
    for (Iterator i = e.getElements ().iterator (); i.hasNext (); )
    {
      Object element = i.next ();
      
      registerValue (element);
      
      if (!changingTransient && transientProperties != null)
        transientProperties.remove (element);
      
      firePropertyChangedEvent (element, null, element, isTransient (element));
    }
  }

  public void elementsRemoved (CollectionEvent e)
  {
    for (Iterator i = e.getElements ().iterator (); i.hasNext (); )
    {
      Object element = i.next ();
      
      unregisterValue (element);

      boolean wasTransient = false;
      
      if (transientProperties != null)
        wasTransient = transientProperties.remove (element);
      
      firePropertyChangedEvent (element, element, null, wasTransient);
    }
  }
  
  // PropertyListener interface
  
  public void childPropertyChanged (Object childProperty, PropertyEvent e)
  {
    if (transientProperties != null && !e.transientProperty)
      transientProperties.remove (e.getSource ());
    
    firePropertyChangedEvent (e.getSource (), e.path,
                              e.oldValue, e.newValue,
                              e.transientProperty);
  }

  protected void firePropertyChangedEvent (Object source,
                                            Object oldValue,
                                            Object newValue,
                                            boolean transientProperty)
  {
    if (listeners.hasListeners ())
    {
      PropertyEvent e =
        new PropertyEvent (this, new PropertyPath (source),
                           oldValue, newValue, transientProperty);
      
      listeners.fireEvent (PropertyListener.class, "propertyValueChanged", e);
    }
  }

  protected void firePropertyChangedEvent (Object property,
                                            PropertyPath basePath,
                                            Object oldValue,
                                            Object newValue,
                                            boolean transientProperty)
  { 
    if (listeners.hasListeners ())
    {
      PropertyEvent e = new PropertyEvent (this, basePath.prepend (property),
                                           oldValue, newValue,
                                           transientProperty);
      
      listeners.fireEvent (PropertyListener.class, "propertyValueChanged", e);
    }
  }
}
