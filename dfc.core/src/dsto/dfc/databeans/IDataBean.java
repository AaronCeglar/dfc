package dsto.dfc.databeans;

/**
 * A data bean is an extension of the IDataObject interface to provide a
 * dynamic JavaBean-type object. Like a JavaBean, a data bean has only string
 * property names. Unlike a JavaBean, a data bean's properties are not fixed:
 * properties can be created and deleted on the fly. Property values can also
 * be dynamically set to be transient/non-transient.<p>
 * 
 * Data beans also add the guarantee of being able to conveniently set/get
 * primitive values via specialised methods (this is also provided by
 * AbstractDataObject but is not part of the IDataObject interface to simplify
 * implementations that aren't based on this class).<p>
 * 
 * Standard JavaBeans can be adapted as data beans using either
 * {@link dsto.dfc.databeans.JavaDataBean} or
 * {@link dsto.dfc.databeans.SimpleJavaDataBean}.<p>
 * 
 * @see dsto.dfc.databeans.DataBean
 * @see dsto.dfc.databeans.DataObjects
 * @see dsto.dfc.databeans.PropertyPath
 * 
 * @author mpp
 * @version $Revision$
 */
public interface IDataBean extends IDataObject
{
  /**
   * Databean's only have strings as their property names. This is a shortcut
   * to get the property names for which getValue () would return a non-null
   * value.
   */
  public String [] getPropertyNames ();

  // shortcuts for reading/writing primitive values
  
  public String getStringValue (String name);
  
  public void setValue (String name, boolean value);
  public void setValue (String name, boolean value, int mode);
  public boolean getBooleanValue (String name);
  
  public void setValue (String name, int value);
  public void setValue (String name, int value, int mode);
  public int getIntValue (String name);
  
  public void setValue (String name, long value);
  public void setValue (String name, long value, int mode);
  public long getLongValue (String name);
  
  public void setValue (String name, float value);
  public void setValue (String name, float value, int mode);
  public float getFloatValue (String name);
  
  public void setValue (String name, double value);
  public void setValue (String name, double value, int mode);
  public double getDoubleValue (String name);
  
  public void setValue (String name, char value);
  public void setValue (String name, char value, int mode);
  public char getCharValue (String name);
  
  public IDataObject getObjectValue (String name);
  public IDataBean getBeanValue (String name);

}
