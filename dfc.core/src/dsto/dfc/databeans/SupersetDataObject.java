package dsto.dfc.databeans;

import java.util.HashSet;
import java.util.Iterator;

/**
 * An object that merges the properties of a set of sub-object's into
 * a single object. As properties change on sub-object's the set
 * updates to match.
 * 
 * @author Matthew Phillips
 */
public class SupersetDataObject extends DataObject
{
  private HashSet subobjects;
  private Listener propertyListener;
  
  public SupersetDataObject ()
  {
    this.subobjects = new HashSet ();
    this.propertyListener = new Listener ();
  }
  
  /**
   * This object listens to all sub-object's, so make sure to call
   * this if this object has a more limited scope.
   */
  public void dispose ()
  {
    if (subobjects != null)
    {
      for (Iterator i = subobjects.iterator (); i.hasNext (); )
        ((IDataObject)i.next ()).removePropertyListener (propertyListener);
      
      subobjects = null;
    }
  }
  
  /**
   * Add a sub object to the set. Properties of the sub object will
   * appear in the super set at this point, except for proeperties
   * that are already defined.
   * 
   * @param object The object to add. May be added more than once
   *          (with no effect on subsequent adds).
   */
  public void add (IDataObject object)
  {
    if (subobjects.add (object))
    {
      for (Iterator i = object.propertyIterator (); i.hasNext (); )
      {
        Object property = i.next ();
        
        add (property, object.getValue (property));
      }
      
      object.addPropertyListener (propertyListener);
    }      
  }
  
  /**
   * Remove an object from the superset. All properties of the sub
   * object disappear from the superset at this point, except for any
   * overlapping properties in other sub objects that may have been
   * hidden by this one.
   * 
   * NOTE: this may be significantly slower than add ()
   */
  public void remove (IDataObject object)
  {
    if (!subobjects.remove (object))
      return;

    object.removePropertyListener (propertyListener);
        
    for (Iterator i = object.propertyIterator (); i.hasNext (); )
    {
      Object property = i.next ();
      
      remove (property, object.getValue (property));
    }
  }

  protected void add (Object property, Object value)
  {
    if (getValue (property) == null)
      setValue (property, value);
  }

  protected void remove (Object property, Object value)
  {
    Object newValue = null;
    
    for (Iterator i = subobjects.iterator (); i.hasNext () && newValue == null; )
    {
      IDataObject subObject = (IDataObject)i.next ();
      
      newValue = subObject.getValue (property);
    }
    
    setValue (property, newValue);
  }
  
  class Listener implements PropertyListener
  {
    public void propertyValueChanged (PropertyEvent e)
    {
      if (e.path.length () != 1)
       return;
      
      if (e.oldValue != null)
        remove (e.path.first (), e.oldValue);
      
      if (e.newValue != null)
        add (e.path.first (), e.newValue);
    }
  }
}
