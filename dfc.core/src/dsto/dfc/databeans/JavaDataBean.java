package dsto.dfc.databeans;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import dsto.dfc.logging.Log;
import dsto.dfc.util.Beans;
import dsto.dfc.util.Objects;

/**
 * Wraps a JavaBean as an IDataBean. The initial values of the JavaBean are
 * considered to be the (transient) defaults until they are observed to change.
 * New properties that do not overlap the set provided by the bean may be added
 * as needed.<p>
 * 
 * This class may either wrap an existing bean, or be subclassed. If this class
 * is subclassed, it is vital that the subclass call
 * {@link #initDataBean(Class)} as the last thing in its constructor.
 * 
 * @author mpp
 * @version $Revision$
 */
public class JavaDataBean extends DataBean implements PropertyChangeListener
{
  private Object bean;
  private PropertyDescriptor [] properties;
  private boolean listening;
  private boolean transientPropertyChanging;
  
  /**
   * Create a default instance. Use setBean () to set the bean before use.
   */
  public JavaDataBean ()
    throws IllegalArgumentException
  {
    this (null);
  }
  
  /**
   * Constructor for JavaDataBean.
   */
  public JavaDataBean (Object bean)
    throws IllegalArgumentException
  {
    super ();
    
    if (bean != null)
      setBean (bean);
  }
  
  /**
   * Subclasses must call this from their constructor after they have
   * initialised their properties to finish initialisation of databean
   * state.
   * 
   * @param beanClass The class of the bean: must be the result of
   *          using JavaBean.class <em>not</em> getClass ().
   */
  protected final void initDataBean (Class beanClass)
  {
    if (getClass ().equals (beanClass))
      setBean (this);
  }
  
  public IDataObject shallowClone () throws UnsupportedOperationException
  {
    try
    {
      JavaDataBean copy = (JavaDataBean)super.shallowClone ();
      
      if (bean != this)
        copy.bean = Objects.cloneObject (bean);
      else
        copy.bean = copy;
      
      copy.listening =
        Beans.addListener (PropertyChangeListener.class, copy.bean, copy);
      
      return copy;
    } catch (CloneNotSupportedException ex)
    {
      throw new UnsupportedOperationException ("Clone not supported: " + ex);
    }
  }
  
  private void initProperties ()
    throws IllegalArgumentException
  {
    try
    {
      BeanInfo info = Introspector.getBeanInfo (bean.getClass ());
      PropertyDescriptor [] allProperties = info.getPropertyDescriptors (); 
      ArrayList mutableProperties = new ArrayList (allProperties.length);
      
      for (int i = 0; i < allProperties.length; i++)
      {
        PropertyDescriptor property = allProperties [i];
        
        if (property.getWriteMethod () != null &&
            property.getReadMethod () != null)
        {
          mutableProperties.add (property);
          
          Object value = readProperty (property);
          
          transientProperties.add (property.getName ());
          
          values.put (property.getName (), value);
          
          registerValue (property.getName (), value);
        }
      }
      
      this.properties = new PropertyDescriptor [mutableProperties.size ()];
      
      mutableProperties.toArray (this.properties);
    } catch (Exception ex)
    {
      Log.diagnostic ("Error reading property", this, ex);
      
      throw new IllegalArgumentException ("Error reading property: " + ex);
    }
  }
  
  public Object getBean ()
  {
    return bean;
  }
  
  public void setBean (Object newBean)
    throws IllegalArgumentException
  {   
    if (bean != null)
      throw new IllegalArgumentException ("Can only set bean once");
      
    this.bean = newBean;
    
    this.listening =
      Beans.addListener (PropertyChangeListener.class, bean, this);
    
    initProperties ();
  }

  public Object getValue (Object name)
  {
    PropertyDescriptor property =
      Beans.findProperty (properties, name.toString ());
      
    if (property != null)
    {
      return readProperty (property);
    } else
    {
      return super.getValue (name);
    }
  }

  public void setValue (Object name, Object value)
    throws IllegalArgumentException
  { 
    PropertyDescriptor property =
      Beans.findProperty (properties, name.toString ());
    
    if (property == null)
    {
      // pass non-bean property to DataBean
      super.setValue (name, value);
    } else
    {
      Object oldValue = readProperty (property);
      
      writeProperty (property, value);
      
      unregisterValue (oldValue);
      registerValue (name, value);
      
      values.put (name, value);
        
      if (!listening)
      {
        transientProperties.remove (property.getName ());
        
        firePropertyChangedEvent (property.getName (), oldValue, value, false);
      }
    }
  }
 
  public void setValue (String name, Object value, int mode)
  {
    PropertyDescriptor property = Beans.findProperty (properties, name);
      
    if (property == null)
    {
      super.setValue (name, value, mode);
    } else
    {     
      Object oldValue = readProperty (property);
      
      if (oldValue != null && (mode & OVERRIDE) == 0)
        return;
  
      if ((mode & TRANSIENT) != 0)
        transientPropertyChanging = true;
      writeProperty (property, value);
      transientPropertyChanging = false;
      
      values.put (name, value);
      
      unregisterValue (oldValue);
      registerValue (name, value);

      if ((mode & TRANSIENT) != 0)
        transientProperties.add (name);

      if (!listening)
        firePropertyChangedEvent (property.getName (), oldValue, value,
                                  (mode & TRANSIENT) != 0);
    }
  }
  
  protected Object readProperty (PropertyDescriptor property)
    throws IllegalArgumentException
  {
    if (property.getReadMethod () == null)
      throw new IllegalArgumentException
        ("Property " + property.getName () + " is not readable");
      
    try
    {
      return property.getReadMethod ().invoke (bean, (Object [])null);
    } catch (InvocationTargetException ex)
    {
      Log.internalError ("Exception while setting property value", this, ex);
    
      throw new IllegalArgumentException
        ("Error while reading property \"" + property.getName () + "\": " +
         ex.getTargetException ());
    } catch (Exception ex)
    {
      throw new IllegalArgumentException
        ("Error while reading property \"" + property.getName () + "\": " + ex);
    }
  }
  
  protected void writeProperty (PropertyDescriptor property, Object value)
    throws IllegalArgumentException
  {
    if (property.getWriteMethod () == null)
      throw new IllegalArgumentException
        ("Property " + property.getName () + " is not writable");

    try
    {     
      property.getWriteMethod ().invoke (bean, new Object [] {value});
    } catch (InvocationTargetException ex)
    {
      Log.internalError ("Exception while setting property value", this, ex);

      throw new IllegalArgumentException
        ("Error while writing property \"" + property.getName () + "\": " +
         ex.getTargetException ());
    } catch (Exception ex)
    {
      throw new IllegalArgumentException
        ("Error while writing property \"" + property.getName () + "\": " + ex);
    }
  }
  
  // PropertyChangeListener interface
  
  public void propertyChange (PropertyChangeEvent e)
  {
    if (!transientPropertyChanging)
      transientProperties.remove (e.getPropertyName ());

    values.put (e.getPropertyName (), e.getNewValue ());
    
    firePropertyChangedEvent (e.getPropertyName (),
                              e.getOldValue (), e.getNewValue (),
                              transientPropertyChanging);
  }
}
