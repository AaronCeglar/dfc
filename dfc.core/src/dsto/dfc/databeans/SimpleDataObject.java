package dsto.dfc.databeans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import dsto.dfc.logging.Log;

/**
 * Base class for simple IDataObject's that expose their state as
 * public fields. Uses reflection to expose all public, non-static
 * fields as IDataObject properties.
 * 
 * @author matthew
 * 
 * @see dsto.dfc.databeans.DataObjects#fireChangedEvents
 */
public class SimpleDataObject extends AbstractDataObject
{
  /**
   * The default implementation uses
   * {@link TypeRegistry#peekTypeInfo(Class)}to see whether any type
   * info is registered for the class.
   */
  public TypeInfo getTypeInfo ()
  {
    return TypeRegistry.peekTypeInfo (getClass ());
  }
  
  public void setValue (Object name, Object value)
  {
    setValue (name, value, PERSISTENT_OVERRIDE);
  }
  
  public void setValue (Object name, Object value, int mode)
  {
    // todo put this back in when there's a way to tell that transient is not supported
    // if ((mode & TRANSIENT) != 0)
    //   throw new IllegalArgumentException ("Transient properties not supported");
    
    try
    {
      Field field = getClass ().getField (name.toString ());
      field.setAccessible (true);
      
      Object oldValue = field.get (this);
      
      if (oldValue == null || (mode & OVERRIDE) != 0)
      {
        // Field.set () generates a useless error message in the
        // following case, so we check for it ourselves
        if (value == null && field.getType ().isPrimitive ())
          throw new IllegalArgumentException ("Cannot set primitive field " + name + " to null");
          
        field.set (this, value);
      
        unregisterValue (oldValue);
        registerValue (name, value);
        
        firePropertyChangedEvent
          (name, oldValue, value,
           (field.getModifiers () & Modifier.TRANSIENT) != 0);
      }
    } catch (Exception ex)
    {
      IllegalArgumentException ex2 =
        new IllegalArgumentException ("Error setting field \"" + name + "\"");
      
      ex2.initCause (ex);
      
      throw ex2;
    }
  }

  public Object getValue (Object name)
  {
    try
    {
      Field field = getClass ().getField (name.toString ());

      field.setAccessible (true);
      
      return field.get (this);
    } catch (NoSuchFieldException ex)
    {
      // non-existent field is null
      return null;
    } catch (Exception ex)
    {
      throw new IllegalArgumentException
        ("Error reading field \"" + name + "\": " + ex);
    }
  }

  public boolean isTransient (Object name)
  {
    try
    {
      Field field = getClass ().getField (name.toString ());

      return (field.getModifiers () & Modifier.TRANSIENT) != 0; 
    } catch (NoSuchFieldException ex)
    {
      // this is OK, non-existent properties are not transient
      return false;
    } catch (Exception ex)
    {
      throw new IllegalArgumentException
        ("Error reading field \"" + name + "\": " + ex);
    }
  }

  public Iterator propertyIterator ()
  {
    return getFieldNames ().iterator ();
  }

  /**
   * Get the current set of field names from this class.
   */
  protected Collection getFieldNames ()
    throws SecurityException
  {
    Field [] allFields = getClass ().getFields ();
    ArrayList fields = new ArrayList (allFields.length);
      
    for (int i = 0; i < allFields.length; i++)
    {
      Field field = allFields [i];

      try
      {
        field.setAccessible (true);
        
        if ((field.getModifiers () & Modifier.STATIC) == 0 && field.get (this) != null)
          fields.add (field.getName ());
      } catch (Exception ex)
      {
        // hide property on error
        
        Log.diagnostic
          ("Error reading field \"" + field.getName () + "\"", this, ex);
      }
    }
    
    return fields;
  }
}
