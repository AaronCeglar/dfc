package dsto.dfc.databeans;

/**
 * Interface for classes that function as an indirect link to a data
 * object.
 * <p>
 *
 * A link may be either "hard" or "soft" as specified by
 * {@link #isSoftLink()}. A hard link behaves exactly the same as a
 * normal reference to a data object: property events automatically
 * propagate down the chain of parents. Soft links do not propagate
 * events and are usually used to create cross-tree connections.
 * Creating hard cross-tree connections at a minimum generates
 * multiple different property change sets for each property change on
 * the linked object (which may or may not be desirable), and opens
 * the possibility of an infinite loop of property change events if a
 * cycle of references is created (which is certainly not desirable
 * :).
 * <p>
 *
 * In general, when you need two references to a data object, nominate
 * one as the primary (usually the logical "parent" of the object) and
 * use soft links for the other references.
 * <p>
 *
 * NOTE: events will only propagate from links that also implement the
 * IDataObject interface regardless of soft or hard link status (it is
 * a general requirement that all event sources are IDataObject's).
 * The IDataObjectLink interface deliberately <em>doesn't</em>
 * extend IDataObject to allow for simple implementations. However,
 * the default implementations ({@link dsto.dfc.databeans.DataObjectLink}
 * and {@link dsto.dfc.databeans.DataBeanLink}) <em>do</em>
 * implement IDataObject and provide a complete facade for the object
 * they link to, allowing events to transparently propagate. This
 * facade feature allows many clients to ignore the fact that the
 * object is a link at all.
 *
 * @author Matthew Phillips
 */
public interface IDataObjectLink
{
  /**
   * Get the data object that this link points to.
   */
  public IDataObject getLinkTarget ();

  /**
   * True if this is a soft link. Soft links do not automatically
   * propagate events from from the linked object.
   */
  public boolean isSoftLink ();

  public String toString ();
}
