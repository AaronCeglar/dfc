package dsto.dfc.databeans;

import java.util.Iterator;


/**
 * Iterates over an IDataObject's property values.
 * 
 * @see dsto.dfc.databeans.IDataObject#propertyIterator()
 * 
 * @author Matthew Phillips
 */
public class PropertyValueIterator implements Iterator
{
  private IDataObject object;
  private Iterator propertyIterator;
  private Object property;

  public PropertyValueIterator (IDataObject object)
  {
    this.object = object;
    this.propertyIterator = object.propertyIterator ();
  }

  public void remove ()
  {
    object.setValue (property, null);
  }

  public boolean hasNext ()
  {
    return propertyIterator.hasNext ();
  }

  public Object next ()
  {
    property = propertyIterator.next ();
    
    return object.getValue (property);
  }
}
