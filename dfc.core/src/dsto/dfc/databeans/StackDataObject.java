package dsto.dfc.databeans;

/**
 * A convenience class that extends ListDataObject to provide a
 * monitored stack.
 * 
 * @author Steven Johnson Created on 24/02/2005
 */
public class StackDataObject extends ListDataObject
{

  public StackDataObject ()
  {
    super ();
  }

  public Object peek ()
    throws IllegalStateException
  {
    checkEmpty ();
    
    return get (size () - 1);
  }

  public Object pop ()
    throws IllegalStateException
  {
    checkEmpty ();
    
    return remove (size () - 1);
  }

  public void push (Object element)
  {
    add (size (), element);
  }
  
  private void checkEmpty ()
    throws IllegalStateException
  {
    if (isEmpty ())
      throw new IllegalStateException ("Stack is empty");
  }
}