package dsto.dfc.databeans;

import java.util.EventListener;

/**
 * Defines a listener to IDataObject property change events.
 * 
 * @author mpp
 * @version $Revision$
 */
public interface PropertyListener extends EventListener
{
  public void propertyValueChanged (PropertyEvent e);
}
