package dsto.dfc.databeans;

import java.util.Collection;
import java.util.Iterator;

/**
 * Defines an object that provides unified access to its state via property
 * accessors. The significant state of a data object may be accessed entirely
 * via its getValue () and setValue () methods.<p>
 * 
 * Data objects also provide unified notification of property changes to
 * themselves and their child data objects. Property events from child objects
 * percolate down to listeners of the parent. These changes to the object tree
 * may be monitored via the {@link dsto.dfc.databeans.PropertyListener}
 * interface, which uses {@link dsto.dfc.databeans.PropertyPath} to define the
 * path to values in the tree.<p>
 *  
 * Properties of data objects can be explicitly marked as transient: see
 * {@link #isTransient(Object)}.
 * 
 * @author mpp
 * @version $Revision$
 * 
 * @see dsto.dfc.databeans.IDataBean
 * @see dsto.dfc.databeans.DataObjects
 * @see dsto.dfc.databeans.PropertyListener
 */
public interface IDataObject
{
  public static final int PERSISTENT = 0;
  public static final int TRANSIENT = 1;
  public static final int OVERRIDE = 2;
  public static final int TRANSIENT_OVERRIDE = TRANSIENT | OVERRIDE;
  public static final int PERSISTENT_OVERRIDE = PERSISTENT | OVERRIDE;

  /**
   * Get the optional type information for this object. If the object
   * has none, this will be null.
   * 
   * @see TypeRegistry
   */
  public TypeInfo getTypeInfo ();
  
  /**
   * Set the value of a given property. If the object supports it, a
   * property change event should be emitted. This is usually a
   * synonym for
   * <code>setValue (name, value, PERSISTENT_OVERRIDE)</code>
   * 
   * @param name The property "name" (may be any object).
   * @param value The property value.
   * 
   * @see #setValue(Object, Object, int)
   * @see #getValue(Object)
   * 
   * @see dsto.dfc.databeans.PropertyListener
   * @see dsto.dfc.databeans.PropertyEvent
   */
  public void setValue (Object name, Object value);
  
  /**
   * Set the value of a given property. If the object supports it, a
   * property change event should be emitted.
   * 
   * @param name A property name.
   * @param value The new value. On most objects using null removes
   *          the property.
   * @param mode The mode for the value. This is a bitwise OR of
   *          TRANSIENT and/or OVERRIDE. Using TRANSIENT specifies the
   *          value is to be marked transient (ie isTransient (name)
   *          will return true). Not all data object implementations
   *          support dynamic transience. OVERRIDE must be set if an
   *          existing value is to be overwritten: if not set and a
   *          value is already defined, then nothing is changed. The
   *          IDataObject.setValue (name, value) call corresponds to
   *          IDataObject.setValue (name, value, OVERRIDE).
   * 
   * @see #setValue(Object, Object)
   */
  public void setValue (Object name, Object value, int mode);
  
  /**
   * Get the value of a given property.
   * 
   * @param name The property "name" (may be any object).
   * @return the value of a given property or null if the property is not
   * defined.
   * 
   * @see #setValue(Object, Object)
   * @see #propertyIterator()
   */
  public Object getValue (Object name);
  
  /**
   * Test a property is transient (ie will not be preserved if the object is
   * saved).
   * 
   * @param name The property name.
   * @return boolean True if the property is transient.
   */
  public boolean isTransient (Object name);

  /**
   * Get an iterator that scans over all currently defined properties.
   * 
   * @return An iterator that yields all defined property values (ie that
   * would get a non-null result from {@link #getValue(Object)}).
   * 
   * @see #getValue(Object)
   * @see PropertyValueIterator
   */
  public Iterator<Object> propertyIterator ();
  
  /**
   * Shallow clone the object prior to being recusively "deep" cloned. This
   * should return a new instance of the same class as the object
   * suitable for receiving a series of setValue () calls to add copied
   * properties from the original. Only state that would not be copied by
   * the copying of all properties needs to be cloned by this method.<p>
   * 
   * NOTE: Clients should not need to call this method:
   * use {@link DataObjects#deepClone(IDataObject)} instead. 
   * 
   * @return A new object of the same type, with the same values as the
   * original.
   * @throws UnsupportedOperationException if the object or child object could not
   * be cloned.
   * 
   * @see DataObjects#deepClone(IDataObject)
   */
  public IDataObject shallowClone () throws UnsupportedOperationException;

  /**
   * Add a listener that will be notified when changes to the properties on
   * this or child objects are made.
   * 
   * @see #removePropertyListener(PropertyListener)
   * @see #getPropertyListeners()
   */
  public void addPropertyListener (PropertyListener l);
  
  /**
   * Reverse the effect of {@link #addPropertyListener(PropertyListener)}.
   */
  public void removePropertyListener (PropertyListener l);
  
  /**
   * Return a non-modifiable list of registered property listeners.
   * 
   * @see #addPropertyListener(PropertyListener) 
   */
  public Collection getPropertyListeners ();
}
