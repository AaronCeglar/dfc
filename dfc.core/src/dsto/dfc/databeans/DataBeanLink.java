package dsto.dfc.databeans;

import static dsto.dfc.databeans.DataObjects.objectLinkToString;

import java.util.Iterator;


/**
 * A link to an IDataBean that also provides a facade to the bean
 * (i.e. exposes the same property set).
 *
 * @see dsto.dfc.databeans.DataObjectLink
 *
 * @author mpp
 * @version $Revision$
 */
public class DataBeanLink
  extends AbstractDataBean
  implements IDataBean, IDataObjectLink, PropertyListener
{
  protected IDataBean bean;
  protected boolean isSoftLink;

  /**
   * Create a link with no target. Use
   * {@link #setLinkTarget(IDataBean)} before use or Bad Things will
   * occur.
   */
  public DataBeanLink ()
  {
    this (null);
  }

  public DataBeanLink (IDataBean bean)
  {
    this (bean, true);
  }

  public DataBeanLink (IDataBean bean, boolean isSoftLink)
  {
    this.bean = bean;
    this.isSoftLink = isSoftLink;

    initListener ();
  }

  public void dispose ()
  {
    if (bean != null)
    {
      disposeListener ();

      bean = null;
    }
  }

  protected void initListener ()
  {
    if (bean != null)
    {
      bean.addPropertyListener (this);
    }
  }

  protected void disposeListener ()
  {
    if (bean != null)
    {
      bean.removePropertyListener (this);
    }
  }

  @Override
  public String toString ()
  {
    return objectLinkToString (this) + "[soft=" + isSoftLink + "]";
  }

  public IDataObject getLinkTarget ()
  {
    return bean;
  }

  public IDataBean getDataBeanTarget ()
  {
    return bean;
  }

  /**
   * Set the object being pointed to by this link. Not recommended to
   * be used outside of initialisation.
   */
  public void setLinkTarget (IDataBean target)
  {
    disposeListener ();

    this.bean = target;

    initListener ();
  }

  public boolean isSoftLink ()
  {
    return isSoftLink;
  }

  public void setSoftLink (boolean newValue)
  {
    if (listeners.size () > 0)
      throw new IllegalStateException ("Cannot change link type while object has listeners");

    this.isSoftLink = newValue;
  }

  @Override
  public void setValue (Object name, Object value)
  {
    bean.setValue (name, value);
  }

  @Override
  public void setValue (Object name, Object value, int mode)
  {
    bean.setValue (name, value, mode);
  }

  public Object getValue (Object name)
  {
    return bean.getValue (name);
  }

  public Iterator propertyIterator ()
  {
    return bean.propertyIterator ();
  }

  @Override
  public IDataObject shallowClone () throws UnsupportedOperationException
  {
    return super.shallowClone ();
  }

  public String [] getPropertyNames ()
  {
    return bean.getPropertyNames ();
  }

  public void setValue (String name, Object value, int mode)
  {
    bean.setValue (name, value, mode);
  }

  public boolean isTransient (Object name)
  {
    return bean.isTransient (name);
  }

  @Override
  public TypeInfo getTypeInfo ()
  {
    return bean.getTypeInfo ();
  }

  // PropertyListener interface

  public void propertyValueChanged (PropertyEvent e)
  {
    // re-fire event as if from this bean
    firePropertyChangedEvent (e.path, e.oldValue, e.newValue,
                              e.transientProperty);
  }
}
