package dsto.dfc.databeans;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Extends SimpleDataObject to allow properties to be added
 * dynamically to the fixed base set supported by the class. If a
 * property that is not a field of the object is set, then the
 * property is added dynamically.
 * <p>
 * 
 * Note: this class has been designed add little extra cost for normal
 * property accesses, so it can be cheaply used in place of
 * SimpleDataObject wherever extended properties might be needed, even
 * if they're not currently required.
 * 
 * @author Matthew Phillips
 */
public class FancyDataObject extends SimpleDataObject
{
  private HashMap extendedProperties;
  private HashSet extendedTransientProperties;
  
  public FancyDataObject ()
  {
    // zip: extendedProperties is lazy-initialized
  }
  
  public IDataObject shallowClone ()
  {
    FancyDataObject copy = (FancyDataObject)super.shallowClone ();
    
    if (extendedProperties != null)
      copy.extendedProperties = new HashMap (extendedProperties);
    
    if (extendedTransientProperties != null)
      copy.extendedTransientProperties = new HashSet (extendedTransientProperties);
    
    return copy;
  }
  
  public Object getValue (Object name)
  {
    Object extendedValue = null;
    
    if (extendedProperties != null)
      extendedValue = extendedProperties.get (name);
    
    if (extendedValue == null)
      return super.getValue (name);
    else
      return extendedValue;
  }
  
  public void setValue (Object name, Object value, int mode)
  {
    try
    {
      super.setValue (name, value, mode);
    } catch (IllegalArgumentException ex)
    {
      // if we hit a property not corresponding to a field...
      if (ex.getCause () instanceof NoSuchFieldException)
        setExtendedValue (name, value, mode);
      else
        throw ex;
    }
  }
  
  private void setExtendedValue (Object name, Object value, int mode)
  {
    if (extendedProperties == null)
    {
      extendedProperties = new HashMap ();
      extendedTransientProperties = new HashSet ();
    }
    
    Object oldValue = extendedProperties.get (name);
    
    if (oldValue == null || (mode & OVERRIDE) != 0)
    {
      if (value == null)
      {
        extendedProperties.remove (name);
        extendedTransientProperties.remove (name);
        
        if (extendedProperties.isEmpty ())
        {
          extendedProperties = null;
          extendedTransientProperties = null;
        }
      } else
      {
        extendedProperties.put (name, value);
        
        if ((mode & TRANSIENT) != 0)
          extendedTransientProperties.add (name);
      }
      
      if (oldValue != null)
        unregisterValue (oldValue);      
      
      if (value != null)
        registerValue (name, value);
      
      firePropertyChangedEvent (name, oldValue, value, (mode & TRANSIENT) != 0);
    }
  }
  
  public Iterator<Object> propertyIterator ()
  {
    if (extendedProperties != null)
    {
      HashSet fieldNames = new HashSet (getFieldNames ());
      
      fieldNames.addAll (extendedProperties.keySet ());
      
      return fieldNames.iterator ();
    } else
    {
      return super.propertyIterator ();
    }
  }
  
  public boolean isTransient (Object name)
  {
    if (extendedProperties != null && extendedProperties.containsKey (name))
      return extendedTransientProperties.contains (name);
    else
      return super.isTransient (name);
  }
  
  public void childPropertyChanged (Object childProperty, PropertyEvent e)
  {
    // upgrade transient to non-transient if we see a non-transient child change
    if (!e.transientProperty && extendedProperties != null)
      extendedTransientProperties.remove (childProperty);

    super.childPropertyChanged (childProperty, e);
  }
}
