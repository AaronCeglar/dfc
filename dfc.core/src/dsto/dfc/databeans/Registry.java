package dsto.dfc.databeans;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import dsto.dfc.databeans.DataObject;

/**
 * A registry of data objects where each object is uniquely identified
 * by one of its properties. The properties of a registry are the key
 * values of objects add ()'ed to it, the values are the objects
 * themselves. This provides extra convenience over using setValue ()
 * and getValue () calls plus automatic uniqueness checking.
 * <p>
 * 
 * Note: the key property of an object should not change once added to
 * the registry.
 * 
 * @author Matthew Phillips
 */
public class Registry extends DataObject
{
  public final Object keyProperty;

  /**
   * Create a default instance using the default string "id" as the
   * key property.
   */
  public Registry ()
  {
    this ("id");
  }

  /**
   * Create a new instance.
   * 
   * @param keyProperty The property that is used to identify objects
   *          added to the registry.
   */
  public Registry (Object keyProperty)
  {
    super ();
    
    this.keyProperty = keyProperty;
  }

  /**
   * Add an object to the registry.
   * 
   * @param object The object to add.
   * 
   * @throws IllegalArgumentException if the objects' key is null or
   *           already in use.
   */
  public void add (IDataObject object)
    throws IllegalArgumentException
  {
    Object key = object.getValue (keyProperty);
    
    if (key == null)
      throw new IllegalArgumentException ("Key cannot be null");
    else if (propertyToValue.containsKey (key))
      throw new IllegalArgumentException
        ("Entity with key " + key + " already defined");
    
    propertyToValue.put (key, object);
    registerValue (key, object);
    
    firePropertyChangedEvent (key, null, object, false);
  }
  
  public void remove (Object key)
  {
    setValue (key, null, OVERRIDE);
  }
  
  /**
   * Peek to see if an object exists.
   * 
   * @param key The object key.
   * 
   * @return The object, or null if not found.
   * 
   * @see #find(Object)
   */
  public synchronized IDataObject peek (Object key)
  {
    return (IDataObject)propertyToValue.get (key);
  }
  
  /**
   * Find an object with a given key. Generates an error if no object
   * with the key is found.
   * 
   * @param key The object key.
   * @return The object.
   * 
   * @throws IllegalArgumentException if no object with the key exists.
   * 
   * @see #peek(Object)
   */
  public IDataObject find (Object key)
    throws IllegalArgumentException
  {
    IDataObject object = peek (key);
    
    if (object != null)
      return object;
    else
      throw new IllegalArgumentException ("No object with key " + key);
  }
  
  /**
   * Test if a given key exists in the registry.
   */
  public boolean containsKey (Object key)
  {
    return propertyToValue.get (key) != null;
  }

  /**
   * Create an iterator over all the entities in the registry.
   */
  public Iterator iterator ()
  {
    return propertyToValue.values ().iterator ();
  }

  /**
   * The number of objects in the registry.
   */
  public int size ()
  {
    return propertyToValue.size ();
  }

  /**
   * Create a snapshot of the current objects. This will not change
   * if the registry is later modified,
   */
  public Set snapshot ()
  {
    return new HashSet (propertyToValue.values ());
  }  
}
