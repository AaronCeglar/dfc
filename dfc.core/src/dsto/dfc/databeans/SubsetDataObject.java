package dsto.dfc.databeans;

import java.util.Iterator;

/**
 * A subset of an IDataObject. Uses an {@link IFilter} to expose only
 * a selected subset of properties from a wrapped {@link IDataObject}.
 * 
 * @see DataObjectView
 * 
 * @author Matthew Phillips
 */
public class SubsetDataObject extends DataObject implements PropertyListener
{
  private IDataObject source;
  private IFilter filter;
  
  /**
   * Create a new instance.
   * 
   * @param source The source object to be filtered.
   * @param filter The filter: only values selected by this will be exposed.
   */
  public SubsetDataObject (IDataObject source, IFilter filter)
  {
    this.source = source;
    this.filter = filter;
    
    for (Iterator i = source.propertyIterator (); i.hasNext ();)
    {
      Object property = i.next ();
      Object value = source.getValue (property);
      
      update (property, value);
    }
    
    source.addPropertyListener (this);
  }
  
  public void dispose ()
  {
    source.removePropertyListener (this);
  }
  
  private void update (Object property, Object value)
  {
    if (filter.include (value))
      setValue (property, value);
    else
      setValue (property, null);
  }
  
  public void propertyValueChanged (PropertyEvent e)
  {
    if (e.path.length () == 1)
    {
      if (e.oldValue != null)
        setValue (e.path.first (), null);
      
      if (e.newValue != null)
        update (e.path.first (), e.newValue);
    } else
    {
      update (e.path.first (), source.getValue (e.path.first ()));
    }
  }
}
