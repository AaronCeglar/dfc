package dsto.dfc.databeans;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * A default implemention of IDataObject based on a java.util.Map.
 * This may be used as is or extended.
 * 
 * @author Matthew Phillips
 */
public class DataObject extends AbstractDataObject
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  
  /**
   * Maps from property names to values. Subclasses may modify, but
   * must ensure that values are registered/deregistered as required.
   */
  protected Map<Object, Object> propertyToValue;
  protected TypeInfo typeInfo;
  
  public DataObject ()
  {
    this (null);
  }
  
  /**
   * Create an instance with a specified type.
   */
  public DataObject (TypeInfo type)
  {
    super ();
    
    this.propertyToValue = new HashMap<Object, Object> ();
    this.typeInfo = type;
  }

  public IDataObject shallowClone ()
  {
    DataObject copy = (DataObject)super.shallowClone ();
    
    copy.propertyToValue = (Map)((HashMap)propertyToValue).clone ();
    
    for (Iterator i = propertyToValue.entrySet ().iterator (); i.hasNext ();)
    {
      Map.Entry entry = (Map.Entry)i.next ();
      
      registerValue (entry.getKey (), entry.getValue ());
    }
    
    return copy;
  }
  
  /**
   * Create an iterator over all the values in the object.
   */
  public Iterator iterator ()
  {
    return new ValueIterator ();
  }
  
  public Iterator<Object> propertyIterator ()
  {
    return new PropertyIterator ();
  }
  
  // IDataObject interface
  
  public TypeInfo getTypeInfo ()
  {
    return typeInfo;
  }
  
  public void setTypeInfo (TypeInfo typeInfo)
  {
    this.typeInfo = typeInfo;
  }
  
  public Object getValue (Object name)
  {
    return propertyToValue.get (name);
  }
  
  public void setValue (Object name, Object value, int mode)
  {
    Object oldValue = propertyToValue.get (name);

    if (oldValue == null || (mode & OVERRIDE) != 0)
    {
      if (value == null)
        propertyToValue.remove (name);
      else
        propertyToValue.put (name, value);
      
      if (oldValue != null)
        unregisterValue (oldValue);      
      
      if (value != null)
        registerValue (name, value);
      
      firePropertyChangedEvent (name, oldValue, value, false);
    }
  }
  
  public boolean isTransient (Object name)
  {
    return false;
  }
  
  /**
   * Add Java serialization support.
   */
  private void readObject (ObjectInputStream in) 
    throws IOException, ClassNotFoundException
  {
    in.defaultReadObject ();
    
    for (Iterator i = propertyToValue.entrySet ().iterator (); i.hasNext ();)
    {
      Map.Entry entry = (Map.Entry)i.next ();
      
      registerValue (entry.getKey (), entry.getValue ());
    }
  }
  
  private abstract class BaseIterator implements Iterator
  {
    protected Iterator entryIterator;
    protected Entry current;

    public BaseIterator ()
    {
      this.entryIterator = propertyToValue.entrySet ().iterator ();
    }
    
    public void remove ()
    {
      boolean wasTransient = isTransient (current.getKey ());
      
      entryIterator.remove ();
      
      firePropertyChangedEvent
        (current.getKey (), current.getValue (), null, wasTransient);
      
      current = null;
    }

    public boolean hasNext ()
    {
      return entryIterator.hasNext (); 
    }
  }
  
  /**
   * Iterates over object values.
   */
  class ValueIterator extends BaseIterator implements Iterator
  {
    public Object next ()
    {
      current = (Entry)entryIterator.next ();
      
      return current.getValue ();
    }
  }
  
  /**
   * Iterates over object properties.
   */
  class PropertyIterator extends BaseIterator implements Iterator
  {
    public Object next ()
    {
      current = (Entry)entryIterator.next ();
      
      return current.getKey ();
    }
  }
}
