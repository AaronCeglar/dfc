package dsto.dfc.databeans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import dsto.dfc.collections.IdentityHashMap;

import static java.lang.String.format;
import static java.lang.System.identityHashCode;

import static dsto.dfc.util.Objects.className;
import static dsto.dfc.util.Objects.objectsEqualEx;

/**
 * Utility methods for data objects.
 *
 * @author mpp
 * @version $Revision: 1.43 $
 */
public final class DataObjects
{
  private DataObjects ()
  {
    // zip
  }

  /**
   * Deep clone a graph of IDataObject's. <em>Non IDataObject's are assumed to
   * be immutable and are not cloned</em>.
   *
   * @param root The root of the object graph.
   * @return A copy of root.
   * @throws UnsupportedOperationException if an object could not be cloned.
   */
  public static IDataObject deepClone (IDataObject root)
    throws UnsupportedOperationException
  {
    try
    {
      return doDeepClone (root, new IdentityHashMap ());
    } catch (CloneNotSupportedException ex)
    {
      throw new UnsupportedOperationException
        ("Clone failed: " + ex.getMessage ());
    }
  }

  private static IDataObject doDeepClone (IDataObject root,
                                          IdentityHashMap copies)
    throws CloneNotSupportedException
  {
    IDataObject copy = (IDataObject)copies.get (root);

    if (copy != null)
      return copy;
    else
      return doDeepCloneObject (root, copies);
  }

  private static IDataObject doDeepCloneObject (IDataObject root,
                                                IdentityHashMap copies)
    throws CloneNotSupportedException
  {
    IDataObject copy = root.shallowClone ();

    copies.put (root, copy);

    for (Iterator i = root.propertyIterator (); i.hasNext ();)
    {
      Object property = i.next ();
      Object value = root.getValue (property);

      // clone value
      if (value instanceof IDataObject)
        value = doDeepClone ((IDataObject) value, copies);

      if (root.isTransient (property))
        copy.setValue (property, value, IDataBean.TRANSIENT | IDataBean.OVERRIDE);
      else
        copy.setValue (property, value, IDataBean.OVERRIDE);
    }

    return copy;
  }

  /**
   * Shallow copy the property values of one object to another. The property
   * values themselves are not cloned. The transient status of property values
   * is preserved.
   */
  public static void shallowCopy (IDataObject source, IDataObject target)
  {
    for (Iterator i = source.propertyIterator (); i.hasNext ();)
    {
      Object property = i.next ();

      Object value = source.getValue (property);

      if (source.isTransient (property)
          && (target.getValue (property) == null ||
              target.isTransient (property)))
      {
        target.setValue (property, value, IDataBean.TRANSIENT | IDataBean.OVERRIDE);
      } else
      {
        target.setValue (property, value, IDataBean.OVERRIDE);
      }
    }
  }

  /**
   * Set a value using a path expression. Any missing parent entries will be
   * filled in by empty, transient DataBean instances.
   *
   * @param object The root object.
   * @param pathExpr A path expression such as "bean1/bean2/property".
   * @param value The value to set the property referenced by pathExpr to.
   * @throws IllegalArgumentException If path expr is not valid for the root
   *           bean.
   *
   * @see #makePath(IDataObject, String)
   */
  public static void setValue (IDataObject object, String pathExpr, Object value)
  {
    setValue (object, pathExpr, value, IDataObject.PERSISTENT_OVERRIDE);
  }

  public static void setValue (IDataObject object, String pathExpr,
      Object value, int mode) throws IllegalArgumentException
  {
    setValue (object, new PropertyPath (pathExpr), value, mode);
  }

  public static void setValue (IDataObject object, PropertyPath path,
      Object value) throws IllegalArgumentException
  {
    setValue (object, path, value, IDataObject.PERSISTENT_OVERRIDE);
  }

  /**
   * Set a value using a property path. Any missing parent entries will be
   * filled in by empty, transient DataBean instances.
   *
   * @param object The root object.
   * @param path The path to the property.
   * @param value The value to set the property referenced by path to.
   * @throws IllegalArgumentException If path expr is not valid for the root
   *           bean.
   * @throws ClassCastException if an object on the parent path exists already
   *           and is not an IDataObject.
   *
   * @see #makePath(IDataObject, String)
   */
  public static void setValue (IDataObject object, PropertyPath path,
      Object value, int mode) throws IllegalArgumentException
  {
    if (path.length () > 1)
      path.parent ().make (object, mode);

    IDataObject targetObject = path.followParent (object);

    targetObject.setValue (path.last (), value, mode);
  }

  /**
   * Get a value using a path expression.
   *
   * @param object The root object.
   * @param pathExpr A path expression such as "bean1/bean2/property".
   * @return The value of the property referenced by pathExpr.
   * @throws IllegalArgumentException If path expr is not valid for the root
   *           bean.
   */
  public static Object getValue (IDataObject object, String pathExpr)
    throws IllegalArgumentException
  {
    PropertyPath path = new PropertyPath (pathExpr);
    IDataObject targetObject = path.followParent (object);

    return targetObject.getValue (path.last ());
  }

  /**
   * Get a value using a property path.
   *
   * @param object The root object.
   * @param path A property path.
   * @return The value of the property referenced by path.
   * @throws IllegalArgumentException If path expr is not valid for the root
   *           bean.
   */
  public static Object getValue (IDataObject object, PropertyPath path)
    throws IllegalArgumentException
  {
    IDataObject targetObject = path.followParent (object);

    return targetObject.getValue (path.last ());
  }

  /**
   * Get a value from a data object using the path expression, returning a
   * default if none set.
   *
   * @param object The object.
   * @param pathExpr The property path.
   * @param defaultValue The default value.
   * @return Either the value of property or defaultValue if no value in bean.
   */
  public static Object getValue (IDataObject object, String pathExpr,
      Object defaultValue)
  {
    PropertyPath path = new PropertyPath (pathExpr);
    IDataObject targetObject = path.followParent (object);

    Object value = targetObject.getValue (path.last ());

    if (value == null)
      return defaultValue;
    else
      return value;
  }

  /**
   * Get a string value from a data object using the path expression, returning
   * a default if none set.
   *
   * @param object The object.
   * @param pathExpr The property path.
   * @param defaultValue The default value.
   * @return Either the value of property or defaultValue if no value in bean.
   */
  public static String getValue (IDataObject object, String pathExpr,
      String defaultValue)
  {
    Object value = getValue (object, pathExpr);

    if (value == null)
      return defaultValue;
    else
      return value.toString ();
  }

  /**
   * Get an int value from a data object using the path expression, returning a
   * default if none set.
   *
   * @param object The object.
   * @param path The property path.
   * @param defaultValue The default value.
   * @return Either the value of property or defaultValue if no value in bean.
   * @throws IllegalArgumentException If path expr is not valid for the object.
   */
  public static int getValue (IDataObject object, String path, int defaultValue)
  {
    Integer value = (Integer)getValue (object, path);

    if (value == null)
      return defaultValue;
    else
      return value.intValue ();
  }

  /**
   * Get a boolean value from a data object using the path expression, returning
   * a default if none set.
   *
   * @param object The object.
   * @param path The property path.
   * @param defaultValue The default value.
   * @return Either the value of property or defaultValue if no value in bean.
   */
  public static boolean getValue (IDataObject object, String path,
      boolean defaultValue)
  {
    Boolean value = (Boolean)getValue (object, path);

    if (value == null)
      return defaultValue;
    else
      return value.booleanValue ();
  }

  /**
   * Ensure a path exists into a given object. Any missing parts of the path are
   * filled in with empty DataBean instances.
   *
   * @param object The bean to create the path on.
   * @param path The path expression (eg "folder/name").
   *
   * @return The last element in the path.
   *
   * @throws ClassCastException if the last item on the path already exists and
   *           is not an IDataObject.
   *
   * @see #makePath(IDataObject, String, int)
   * @see PropertyPath#make(IDataObject)
   */
  public static IDataObject makePath (IDataObject object, String path)
    throws ClassCastException
  {
    return makePath (object, path, IDataObject.PERSISTENT_OVERRIDE);
  }

  /**
   * Ensure a path exists into a given object. Any missing parts of the path are
   * filled in with empty DataBean instances.
   *
   * @param object The bean to create the path on.
   * @param path The path expression (eg "folder/name").
   * @param mode The create mode for new path elements. One of
   *          IDataObject.TRANSIENT or IDataObject.PERSISTENT.
   *
   * @return The last element in the path.
   *
   * @throws ClassCastException if the last item on the path already exists and
   *           is not an IDataObject.
   *
   * @see PropertyPath#make(IDataObject)
   */
  public static IDataObject makePath (IDataObject object, String path,
                                      int mode)
    throws ClassCastException
  {
    return new PropertyPath (path).make (object, mode);
  }

  /**
   * Ensure a path exists into a given object. Any missing parts of the path are
   * filled in with empty DataBean instances, except the last which will be an
   * instance of the supplied leafClass type.
   *
   * @param object The bean to create the path on.
   * @param path The path expression (eg "folder/name").
   * @param leafClass The type of the last element in the path.
   *
   * @return The last element in the path.
   *
   * @throws ClassCastException if the last item on the path already exists and
   *           is not an IDataObject.
   *
   * @see PropertyPath#make(IDataObject)
   */
  public static Object makePath (IDataObject object, String path,
                                 Class leafClass)
    throws ClassCastException
  {
    return makePath (object, path, leafClass, DataObject.PERSISTENT_OVERRIDE);
  }

  /**
   * Ensure a path exists into a given object. Any missing parts of the path are
   * filled in with empty DataBean instances, except the last which will be an
   * instance of the supplied leafClass type.
   *
   * @param object The bean to create the path on.
   * @param path The path expression (eg "folder/name").
   * @param leafClass The type of the last element in the path.
   * @param mode The create mode for new path elements. One of
   *          IDataObject.TRANSIENT or IDataObject.PERSISTENT.
   *
   * @return The last element in the path.
   *
   * @throws ClassCastException if the last item on the path already exists and
   *           is not an IDataObject.
   *
   * @see PropertyPath#make(IDataObject)
   */
  public static Object makePath (IDataObject object, String path,
                                 Class leafClass, int mode)
    throws ClassCastException
  {
    return new PropertyPath (path).make (object, leafClass, mode);
  }

  /**
   * Compare the properties of two data objects and fire appropriate property
   * change events. This can be useful for generating events when simple data
   * objects (eg {@link SimpleDataObject}) have changed in an unknown way.
   *
   * @param oldObject The old value.
   * @param newObject The new value. This is the value for which events will be
   *          fired.
   */
  public static void fireChangedEvents (AbstractDataObject oldObject,
                                        AbstractDataObject newObject)
  {
    for (Iterator i = oldObject.propertyIterator (); i.hasNext ();)
    {
      Object property = i.next ();

      Object oldValue = oldObject.getValue (property);
      Object newValue = newObject.getValue (property);

      newObject.firePropertyChangedEvent (property, oldValue, newValue,
                                          newObject.isTransient (property));
    }
  }

  /**
   * Find a data object with a given property value in a data objects value
   * set.
   *
   * @param objects A data object.
   * @param property The property to match.
   * @param value The value to match.
   *
   * @return The first matching object, or null if none found.
   */
  public static IDataObject find (IDataObject objects, Object property,
                                  Object value)
  {
    for (Iterator i = objects.propertyIterator (); i.hasNext ();)
    {
      Object object = objects.getValue (i.next ());

      if (object instanceof IDataObject
          && objectsEqualEx (((IDataObject)object).getValue (property), value))
      {
        return (IDataObject)object;
      }
    }

    return null;
  }

  /**
   * Find a data object with a given property value in a collection.
   *
   * @param objects A collection of IDataObject's.
   * @param property The property to match.
   * @param value The value to match.
   *
   * @return The first matching object, or null if none found.
   */
  public static IDataObject findInCollection (Collection objects,
                                              Object property, Object value)
  {
    for (Iterator i = objects.iterator (); i.hasNext ();)
    {
      IDataObject object = (IDataObject) i.next ();

      if (objectsEqualEx (object.getValue (property), value))
        return object;
    }

    return null;
  }

  /**
   * Find a data object with a given property value in a collection.
   *
   * @param comparator The comparator to use in matching objects.
   * @param objects A collection of IDataObject's.
   * @param property The property to match.
   * @param value The value to match.
   *
   * @return The first matching object, or null if none found.
   */
  public static IDataObject findInCollection (Comparator comparator,
                                              Collection objects,
                                              Object property,
                                              Object value)
  {
    for (Iterator i = objects.iterator (); i.hasNext ();)
    {
      IDataObject object = (IDataObject) i.next ();

      Object propertyValue = object.getValue (property);

      if (propertyValue == value ||
            (propertyValue != null &&
             comparator.compare (propertyValue, value) == 0))
      {
        return object;
      }
    }

    return null;
  }

  /**
   * Find the index of a data object with a given property value in a list.
   *
   * @param objects A list of IDataObject's.
   * @param property The property to match.
   * @param value The value to match.
   *
   * @return The index of the first matching object, or -1 if none found.
   */
  public static int findIndex (List objects, Object property, Object value)
  {
    for (ListIterator i = objects.listIterator (); i.hasNext ();)
    {
      IDataObject object = (IDataObject) i.next ();

      if (objectsEqualEx (object.getValue (property), value))
        return i.nextIndex () - 1;
    }

    return -1;
  }

  /**
   * Generate an array of string property values from a collection of
   * IDataObject's.
   *
   * @param objects A collection of IDataObject's.
   * @param property The property to extract from each object.
   */
  public static String [] stringValues (Collection objects, String property)
  {
    String [] values = new String [objects.size ()];

    int index = 0;

    for (Iterator i = objects.iterator (); i.hasNext ();)
    {
      IDataBean bean = (IDataBean) i.next ();

      values [index++] = bean.getStringValue (property);
    }

    return values;
  }

  /**
   * Test if two data objects are equal. This does a deep test of any
   * IDataObject properties.
   */
  public static boolean objectsEqual (IDataObject o1, IDataObject o2)
  {
    if (o1 == o2)
      return true;
    else if (o1 == null || o2 == null)
      return false;

    ArrayList o1Props = new ArrayList ();
    ArrayList o2Props = new ArrayList ();

    for (Iterator i = o1.propertyIterator (); i.hasNext ();)
      o1Props.add (i.next ());

    for (Iterator i = o2.propertyIterator (); i.hasNext ();)
      o2Props.add (i.next ());

    if (o1Props.size () == o2Props.size ())
    {
      for (int i = 0; i < o1Props.size (); i++)
      {
        Object property = o1Props.get (i);
        Object o1Value = o1.getValue (property);
        Object o2Value = o2.getValue (property);

        if (o1Value instanceof IDataObject && o2Value instanceof IDataObject)
        {
          if (!objectsEqual ((IDataObject)o1Value, (IDataObject)o2Value))
            return false;
        } else
        {
          if (!objectsEqualEx (o1Value, o2Value))
            return false;
        }
      }

      return true;
    } else
    {
      return false;
    }
  }

  /**
   * Shortcut to merge a source object into a property of a target object,
   * recursively merging embedded IDataObject's using
   * {@link #merge(IDataObject, IDataObject)}.
   *
   * @param path The path into the target to merge the source object with. Using
   *          an empty path is equivalent to just calling
   *          {@link #merge(IDataObject, IDataObject)}
   * @param source The source to merge into target. This may either be an
   *          IDataObject, in which case it is merged with the target value if
   *          it is also an IDataObject, or a primitive value, in which case
   *          this call has the same effect as a setValue ().
   * @param target The target to merge to.
   *
   * @see #merge(IDataObject, IDataObject)
   */
  public static void merge (PropertyPath path, Object source,
                            IDataObject target)
  {
    merge (path, source, target, IDataBean.OVERRIDE, false);
  }

  /**
   * Shortcut to merge a source object into a property of a target object,
   * recursively merging embedded IDataObject's using
   * {@link #merge(IDataObject, IDataObject)}.
   *
   * @param path The path into the target to merge the source object with. Using
   *          an empty path is equivalent to just calling
   *          {@link #merge(IDataObject, IDataObject)}
   * @param source The source to merge into target. This may either be an
   *          IDataObject, in which case it is merged with the target value if
   *          it is also an IDataObject, or a primitive value, in which case
   *          this call has the same effect as a setValue ().
   * @param target The target to merge to.
   * @param orMode A bitwise OR of TRANSIENT and/or OVERRIDE. These bits will be
   *          or'd with the default mode used in
   *          {@link IDataObject#setValue(Object, Object, int)}.
   * @param equalise If true then the property set of target is made equal to
   *          that of the source source by deleting any properties in target
   *          that are not in the source. i.e. the target becomes a logical copy
   *          of source. This can be used to generate the minimal number of
   *          changes needed to change target into source. If false, then target
   *          becomes a union of the property sets of source and target.
   *
   * @see #merge(IDataObject, IDataObject, int, boolean)
   */
  public static void merge (PropertyPath path, Object source,
      IDataObject target, int orMode, boolean equalise)
  {
    IDataObject subTarget = path.followParent (target);

    Object targetValue = subTarget.getValue (path.last ());

    // if both are IDataObject's then merge rather than overwrite
    if (source instanceof IDataObject && targetValue instanceof IDataObject)
      merge ((IDataObject) source, (IDataObject) targetValue, orMode, equalise);
    else
      subTarget.setValue (path.last (), source);
  }

  /**
   * Merge a source object into a target object. Recursively merges embedded
   * IDataObject's. Values in target are overrwritten by values in source.
   *
   * @param source The source to merge from.
   * @param target The target to merge to.
   *
   * @see #merge(IDataObject, IDataObject, int)
   */
  public static void merge (IDataObject source, IDataObject target)
  {
    merge (source, target, IDataBean.OVERRIDE, false);
  }

  /**
   * Merge a source object into a target object. Recursively merges embedded
   * IDataObject's.
   *
   * @param source The source to merge from.
   * @param target The target to merge to.
   * @param orMode A bitwise OR of TRANSIENT and/or OVERRIDE. These bits will be
   *          or'd with the default mode used in
   *          {@link IDataObject#setValue(Object, Object, int)}.
   *
   * @see #merge(IDataObject, IDataObject, int, boolean)
   */
  public static void merge (IDataObject source, IDataObject target, int orMode)
  {
    merge (source, target, orMode, false);
  }

  /**
   * Merge a source object into a target object. Recursively merges embedded
   * IDataObject's.
   * <p>
   *
   * NOTE: this has the potential to cause aliasing bugs if the source object
   * continues to be used after merging with target. If the source object is
   * going to continue to be used after merge, it is recommended to
   * {@link #deepClone(IDataObject)} a copy and pass the copy to this method.
   *
   * @param source The source to merge from.
   * @param target The target to merge to.
   * @param orMode A bitwise OR of TRANSIENT and/or OVERRIDE. These bits will be
   *          or'd with the default mode used in
   *          {@link IDataObject#setValue(Object, Object, int)}.
   * @param equalise If true then the property set of target is made equal to
   *          that of the source source by deleting any properties in target
   *          that are not in the source. i.e. the target becomes a logical copy
   *          of source. This can be used to generate the minimal number of
   *          changes needed to change target into source. If false, then target
   *          becomes a union of the property sets of source and target.
   */
  public static void merge (IDataObject source, IDataObject target, int orMode,
                            boolean equalise)
  {
    if (source == target)
      return;

    if (equalise)
    {
      // blow away any properties in target that are not in source
      for (Iterator i = propertySet (target).iterator (); i.hasNext ();)
      {
        Object property = i.next ();

        if (source.getValue (property) == null)
          setValue (source, target, property, null, orMode);
      }
    }

    for (Iterator i = source.propertyIterator (); i.hasNext ();)
    {
      Object property = i.next ();
      Object sourceValue = source.getValue (property);
      Object targetValue = target.getValue (property);

      if (sourceValue instanceof IDataObject &&
          targetValue instanceof IDataObject)
      {
        merge ((IDataObject)sourceValue, (IDataObject)targetValue,
               orMode, equalise);
      } else
      {
        setValue (source, target, property, sourceValue, orMode);
      }
    }
  }

  private static void setValue (IDataObject source, IDataObject target,
      Object property, Object value, int orMode)
  {
    int mode =
      source.isTransient (property) ?
        IDataBean.TRANSIENT : IDataBean.PERSISTENT;

    if (target.getValue (property) != null && !target.isTransient (property))
      mode = IDataBean.PERSISTENT;

    mode |= orMode;

    target.setValue (property, value, mode);
  }

  /**
   * Shorcut to create a copy of the current property set of a data object.
   *
   * @param object The data object
   * @return A snapshot of the object's property set.
   *
   * @see IDataObject#propertyIterator()
   * @see IDataBean#getPropertyNames()
   * @see #valueSet(IDataObject)
   */
  public static Set<String> propertySet (IDataObject object)
  {
    HashSet set = new HashSet ();

    for (Iterator i = object.propertyIterator (); i.hasNext ();)
      set.add (i.next ());

    return set;
  }

  /**
   * Shorcut to create a copy of the current set of values of a data object.
   *
   * @param object The data object
   * @return A snapshot of the object's value set.
   *
   * @see IDataObject#propertyIterator()
   * @see IDataBean#getPropertyNames()
   * @see #propertySet(IDataObject)
   */
  public static Set valueSet (IDataObject object)
  {
    HashSet set = new HashSet ();

    for (Iterator i = object.propertyIterator (); i.hasNext ();)
      set.add (object.getValue (i.next ()));

    return set;
  }

  /**
   * Provides support for data object implementations in propagating child
   * events. Registers a child property value for event propagation if it is an
   * IDataBean (taking into account the semantics required by
   * {@link IDataObjectLink}). When a child's property changes, the
   * {@link IDataObjectChildListener#childPropertyChanged(Object, PropertyEvent)}
   * method on the parent is called. Data object implementations usually call
   * this when a new property value is added with setValue ().
   *
   * @param parent The parent data object.
   * @param property The property name of the child.
   * @param value The child property value.
   *
   * @see #unregisterChildValue(IDataObjectChildListener, Object)
   */
  public static void registerChildValue (IDataObjectChildListener parent,
                                         Object property, Object value)
  {
    // the value != this breaks simple cycles where an object is a
    // value of itself. more complex cycles are another question
    if (value == parent)
      return;

    IDataObject propertySource = resolvePropertySource (value);

    if (propertySource != null)
    {
      ChildListener listener = new ChildListener (parent, property);

      if (propertySource instanceof AbstractDataObject)
        ((AbstractDataObject)propertySource).addFirstPropertyListener (listener);
      else
        propertySource.addPropertyListener (listener);
    }
  }

  /**
   * Reverses the effect of
   * {@link #registerChildValue(IDataObjectChildListener, Object, Object)}.
   * Data object implementations usually call this when a a property value is
   * removed.
   *
   * @param parent The parent data object.
   * @param value The property value to remove.
   *
   * @see #registerChildValue(IDataObjectChildListener, Object, Object)
   */
  public static void unregisterChildValue (IDataObjectChildListener parent,
                                           Object value)
  {
    IDataObject propertySource = resolvePropertySource (value);

    if (propertySource != null)
    {
      for (Iterator i = propertySource.getPropertyListeners ().iterator ();
           i.hasNext ();)
      {
        Object l = i.next ();

        if (l instanceof ChildListener && ((ChildListener) l).parent == parent)
          propertySource.removePropertyListener ((ChildListener) l);
      }
    }
  }

  /**
   * Test if an object is a soft link (i.e. an {@link IDataObjectLink} with
   * {@link IDataObjectLink#isSoftLink()} returns true).
   */
  public static boolean isSoftLink (Object value)
  {
    return value instanceof IDataObjectLink &&
            ((IDataObjectLink) value).isSoftLink ();
  }

  /**
   * Returns value cast an IDataObject if the given value is both an IDataObject
   * and not a soft link. Used by registerChildValue() and
   * unregisterChildValue().
   */
  private static IDataObject resolvePropertySource (Object value)
  {
    if (value instanceof IDataObject)
    {
      if (!isSoftLink (value))
        return (IDataObject) value;
    }

    return null;
  }

  /**
   * Provides a string representation of an {@link IDataObjectLink} which can be
   * used by all its implementing classes. The representation is in the format:
   * <code>linkClassName(targetClassName:targetIdentityHashCode)</code>.
   *
   * @param link An {@link IDataObjectLink} object.
   * @return A String representation of the link object.
   */
  protected static String objectLinkToString (IDataObjectLink link)
  {
    String className = className (link.getClass ());
    String targetClassName = className (link.getLinkTarget ().getClass ());
    int hashCode = identityHashCode (link.getLinkTarget ());

    return format ("%s(%s:%d)", className, targetClassName, hashCode);
  }

  /**
   * Listener proxy for a child object that forwards the event to the parent's
   * childPropertyChanged() method.
   */
  private static class ChildListener implements PropertyListener
  {
    public IDataObjectChildListener parent;
    public Object property;

    public ChildListener (IDataObjectChildListener parent, Object property)
    {
      this.parent = parent;
      this.property = property;
    }

    public void propertyValueChanged (PropertyEvent e)
    {
      parent.childPropertyChanged (property, e);
    }
  }

  public static IDataObject make (Object... pairs)
  {
    IDataObject obj = new DataObject ();
    
    for (int i = 0; i < pairs.length - 1; i += 2)
      obj.setValue (pairs[i], pairs[i+1]);
    
    return obj;
  }
}
