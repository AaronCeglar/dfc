package dsto.dfc.databeans;

import dsto.dfc.util.Objects;

import dsto.dfc.text.StringUtility;

/**
 * Describes a path built of property names into a tree of IDataObject's,
 * similar in concept to a file system path.
 * 
 * @author mpp
 * @version $Revision: 1.20 $
 */
public class PropertyPath
{
  public static final PropertyPath EMPTY_PATH = new PropertyPath ();
  
  private Object [] path;
  
  /**
   * Create an empty path.
   * 
   * @see #EMPTY_PATH
   */
  public PropertyPath ()
  {
    this.path = new Object [0];
  }
  
  /**
   * Create path from a path expression such as "root/bean1/bean2/property".
   */
  public PropertyPath (String pathExpr)
  {
    this.path = StringUtility.split (pathExpr, "/");
  }
  
  public PropertyPath (Object path)
  {
    this.path = new Object [] {path};
  }
   
  /**
   * Create path from an array of property names.
   */
  public PropertyPath (Object [] path)
  {
    this.path = path;
  }

  /**
   * Follow the path to the last-but-one entry, starting with given root.
   * 
   * @param root A root data object.
   * @return The parent object of the property described by this path.
   * @throws IllegalArgumentException If the path is empty or one of the
   * properties in the path cannot be read (ie null).
   * @throws ClassCastException If any of the entries except the last is not 
   * an IDataObject.
   */
  public IDataObject followParent (IDataObject root)
    throws IllegalArgumentException, ClassCastException
  {
    if (path.length == 0)
      throw new IllegalArgumentException ("Path is empty");
      
    IDataObject object = root;    
    int end = path.length - 1;

    for (int i = 0; i < end; i++)
    {
      try
      {
        object = (IDataObject)object.getValue (path [i]);
      } catch (ClassCastException ex)
      {
        IllegalArgumentException ex2 =
          new IllegalArgumentException ("Found a non-IDataObject in path");
        
        ex2.initCause (ex);
        
        throw ex2;
      }
      
      if (object == null)
        throw new IllegalArgumentException ("No property named \"" + path [i] + "\" found");
    }
      
    return object;
  }
  
  /**
   * Follow this path into a given object and return the value. This
   * is equivalent to
   * {@link DataObjects#getValue(IDataObject, PropertyPath)}.
   * 
   * @see #followParent(IDataObject)
   */
  public Object follow (IDataObject object)
  {
    return DataObjects.getValue (object, this);
  }
  
  /**
   * Ensure this path exists into a given object. Any missing parts of
   * the path are filled in with empty  DataBean instances.
   * 
   * @param object The object to create the path in.
   * @return The last element in the path.
   * 
   * @throws ClassCastException if the last item on the path already
   *           exists and is not an IDataObject.
   */
  public IDataObject make (IDataObject object)
    throws ClassCastException
  {
    return make (object, IDataObject.PERSISTENT_OVERRIDE);
  }
  
  /**
   * Ensure this path exists into a given object. Any missing parts of
   * the path are filled in with empty DataBean instances.
   * 
   * @param object The object to create the path in.
   * @param mode The create mode for new path elements. One of
   *          IDataObject.TRANSIENT or IDataObject.PERSISTENT.
   *          
   * @return The last element in the path.
   * 
   * @throws ClassCastException if the last item on the path already
   *           exists and is not an IDataObject.
   */
  public IDataObject make (IDataObject object, int mode)
    throws ClassCastException
  {
    return (IDataObject)make (object, DataBean.class, mode);
  }

  /**
   * Ensure this path exists into a given object. Any missing parts of
   * the path are filled in with empty DataBean instances.
   * 
   * @param object The object to create the path in.
   * @param leafClass The class of the last object in the path. This
   *          is instantiated using the default constructor if it does
   *          not exist.
   * @return The last element in the path.
   * 
   * @throws ClassCastException if the last item on the path already
   *           exists and is not an IDataObject.
   * @throws IllegalArgumentException if leafClass cannot be
   *           instantiated.
   */
  public Object make (IDataObject object, Class leafClass)
    throws ClassCastException, IllegalArgumentException
  {
    return make (object, leafClass, IDataObject.PERSISTENT_OVERRIDE);
  }
  
  /**
   * Ensure this path exists into a given object. Any missing parts of
   * the path are filled in with empty DataBean instances.
   * 
   * @param object The object to create the path in.
   * @param leafClass The class of the last object in the path. This
   *          is instantiated using the default constructor if it does
   *          not exist.
   * @param mode The create mode for new path elements. One of
   *          IDataObject.TRANSIENT or IDataObject.PERSISTENT.
   *          
   * @return The last element in the path.
   * 
   * @throws ClassCastException if the last item on the path already
   *           exists and is not an IDataObject.
   * @throws IllegalArgumentException if leafClass cannot be
   *           instantiated.
   */
  public Object make (IDataObject object, Class leafClass, int mode)
    throws ClassCastException, IllegalArgumentException
  {
    Object current = object;
    
    for (int i = 0; i < path.length; i++)
    {
      Object value = ((IDataObject)current).getValue (path [i]);
      boolean lastElement = i == path.length - 1;
      
      if (value == null)
      {
        if (!lastElement || leafClass == DataBean.class)
          value = new DataBean ();
        else
          value = Objects.instantiate (leafClass);
        
        ((IDataObject)current).setValue (path [i], value, mode);
      }
      
      current = value;
    }
    
    return current;
  }
  
  public boolean isEmpty ()
  {
    return path.length == 0;
  }

  public int length ()
  {
    return path.length;
  }
  
  /**
   * Get the last component of the path.
   * 
   * @return The last path component.
   * 
   * @throws IllegalArgumentException If the path is empty.
   */
  public Object last ()
    throws IllegalArgumentException
  {
    if (path.length == 0)
      throw new IllegalArgumentException ("Path is empty");
      
    return path [path.length - 1];
  }
  
  /**
   * Get the first component of the path.
   * 
   * @return The first path component.
   * 
   * @throws IllegalArgumentException If the path is empty.
   * 
   * @see #first(int)
   */
  public Object first ()
    throws IllegalArgumentException
  {
    if (path.length == 0)
      throw new IllegalArgumentException ("Path is empty");
      
    return path [0];
  }
  
  /**
   * Get a the first x components of the path.
   * 
   * @param items The number of items to get.
   * @return The new path.
   * 
   * @throws IllegalArgumentException If the path is not long enough.
   * 
   * @see #first()
   * @see #tail(int)
   */
  public PropertyPath first (int items)
    throws IllegalArgumentException
  {
    if (items > path.length)
    {
      throw new IllegalArgumentException ("Cannot get first " + items + 
                                          " items from path length " + path.length);
    }
    
    Object [] newPath = new Object [items];
    
    System.arraycopy (path, 0, newPath, 0, items);
    
    return new PropertyPath (newPath);
  }
  
  public Object element (int index)
  {
    return path [index];
  }
  
  /**
   * Append a new element to the end of the path.
   * 
   * @param entry The new entry (may not be null).
   * @return The new path.
   */
  public PropertyPath append (Object entry)
  {
    if (entry == null)
      throw new IllegalArgumentException ("Entry cannot be null");
      
    Object [] newPath = new Object [path.length + 1];
    
    System.arraycopy (path, 0, newPath, 0, path.length);
    
    newPath [path.length] = entry;
    
    return new PropertyPath (newPath);
  }
  
  /**
   * Prepend a new element to the starting of the path.
   * 
   * @param entry The new entry (may not be null).
   * @return The new path.
   */
  public PropertyPath prepend (Object entry)
  {
    if (entry == null)
      throw new IllegalArgumentException ("Entry cannot be null");
      
    Object [] newPath = new Object [path.length + 1];
    
    System.arraycopy (path, 0, newPath, 1, path.length);
    
    newPath [0] = entry;
    
    return new PropertyPath (newPath);
  }
  
  /**
   * Create a new path from the last length () - 1 elements of this path. Path
   * length must be >= 2.
   * 
   * @see #parent()
   * @see #tail(int)
   */
  public PropertyPath tail ()
  {
    if (path.length < 2)
      throw new IllegalArgumentException
        ("Cannot get tail of path length " + path.length);
    
    Object [] newPath = new Object [path.length - 1];
    System.arraycopy (path, 1, newPath, 0, path.length - 1);

    return new PropertyPath (newPath);
  }
  
  /**
   * Create a new path from the last length () - chop elements of this path.
   * e.g. path.tail (3) is the same as path.tail ().tail ().tail ().
   * 
   * @see #parent()
   * @see #tail()
   * @see #first(int)
   */
  public PropertyPath tail (int chop)
  {
    if (path.length - chop <= 0)
      throw new IllegalArgumentException
        ("Cannot get tail " + chop + " of path length " + path.length);
    
    Object [] newPath = new Object [path.length - chop];
    System.arraycopy (path, chop, newPath, 0, path.length - chop);

    return new PropertyPath (newPath);
  }
  
  /**
   * Create a new path to the parent object of this path (ie the first length () -
   * 1 elements). Path length must be >= 2.
   * 
   * @see #tail()
   */
  public PropertyPath parent ()
  {
    if (path.length < 2)
      throw new IllegalArgumentException ("Path has no parent");
    
    Object [] newPath = new Object [path.length - 1];
    System.arraycopy (path, 0, newPath, 0, path.length - 1);

    return new PropertyPath (newPath);
  }
  
  /**
   * Test if this path is prefix of the given path. 
   * e.g "", "a", "a/b" and "a/b/c" are all prefixes of "a/b/c"
   * 
   * @param propertyPath The path to test.
   * @return True if this path is a prefix of propertyPath.
   */
  public boolean isPrefixOf (PropertyPath propertyPath)
  {
    if (path.length <= propertyPath.path.length)
    {
      for (int i = 0; i < path.length; i++)
      {
        if (!path [i].equals (propertyPath.path [i]))
          return false;
      }
      
      return true;
    }
    
    return false;
  }
  
  /**
   * Test if a given path expression is equivalent to this path.
   * 
   * @param pathExpr A path expression eg "bean1/bean2/property".
   * @return boolean True of pathExpr is equivalent.
   */
  public boolean isSamePath (String pathExpr)
  {
    if (path.length == 1)
    {
      if (pathExpr.indexOf ('/') == -1)
        return path [0].equals (pathExpr);
      else
        return false;
    } else
    {
      return equals (new PropertyPath (pathExpr));
    }
  }
  
  /**
   * Turn the path into a string expression such as "bean/bean2/property".
   */
  public String toString ()
  {
    if (path.length == 1)
      return path [0].toString ();
      
    StringBuilder str = new StringBuilder ();
    
    for (int i = 0; i < path.length; i++)
    {
      if (i != 0)
        str.append ('/');
        
      str.append (path [i].toString ());
    }
    
    return str.toString ();
  }
  
  /**
   * Shortcut to test whether path starts with a given property. Same
   * as path.first ().equals (property);
   */
  public boolean startsWith (Object property)
  {
    return first ().equals (property);
  }
  
  public int hashCode ()
  {
    int hash = 0;
    
    for (int i = 0; i < path.length; i++)
      hash ^= path [i].hashCode ();
      
    return hash;
  }

  /**
   * Test if two paths are the same.
   */
  public boolean equals (Object o)
  {
    if (o instanceof PropertyPath)
      return equals ((PropertyPath)o);
    else
      return false; 
  }
  
  public boolean equals (PropertyPath p)
  {
    if (p.path.length == path.length)
    {
      for (int i = 0; i < path.length; i++)
      {
        if (!path [i].equals (p.path [i]))
          return false;
      }
      
      return true;
    } else
    {
      return false;
    }
  }
}
