package dsto.dfc.databeans;

import java.util.Date;
import java.util.HashMap;

/**
 * The global registry of known data object types. Every type has a
 * unique logical name, which does NOT necessarily correspond to its
 * Java class name (eg "person", "key"). This class maintains a
 * mapping from logical type names to
 * {@link dsto.dfc.databeans.TypeInfo} objects that describe them.
 * <p>
 * 
 * This class also provides a mapping from Java class names to type
 * names, allowing type info to be easily attached to POJO-based data
 * objects (eg those derived from
 * {@link dsto.dfc.databeans.SimpleDataObject} or
 * {@link dsto.dfc.databeans.JavaDataBean}).
 * 
 * @see dsto.dfc.databeans.TypeInfo
 * 
 * @author Matthew Phillips
 */
public final class TypeRegistry
{
  private static HashMap typeToInfo;
  private static HashMap classToInfo;
  
  static
  {
    typeToInfo = new HashMap ();
    classToInfo = new HashMap ();
    
    register ("string", String.class);
    register ("int", Integer.class);
    register ("boolean", Boolean.class);
    register ("long", Long.class);
    register ("short", Short.class);
    register ("float", Float.class);
    register ("double", Double.class);
    register ("char", Character.class);
    register ("byte", Byte.class);
    register ("date", Date.class);
  }
  
  private TypeRegistry ()
  {
    // zip
  }
  
  /**
   * Shortcut to register type info against a Java class.
   * 
   * @param type The type name.
   * @param valueClass The value class.
   * 
   * @return The type info object created.
   * 
   * @see #register(String,TypeInfo)
   * @see #peekTypeInfo(Class)
   */
  public static TypeInfo register (String type, Class valueClass)
  {
    TypeInfo typeInfo = new TypeInfo (valueClass);
    
    register (type, typeInfo);
    
    classToInfo.put (valueClass.getName (), typeInfo);
    
    return typeInfo;
  }

  /**
   * Register type info.
   * 
   * @param info The new info.
   * 
   * @throws IllegalArgumentException if an attempt is made to
   *           register a different set of info for an existing type.
   * 
   * @see #peekTypeInfo(String)
   */
  public static void register (String name, TypeInfo info)
    throws IllegalArgumentException
  {
    TypeInfo oldInfo = (TypeInfo)typeToInfo.get (name);
    
    if (oldInfo != null && oldInfo != info)
      throw new IllegalArgumentException 
       ("Conflicting type info already registered for \"" + name + "\"");
    
    typeToInfo.put (name, info); 
  }
  
  /**
   * Peek at the type info (if any) for a given type.
   * 
   * @param typeName The name of the type.
   * @return The type info or null if not defined.
   * 
   * @see #getTypeInfo(String)
   */
  public static TypeInfo peekTypeInfo (String typeName)
  {
    return (TypeInfo)typeToInfo.get (typeName);
  }
  
  /**
   * Peek at the type info (if any) for a given type.
   * 
   * @param type The name of the type. 
   * @return The type info.
   * 
   * @see #peekTypeInfo(String)
   *
   * @throws IllegalArgumentException if type is not defined.
   */
  public static TypeInfo getTypeInfo (String type)
    throws IllegalArgumentException
  {
    TypeInfo info = (TypeInfo)typeToInfo.get (type);
    
    if (info == null)
      throw new IllegalArgumentException 
       ("No type info registered for \"" + type + "\"");
    
    return info;
  }

  /**
   * Shortcut to peek for type info associated with a class.
   */
  public static TypeInfo peekTypeInfo (Class classType)
  {
    return (TypeInfo)classToInfo.get (classType.getName ());
  }

  public static TypeInfo getPropertyInfo (String type, String property)
  {
    return getTypeInfo (type).getPropertyInfo (property);
  }
}
