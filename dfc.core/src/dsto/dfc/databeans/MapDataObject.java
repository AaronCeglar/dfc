package dsto.dfc.databeans;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import dsto.dfc.collections.BasicMonitoredMap;
import dsto.dfc.collections.CollectionEvent;
import dsto.dfc.collections.CollectionListener;
import dsto.dfc.util.EventListenerList;

/**
 * An extension of the DFC monitored map that supports access via the
 * IDataObject interface. Any IDataObject values in the map are
 * automatically monitored and the appropriate property events fired.
 * 
 * @author mpp
 * @version $Revision$
 */
public class MapDataObject
  extends BasicMonitoredMap
  implements IDataObject, IDataObjectChildListener, CollectionListener, Cloneable
{
  protected HashSet transientProperties;
  private transient boolean changingTransient;
  private EventListenerList listeners;
  
  public MapDataObject ()
  {
    this (new HashMap ());
  }
  
  public MapDataObject (Map map)
  {
    super (map);
    
    this.listeners = new EventListenerList ();
    
    addCollectionListener (this);
    
    if (!map.isEmpty ())
    {
      for (Iterator i = map.entrySet ().iterator (); i.hasNext (); )
        register ((Map.Entry)i.next ());
    }
  }
  
  public TypeInfo getTypeInfo ()
  {
    return null;
  }
  
  public Collection getPropertyListeners ()
  {
    return listeners.getListeners ();
  }

  protected void register (Map.Entry entry)
  {
    if (entry.getValue () instanceof IDataObject)
      DataObjects.registerChildValue (this, entry.getKey (), entry.getValue ());
  }
  
  protected void unregister (Map.Entry entry)
  {
    DataObjects.unregisterChildValue (this, entry.getValue ()); 
  }
  
  // IDataObject interface
  
  public IDataObject shallowClone () throws UnsupportedOperationException
  {
    try
    {
      MapDataObject copy = (MapDataObject)emptyClone ();
      
      copy.listeners = new EventListenerList ();
      
      if (transientProperties != null)
        copy.transientProperties = new HashSet (transientProperties);

      copy.addCollectionListener (copy);
      
      return copy;    
    } catch (CloneNotSupportedException ex)
    {
      throw new UnsupportedOperationException ("Cannot clone: " + ex);
    }
  }

  public void setValue (Object name, Object value)
  {
    setValue (name, value, PERSISTENT_OVERRIDE);
  }
  
  public void setValue (Object name, Object value, int mode)
  {
    if (name == null)
      throw new IllegalArgumentException ("Key cannot be null");

    // null op if no override and value already set
    if ((mode & OVERRIDE) == 0 && containsKey (name))
      return;
      
    if ((mode & TRANSIENT) != 0)
    {
      if (transientProperties == null)
        transientProperties = new HashSet ();

      if (value != null)
        transientProperties.add (name);
      
      changingTransient = true;
    }
    
    try
    {
      if (value != null)
        put (name, value);
      else
        remove (name);
    } finally
    {
      changingTransient = false;
    }
  }

  public Object getValue (Object name)
  {
    return get (name);
  }

  public boolean isTransient (Object name)
  {
    return transientProperties != null && transientProperties.contains (name);
  }

  public Iterator propertyIterator ()
  {
    return keySet ().iterator ();
  }
  
  public void addPropertyListener (PropertyListener l)
  {
    listeners.addListener (l);
  }

  public void removePropertyListener (PropertyListener l)
  {
    listeners.removeListener (l);
  }

  // CollectionListener interface
  
  public void elementsAdded (CollectionEvent e)
  {
    for (Iterator i = e.getElements ().iterator (); i.hasNext (); )
    {
      Map.Entry entry = (Map.Entry)i.next ();
      
      register (entry);
      
      if (!changingTransient && transientProperties != null)
        transientProperties.remove (entry.getKey ());
      
      firePropertyChangedEvent (entry.getKey (), null, entry.getValue (),
                                isTransient (entry.getKey ()));
    }
  }

  public void elementsRemoved (CollectionEvent e)
  {
    for (Iterator i = e.getElements ().iterator (); i.hasNext (); )
    {
      Map.Entry entry = (Map.Entry)i.next ();
      
      unregister (entry);
      
      boolean wasTransient = false;
      
      if (transientProperties != null)
        wasTransient = transientProperties.remove (entry.getKey ());
      
      firePropertyChangedEvent (entry.getKey (), entry.getValue (), null,
                                wasTransient);
    }
  }
  
  public void childPropertyChanged (Object childProperty, PropertyEvent e)
  {
    if (transientProperties != null && !e.transientProperty)
      transientProperties.remove (childProperty);
          
    firePropertyChangedEvent (childProperty, e.path,
                              e.oldValue, e.newValue,
                              e.transientProperty);
  }

  protected void firePropertyChangedEvent (Object property,
                                           Object oldValue,
                                           Object newValue,
                                           boolean transientProperty)
  {
    if (listeners.hasListeners ())
    {
      PropertyEvent e =
        new PropertyEvent (this, new PropertyPath (property),
                           oldValue, newValue, transientProperty);
      
      listeners.fireEvent (PropertyListener.class, "propertyValueChanged", e);
    }
  }

  protected void firePropertyChangedEvent (Object property,
                                           PropertyPath basePath,
                                           Object oldValue,
                                           Object newValue,
                                           boolean transientProperty)
  {
    if (listeners.hasListeners ())
    {
      PropertyEvent e = new PropertyEvent (this, basePath.prepend (property),
                                           oldValue, newValue,
                                           transientProperty);
      
      listeners.fireEvent (PropertyListener.class, "propertyValueChanged", e);
    }
  }
}
