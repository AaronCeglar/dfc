package dsto.dfc.databeans;

/**
 * Interface for view filters.
 * 
 * @see DataObjectView
 */
public interface IFilter<T>
{
  /**
   * A filter that does nothing.
   */
  public static final IFilter NULL_FILTER = new IFilter ()
  {
    public boolean include (Object value)
    {
      return true;
    }
  };

  /**
   * Return true if a given top-level value from source should be included.
   */
  public boolean include (T value);
}