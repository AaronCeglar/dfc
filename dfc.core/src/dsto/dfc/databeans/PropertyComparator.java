package dsto.dfc.databeans;

import java.util.Comparator;

import dsto.dfc.util.DefaultComparator;

/**
 * Compares two IDataObject's using the value of one of their properties.
 * 
 * @author Matthew Phillips
 */
public class PropertyComparator implements Comparator<IDataObject>
{
  private final String property;
  private final Comparator valueComparator;

  /**
   * Create a new instance using default compare order for properties (all
   * values must be Comparable).
   * 
   * @param property The property name.
   */
  public PropertyComparator (String property)
  {
    this (property, DefaultComparator.instance ());
  }

  /**
   * Create a new instance.
   * 
   * @param property The property name.
   * @param valueComparator The comparator to use to compare the property
   *          values.
   */
  public PropertyComparator (String property, Comparator valueComparator)
  {
    this.property = property;
    this.valueComparator = valueComparator;
  }

  public int compare (IDataObject v1, IDataObject v2)
  {
    if (v1 == v2)
      return 0;
    else if (v1 == null)
      return -1;
    else if (v2 == null)
      return 1;
    else
      return valueComparator.compare (v1.getValue (property), 
                                      v2.getValue (property));
  }
  
  public boolean equals (Object o)
  {
    return o instanceof PropertyComparator && equals ((PropertyComparator)o);
  }
  
  public boolean equals (PropertyComparator o)
  {
    return o.property.equals (property) && o.valueComparator.equals (valueComparator);
  }
  
  public int hashCode ()
  {
    return property.hashCode () ^ valueComparator.hashCode ();
  }
} 