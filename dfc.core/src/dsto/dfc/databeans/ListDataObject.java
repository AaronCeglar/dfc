package dsto.dfc.databeans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import dsto.dfc.util.EventListenerList;
import dsto.dfc.util.Objects;

import dsto.dfc.collections.CollectionEvent;
import dsto.dfc.collections.DfcMonitoredList;
import dsto.dfc.collections.DfcMonitoredListListener;
import dsto.dfc.collections.NumberIterator;

/**
 * An extension of the DFC monitored list that supports access via the
 * IDataObject interface. Any IDataObject's in the list are
 * automatically monitored and the appropriate property events fired.
 * 
 * @author mpp
 * @version $Revision$
 */
public class ListDataObject
  extends DfcMonitoredList
  implements IDataObject, IDataObjectChildListener,
               DfcMonitoredListListener
{
  private EventListenerList listeners;
  
  public ListDataObject ()
  {
    this (new ArrayList ());
  }
  
  public ListDataObject (List list)
  {
    super (list);
    
    this.listeners = new EventListenerList ();
    
    addCollectionListener (this);
    
    for (int i = 0; i < list.size (); i++)
      registerValue (list.get (i));
  }
  
  public Object clone () throws CloneNotSupportedException
  {
    ListDataObject copy = (ListDataObject)super.clone ();
    
    copy.listeners = new EventListenerList ();
    copy.addCollectionListener (copy);
    
    return copy;
  }
  
  public TypeInfo getTypeInfo ()
  {
    return null;
  }

  public Collection getPropertyListeners ()
  {
    return listeners.getListeners ();
  }

  protected void registerValue (Object value)
  {
    DataObjects.registerChildValue (this, null, value);
  }
  
  protected void unregisterValue (Object value)
  {
    DataObjects.unregisterChildValue (this, value);
  }
  
  // IDataObject interface
  
  public IDataObject shallowClone () throws UnsupportedOperationException
  {
    try
    {
      ListDataObject copy = (ListDataObject)super.emptyClone ();
      
      copy.listeners = new EventListenerList ();
      copy.addCollectionListener (copy);
      
      return copy;    
    } catch (CloneNotSupportedException ex)
    {
      throw new UnsupportedOperationException ("Cannot clone: " + ex);
    }
  }

  public void setValue (Object name, Object value)
  {
    setValue (name, value, PERSISTENT_OVERRIDE);
  }
  
  public void setValue (Object name, Object value, int mode)
  {
    if ((mode & TRANSIENT) != 0)
      throw new IllegalArgumentException ("Transient properties not supported");
    
    try
    {
      int index = nameToIndex (name);
      
      if (value == null)
      {
        // if removing a defined property
        if (index < size ())
          remove (index);
      } else
      {
        if (index < size ())
        {
          if ((mode & OVERRIDE) != 0 || get (index) == null)
            set (index, value);
        } else
        {
          add (index, value);
        }
      }
    } catch (IndexOutOfBoundsException ex)
    {
      IllegalArgumentException ex2 =
        new IllegalArgumentException ("Invalid property " + name);
      
      ex2.initCause (ex);
      
      throw ex2;
    } catch (NumberFormatException ex)
    {
      IllegalArgumentException ex2 =
        new IllegalArgumentException ("List property must be a number " + name);
      
      ex2.initCause (ex);
      
      throw ex2;
    }
  }

  public Object getValue (Object name)
  {
    try
    {
      return get (nameToIndex (name));
    } catch (IndexOutOfBoundsException ex)
    {
      // IDataObject's should return null for undefined properties
      return null;
    } catch (NumberFormatException ex)
    {
      /*
       * nameToIndex () failed i.e. name is neither and Integer or a
       * can be parsed as one. Report as undefined property.
       */
      
      return null;
    }
  }

  public boolean isTransient (Object name)
  {
    return false;
  }
  
  /**
   * Translate a property name into an index into the list. Accepts
   * either Integer's or objects whose toString () value parses as an
   * integer.
   * 
   * @param name The property name.
   * @return The index into the list.
   * 
   * @throws NumberFormatException if name is neither an Integer or a
   *           string that parses with
   *           {@link Integer#parseInt(java.lang.String)}.
   */
  protected int nameToIndex (Object name)
    throws NumberFormatException
  {
    // handle either Integer or String
    if (name instanceof Integer)
      return ((Integer)name).intValue ();
    else
      return Integer.parseInt (name.toString ());
  }

  public Iterator propertyIterator ()
  {
    return new NumberIterator (0, size ());
  }
  
  public void addPropertyListener (PropertyListener l)
  {
    listeners.addListener (l);
  }

  public void removePropertyListener (PropertyListener l)
  {
    listeners.removeListener (l);
  }

  // List interface
  
  /**
   * Override set () to generate a single property change event rather than
   * two (old -> null, null -> new).
   */
  public Object set (int index, Object newValue)
  {
    Object oldValue;

    removeCollectionListener (this);
    
    try
    {
      oldValue = super.set (index, newValue);
    } finally
    {
      // ensure listener is re-added regardless of how we exit
      addCollectionListener (this);
    }

    unregisterValue (oldValue);
    registerValue (newValue);
    
    firePropertyChangedEvent (index, oldValue, newValue, false);

    return oldValue;
  }

  // CollectionListener interface
  
  public void elementsAdded (CollectionEvent e)
  {
    int index = e.getStartIndex ();
    
    for (Iterator i = e.getElements ().iterator (); i.hasNext (); index++)
    {
      Object element = i.next ();
      
      registerValue (element);
      
      firePropertyChangedEvent (index, null, element, false);
    }
  }

  public void elementsRemoved (CollectionEvent e)
  {
    int index = e.getStartIndex ();
    
    for (Iterator i = e.getElements ().iterator (); i.hasNext (); index++)
    {
      Object element = i.next ();
      
      unregisterValue (element);
      
      firePropertyChangedEvent (index, element, null, false);
    }
  }
  
  public void elementMoved (CollectionEvent e)
  {
    Object element = e.iterator ().next ();
    
    // simulate a remove/add
    firePropertyChangedEvent (e.getStartIndex (), element, null, false);
    firePropertyChangedEvent (e.getEndIndex (), null, element, false);
  }
  
  public void childPropertyChanged (Object childProperty, PropertyEvent e)
  {
    firePropertyChangedEvent (new Integer (indexOf (e.getSource ())),
                              e.path,
                              e.oldValue, e.newValue,
                              e.transientProperty);
  }
  
  protected void firePropertyChangedEvent (int index,
                                           Object oldValue,
                                           Object newValue,
                                           boolean transientProperty)
  {
    if (listeners.hasListeners () &&
        !Objects.objectsEqualEx (oldValue, newValue))
    {
      PropertyEvent e =
        new PropertyEvent (this, new PropertyPath (new Integer (index)),
                           oldValue, newValue, transientProperty);
      
      listeners.fireEvent (PropertyListener.class, "propertyValueChanged", e);
    }
  }

  protected void firePropertyChangedEvent (Object property,
                                           PropertyPath basePath,
                                           Object oldValue,
                                           Object newValue,
                                           boolean transientProperty)
  {
    if (listeners.hasListeners () &&
        !Objects.objectsEqualEx (oldValue, newValue))
    {
      PropertyEvent e = new PropertyEvent (this, basePath.prepend (property),
                                           oldValue, newValue,
                                           transientProperty);
      
      listeners.fireEvent (PropertyListener.class, "propertyValueChanged", e);
    }
  }   
}
