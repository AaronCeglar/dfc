package dsto.dfc.databeans;

/**
 * Interface for data objects that listen for child property changes.
 * See
 * {@link dsto.dfc.databeans.DataObjects#registerChildValue(IDataObjectChildListener, Object, Object)}
 * for a use of this.
 * 
 * @author Matthew Phillips
 */
public interface IDataObjectChildListener extends IDataObject
{
  /**
   * Called when a child data object's property changes.
   * 
   * @param childProperty The property the child is registered under.
   * @param e The property change event emitted by the child.
   */
  public void childPropertyChanged (Object childProperty, PropertyEvent e);
}
