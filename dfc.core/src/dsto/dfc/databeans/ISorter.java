package dsto.dfc.databeans;

import java.util.Comparator;

import dsto.dfc.util.DefaultComparator;

/**
 * Interface for view sorters.
 * 
 * @see DataObjectView
 */
public interface ISorter<T> extends Comparator<T>
{
  /**
   * A sorter that does nothing.
   */
  public static final ISorter NULL_SORTER = new ISorter ()
  {
    public boolean affectsOrder (PropertyPath path)
    {
      return false;
    }
    
    public int compare (Object o1, Object o2)
    {
      return o1.hashCode ()  - o2.hashCode ();
    }
  };
  /**
   * A sorter that delegates to {@link DefaultComparator}.
   */
  public static final IFilterSorter DEFAULT_SORTER = new IFilterSorter ()
  {
    public boolean include (Object value)
    {
      return true;
    }
  
    public boolean affectsOrder (PropertyPath path)
    {
      return false;
    }
  
    public int compare (Object o1, Object o2)
    {
      return DefaultComparator.instance ().compare (o1, o2);
    }
  };

  /**
   * See {@link Comparator#compare(Object, Object)}.
   */
  public int compare (T o1, T o2);

  /**
   * Return true if a change to a given path might affect the sort
   * order of the object at the root of the path. This is an
   * opportunity for optimization: simply returning true will work
   * at the cost of some wasted ovehead.
   */
  public boolean affectsOrder (PropertyPath path);
}