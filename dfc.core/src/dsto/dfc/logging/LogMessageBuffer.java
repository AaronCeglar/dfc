package dsto.dfc.logging;

import java.io.PrintWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import dsto.dfc.collections.BasicCollectionEventSource;

/**
 * Stores log messages emitted by the system log.
 *
 * @version $Revision$
 */
public class LogMessageBuffer
  extends BasicCollectionEventSource implements LogListener
{
  private int maxEvents = 1000;
  
  private ArrayList events = new ArrayList ();
  
  protected LogFilter filter;

  public LogMessageBuffer ()
  {
    this.filter = Log.getFilter ();

    Log.addLogListener (this);
  }
  
  public LogMessageBuffer (int maxEvents)
  {
    this ();

    this.maxEvents = maxEvents;
  }

  public void dispose ()
  {
    Log.removeLogListener (this);
  }
  
  public void disableAll ()
  {
    if (filter == Log.getFilter ())
      filter = new LogFilter (filter);
    
    filter.disableAll ();
  }
  
  public void enableAll ()
  {
    if (filter == Log.getFilter ())
      filter = new LogFilter (filter);
    
    filter.enableAll ();
  }
  
  public void setEnabled (int type, boolean enabled)
  {
    if (filter == Log.getFilter ())
      filter = new LogFilter (filter);
    
    filter.setEnabled (type, enabled);
  }
  
  public int getMaxEvents ()
  {
    return maxEvents;
  }

  /**
   * Set the maximum number of events that are stored in the buffer.
   * After this limit is reached, events are expired on a FIFO basis.
   */
  public void setMaxEvents (int newValue)
  {
    maxEvents = newValue;

    expireEvents ();
  }

  public void clear ()
  {
    if (events.size () > 0)
    {
      ArrayList oldEvents = events;
      events = new ArrayList ();

      fireElementsRemoved (oldEvents, 0, oldEvents.size () - 1);
    }
  }

  public int getEventCount ()
  {
    return events.size ();
  }

  public LogEvent getEvent (int index)
  {
    return (LogEvent)events.get (index);
  }

  public int indexOfEvent (LogEvent event)
  {
    return events.indexOf (event);
  }
  
  public Iterator iterator ()
  {
    return new ArrayList (events).iterator ();
  }

  public Object [] toArray ()
  {
    return new ArrayList (events).toArray ();
  }
  
  /**
   * Add an event to the buffer.
   */
  public void add (LogEvent event)
  {
    expireEvents ();

    int index = events.size ();

    events.add (event);

    fireElementAdded (event, index);
  }
  
  /**
   * Add an event to the specified index of buffer.
   */
  public void add (LogEvent event, int index)
  {
    expireEvents ();

    events.add (index, event);

    fireElementAdded (event, index);
  }
  
  /**
   * Remove an event from the buffer.
   */
  public void remove (LogEvent event)
  {
    int index = events.indexOf (event);

    events.remove (event);
    
    fireElementRemoved (event, index);
    
  }
  
  /**
   * Remove an event at the specified index from buffer.
   */
  public void remove (int index)
  {

    LogEvent oldEvent = (LogEvent) events.get (index);
    
    events.remove (index);

    fireElementRemoved (oldEvent, index);
  }

  /**
   * Write the contents of the buffer.
   *
   * @see #write(PrintWriter)
   */
  public void write (Writer writer)
    throws SecurityException
  {
    PrintWriter printWriter = new PrintWriter (writer);

    write (printWriter);
    
    printWriter.flush ();
  }

  /**
   * Write the contents of the buffer.
   */
  public void write (PrintWriter printWriter)
  {
    int numOfLogEvent = getEventCount ();
    DateFormat dateFormat = DateFormat.getTimeInstance ();
    
    for (int i = 0; i < numOfLogEvent; i++)
    {
      LogEvent event = getEvent (i);
      int type = event.getType ();
      String message = event.getMessage ();
      Throwable exception = event.getException ();
	  	
      printWriter.print (dateFormat.format (event.getTime ()));
      printWriter.print (" [" + Log.getTypeString (type) + "] ");
      printWriter.print (message);

      if (exception != null)
      {
        printWriter.println(": " + exception.getMessage ());
        printWriter.println("Exception trace:");
        exception.printStackTrace (printWriter);
      }
      
      printWriter.println ();
    }
  }

  /**
   * Expire any excess events from the buffer so that a new event may
   * be added.
   */
  protected void expireEvents ()
  {
    synchronized (events)
    {
      if (events.size () >= maxEvents)
      {
        int expiredCount = events.size () - maxEvents + 1;
        ArrayList expiredEvents = new ArrayList (
            events.subList (0, expiredCount));

        for (int i = 0; i < expiredCount; i++)
          events.remove (0);

        fireElementsRemoved (expiredEvents, 0, expiredCount - 1);
      }
    }
  }

  public void messageReceived (LogEvent e)
  {
    if (filter.select (e))
      add (e);
  }  
}
