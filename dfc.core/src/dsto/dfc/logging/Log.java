package dsto.dfc.logging;

import static java.lang.Integer.parseInt;

import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

/**
 * A basic application log. Log events by default go to console, but
 * can be redirected via listeners and adapters.
 * <p>
 *
 * <h2>Filtering</h2>
 *
 * Events sent to the log are automatically printed to the console.
 * This can be controlled by changing the filter obtained via
 * {@link #getFilter()} or using the shortcuts such as
 * {@link #enableAll()}.
 * <p>
 *
 * The initial set of enabled console events can also be controlled
 * via the system properties <code>dfc.log.console.show</code> and
 * <code>dfc.log.console.hide</code>. For example, to show only
 * WARNING and ALARM events, you could use
 * "-Ddfc.log.console.show=WARNING,ALARM" on the command line.
 * <p>
 *
 * Example usage:
 *
 * <pre>
 *
 *      Log.info (&quot;Starting application&quot;, this);
 *
 *      try
 *      {
 *        ...
 *      } catch (Exception ex)
 *      {
 *        Log.alarm (&quot;Unexpected exception&quot;, this, ex);
 *      }
 *
 * </pre>
 *
 * @see dsto.dfc.logging.FileLogger
 * @see dsto.dfc.logging.LogListener
 * @see dsto.dfc.logging.LogMessageBuffer
 *
 * @version $Revision: 1.49 $
 */
public final class Log
{
  public static final int TRACE = 0;
  public static final int INFO = 1;
  public static final int WARNING = 2;
  public static final int ALARM = 3;
  public static final int PERFORMANCE = 4;
  public static final int DIAGNOSTIC = 5;
  public static final int SYSTEM_EVENT = 6;
  public static final int INTERNAL_ERROR = 7;

  // messages will be truncated down to the specified size
  private static int truncateSize;

  private static final String [] TYPE_NAMES = new String []{"Trace",
      "Info", "Warning", "Alarm", "Performance", "Diagnostic",
      "System Event", "Internal Error"};

  private static final String [] TYPE_STRINGS = new String []{
      "TRACE", "INFO", "WARNING", "ALARM", "PERFORMANCE",
      "DIAGNOSTIC", "SYSTEM_EVENT", "INTERNAL_ERROR"};

  private static final LogFilter filter;
  private static DateFormat DATE_FORMAT = DateFormat.getTimeInstance ();

  private static PrintWriter infoOutput;
  private static PrintWriter errorOutput;
  private static Vector logListeners;
  private static String applicationName = null;

  static
  {
    filter = new LogFilter ();

    filter.enableAll ();

    readLogFilterSystemProperty ();

    infoOutput = new PrintWriter (System.out);
    errorOutput = new PrintWriter (System.err);
  }

  private Log ()
  {
    // zip
  }

  /**
   * Read the dfc.log.console.show and dfc.log.console.hide system
   * properties and update the filter settings. This is called on
   * class load, but clients may call this if the system properties
   * have changed.
   */
  public static void readLogFilterSystemProperty ()
  {
    String show = System.getProperty ("dfc.log.console.show");
    String hide = System.getProperty ("dfc.log.console.hide");

    truncateSize =
      parseInt (System.getProperty ("dfc.log.console.truncatesize", "6000"));

    if (show != null)
    {
      try
      {
        filter.disableAll ();
        filter.setEnabled (show, true);
      } catch (IllegalArgumentException ex)
      {
        filter.enableAll ();

        Log.warn ("Error in dfc.log.console.show system property (\""
            + show + "\"): " + ex.getMessage (), Log.class);
      } catch (Error ex)
      {
        // ensure log events get shown on serious error
        filter.enableAll ();

        throw ex;
      }
    }

    if (hide != null)
    {
      try
      {
        filter.setEnabled (hide, false);
      } catch (IllegalArgumentException ex)
      {
        Log.warn ("Error in dfc.log.console.hide system property (\""
            + hide + "\"): " + ex.getMessage (), Log.class);
      }
    }
  }

  /**
   * Set the user-readable application name. If set, this is prefixed
   * to all log events sent to the console to identify the
   * application's output.
   */
  public static void setApplicationName (String newValue)
  {
    applicationName = newValue;
  }

  /**
   * Get the application name, or null of none set.
   *
   * @see #setApplicationName(String)
   */
  public static String applicationName ()
  {
    return applicationName;
  }
    
  public static void setDateFormat (DateFormat df)
  {
    DATE_FORMAT = df;
  }
  
  /**
   * Enable redirection of unhandled exceptions in the AWT event
   * thread to the log.
   */
  public static void enableAwtErrorLogging ()
  {
    System.setProperty ("sun.awt.exception.handler",
                        "dsto.dfc.logging.AwtEventThreadExceptionLogger");
  }

  /**
   * Sets both the info and error output streams that log messages are
   * echoed to. If null, no echo is done.
   *
   * @see #setInfoOutput(PrintWriter)
   * @see #setErrorOutput(PrintWriter)
   */
  public static void setOutput (PrintWriter newOutput)
  {
    infoOutput = newOutput;
    errorOutput = newOutput;
  }

  /**
   * Set the stream that info (non-error) messages are echoed to.
   *
   * @see #setErrorOutput(PrintWriter)
   */
  public static void setInfoOutput (PrintWriter newValue)
  {
    infoOutput = newValue;
  }

  public static PrintWriter getInfoOutput ()
  {
    return infoOutput;
  }

  /**
   * Set the stream that error messages are echoed to.
   *
   * @see #setInfoOutput(PrintWriter)
   */
  public static void setErrorOutput (PrintWriter newValue)
  {
    errorOutput = newValue;
  }

  public static PrintWriter getErrorOutput ()
  {
    return errorOutput;
  }

  /**
   * Get the filter for sending log events to the console.
   */
  public static LogFilter getFilter ()
  {
    return filter;
  }

  /**
   * Shortcut to set filtering for sending log events to the console.
   */
  public static void setEnabled (int type, boolean enabled)
  {
    filter.setEnabled (type, enabled);
  }

  public static boolean isEnabled (int type)
  {
    return filter.select (type);
  }

  /**
   * Shortcut for disabling printing all log events from the console.
   */
  public static void disableAll ()
  {
    filter.disableAll ();
  }

  /**
   * Shortcut for enabling printing all log events to the console.
   */
  public static void enableAll ()
  {
    filter.enableAll ();
  }

  /**
   * Generate a string from a date formatted in the same way as the
   * logger.
   */
  public static String getDateString (Date time)
  {
    return DATE_FORMAT.format (time);
  }

  /**
   * Get a string for a given log message type (INFO, WARNING, etc).
   * This is a human-readable string, se also {@link #typeToString}.
   */
  public static String getTypeString (int type)
  {
    return TYPE_NAMES [type];
  }

  /**
   * Get the type code for a log entry type.
   *
   * @param typeName One of "TRACE", "INFO", "WARNING", "ALARM",
   *                "PERFORMANCE", "DIAGNOSTIC", "SYSTEM_EVENT",
   *                "INTERNAL_ERROR"
   * @return The log entry type "WARN" yields Log.WARN.
   *
   * @throws IllegalArgumentException if typeName is not valid.
   *
   * @see #typeToString(int)
   */
  public static int stringToType (String typeName)
    throws IllegalArgumentException
  {
    for (int i = 0; i < TYPE_STRINGS.length; i++)
    {
      if (TYPE_STRINGS [i].equalsIgnoreCase (typeName))
        return i;
    }

    throw new IllegalArgumentException ("\"" + typeName
        + "\" is not a valid log entry type");
  }

  public static String typeToString (int type)
  {
    return TYPE_STRINGS [type];
  }

  /**
   * Log an existing log event. Note: this currently rewrites the
   * event's time to the current time (this is likely to change at
   * some point, do not rely on it).
   */
  public static void add (LogEvent event)
  {
    add (event.getType (), event.getMessage (), event.getSource (),
         event.getException ());
  }

  /**
   * Add a message to the log.
   *
   * @param type The type of message: TRACE, INFO, WARNING, ALARM, etc.
   * @param messageStr The message to add to the log.
   * @param source The source of the message.
   */
  public static void add (int type, String messageStr, Object source)
  {
    add (type, messageStr, source, null);
  }

  /**
   * Add a message to the log.
   *
   * The message is printed out to the console. If the message is too
   * long and the truncate flag is turned on, the message is truncated
   * before being be printed.
   *
   * @param type The type of message: TRACE, INFO, WARNING, ALARM, etc.
   * @param messageStr The message to add to the log.
   * @param source The source of the message.
   * @param exception The exception that relates to the message.
   */
  public static void add (int type, String messageStr, Object source,
                          Throwable exception)
  {
    Date time = Calendar.getInstance ().getTime ();
    PrintWriter output =
      (type == ALARM || type == INTERNAL_ERROR || type == WARNING) ?
        errorOutput : infoOutput;

    if (output != null && filter.select (type))
    {
      printMessage (output, time, DATE_FORMAT, type, messageStr, truncateSize);

      if (exception != null)
      {
        int msgLength = (messageStr != null ? messageStr.length () : 0);
        printException (output, exception, truncateSize - msgLength);
      }

      output.flush ();
    }

    if (logListeners != null && logListeners.size () > 0)
    {
      // use class as source rather than instance to avoid holding a
      // reference to an object that might otherwise be GC'd
      Class sourceClass =
        (source instanceof Class) ? (Class)source : source.getClass ();

      fireMessageReceived (new LogEvent (sourceClass, type,
          messageStr, time, exception));
    }
  }

  public static boolean printMessage (PrintWriter str, Date time, int type,
                                      String messageStr)
  {
    return printMessage (str, time, DATE_FORMAT, type, messageStr, truncateSize);
  }

  public static boolean printMessage (PrintWriter str, Date time,
                                      DateFormat dateFormat, int type,
                                      String messageStr)
  {
    return printMessage (str, time, dateFormat, type, messageStr, truncateSize);
  }

  /**
   * Prints a message to the supplied PrintWriter instance. The message is
   * truncated if it exceeds maxLength
   *
   * @param logWriter The writer to print the message to.
   * @param time The timestamp of the message.
   * @param dateFormat The date format to use.
   * @param type The type of message (log level).
   * @param messageStr The content of the message.
   * @param maxLength The maximum length the message can be.
   * @return False if the message was truncated, true otherwise.
   */
  public static boolean printMessage (PrintWriter logWriter, Date time,
                                      DateFormat dateFormat, int type,
                                      String messageStr, int maxLength)
  {
    StringBuilder strBuilder = new StringBuilder (100);
    buildAppName (strBuilder, time, dateFormat);

    strBuilder.append (" ");

    boolean truncated =
      !buildMsg (strBuilder, messageStr, TYPE_NAMES[type], maxLength);
    logWriter.println (strBuilder);
    return !truncated;
  }

  private static void buildAppName (StringBuilder strBuilder, Date time,
                                    DateFormat dateFormat)
  {
    strBuilder.append (dateFormat.format (time));

    if (applicationName != null)
      strBuilder.append (": ").append (applicationName);
  }

  private static boolean buildMsg (StringBuilder strBuilder, String msg,
                                   String type, int maxLength)
  {
    // truncate message if necessary.
    boolean truncated = msg.length () > maxLength;
    String outputedMsg = truncated ? msg.substring (0, maxLength - 1)
        + "... Message too long, TRUNCATED" : msg;

    strBuilder.append ("[").append (type);
    strBuilder.append ("] ").append (outputedMsg);
    return !truncated;
  }

  private static boolean buildExceptionMsg (StringBuilder strBuilder,
                                            String msg, int maxLength)
  {
    boolean truncated = false;
    String outputedMsg = msg;
    if (msg != null)
    {
      truncated = msg.length () > maxLength;
      outputedMsg = truncated ? msg.substring (0, maxLength - 1)
            + "... Exception message too long, TRUNCATED" : msg;
    }
    strBuilder.append (outputedMsg);
    return !truncated;
  }

  /**
   * Generate a string from a log event, minus the application and
   * date info.
   */
  public static String toLogString (LogEvent event)
  {
    StringBuilder str = new StringBuilder ();
    String msg = event.getMessage ();
    buildMsg (str, msg, TYPE_NAMES[event.getType ()], truncateSize);

    // append exception message when present.
    Throwable exception = event.getException ();
    if (exception != null)
    {
      String exceptionMsg = exception.getMessage ();
      if (exceptionMsg != null)
      {
        str.append (" : ");
        buildExceptionMsg (str, exceptionMsg, truncateSize - msg.length ());
      }
    }

    return str.toString ();
  }

  public static void printException (PrintWriter str, Throwable exception)
  {
    printException (str, exception, truncateSize);
  }

  private static void printException (PrintWriter str, Throwable exception,
                                      int maxLength)
  {
    printExceptionMessage (str, exception, maxLength);

    printExceptionTrace (str, exception, maxLength);
  }

  public static boolean printExceptionMessage (PrintWriter str,
                                               Throwable exception,
                                               int maxLength)
  {
    StringBuilder strBuilder = new StringBuilder ();
    strBuilder.append (exception.getClass ().getName ()).append (": ");
    boolean truncated  = !buildExceptionMsg (strBuilder, exception.getMessage (), maxLength);
    str.println (strBuilder);
    return !truncated;
  }


  /**
   * Prints the exception stack trace. Note the stack trace is not truncated to
   * preserve diagnostic information.
   *
   * @param str The output character stream.
   * @param exception
   * @param maxLength Max length of nested exception message, if any.
   */
  public static void printExceptionTrace (PrintWriter str, Throwable exception,
                                          int maxLength)
  {
    // print the stack.
    for (StackTraceElement element : exception.getStackTrace ())
      str.println ("\tat " + element.toString ());

    Throwable cause = exception.getCause ();

    if (cause != null)
    {
      str.println ("--------------------");
      str.println ("Nested exception:");
      printException (str, cause, maxLength);
    }
  }

  /**
   * A trace is used to show the current state of the program for
   * debugging purposes.
   */
  public static void trace (String messageStr, Object source)
  {
    add (TRACE, messageStr, source);
  }

  /**
   * A trace is used to show the current state of the program for
   * debugging purposes.
   */
  public static void trace (String messageStr, Object source, Throwable ex)
  {
    add (TRACE, messageStr, source, ex);
  }

  /**
   * A warning is an alert that something anomalous has happened that
   * *may* indicate something has gone wrong.
   */
  public static void warn (String messageStr, Object source)
  {
    add (WARNING, messageStr, source);
  }

  /**
   * A warning is an alert that something anomalous has happened that
   * *may* indicate something has gone wrong.
   */
  public static void warn (String messageStr, Object source, Throwable ex)
  {
    add (WARNING, messageStr, source, ex);
  }

  /**
   * An alarm is for serious errors that indicate the program has
   * failed in some non-recoverable way.
   */
  public static void alarm (String messageStr, Object source)
  {
    add (ALARM, messageStr, source);
  }

  /**
   * An alarm is for serious errors that indicate the program has
   * failed in some non-recoverable way.
   */
  public static void alarm (String messageStr, Object source, Throwable ex)
  {
    add (ALARM, messageStr, source, ex);
  }

  /**
   * An info message is for the information that may be of interest to
   * the user, rather than a trace for the developer.
   */
  public static void info (String messageStr, Object source)
  {
    add (INFO, messageStr, source);
  }

  /**
   * Indicates some extremely serious error has occurred, usually
   * requiring immediate shutdown of the system.
   */
  public static void internalError (String messageStr, Object source)
  {
    add (INTERNAL_ERROR, messageStr, source);
  }

  /**
   * Indicates some extremely serious error has occurred, usually
   * requiring immediate shutdown of the system.
   */
  public static void internalError (String messageStr, Object source,
                                    Throwable ex)
  {
    add (INTERNAL_ERROR, messageStr, source, ex);
  }

  /**
   * A diagnostic is something that is likely to be useful for
   * diagnosing problems, and thus is more interesting than a trace,
   * but si not an actual warning of anything going wrong. An example
   * would be a messsage like "About to expire old messages",
   * "Completed system initialization", etc.
   */
  public static void diagnostic (String messageStr, Object source)
  {
    add (DIAGNOSTIC, messageStr, source);
  }

  /**
   * A diagnostic is something that is likely to be useful for
   * diagnosing problems, and thus is more interesting than a trace,
   * but si not an actual warning of anything going wrong. An example
   * would be a messsage like "About to expire old messages",
   * "Completed system initialization", etc.
   */
  public static void diagnostic (String messageStr, Object source, Throwable ex)
  {
    add (DIAGNOSTIC, messageStr, source, ex);
  }

  public static synchronized void removeLogListener (LogListener l)
  {
    if (logListeners != null && logListeners.contains (l))
    {
      Vector v = (Vector)logListeners.clone ();
      v.removeElement (l);

      logListeners = v;
    }
  }

  public static synchronized void addLogListener (LogListener l)
  {
    Vector v =
      logListeners == null ? new Vector (2) : (Vector)logListeners.clone ();

    if (!v.contains (l))
    {
      v.addElement (l);

      logListeners = v;
    }
  }

  protected static void fireMessageReceived (LogEvent e)
  {
    if (logListeners != null)
    {
      Vector listeners = logListeners;
      int count = listeners.size ();

      for (int i = 0; i < count; i++)
        ((LogListener)listeners.elementAt (i)).messageReceived (e);
    }
  }
}
