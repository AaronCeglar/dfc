package dsto.dfc.logging;

import java.util.Calendar;
import java.util.Date;
import java.util.EventObject;

public class LogEvent extends EventObject
{
  private String message;
  private int type;
  private Date time;
  private Throwable exception;

  public LogEvent (Object source, int type, String message)
  {
    this (source, type, message, Calendar.getInstance ().getTime ());
  }
  
  public LogEvent (Object source, int type, String message, Throwable ex)
  {
    this (source, type, message, Calendar.getInstance ().getTime (), ex);
  }

  public LogEvent (Object source, int type, String message, Date time)
  {
    this (source, type, message, time, null);
  }

  public LogEvent (Object source, int type, String message,
                   Date time, Throwable exception)
  {
    super (source);
    this.type = type;
    this.message = message;
    this.time = time;
    this.exception = exception;
  }

  public String getMessage ()
  {
    return message;
  }

  public int getType ()
  {
    return type;
  }

  public Date getTime ()
  {
    return time;
  }

  public Throwable getException ()
  {
    return exception;
  }
  
  /**
   * Get the class that raised this log message.
   */
  public Class getSourceClass ()
  {
    return (Class)getSource ();
  }
}