package dsto.dfc.logging;

/**
 * A filter for LogEvent's.
 * 
 * @author Matthew Phillips
 */
public class LogFilter
{ 
  private boolean [] enabledTypes;

  public LogFilter ()
  {
    this.enabledTypes =
      new boolean [] {/*TRACE*/false,
                       /*INFO*/false,
                       /*WARNING*/true,
                       /*ALARM*/true,
                       /*PERFORMANCE*/false,
                       /*DIAGNOSTIC*/false,
                       /*SYSTEM_EVENT*/false,
                       /*INTERNAL_ERROR*/true};
  }
  
  public LogFilter (LogFilter filter)
  {
    this.enabledTypes = filter.enabledTypes.clone ();
  }
  
  public boolean select (LogEvent e)
  {
    return enabledTypes [e.getType ()];
  }
  
  public boolean select (int type)
  {
    return enabledTypes [type];
  }

  public void setEnabled (int type, boolean isEnabled)
  {
    enabledTypes [type] = isEnabled;
  }

  public void enableAll ()
  {
    for (int i = 0; i < enabledTypes.length; i++)
      enabledTypes [i] = true;
  }

  public void disableAll ()
  {
    for (int i = 0; i < enabledTypes.length; i++)
      enabledTypes [i] = false;
  }

  /**
   * Enable/disable a set of types specified as a comma-delimited list.
   * 
   * @param commaDelimitedTypes A comma-delimited set of types
   *          acceptable to {@link Log#stringToType(String)}. e.g.
   *          "WARN,DIAGNOSTIC".
   * @param enabled True to enable the types, false to disable.
   * 
   * @throws IllegalArgumentException if any of the types in the list
   *           is invalid.
   *           
   * @see #setEnabled(int, boolean)
   */
  public void setEnabled (String commaDelimitedTypes, boolean enabled)
    throws IllegalArgumentException
  {
    String [] typeNames = commaDelimitedTypes.split (" *, *");
    int [] types = new int [typeNames.length];
    
    for (int i = 0; i < typeNames.length; i++)
      types [i] = Log.stringToType (typeNames [i].trim ());
    
    for (int i = 0; i < typeNames.length; i++)
      setEnabled (types [i], enabled);
  }
}
