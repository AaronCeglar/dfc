package dsto.dfc.logging;

import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Comparator;

/**
 * Prints selected log events to a file.
 * 
 * @author mpp
 */
public class FileLogger implements LogListener
{
  private File path;
  private PrintWriter output;
  private LogFilter filter;
  private DateFormat dateFormat;

  public FileLogger (String path)
  {
    this (new File (path));
  }

  public FileLogger (File path)
  {
    this.path = path;
    this.output = null;
    this.dateFormat = DateFormat.getDateTimeInstance ();
    this.filter = new LogFilter ();

    Log.addLogListener (this);
  }

  public void dispose ()
  {
    Log.removeLogListener (this);

    if (output != null)
      output.close ();
  }

  public void messageReceived (LogEvent e)
  {
    // todo date format is not thread-safe: make this method sync?
    if (filter.select (e) && openFile ())
    {
      Log.printMessage (output, e.getTime (), dateFormat,
                        e.getType (), e.getMessage ());

      if (e.getException () != null)
        Log.printException (output, e.getException ());

      output.flush ();
    }
  }

  public void print (String message)
  {
    output.println (message);

    output.flush ();
  }

  public void print (Throwable exception)
  {
    Log.printException (output, exception);

    output.flush ();
  }

  private boolean openFile ()
  {
    if (output == null)
    {
      try
      {
        path.getParentFile ().mkdirs ();

        output = new PrintWriter (new FileWriter (path.getPath (),
            true));
      } catch (IOException ex)
      {
        Log.removeLogListener (this);

        Log.warn ("Failed to open log file", this, ex);
      }
    }

    return output != null;
  }

  public void setEnabled (int type, boolean enabled)
  {
    filter.setEnabled (type, enabled);
  }

  /**
   * Rolls over the supplied logFile using the convention logFilename ->
   * logFilename.1, logFilename.1 -> logFilename.2
   * 
   * TODO Should to make this an 'automatic' feature of FileLogger
   * 
   * @param logFile The logFile to rollover
   * @param rolloverMax The maximum number of rolled over log files,
   *          log files exceeding this number will be deleted
   * @return Returns a reference to the new, empty rolled over log
   *         file
   */
  public static File rollover (final File logFile, int rolloverMax)
  {
    File parent = logFile.getParentFile ();

    // filter on files which 'look like' rolled over log files
    FileFilter rolloverFiles = new FileFilter ()
    {
      public boolean accept (File rolloverFile)
      {
        String rolloverRegex = logFile.getName () + "[.]\\d+";
        return rolloverFile.getName ().matches (rolloverRegex);
      }
    };
    
    File [] files = parent.listFiles (rolloverFiles);

    Arrays.sort (files, new Comparator ()
    {
      public int compare (Object o1, Object o2)
      {
        File file1 = (File)o1;
        File file2 = (File)o2;
        
        return file1.compareTo (file2);
      }
    });

    // loop through file list in reverse order
    // otherwise unable to rename file to existing
    // filename
    for (int i = files.length - 1; i >= 0; i--)
    {
      File file = files [i];
      // increment file name, eg log.txt.1 -> log.txt.2
      String filename = file.getName ();
      int index = filename.lastIndexOf ('.');
      String prefix = filename.substring (0, index + 1);
      String suffix = filename.substring (index + 1);
      int intSuffix = new Integer (suffix).intValue ();

      if (intSuffix < rolloverMax)
      {
        filename = prefix + (++intSuffix);
        File newFile = new File (parent, filename);

        if (!file.renameTo (newFile))
        {
          Log.warn ("Unable to rename " + file.getName () + " to " +
                    newFile.getName (), FileLogger.class);
        }
      } else
      {
        file.delete ();
      }
    }

    // increment/rename the logFile -> logFile.1;
    File newFile = new File (parent, logFile.getName () + ".1");

    if (!logFile.renameTo (newFile))
    {
      Log.warn ("Unable to rename " + logFile.getName () + " to " +
                newFile.getName (), FileLogger.class);
    }

    // create a new empty logfile
    return new File (parent, logFile.getName ());
  }
}
