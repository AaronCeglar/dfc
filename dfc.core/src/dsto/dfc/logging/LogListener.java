package dsto.dfc.logging;

import java.util.EventListener;

public interface LogListener extends EventListener
{
  public void messageReceived (LogEvent e);
}