@echo off

rem DFC build system bootstrap script: fires up Ant build utility.
rem
rem Requirements:
rem   JAVA_HOME    = path to Java 2 JDK installation
rem   DFC_HOME     = path to DFC core module
rem   ANT_ARGS     = optional extra command line options for Ant
rem   JAVA_OPTIONS = optional extra JVM parameters
rem
rem $Id$

set ERROR_STR=JAVA_HOME not set (must point to Java 2 installation)
if "%JAVA_HOME%"=="" goto error

set ERROR_STR=JAVA_HOME incorrect (must point to Java 2 installation)
if not exist %JAVA_HOME%\bin\java.exe goto error

set ERROR_STR=DFC_HOME not set (must point to DFC module)
if "%DFC_HOME%"=="" goto error

set ERROR_STR=DFC_HOME incorrect (must point to DFC module)
if not exist %DFC_HOME%\dfc.properties goto error

:runnit

if not "%JAVAC%"=="" set BUILD_COMPILER=-Dbuild.compiler=%JAVAC%

"%JAVA_HOME%\bin\java" "-Ddfc.home=%DFC_HOME%" %BUILD_COMPILER% %JAVA_OPTIONS% -classpath "%DFC_HOME%/classes;%JAVA_HOME%\lib\tools.jar;%DFC_HOME%\bin\ant.jar;%DFC_HOME%\lib\crimson.jar;%DFC_HOME%\lib\jaxp.jar;%DFC_HOME%\lib\junit.jar;%DFC_HOME%\lib\jakarta-oro-2.0.6.jar;%DFC_HOME%\lib\dfc.jar;%CLASSPATH%" "-Dant.home=%DFC_HOME%\bin" org.apache.tools.ant.Main %ANT_ARGS% %1 %2 %3 %4 %5

goto end

:error

echo ERROR: %ERROR_STR%

:end
