DSTO Foundation Classes
======================================================================

DFC Structure
----------------------------------------------------------------------

The DFC package is broken into the following units:

 * src: The source code.

 * lib: Assorted 3rd party libraries needed by DFC.

 * classes: Generated classes go here. Also contains icons and other
   resources.

 * bin: Executable utilities.

 * bin/ant: Subset of the standard Ant distribution needed to build.

 * design: Rose UML design for DFC.

 * dist: Distribition ZIP's are placed here during a build.

Some important files:

 * dfc.properties: Identifies DFC build and release numbers and
   contains the property (eg dfc.release_1_5) that allows clients to
   check for DFC compatibility.

 * doc/branches.txt: (Development branch only) Contains information
   about release branches.


Building DFC
----------------------------------------------------------------------

DFC uses Ant (http://jakarta.apache.org/ant) to build from
instructions in "build.xml". The Ant binaries needed to compile DFC
are included as part of DFC.

To build DFC from source:

 * Make sure JAVA_HOME points to JDK 1.4 or later.

 * Optionally set DFC_HOME to point to the DFC install directory (the
   default is to use the current one).

 * Run "build.bat" (Windows) or "build.sh" (Unix) to compile DFC. You
   can also run "build jar" to build "lib/dfc.jar" and "build apidoc"
   to build JavaDoc into the "doc/api" directory. Use "build src_dist"
   to build a source distribution.
