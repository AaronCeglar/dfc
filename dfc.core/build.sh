#!/bin/sh

# DFC build system bootstrap script: fires up Ant build utility.
#
# Requirements:
#   JAVA_HOME    = path to Java 2 JDK installation
#   DFC_HOME     = path to DFC module
#   ANT_ARGS     = optional extra command line options for Ant
#   JAVA_OPTIONS = optional extra JVM parameters
#
# JAVAC can be set to invoke a different compiler; default is 'modern'
#
# $Id$

JAVA=$JAVA_HOME/java

if [ ! -e $JAVA ] ; then
  JAVA=`which java`
fi

if [ ! -e $JAVA ] ; then
  echo "Cannot find Java. Please set JAVA_HOME to a Java 2 installation"
  exit 1
fi

if [ -z "$DFC_HOME" ] ; then
  export DFC_HOME=../dfc.core
fi

if [ -z "$DFC_HOME" ] ; then
  echo "DFC_HOME not set (must point to DFC module)"
  exit 1
fi

if [ ! -e $DFC_HOME/dfc.properties ] ; then
  echo "DFC_HOME incorrect (must point to DFC module)"
  exit 1
fi

JAVAC=${JAVAC:-modern}					# possible options: classic, modern, jikes

# decide on path separator for this platform

PS=":"                          # default

if [ $OSTYPE = "cygwin" ]  ||  [ $OSTYPE = "cygwin32" ] ; then
    # allow Robbe to use Cygwin ;)
		PS=";"
    DFC_HOME=`cygpath -p -w "$DFC_HOME"`
fi

$JAVA -Ddfc.home=$DFC_HOME $JAVA_OPTIONS -classpath "$DFC_HOME/classes${PS}${PS}$JAVA_HOME/lib/tools.jar${PS}$DFC_HOME/bin/ant.jar${PS}$DFC_HOME/lib/jaxp.jar${PS}$DFC_HOME/lib/junit.jar${PS}$DFC_HOME/lib/crimson.jar${PS}$DFC_HOME/lib/jakarta-oro-2.0.6.jar${PS}$DFC_HOME/lib/dfc.core.jar${PS}$CLASSPATH" -Dbuild.compiler=${JAVAC} -Dant.home=$DFC_HOME/bin org.apache.tools.ant.Main $ANT_ARGS $*
