#!/usr/bin/perl

# Run a DFC-based Java application
#
# Changeable environment settings:
#   JB_HOME -> JBuilder 3.0 installation
#   JAVA2_HOME -> JDK 1.2.2 or later installation.
#
# Everything else gets set automatically.

# $Id$

BEGIN
{
 # add DFC bin to include path
 die "DFC_HOME not set" if !$ENV{"DFC_HOME"};

 @INC = (@INC, $ENV{"DFC_HOME"}."/bin");
}

use File::Find;
use Getopt::Long;
use Cwd;
use environ;

$PROJECT = "";
$add_cp = "";

$options_status = &GetOptions ("project:s" => \$PROJECT,
                               "add_cp:s" => \$add_cp,
                               "home:s" => \$home_directory);
$java_class = shift (@ARGV);

if ($options_status == 0 || $java_class eq "")
{
  print STDERR "usage: run_java_app.pl [-home <home_directory>] [-project <project>] [-add_cp <classpath>] <java_class>";
  exit 1;
}

# divine various settings

divine_classpath ();

if ($add_cp ne "")
{
  $CLASSPATH = "$add_cp;$CLASSPATH";
}

$java_command =
  "$JAVA2_HOME/bin/java -classpath $CLASSPATH $java_class " . join (' ', @ARGV);
fix_path_sep ($java_command);

# run command

chdir ($home_directory) if $home_directory ne "";

$status =
  system ($java_command);

if ($status != 0)
{
  print ("*** JVM abnormal exit\n");
  exit ($status);
}
