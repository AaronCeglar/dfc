#!/usr/bin/perl

# Setup DFC-based project build environment.
#
# Must be set:
# 
# Changeable environment settings:
#   JAVA2_HOME -> JDK 1.2.2 or later installation.
#
# Everything else gets set automatically.

# $Id$

package environ;

use Cwd;
require Exporter;

@ISA = qw (Exporter);
@EXPORT = qw (divine_classpath fix_path_sep fix_env_sep
              $DEV_ROOT $DFC_HOME $PROJECT $PROJECT_HOME
              $CLASSPATH $JAVA2_HOME $JB_HOME $PATH_SEP $ENV_SEP);

# init DFC_HOME

$DFC_HOME = $ENV{"DFC_HOME"};

if ($DFC_HOME eq '')
{
  # try to guess DFC_HOME from exec path and CWD
  $EXEC_PATH = $0;
    
  # tack CWD on EXEC_PATH if not already absolute
  $EXEC_PATH = getcwd ()."/$EXEC_PATH" if (!($EXEC_PATH =~ m|^([a-zA-Z]:)?[/\\]|));

  $DFC_HOME = ($EXEC_PATH =~ m|^(.*[/\\]dfc)|i) [0];

  die "DFC_HOME not defined and couldn't be guessed" if $DFC_HOME eq '';
}

# add DFC bin to include path
@INC = (@INC, "$DFC_HOME/bin");

$PROJECT = "";
$OS = find_os ();
$PATH_SEP = $OS =~ /windows/i ? "\\" : "/";
$ENV_SEP = $OS =~ /windows/i ? ";" : ":";

sub divine_classpath
{
  die "DFC_HOME not set" if $DFC_HOME eq '';

  @CLASSPATH = split ($ENV_SEP, $ENV{'CLASSPATH'});

  if ($OS =~ /windows/i)
  {
    @JB_HOMES = ($ENV{'JB_HOME'}, "c:/jbuilder3", "d:/jbuilder3");
    @JAVA2_HOMES = ($ENV{'JAVA2_HOME'}, "c:/jdk1.3", "d:/jdk1.3",
                    "c:/jdk1.2.2", "d:/jdk1.2.2");
    $JAVAC = "javac.exe";
  } else
  {
    @JAVA2_HOMES = ($ENV{'JAVA2_HOME'},
                    "/usr/local/jdk1.3",
                    "/usr/local/jdk1.2.2", 
                    "/usr/jdk1.3",
                    "/usr/jdk1.2.2",
                    "/opt/jdk1.3",
                    "/opt/jdk1.2.2");

    $JAVAC = "javac";
  }

  foreach $HOME (@JB_HOMES)
  {
    if (-e "$HOME/bin/bmj.exe")
    {
      $JB_HOME = $HOME;
      last;
    }
  }

  foreach $HOME (@JAVA2_HOMES)
  {
    if (-e "$HOME/bin/$JAVAC")
    {
      $JAVA2_HOME = $HOME;
      last;
    }
  }

  if ($JB_HOME eq "")
  {
    print STDERR ("JB_HOME was not set or not valid and couldn't be guessed\n");
  }

  if ($JAVA2_HOME eq "")
  {
    print STDERR ("JAVA2_HOME was not set or not valid and couldn't be guessed\n");
  }

  @DFC_LIBS = map { glob ("$DFC_HOME/lib/$_") } "*.jar", "*.zip";

  # add DFC libs
  @CLASSPATH = (@DFC_LIBS, @CLASSPATH);
  @CLASSPATH = ("$DFC_HOME/classes", @CLASSPATH);
  @CLASSPATH = ("$DFC_HOME/src", @CLASSPATH);

  $cwd = getcwd ();

  # add optional project libs
  if ($PROJECT ne "")
  {
    $PROJECT_HOME = ($cwd =~ m|^(.*[/\\]$PROJECT)|) [0];
    $DEV_ROOT = $PROJECT_HOME;

    # print ("PROJECT_HOME = $PROJECT_HOME\n");

    if ($PROJECT_HOME eq "")
    {
      die "Cannot determine home for project $PROJECT";
    }

    @PROJECT_LIBS = map { glob ("$PROJECT_HOME/lib/$_") } "*.jar", "*.zip";

    @CLASSPATH = (@PROJECT_LIBS, @CLASSPATH);
    @CLASSPATH = ("$PROJECT_HOME/classes", @CLASSPATH);
  } else
  {
    $DEV_ROOT = $DFC_HOME;
  }

  # make CLASSPATH string
  $CLASSPATH = join ($ENV_SEP, @CLASSPATH);

  # convert path and env separators
  
  $CLASSPATH = fix_path_sep ($CLASSPATH);
  $JB_HOME = fix_path_sep ($JB_HOME);
  $JAVA2_HOME = fix_path_sep ($JAVA2_HOME);
  $DFC_HOME = fix_path_sep ($DFC_HOME);
  $PROJECT_HOME = fix_path_sep ($PROJECT_HOME);
  $DEV_ROOT = fix_path_sep ($DEV_ROOT);

  print ("JAVA2_HOME is $JAVA2_HOME\n");
  print ("JB_HOME is $JB_HOME\n");
  print ("DFC_HOME is $DFC_HOME\n");
  print ("PROJECT_HOME is $PROJECT_HOME\n");
}

sub find_os
{
  my $os;

  $os = $ENV{"OS"};

  if ($os eq '')
  {
    $os = $ENV{"OSTYPE"}; 
  }
  
  return $os;
}

# convert path separators to OS standard
sub fix_path_sep
{
  my ($path) = @_;

  if ($OS =~ /windows/i)
  {
    $path =~ s!/!\\!g;
  } else
  {
    $path =~ s!\\!/!g;
  }

  return $path;
}

# convert environment path list separators (eg ':' for Unix) to OS
# standard
sub fix_env_sep
{
  my ($env) = @_;

  if ($OS =~ /windows/i)
  {
    $env =~ s!:!;!g;
  } else
  {
    $env =~ s!;!:!g;
  }

  return $env;
}
