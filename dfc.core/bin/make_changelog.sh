#!/bin/sh

branch=$1
date=$2

if [ -z $DFC_HOME ]; then
  echo DFC_HOME not set
  exit 1
fi

if [ -z "$date" ]; then
  echo "usage: make_changelog.sh <branch> <date>"
  echo
  echo "  branch can be trunk for main branch"
  echo "  hint: use a date like \"24 feb 2001\""
  echo
  exit 1
fi

cvs log -d '>='"$date" . | $DFC_HOME/bin/cvs2cl.pl --stdin -F $branch
