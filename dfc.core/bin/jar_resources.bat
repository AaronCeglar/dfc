@echo off

rem Add image resources from DFC into a JAR file
rem $Id$

if {%1}=={} goto error

cd ..\classes
zip -r %1 dsto\dfc\images -x *CVS*
cd ..\bin

goto finish

:error

echo usage: jar_resources [jar_name]
echo
echo [run from DFC bin dir]

:finish
