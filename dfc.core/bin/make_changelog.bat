@echo off

rem Create a ChangeLog file in the current directory showing changes
rem from a give date on a given branch.
rem
rem $Id$

set MY_BRANCH=%1%
set MY_DATE=%2%

if not !%MY_DATE%!==!! goto runnit

echo usage: make_changelog ^<branch^> ^<date^>
echo   branch can be trunk for main branch
echo   hint: use a date like 2001-06-21

goto end

:runnit

cvs log -d ^>=%MY_DATE% . | perl %DFC_HOME%\bin\cvs2cl.pl --stdin -F %MY_BRANCH%

:end
