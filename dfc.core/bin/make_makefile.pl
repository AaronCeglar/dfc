#!/usr/bin/perl

# makes a makefile to build a source tree.
#
# $Id$

BEGIN
{
 # add DFC bin to include path
 die "DFC_HOME not set" if !$ENV{"DFC_HOME"};

 @INC = (@INC, $ENV{"DFC_HOME"}."/bin");
}

use File::Find;
use Getopt::Long;
use environ;

$out = "-";
$add_cp = "";
$compiler = "javac";
%options = ("notest" => \$notest, "out:s" => \$out,
            "project:s" => \$PROJECT, "add_cp:s" => \$add_cp,
            "compiler:s" => \$compiler);

if (&GetOptions (%options) == 0)
{
  print STDERR "usage: make_makefile.pl [-notest] [-out <outfile>] [-project <project>] [-compiler javac|bmj] <src_directory>...";
  exit 1;
}

open (OUTFILE, ">$out") || die "failed to open $out: $!";

divine_classpath ();

if ($add_cp ne "")
{
  $CLASSPATH = "${add_cp}${ENV_SEP}$CLASSPATH";
}

%java_dirs = ();
@base_dirs = @ARGV;
$source_path = join ($ENV_SEP, @base_dirs);

foreach $base_dir (@base_dirs)
{
  find (\&handle_file, $base_dir);
}

@java_dirs = keys (%java_dirs);

sort @java_dirs;

if ($compiler eq 'bmj')
{
  print OUTFILE ("setlocal\n") if $ENV{"OS"} eq 'Windows_NT';
  print OUTFILE ("call $JB_HOME\\bin\\setvars.bat $JB_HOME\n");
}

$SRC_ROOT = "${DEV_ROOT}${PATH_SEP}src";

foreach $dir (@java_dirs)
{
  $dir = fix_path_sep ($dir);

  $pkg = $dir;
  $pkg =~ s!/|\\!.!g;

  if (!$notest || ! ($pkg =~ /.test./))
  {
    if ($compiler eq 'bmj')
    {
      print OUTFILE ("bmj -p $pkg -d classes -sync -nowarn -nocheckstable -classpath $CLASSPATH\n");
    } elsif ($compiler eq 'javac')
    {
      print OUTFILE ("javac -sourcepath $source_path -d classes -classpath $CLASSPATH $SRC_ROOT$PATH_SEP$dir${PATH_SEP}*.java\n");
    }
  }
}

close (OUTFILE);

sub handle_file
{
  return if !/^.*\.java$/;

  $directory = $File::Find::dir;
  $directory =~ s|^${base_dir}/||;

  $java_dirs{$directory} = 1;
}
