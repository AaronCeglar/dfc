#!/usr/bin/perl

# rename tags (actually any defined CVS sybmol) in a CVS repository.
#
# $Id$

use File::Find;

if ($#ARGV <= 1)
{
  print "usage: cvs_rename_tag.pl from_tag to_tag dir...";
  exit (1);
}

$doit = 1;                      # set to 0 to run but do not change
$from_tag = shift (@ARGV);
$to_tag = shift (@ARGV);

find (\&scan_file, @ARGV);

sub scan_file
{
  if (/,v$/)
  {
    do_rename_tag ("$File::Find::dir/$_");
  }
}

sub do_rename_tag
{
  my ($file) = @_;

  print ("rename in file " . $file . "\n");

  $src_file = "$file.orig";
  $trg_file = $file;

  if ($doit)
  {
    rename ($trg_file, $src_file) || die "failed to rename $file: $!";

    open (TRG_FILE, ">$trg_file") || die "failed to open $trg_file: $!";
    open (SRC_FILE, "<$src_file") || die "failed to open $src_file: $!";
  } else
  {
    open (SRC_FILE, "<$file") || die "failed to open $file: $!";
  }

  while ($line = <SRC_FILE>)
  {
    if ($line =~ /^symbols/)
    {
      $in_symbols = 1;
    }

    # strict rename
    # if ($in_symbols && $line =~ /(\t)($from_tag)(:.*)/)
    # sloppy rename
    if ($in_symbols && $line =~ /(\t.*)($from_tag)(.*:.*)/)
    {
      $changed = 1;
      $line = "${1}${to_tag}${3}\n";
    } else
    {
      $changed = 0;
    }

    if ($line =~ /;$/)
    {
      $in_symbols = 0;
    }

    if ($doit)
    {
      print TRG_FILE ($line);
    } else
    {
      print ("  changed line: $line") if $changed;
    }
  }

  close (SRC_FILE);

  if ($doit)
  {
    close (TRG_FILE);
  }
}
