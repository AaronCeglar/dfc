package dsto.dfc.databeans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.NoSuchElementException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;

import java.net.URI;
import java.net.URL;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;

import junit.framework.AssertionFailedError;
import junit.framework.TestCase;

import org.jdom.Element;
import org.jdom.JDOMException;

import dsto.dfc.util.EnumerationValue;
import dsto.dfc.util.JavaBean;
import dsto.dfc.util.Singleton;
import dsto.dfc.util.StringEnumerationValue;

import dsto.dfc.databeans.io.XmlAwtCodec;
import dsto.dfc.databeans.io.XmlDecodeContext;
import dsto.dfc.databeans.io.XmlDecoder;
import dsto.dfc.databeans.io.XmlEncodeContext;
import dsto.dfc.databeans.io.XmlEncoder;
import dsto.dfc.databeans.io.XmlInput;
import dsto.dfc.databeans.io.XmlMemberwiseCodec;
import dsto.dfc.databeans.io.XmlOutput;

public class JUTestXmlInput extends TestCase
{
  /**
   * Constructor for JUTestXmlInput.
   * @param arg0
   */
  public JUTestXmlInput (String arg0)
  {
    super (arg0);
  }

  public static void main (String[] args)
  {
    junit.textui.TestRunner.run (JUTestXmlInput.class);
  }
  
  public void testInput () throws Exception
  {
    roundtripCodec ("test_input1.dbxml");
  }
  
  public void testString ()
    throws Exception
  {
    DataBean bean = new DataBean ();
    
    StringBuilder longEmptyString = new StringBuilder ();
    
    for (int i = 0; i < 1000; i++)
      longEmptyString.append (' ');
    
    bean.setValue ("emptyString", "");
    bean.setValue ("string", "hello");
    bean.setValue ("longString", longEmptyString.toString ());
    
    roundtripCodecFromString (toXml (bean));
  }
  
  public void testCustomXmlCodec () throws Exception
  {
    FooBeanXmlCodec fooCodec = new FooBeanXmlCodec ();
    
    XmlInput xmlInput = new XmlInput ();
    xmlInput.registerXmlDecoder ("foo", fooCodec);
    
    String xml = readResource ("test_input2.dbxml");
    
    Reader stream = new StringReader (xml);
   
    Object value = xmlInput.read (stream);
    
    // write it out and test roundtrip
    StringWriter outStream = new StringWriter (2048);    

    XmlOutput xmlStream = new XmlOutput ();
    xmlStream.registerXmlEncoder (fooCodec);
    xmlStream.write (outStream, value, true);
    
    String result = outStream.toString ();
    
    //System.out.println ("result:\n" + result);
    assertEquals (xml.trim (), result.trim ());
    
    stream.close ();
  }
  
  public void testEnumXmlCodec () throws Exception
  {
    roundtripCodec ("test_input3.dbxml");
  }
  
  public void testSingletonXmlCodec () throws Exception
  {
    roundtripCodec ("test_input4.dbxml");
  }
  
  public void testAdvancedCollections () throws Exception
  {
    // roundtripCodec ("test_input5.dbxml");
    // todo rountrip doesn't work because set serializes in variable order
    readResource ("test_input5.dbxml");
  }
  
  public void testDataObject () throws Exception
  {
    roundtripCodec ("test_input6.dbxml");
  }

  public void testJavaBean () throws Exception
  {
    roundtripCodec ("test_input7.dbxml");
  }
  
  public void testMemberwise () throws Exception
  {
    XmlMemberwiseCodec.register ("memberwise_address", TestMemberwise.class);
    
    roundtripCodec ("test_input8.dbxml");
  }
  
  /**
   * Test that invalid XML characters are handled by default text codec.
   */
  public void testBinary () throws Exception
  {
    DataBean bean1 = new DataBean ();
    bean1.setValue ("char1", 0);
    bean1.setValue ("char2", '\b');
    bean1.setValue ("str", "\u0000hello");
    
    // write bean
    StringWriter writer = new StringWriter ();
    XmlOutput output = new XmlOutput ();
    output.write (writer, bean1, false);
    
    // System.out.println (writer.toString ());
    // read bean
    StringReader reader = new StringReader (writer.toString ());
    
    XmlInput input = new XmlInput ();
    
    DataBean bean2 = (DataBean)input.read (reader);
    
    assertTrue ("Failed to roundtrip binary data",
                DataObjects.objectsEqual (bean1, bean2));
  }
  
  public void testDateCodec () throws Exception
  {
    Date date = new Date (424242424242l);
    
    DataBean bean = new DataBean ();
    bean.setValue ("date", date);
    
    // output bean
    XmlOutput xmlOutput = new XmlOutput ();
    Element root = xmlOutput.write (bean);
    Element dateElement = root.getChild ("date");
    
//    StringWriter writer = new StringWriter ();
//    xmlOutput.output (writer, root);
//    
//    System.out.println (writer.toString());
    
    assertNotNull (dateElement);
    
    // input bean
    XmlInput xmlInput = new XmlInput ();
    DataBean beanCopy = (DataBean)xmlInput.read (root);
    
    assertTrue (DataObjects.objectsEqual (bean, beanCopy));
  }
  
  public void testURICodec () throws Exception
  {
    URL url = new URL ("http://hello.com/frood.html"); 
    URI uri = new URI ("#hello");
    
    DataBean bean = new DataBean ();
    bean.setValue ("url", url);
    bean.setValue ("uri", uri);
    
    // output bean
    XmlOutput xmlOutput = new XmlOutput ();
    Element root = xmlOutput.write (bean);

    // output XML
    //    Element urlElement = root.getChild ("url");
    //    Element uriElement = root.getChild ("uri");
    //    
    //    StringWriter writer = new StringWriter ();
    //    xmlOutput.output (writer, root);
    //    
    //    System.out.println (writer.toString());
    //    
    //    assertNotNull (urlElement);
    //    assertNotNull (uriElement);
    
    // input bean
    XmlInput xmlInput = new XmlInput ();
    DataBean beanCopy = (DataBean)xmlInput.read (root);
    
    assertTrue (DataObjects.objectsEqual (bean, beanCopy));
  }
  
  public void testAwtCodec ()
    throws Exception
  {
    XmlAwtCodec.register ();
    
    DataBean bean = new DataBean ();
    
    bean.setValue ("point", new Point (24, 42));
    bean.setValue ("colour", new Color (1, 2, 3));
    bean.setValue ("rect", new Rectangle (10, 20, 100, 200));
    bean.setValue ("rect2dfloat", new Rectangle2D.Float (10.1f, 20.2f, 100.42f, 200.9f));
    bean.setValue ("rect2double", new Rectangle2D.Double (10.1, 20.2, 100.42, 200.9));
    
    String xml = new XmlOutput ().writeToString (bean);
   
    // System.out.println ("xml = " + xml);
    
    DataBean bean2 = (DataBean)new XmlInput ().read (new StringReader (xml));
    
    assertXmlIsSame (bean, bean2);
    //assertTrue (DataObjects.objectsEqual (bean, bean2));
  }
  
  /**
   * Test array serialization rountrip support.
   */
  public void testArrays ()
    throws IOException, JDOMException, ClassNotFoundException
  {
    DataBean bean1 = new DataBean ();
    
    bean1.setValue ("stringSingleArray", new String [] {"hello", "world"});
    
    bean1.setValue ("shortSingleArray", new short [] {-1, 42});
    
    bean1.setValue ("intSingleArray", new int [] {-1, 42});
    
    bean1.setValue ("longSingleArray", new long [] {-1, Long.MAX_VALUE});
    
    bean1.setValue ("floatSingleArray", new float [] {-1.2f, (float)Math.PI});
    
    bean1.setValue ("charSingleArray", new char [] {'a', 0x035F});
    
    bean1.setValue ("booleanSingleArray",
                   new boolean [] {true, false});
    
    // multi-dimensional arrays
    
    bean1.setValue ("stringMultiArray",
                    new String [] [] {new String [] {"hello", "world"}, 
                                      new String [] {"frob", "wibble"}});
    
    bean1.setValue ("intMultiArray",
                   new int [] [] {new int [] {1, 2}, new int [] {3, 4}});

    bean1.setValue ("longArray",
                   new long [] [] {new long [] {1, 2}, new long [] {3, 4}});
    
    bean1.setValue ("byteArray",
                   new byte [] [] {new byte [] {1, 2}, new byte [] {3, 4}});
    
    bean1.setValue ("charArray",
                   new char [] [] {new char [] {1, 2}, new char [] {3, 4}});
    
    bean1.setValue ("booleanArray",
                   new boolean [] [] {new boolean [] {true, false},
                                       new boolean [] {false, true}});
    
    String result = new XmlOutput ().writeToString (bean1);
    
    DataBean bean2 =
      (DataBean)new XmlInput ().read (new StringReader (result));
    
    //assertTrue (DataObjects.objectsEqual (bean1, bean2));
    assertXmlIsSame (bean1, bean2);
  }
  
  public void testLinks () throws Exception
  {
    roundtripCodec ("link_test_input.dbxml");
  }
  
  private void assertXmlIsSame (IDataObject object1, IDataObject object2)
    throws IOException, AssertionFailedError
  {
    XmlOutput output = new XmlOutput ();
    
    String bean1Xml = output.writeToString (object1);
    String bean2Xml = output.writeToString (object2);
    
//    System.out.println ("Bean 1 = \n" + bean1Xml);
//    System.out.println ("Bean 2 = \n" + bean2Xml);
    
    assertEquals ("XML representations do not match", bean1Xml, bean2Xml); 
  }

  private static String toXml (IDataObject object) 
    throws IOException
  {
    StringWriter outStream = new StringWriter (2048);    
  
    XmlOutput xmlStream = new XmlOutput ();
    xmlStream.write (outStream, object, true);
    
    return outStream.toString ();
  }

  private void roundtripCodec (String resource)
    throws IOException, JDOMException, ClassNotFoundException
  {
    roundtripCodecFromString (readResource (resource));
  }

  private void roundtripCodecFromString (String xml)
    throws IOException, JDOMException, ClassNotFoundException
  {
    XmlInput xmlInput = new XmlInput ();
    Reader stream = new StringReader (xml);
    Object value = xmlInput.read (stream);
    
    // write it out and test roundtrip
    StringWriter outStream = new StringWriter (2048);    
    
    XmlOutput xmlStream = new XmlOutput ();
    xmlStream.write (outStream, value, true);
    
    String result = outStream.toString ();
    
    try
    {
      assertEquals (xml.trim (), result.trim ());
    } catch (AssertionFailedError ex)
    {
      // System.out.println ("result:\n" + result);
      //      for (int i = 0; i < xml.length (); i++)
      //      {
      //        if (xml.charAt (i) != result.charAt (i))
      //        {
      //          System.out.println ("char 1 = " + (int)xml.charAt (i) + 
      //                              ", char 2 = " + (int)result.charAt (i));
      //        }
      //      }
      throw ex;
    }
    
    stream.close ();
  }
  
  private String readResource (String res) throws IOException
  {
    Reader reader =
      new InputStreamReader (getClass ().getResourceAsStream (res));
    
    char [] buff = new char[4096];
    StringBuilder string = new StringBuilder ();
    
    for (;;)
    {
      int len = reader.read (buff);
      
      if (len == -1)
        break;
        
      string.append (buff, 0, len);
    }
    
    reader.close ();
    
    return string.toString ();
  }
  
  private static final class FooBean
  {
    public String text1;
    public String text2;
    
    public FooBean (String text1, String text2)
    {
      this.text1 = text1;
      this.text2 = text2;
    }
  }
  
  static final class FooBeanXmlCodec implements XmlDecoder, XmlEncoder
  {
    public boolean shouldPreserveIdentity (XmlEncodeContext context, Object value)
    {
      return true;
    }

    public boolean canEncode (XmlEncodeContext context, Object value)
    {
      return value instanceof FooBean;
    }
    
    public Object decode (XmlDecodeContext context, Element element)
      throws IllegalArgumentException, ClassNotFoundException
    {
      return new FooBean (element.getChild ("text1").getText (),
                          element.getChild ("text2").getText ());
    }

    public Element encode (XmlEncodeContext context, Object value)
    {
      FooBean bean = (FooBean)value;
      Element root = new Element ("foo");
      
      Element text1 = new Element ("text1");
      text1.setText (bean.text1);
      
      Element text2 = new Element ("text2");
      text2.setText (bean.text2);
      
      root.addContent (text1);
      root.addContent (text2);
      
      return root;
    }
  }
  
  public static final class FooEnumValue extends StringEnumerationValue
  {
    public static final FooEnumValue GREEN = new FooEnumValue ("green");
    public static final FooEnumValue BLUE = new FooEnumValue ("blue");
    public static final FooEnumValue GREY = new FooEnumValue ("grey");
    
    public static final FooEnumValue [] VALUES = new FooEnumValue [] {GREEN, BLUE, GREY};
    
    public FooEnumValue ()
    {
      // zip
    }
    
    public FooEnumValue (String name)
    {
      super (name);
    }
    
    public EnumerationValue [] getEnumValues()
    {
      return VALUES;
    }
  }
  
  public static final class FooSingleton extends DataBean implements Singleton
  {
    public static final FooSingleton INSTANCE = new FooSingleton ();
  }
  
  /**
   * A data object that looks like a list.
   */
  public static final class FooDataObject implements IDataObject
  {
    private ArrayList list = new ArrayList ();
    
    public FooDataObject ()
    {
      // zip
    }
    
    public TypeInfo getTypeInfo ()
    {
      return null;
    }
    
    public Collection getPropertyListeners ()
    {
      return Collections.EMPTY_SET;
    }

    public void setValue (Object name, Object value)
    {
      setValue (name, value, PERSISTENT_OVERRIDE);
    }
    
    /**
     * NOTE: does not implement OVERRIDE.
     */
    public void setValue (Object name, Object value, int mode)
    {
      if ((mode & TRANSIENT) != 0)
        throw new IllegalArgumentException ("Transient properties not supported");
      
      int index = ((Integer)name).intValue ();
      
      if (index < list.size ())
        list.set (index, value);
      else
        list.add (index, value);
    }
  
    public Object getValue (Object name)
    {
      return list.get (((Integer)name).intValue ());
    }
    
    public boolean isTransient (Object name)
    {
      return false;
    }

    public Iterator propertyIterator ()
    {
      return new NumberIterator (0, list.size ());
    }
  
    public IDataObject shallowClone () throws UnsupportedOperationException
    {
      throw new UnsupportedOperationException ();
    }
  
    public void addPropertyListener (PropertyListener l)
    {
      // zip
    }
  
    public void removePropertyListener (PropertyListener l)
    {
      // zip
    }
  }
  
  public static final class FooJavaBean implements JavaBean
  {
    private int number = 42;
    private String name = "hello";
    
    
    /**
     * Returns the number.
     * @return int
     */
    public int getNumber()
    {
      return number;
    }

    /**
     * Sets the number.
     * @param number The number to set
     */
    public void setNumber(int number)
    {
      this.number = number;
    }

    /**
     * Returns the name.
     * @return String
     */
    public String getName()
    {
      return name;
    }

    /**
     * Sets the name.
     * @param name The name to set
     */
    public void setName(String name)
    {
      this.name = name;
    }

  }
  
  private static final class NumberIterator implements Iterator
  {
    private int upper;
    private int i;
    
    public NumberIterator (int lower, int upper)
    {
      this.upper = upper;
      this.i = lower;
    }

    public boolean hasNext ()
    {
      return i < upper;
    }

    public Object next ()
    {
      if (hasNext ())
      {
        return new Integer (i++);
      } else
      {
        throw new NoSuchElementException ();
      }
    }

    public void remove ()
    {
      throw new UnsupportedOperationException ();
    }
  }
  
  public static class TestMemberwise implements Serializable
  {
    protected int postCode = 42;
    protected String street = "Hello";
    protected transient int bogus = 1234;
  }
}
