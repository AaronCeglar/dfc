package dsto.dfc.databeans;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.util.ArrayList;

import junit.framework.TestCase;

import org.jdom.Element;

import dsto.dfc.databeans.DataBean;
import dsto.dfc.databeans.IDataBean;
import dsto.dfc.databeans.io.XmlOutput;

/**
 * todo move tests here to JUTextXmlInput.
 * 
 * @author mpp
 * @version $Revision$
 */
public class JUTestXmlOutput extends TestCase
{

  /**
   * Constructor for JUTestXmlOutputStream.
   * @param arg0
   */
  public JUTestXmlOutput(String arg0)
  {
    super(arg0);
  }

  public static void main(String[] args)
  {
    junit.textui.TestRunner.run(JUTestXmlOutput.class);
  }

  public void testOutput () throws Exception
  {
    String correct = readResource ("test_output1.dbxml");
    
    DataBean root = new DataBean ();
    root.setValue ("name", "matthew");
    root.setValue ("age", 42, IDataBean.TRANSIENT);
    
    DataBean address = new DataBean ();
    root.setValue ("address", address);
    
    address.setValue ("postCode", 5067);
    address.setValue ("street", "George St Norwood SA");
    
    root.setValue ("address2", address);
    
    ArrayList colours = new ArrayList ();
    String green = "Green";
    colours.add (green);
    colours.add ("Blue");
    colours.add ("Brown");
    colours.add (green);
    
    root.setValue ("colours", colours);
    
    //int [] numbers = new int [] {12, 66, 99};
    Integer [] numbers = new Integer [] {new Integer (12), null, new Integer (99)};
    root.setValue ("numbers", numbers);

    StringWriter stream = new StringWriter (2048);    

    XmlOutput xmlStream = new XmlOutput ();
    xmlStream.write (stream, root, true);
    
    String result = stream.toString ();
    System.out.println ("result:\n" + result);
    assertEquals (correct, result);
  }

  public void testEncode () throws Exception
  {
    /** @todo this is a fairly lame test, either make it better or remove */
    DataBean root = new DataBean ();
    root.setValue ("name", "matthew");
    root.setValue ("age", 42, IDataBean.TRANSIENT);
    
    DataBean address = new DataBean ();
    root.setValue ("address", address);

    address.setValue ("postCode", 5067);
    address.setValue ("street", "George");
    
    root.setValue ("address2", address);
    
    XmlOutput xmlStream = new XmlOutput ();
    Element xroot = xmlStream.write (root);
    Element xaddress = (Element)xroot.getChildren ().get (0);
    
    assertEquals ("bean", xroot.getName ());
    assertNotNull (xaddress);
    assertEquals ("address", xaddress.getAttributeValue ("id"));
  }
  
  private String readResource (String res) throws IOException
  {
    Reader reader =
      new InputStreamReader (getClass ().getResourceAsStream (res));
    
    char [] buff = new char[4096];
    StringBuilder string = new StringBuilder ();
    
    for (;;)
    {
      int len = reader.read (buff);
      
      if (len == -1)
        break;
        
      string.append (buff, 0, len);
    }
    
    reader.close ();
    
    return string.toString ();
  }

}
