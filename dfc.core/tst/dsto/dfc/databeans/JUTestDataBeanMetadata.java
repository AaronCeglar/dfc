package dsto.dfc.databeans;

import java.util.Date;

import junit.framework.TestCase;

/**
 * @author Matthew Phillips
 * 
 * todo handle generic "anything" types 
 */
public class JUTestDataBeanMetadata extends TestCase
{

  public static void main (String [] args)
  {
    junit.textui.TestRunner.run (JUTestDataBeanMetadata.class);
  }

  public void testBasic ()
  {
    TypeInfo intInfo = TypeRegistry.getTypeInfo ("int");
    
    assertNotNull (intInfo);
    assertEquals (Integer.class, intInfo.valueClass);
  }
  
  public void testAdvanced ()
  {
    TypeInfo personType = new TypeInfo ();
    personType.addPropertyInfo ("name", "string");
    personType.addPropertyInfo ("age", new TypeInfo (0, Integer.MAX_VALUE));
    personType.addPropertyInfo ("PIN", new TypeInfo (0, 100));
    
    TypeInfo birthDayType = new TypeInfo (Date.class);
    birthDayType.setValue ("units", "day");
    
    personType.addPropertyInfo ("birthDay", birthDayType);
    
    TypeInfo addressType = new TypeInfo ();
    addressType.addPropertyInfo ("streetName", "string");
    
    TypeInfo postCodeType = new TypeInfo (0, 1000);
    postCodeType.setValue ("inputPattern", "dddd");
    
    addressType.setValue ("postCode", postCodeType);

    personType.addPropertyInfo ("address", addressType);
    
    TypeRegistry.register ("address", addressType);
    TypeRegistry.register ("person", personType);
    
    //DataBean personBean = new DataBean (personType);
    
    TypeInfo nameInfo = TypeRegistry.getPropertyInfo ("person", "name");
    
    assertEquals (String.class, nameInfo.valueClass);
  }
}
