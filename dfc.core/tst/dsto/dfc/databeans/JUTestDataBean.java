package dsto.dfc.databeans;

import java.util.Arrays;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import java.io.IOException;

import java.awt.Font;

import junit.framework.AssertionFailedError;
import junit.framework.TestCase;

import dsto.dfc.util.BasicPropertyEventSource;
import dsto.dfc.util.Copyable;

import dsto.dfc.databeans.io.XmlOutput;

/**
 * Test IDataBean and IDataObject implementations.
 * 
 * @author mpp
 * @version $Revision$
 */
public class JUTestDataBean
  extends TestCase 
{
  private DataBean root;

  private DataBean bean1;

  private Listener rootListener;

  private Listener bean1Listener;

  private DataBean bean2;
  
  private JavaDataBean javaDataBean1;
  private TestBean javaBean1;

  private Listener javaDataBean1Listener;
  
  public JUTestDataBean(String arg0)
  {
    super(arg0);
  }

  public static void main(String[] args)
  {
    junit.textui.TestRunner.run(JUTestDataBean.class);
  }

  /**
   * @see TestCase#setUp()
   */
  protected void setUp() throws Exception
  {
    super.setUp ();
    
    root = new DataBean ();
    bean1 = new DataBean ();
    bean2 = new DataBean ();
    javaBean1 = new TestBean ();
    javaDataBean1 = new JavaDataBean (javaBean1);
    
    rootListener = new Listener ("root");
    bean1Listener = new Listener ("bean1");
    javaDataBean1Listener = new Listener ("javabean1");
    
    root.addPropertyListener (rootListener);
    bean1.addPropertyListener (bean1Listener);
    javaDataBean1.addPropertyListener (javaDataBean1Listener);
  }

  public void testSetValue ()
  {
    // base property
    root.setValue ("foo", "bar");
    assertEquals ("bar", root.getValue ("foo"));
    assertEquals ("foo", rootListener.getLastProperty ());
    
    // embedded bean1
    root.setValue ("bean1", bean1);
    
    bean1.setValue ("frob", "flib");
    
    assertEquals ("bean1/frob", rootListener.getLastProperty ());
    
    Object flib = DataObjects.getValue (root, "bean1/frob");
    assertEquals ("flib", flib);
    
    root.setValue ("bean1", null);
    
    bean1.setValue ("frob", "hello");
    
    assertEquals ("bean1", rootListener.getLastProperty ());
    
    // embedded bean2
    root.setValue ("bean1", bean1);
    bean1.setValue ("bean2", bean2);
    
    bean2.setValue ("hello", "there");
    
    assertEquals ("bean1/bean2/hello", rootListener.getLastProperty ());
    assertEquals ("bean2/hello", bean1Listener.getLastProperty ());
    
    // test setting to same value twice
    root.setValue ("foo", "bar");
    rootListener.lastEvent = null;
    root.setValue ("foo", "bar");
    assertNull (rootListener.lastEvent);
  }
  
  public void testSetValueToFail ()
  {
    DataBean bean = new DataBean ();
    
    try
    {
      bean.setValue (Integer.valueOf (1), 2);
      
      fail ("setValue() should have failed with a ClassCastException");
      
    } catch (ClassCastException e)
    {
      // all good - the non-String key was caught
    }
  }
  
  public void testGetValueToFail ()
  {
    DataBean bean = new DataBean ();
    
    try
    {
      bean.getValue (Integer.valueOf (1));
      
      fail ("getValue() should have failed with a ClassCastException");
      
    } catch (ClassCastException e)
    {
      // all good - the non-String key was caught
    }
  }

  public void testDataObjectsGetValue ()
  {
    DataBean bean11 = new DataBean ();
    root.setValue ("bean1", bean1);
    bean1.setValue ("bean11", bean11);
    bean11.setValue ("test", 1);
    
    // sanity check
    assertEquals (new Integer (1), DataObjects.getValue (root, "bean1/bean11/test"));
    assertEquals (null, DataObjects.getValue (root, "bean1/bean11/not-there"));
    
    // test int primitives
    assertEquals (1, DataObjects.getValue (root, "bean1/bean11/test", 2));
    assertEquals (2, DataObjects.getValue (root, "bean1/bean11/not-there", 2));
    
    // test boolean primitives
    DataObjects.setValue (root, "bean1/bean11/test", Boolean.FALSE);
    assertEquals (false, DataObjects.getValue (root, "bean1/bean11/test", true));
    assertEquals (true, DataObjects.getValue (root, "bean1/bean11/not-there", true));

    // test String objects
    DataObjects.setValue (root, "bean1/bean11/test", "myrtle");
    assertEquals ("myrtle", DataObjects.getValue (root, "bean1/bean11/test", "fred"));
    assertEquals ("fred", DataObjects.getValue (root, "bean1/bean11/not-there", "fred"));

    // test non-String objects
    Font font1 = new Font ("courier", Font.PLAIN, 12);
    Font font2 = new Font ("arial", Font.BOLD, 10);
    DataObjects.setValue (root, "bean1/bean11/test", font1);
    assertEquals (font1, DataObjects.getValue (root, "bean1/bean11/test", font2));
    assertEquals (font2, DataObjects.getValue (root, "bean1/bean11/not-there", font2));

    // test top-level properties (ie property paths of length 1)
    assertEquals (bean1, DataObjects.getValue (root, "bean1", bean2));
    assertEquals (bean2, DataObjects.getValue (root, "not-there", bean2));
  }
  
  /**
   * Test using an iterator to scan through and delete values from a
   * DataObject.
   */
  public void testIterator ()
  {
    // test value iterator
    DataObject object1 = new DataObject ();
    object1.setValue ("foo", "bar");
    object1.setValue ("hello", "there");
 
    doTestIterator (object1, object1.iterator ());
    
    // test property iterator
    DataObject object2 = new DataObject ();
    object2.setValue ("foo", "bar");
    object2.setValue ("hello", "there");

    doTestIterator (object2, object2.propertyIterator ());
  }
  
  private void doTestIterator (DataObject object, Iterator i)
  {    
    Listener listener = new Listener ("object");
    object.addPropertyListener (listener);
    
    int count = 0;
    
    while (i.hasNext ())
    {
      i.next ();
      
      count++;
      
      listener.lastEvent = null;
      
      i.remove ();
      
      assertNotNull (listener.lastEvent);
      
      // check that the property looks right
      assertEquals (1, listener.lastEvent.path.length ());
      Object property = listener.lastEvent.path.last ();
      assertTrue (property.equals ("foo") || property.equals ("hello"));
    }
    
    assertEquals (2, count);
    assertNull (object.getValue ("foo"));
    assertNull (object.getValue ("hello"));
  }

  public void testClone () throws Exception
  {
    root.setValue ("default", "default", IDataBean.TRANSIENT);
    
    root.setValue ("foo", "bar");
    root.setValue ("bean1", bean1);
    bean1.setValue ("hello", "there");
    
    DataBean copy = (DataBean)DataObjects.deepClone (root);

    assertEquals (root.getValue ("foo"), copy.getValue ("foo"));
    assertEquals (DataObjects.getValue (copy, "bean1/hello"), "there");
    assertNotSame (root.getValue ("bean1"), copy.getValue ("bean1"));
    assertEquals ("default", copy.getValue ("default"));
    assertTrue (copy.isTransient ("default"));
    
    rootListener.lastEvent = null;
    copy.setValue ("foo", "bar");
    assertNull (rootListener.lastEvent);
    
    Listener copyListener = new Listener ("copy");
    copy.addPropertyListener (copyListener);
    copy.setValue ("foo", "frob");
    assertEquals ("foo", copyListener.getLastProperty ());
  }
  
  public void testDefaults ()
  {
    // test setting unhidden default
    root.setValue ("foo", "bar", IDataBean.TRANSIENT);
    assertEquals ("foo", rootListener.getLastProperty ());
    assertEquals ("bar", root.getValue ("foo"));
    assertTrue (root.isTransient ("foo"));
    
    // test hide of default value
    rootListener.lastEvent = null;
    
    root.setValue ("foo", "frob");
    assertEquals ("foo", rootListener.getLastProperty ());
    assertEquals ("frob", root.getValue ("foo"));
    assertFalse (root.isTransient ("foo"));
    
    // test removing default fires event
    root.setValue ("foo", null);
    root.setValue ("foo", null, IDataBean.TRANSIENT);
    root.setValue ("foo", "bar", IDataBean.TRANSIENT);
    assertEquals ("bar", rootListener.lastEvent.newValue);
  }

  public void testJavaDataBean ()
  {
    // root.setValue ("bean1", javaDataBean1);
    
    assertEquals ("default", javaDataBean1.getValue ("string"));
    assertTrue (javaDataBean1.isTransient ("string"));
    
    javaBean1.setString ("hello");
    assertEquals ("hello", javaDataBean1.getValue ("string"));
    assertFalse (javaDataBean1.isTransient ("string"));
    
    javaDataBean1.setValue ("string", "foo");
    assertEquals ("foo", javaBean1.getString ());
  }
  
  public void testJavaDataBeanClone () throws Exception
  {
    javaBean1.setNumber (101);
    
    JavaDataBean copy = (JavaDataBean)DataObjects.deepClone (javaDataBean1);
    
    assertEquals ("default", copy.getValue ("string"));
    assertTrue (copy.isTransient ("string"));
    assertNotSame (copy.getBean (), javaDataBean1.getBean ());
    
    assertEquals (101, copy.getIntValue ("number"));
    assertFalse (copy.isTransient ("number"));
  }
  
  public void testListDataObject () throws Exception
  {
    ListDataObject list = new ListDataObject ();
    
    root.setValue ("list", list);
    
    // test add
    rootListener.lastEvent = null;
    
    list.add ("hello");
    list.add ("there");
    
    assertNotNull (rootListener.lastEvent);
    assertEquals ("list/1", rootListener.lastEvent.path.toString ());
    
    // test set 
    rootListener.lastEvent = null;
    
    list.set (0, "there");
    
    assertNotNull (rootListener.lastEvent);
    assertEquals ("list/0", rootListener.lastEvent.path.toString ());
    
    // test getValue () on undefined values
    assertNull (list.getValue (new Integer (-1)));
    assertNull (list.getValue (new Integer (10000)));
    assertNull (list.getValue ("frob"));
    
    // test embedded data object
    DataBean bean3 = new DataBean ();
    
    list.add (bean3);
    
    rootListener.lastEvent = null;
    bean3.setValue ("hello", "there");
    
    assertNotNull (rootListener.lastEvent);
    assertEquals ("there", DataObjects.getValue (root, "list/2/hello"));
    assertNotNull (rootListener.lastEvent);
    assertEquals ("list/2/hello", rootListener.lastEvent.path.toString ());
    
    // test clone
    list = new ListDataObject ();
    String foo = "foo";
    list.setValue (new Integer (0), foo);
    list.setValue (new Integer (1), foo);
    
    DataBean beanInList = new DataBean ();
    beanInList.setValue ("name", "foo");
    
    list.setValue (new Integer (2), beanInList);
    
    ListDataObject copy = (ListDataObject)DataObjects.deepClone (list);
    
    
    assertEquals ("foo", DataObjects.getValue (copy, "0")); 
    // check that clone *did not* clone the foo string
    assertSame (foo, DataObjects.getValue (copy, "0"));
    // check that clone *did* clone the list
    assertNotSame (copy.getValue ("list"), list);

    // check that lists are functional copies of each other
    assertXmlIsSame (list, copy);
    assertTrue (DataObjects.objectsEqual (list, copy));

    // test listener
    rootListener.lastEvent = null;
    
    Listener copyListener = new Listener ("copy");
    copy.addPropertyListener (copyListener);
    
    DataObjects.setValue (list, "0", "bar");
    
    assertNull (copyListener.lastEvent);
    
    copy.setValue (new Integer (0), "bar");
    
    assertNotNull (copyListener.lastEvent);
    assertEquals ("0", copyListener.lastEvent.path.toString ());    
  }
  
  public void testStackDataObject() throws Exception
  {
    StackDataObject stack = new StackDataObject();
    
    root.setValue ("stack", stack);
    
    // test add
    rootListener.lastEvent = null;
    
    stack.push ("one");
    stack.push ("two");
    
    assertNotNull (rootListener.lastEvent);
    assertEquals ("stack/1", rootListener.lastEvent.path.toString ());
    
    //test peek
    String s = (String)stack.peek();
    assertEquals("two", s);
    
    //test add again
    rootListener.lastEvent = null;
    stack.push("three");
    stack.push("four");
    assertNotNull(rootListener.lastEvent);
    assertEquals("stack/3", rootListener.lastEvent.path.toString());
    
    //test pop and then add
    s = (String)stack.pop();
    assertEquals("four", s);
    rootListener.lastEvent = null;
    stack.push("five");
    assertNotNull(rootListener.lastEvent);
    assertEquals("stack/3", rootListener.lastEvent.path.toString());
  }
  
  public void testMapDataObject () throws Exception
  {
    MapDataObject map = new MapDataObject ();
    
    Listener listener = new Listener ("test");
    
    map.addPropertyListener (listener);
    
    map.setValue ("transient", "transient", IDataObject.TRANSIENT);
    map.setValue ("persistent", "persistent", IDataObject.PERSISTENT);
    
    map.setValue ("override", "override", IDataObject.TRANSIENT);
    assertTrue (listener.lastEvent.transientProperty);
    map.setValue ("override", "fail", IDataObject.PERSISTENT);
    assertTrue (listener.lastEvent.transientProperty);
    
    assertTrue (map.isTransient ("transient"));
    assertFalse (map.isTransient ("persistent"));
    assertEquals ("override", map.getValue ("override"));
    
    map.setValue ("transient", null);
    assertEquals ("transient", listener.lastEvent.path.first ());
    assertTrue (listener.lastEvent.transientProperty);
    
    map.setValue ("persistent", null);
    assertEquals ("persistent", listener.lastEvent.path.first ());
    assertFalse (listener.lastEvent.transientProperty);
    
    // test listener on embedded IDataObject's
    DataBean childBean = new DataBean ();
    assertEquals (0, childBean.getPropertyListeners ().size ());
    
    map.setValue ("child", childBean);
    assertEquals (1, childBean.getPropertyListeners ().size ());
    
    listener.lastEvent = null;
    childBean.setValue ("hello", "there");
    assertEquals (2, listener.lastEvent.path.length ());
    assertEquals ("child", listener.lastEvent.path.first ());
    assertEquals ("hello", listener.lastEvent.path.last ());
    
    map.setValue ("child", null);
    assertEquals (0, childBean.getPropertyListeners ().size ());
  }

  public void testSetDataObject () throws Exception
  {
    SetDataObject set = new SetDataObject ();
    
    Listener listener = new Listener ("test");
    
    set.addPropertyListener (listener);
    
    set.setValue ("transient", "transient", IDataObject.TRANSIENT);
    set.setValue ("persistent", "persistent", IDataObject.PERSISTENT);
        
    assertTrue (set.isTransient ("transient"));
    assertFalse (set.isTransient ("persistent"));
    
    set.setValue ("transient", null);
    assertEquals ("transient", listener.lastEvent.path.first ());
    assertTrue (listener.lastEvent.transientProperty);
    
    set.setValue ("persistent", null);
    assertEquals ("persistent", listener.lastEvent.path.first ());
    assertFalse (listener.lastEvent.transientProperty);
    
    // test listener on embedded IDataObject's
    DataBean childBean = new DataBean ();
    assertEquals (0, childBean.getPropertyListeners ().size ());
    
    set.add (childBean);
    assertEquals (1, childBean.getPropertyListeners ().size ());
    
    childBean.setValue ("hello", "hello");
    assertEquals (childBean, listener.lastEvent.path.first ());
    
    set.setValue (childBean, null);
    assertEquals (0, childBean.getPropertyListeners ().size ());
  }
  
  public void testSetDataObjectClone () throws Exception
  {
    SetDataObject set = new SetDataObject ();

    DataBean localBean1 = new DataBean ();
    localBean1.setValue ("name", "bean1");
    localBean1.setValue ("magic", 1);
    
    DataBean localBean2 = new DataBean ();
    localBean2.setValue ("name", "bean2");
    localBean2.setValue ("magic", 2);
    
    set.add (localBean1);
    set.add (localBean2);
    
    SetDataObject copy = (SetDataObject)DataObjects.deepClone (set);
    DataBean bean1Copy = (DataBean)DataObjects.find (copy, "name", "bean1");
    DataBean bean2Copy = (DataBean)DataObjects.find (copy, "name", "bean2");
    
    assertEquals (2, copy.size ());
    assertNotSame (localBean1, bean1Copy);
    assertNotSame (localBean2, bean2Copy);
    assertEquals ("bean1", bean1Copy.getValue ("name"));
    assertEquals (1, bean1Copy.getIntValue ("magic"));
    assertEquals ("bean2", bean2Copy.getValue ("name"));
    assertEquals (2, bean2Copy.getIntValue ("magic"));
  }
  
  public void testFancyDataObject ()
  {
    FunkyTestObject object = new FunkyTestObject ();
    
    assertEquals (42, object.getIntValue ("number"));
    assertEquals ("fourty two", object.getValue ("string"));
    
    object.setValue ("extendedNumber", 24);
    assertEquals (24, object.getIntValue ("extendedNumber"));
    
    object.setValue ("number", 10);
    assertEquals (10, object.getIntValue ("number"));
    
    HashSet properties = new HashSet ();
    for (Iterator i = object.propertyIterator(); i.hasNext ();)
      properties.add (i.next ());
    
    assertTrue (properties.contains ("number"));
    assertTrue (properties.contains ("string"));
    assertTrue (properties.contains ("extendedNumber"));
    
    IDataObject copy = DataObjects.deepClone (object);
    
    assertTrue (DataObjects.objectsEqual (object, copy));
    
    // test transience
    FunkyTestObject object2 = new FunkyTestObject ();
    object2.setValue ("transient1", "true", IDataBean.TRANSIENT);
    assertFalse (object2.isTransient ("number"));
    assertTrue (object2.isTransient ("transient1"));
    
    // check that child changes "upgrade" transient to non-transient
    DataBean childBean = new DataBean ();
    object2.setValue ("child", childBean, IDataBean.TRANSIENT);
    assertTrue (object2.isTransient ("child"));
    
    childBean.setValue ("nontransient", 1);
    assertFalse (object2.isTransient ("child"));
  }
  
  public void testMapCopyConstructor ()
  {
    Map<String, String> goodMap = new HashMap<String, String> ();
    goodMap.put ("hello", "world");
    
    DataBean bean = new DataBean (goodMap);
    assertEquals ("world", bean.getValue ("hello"));
    assertNull (bean.getValue ("goodbye"));
    
    Map<Integer, String> badMap = new HashMap<Integer, String> ();
    badMap.put (1, "fail");
    
    try
    {
      bean = new DataBean (badMap);
      fail ("Copy constructor should have failed with a ClassCastException");
      
    } catch (ClassCastException e)
    {
      // all good - the non-String key was caught
    }
  }
  
  public void testDictionaryCopyConstructor ()
  {
    Dictionary<String, String> goodDict = new Hashtable<String, String> ();
    goodDict.put ("hello", "world");
    
    DataBean bean = new DataBean (goodDict);
    assertEquals ("world", bean.getValue ("hello"));
    assertNull (bean.getValue ("goodbye"));
    
    Dictionary<Integer, String> badDict = new Hashtable<Integer, String> ();
    badDict.put (1, "fail");
    
    try
    {
      bean = new DataBean (badDict);
      fail ("Copy constructor should have failed with a ClassCastException");
      
    } catch (ClassCastException e)
    {
      // all good - the non-String key was caught
    }
  }
  
  public void testIDataObjectCopyConstructor ()
  {
    IDataObject goodIDO = DataObjects.make ("hello", "world");
    
    DataBean bean = new DataBean (goodIDO);
    assertEquals ("world", bean.getValue ("hello"));
    assertNull (bean.getValue ("goodbye"));
    
    IDataObject badIDO = DataObjects.make (1, "fail");
    
    try
    {
      bean = new DataBean (badIDO);
      fail ("Copy constructor should have failed with a ClassCastException");
      
    } catch (ClassCastException e)
    {
      // all good - the non-String key was caught
    }
  }
  
  public void testArrays ()
  {
    DataObject dataobject = new DataObject ();
    
    int [] intArray1 = new int [] {1, 42, 33};
    int [] intArray2 = new int [] {66, 0, Integer.MAX_VALUE};
    
    Listener listener = new Listener ("dataobject");
    
    dataobject.addPropertyListener (listener);
    dataobject.setValue ("intArray", intArray1);
    
    assertEquals ("intArray", listener.lastEvent.path.first ());
    
    assertTrue
      (Arrays.equals (intArray1,
                      (int [])dataobject.getValue ("intArray")));
    
    // check redundant sets don't generate an event
    listener.lastEvent = null;
    dataobject.setValue ("intArray", intArray1.clone ());
    assertNull (listener.lastEvent);
    
    // check changes work
    dataobject.setValue ("intArray", intArray2);
    assertNotNull(listener.lastEvent);
    assertEquals ("intArray", listener.lastEvent.path.first ());
    assertTrue
      (Arrays.equals (intArray2,
                      (int [])dataobject.getValue ("intArray")));
  }
  
  public void testLinks ()
  {
    doTestLink (new DataBean (), "childHardLinked", "childSoftLinked");
    //doTestLink (new LinkTestDataObject (), "childHardLinked", "childSoftLinked");
    doTestLink (new ListDataObject (), new Integer (0), new Integer (1));
    doTestLink (new MapDataObject (), "childHardLinked", "childSoftLinked");
    doTestLink (new TestLinkObject (), "childHardLinked", "childSoftLinked");
    // todo this currently fails: find out why
    //doTestLink (new TestLinkJavaBean (), "childHardLinked", "childSoftLinked");
    doTestSetLink ();
  }
  
  private void doTestLink (IDataObject object,                            
                            Object hardLinkProperty,
                            Object softLinkProperty)
  {
    IDataBean childHardlinked = new DataBean ();
    IDataBean childSoftlinked = new DataBean ();
    
    object.setValue (hardLinkProperty, new DataBeanLink (childHardlinked, false));
    object.setValue (softLinkProperty, new DataBeanLink (childSoftlinked, true));
    
    Listener listener = new Listener ("object");
    
    object.addPropertyListener (listener);
    
    // check property event for hard link
    childHardlinked.setValue ("name", "foo");
    assertNotNull (listener.lastEvent);
    assertEquals (new PropertyPath (new Object [] {hardLinkProperty, "name"}),
                  listener.lastEvent.path);
    
    // check no property event for soft link
    listener.lastEvent = null;
    childSoftlinked.setValue ("name", "bar");
    assertNull (listener.lastEvent);
    
    // check clone
    IDataObject objectCopy = DataObjects.deepClone (object);
    assertNotSame (object.getValue (hardLinkProperty),
                   objectCopy.getValue (hardLinkProperty));
  }
  
  private void doTestSetLink ()
  {
    IDataBean childHardlinked = new DataBean ();
    IDataBean childSoftlinked = new DataBean ();
    DataBeanLink hardLink = new DataBeanLink (childHardlinked, false);
    DataBeanLink softLink = new DataBeanLink (childSoftlinked, true);
    
    SetDataObject set = new SetDataObject ();
    set.add (hardLink);
    set.add (softLink);
    
    Listener listener = new Listener ("object");
    
    set.addPropertyListener (listener);
    
    // check property event for hard link
    childHardlinked.setValue ("name", "foo");
    assertNotNull (listener.lastEvent);
    assertEquals (new PropertyPath (new Object [] {hardLink, "name"}),
                  listener.lastEvent.path);
    
    // check no property event for soft link
    listener.lastEvent = null;
    childSoftlinked.setValue ("name", "bar");
    assertNull (listener.lastEvent);
  }
  
  public void testDataObjectFilter ()
  {
    final DataBean source = new DataBean ();
    final DataBean target = new DataBean ();
    
    DataBean child1 = (DataBean)DataObjects.makePath (source, "child1");
    DataBean child2 = (DataBean)DataObjects.makePath (source, "child2");
    DataBean child3 = (DataBean)DataObjects.makePath (source, "child3");
    
    child1.setValue ("included", true);
    child2.setValue ("included", false);
    
    new DataObjectFilter (source, target, new DataObjectFilter.Selector ()
    {
      public boolean select (PropertyPath property,
                               Object oldValue, Object newValue)
      {
        // System.out.println (source.getBeanValue ((String)property.first ()) + " include = " + source.getBeanValue ((String)property.first ()).getBooleanValue ("included"));
        return source.getBeanValue ((String)property.first ()).getBooleanValue ("included");
      }
    });
    
    assertSame (child1, target.getValue ("child1"));
    assertNull (target.getValue ("child2"));
    assertNull (target.getValue ("child3"));
    
    // change child 3 property to make filter include
    child3.setValue ("included", true);
    assertSame (child3, target.getValue ("child3"));
    
    // change child 1 property to make filter not include
    child1.setValue ("included", null);
    assertNull (target.getValue ("child1"));
    
    // change child 2 property to make filter include
    child2.setValue ("included", true);
    assertSame (child2, target.getValue ("child2"));
    
    // remove child 2 entirely
    source.setValue ("child2", null);
    assertNull (target.getValue ("child2"));
  }
  
  private void assertXmlIsSame (IDataObject object1, IDataObject object2)
    throws IOException, AssertionFailedError
  {
    XmlOutput output = new XmlOutput ();
    
    String bean1Xml = output.writeToString (object1);
    String bean2Xml = output.writeToString (object2);
    
//    System.out.println ("Bean 1 = \n" + bean1Xml);
//    System.out.println ("Bean 2 = \n" + bean2Xml);
    
    assertEquals ("XML representations do not match", bean1Xml, bean2Xml); 
  }
  
  public static class Listener implements PropertyListener
  {
    public String name;
    public PropertyEvent lastEvent;
    
    public Listener (String name)
    {
      this.name = name;
    }
    
    public void propertyValueChanged (PropertyEvent e)
    {
      lastEvent = e;
      
      // System.out.println (name + ": " + e);
    }
    
    public String getLastProperty ()
    {
      return lastEvent.path.toString ();
    }
  }
  
  public static class TestBean
    extends BasicPropertyEventSource implements Copyable
  {
    private String string = "default";
    private int number = 42;
    private DataBean subBean = new DataBean ();
    
    public Object clone () throws CloneNotSupportedException
    {
      return super.clone();
    }

    public int getNumber()
    {
      return number;
    }

    public String getString()
    {
      return string;
    }

    public void setNumber(int newValue)
    {
      int oldValue = number;
      
      this.number = newValue;
      
      firePropertyChange ("number", oldValue, newValue);
    }

    public void setString (String newValue)
    {
      String oldValue = string;
      
      this.string = newValue;
      
      firePropertyChange ("string", oldValue, newValue);
    }

    public DataBean getSubBean()
    {
      return subBean;
    }

    public void setSubBean(DataBean subBean)
    {
      this.subBean = subBean;
    }
  }

  public static class FunkyTestObject extends FancyDataObject
  {
    public int number;
    public String string;
    public boolean [] bunchOBooleans;
    
    public FunkyTestObject ()
    {
      super ();
      
      number = 42;
      string = "fourty two";
      bunchOBooleans = new boolean [] {true, false, false, true};
    }
  }
  
  public static class TestLinkObject extends FancyDataObject
  {
    public IDataObject childHardLinked;
    public IDataObject childSoftLinked;
    
    public TestLinkObject ()
    {
      super ();
    }
  }
  
  public static class TestLinkJavaBean extends JavaDataBean
  {
    private IDataObject childHardLinked;
    private IDataObject childSoftLinked;
    
    public TestLinkJavaBean ()
    {
      initDataBean (TestLinkJavaBean.class);
    }
    
    public IDataObject getChildHardLinked ()
    {
      return childHardLinked;
    }
    
    public void setChildHardLinked (IDataObject childHardLinked)
    {
      this.childHardLinked = childHardLinked;
    }
    
    public IDataObject getChildSoftLinked ()
    {
      return childSoftLinked;
    }
    
    public void setChildSoftLinked (IDataObject childSoftLinked)
    {
      this.childSoftLinked = childSoftLinked;
    }
  }
}
