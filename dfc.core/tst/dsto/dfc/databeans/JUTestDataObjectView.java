package dsto.dfc.databeans;

import junit.framework.TestCase;

import dsto.dfc.databeans.JUTestDataBean.Listener;

import static dsto.dfc.databeans.IFilter.NULL_FILTER;

public class JUTestDataObjectView extends TestCase
{
  public void testDataObjectView ()
  {
    IFilter filter = new IFilter ()
    {
      public boolean include (Object value)
      {
        return ((IDataBean)value).getValue ("name") != null;
      }
    };
    
    ISorter sorter = new  ISorter<IDataBean> () 
    {
      public boolean affectsOrder (PropertyPath path)
      {
        return path.length () == 1 && path.last ().equals ("name");
      }
      
      public int compare (IDataBean o1, IDataBean o2)
      {
        return o1.getStringValue ("name").compareToIgnoreCase (o2.getStringValue ("name"));
      }
    };
    
    final DataBean source = new DataBean ();
    
    DataBean child1 = (DataBean)DataObjects.makePath (source, "child1");
    DataBean child2 = (DataBean)DataObjects.makePath (source, "child2");
    DataBean child3 = (DataBean)DataObjects.makePath (source, "child3");
    
    child1.setValue ("name", "aaa");
    child2.setValue ("name", "zzz");
    
    final DataObjectView target = new DataObjectView (source, filter, sorter);

    Listener listener = new Listener ("dataview");
    target.addPropertyListener (listener);
    
    assertEquals (2, target.size ());
    assertSame (child1, target.getValue (0));
    assertSame (child2, target.getValue (1));
    
    // make child 3 appear
    listener.lastEvent = null;
    child3.setValue ("name", "CCC");
    assertEquals (3, target.size ());
    assertSame (child1, target.getValue (0));
    assertSame (child2, target.getValue (2)); 
    assertSame (child3, target.getValue (1));
    
    // test event that was fired
    assertNotNull (listener.lastEvent);
    assertEquals (1, ((Integer)listener.lastEvent.path.first ()).intValue ());
    assertNull (listener.lastEvent.oldValue);
    assertSame (child3, listener.lastEvent.newValue);
    
    // make child 2 move
    child2.setValue ("name", "BBB");
    assertEquals (3, target.size ());
    assertSame (child1, target.getValue (0));
    assertSame (child2, target.getValue (1)); 
    assertSame (child3, target.getValue (2));
    
    // make child 1 disappear
    child1.setValue ("name", null);
    assertEquals (2, target.size ());
    assertSame (child2, target.getValue (0)); 
    assertSame (child3, target.getValue (1));
    
    // remove child 2
    source.setValue ("child2", null);
    assertEquals (1, target.size ());
    assertSame (child3, target.getValue (0));
    
    // change child1 and child2, test no events fired
    listener.lastEvent = null;
    child1.setValue ("value", "1");
    assertNull (listener.lastEvent);
    child2.setValue ("name", "2");
    assertNull (listener.lastEvent);
    
    // change child 3
    listener.lastEvent = null;
    child3.setValue ("value", "1");
    assertNotNull (listener.lastEvent);
  }
  
  public void testChangeFiltering ()
  {
    DataBean bean1 = new DataBean ();
    DataBean bean2 = new DataBean ();
    DataBean bean3 = new DataBean ();
    
    SetDataObject bean1Tags = new SetDataObject ();
    SetDataObject bean2Tags = new SetDataObject ();
    SetDataObject bean3Tags = new SetDataObject ();
    
    bean1Tags.add ("client");
    bean2Tags.add ("client");
    bean2Tags.add ("server");
    bean3Tags.add ("display");
    
    bean1.setValue ("tags", bean1Tags);
    bean2.setValue ("tags", bean2Tags);
    bean3.setValue ("tags", bean3Tags);
    
    DataBean root = new DataBean ();
    root.setValue ("bean1", bean1);
    root.setValue ("bean2", bean2);
    root.setValue ("bean3", bean3);
    
    DataObjectView view = new DataObjectView (root);
    
    assertEquals (3, view.size ());
    
    TagFilter clientFilter = new TagFilter ("client");
    TagFilter serverFilter = new TagFilter ("server");
    TagFilter displayFilter = new TagFilter ("display");
    
    view.setFilter (clientFilter);
    assertEquals (2, view.size ());
    
    view.setFilter (NULL_FILTER);
    assertEquals (3, view.size ());
    
    view.setFilter (displayFilter);
    assertEquals (1, view.size ());
    
    UnionFilter<IDataObject> unionFilter = new UnionFilter<IDataObject> ();
    
    unionFilter.add (clientFilter);
    
    view.setFilter (unionFilter);
    
    assertEquals (2, view.size ());
    
    unionFilter.add (displayFilter);
   
    view.refilter ();

    assertEquals (3, view.size ());
    
    unionFilter.remove (clientFilter);
    view.refilter ();

    assertEquals (1, view.size ());
    
    unionFilter.clear ();
    unionFilter.add (serverFilter);
    view.refilter ();
    
    assertEquals (1, view.size ());
  }
  
  public class TagFilter implements IFilter<IDataObject>
  {
    private String tag;

    public TagFilter (String tag)
    {
      this.tag = tag;
    }

    public boolean include (IDataObject object)
    {
      return ((SetDataObject)object.getValue ("tags")).contains (tag);
    }
  }
}
