package dsto.dfc.databeans;

import junit.framework.TestCase;

/**
 * @author Matthew Phillips
 */
public class JUTestDataObjectMerger extends TestCase
{
  public static void main (String [] args)
  {
    junit.textui.TestRunner.run (JUTestDataObjectMerger.class);
  }

  public void testMergeFlat ()
  {
    DataBean target = new DataBean ();
    DataBean extra = new DataBean ();

    target.setValue ("name", "Matthew", IDataObject.PERSISTENT);
    target.setValue ("age", 42, IDataObject.TRANSIENT);
    target.setValue ("location", "home", IDataObject.PERSISTENT);

    extra.setValue ("name", "Foobar", IDataObject.TRANSIENT);
    extra.setValue ("age", 24, IDataObject.PERSISTENT);
    extra.setValue ("status", "hungry", IDataObject.PERSISTENT);

    DataObjects.merge (extra, target);

    assertEquals ("Foobar", target.getValue ("name"));
    assertEquals (24, target.getIntValue ("age"));
    assertEquals ("home", target.getValue ("location"));
    assertEquals ("hungry", target.getValue ("status"));

    assertFalse (target.isTransient ("name"));
    assertFalse (target.isTransient ("age"));
    assertFalse (target.isTransient ("location"));
    assertFalse (target.isTransient ("status"));
  }

  public void testMergeBeanTree ()
  {
    DataBean target = new DataBean ();
    DataBean targetChild1 = new DataBean ();

    DataBean extra = new DataBean ();
    DataBean extraChild1 = new DataBean ();
    DataBean extraChild2 = new DataBean ();

    targetChild1.setValue ("from", "target", IDataObject.TRANSIENT);
    targetChild1.setValue ("target", "true", IDataObject.TRANSIENT);
    target.setValue ("name", "Matthew", IDataObject.PERSISTENT);
    target.setValue ("child1", targetChild1, IDataObject.PERSISTENT);

    extraChild1.setValue ("from", "extra", IDataObject.TRANSIENT);
    extraChild1.setValue ("extra", "true", IDataObject.TRANSIENT);
    extraChild2.setValue ("extra", "true", IDataObject.TRANSIENT);
    extra.setValue ("age", 24, IDataObject.PERSISTENT);
    extra.setValue ("name", "Foobar", IDataObject.PERSISTENT);
    extra.setValue ("child1", extraChild1, IDataObject.TRANSIENT);
    extra.setValue ("child2", extraChild2, IDataObject.TRANSIENT);

    DataObjects.merge (extra, target);

    assertEquals ("Foobar", target.getValue ("name"));
    assertEquals ("extra", targetChild1.getValue ("from"));
    assertEquals ("true", targetChild1.getValue ("target"));
    assertEquals ("true", targetChild1.getValue ("extra"));

    IDataBean targetChild2 = (IDataBean)target.getValue ("child2");
    assertNotNull (targetChild2);
    assertEquals ("true", targetChild2.getValue ("extra"));

    assertFalse (target.isTransient ("child1"));
  }

  public void testMergeMap ()
  {
    DataBean target = new DataBean ();
    MapDataObject targetChild1 = new MapDataObject ();
    DataBean extra = new DataBean ();
    MapDataObject extraChild1 = new MapDataObject ();

    targetChild1.setValue ("from", "target");
    targetChild1.setValue ("target", "true");
    target.setValue ("child1", targetChild1, IDataObject.PERSISTENT);

    extraChild1.setValue ("from", "extra");
    extraChild1.setValue ("extra", "true");
    extra.setValue ("child1", extraChild1, IDataObject.PERSISTENT);

    DataObjects.merge (extra, target);

    assertEquals ("extra", targetChild1.getValue ("from"));
    assertEquals ("true", targetChild1.getValue ("target"));
    assertEquals ("true", targetChild1.getValue ("extra"));
  }

  public void testMergeWithFlags ()
  {
    DataBean person1 = new DataBean ();
    person1.setValue ("name", "foobar", IDataObject.PERSISTENT);
    person1.setValue ("age", 42, IDataObject.PERSISTENT);

    DataBean address1 = new DataBean ();
    address1.setValue ("street", "frob", IDataObject.PERSISTENT);
    address1.setValue ("postcode", 5555, IDataObject.TRANSIENT);

    person1.setValue ("address", address1, IDataObject.PERSISTENT);

    // person2: merged over person1
    DataBean person2 = new DataBean ();
    person2.setValue ("age", 24, IDataObject.TRANSIENT);
    person2.setValue ("extra", "x", IDataObject.TRANSIENT);

    DataBean address2 = new DataBean ();
    address2.setValue ("postcode", 7777, IDataObject.PERSISTENT);
    address2.setValue ("extra", "x", IDataObject.TRANSIENT);

    person2.setValue ("address", address2, IDataObject.PERSISTENT);

    // merge with override on into person1Copy
    DataBean person1Copy = (DataBean)DataObjects.deepClone (person1);

    DataObjects.merge (person2, person1Copy, IDataObject.TRANSIENT_OVERRIDE);

    DataBean address1Copy = (DataBean)person1Copy.getBeanValue ("address");

    assertEquals ("foobar", person1Copy.getValue ("name"));
    assertEquals (24, person1Copy.getIntValue ("age"));
    assertTrue (person1Copy.isTransient ("age"));
    assertEquals (7777, address1Copy.getIntValue ("postcode"));
    assertTrue (address1Copy.isTransient ("postcode"));
    assertEquals ("x", address1Copy.getValue ("extra"));

    // merge with override off into person1Copy
    person1Copy = (DataBean)DataObjects.deepClone (person1);

    DataObjects.merge (person2, person1Copy, IDataObject.TRANSIENT);

    address1Copy = (DataBean)person1Copy.getBeanValue ("address");

    assertEquals ("foobar", person1Copy.getValue ("name"));
    assertEquals (42, person1Copy.getIntValue ("age"));
    assertFalse (person1Copy.isTransient ("age"));
    assertEquals (5555, address1Copy.getIntValue ("postcode"));
    assertTrue (address1Copy.isTransient ("postcode"));
    assertEquals ("x", address1Copy.getValue ("extra"));
    assertTrue (address1Copy.isTransient ("extra"));
  }

  /**
   * Test merging into sub-objects using
   * {@link DataObjects#merge(PropertyPath, Object, IDataObject)}.
   */
  public void testMergeSubObjects ()
  {
    DataBean person1 = new DataBean ();
    person1.setValue ("name", "foobar", IDataObject.PERSISTENT);
    person1.setValue ("age", 42, IDataObject.PERSISTENT);

    DataBean address1 = new DataBean ();
    address1.setValue ("street", "frob", IDataObject.PERSISTENT);
    address1.setValue ("postcode", 5555, IDataObject.TRANSIENT);

    person1.setValue ("address", address1, IDataObject.PERSISTENT);

    DataBean address2 = new DataBean ();
    address2.setValue ("postcode", 7777, IDataObject.PERSISTENT);
    address2.setValue ("extra", "x", IDataObject.TRANSIENT);

    // merge a top-level property (equivalent to a simple setValue ()
    DataObjects.merge (new PropertyPath ("name"), "foobarless", person1);

    assertEquals ("foobarless", person1.getValue ("name"));

    // merge a sub property on address
    DataObjects.merge (new PropertyPath ("address/street"), "newstreet", person1);

    assertEquals ("newstreet", address1.getValue ("street"));

    // merge a new address over the old
    DataObjects.merge (new PropertyPath ("address"), address2, person1);

    assertEquals (address2.getValue ("postcode"),
                  DataObjects.getValue (person1, "address/postcode"));
    assertEquals (address2.getValue ("extra"),
                  DataObjects.getValue (person1, "address/extra"));
    assertEquals (address1.getValue ("street"),
                  DataObjects.getValue (person1, "address/street"));

    // merge a primitive value over the address
    DataObjects.merge (new PropertyPath ("address"), "hello!", person1);
    assertEquals ("hello!",
                  DataObjects.getValue (person1, "address"));
  }

  /**
   * Test that using equalise = true with merge () works OK.
   */
  public void testMergeWithEqualise ()
  {
    DataBean person1 = new DataBean ();

    person1.setValue ("name", "foobar");
    person1.setValue ("age", 42);
    person1.setValue ("object", "person1");

    DataBean address1 = new DataBean ();
    address1.setValue ("street", "frob");
    address1.setValue ("postcode", 5555);

    person1.setValue ("address", address1);

    DataBean person2 = new DataBean ();

    person2.setValue ("name", "frodo");
    person2.setValue ("age", 24);
    person2.setValue ("extraNotInSource", "hello"); // not in person1

    DataBean address2 = new DataBean ();
    address2.setValue ("street", "frob");
    address2.setValue ("postcode", 6666);
    address2.setValue ("extraNotInSource", "hello"); // not in person1

    person2.setValue ("address", address2);

    DataObjects.merge (person1, person2, IDataObject.OVERRIDE, true);

    assertEquals ("foobar", person2.getValue ("name"));
    assertEquals (42, person2.getIntValue ("age"));
    assertEquals ("person1", person2.getValue ("object"));

    // check properties not in source were removed from target
    assertEquals (null, person2.getValue ("extraNotInSource"));
    assertEquals (null, address2.getValue ("extraNotInSource"));
  }

  /**
   * Test that merging a tree where a child value in one tree is an
   * IDataBean and it's equivalent at the same path in the other tree
   * is not (e.g. a string). This used to throw a wobbly up until
   * version 1.32 of DataObjects.
   */
  public void testMergeNonBeanOverBean ()
  {
    DataBean bean1 = new DataBean ();

    DataBean beanChild1 = new DataBean ();
    beanChild1.setValue ("name", "foobar");
    beanChild1.setValue ("hello", "there");

    bean1.setValue ("child1", beanChild1);

    DataBean bean2 = new DataBean ();
    bean2.setValue ("child1", "child 1");

    DataObjects.merge (bean1, bean2, IDataObject.OVERRIDE, true);

    assertEquals (DataObjects.getValue (bean2, "child1/name"), "foobar");
  }
}
