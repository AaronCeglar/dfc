package dsto.dfc.databeans.io;

import static dsto.dfc.databeans.DataObjects.objectsEqual;

import java.io.File;
import java.text.SimpleDateFormat;

import junit.framework.TestCase;
import dsto.dfc.databeans.DataBean;
import dsto.dfc.databeans.DataObjects;
import dsto.dfc.databeans.io.DataObjectPersister2.CleanupTask;
import dsto.dfc.util.Scheduler;

/**
 * @author Matthew Phillips
 */
public class JUTestDataObjectPersister2 extends TestCase
{
  private Scheduler scheduler;
  private DataBean frodo;
  private DataBean address;
  private File file;
  private DataObjectPersister2 persister;

  SimpleDateFormat DATE_FORMAT = new SimpleDateFormat ("yyyyMMdd-HHmmssSSS");

  public static void main (String [] args)
  {
    junit.textui.TestRunner.run (JUTestDataObjectPersister2.class);
  }

  @Override
  protected void setUp () throws Exception
  {
    scheduler = new Scheduler ();

    frodo = new DataBean ();

    frodo.setValue ("name", "Frodo");
    frodo.setValue ("age", 42);

    address = new DataBean ();
    address.setValue ("stuff", "ImaginativeValueNotFoundError");

    frodo.setValue ("address", address);

    file = File.createTempFile ("test", ".dbxml");
    file.deleteOnExit ();

    persister = new DataObjectPersister2 (scheduler, frodo, file);
    persister.setSaveInterval (2);
    persister.setMaxSaveInterval (3);


  }

  @Override
  protected void tearDown () throws Exception
  {
    file.delete ();
    persister.dispose ();

//    // delete all previous backup files
//    File[] files = file.getParentFile ().listFiles (new FileFilter ()
//    {
//      public boolean accept (File f)
//      {
//        return Files.getExtension (f).equals ("dbxml");
//      }
//    });
//
//    for (File f : files)
//      f.delete ();
  }


  /**
   * This test is slightly bogus in that it requires second-level
   * thread timing and may fail on a loaded system. Not sure how to
   * improve at this stage.
   */
  public void testAutoPersistence ()
    throws Exception
  {
    // simulate ongoing changes longer than max save limit
    address.setValue ("borked", true);
    Thread.sleep (1000);
    frodo.setValue ("hello", "1");
    Thread.sleep (1000);
    frodo.setValue ("hello", "2");
    Thread.sleep (1000);
    frodo.setValue ("hello", "3");
    Thread.sleep (1000);
    frodo.setValue ("hello", "4");
    Thread.sleep (1000);
    frodo.setValue ("hello", "5");

    // file should be created by now...
    assertTrue ("File created", file.length () > 0);

    // wait for all existing changes to get saved
    Thread.sleep (4000);

    // debug: dump file
    /* BufferedReader reader = new BufferedReader (new FileReader (file.getAbsolutePath ()));
    String line;
    while ((line = reader.readLine()) != null)
    {
      System.out.println (line);
    }

    reader.close ();*/

    DataBean frodo2 = (DataBean)new XmlInput ().readFile (file.getAbsolutePath ());

    assertTrue ("Saved incorrect data", objectsEqual (frodo, frodo2));

    file.delete ();
  }

  public void testBackups () throws Exception
  {
    persister.dispose ();
    DataObjectPersister2 dop = new DataObjectPersister2 (scheduler, frodo, file, 2000, 4000, 6000, true);

    Thread.sleep (5000); // 5s

    assertEquals (0, dop.getBackups ().length);

    Thread.sleep (1500); // 6.5s

    // assert backup taken after max backup even if no property change
    assertEquals (1, dop.getBackups ().length);

    frodo.setValue ("hello", "barney");

    Thread.sleep (2500);

    // assert no backup taken, just a save
    assertEquals (1, dop.getBackups ().length);

    frodo.setValue ("hello", "fred");
    Thread.sleep (500);
    frodo.setValue ("hello", "wilam");
    Thread.sleep (500);
    frodo.setValue ("hello", "betty");
    Thread.sleep (500);
    frodo.setValue ("hello", "bill");
    Thread.sleep (500);
    frodo.setValue ("hello", "bob");
    Thread.sleep (500);

    // assert no backup taken after prop change inside max backup interval
    assertEquals (1, dop.getBackups ().length);

    Thread.sleep (2500);

    // assert backup taken
    assertEquals (2, dop.getBackups ().length);

    Thread.sleep (6500);

    // assert no more schedules backups scheduled
    assertEquals (2, dop.getBackups ().length);

    // test backups are returned in the correct order
    assertTrue (dop.getBackups () [0].lastModified () > dop.getBackups () [1].lastModified ());

  }


  /**
   * Test that persister is disposed properly when scheduler is shutdown.
   */
  public void testAutoShutdown ()
    throws Exception
  {
    address.setValue ("borked", true);

    scheduler.shutdown ();

    // file should be created on shutdown
    assertTrue ("File created", file.length () > 0);

    DataBean frodo2 = (DataBean)new XmlInput ().readFile (file.getAbsolutePath ());

    assertTrue ("Saved correct data", DataObjects.objectsEqual (frodo, frodo2));
  }

  public void testCleanup () throws Exception
  {
    long now = System.currentTimeMillis ();
    long oneHour = 60 * 60 * 1000;

    // add backups
    for (long i = 0; i < 350; i++)
    {
      String backupFilename = persister.getBackupFilename (file);
      File backup = new File (file.getParent (), backupFilename);
      backup.createNewFile ();
      if (i < 200)
        backup.setLastModified (now - (oneHour * i));
      else if (i < 250)
        backup.setLastModified (now - (oneHour * 24 * (i - 199)));
      else if (i < 350)
        backup.setLastModified (now - (oneHour * 24 * 7 * (i - 249)));

      System.out.println ("JUTestDataObjectPersister2.testCleanup(): create file : " + backup.getName ());

      Thread.sleep (25);
    }

    assertEquals (350, persister.getBackups ().length);

    CleanupTask task = persister.new CleanupTask ();
    task.run ();

    File[] backups = persister.getBackups ();
    System.out.println ("# backups: " + backups.length);
    assertTrue (backups.length > 0);

    // cleanup
    for (File f : backups)
      f.delete ();
  }
}
