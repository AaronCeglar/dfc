package dsto.dfc.databeans;

import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;

public class JUTestSubsetDataObject extends TestCase
{
  public void test ()
  {
    DataBean bean = new DataBean ();
    
    bean.setValue ("value1", "include1");
    bean.setValue ("value2", "include2");
    bean.setValue ("value3", "notinclude1");
    bean.setValue ("value4", "notinclude2");
    
    DataBean subBean1 = new DataBean ();
    subBean1.setValue ("include", "true");
    
    DataBean subBean2 = new DataBean ();
    subBean2.setValue ("include", "false");
    
    bean.setValue ("bean1", subBean1);
    bean.setValue ("bean2", subBean2);
    
    SubsetDataObject subset = new SubsetDataObject (bean, new IFilter ()
    {
      public boolean include (Object value)
      {
        if (value instanceof String)
          return value.toString ().startsWith ("include");
        else
          return ((IDataObject)value).getValue ("include").equals ("true");
      }
    });
    
    assertEquals (set ("value1", "value2", "bean1"), propertySet (subset));
    
    // remove value1
    bean.setValue ("value1", "notinclude");
    
    // add a value
    bean.setValue ("value5", "include");
    
    assertEquals (set ("value5", "value2", "bean1"), propertySet (subset));
    
    // disqualify bean1
    subBean1.setValue ("include", "false");
    assertEquals (set ("value5", "value2"), propertySet (subset));
    
    // qualify bean2
    subBean2.setValue ("include", "true");
    assertEquals (set ("value5", "value2", "bean2"), propertySet (subset));
  }

  private Set propertySet (IDataObject object)
  {
    return DataObjects.propertySet (object);
  }

  private Set set (String string1, String string2)
  {
    HashSet set = new HashSet ();
    
    set.add (string1);
    set.add (string2);
    
    return set;
  }
  
  private Set set (String string1, String string2, String string3)
  {
    HashSet set = new HashSet ();
    
    set.add (string1);
    set.add (string2);
    set.add (string3);
    
    return set;
  }
}
