package dsto.dfc.databeans;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import junit.framework.AssertionFailedError;
import junit.framework.TestCase;

public class JUTestSupersetDataObject extends TestCase
{
  public void testSingle ()
  {
    SupersetDataObject superset = new SupersetDataObject ();
    
    TestListener listener = new TestListener ();
    superset.addPropertyListener (listener);
    
    DataObject object1 = new DataObject (); 
    object1.setValue ("value1", "1 value #1");
    object1.setValue ("value2", "1 value #2");
    
    superset.add (object1);
    
    assertEquals (2, listener.events.size ());
    assertNull (listener.event ("value1").oldValue);
    assertEquals ("1 value #1", listener.event ("value1").newValue);
    assertNull (listener.event ("value2").oldValue);
    assertEquals ("1 value #2", listener.event ("value2").newValue);
    
    assertEquals ("1 value #1", superset.getValue ("value1"));
    assertEquals ("1 value #2", superset.getValue ("value2"));
    
    // add a value
    listener.events.clear ();
    object1.setValue ("value3", "1 value #3");
    assertNull (listener.event ("value3").oldValue);
    assertEquals ("1 value #3", listener.event ("value3").newValue);
    assertEquals ("1 value #3", superset.getValue ("value3"));
    
    // change a value
    listener.events.clear ();
    object1.setValue ("value2", "1 value #2.1");
    assertEquals ("1 value #2", listener.event ("value2").oldValue);
    assertEquals ("1 value #2.1", listener.event ("value2").newValue);
    
    // remove a value
    listener.events.clear ();
    object1.setValue ("value1", null);
    assertEquals ("1 value #1", listener.event ("value1").oldValue);
    assertNull (listener.event ("value1").newValue);
    assertNull (superset.getValue ("value1"));
  }
  
  public void testMulti ()
  {
    SupersetDataObject superset = new SupersetDataObject ();
    
    TestListener listener = new TestListener ();
    superset.addPropertyListener (listener);
    
    DataObject object1 = new DataObject (); 
    object1.setValue ("value1", "1 value #1");
    object1.setValue ("value2", "1 value #2");
    
    DataObject object2 = new DataObject (); 
    object2.setValue ("value3", "2 value #1");
    object2.setValue ("value4", "2 value #2");
    
    superset.add (object1);
    superset.add (object2);
    
    assertEquals (4, listener.events.size ());
    assertNull (listener.event ("value1").oldValue);
    assertEquals ("1 value #1", listener.event ("value1").newValue);
    assertNull (listener.event ("value2").oldValue);
    assertEquals ("1 value #2", listener.event ("value2").newValue);
    
    assertNull (listener.event ("value3").oldValue);
    assertEquals ("2 value #1", listener.event ("value3").newValue);
    assertNull (listener.event ("value4").oldValue);
    assertEquals ("2 value #2", listener.event ("value4").newValue);
    
    assertEquals ("1 value #1", superset.getValue ("value1"));
    assertEquals ("1 value #2", superset.getValue ("value2"));
    assertEquals ("2 value #1", superset.getValue ("value3"));
    assertEquals ("2 value #2", superset.getValue ("value4"));
    
    // remove object 1
    listener.events.clear ();
    
    superset.remove (object1);
    assertEquals (2, listener.events.size ());
    assertNull (listener.event ("value1").newValue);
    assertEquals ("1 value #1", listener.event ("value1").oldValue);
    assertNull (listener.event ("value2").newValue);
    assertEquals ("1 value #2", listener.event ("value2").oldValue);
    
    assertNull (superset.getValue ("value1"));
    assertNull (superset.getValue ("value2"));    
  }
  
  public void testOverlap ()
  {
    SupersetDataObject superset = new SupersetDataObject ();
    
    TestListener listener = new TestListener ();
    superset.addPropertyListener (listener);
    
    DataObject object1 = new DataObject (); 
    object1.setValue ("value1", "1 value #1");
    object1.setValue ("value2", "1 value #2");
    
    DataObject object2 = new DataObject (); 
    object2.setValue ("value2", "2 value #1");
    object2.setValue ("value3", "2 value #2");
    
    superset.add (object1);
    superset.add (object2);
    
    assertEquals (3, listener.events.size ());
    
    // todo should test remove here
  }
  
  static class TestListener implements PropertyListener
  {
    public Set events = new HashSet ();
    
    public void propertyValueChanged (PropertyEvent e)
    {
      events.add (e);
    }

    public PropertyEvent event (Object property)
    {
      PropertyPath path = new PropertyPath (property);
      
      for (Iterator i = events.iterator (); i.hasNext ();)
      {
        PropertyEvent e = (PropertyEvent)i.next ();
        
        if (e.path.equals (path))
          return e;
      }
      
      throw new AssertionFailedError ("No event for " + property);
    }
  }
}
