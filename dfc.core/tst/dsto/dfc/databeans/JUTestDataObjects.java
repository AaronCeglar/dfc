package dsto.dfc.databeans;

import java.io.CharArrayReader;
import java.io.CharArrayWriter;
import java.io.IOException;

import org.jdom.JDOMException;

import org.junit.Test;

import dsto.dfc.databeans.io.XmlInput;
import dsto.dfc.databeans.io.XmlOutput;

import static dsto.dfc.databeans.DataObjects.objectsEqual;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class JUTestDataObjects
{
  @Test
  public void objectEquals () throws IOException, JDOMException, ClassNotFoundException
  {
    IDataObject ido1 = mkdo ();
    IDataObject ido2 = mkdo ();

    assertTrue (objectsEqual (ido1, ido2));

    CharArrayWriter writer = new CharArrayWriter ();
    XmlOutput out = new XmlOutput ();

    out.write (writer, ido2);

    writer.close ();

    char[] ido2xml = writer.toCharArray ();
    System.out.println (writer.toString ());

    CharArrayReader reader = new CharArrayReader (ido2xml);
    XmlInput in = new XmlInput ();

    ido2 = (IDataObject)in.read (reader);

    reader.close ();

    assertTrue (objectsEqual (ido1, ido2));
  }
  
  @Test
  public void make ()
  {
    IDataObject happy = DataObjects.make (1, 2, 3, 4, 5, 6);
    assertTrue (happy.getValue (1).equals (2));
    assertTrue (happy.getValue (3).equals (4));
    assertTrue (happy.getValue (5).equals (6));
    
    happy = DataObjects.make ();
    
    IDataObject unhappy = DataObjects.make (1, 2, 3);
    assertTrue (unhappy.getValue (1).equals (2));
    assertNull (unhappy.getValue (3));
  }

  @Test
  public void testToString () throws Exception
  {
    IDataObject ido = mkdo ();
    ido.setValue ("self", new DataObjectLink (ido)); // add a link-based cycle

    ido.toString ();

    ido = new FancyDataObject ();
    ido.setValue ("self", new DataObjectLink (ido));

    String prefix = "FancyDataObject [self=DataObjectLink(FancyDataObject:";
    String suffix = ")]";
    String actual = ido.toString ();

    assertTrue (actual.startsWith (prefix) && actual.endsWith (suffix));
    assertTrue (actual.length () > prefix.length () + suffix.length ());
  }

  private IDataObject mkdo ()
  {
    FancyDataObject fdo = new FancyDataObject ();

    fdo.setValue ("xy", "0,0");
    fdo.setValue ("wh", "1024,768");
    fdo.setValue (new Integer (1), "one");
    fdo.setValue (new Long (3), 3);
    fdo.setValue (new Double (2.0), 2.0);
    fdo.setValue (new Float (4), 4f);

    FancyDataObject subfdo = new FancyDataObject ();
    subfdo.setValue ("borked", true);
    subfdo.setValue ("stuff", "ImaginitiveValueNotFoundError");
//    subfdo.setValue ("self", fdo); // cycles are not supported
//    subfdo.setValue ("self", new DataObjectLink (fdo)); // links are not supported

    fdo.setValue ("address", subfdo);

    for (int c = 'a'; c < 'a' + 26; c++)
      fdo.setValue (new Character ((char)c), (char)c);

    return fdo;
  }
}
