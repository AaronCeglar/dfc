package dsto.dfc.logging;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.CharArrayWriter;
import java.io.PrintWriter;

import org.junit.Test;

/**
 * Unit tests for the {@link dsto.dfc.logging.Log DFC Log} class.
 *
 * @author weberd
 */
public class JUTestLog
{
  @Test
  public void nullExceptionsInAlarms ()
  {
    CharArrayWriter caw = new CharArrayWriter ();
    PrintWriter pw = new PrintWriter (caw);

    // start of with a normal exception
    String exMsg = "AARRGGHH!!";
    Log.printExceptionMessage (pw, new Exception (exMsg), 100);

    String output = new String (caw.toCharArray ());
    assertEquals ("java.lang.Exception: "+exMsg+"\n", output);

    // try out a diabolical exception with a null message
    caw.reset ();
    Log.printExceptionMessage (pw, new Exception ((String) null), 100);

    output = new String (caw.toCharArray ()).split ("\n")[0];
    assertEquals ("java.lang.Exception: null", output);

    // try a normal exception in an Alarm message
    caw.reset ();
    Log.setErrorOutput (pw);
    Log.add (Log.ALARM, exMsg, "source", new Exception (exMsg));

    output = new String (caw.toCharArray ());
    output = output.split ("\n")[0];
    assertTrue (output.endsWith ("[Alarm] " + exMsg));

    // try a null exception in an Alarm message
    caw.reset ();
    Log.add (Log.ALARM, exMsg, "source", null);

    output = new String (caw.toCharArray ()).split ("\n")[0];
    assertTrue (output.endsWith ("[Alarm] " + exMsg));

    // try a diabolical exception with a null message in an Alarm message
    caw.reset ();
    Log.add (Log.ALARM, exMsg, "source", new Exception ((String) null));

    output = new String (caw.toCharArray ()).split ("\n")[0];
    assertTrue (output.endsWith ("[Alarm] " + exMsg));
  }

  @Test
  public void testLongMessage ()
  {
    try
    {
      StringBuilder buffer = new StringBuilder ();
      for (int i = 0; i < 30000; i++)
        buffer.append (i);
      throw new Exception ("Very long message " + buffer.toString ());
    }
    catch (Exception e2)
    {
      Log.warn ("Long message", this, e2);
    }
  }

  @Test
  public void testNestedException () throws Exception
  {
    StringBuilder builder = new StringBuilder ();
    for (int i = 0; i < 3000; i++)
      builder.append (i);

    try
    {
      throw new Exception ("short level 1");
    }
    catch (Exception e)
    {
      try
      {
        throw new Exception ("long level 2: "+ builder.toString (), e);
      }
      catch (Exception e1)
      {
        try
        {
          throw new Exception ("long level 3: " + builder.toString (), e1);
        }
        catch (Exception e2)
        {
          Log.warn ("Nested exception", this, e2);
        }
      }
    }
  }

}
