package dsto.dfc.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import junit.framework.TestCase;

import dsto.dfc.text.StringUtility;

/**
 * @author Matthew Phillips
 */
public class JUTestMixedTypeComparator extends TestCase
{
  private static final long DAY = 24 * 60 * 60 * 1000;
  
  public static void main (String [] args)
  {
    junit.textui.TestRunner.run (JUTestMixedTypeComparator.class);
  }

  public void testComparator ()
  {
    ArrayList items = new ArrayList ();

    items.add ("ZZZ");
    items.add ("GGG");

    items.add (new Date (3 * DAY));
    items.add (new Integer (2));
    items.add (new Date (1 * DAY));
    items.add (new Integer (1));

    items.add (new Date (2 * DAY));
    items.add (new Blob1 ());
    items.add ("AAA");
    items.add (new Blob2 ());
    items.add (new Integer (3));

    ArrayList correct = new ArrayList ();

    correct.add (new Integer (1));
    correct.add (new Integer (2));
    correct.add (new Integer (3));

    correct.add ("AAA");
    correct.add ("GGG");
    correct.add ("ZZZ");

    correct.add (new Date (1 * DAY));
    correct.add (new Date (2 * DAY));
    correct.add (new Date (3 * DAY));

    /*
     * kind of cheating, but we don't really care about relative
     * order, just that unknown blobs appear at the end
     */
    if (MixedTypeComparator.INSTANCE.compare (new Blob1 (),
                                              new Blob2 ()) <= 0)
    {
      correct.add (new Blob1 ());
      correct.add (new Blob2 ());
    } else
    {
      correct.add (new Blob2 ());
      correct.add (new Blob1 ());
    }

    Collections.sort (items, MixedTypeComparator.INSTANCE);

    assertEquals (stringify (correct), stringify (items));
  }
  
  private static String stringify (ArrayList items)
  {
    return StringUtility.join (items, ",");
  }

  class Blob1
  {
    public String toString ()
    {
      return "blob1";
    }
  }
  
  class Blob2
  {
    public String toString ()
    {
      return "blob2";
    }
  }
}