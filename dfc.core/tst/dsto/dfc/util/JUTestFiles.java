package dsto.dfc.util;

import java.util.ArrayList;

import java.io.StringReader;

import junit.framework.TestCase;

public class JUTestFiles extends TestCase
{
public void testReadLine ()
    throws Exception
  {
    ArrayList lines = new ArrayList ();
    StringReader input = new StringReader
      ("      \n" +
       "line 1\n" +
       "  line 2 # this is not a comment \n" +
       "\n\n\r\r" +
       " # comment!  # \\\r\n" +
       "a continued \\\n" +
       "  line\\\n" +
       "  !\n" +
       "a line with a \\ and a \\n in it\n" +
       "\\"); 
    
    String line;
    while ((line = Files.readScriptLine (input)) != null)
    {
      System.out.println ("line = \"" + line + "\"");
      lines.add (line);
    }
    
    assertEquals (4, lines.size ());
    
    assertEquals ("line 1", lines.get (0));
    assertEquals ("line 2 # this is not a comment", lines.get (1));
    assertEquals ("a continued line!", lines.get (2));
    assertEquals ("a line with a \\ and a \\n in it", lines.get (3));
  }
}
