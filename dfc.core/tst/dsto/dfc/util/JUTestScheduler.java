package dsto.dfc.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import dsto.dfc.util.Scheduler.Task;

import static java.lang.System.currentTimeMillis;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class JUTestScheduler
{
  @Before
  public void setup ()
  {
    // reset global.
    AddTask.total = 0;
  }
  
  @Test
  public void testBasic ()
    throws InterruptedException
  {
    Scheduler scheduler = new Scheduler ();
    
    long now = currentTimeMillis ();

    TestTask immediateTask = new TestTask (now, 0);
    TestTask delayedTask1 = new TestTask (now, 1000);
    TestTask delayedTask2 = new TestTask (now, 2000);
    TestTask delayedTask3 = new TestTask (now, 3000);
    TestTask delayedTask5 = new TestTask (now, 5000);
    
    scheduler.schedule (delayedTask1, delayedTask1.delay);
    scheduler.schedule (delayedTask3, delayedTask3.delay);
    scheduler.schedule (delayedTask5, delayedTask5.delay);
    scheduler.schedule (delayedTask2, delayedTask2.delay);
    scheduler.schedule (immediateTask, immediateTask.delay);
    
    Thread.sleep (4000);

    assertEquals (1, scheduler.getTaskCount ());

    scheduler.shutdown ();

    assertEquals (0, scheduler.getTaskCount ());
    
    assertTrue (immediateTask.wasRun ());
    assertRunAtCorrectTime (immediateTask);
    assertTrue (delayedTask1.wasRun ());
    assertRunAtCorrectTime (delayedTask1);
    assertTrue (delayedTask2.wasRun ());
    assertRunAtCorrectTime (delayedTask2);
    assertTrue (delayedTask3.wasRun ());
    assertRunAtCorrectTime (delayedTask3);
    
    assertFalse (delayedTask5.wasRun ());
    assertTrue ("Existing task disposed", delayedTask5.disposeCalled);
  }
  
  @Test
  public void testEmptyScheduler ()
    throws InterruptedException
  {
    // clean shutdown when unused?
    
    new Scheduler ().shutdown ();

    // now try loading and unloading it before any tasks are run
    
    Scheduler scheduler = new Scheduler ();

    scheduler = new Scheduler ();

    assertEquals (0, scheduler.getTaskCount ());
    
    TestTask delayedTask1 = new TestTask (currentTimeMillis (), 1000);

    scheduler.schedule (delayedTask1);

    assertEquals (1, scheduler.getTaskCount ());
    
    scheduler.unschedule (delayedTask1);
    
    assertEquals (0, scheduler.getTaskCount ());
    
    scheduler.shutdown ();
  }
  
  @Test
  public void testRepeatingTask ()
    throws InterruptedException
  {
    Scheduler scheduler = new Scheduler ();
    
    long now = currentTimeMillis ();
    
    RepeatingTask task = new RepeatingTask ();
    scheduler.schedule (task, 1000);
    
    Thread.sleep (3500);
    
    scheduler.shutdown ();
    
    assertEquals (3, task.calledTimes.size ());
    
    // check call times
    long lastTime = now;
    for (Iterator i = task.calledTimes.iterator (); i.hasNext ();)
    {
      Long time = (Long)i.next ();
      
      long delta = time.longValue () - lastTime;
      assertTrue (delta >= 1000 && delta < 1500);
      
      lastTime = time.longValue ();
    }
  }

  /**
   * This test is intended to ensure that a race condition in scheduling cannot
   * occur, so a task is never scheduled more than once. This is a particular
   * issue with tasks that schedule themselves.
   * 
   * @throws Exception If an exception occurs while repeatedly scheduling and
   *                   unscheduling a {@link RepeatingTask} then it is reported
   *                   and rethrown. A working scheduler will make sure it
   *                   doesn't happen.
   */
  @Test
  public void testRaceCondition () throws Exception
  {
    Scheduler scheduler = new Scheduler ();
    RepeatingTask task = new RepeatingTask (10);
    
    int i = 0;
    try
    {
      for (i = 0; i < 100000; i++)
      {
        System.out.println ("unschedule: " + scheduler.unschedule (task));
        scheduler.schedule (task);
      }
      
      System.out.println (String.format ("Called %d times", task.calledTimes.size ()));
    }
    catch (Exception e)
    {
      System.out.println ("Number of runs: "+ i);
      System.out.println (shortStackTrace (e));
      throw e;
    }
  }
  
  @Test
  public void testAllScheduled ()
  {
    Random rand = new Random ();
    Scheduler scheduler = new Scheduler ();
    int expected = 0;
    for (int i = 0; i < 10000; i++)
    {
      int delay = rand.nextInt (5000);
      expected += delay;
      scheduler.schedule (new AddTask (delay), delay);
    }
    
    scheduler.waitForIdle ();
    
    assertEquals (expected, AddTask.total);
  }

  private String shortStackTrace (Throwable e)
  {
    StringBuilder s = new StringBuilder (e.getClass ().getName ());
    s.append (": ");
    s.append (e.getMessage ());
    s.append ('\n');

    for (StackTraceElement ste : e.getStackTrace ())
    {
      String className = ste.getClassName ();
      
      if (className.startsWith ("sun") || className.startsWith ("java"))
      {
        s.append ("\t...\n");
        break;
      }
      
      s.append ("\tat ");
      s.append (className);
      s.append ('(');
      
      if (ste.isNativeMethod ())
        s.append ("Native Method");
      else
        s.append (ste.getFileName ()).append (':').append (ste.getLineNumber ());
      
      s.append (")\n");
    }
    
    return s.toString ();
  }
  
  private void assertRunAtCorrectTime (TestTask task)
  {
    long delta = task.ranAt - task.shouldRunAt;
    
    assertTrue ("Task not run close enough to correct time", delta < 500);
  }

  private static class AddTask extends Task
  {
    static int total = 0;
    int incr;
    AddTask (int incr)
    {
      this.incr = incr;
    }
    
    public void run ()
    {
      total += incr;
    }
  }

  public static class TestTask extends Scheduler.Task
  {
    public long ranAt;
    public long shouldRunAt;
    public long delay;
    public boolean disposeCalled;

    public TestTask (long now, long delay)
    {
      this.ranAt = 0;
      this.delay = delay;
      this.shouldRunAt = now + delay;
    }

    public void run ()
    {
      ranAt = currentTimeMillis ();
    }

    public boolean wasRun ()
    {
      return ranAt != 0;
    }
    
    public void dispose ()
    {
      disposeCalled = true;
      super.dispose ();
    }
  }
  
  public static class RepeatingTask extends Scheduler.Task
  {
    public List<Long> calledTimes; 
    
    private long delay;
    
    public RepeatingTask ()
    {
      this (1000);
    }
    
    public RepeatingTask (long delay)
    {
      this.delay = delay;
      this.calledTimes = new ArrayList<Long> ();
    }
    
    public void run ()
    {
      calledTimes.add (new Long (currentTimeMillis ()));
      System.err.println ("RepeatingTask.runAt: " + runAt);

      scheduler.schedule (this, delay);
    }
    
    public void dispose ()
    {
      super.dispose ();
    }
  }
}
