package dsto.dfc.util;

/**
 * Title:        Utilities
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:      ITD/DSTO
 * @author Mofeed Shahin
 * @version
 */

import dsto.dfc.util.plugins.PluginManager;

public class testPluginManager
{

  public testPluginManager()
  {
    try{
      PluginManager pluginManager = PluginManager.INSTANCE;
      /**
       * This uses a jar file which will most likely not exist on your machine.
       * So if you really want to test it :
       * 1 ) Create a jar file
       * 2 ) Modify the Manifest so that it has the Tag "Plugin-Class:" which points at the plugin class
       * 3 ) Add a class in the right place which has a "initPlugin()" method in that class.
       */
      pluginManager.addPlugin("d:/sticker/demo/demo.jar");
    }catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  public static void main(String[] args)
  {
    new testPluginManager();
  }
}