package dsto.dfc.text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import junit.framework.TestCase;

import static dsto.dfc.text.StringUtility.cleanFilename;
import static dsto.dfc.text.StringUtility.concat;
import static dsto.dfc.text.StringUtility.pad;

/**
 * @author Matthew Phillips
 * @version
 */
public class JUTestStringUtility extends TestCase
{
  public void testExpandSymbols ()
  {
    Properties props = new Properties ();
    props.setProperty ("foo", "FOO");
    props.setProperty ("bar", "BAR");
    props.setProperty ("h", "Hello");

    String expanded = StringUtility.expandSymbols ("%h, ${foo} and ${bar}", props, "$%", true); 
    
    assertEquals ("Hello, FOO and BAR", expanded);
  }
  
  public void testEscapeChars ()
  {
    String str;
    String escaped;
    
    str = "hello there";
    escaped = StringUtility.escapeCharacters (str, "'", '^');
    assertSame (str, escaped);
    
    str = "hello 'there'";
    escaped = StringUtility.escapeCharacters (str, "'", '^');
    assertEquals ("hello ^'there^'", escaped);
    
    str = "hello 'there'";
    escaped = StringUtility.escapeCharacters (str, "' ", '^');
    assertEquals ("hello^ ^'there^'", escaped);
    
    // test implicit escape of escape char
    str = "hello ^there";
    escaped = StringUtility.escapeCharacters (str, " ", '^');
    assertEquals ("hello^ ^^there", escaped);
  }
  
  public void testJoin ()
  {
    assertEquals ("", StringUtility.join (new ArrayList (), ","));
    
    List list = Arrays.asList (new Integer[]{new Integer (1)});
    assertEquals ("1", StringUtility.join (list, ","));
    
    list = Arrays.asList (new Integer[]{new Integer (1), 
                                        new Integer (2), 
                                        new Integer (3)});
    assertEquals ("1+2+3", StringUtility.join (list, "+"));
  }
  
  public void testConcat ()
  {
    assertEquals ("", concat ());
    
    assertEquals ("1", concat (1));
    
    String host = "localhost";
    int port = 8080;
    String dir = null;
    assertEquals ("http://localhost:8080/path/to/null",
                  concat ("http://", host, ':', port, "/path/to/", dir));
  }
  
  public void testPad ()
  {
    assertEquals ("foobar", pad ("foobar", 2, "X"));
    
    assertEquals ("foobar", pad ("foobar", 6, "X"));
    
    assertEquals ("foobarXYX", pad ("foobar", 9, "XY"));
    
    assertEquals ("foobar ", pad ("foobar", 7, ""));

    assertEquals ("foobar ", pad ("foobar", 7, null));

    assertEquals ("foobar ", pad ("foobar", 7));
  }
  
  public void testCapitaliseAllWords ()
  {
    assertEquals ("", StringUtility.capitaliseAllWords (""));
    assertEquals ("A Dog Runneth", 
                  StringUtility.capitaliseAllWords ("a dog runneth"));
    assertEquals (" Word ", StringUtility.capitaliseAllWords (" word "));
  }
  
  public void testSplit ()
  {
    String input = "3$4$5$";
    char[] delimiters = {'$'};
    String[] expected = {"3$", "4$", "5$"};
    splitAndCheck (input, delimiters, expected);

    input = "3!4!5!";
    delimiters = new char[]{'!'};
    expected = new String[]{"3!", "4!", "5!"};
    splitAndCheck (input, delimiters, expected);
    
    input = "3%4%5%";
    delimiters = new char[]{'%'};
    expected = new String[]{"3%", "4%", "5%"};
    splitAndCheck (input, delimiters, expected);
    
    input = "3*3!4*4!5*5!";
    delimiters = new char[]{'!'};
    expected = new String[]{"3*3!", "4*4!", "5*5!"};
    splitAndCheck (input, delimiters, expected);
    
    input = "3*3!4*4!5*5!";
    delimiters = new char[]{'%', '!'};
    expected = new String[]{"3*3!", "4*4!", "5*5!"};
    splitAndCheck (input, delimiters, expected);
    
    input = "3*3!4%";
    delimiters = new char[]{'%', '!'};
    expected = new String[]{"3*3!", "4%"};
    splitAndCheck (input, delimiters, expected);
    
    input = "3$3*4%4*234!3$";
    delimiters = new char[]{'$','!','%'};
    expected = new String[]{"3$", "3*4%", "4*234!", "3$"};
    splitAndCheck (input, delimiters, expected);

    // test not including delimiters
    expected = new String[]{"3", "3*4", "4*234", "3"};
    String[] substrings = StringUtility.split (input, delimiters, false);
    assertEquals (expected.length, substrings.length);
    for (int i = 0; i < substrings.length; i++)
      assertEquals (expected[i], substrings[i]);
  }

  private void splitAndCheck (String input, char[] delimiters, String[] expected)
  {
    String[] substrings = StringUtility.split (input, delimiters);
    assertEquals (expected.length, substrings.length);
    
    for (int i = 0; i < substrings.length; i++)
      assertEquals (expected[i], substrings[i]);
  }
  
  public void testRemoveIllegalFilenameCharacters ()
  {
    assertEquals ("happy.txt", cleanFilename ("happy.txt"));
    assertEquals ("happy_.txt", cleanFilename ("happy:.txt"));
    assertEquals ("happyX.txt", cleanFilename ("happy:.txt", "X"));
    assertEquals ("happy_________________.txt", 
                  cleanFilename ("happy^:?[]/\\=+<>;\",*|\u0000.txt"));
    for (int i = 1; i < 10; i++)
    {
      assertEquals ("Failed: " + i, "COM" + i + "_", cleanFilename ("COM" + i));
      assertEquals ("Failed: " + i, "LPT" + i + "_", cleanFilename ("LPT" + i));
    }
    for (String name : Arrays.asList ("CON", "NIL", "PRN", "AUX"))
      assertEquals ("Failed on " + name, name + "_", cleanFilename (name));

    try
    {
      cleanFilename ("happy:.txt", ":");
      fail ("Should have picked up that I gave it a bad character to use.");
    } catch (IllegalArgumentException e)
    {
      // pass!
    }
  }
}