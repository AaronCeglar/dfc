package dsto.dfc.text;

import java.util.Calendar;
import java.util.Date;

import java.text.ParseException;

import junit.framework.TestCase;

public class JUTestInputDateFormat extends TestCase
{
  public void testParse () throws ParseException
  {
    InputDateFormat format = new InputDateFormat ();
    
    Date date;
    
    date = format.parse ("10/04/1972");
    assertNotNull (date);
    assertEquals (date (10, 4, 1972), date);
    
    date = format.parse ("10/04/1972 13:25:27");
    assertNotNull (date);
    assertEquals (date (10, 4, 1972, 13, 25, 27), date);
    
    date = format.parse ("10/04");
    
    assertNotNull (date);
    assertEquals (date (10, 4, -1), date);
    
    date = format.parse ("10 april 1972");
    
    assertNotNull (date);
    assertEquals (date (10, 4, 1972), date);
    
    date = format.parse ("april 10 1972");
    
    assertNotNull (date);
    assertEquals (date (10, 4, 1972), date);
    
    date = format.parse ("10/04/1972 12:45");
    
    assertNotNull (date);
    assertEquals (date (10, 4, 1972, 12, 45, 0), date);
    
    date = format.parse ("13:22");
    
    assertNotNull (date);
    assertEquals (date (-1, -1, -1, 13, 22, 0), date);
    
    date = format.parse ("1:30pm");
    
    assertNotNull (date);
    assertEquals (date (-1, -1, -1, 13, 30, 0), date);
    
    date = format.parse ("4pm");
    
    assertNotNull (date);
    assertEquals (date (-1, -1, -1, 16, 0, 0), date);
  }
  
  private static Date date (int day, int month, int year)
  {
    return date (day, month, year, -1, -1, -1);
  }
  
  private static Date date (int day, int month, int year,
                            int hour, int min, int second)
  {
    Calendar correctDate = Calendar.getInstance ();

    correctDate.set (Calendar.MILLISECOND, 0);

    if (second == -1)
      correctDate.set (Calendar.SECOND, 0);
    else
      correctDate.set (Calendar.SECOND, second);
    
    if (hour == -1)
      correctDate.set (Calendar.HOUR_OF_DAY, 0);
    else
      correctDate.set (Calendar.HOUR_OF_DAY, hour);
    
    if (min == -1)
      correctDate.set (Calendar.MINUTE, 0);
    else
      correctDate.set (Calendar.MINUTE, min);
    
    if (year != -1)
      correctDate.set (Calendar.YEAR, year);
    
    if (month != -1)
      correctDate.set (Calendar.MONTH, month - 1);
    
    if (day != -1)
      correctDate.set (Calendar.DATE, day);
    
    return correctDate.getTime ();
  }
}
