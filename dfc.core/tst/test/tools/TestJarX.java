package test.tools;

import java.io.File;
import java.util.Arrays;

import dsto.dfc.tools.JarX;

public class TestJarX
{
  private static final String [] CLASSPATH =
    new String [] {"d:\\development\\dfc\\lib\\jbcl3.0-res-rt.jar",
                   "d:\\development\\dfc\\lib\\jbcl3.0-rt.jar",
                   "d:\\development\\dfc\\lib\\oromatcher.zip",
                   "d:\\development\\dfc\\lib\\xml4j.jar",
                   "d:\\development\\dfc\\lib\\xerces.jar",
                   "d:\\development\\dfc\\lib\\kbml-2.x.jar",
                   "d:\\development\\sticker\\classes",
                   "d:\\development\\sticker\\lib\\elvin.zip",
                   "d:\\development\\dfc\\bogus_classes",
                   "d:\\development\\dfc\\classes"};

  private static final String [] ROOTS =
    new String [] {"dsto/elvin/sticker/Sticker.class",
                   "com/ibm/xml/parsers/SAXParser.class",
                   "fr/dyade/koala/xml/kbml/editors/*",
                   "java_cup/*",
                   "dsto*.gif", "images/*.gif"};

  public void run ()
  {
    JarX jarx = new JarX (new File ("d:\\development\\sticker-ziplock.jar"));

    jarx.setClassPath (CLASSPATH);

    jarx.addRoots (Arrays.asList (ROOTS));
//    zipper.addRootRegexps (new String [] {"dsto.*\\.gif"});
    jarx.setMainClassName ("dsto.elvin.sticker.Sticker");

    try
    {
      jarx.run ();
    } catch (Exception ex)
    {
      ex.printStackTrace ();
    }
  }

  public static void main (String[] args)
  {
    TestJarX testJarX = new TestJarX ();

    testJarX.run ();
  }
}
