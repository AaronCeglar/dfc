package test.net;

import java.io.File;
import java.net.URL;

import dsto.dfc.net.URLSearchPath;

/**
 * @author Matthew Phillips
 * @version
 */
public class TestSearchPath
{
  public TestSearchPath ()
  {
    // zip
  }

  public void run ()
  {
    URLSearchPath searchPath = new URLSearchPath ();

    try
    {
      searchPath.addPathEntry (new File ("d:\\temp\\").toURI ().toURL ());
      searchPath.addPathEntry (new URL ("http://picasso/apps/sticker/"));

      // test resolve
      URL url = searchPath.resolveToURL ("sticker.gif");
      System.out.println ("Resolved URL is: " + url.toExternalForm ());

      // test relativizer
      //String relative = searchPath.unresolveToPath (new URL ("file:/d:/temp/sticker.gif"));
      String relative = searchPath.unresolveToPath (new URL ("http://picasso/apps/sticker/sticker.gif"));
      System.out.println ("Relative path is: " + relative);
    } catch (Exception ex)
    {
      ex.printStackTrace ();
    }
  }

  public static void main (String[] args)
  {
    TestSearchPath testSearchPath = new TestSearchPath();
    testSearchPath.run ();
  }
}