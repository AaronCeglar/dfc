package test.net;

import static dsto.dfc.net.Localhost.getNetworkAddress;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import dsto.dfc.net.NoHostNameException;

/**
 *
 * @author weberd
 * @created 24/02/2009
 */
public class JUTestLocalhost
{
  @Test
  public void testGetNetworkAddress () throws NoHostNameException, IOException
  {
    assertNotSame ("127.0.0.1", getNetworkAddress ().getHostAddress ());
  }

  @Test
  public void testIPComparator ()
  {
    List<String> addrs =
      Arrays.asList ("2001:388:40b0:1004:20a:95ff:feba:8ea8", "xyz",
                     "fe80:0:0:0:20a:95ff:feba:8ea8%4", "131.185.4.52",
                     "192.168.0.1", "169.200.0.0", "172.231.10.10",
                     "127.0.0.1", "0:0:0:0:0:0:0:1", "fe80:0:0:0:0:0:0:1%1");
    IPComparator comp = new IPComparator ();
    Collections.sort (addrs, comp);
    for (String addr : addrs)
    {
      System.out.println (addr + " " + comp.category (addr));
    }
    Assert.assertTrue ("127.0.0.1".matches ("^127.*"));
    assertSame ("131.185.4.52", addrs.get (0));
  }

  class IPComparator implements Comparator<String>
  {
    public int compare (String ia1, String ia2)
    {
      int cat1 = category (ia1);
      int cat2 = category (ia2);

      if (cat1 != cat2)
        return cat1 - cat2;
      else
        return ia1.compareTo (ia2);
    }

    public int category (String ip)
    {
      if (ip.startsWith ("fe80")) // link local address
        return 6;
      else if (ip.matches ("^(10\\.|169\\.|172\\.|192\\.).*")) // local addr
        return 5;
      else if (ip.matches ("^0:0:0:0:0:0:0:1")) // loopback address
        return 4;
      else if (ip.matches ("^127.*"))
        return 3;
      else if (ip.matches ("(\\w{1,4}:){7}\\w{1,4}")) // ip v6
        return 2;
      else if (ip.matches ("(\\d{1,3}.){3}\\d{1,3}")) // ip v4
        return 1;
      else
        return 10;
    }
  }
}
