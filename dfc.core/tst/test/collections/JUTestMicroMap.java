package test.collections;

import junit.framework.TestCase;

import dsto.dfc.collections.MicroMap;

/**
 * @author mpp
 * @version $Revision$
 */
public class JUTestMicroMap extends TestCase
{

  /**
   * Constructor for JUTestMicroMap.
   * @param arg0
   */
  public JUTestMicroMap (String arg0)
  {
    super (arg0);
  }

  public void testMap ()
  {
    MicroMap map = new MicroMap (2);
    
    map.put ("hello", "there");
    
    assertEquals ("there", map.get ("hello"));
    
    map.put ("foo", "bar");
    assertEquals ("bar", map.get ("foo"));
    
    map.put ("frob", "flib");
    assertEquals ("flib", map.get ("frob"));
    
    map.put ("foo", "foo2");
    assertEquals ("foo2", map.get ("foo"));
    
    map.remove ("hello");
    assertNull (map.get ("hello"));
    assertEquals ("foo2", map.get ("foo"));
    assertEquals ("flib", map.get ("frob"));
    
    map.put ("hello", "there2");
    assertEquals ("there2", map.get ("hello"));
    
    map.remove ("hello");
    assertNull (map.get ("hello"));
    
    map.remove ("foo");
    assertNull (map.get ("foo"));
  }
  
  public static void main(String[] args)
  {
    junit.textui.TestRunner.run(JUTestMicroMap.class);
  }

}
