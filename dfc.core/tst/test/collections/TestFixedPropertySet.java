package test.collections;

import dsto.dfc.collections.FixedPropertySet;
import dsto.dfc.collections.PropertySets;

public class TestFixedPropertySet
{
  public TestFixedPropertySet ()
  {
    String [] names = new String [] {"color", "size"};
    Object [] values = new Object [] {new String [] {"yellow", "black"}, new Integer (123)};
    FixedPropertySet set = new FixedPropertySet (names, values);

    PropertySets.xmlOutput (System.out, set);
  }

  public static void main (String[] args)
  {
    new TestFixedPropertySet ();
  }
}
