package test.collections;

import java.util.ArrayList;

import dsto.dfc.collections.BasicPropertySet;
import dsto.dfc.collections.PropertySets;

public class TestPropertySet
{
  public TestPropertySet ()
  {
    BasicPropertySet set = new BasicPropertySet ();
    set.addPropertyValue ("name", "Dr. No");
    set.addPropertyValue ("age", new Integer (42));
    ArrayList values = new ArrayList ();
    values.add ("(Not Really)");
    values.add (new Integer (27));

    set.addPropertyValues ("age", values);

//    PropertySets.xmlOutput (System.out, set);

    // test setPropertyValues
//    values = new ArrayList ();
//    values.add (new Integer (1111));
//    set.setPropertyValues ("age", values);
//    PropertySets.xmlOutput (System.out, set);

    set.removePropertyValue ("age", new Integer (42));
    set.removePropertyValue ("age", "(Not Really)");
    set.removePropertyValue ("age", new Integer (27));
    set.addPropertyValue ("age", new Integer (678));
    set.addPropertyValue ("age", "Hello");
    PropertySets.xmlOutput (System.out, set);
  }

  public static void main (String[] args)
  {
    new TestPropertySet();
  }
}


