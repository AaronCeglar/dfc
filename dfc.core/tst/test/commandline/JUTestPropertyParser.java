package test.commandline;

import java.util.Properties;

import dsto.dfc.commandline.PropertyParser;

import junit.framework.TestCase;

/**
 * @author Matthew Phillips
 */
public class JUTestPropertyParser extends TestCase
{

  public static void main (String [] args)
  {
    junit.textui.TestRunner.run (JUTestPropertyParser.class);
  }

  public void testParser ()
  {
    String [] args = new String []
    {"arg1", "-Dtest1=test1", "arg2", "-Dtest2=test2",
     "-Dtest3=test3", "-Dtest3=", "-Dtest4", "-D", "-D=nothing"};
    
    Properties properties = new Properties ();
    
    String [] otherArgs =
      PropertyParser.parseOptions (args, properties);
    
    assertEquals ("test1", properties.get ("test1"));
    assertEquals ("test2", properties.get ("test2"));
    assertNull (properties.get ("test3"));
    assertNull (properties.get ("test4"));
    assertEquals (2, otherArgs.length);
    assertEquals ("arg1", otherArgs [0]);
    assertEquals ("arg2", otherArgs [1]);
  }
}
