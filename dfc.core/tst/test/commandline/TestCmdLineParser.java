package test.commandline;

/**
 * Title:        Command Line Parser
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:
 * @author Mofeed Shahin
 * @version 1.0
 */

import dsto.dfc.commandline.BooleanOption;
import dsto.dfc.commandline.CmdLineParser;
import dsto.dfc.commandline.IllegalOptionValueException;
import dsto.dfc.commandline.IntegerOption;
import dsto.dfc.commandline.Option;
import dsto.dfc.commandline.StringOption;
import dsto.dfc.commandline.UnknownOptionException;

public class TestCmdLineParser
{

  public TestCmdLineParser()
  {
    // zip
  }

  private static void printUsage()
  {
    System.err.println("usage: prog [{-v,--verbose,-verbose}] [{-n,--name,-name} a_name] [{-s,--size,-size} a_number]");
  }

  public static void main(String[] args)
  {
    CmdLineParser cmdLineParser = new CmdLineParser();

    cmdLineParser.addOption(new BooleanOption('v', "verbose"));
    cmdLineParser.addOption(new IntegerOption('s', "size"));
    cmdLineParser.addOption(new StringOption('n', "name"));

    try
    {
      cmdLineParser.parse(args);
    }catch (UnknownOptionException e)
    {
      System.err.println(e.getMessage());
      printUsage();
      System.exit(2);
    }catch (IllegalOptionValueException e)
    {
      System.err.println(e.getMessage());
      printUsage();
      System.exit(2);
    }

    Option[] allOptions = cmdLineParser.getOptions();
    for (int i=0; i<allOptions.length; i++)
    {
      System.out.println(allOptions[i].longForm() + ": " + allOptions[i].getValue());
    }

    String[] otherArgs = cmdLineParser.getRemainingArgs();
    System.out.println("Remaining args: ");
    for (int i=0; i<otherArgs.length; i++)
    {
      System.out.println(otherArgs[i]);
    }
  }
}
