package dsto.dfc.swing.forms;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import dsto.dfc.util.Disposable;
import dsto.dfc.util.IllegalFormatException;

import dsto.dfc.swing.text.Text;
import dsto.dfc.swing.text.ValueConverter;

/**
 * A form editor that can be used to bind JComboBox'es.
 *
 * @author Matthew
 * @version $Revision$
 */
public class JComboBoxFormEditor
  extends AbstractFormEditor
  implements FormEditor, Disposable, ActionListener
{
  private JComboBox comboBox;
  private Object value;
  private Class valueType = null;
  /** The type of item to put into the combo box model. May be null to
      use the same type as the editor value. */
  private Class comboItemType = null;

  public JComboBoxFormEditor ()
  {
    this (new JComboBox ());
  }

  public JComboBoxFormEditor (JComboBox comboBox)
  {
    this (comboBox, null, null);
  }

  public JComboBoxFormEditor (JComboBox comboBox, String description)
  {
    this (comboBox, null, description);
  }

  public JComboBoxFormEditor (JComboBox comboBox,
                              ValueConverter converter,
                              String description)
  {
    this (comboBox, converter, description, null);
  }

  /**
   * Creates a new <code>JComboBoxFormEditor</code> instance.
   *
   * @param comboBox The combo box to bind to.
   * @param converter The value converter to use (may be null).
   * @param description The description of the value being edited (eg
   * "first name")
   * @param comboItemType The type of items in the combo box's model.
   * Used to determine what type to tell the converter to convert to
   * on loading.  May be null for no conversion.
   */
  public JComboBoxFormEditor (JComboBox comboBox,
                              ValueConverter converter,
                              String description,
                              Class comboItemType)
  {
    super (description, converter);

    this.comboBox = comboBox;
    this.comboItemType = comboItemType;

    comboBox.addActionListener (this);
  }

  /**
   * Set the type of item in the combo box.  Default is to use the
   * same type of value as the editor value type.
   */
  public void setComboItemType (Class newValue)
  {
    comboItemType = newValue;
  }

  public void dispose ()
  {
    if (comboBox != null)
    {
      comboBox.removeActionListener (this);
      comboBox = null;
    }
  }

  public void setComboBox (JComboBox newValue)
  {
    if (comboBox != null)
      comboBox.removeActionListener (this);

    this.comboBox = newValue;

    comboBox.addActionListener (this);
  }

  public JComboBox getComboBox ()
  {
    return comboBox;
  }

  // FormEditor interface

  public boolean isDirectEdit ()
  {
    return false;
  }

  public Class getPreferredValueType ()
  {
    // if valueType not set, return Object.class as default
    return valueType == null ? Object.class : valueType;
  }

  public void setPreferredValueType (Class newValueType)
  {
    valueType = newValueType;
  }

  public void commitEdits () throws IllegalFormatException
  {
    try
    {
      if (comboBox.getSelectedItem () != null)
      {
        value =
          Text.convertValue (comboBox.getSelectedItem (),
                                          getPreferredValueType (), converter);

        fireEditCommitted ();
      }
    } catch (IllegalFormatException ex)
    {
      throw new IllegalFormatException (this, ex);
    }
  }

  public void setEditorValue (Object newValue)
    throws IllegalFormatException
  {
    if (value == newValue)
      return;

    Object comboValue = newValue;

    if (comboItemType != null)
    {
      comboValue = Text.convertValue (comboValue,
                                                   comboItemType,
                                                   converter);
    }

    value = newValue;

    // auto-set valueType value is not null and if not already set
    if (value != null && valueType == null)
      valueType = value.getClass ();

    // set selected item & ignore action event
    comboBox.removeActionListener (this);
    comboBox.setSelectedItem (comboValue);
    comboBox.addActionListener (this);
  }

  public Object getEditorValue ()
  {
    return value;
  }

  public Component getEditorComponent ()
  {
    return comboBox;
  }

  // ActionListener interface

  public void actionPerformed (ActionEvent e)
  {
    fireEditCommitRequested ();
  }
}
