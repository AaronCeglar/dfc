package dsto.dfc.swing.forms;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;

import dsto.dfc.util.IllegalFormatException;

/**
 * Wrapper editor into which can be plugged an 'inline' editor which
 * is displayed to the left of a '...'  button, which shows a
 * secondary 'popup' editor in a form dialog.  To use in visual designer,
 * drop inline and popup editors outside of panel, then set inlineEditor
 * and popupEditor properties.
 *
 * @author Matthew
 * @version $Revision$
 *
 * @see dsto.dfc.swing.controls.AbstractPopupEditor
 */
public class PopupFormEditor extends AbstractFormEditorComponent
{
  public static final int EXPAND_NONE = GridBagConstraints.NONE;
  public static final int EXPAND_HORIZONTAL = GridBagConstraints.HORIZONTAL;
  public static final int EXPAND_VERTICAL = GridBagConstraints.VERTICAL;
  public static final int EXPAND_BOTH = GridBagConstraints.BOTH;

  public static final int ALIGN_CENTER_CLIENT = FormDialog.ALIGN_CENTER_CLIENT;
  public static final int ALIGN_CENTER_SCREEN = FormDialog.ALIGN_CENTER_SCREEN;
  public static final int ALIGN_NONE = FormDialog.ALIGN_NONE;

  private static final GridBagConstraints INLINE_EDITOR_CONSTRAINTS =
    new GridBagConstraints (0, 0, 1, 1, 1.0, 1.0,
                            GridBagConstraints.CENTER,
                            GridBagConstraints.BOTH,
                            new Insets (0, 0, 0, 3), 0, 0);

  protected FormEditor inlineEditor;
  protected FormEditor popupEditor;
  protected JButton popupButton = new JButton ("...");
  protected int expandInlineEditor = EXPAND_BOTH;
  protected boolean popupInScrollPane = false;
  protected boolean popupModal = true;
  protected int dialogAlignment = ALIGN_CENTER_CLIENT;
  protected PopupDialog activeDialog = null;
  protected Listener listener = new Listener ();

  public PopupFormEditor ()
  {
    this (new JTextFieldFormEditor (new JTextField (10)),
          new JTextFieldFormEditor (new JTextField ()));
  }

  public PopupFormEditor (FormEditor inlineEditor, FormEditor popupEditor)
  {
    this.inlineEditor = inlineEditor;
    this.popupEditor = popupEditor;

    inlineEditor.addFormEditorListener (listener);

    jbInit ();

    add (inlineEditor.getEditorComponent (), INLINE_EDITOR_CONSTRAINTS, 0);
  }

  public void updateUI ()
  {
    super.updateUI ();

    if (popupEditor.getEditorComponent () instanceof JComponent)
      ((JComponent)popupEditor.getEditorComponent ()).updateUI ();
  }

  public void requestFocus ()
  {
    // redirect focus to inline editor
    if (inlineEditor.getEditorComponent () != null)
      inlineEditor.getEditorComponent ().requestFocus ();
  }

  public void setEditorDescription (String newValue)
  {
    super.setEditorDescription (newValue);

    if (popupButton.getToolTipText () == null)
      popupButton.setToolTipText ("Click to show " + newValue + " window");
  }

  public void setFont (Font font)
  {
    super.setFont (font);

    popupButton.setFont (font);

    if (inlineEditor != null)
      inlineEditor.getEditorComponent ().setFont (font);
  }

  /**
   * The "..." button that shows the popup editor.
   */
  public JButton getPopupButton ()
  {
    return popupButton;
  }

  public boolean isPopupInScrollPane ()
  {
    return popupInScrollPane;
  }

  /**
   * If true, popup editor will be placed into a scroll pane when shown.
   */
  public void setPopupInScrollPane (boolean newValue)
  {
    popupInScrollPane = newValue;
  }

  public boolean isPopupModal ()
  {
    return popupInScrollPane;
  }

  /**
   * If true, popup editor will be in a modal dialog (the default).
   */
  public void setPopupModal (boolean newValue)
  {
    popupModal = newValue;
  }

  /**
   * Set the alignment for the dialog containing the popup editor.
   *
   * @param newValue The new alignment: of ALIGN_CENTER_CLIENT (center the
   * dialog inside the client), ALIGN_CENTER_SCREEN (center the dialog in the
   * screen) or ALIGN_NONE (no centering).
   */
  public void setDialogAlignment (int newValue)
  {
    dialogAlignment = newValue;
  }

  public int getDialogAlignment ()
  {
    return dialogAlignment;
  }

  public void setInlineEditor (FormEditor newValue)
  {
    if (inlineEditor != null)
      inlineEditor.removeFormEditorListener (listener);

    inlineEditor = newValue;

    inlineEditor.addFormEditorListener (listener);

    updateInlineEditor (inlineEditor.getEditorComponent ());
  }

  public FormEditor getInlineEditor ()
  {
    return inlineEditor;
  }

  public void setPopupEditor (FormEditor newValue)
  {
    popupEditor = newValue;
  }

  public FormEditor getPopupEditor ()
  {
    return popupEditor;
  }

  /**
   * Set how the inlineEditor will be sized when extra space is
   * available.
   *
   * @param mode The expansion mode (one of the EXPAND_* constants).
   */
  public void setExpandInlineEditor (int mode)
  {
    expandInlineEditor = mode;

    updateInlineEditor (inlineEditor.getEditorComponent ());
  }

  public int getExpandInlineEditor ()
  {
    return expandInlineEditor;
  }

  /**
   * Show the popup editor.
   */
  public void showPopup ()
  {
    commitEdits ();

    String title = popupEditor.getEditorDescription ();

    if (title == null)
      title = getEditorDescription ();

    if (title == null)
      title = "Customize";

    PopupDialog dialog = new PopupDialog (this, title);

    // try to put popup editor in deferred commit so that editor value
    // does not have to be copied
    if (popupEditor instanceof Form)
    {
      ((Form)popupEditor).setDeferredCommit (true);
      dialog.setDeferredCommit (false);

      // add editor to dialog, and bind to editor value property of this
      dialog.setFormPanel (popupEditor, "editorValue");
      dialog.setEditorValue (this);
    } else
    {
      // can't use deferred commit: flag need to copy value as it goes in dialog
      dialog.setDeferredCommit (false);

      // add editor to dialog
      dialog.setFormPanel (popupEditor);

      dialog.setEditorValue (inlineEditor.getEditorValue ());
    }

    // put editor component in into dialog, maybe wrapped in scroll pane...
    if (popupInScrollPane)
      dialog.setDialogPanel (createScrollPane (popupEditor.getEditorComponent ()));
    else
      dialog.setDialogPanel (popupEditor.getEditorComponent ());

    dialog.pack ();
    dialog.setVisible (true);
  }

  /**
   * Create a scroll pane to wrap the popup editor. Subclasses may override
   * this to customize the scroll pane settings. The default tries to set
   * a reasonable preferred size based on the popup component's preferred
   * size.
   */
  protected JScrollPane createScrollPane (Component popupComponent)
  {
    JScrollPane scrollPane = new JScrollPane (popupComponent);
    Dimension prefSize = new Dimension (popupComponent.getPreferredSize ());

    prefSize.height = Math.min (prefSize.height, 400);
    prefSize.width = Math.min (prefSize.width, 300);

    scrollPane.getViewport ().setPreferredSize (prefSize);

    return scrollPane;
  }

  /**
   * Sets the inline editor and the popup editor to be enabled or disabled.
   */
  public void setEnabled (boolean enabled)
  {
    inlineEditor.getEditorComponent().setEnabled(enabled);
    popupEditor.getEditorComponent().setEnabled(enabled);
    popupButton.setEnabled (enabled);
  }

  // FormEditor interface (delegated to inlineEditor)

  public boolean isDirectEdit ()
  {
    // we can do direct editing IF inline editor is direct edit and we
    // can use Form's deferred commit mode for popup editing
    return inlineEditor.isDirectEdit () && popupEditor instanceof Form;
  }

  public void setEditorValue (Object value) throws IllegalFormatException
  {
    inlineEditor.setEditorValue (value);
  }

  public Object getEditorValue () throws IllegalFormatException
  {
    return inlineEditor.getEditorValue ();
  }

  public void commitEdits ()
  {
    inlineEditor.commitEdits ();
  }

  private void updateInlineEditor (Component component)
  {
    GridBagConstraints constraints =
      (GridBagConstraints)INLINE_EDITOR_CONSTRAINTS.clone ();

    constraints.fill = expandInlineEditor;

    remove (0);
    add (component, constraints, 0);
  }

  private void jbInit ()
  {
    setLayout (new GridBagLayout ());
    popupButton.setToolTipText ("Click to customize");
    popupButton.setMargin (new Insets (2, 4, 2, 4));
    popupButton.addActionListener (listener);
    add (popupButton,
         new GridBagConstraints (1, 0, 1, 1, 0.0, 1.0,
         GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
         new Insets(0, 0, 0, 0), 0, 0));
  }

  class Listener implements FormEditorListener, ActionListener
  {
    public void editCommitted (ChangeEvent e)
    {
      fireEditCommitted ();
    }

    public void editCommitRequested (ChangeEvent e)
    {
      fireEditCommitRequested ();
    }

    public void actionPerformed (ActionEvent e)
    {
      if (activeDialog == null)
        showPopup ();
      else
        activeDialog.toFront ();
    }
  }

  private static final class PopupDialog extends FormDialog
  {
    private PopupFormEditor parent;

    public PopupDialog (PopupFormEditor parent, String title)
    {
      super (parent, title, parent.popupModal);

      this.parent = parent;

      setAlignMode (parent.dialogAlignment);
      setDefaultCloseOperation (JDialog.DISPOSE_ON_CLOSE);

      parent.activeDialog = this;
    }

    public void dispose ()
    {
      if (parent.activeDialog != null)
      {
        // remove editor to stop it being disposed along with form dialog
        removeEditor (parent.popupEditor);

        parent.activeDialog = null;
      }

      super.dispose ();
    }

    public void commitEdits ()
    {
      super.commitEdits ();

      // copy back and fire committed event
      parent.inlineEditor.setEditorValue (parent.popupEditor.getEditorValue ());
      parent.fireEditCommitted ();
    }
  }
}
