package dsto.dfc.swing.forms;

import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.event.ChangeEvent;

import dsto.dfc.util.Copyable;

/**
 * Utility class for managing a set of FormEditorListener's.
 *
 * @version $Revision$
 */
public class BasicFormEditorEventSource implements Serializable, Copyable
{
  private Object source;
  private transient ArrayList listeners;

  public BasicFormEditorEventSource ()
  {
    this.source = this;
  }

  public BasicFormEditorEventSource (Object source)
  {
    this.source = source;
  }

  public void setFormEditorEventSource (Object source)
  {
    this.source = source;
  }

  public synchronized void addFormEditorListener (FormEditorListener l)
  {
    if (listeners == null)
      listeners = new ArrayList ();

    listeners.add (l);
  }

  public synchronized void removeFormEditorListener (FormEditorListener l)
  {
    if (listeners != null)
      listeners.remove (l);
  }

  public Object clone () throws CloneNotSupportedException
  {
    BasicFormEditorEventSource newObject =
      (BasicFormEditorEventSource)super.clone ();

    newObject.listeners = null;

    return newObject;
  }

  /**
   * @deprecated Supports old typo, ise fireEditCommitted instead.
   */
  public void fireEditComitted ()
  {
    fireEditCommitted ();
  }

  public void fireEditCommitted ()
  {
    if (listeners != null)
    {
      ChangeEvent e = new ChangeEvent (source);
      FormEditorListener [] theListeners;

      synchronized (this)
      {
        theListeners = new FormEditorListener [listeners.size ()];
        theListeners = (FormEditorListener [])listeners.toArray (theListeners);
      }

      for (int i = 0; i < theListeners.length; i++)
       theListeners [i].editCommitted (e);
    }
  }

  public void fireEditCommitRequested ()
  {
    if (listeners != null)
    {
      ChangeEvent e = new ChangeEvent (source);
      FormEditorListener [] theListeners;

      synchronized (this)
      {
        theListeners = new FormEditorListener [listeners.size ()];
        theListeners = (FormEditorListener [])listeners.toArray (theListeners);
      }

      for (int i = 0; i < theListeners.length; i++)
       theListeners [i].editCommitRequested (e);
    }
  }
}
