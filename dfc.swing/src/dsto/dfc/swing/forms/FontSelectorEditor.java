package dsto.dfc.swing.forms;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import dsto.dfc.swing.controls.ComboList;
import dsto.dfc.util.IllegalFormatException;

/**
 * Form editor that allows selection of a font.
 *
 * @version $Revision$
 */
public class FontSelectorEditor extends AbstractFormEditorComponent
{
  public static final String [] SIZES =
    new String [] {"6", "8", "10", "12", "14", "16", "18", "20",
                   "22", "24", "28", "32", "36", "40"};

  public static final String [] STYLES =
    new String [] {"Regular", "Italic", "Bold", "Bold Italic"};

  private Font font;
  private boolean loading = false;
  private JLabel fontLabel = new JLabel();
  private JLabel styleLabel = new JLabel();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private ComboList fontList = new ComboList ();
  private ComboList styleList = new ComboList ();
  private JLabel sizeLabel = new JLabel ();
  private ComboList sizeList = new ComboList ();
  JLabel previewLabel = new JLabel ();

  public FontSelectorEditor ()
  {
    sizeList.setListData (SIZES);
    styleList.setListData (STYLES);
    fontList.setListData
      (GraphicsEnvironment.getLocalGraphicsEnvironment ().getAvailableFontFamilyNames ());

    setEditorValue (UIManager.getFont ("Label.font"));

    try
    {
      jbInit ();
    } catch (Exception ex)
    {
      ex.printStackTrace ();
    }
  }

  public Class getPreferredValueType ()
  {
    return Font.class;
  }

  public boolean isDirectEdit ()
  {
    return false;
  }

  public Object getEditorValue ()
  {
    return font;
  }

  public void setEditorValue (Object newValue) throws IllegalFormatException
  {
    if (newValue instanceof Font)
    {
      font = (Font)newValue;

      loadFields ();
      updatePreview ();
    } else
    {
      throw new IllegalFormatException (this, newValue, Font.class);
    }
  }

  public void commitEdits () throws IllegalFormatException
  {
    String familyStr = (String)fontList.getSelectedItem ();
    String styleStr = (String)styleList.getSelectedItem ();
    String sizeStr = (String)sizeList.getSelectedItem ();

    // style
    int style = Font.PLAIN;
    if (styleStr.equalsIgnoreCase ("Bold"))
      style = Font.BOLD;
    else if (styleStr.equalsIgnoreCase ("Italic"))
      style = Font.ITALIC;
    else if (styleStr.equalsIgnoreCase ("Bold Italic"))
      style = (Font.ITALIC | Font.BOLD);

    // size
    int size;

    try
    {
      size = Integer.parseInt (sizeStr);
    } catch (NumberFormatException ex)
    {
      size = 12;
    }

    font = new Font (familyStr, style, size);

    updatePreview ();

    fireEditCommitted ();
  }

  protected void loadFields ()
  {
    loading = true;

    String fontFamily = font.getFamily ();
    fontList.setSelectedItem (fontFamily);

    if (font.getStyle () == Font.BOLD)
      styleList.setSelectedItem ("Bold");
    else if (font.getStyle () == Font.ITALIC)
      styleList.setSelectedItem ("Italic");
    else if (font.getStyle () == (Font.BOLD | Font.ITALIC))
      styleList.setSelectedItem ("Bold Italic");
    else
      styleList.setSelectedItem ("Regular");

    sizeList.setSelectedItem (Integer.toString (font.getSize ()));

    loading = false;
  }

  private void updatePreview ()
  {
    // The following is a bizarre workaround hack for a known bug
    // (4209476) in JDK's up to and including 1.2.2.  If the same
    // family and size are used to create 2 fonts, the second font
    // ignores the style info: the formula below forces the size to
    // change in an invisible way.
    Font dummyFont =
      font.deriveFont ((float) (font.getSize2D () + 0.0001 * font.getStyle ()));

    previewLabel.setFont (dummyFont);
  }

  private void jbInit () throws Exception
  {
    Border border2 = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new java.awt.Color(134, 134, 134)),"Preview"),BorderFactory.createEmptyBorder(5,5,10,5));
    previewLabel.setBorder(border2);
    previewLabel.setMaximumSize(new Dimension(500, 60));
    previewLabel.setMinimumSize(new Dimension(500, 60));
    previewLabel.setPreferredSize(new Dimension(250, 60));
    previewLabel.setHorizontalAlignment(SwingConstants.CENTER);
    previewLabel.setText("The quick brown fox jumps over the lazy dog 0123456789");
    previewLabel.setForeground (Color.black);
    fontLabel.setDisplayedMnemonic('F');
    fontLabel.setLabelFor(fontList);
    fontLabel.setText("Font:");
    this.setLayout(gridBagLayout1);
    styleLabel.setDisplayedMnemonic('Y');
    styleLabel.setLabelFor(styleList);
    styleLabel.setText("Style:");
    fontList.setMinimumSize(new Dimension(150, 100));
    fontList.setVisibleRowCount(5);
    fontList.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        list_actionPerformed(e);
      }
    });
    styleList.setMinimumSize(new Dimension(100, 100));
    styleList.setVisibleRowCount(5);
    styleList.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        list_actionPerformed(e);
      }
    });
    sizeLabel.setDisplayedMnemonic('S');
    sizeLabel.setLabelFor(sizeList);
    sizeLabel.setText("Size:");
    sizeList.setMinimumSize(new Dimension(80, 100));
    sizeList.setVisibleRowCount(5);
    sizeList.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        list_actionPerformed(e);
      }
    });
    previewLabel.setText("The Quick Brown Fox Jumped Over The Lazy Dog 1234567890");
    this.add(fontLabel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    this.add(styleLabel, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    this.add(fontList, new GridBagConstraints(1, 1, 1, 1, 0.5, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 0, 0, 5), 0, 0));
    this.add(styleList, new GridBagConstraints(2, 1, 1, 1, 0.3, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 0, 0, 5), 0, 0));
    this.add(sizeLabel, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    this.add(sizeList, new GridBagConstraints(3, 1, 1, 1, 0.2, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 0, 0, 0), 0, 0));
    this.add(previewLabel, new GridBagConstraints(0, 2, 4, 1, 1.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(6, 0, 0, 0), 0, 0));
  }

  protected void list_actionPerformed (ActionEvent e)
  {
    if (!loading)
    {
      loading = true;

      commitEdits ();

      loading = false;
    }
  }
}