package dsto.dfc.swing.forms;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Image;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;

import dsto.dfc.logging.Log;
import dsto.dfc.swing.icons.Iconic;
import dsto.dfc.util.Disposable;
import dsto.dfc.util.IllegalFormatException;
import dsto.dfc.util.Objects;

/**
 * A form editor that allows selection from a number of different
 * predefined value types.  The possible values are presented in a tab
 * pane, with the editor for each sort of value in each tab.  As the
 * user selects different tabs, the editor value is switched to the
 * value contained in the tab, and the new value's editor is
 * activated, allowing the user to customize the value.<p>
 *
 * Editors may be added by adding "template" editor values via {@link
 * #addEditorValue}, which adds the value as a template for new ones
 * and adds an editor for the value using its BeanInfo customizer. A
 * slightly more intuitive way is to simply add an editor using
 * Component.add (editor, null), ensuring first that the editor has a
 * sensible default value loaded (ie not null). This also allows
 * editors to be added using visual GUI designers.<p>
 *
 * The tab name and icon for the editor are chosen from the icon and
 * display name specified by the editor's BeanInfo. If none are
 * specified, and the editor value is {@link Iconic}, then
 * the icon and name from this is used instead.
 *
 * <b>Example</b><p>
 *
 * This editor could be used in the case where a property of type Book
 * is being edited on a BookOrder bean.  If Book is an interface with
 * two concrete implementations, Novel and Magazine, this editor could
 * be used as follows:
 *
 * <pre>
 *
 * ArrayList bookTemplates = new ArrayList ();
 *
 * bookTemplates.add (new Magazine ("New Magazine"));
 * bookTemplates.add (new Novel ("New Novel"));
 *
 * MultiplexFormEditor bookEditor =
 *   new MultiplexFormEditor (bookTemplates);
 *
 * Form orderForm = ...;
 * BookOrder order = ...;
 *
 * orderForm.addEditor ("book", bookEditor);
 * orderForm.setEditorValue (order);
 *
 * </pre>
 *
 * This would allow the user to select either a Novel or a Magazine as
 * the value of "book" on a BookOrder and to set whatever specific
 * properties are available on each.
 *
 * @version $Revision$
 */
public class MultiplexFormEditor
  extends FormPanel implements FormEditorListener, Disposable
{
  protected JTabbedPane tabPane = new JTabbedPane ();
  protected FormEditor activeEditor;
  protected ArrayList editorValues;
  protected ArrayList editors;
  protected boolean tabListenerEnabled = true;

  public MultiplexFormEditor ()
  {
    this (Collections.EMPTY_LIST);
  }

  /**
   * Creates a new MultiplexFormEditor.
   *
   * @param editorValues The editor template values.  These should be
   * of different classes and must also be cloneable (preferably
   * {@link dsto.dfc.util.Copyable}).  A tab and an editor is
   * created for each value type.  The editor is the value's Bean
   * customizer, which must also implement the FormEditor interface.
   * The name and icon for the tab come either via the {@link
   * Iconic} interface (if implemented) or from the
   * JavaBean 16x16 icon and display name.
   *
   * @see #addEditorValue
   */
  public MultiplexFormEditor (List editorValues)
  {
    this.editorValues = new ArrayList (editorValues);
    this.editors = new ArrayList ();

    createTabsAndEditors ();

    try
    {
      jbInit ();
    } catch (Exception ex)
    {
      ex.printStackTrace ();
    }
  }

  @Override
  public void dispose ()
  {
    if (editors != null)
    {
      for (int i = 0; i < editors.size (); i++)
      {
        Object editor = editors.get (i);

        if (editor instanceof Disposable)
          ((Disposable)editor).dispose ();
      }

      editors = null;
    }

    super.dispose ();
  }

  @Override
  protected void addImpl (Component component, Object constraint, int index)
  {
    if (component instanceof FormEditor)
    {
      FormEditor editor = (FormEditor)component;
      Object value = editor.getEditorValue ();

      if (value != null)
      {
        try
        {
          value = Objects.cloneObject (value);

          editorValues.add (value);
          editors.add (editor);

          createTab (component, value);
        } catch (CloneNotSupportedException ex)
        {
          throw new IllegalArgumentException ("Editor value must be cloneable");
        }
      } else
      {
        throw new IllegalArgumentException ("Editor must have a value");
      }
    } else
    {
      super.addImpl (component, constraint, index);
    }
  }

  /**
   * Add a new editor template value.
   *
   * @see #MultiplexFormEditor(List)
   */
  public void addEditorValue (Object newValue)
  {
    editorValues.add (newValue);

    editors.add (createTabAndEditor (newValue));
  }

  public JTabbedPane getTabPane ()
  {
    return tabPane;
  }

  // FormEditor interface

  @Override
  public boolean isDirectEdit ()
  {
    // must return false because entire object changes when switching tabs.
    // since sub-editors may actually be direct edit, setEditorValue ()
    // copied value as it comes in.
    return false;
  }

  /**
   * Sets the current editor value. If the value (current or new) is
   * unknown, then it will be added to the list of known ones and a new
   * customiser tab panel will be added for it.
   */
  @Override
  public void setEditorValue (Object newValue) throws IllegalFormatException
  {
    if (newValue == null)
    {
      throw new IllegalFormatException (this, "Null values not allowed",
                                        null, Object.class);
    }

    tabListenerEnabled = false;

    // store old value in values list as new default for that value type.
    if (activeEditor != null && activeEditor.getEditorValue () != null)
    {
      Object oldValue = activeEditor.getEditorValue ();

      int oldValueIndex =
        getIndexForValue (oldValue.getClass ());

      editorValues.set (oldValueIndex, oldValue);
    }

    // find new tab index
    int tabIndex = getIndexForValue (newValue.getClass ());

    if (tabIndex == -1)
    {
      tabListenerEnabled = true;

      throw new IllegalFormatException (this, "No editor for value type",
                                              newValue, Object.class);
    }

    // set value in active editor
    try
    {
      activeEditor = (FormEditor)editors.get (tabIndex);

      // if editor is direct edit and not a form in deferred commit mode...
      if (activeEditor.isDirectEdit () &&
           !((activeEditor instanceof Form) && ((Form)activeEditor).isDeferredCommit ()))
      {
        // this editor is never in direct edit mode because entire object
        // may change => need to copy values if active sub-editor will modify
        // value
        try
        {
          newValue = Objects.cloneObject (newValue);
        } catch (CloneNotSupportedException ex)
        {
          Log.diagnostic ("Clone failed", this, ex);

          throw new Error ("Editor value " + newValue.getClass () +
                           " is not cloneable");
        }
      }

      activeEditor.setEditorValue (newValue);
    } catch (ClassCastException ex)
    {
      tabListenerEnabled = true;

      throw new Error ("Customizer is not a form editor: " + newValue);
    }

    // display tab
    tabPane.setSelectedIndex (tabIndex);

    tabListenerEnabled = true;
  }

  @Override
  public Object getEditorValue ()
  {
    return activeEditor.getEditorValue ();
  }

  @Override
  public void commitEdits () throws IllegalFormatException
  {
    if (activeEditor != null)
      activeEditor.commitEdits ();
  }

  @Override
  public Component getEditorComponent ()
  {
    return this;
  }

  // FormEditorListener interface

  public void editCommitted (ChangeEvent e)
  {
    fireEditCommitted ();
  }

  public void editCommitRequested (ChangeEvent e)
  {
    fireEditCommitRequested ();
  }

  /**
   * Adds a FormEditor to the end of the editor list.
   */
  protected void addNewEditor (FormEditor editor)
  {
    editors.add (editors.size (), editor);
  }

  /**
   * Get the index for a given editor class (corresponds to index into
   * strategies list and index of editor tab in tab pane).
   */
  protected int getIndexForValue (Class valueClass)
  {
    for (int i = 0; i < editorValues.size (); i++)
    {
      if (editorValues.get (i).getClass ().equals (valueClass))
        return i;
    }

    return -1;
  }

  /**
   * Called when selected tab changes.
   */
  protected void tabChanged (ChangeEvent e)
  {
    if (!tabListenerEnabled)
      return;

    int index = tabPane.getSelectedIndex ();

    Object newValue = editorValues.get (index);

    setEditorValue (newValue);

    fireEditCommitted ();
  }

  /**
   * Sets up GUI for editor tabs.
   */
  private void createTabsAndEditors ()
  {
    editors.clear ();
    tabPane.removeAll ();

    for (int i = 0; i < editorValues.size (); i++)
    {
      Object value = editorValues.get (i);
      editors.add (createTabAndEditor (value));
    }
  }

  /**
   * Sets up a GUI editor tab for a given value.
   */
  private FormEditor createTabAndEditor (Object value)
  {
    // find customizer using bean introspection
    BeanInfo beanInfo;
    Class editorClass;
    JComponent editor;

    try
    {
      beanInfo = Introspector.getBeanInfo (value.getClass ());
      editorClass =
        beanInfo.getBeanDescriptor ().getCustomizerClass ();

      if (editorClass != null)
        editor = (JComponent)editorClass.newInstance ();
      else
        editor = new PlaceholderEditor ();
    } catch (Exception ex)
    {
      Log.alarm
        ("Exception while processing editor for " + value.getClass (),
         this, ex);

      tabPane.addTab ("!!" + value.getClass ().getName () + "!!", new JPanel ());
      return null;
    }

    createTab (editor, value);

    return (FormEditor)editor;
  }

  private void createTab (Component editor, Object value)
  {
    // decide on an icon and name for the editor
    Icon icon = null;
    String name = null;

    // try to use BeanInfo for icon and name
    try
    {
      BeanInfo beanInfo = Introspector.getBeanInfo (value.getClass ());
      // try to use Bean info
      Image iconImage = beanInfo.getIcon (BeanInfo.ICON_COLOR_16x16);

      if (iconImage != null)
        icon = new ImageIcon (iconImage);

      name = beanInfo.getBeanDescriptor ().getDisplayName ();
    } catch (IntrospectionException ex)
    {
      // zip: can't use BeanInfo
    }

    // use Iconic as fallback for missing name/icon
    if (value instanceof Iconic)
    {
      Iconic iconicValue = (Iconic)value;

      if (icon == null)
        icon = iconicValue.getIcon ();

      if (name == null)
        name = iconicValue.getName ();
    }

    if (name == null)
    {
      // fallback is class name minus package
      name = value.getClass ().toString ();
      name = name.substring (name.lastIndexOf ('.') + 1);
    }

    // do it
    if (editor instanceof JComponent)
      ((JComponent)editor).setBorder (BorderFactory.createEmptyBorder (6, 6, 6, 6));

    tabPane.addTab (name, icon, editor);

    // add form editor listener
    ((FormEditor)editor).addFormEditorListener (this);
  }

  private void jbInit () throws Exception
  {
    this.setLayout (new BorderLayout ());
    tabPane.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        tabChanged (e);
      }
    });
    this.add (tabPane, BorderLayout.CENTER);
  }

  /**
   * Editor class used for values with no customizer.
   */
  private static class PlaceholderEditor extends JLabel implements FormEditor
  {
    private Object value;

    public PlaceholderEditor ()
    {
      super ("No Properties");

      setHorizontalAlignment (JLabel.CENTER);
    }

    // FormEditor interface

    public boolean isDirectEdit ()
    {
      return true;
    }

    public Class getPreferredValueType ()
    {
      return Object.class;
    }

    public void setEditorValue (Object newValue)
    {
      value = newValue;
    }

    public void commitEdits ()
    {
      // zip
    }

    public Object getEditorValue ()
    {
      return value;
    }

    public Component getEditorComponent ()
    {
      return this;
    }

    public String getEditorDescription ()
    {
      return null;
    }

    public void addFormEditorListener (FormEditorListener l)
    {
      // zip: editor never changes value
    }

    public void removeFormEditorListener (FormEditorListener l)
    {
      // zip: editor never changes value
    }
  }
}
