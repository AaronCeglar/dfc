package dsto.dfc.swing.forms;

import java.awt.Component;

import javax.swing.JPanel;

import dsto.dfc.swing.controls.DfcOkCancelDialog;
import dsto.dfc.swing.text.ValueConverter;

import dsto.dfc.util.Disposable;
import dsto.dfc.util.IllegalFormatException;
import dsto.dfc.util.Objects;

/**
 * A Form-compatible extension of DfcOkCancelDialog designed to
 * contain FormEditor's.  Clients should use this class by adding a
 * FormEditor-compatible component as the main dialog panel (via
 * setFormPanel ()) and NOT by using setDialogPanel () in the usual
 * way for DfcOkCancelDialog's.  The main dialog panel will be
 * committed when the user selects either OK or Apply.
 *
 * <p>NOTE: There are two modes this dialog can be used in: bound and
 * non-bound.  In bound mode, the editor inside the dialog is bound to
 * a property of the dialog's editor value.  In this mode, changes to
 * the form dialog's value can be updated automatically when the OK
 * button is pressed (to enable cancelling, FormDialog is set to be in
 * deferred commit mode so that edits are not committed into the form
 * value until the OK or Apply button is pressed even if child editors
 * fire an editCommitted event).  Bound mode is enabled when {@link
 * #setFormPanel(FormEditor,String)} is used. In non bound mode (the
 * default), the editor value for the dialog and the panel is the
 * same, and is cloned when it is passed into the dialog's
 * setEditorValue () method.  In this mode it is up to the client to
 * store the edited value when OK or Apply is pressed.<p>
 *
 * Eg for non bound mode:
 * <pre>
 *   FormDialog dialog = new FormDialog ("Test");
 *   Person person = new Person ("Foo");
 *   // PersonCustomizer is a form editor for Person objects
 *   PersonCustomizer personEditor = new PersonCustomizer ();
 *
 *   dialog.setFormPanel (personEditor);
 *   dialog.setEditorValue (person);  // clones person before setting
 *   dialog.show ();                  // show and edit person
 *
 *   if (!dialog.isCancelled ())
 *   {
 *     Person newPerson = (Person)dialog.getEditorValue ();
 *
 *     // do something with edited newPerson
 *   }
 * </pre>
 *
 * <p>TIP: If no natural class makes sense as the editor value for
 * this dialog in bound mode, clients can implement a special property
 * for loading/saving the editor value.  For example: a command that
 * pops up a FormDialog could provide a "dialogValue" property and
 * bind itself as the form value.
 *
 * @see dsto.dfc.swing.forms.Form
 * @see dsto.dfc.swing.controls.DfcOkCancelDialog
 * @author Matthew
 * @version $Revision$
 */
public class FormDialog extends DfcOkCancelDialog implements Form, Disposable
{
  /** BasicForm instance provides Form interface implementation. */
  private BasicForm form = new BasicForm ();
  private FormEditor formPanel = null;

  public FormDialog (Component client, String title, boolean modal)
  {
    super (client, title, modal);

    form.setDeferredCommit (true);
    form.setFormEditorEventSource (this);

    try
    {
      jbInit ();
      pack ();
    } catch (Exception ex)
    {
      ex.printStackTrace ();
    }
  }

  public FormDialog ()
  {
    this (null, "", false);
  }

  public void dispose ()
  {
    if (formPanel != null)
    {
      form.dispose ();

      if (formPanel instanceof Disposable)
        ((Disposable)formPanel).dispose ();

      // NOTE: not nulling form so that clients can still get editor value
      formPanel = null;
    }

    super.dispose ();
  }

  /**
   * Default implementation: returns java.lang.Object to indicate no
   * preferred type.
   */
  public Class getPreferredValueType ()
  {
    return Object.class;
  }

  public boolean isBoundMode ()
  {
    // assume bound mode if any editors are bound
    return form.getEditorCount () > 0;
  }

  /**
   * Set the main panel for the dialog.
   *
   * @param panel The main panel (must both extend Component and
   * implement the FormEditor interface).
   */
  public void setFormPanel (FormEditor panel)
  {
    setFormPanel (panel, null);
  }

  /**
   * Set the main panel for the dialog.
   *
   * @param panel The main panel (must both extend Component and
   * implement the FormEditor interface).  May be null to unset form panel.
   * @param property The property of the dialog editor value
   * value to bind to the panel.  May be null for no binding.
   */
  public void setFormPanel (FormEditor panel, String property)
  {
    this.formPanel = panel;

    if (panel != null)
    {
      setDialogPanel (panel.getEditorComponent ());

      // TODO: remove old editor (if any)
      if (property != null)
      {
        addEditor (property, panel);
      } else if (getEditorValue () != null)
      {
        // in non-bound mode and value already set: load editor
        panel.setEditorValue (getEditorValue ());
      }
    } else
    {
      setDialogPanel (new JPanel ());
    }
  }

  public FormEditor getFormPanel ()
  {
    return formPanel;
  }

  /**
   * Do not override.
   */
  public boolean saveResult ()
  {
    try
    {
      commitEdits ();

      return true;
    } catch (IllegalFormatException ex)
    {
      BasicForm.handleCommitError (ex);

      return false;
    }
  }

  // Form interface

  public void addEditor (String property, FormEditor editor)
  {
    form.addEditor (property, editor);
  }

  public void addEditor (String property, FormEditor editor,
                         ValueConverter converter)
  {
    form.addEditor (property, editor, converter);
  }

  public boolean removeEditor (FormEditor editor)
  {
    if (editor == formPanel)
    {
      setDialogPanel (null);
      formPanel = null;
    }

    return form.removeEditor (editor);
  }

  public boolean isDeferredCommit ()
  {
    return form.isDeferredCommit ();
  }

  public void setDeferredCommit (boolean newValue)
  {
    form.setDeferredCommit (newValue);
  }

  // FormEditor interface

  public String getEditorDescription ()
  {
    return form.getEditorDescription ();
  }

  public boolean isDirectEdit ()
  {
    return form.isDirectEdit ();
  }

  public void commitEdits ()
    throws IllegalFormatException
  {
    if (!isBoundMode ())
    {
      // if no editors have been bound to properties, commit formPanel
      // (if any) directly.
      if (formPanel != null)
        formPanel.commitEdits ();
    } else
    {
      form.commitEdits ();
    }
  }

  public void setEditorValue (Object newValue) throws IllegalFormatException
  {
    if (!isBoundMode ())
    {
      // in non-bound mode: copy value so that we can cancel edits
      try
      {
        Object editedValue;

        // copy value if editor does direct edit
        if (formPanel != null && formPanel.isDirectEdit ())
          editedValue = Objects.cloneObject (newValue);
        else
          editedValue = newValue;

        form.setEditorValue (editedValue);

        if (formPanel != null)
          formPanel.setEditorValue (editedValue);
      } catch (CloneNotSupportedException ex)
      {
        throw new IllegalFormatException (this, "Value is not cloneable",
                                          newValue, Object.class);
      }
    } else
    {
      // in bound mode: allow form to handle loading
      form.setEditorValue (newValue);
    }
  }

  public Object getEditorValue ()
  {
    return form.getEditorValue ();
  }

  public Component getEditorComponent ()
  {
    return this;
  }

  public void addFormEditorListener (FormEditorListener l)
  {
    form.addFormEditorListener (l);
  }

  public void removeFormEditorListener (FormEditorListener l)
  {
    form.removeFormEditorListener (l);
  }

  private void jbInit () throws Exception
  {
    // zip
  }
}
