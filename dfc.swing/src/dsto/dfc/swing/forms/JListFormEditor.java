package dsto.dfc.swing.forms;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JList;
import javax.swing.ListModel;

import dsto.dfc.swing.list.ListModelAdapter;
import dsto.dfc.util.Disposable;
import dsto.dfc.util.IllegalFormatException;

/**
 * Displays a java.util.List value in a JList component.  Default behaviour
 * is to create a {@link ListModelAdapter} to display the
 * list - override {@link #createListModel} to change this.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class JListFormEditor
  extends AbstractFormEditor implements Disposable
{
  protected JList list;
  protected List editorValue;
  protected boolean directEdit;

  public JListFormEditor (JList list)
  {
    this.list = list;
    this.directEdit = true;

    setEditorValue (new ArrayList ());
  }

  public void dispose ()
  {
    if (list != null)
    {
      disposeModel ();

      list = null;
    }
  }

  protected void disposeModel ()
  {
    if (list.getModel () instanceof Disposable)
    {
      Disposable listModel = (Disposable)list.getModel ();

      listModel.dispose ();
    }
  }

  public Component getEditorComponent ()
  {
    return list;
  }

  public boolean isDirectEdit ()
  {
    return directEdit;
  }

  /**
   * Set whether the editor edits value directly
   * @param newValue
   */
  public void setDirectEdit (boolean newValue)
  {
    directEdit = newValue;
  }

  public Object getEditorValue ()
  {
    return editorValue;
  }

  public void setEditorValue (Object value) throws IllegalFormatException
  {
    try
    {
      editorValue = (List)value;

      disposeModel ();

      list.setModel (createListModel (editorValue));
    } catch (ClassCastException ex)
    {
      throw new IllegalFormatException (this, value, List.class, ex);
    }
  }

  /**
   * Called to create a list model for a list.  Default is to create a
   * {@link ListModelAdapter} - subclasses may override
   * this.
   */
  protected ListModel createListModel (List itemList)
  {
    return new ListModelAdapter (itemList, !directEdit);
  }

  public void commitEdits () throws IllegalFormatException
  {
    // zip
  }
}