package dsto.dfc.swing.forms;

import java.awt.Component;
import java.util.Map;

import javax.swing.JTable;
import javax.swing.table.TableModel;

import dsto.dfc.swing.table.MapTableModel;
import dsto.dfc.util.Disposable;
import dsto.dfc.util.IllegalFormatException;

/**
 * Edits a value in a Swing JTable.  See {@link #createTableModel} for
 * instructions on how to generate a table model for each value.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class JTableFormEditor extends AbstractFormEditor implements Disposable
{
  protected JTable table;
  protected Object editorValue;

  public JTableFormEditor (JTable table)
  {
    this.table = table;
  }

  public void dispose ()
  {
    if (table != null)
    {
      disposeModel ();

      table = null;
    }
  }

  protected void disposeModel ()
  {
    if (table.getModel () instanceof Disposable)
    {
      Disposable model = (Disposable)table.getModel ();

      model.dispose ();
    }
  }

  public Component getEditorComponent ()
  {
    return table;
  }

  public Object getEditorValue ()
  {
    return editorValue;
  }

  /**
   * Loads the editor value into the table by creating a new model with
   * @[link #createTableModel}.
   */
  public void setEditorValue (Object value) throws IllegalFormatException
  {
    TableModel model = createTableModel (value);

    if (model != null)
    {
      if (model != table.getModel ())
        disposeModel ();

      table.setModel (model);
    } else
    {
      throw new IllegalFormatException (this, value, TableModel.class);
    }
  }

  public boolean isDirectEdit ()
  {
    return true;
  }

  public void commitEdits () throws IllegalFormatException
  {
    // zip
  }

  /**
   * Create a table model for a value.  The default implementation knows how
   * to create a MapTableModel for Map's.  Superclasses should override this
   * for other value types.
   *
   * @return The table model for value, or null if no model could be created
   * for the value.
   */
  protected TableModel createTableModel (Object value)
  {
    if (value instanceof Map)
      return new MapTableModel ((Map)value);
    else
      return null;
  }
}