package dsto.dfc.swing.forms;

/**
 * Simple JSlider form editor binding that assumes integers are used as
 * the editor value.
 *
 * @author Mofeed Shahin
 * @version $Revision$
 */
import java.awt.Component;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import dsto.dfc.util.Disposable;
import dsto.dfc.util.IllegalFormatException;

import dsto.dfc.swing.text.ValueConverter;

public class JSliderFormEditor extends AbstractFormEditor
  implements FormEditor, Disposable, ChangeListener
{
  private JSlider jSlider;
  private Object value;

  public JSliderFormEditor(JSlider slider)
  {
    this (slider, null);
  }

  public JSliderFormEditor (JSlider slider, String description)
  {
    super (description, null);
    this.jSlider = slider;
    jSlider.addChangeListener (this);
  }

  public JSliderFormEditor (JSlider slider,
                               ValueConverter converter,
                               String description)
  {
    super (description, converter);

    this.jSlider = slider;

    jSlider.addChangeListener (this);
  }


  public void dispose ()
  {
    if (jSlider != null)
    {
      jSlider.removeChangeListener (this);
      jSlider = null;
    }
  }

  // FormEditor interface

  public boolean isDirectEdit ()
  {
    return false;
  }

  public void commitEdits () throws IllegalFormatException
  {
    value = new Integer(jSlider.getValue());

    fireEditCommitted ();
  }

  public void setEditorValue (Object newValue)
    throws IllegalFormatException
  {
    if (newValue instanceof Integer)
    {
      jSlider.removeChangeListener (this);
      jSlider.setValue (((Integer)newValue).intValue ());
      jSlider.addChangeListener (this);
    } else
    {
      throw new IllegalFormatException (this, newValue, Integer.class);
    }

    value = newValue;
  }

  public Object getEditorValue ()
  {
    return value;
  }

  public Component getEditorComponent ()
  {
    return jSlider;
  }

  // ChangeListener interface

  public void stateChanged (ChangeEvent e)
  {
    commitEdits ();
  }
}