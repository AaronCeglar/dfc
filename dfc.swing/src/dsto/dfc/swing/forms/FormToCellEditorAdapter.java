package dsto.dfc.swing.forms;

import java.awt.Component;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.tree.TreeCellEditor;

/**
 * Allows any FormEditor to a be used in a JTable or JTree by adapting it to
 * the tree/table cell editor interfaces.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class FormToCellEditorAdapter
  extends AbstractCellEditor
  implements FormEditorListener, TableCellEditor, TreeCellEditor
{
  protected FormEditor editor;

  public FormToCellEditorAdapter (FormEditor editor)
  {
    this.editor = editor;
    editor.addFormEditorListener (this);
  }

  public Object getCellEditorValue ()
  {
    return editor.getEditorValue ();
  }

  // FormEditorListener interface

  public void editCommitted (ChangeEvent e)
  {
    fireEditingStopped ();
  }

  public void editCommitRequested (ChangeEvent e)
  {
    fireEditingStopped ();
    editor.commitEdits ();
  }

  // TableCellEditor interface

  public Component getTableCellEditorComponent (JTable table, Object value,
                                                boolean isSelected, int row,
                                                int column)
  {
    editor.setEditorValue (value);

    return editor.getEditorComponent ();
  }


  // TreeCellEditor interface

  public Component getTreeCellEditorComponent (JTree tree, Object value,
                                               boolean isSelected,
                                               boolean expanded,
					       boolean leaf, int row)
  {
    editor.setEditorValue (value);

    return editor.getEditorComponent ();
  }
}