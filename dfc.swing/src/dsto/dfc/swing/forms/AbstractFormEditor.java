package dsto.dfc.swing.forms;

import java.awt.Component;

import dsto.dfc.util.IllegalFormatException;

import dsto.dfc.swing.text.NullValueConverter;
import dsto.dfc.swing.text.ValueConverter;

/**
 * Utility base class for FormEditor's.
 *
 * @version $Revision$
 */
public abstract class AbstractFormEditor
  extends BasicFormEditorEventSource implements FormEditor
{
  protected ValueConverter converter;
  protected String description = null;

  public AbstractFormEditor ()
  {
    this (NullValueConverter.INSTANCE);
  }

  public AbstractFormEditor (ValueConverter converter)
  {
    this.converter = converter;
  }

  public AbstractFormEditor (String description, ValueConverter converter)
  {
    this.description = description;
    this.converter = converter;
  }

  public String getEditorDescription ()
  {
    return description;
  }

  public void setEditorDescription (String newValue)
  {
    description = newValue;
  }

  /**
   * Default implementation: returns java.lang.Object to indicate no
   * preferred type.
   */
  public Class getPreferredValueType ()
  {
    return Object.class;
  }

  public abstract boolean isDirectEdit ();

  public abstract void setEditorValue (Object value)
    throws IllegalFormatException;

  public abstract Object getEditorValue ();

  public abstract Component getEditorComponent ();

  public abstract void commitEdits ()
    throws IllegalFormatException;
}
