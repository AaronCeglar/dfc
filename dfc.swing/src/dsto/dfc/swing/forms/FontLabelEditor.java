package dsto.dfc.swing.forms;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JLabel;

import dsto.dfc.util.IllegalFormatException;

/**
 * Form editor that displays a font as a text description.
 *
 * @see dsto.dfc.swing.forms.FontSelectorEditor
 *
 * @version $Revision$
 */
public class FontLabelEditor extends AbstractFormEditorComponent
{
  protected Font font;
  protected JLabel label = new JLabel ();

  public FontLabelEditor ()
  {
    jbInit ();
  }

  private void jbInit ()
  {
    setLayout (new BorderLayout ());

    add (label, BorderLayout.CENTER);
  }

  public Class getPreferredValueType ()
  {
    return Font.class;
  }

  public boolean isDirectEdit ()
  {
    return false;
  }

  public void commitEdits ()
  {
    // zip
  }

  public Object getEditorValue ()
  {
    return font;
  }

  public void setEditorValue (Object newValue) throws IllegalFormatException
  {
    if (newValue instanceof Font)
    {
      font = (Font)newValue;

      updateLabel ();
    } else
    {
      throw new IllegalFormatException (this, newValue, Font.class);
    }
  }

  protected void updateLabel ()
  {
    String style;

    if (font.getStyle () == Font.BOLD)
      style = "Bold";
    else if (font.getStyle () == Font.ITALIC)
      style = "Italic";
    else if (font.getStyle () == (Font.BOLD | Font.ITALIC))
      style = "Bold Italic";
    else
      style = "Regular";

    StringBuffer fontText = new StringBuffer (40);

    fontText.append (font.getFamily ());
    fontText.append (" ").append (font.getSize ()).append ("pt ");
    fontText.append (style);

    label.setText (fontText.toString ());
  }
}
