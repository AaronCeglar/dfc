package dsto.dfc.swing.forms;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import dsto.dfc.util.Beans;
import dsto.dfc.util.Disposable;
import dsto.dfc.util.IllegalFormatException;

import dsto.dfc.swing.text.Text;
import dsto.dfc.swing.text.ValueConverter;

/**
 * A form editor that can be used to bind any JTextComponent, including
 * JTextField's.
 *
 * @author Matthew
 * @version $Revision$
 */
public class JTextFieldFormEditor
  extends AbstractFormEditor
  implements Disposable, ActionListener
{
  private JTextComponent textField;
  private Object value;
  private Class valueType = null;

  public JTextFieldFormEditor ()
  {
    this (new JTextField ());
  }

  public JTextFieldFormEditor (JTextComponent textField)
  {
    this (textField, null, null);
  }

  public JTextFieldFormEditor (JTextComponent textField, String description)
  {
    this (textField, null, description);
  }

  public JTextFieldFormEditor (JTextComponent textField,
                               ValueConverter converter,
                               String description)
  {
    super (description, converter);

    this.textField = textField;

    // add action listener if supported
    Beans.addListener (ActionListener.class, textField, this);
  }

  public void dispose ()
  {
    if (textField != null)
    {
      Beans.removeListener (ActionListener.class, textField, this);

      textField = null;
    }
  }

  public void setTextField (JTextComponent newValue)
  {
    if (textField != null)
      Beans.removeListener (ActionListener.class, textField, this);

    this.textField = newValue;

    Beans.addListener (ActionListener.class, textField, this);
  }

  public JTextComponent getTextField ()
  {
    return textField;
  }

  // FormEditor interface

  public boolean isDirectEdit ()
  {
    return false;
  }

  public Class getPreferredValueType ()
  {
    // if valueType not set, return Object.class as default
    return valueType == null ? Object.class : valueType;
  }

  public void setPreferredValueType (Class newValueType)
  {
    valueType = newValueType;
  }

  public void commitEdits () throws IllegalFormatException
  {
    try
    {
      if (textField.isEditable ())
      {
        value =
          Text.convertValue (textField.getText (),
                                          getPreferredValueType (), converter);

        fireEditCommitted ();
      }
    } catch (IllegalFormatException ex)
    {
      throw new IllegalFormatException (this, ex);
    }
  }

  public void setEditorValue (Object newValue)
    throws IllegalFormatException
  {
    textField.setText
      ((String)Text.convertValue (newValue,
                                               String.class,
                                               converter));
    value = newValue;

    // auto-set valueType value is not null and if not already set
    if (value != null && valueType == null)
      valueType = value.getClass ();
  }

  public Object getEditorValue ()
  {
    return value;
  }

  public Component getEditorComponent ()
  {
    return textField;
  }

  // ActionListener interface

  public void actionPerformed (ActionEvent e)
  {
    fireEditCommitRequested ();
  }
}
