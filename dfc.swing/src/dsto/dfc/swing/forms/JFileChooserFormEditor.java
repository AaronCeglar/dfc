package dsto.dfc.swing.forms;

import java.awt.BorderLayout;
import java.io.File;

import javax.swing.JFileChooser;

import dsto.dfc.util.IllegalFormatException;

/**
 * FormEditor that wraps a JFileChooser component.  Will accept editor
 * values of either String or File.
 *
 * @author Matthew
 * @version $Revision$
 */
public class JFileChooserFormEditor extends AbstractFormEditorComponent
{
  private Object editorValue;
  private JFileChooser chooser;

  public JFileChooserFormEditor ()
  {
    this (new JFileChooser ());
  }

  public JFileChooserFormEditor (JFileChooser chooser)
  {
    this.chooser = chooser;
    chooser.setControlButtonsAreShown (false);
    setLayout (new BorderLayout ());
    add (chooser, BorderLayout.CENTER);
  }
  
  public JFileChooser getFileChooser ()
  {
    return chooser;
  }

  // FormEditor interface

  public boolean isDirectEdit ()
  {
    return false;
  }

  public void setEditorValue (Object value) throws IllegalFormatException
  {
    editorValue = value;

    File file;

    if (value instanceof File)
      file = (File)value;
    else
      file = new File (value.toString ());

    if (!file.isDirectory () || chooser.isDirectorySelectionEnabled ())
      chooser.setSelectedFile (file);
    else
      chooser.setCurrentDirectory (file);
  }

  public Object getEditorValue ()
  {
    return editorValue;
  }

  public void commitEdits ()
  {
    File file = chooser.getSelectedFile ();

    if (editorValue instanceof String)
      editorValue = file.toString ();
    else
      editorValue = file;

    fireEditCommitted ();
  }
}
