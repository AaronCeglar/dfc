package dsto.dfc.swing.forms;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

import javax.swing.Icon;
import javax.swing.tree.MutableTreeNode;

import dsto.dfc.swing.icons.Iconic;
import dsto.dfc.swing.icons.ImageLoader;
import dsto.dfc.swing.tree.DfcSwingTreeModel;
import dsto.dfc.swing.tree.DfcTreeNode;
import dsto.dfc.util.BasicPropertyEventSource;
import dsto.dfc.util.Disposable;

/**
 * The tree model used by CustomizersPanel.  Clients will generally extend
 * this class to to initialse the editors and groups.
 *
 * @version $Revision$
 * @see CustomizersPanel
 */
public class CustomizersTreeModel
  extends DfcSwingTreeModel implements PropertyChangeListener, Disposable
{
  public static final Icon CUSTOMIZER_ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/prefs16.gif");

  protected HashSet customizers = new HashSet ();
  protected HashMap entryToNode = new HashMap ();

  public CustomizersTreeModel ()
  {
    super ();
  }

  public void dispose ()
  {
    dispose ((DfcTreeNode)getRoot ());
  }

  protected void dispose (DfcTreeNode node)
  {
    Entry entry = (Entry)node.getUserObject ();

    if (entry != null)
      entry.dispose ();

    for (int i = 0; i < node.getChildCount (); i++)
      dispose ((DfcTreeNode)node.getChildAt (i));
  }

  public DfcTreeNode addCustomizer (MutableTreeNode parent,
                                    Entry entry)
  {
    return addCustomizer (parent, entry, parent.getChildCount ());
  }

  /**
   * Add a new customizer.
   *
   * @param parent the parent node to add the customizer to.
   * @param entry The new entry to add.
   * @param index The index for the new child.
   */
  public DfcTreeNode addCustomizer (MutableTreeNode parent,
                                    Entry entry, int index)
  {
    customizers.add (entry.getCustomizer ());

    entry.addPropertyChangeListener (this);

    DfcTreeNode node = addChild (parent, entry, index);

    entryToNode.put (entry, node);

    return node;
  }

  public Collection getCustomizers ()
  {
    return customizers;
  }

  public void propertyChange (PropertyChangeEvent e)
  {
    Entry source = (Entry)e.getSource ();

    if (e.getPropertyName ().equals ("name") ||
        e.getPropertyName ().equals ("icon"))
    {
      DfcTreeNode node = (DfcTreeNode)entryToNode.get (source);

      fireTreeNodeChanged (node);
    }
  }

  /**
   * An entry in the tree model.
   */
  public static class Entry
    extends BasicPropertyEventSource implements Iconic, Disposable
  {
    protected String name;
    protected Icon icon;
    protected Icon largeIcon;
    protected String description;
    protected Component customizer;

    /**
     * Create a new entry.
     * 
     * @param name The name that appears in the tree.
     * @param description The descriptive text that will appear above
     *          the customizer. May be null.
     * @param customizer The customizer component.
     */
    public Entry (String name, String description,
        Component customizer)
    {
      this (name, CUSTOMIZER_ICON, null, description, customizer);
    }

    /**
     * Create a new entry.
     * 
     * @param name The name that appears in the tree.
     * @param icon The icon that appears in the tree.
     * @param largeIcon The large icon that appears in the customizer
     *          title.
     * @param description The descriptive text that will appear above
     *          the customizer. May be null.
     * @param customizer The customizer component.
     */
    public Entry (String name, Icon icon, Icon largeIcon,
        String description, Component customizer)
    {
      this.name = name;
      this.icon = icon;
      this.largeIcon = largeIcon;
      this.description = description;
      this.customizer = customizer;
    }

    public void dispose ()
    {
      if (customizer instanceof Disposable)
        ((Disposable)customizer).dispose ();

      customizer = null;
    }

    public String getName ()
    {
      return name;
    }

    public void setName (String newValue)
    {
      String oldValue = name;

      name = newValue;

      firePropertyChange ("name", oldValue, newValue);
    }

    public Icon getIcon ()
    {
      return icon;
    }

    public void setIcon (Icon newValue)
    {
      Icon oldValue = icon;

      icon = newValue;

      firePropertyChange ("icon", oldValue, newValue);
    }

    public Icon getLargeIcon ()
    {
      return largeIcon;
    }

    public void setLargeIcon (Icon newValue)
    {
      Icon oldValue = largeIcon;

      largeIcon = newValue;

      firePropertyChange ("largeIcon", oldValue, newValue);
    }

    public String getDescription ()
    {
      return description;
    }

    public Component getCustomizer ()
    {
      return customizer;
    }

    public String toString ()
    {
      return name;
    }
  }
}