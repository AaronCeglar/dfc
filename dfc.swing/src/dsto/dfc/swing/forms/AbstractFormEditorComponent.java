package dsto.dfc.swing.forms;

import java.awt.Component;
import java.beans.Customizer;

import javax.swing.JComponent;

import dsto.dfc.util.IllegalFormatException;

/**
 * Abstract base class for JComponent-based FormEditor's.
 *
 * @version $Revision$
 */
public abstract class AbstractFormEditorComponent
  extends JComponent implements FormEditor, Customizer
{
  protected String description = null;
  private BasicFormEditorEventSource editorEventSource =
    new BasicFormEditorEventSource (this);

  public AbstractFormEditorComponent ()
  {
    // zip
  }

  public String getEditorDescription ()
  {
    return description;
  }

  public void setEditorDescription (String newValue)
  {
    description = newValue;
  }

  /**
   * Default implementation: returns java.lang.Object to indicate no
   * preferred type.
   */
  public Class getPreferredValueType ()
  {
    return Object.class;
  }

  public abstract boolean isDirectEdit ();

  public abstract void setEditorValue (Object value) throws IllegalFormatException;

  public abstract Object getEditorValue ();

  /**
   * Default implementation does nothing.
   */
  public void commitEdits () throws IllegalFormatException
  {
    // zip
  }

  public Component getEditorComponent ()
  {
    return this;
  }

  public void addFormEditorListener (FormEditorListener l)
  {
    editorEventSource.addFormEditorListener (l);
  }

  public void removeFormEditorListener (FormEditorListener l)
  {
    editorEventSource.removeFormEditorListener (l);
  }

  /**
   * @deprecated Supports old typo, ise fireEditCommitted instead.
   */
  public void fireEditComitted ()
  {
    fireEditCommitted ();
  }

  public void fireEditCommitted ()
  {
    editorEventSource.fireEditCommitted ();
  }

  public void fireEditCommitRequested ()
  {
    editorEventSource.fireEditCommitRequested ();
  }

  // Customizer interface

  public void setObject (Object bean)
  {
    try
    {
      setEditorValue (bean);
    } catch (IllegalFormatException ex)
    {
      throw new Error
        ("Value is not the correct format for this editor: " + ex);
    }
  }
}
