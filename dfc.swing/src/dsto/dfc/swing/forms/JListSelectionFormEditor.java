package dsto.dfc.swing.forms;

import java.awt.Component;

import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import dsto.dfc.util.Disposable;
import dsto.dfc.util.IllegalFormatException;

/**
 * Form editor that sets the current editor value to the selected value in
 * a JList.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class JListSelectionFormEditor
  extends AbstractFormEditor implements ListSelectionListener, Disposable
{
  protected JList list;

  public JListSelectionFormEditor (JList list)
  {
    this.list = list;

    list.getSelectionModel ().setSelectionMode
      (ListSelectionModel.SINGLE_SELECTION);
  }

  public void dispose ()
  {
    list.getSelectionModel ().removeListSelectionListener (this);
  }

  // ListSelectionListener interface

  public void valueChanged (ListSelectionEvent e)
  {
    fireEditCommitted ();
  }

  // FormEditor interface

  public Component getEditorComponent ()
  {
    return list;
  }

  public void setEditorValue (Object value) throws IllegalFormatException
  {
    list.getSelectionModel ().removeListSelectionListener (this);

    list.setSelectedValue (value, true);

    list.getSelectionModel ().addListSelectionListener (this);
  }

  public Object getEditorValue ()
  {
    return list.getSelectedValue ();
  }

  public boolean isDirectEdit ()
  {
    return false;
  }

  public void commitEdits () throws IllegalFormatException
  {
    // zip
  }
}