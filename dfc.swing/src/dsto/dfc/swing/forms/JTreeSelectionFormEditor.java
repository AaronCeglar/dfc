package dsto.dfc.swing.forms;

import java.awt.Component;

import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;

import dsto.dfc.swing.tree.DfcTree;
import dsto.dfc.util.Disposable;
import dsto.dfc.util.IllegalFormatException;

/**
 * Adapts JTree to act as a single-selection editor (the selected
 * entry in the tree is the editor value).
 *
 * @author Matthew
 * @version $Revision$
 */
public class JTreeSelectionFormEditor extends BasicFormEditorEventSource
  implements FormEditor, Disposable, TreeSelectionListener
{
  private JTree tree;
  private Object editorValue = null;
  private Class treeEntryClass;
  private String description;

  public JTreeSelectionFormEditor (JTree tree)
  {
    this (tree, null);
  }

  public JTreeSelectionFormEditor (JTree tree, String description)
  {
    this (tree, Object.class, description);
  }

  public JTreeSelectionFormEditor (JTree tree,
                                   Class treeEntryClass,
                                   String description)
  {
    this.tree = tree;
    this.treeEntryClass = treeEntryClass;
    this.description = description;

    tree.getSelectionModel ().addTreeSelectionListener (this);
  }

  public void dispose ()
  {
    if (tree != null)
    {
      tree.getSelectionModel ().removeTreeSelectionListener (this);

      if (tree instanceof Disposable)
        ((Disposable)tree).dispose ();

      tree = null;
    }
  }

  public String getEditorDescription ()
  {
    return description;
  }

  public void setEditorDescription (String newValue)
  {
    description = newValue;
  }

  public Class getPreferredValueType ()
  {
    return treeEntryClass;
  }

  public boolean isDirectEdit ()
  {
    return false;
  }

  public void setEditorValue (Object value) throws IllegalFormatException
  {
    editorValue = value;

    selectEntry (editorValue);
  }

  public Object getEditorValue () throws IllegalFormatException
  {
    return editorValue;
  }

  public Component getEditorComponent ()
  {
    return tree;
  }

  public void commitEdits ()
  {
    // zip
  }

  protected void selectEntry (Object entry)
  {
    if (entry != null)
    {
      tree.setSelectionPath
        (new TreePath (DfcTree.getPathForEntry (tree.getModel (), entry)));
    } else
    {
      tree.clearSelection ();
    }
  }

  // TreeSelectionListener interface

  public void valueChanged (TreeSelectionEvent e)
  {
    if (tree.getSelectionCount () == 1)
    {
      Object entry = tree.getSelectionPath ().getLastPathComponent ();

      editorValue = entry;

      fireEditCommitted ();
    }
  }
}
