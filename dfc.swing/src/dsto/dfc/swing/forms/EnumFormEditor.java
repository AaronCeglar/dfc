package dsto.dfc.swing.forms;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.event.ChangeEvent;

import dsto.dfc.swing.controls.EnumComboBox;
import dsto.dfc.swing.controls.RadioBox;
import dsto.dfc.util.IllegalFormatException;

/**
 * A form editor for choosing from an enumerated set of values using
 * either a radio box or combo box.  The editor value can be any
 * object implementing the {@link dsto.dfc.util.EnumerationValue}
 * interface.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class EnumFormEditor extends AbstractFormEditorComponent
{
  private boolean radio = true;
  private FormEditor editor = new RadioBox ();
  private Listener listener = new Listener ();

  public EnumFormEditor ()
  {
    try
    {
      jbInit ();
    } catch (Exception e)
    {
      e.printStackTrace ();
    }

    editor.addFormEditorListener (listener);
  }

  public boolean isRadio ()
  {
    return radio;
  }

  /**
   * Set to true (default) to display a radio box, false to display a
   * combo box.
   * @throws IllegalFormatException 
   */
  public void setRadio (boolean newValue)
  {
    boolean oldValue = radio;

    if (oldValue != newValue)
    {
      Object value = editor.getEditorValue ();

      remove ((Component)editor);
      editor.removeFormEditorListener (listener);

      if (newValue)
      {
        editor = new RadioBox ();
      } else
      {
        editor = new JComboBoxFormEditor (new EnumComboBox ());
      }

      editor.setEditorValue (value);
      editor.addFormEditorListener (listener);

      add ((Component)editor, BorderLayout.CENTER);

      firePropertyChange ("radio", oldValue, newValue);
    }
  }

  public boolean isDirectEdit ()
  {
    return false;
  }

  public void commitEdits ()
    throws IllegalFormatException
  {
    editor.commitEdits ();
  }

  public Object getEditorValue ()
  {
    return editor.getEditorValue ();
  }

  public void setEditorValue (Object value)
    throws IllegalFormatException
  {
    editor.setEditorValue (value);
  }

  private void jbInit() throws Exception
  {
    this.setLayout(new BorderLayout ());
    this.add((Component)editor, BorderLayout.CENTER);
  }

  class Listener implements FormEditorListener
  {
    public void editCommitted (ChangeEvent e)
    {
      fireEditCommitted ();
    }

    public void editCommitRequested (ChangeEvent e)
    {
      fireEditCommitRequested ();
    }
  }
}
