package dsto.dfc.swing.forms;

import java.awt.Component;

import javax.swing.CellEditor;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.tree.TreeCellEditor;

import dsto.dfc.util.IllegalFormatException;

/**
 * Attempts to adapt a JFC TreeCellEditor/TableCellEditor
 * implementation to the DFC FormEditor interface.  Will only work
 * with CellEditor's that do not care what component they are hosted
 * in ie editors that do not need access the JTree or JTable instance
 * that is passed into getTableCellEditorComponent() or
 * getTreeCellEditorComponent() (which will be null).
 *
 * @see dsto.dfc.swing.controls.AbstractCellEditor
 * @version $Revision$
 * @author Matthew
 */
public class CellToFormEditorAdapter
  extends AbstractFormEditor implements CellEditorListener
{
  private CellEditor cellEditor;
  private Component component;
  private boolean inEditingStopped = false;

  public CellToFormEditorAdapter (CellEditor cellEditor)
  {
    this.cellEditor = cellEditor;

    cellEditor.addCellEditorListener (this);
  }

  public boolean isDirectEdit ()
  {
    return false;
  }

  /**
   * Implements the FormEditor.setEditorValue () mathod by calling
   * get*CellEditorComponent().  Incidentally sets the component
   * returned by getEditorComponent().
   */
  public void setEditorValue (Object value) throws IllegalFormatException
  {
    if (cellEditor instanceof TableCellEditor)
    {
      component =
        ((TableCellEditor)cellEditor).getTableCellEditorComponent
          (null, value, false, 0, 0);
    } else if (cellEditor instanceof TreeCellEditor)
    {
      component =
        ((TreeCellEditor)cellEditor).getTreeCellEditorComponent
          (null, value, false, false, true, 0);
    }
  }

  public Object getEditorValue ()
  {
    return cellEditor.getCellEditorValue ();
  }

  public Component getEditorComponent ()
  {
    return component;
  }

  public void commitEdits ()
  {
    if (!inEditingStopped)
      cellEditor.stopCellEditing ();

    fireEditCommitted ();
  }

  // CellEditorListener interface

  public void editingStopped (ChangeEvent e)
  {
    inEditingStopped = true;

    fireEditCommitted ();

    inEditingStopped = false;
  }

  public void editingCanceled (ChangeEvent e)
  {
    // zip: edit cancelled -> no commit
  }
}
