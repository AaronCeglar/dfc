package dsto.dfc.swing.forms;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import dsto.dfc.swing.SwingSupport;
import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.swing.controls.EtchedDividingBorder;
import dsto.dfc.swing.event.SelectionEvent;
import dsto.dfc.swing.icons.ImageLoader;
import dsto.dfc.swing.tree.DfcTree;
import dsto.dfc.swing.tree.DfcTreeNode;
import dsto.dfc.util.Disposable;
import dsto.dfc.util.IllegalFormatException;

/**
 * A generic host for a set of customizers (usually form editors).  Displays a
 * tree of customizer groups on the left, shows the customizer for the
 * selected entry on the right with some optional descriptive text.
 *
 * @version $Revision$
 * @see CustomizersTreeModel
 */
public class CustomizersPanel
  extends JPanel implements PropertyChangeListener, Disposable
{
  public static final Icon CUSTOMIZER_ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/prefs24.gif");

  protected DfcTree customizersTree;
  protected CustomizersTreeModel.Entry activeEntry;
  protected CustomizersTreeModel customizersTreeModel;
  private BorderLayout borderLayout1 = new BorderLayout();
  private JSplitPane splitPane = new JSplitPane();
  private JPanel rightPanel = new JPanel();
  private JPanel customizerContainer = new JPanel();
  private BorderLayout borderLayout3 = new BorderLayout();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private JScrollPane treeScrollPane = new JScrollPane();
  private EtchedDividingBorder etchedDividingBorder = new EtchedDividingBorder();
  private JPanel titlePanel = new JPanel();
  private JLabel customizerLabel = new JLabel();
  private GridBagLayout gridBagLayout2 = new GridBagLayout();
  private JTextArea customizerText = new JTextArea();

  public CustomizersPanel ()
  {
    this (new CustomizersTreeModel ());
  }

  public CustomizersPanel (CustomizersTreeModel customizersTreeModel)
  {
    this.customizersTreeModel = customizersTreeModel;

    customizersTree = new DfcTree (customizersTreeModel);
    customizersTree.expandAll ();

    SwingSupport.killSplitPaneBorder (splitPane);

    CommandView contextView =
      customizersTree.getCommandView (CommandView.CONTEXT_MENU_VIEW);
    contextView.removeCommand ("tree.Expand All");
    contextView.removeCommand ("tree.Collapse");

    /** @todo do this properly */
    treeScrollPane.setMinimumSize (new Dimension (150, 50));
    treeScrollPane.setPreferredSize (new Dimension (150, 50));

    jbInit ();

    // select first entry

    try
    {
      Object firstEntry =
        customizersTreeModel.getChild (customizersTreeModel.getRoot (), 0);

      customizersTree.setSelectedEntry (firstEntry);
    } catch (ArrayIndexOutOfBoundsException ex)
    {
      // no entry => no selection
    }
  }

  public void dispose ()
  {
    if (customizersTreeModel != null)
    {
      customizersTreeModel.dispose ();

      customizersTreeModel = null;
    }
  }

  public void updateUI ()
  {
    super.updateUI ();

    if (customizersTreeModel != null)
    {
      Component activeCustomizer = getActiveCustomizer ();

      for (Iterator i = customizersTreeModel.getCustomizers ().iterator ();
           i.hasNext (); )
      {
        Component customizer = (Component)i.next ();

        if (customizer != activeCustomizer && customizer instanceof JComponent)
        {
          SwingUtilities.updateComponentTreeUI (customizer);
        }
      }
    }
  }

  public Dimension getPreferredSize ()
  {
    Dimension treeSize = treeScrollPane.getPreferredSize ();
    Dimension prefSize = new Dimension (0, 0);

    // aggregate max pref size of customizers into prefSize
    for (Iterator i = customizersTreeModel.getCustomizers ().iterator ();
         i.hasNext (); )
    {
      Component customizer = (Component)i.next ();
      Dimension custSize = customizer.getPreferredSize ();

      prefSize.width = Math.max (prefSize.width, custSize.width);
      prefSize.height = Math.max (prefSize.height, custSize.height);
    }

    // add tree width plus magic number to total width
    prefSize.width += treeSize.width + 20;
    prefSize.height = Math.max (prefSize.height, treeSize.height);

    // adjust for description area
    /** @todo do this properly */
    prefSize.height += 150;

    return prefSize;
  }

  /**
   * Calls commitEdits () on any form editors in the tree.
   */
  public void commitEdits () throws IllegalFormatException
  {
    if (activeEntry != null &&
        activeEntry.getCustomizer () instanceof FormEditor)
    {
      ((FormEditor)activeEntry.getCustomizer ()).commitEdits ();
    }
  }

  public CustomizersTreeModel.Entry getActiveEntry ()
  {
    return activeEntry;
  }

  public Component getActiveCustomizer ()
  {
    return activeEntry != null ? activeEntry.customizer : null;
  }

  /**
   * Activate a customizer
   */
  public boolean activateCustomizer (CustomizersTreeModel.Entry entry)
  {
    if (entry == activeEntry)
      return true;

    try
    {
      commitEdits ();
    } catch (IllegalFormatException ex)
    {
      BasicForm.handleCommitError (ex);

      return false;
    }

    if (activeEntry != null)
      activeEntry.removePropertyChangeListener (this);

    activeEntry = entry;

    activeEntry.addPropertyChangeListener (this);

    updateTitle ();

    customizerContainer.removeAll ();
    customizerContainer.add (entry.getCustomizer (), BorderLayout.CENTER);
    customizerContainer.revalidate ();
    customizerContainer.repaint ();

    return true;
  }

  protected void updateTitle ()
  {
    customizerLabel.setText (activeEntry.getName ());
    customizerLabel.setIcon
      (activeEntry.getLargeIcon () == null ? CUSTOMIZER_ICON :
                                             activeEntry.getLargeIcon ());

    if (activeEntry.getDescription () != null)
    {
      customizerText.setText (activeEntry.getDescription ());
      customizerText.setVisible (true);
    } else
    {
      customizerText.setText ("");
      customizerText.setVisible (false);
    }
  }

  protected void customizersTree_selectionChanged (SelectionEvent e)
  {
    DfcTreeNode selectedNode = (DfcTreeNode)customizersTree.getSelectedEntry ();

    if (selectedNode != null)
    {
      CustomizersTreeModel.Entry customizerEntry =
        (CustomizersTreeModel.Entry)selectedNode.getUserObject ();

      activateCustomizer (customizerEntry);
    }
  }

  public void propertyChange (PropertyChangeEvent e)
  {
    String property = e.getPropertyName ();

    if (property.equals ("name") || property.equals ("description"))
    {
      updateTitle ();
    }
  }

  private void jbInit ()
  {
    customizerText.setFont(UIManager.getFont ("Label.font"));

    this.setLayout(borderLayout1);
    splitPane.setContinuousLayout(true);
    splitPane.setDividerSize(4);
    rightPanel.setLayout(gridBagLayout1);
    customizerContainer.setLayout(borderLayout3);
    rightPanel.setBorder(BorderFactory.createEtchedBorder());
    customizersTree.setShowsRootHandles(false);
    customizersTree.addSelectionListener(new dsto.dfc.swing.event.SelectionListener()
    {
      public void selectionChanged(SelectionEvent e)
      {
        customizersTree_selectionChanged(e);
      }
    });
    customizersTree.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
    customizersTree.setRootVisible(false);
    customizerLabel.setFont(UIManager.getFont ("Label.font").deriveFont (Font.BOLD));
    customizerLabel.setText("Customizer");
    titlePanel.setLayout(gridBagLayout2);
    titlePanel.setBackground(new Color(242, 242, 242));
    titlePanel.setBorder(etchedDividingBorder);
    treeScrollPane.setBorder(BorderFactory.createEtchedBorder());
    customizerText.setColumns(30);
    customizerText.setWrapStyleWord(true);
    customizerText.setLineWrap(true);
    customizerText.setOpaque(false);
    customizerText.setText("Customizer\'s descriptive text");
    customizerText.setEditable(false);
    this.add(splitPane, BorderLayout.CENTER);
    splitPane.add(rightPanel, JSplitPane.RIGHT);
    rightPanel.add(customizerContainer, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(6, 6, 6, 6), 0, 0));
    rightPanel.add(titlePanel, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
    titlePanel.add(customizerLabel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(6, 6, 6, 6), 0, 0));
    titlePanel.add(customizerText, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 6, 6, 6), 0, 0));
    splitPane.add(treeScrollPane, JSplitPane.LEFT);
    treeScrollPane.getViewport().add(customizersTree, null);
  }
}