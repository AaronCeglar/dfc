package dsto.dfc.swing.forms;

import java.util.EventListener;

import javax.swing.event.ChangeEvent;

/**
 * Defines the events fired by a FormEditor.
 *
 * @version $Revision$
 */
public interface FormEditorListener extends EventListener
{
  /**
   * Signals that an edit has been committed into the model.
   */
  public void editCommitted (ChangeEvent e);

  /**
   * Signals that the editor is ready to commit a value into the
   * model.  Parent editors monitor will invoke commitEdits () on the
   * the requesting editor if appropriate.
   */
  public void editCommitRequested (ChangeEvent e);
}
