package dsto.dfc.swing.forms;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;

import dsto.dfc.util.IllegalFormatException;

/**
 * A form editor that can be used to bind JCheckBox's.
 *
 * @author Matthew
 * @version $Revision$
 */
public class JCheckBoxFormEditor
  extends AbstractFormEditor
  implements FormEditor, ActionListener
{
  private JCheckBox checkBox;
  private Object value;

  public JCheckBoxFormEditor ()
  {
    this (new JCheckBox ());
  }

  public JCheckBoxFormEditor (JCheckBox checkBox)
  {
    this (checkBox, null);
  }

  public JCheckBoxFormEditor (JCheckBox checkBox, String description)
  {
    super (description, null);

    this.checkBox = checkBox;

    checkBox.addActionListener (this);
  }

  public void dispose ()
  {
    if (checkBox != null)
    {
      checkBox.removeActionListener (this);
      checkBox = null;
    }
  }

  public void setCheckBox (JCheckBox newValue)
  {
    if (checkBox != null)
      checkBox.removeActionListener (this);

    this.checkBox = newValue;

    checkBox.addActionListener (this);
  }

  public JCheckBox getCheckBox ()
  {
    return checkBox;
  }

  // FormEditor interface

  public boolean isDirectEdit ()
  {
    return false;
  }

  public void commitEdits () throws IllegalFormatException
  {
    // zip
  }

  public void setEditorValue (Object newValue)
    throws IllegalFormatException
  {
    if (newValue instanceof Boolean)
      checkBox.setSelected (((Boolean)newValue).booleanValue ());
    else
      throw new IllegalFormatException (this, newValue, Boolean.class);

    value = newValue;
  }

  public Object getEditorValue ()
  {
    return value;
  }

  public Component getEditorComponent ()
  {
    return checkBox;
  }

  // ActionListener interface

  public void actionPerformed (ActionEvent e)
  {
    value = checkBox.isSelected () ? Boolean.TRUE : Boolean.FALSE;

    fireEditCommitted ();
  }
}
