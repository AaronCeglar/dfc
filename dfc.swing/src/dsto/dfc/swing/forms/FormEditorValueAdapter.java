package dsto.dfc.swing.forms;

import java.awt.Component;

import javax.swing.event.ChangeEvent;

import dsto.dfc.util.Disposable;
import dsto.dfc.util.IllegalFormatException;

import dsto.dfc.swing.text.ValueConverter;

/**
 * A form editor that acts as a simple wrapper around any FormEditor
 * instance.  Allows a ValueConverter to convert to/from the format
 * used by it's wrapped editor.  Note that this type of auto
 * conversion can also be performed by BasicForm's {@link
 * BasicForm#addEditor(java.lang.String,dsto.dfc.swing.forms.FormEditor,dsto.dfc.swing.text.ValueConverter)}
 * method.
 *
 * @version $Revision$
 */
public class FormEditorValueAdapter
  extends AbstractFormEditor
  implements FormEditor, FormEditorListener, Disposable
{
  private FormEditor adaptee;
  private ValueConverter valueConverter;
  private Object editorValue;
  private Class valueClass;

  public FormEditorValueAdapter (FormEditor adaptee, ValueConverter converter)
  {
    this.adaptee = adaptee;
    this.valueConverter = converter;

    adaptee.addFormEditorListener (this);
  }

  public void dispose ()
  {
    if (adaptee != null)
    {
      adaptee.removeFormEditorListener (this);
      adaptee = null;
    }
  }

  public boolean isDirectEdit ()
  {
    return adaptee.isDirectEdit ();
  }

  public Class getPreferredValueType ()
  {
    return adaptee.getPreferredValueType ();
  }

  public void setEditorValue (Object value) throws IllegalFormatException
  {
    valueClass = value.getClass ();
    editorValue = value;

    if (valueConverter != null)
      value = valueConverter.convertTo (value, getPreferredValueType ());

    adaptee.setEditorValue (value);
  }

  public Object getEditorValue ()
  {
    return editorValue;
  }

  public void commitEdits () throws IllegalFormatException
  {
    adaptee.commitEdits ();

    Object value = adaptee.getEditorValue ();

    if (valueConverter != null)
      editorValue = valueConverter.convertTo (value, valueClass);
    else
      editorValue = value;
  }

  public Component getEditorComponent ()
  {
    return adaptee.getEditorComponent ();
  }

  public String getEditorDescription ()
  {
    return adaptee.getEditorDescription ();
  }

  // FormEditorListener interface

  public void editCommitted (ChangeEvent e)
  {
    // forward event to listeners
    fireEditCommitted ();
  }

  public void editCommitRequested (ChangeEvent e)
  {
    // forward event to listeners
    fireEditCommitRequested ();
  }
}
