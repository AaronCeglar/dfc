package dsto.dfc.swing.forms;

import java.awt.Component;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;

import javax.swing.JOptionPane;

import dsto.dfc.swing.commands.BasicPropertiesCommand;
import dsto.dfc.text.StringUtility;

/**
 * Base implementation of the properties command that attempts to
 * show a JavaBean's FormEditor-compliant customizer.
 *
 * @author Matthew
 * @version $Revision$
 */
public abstract class AbstractShowCustomizerCommand
  extends BasicPropertiesCommand
{
  protected Component client;
  protected Class editorClass = null;

  public AbstractShowCustomizerCommand (Component client)
  {
    this.client = client;

    setEnabled (false);
  }

  /**
   * Return true if the value returned by getCustomizerValue () should
   * be directly loaded into its form (the form will be set to deferred
   * commit mode).
   */
  public abstract boolean isDirectEdit ();

  /**
   * The value to be loaded into the customizer.
   */
  public abstract Object getCustomizerValue ();

  /**
   * Save a new value edited by the customizer.  Only needed when
   * isDirectEdit () is false.
   */
  public abstract void setCustomizerValue (Object newValue);

  public void execute ()
  {
    if (editorClass != null)
    {
      try
      {
        FormEditor editor = (FormEditor)editorClass.newInstance ();
        FormDialog dialog = new FormDialog (client, getDialogTitle (), true);

        if (isDirectEdit ())
        {
          dialog.setDeferredCommit (false);

          if (editor instanceof Form)
            ((Form)editor).setDeferredCommit (true);
        }

        dialog.setFormPanel (editor, "customizerValue");
        dialog.setEditorValue (this);

        customizeDialog (dialog);

        dialog.pack ();

        dialog.setVisible (true);
        dialog.dispose ();

      } catch (InstantiationException ex)
      {
        handleEditorCreateError (ex);
      } catch (IllegalAccessException ex)
      {
        handleEditorCreateError (ex);
      } catch (ClassCastException ex)
      {
        // assume this is because customizer is not a FormEditor
        handleEditorCreateError (ex);
      }
    }
  }

  /**
   * Subclasses should call this when the customizerValue property
   * changes.
   */
  protected void customizerValueChanged ()
  {
    Object value = getCustomizerValue ();

    editorClass = null;

    if (value != null)
    {
      editorClass = getEditorClass (value);

      // sanity check for correct type of editor
      if (editorClass != null &&
          !FormEditor.class.isAssignableFrom (editorClass))
      {
        editorClass = null;
      }
    }

    setEnabled (editorClass != null);
  }

  /**
   * Get the editor class for a particular value. The default is to use
   * the editor indicated by BeanDescriptor.getCustomizerClass (), but
   * subclasses may choose to override this.
   *
   * @return The editor class for value. May return null for no editor.
   */
  protected Class getEditorClass (Object value)
  {
    try
    {
      BeanInfo beanInfo = Introspector.getBeanInfo (value.getClass ());

      return beanInfo.getBeanDescriptor ().getCustomizerClass ();
    } catch (IntrospectionException ex)
    {
      // assume no customizerClass available
      return null;
    }
  }

  /**
   * Handle an exception thrown when trying to instantiate the editor.
   */
  protected void handleEditorCreateError (Throwable ex)
  {
    JOptionPane.showMessageDialog
      (client, "Failed to create customizer: " + ex,
       "Customize Error", JOptionPane.ERROR_MESSAGE);
  }

  /**
   * The title displayed on the property dialog.  Default title is based on
   * the editor value's class name.
   */
  protected String getDialogTitle ()
  {
    String className = getCustomizerValue ().getClass ().getName ();

    className = className.substring (className.lastIndexOf ('.') + 1);
    className = StringUtility.wordifyCaps (className);

    return className + " Properties";
  }

  /**
   * Customize the form dialog that holds the editor before display.
   */
  protected void customizeDialog (FormDialog dialog)
  {
    // zip
  }
}
