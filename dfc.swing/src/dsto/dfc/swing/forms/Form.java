package dsto.dfc.swing.forms;

import dsto.dfc.swing.text.ValueConverter;

/**
 * Defines a form that acts as a host for FormEditor's.
 *
 * @author Matthew
 * @version $Revision$
 */
public interface Form extends FormEditor
{
  public void addEditor (String property, FormEditor editor);

  /**
   * Add an editor for a given property of the form value.
   *
   * @param property The property name.
   * @param editor The editor for the property.
   * @param converter The value converter that will be used to convert
   * values into the editor's format (determined by {@link
   * FormEditor#getPreferredValueType} and from the editor back to the
   * property value.  May be null for no conversion.
   *
   * @see #removeEditor
   */
  public void addEditor (String property, FormEditor editor,
                         ValueConverter converter);

  /**
   * Remove a bound form editor.
   *
   * @return True if the editor was removed.
   * @see #addEditor
   */
  public boolean removeEditor (FormEditor editor);

  /**
   * True if editor commit requests from child editors are deferred until
   * commitEdits () is explicitly called on this form.  This can be used to
   * implement forms where the user can apply changes via commitEdits () or
   * cancel edits by simply not calling commitEdits () without having to
   * copy the value being edited by the form.
   *
   * When in deferred commit mode, the form will copy values before
   * passing them to setValue () on "direct edit" child FormEditor's and
   * handle editCommitRequested events from sub-editors by calling commit ()
   * but not storing the subeditor's value.
   */
  public boolean isDeferredCommit ();

  public void setDeferredCommit (boolean newValue);
}
