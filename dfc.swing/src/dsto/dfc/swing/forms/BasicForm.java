package dsto.dfc.swing.forms;

import java.util.ArrayList;
import java.util.Iterator;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;

import dsto.dfc.logging.Log;

import dsto.dfc.swing.text.ValueConverter;

import dsto.dfc.util.Beans;
import dsto.dfc.util.Disposable;
import dsto.dfc.util.IllegalFormatException;
import dsto.dfc.util.Objects;
import dsto.dfc.util.PropertyEventSource;

/**
 * Basic implementation of the Form interface.  Designed to be used
 * either as a base as a delegate.
 *
 * @author Matthew
 * @version $Revision$
 */
public class BasicForm
  extends BasicFormEditorEventSource
  implements Form, Disposable, FormEditorListener, PropertyChangeListener
{
  /** A list of EditorEntry's. */
  private ArrayList editors = new ArrayList ();
  /** The value being edited by the form. */
  private Object formValue;
  public boolean directEdit = true;
  /** True if commits are deferred. */
  private boolean deferredCommit = false;
  /** Set to false to stop form editor commits being copied back into
      model. */
  private boolean unloadEnabled = true;
  /** Set to false to stop property change events triggering editor
    * reloads. */
  private boolean propertyTrackingEnabled = true;
  /** The preferred form value type. */
  private Class preferredValueType = Object.class;

  public BasicForm ()
  {
    // zip
  }

  public void dispose ()
  {
    if (formValue instanceof PropertyEventSource)
      ((PropertyEventSource)formValue).removePropertyChangeListener (this);

    if (editors != null)
    {
      for (int i = 0; i < editors.size (); i++)
      {
        EditorEntry entry = (EditorEntry)editors.get (i);

        entry.getEditor ().removeFormEditorListener (this);

        if (entry.getEditor () instanceof Disposable)
          ((Disposable)entry.getEditor ()).dispose ();
      }

      editors = null;
    }
  }

  public boolean isDirectEdit ()
  {
    return directEdit;
  }

  /**
   * Set whether this editor works in direct edit (default) mode, or indirect.
   * In indirect mode, a copy of the editor value passed to setEditorValue ()
   * is edited and returned from getEditorValue ().
   */
  public void setDirectEdit (boolean newValue)
  {
    directEdit = newValue;
  }

  public Class getPreferredValueType ()
  {
    return preferredValueType;
  }

  public void setPreferredValueType (Class newValueType)
  {
    preferredValueType = newValueType;
  }

  public String getEditorDescription ()
  {
    return null;
  }

  public boolean isDeferredCommit ()
  {
    return deferredCommit;
  }

  public void setDeferredCommit (boolean newValue)
  {
    deferredCommit = newValue;
    unloadEnabled = !deferredCommit;
  }

  public void addEditor (String property, FormEditor editor)
  {
    addEditor (property, editor, null);
  }

  public void addEditor (String property, FormEditor editor,
                         ValueConverter converter)
  {
    editors.add (new EditorEntry (property, editor, converter));

    editor.addFormEditorListener (this);
  }

  public boolean removeEditor (FormEditor editor)
  {
    EditorEntry entry = findEntry (editor);

    if (entry != null)
    {
      editors.remove (entry);
      editor.removeFormEditorListener (this);

      return true;
    } else
      return false;
  }

  public int getEditorCount ()
  {
    return editors.size ();
  }

  protected EditorEntry findEntry (FormEditor editor)
  {
    for (Iterator i = editors.iterator (); i.hasNext (); )
    {
      EditorEntry entry = (EditorEntry)i.next ();

      if (entry.getEditor () == editor)
        return entry;
    }

    return null;
  }

  protected void loadEditors () throws IllegalFormatException
  {
    for (int i = 0; i < editors.size (); i++)
    {
      EditorEntry entry = (EditorEntry)editors.get (i);

      loadEditor (entry);
    }
  }

  protected void unloadEditors () throws IllegalFormatException
  {
    for (int i = 0; i < editors.size (); i++)
    {
      EditorEntry entry = (EditorEntry)editors.get (i);

      unloadEditor (entry);
    }
  }

  protected void loadEditor (EditorEntry entry)
    throws IllegalFormatException
  {
    propertyTrackingEnabled = false;

    try
    {
      Object value =
        Beans.getPropertyValue (formValue, entry.getProperty ());
      entry.propertyClass =
        Beans.getPropertyClass (formValue.getClass (), entry.getProperty ());

      if (deferredCommit && entry.getEditor ().isDirectEdit ())
        value = Objects.cloneObject (value);

      // convert value to editor's preferred form
      if (entry.getConverter () != null)
      {
        value = entry.getConverter ().convertTo
          (value, entry.getEditor ().getPreferredValueType ());
      }

      entry.getEditor ().setEditorValue (value);
    } catch (NoSuchMethodException ex)
    {
      throw new Error ("Could not read property \"" + entry.getProperty () +
                       "\" from class " + formValue.getClass ().getName () +
                       " found in form (exception " + ex.getMessage () + ")");
    } catch (CloneNotSupportedException ex)
    {
      Log.alarm ("Failed to clone value while loading form property \"" +
                 entry.getProperty () + "\"", this, ex);

      /** @todo throw wobbly like this or a (slightly bogus)
          IllegalFormatException? */
      throw new Error ("Unclonable value for property: " + entry.getProperty ());
    } finally
    {
      propertyTrackingEnabled = true;
    }
  }

  protected void unloadEditor (EditorEntry entry)
    throws IllegalFormatException
  {
    Object editorValue = entry.getEditor ().getEditorValue ();

    if (entry.getConverter () != null)
    {
      editorValue = entry.getConverter ().convertTo
        (editorValue, entry.propertyClass);
    }

    try
    {
      // prevent property change triggering a redundant loadEditor () call
      propertyTrackingEnabled = false;
      Beans.setPropertyValue (formValue, entry.getProperty (), editorValue);

    } catch (NoSuchMethodException ex)
    {
      String valueClass = (editorValue == null) ? "of unknown class" :
                                                  editorValue.getClass ().getName ();

      throw new Error ("Cannot write value " +
                       valueClass +
                       " to property " + entry.getProperty () +
                       " of type " + entry.propertyClass.getName () +
                       " to bean " + formValue.getClass ().getName () +
                       " (exception " + ex + ")");
    } catch (InvocationTargetException ex)
    {
      // allow IllegalFormatException's to escape
      if (ex.getTargetException () instanceof IllegalFormatException)
      {
        throw (IllegalFormatException)ex.getTargetException ();
      } else
      {
        Log.alarm ("Exception while writing property '" +
                   entry.getProperty () + "'",
                   this, ex.getTargetException ());

        throw new Error ("error while writing property '" +
                         entry.getProperty () + "': " + ex);
      }
    } finally
    {
      propertyTrackingEnabled = true;
    }
  }

  protected void commitChildren () throws IllegalFormatException
  {
    for (Iterator i = editors.iterator (); i.hasNext (); )
    {
      EditorEntry entry = (EditorEntry)i.next ();

      entry.getEditor ().commitEdits ();
    }
  }

  // FormEditor interface

  public void commitEdits () throws IllegalFormatException
  {
    if (getEditorValue () == null)
      return;

    unloadEnabled = true;

    try
    {
      commitChildren ();

      // in deferred commit, no changes will have been
      // comitted: save them now
      if (deferredCommit)
        unloadEditors ();
    } finally
    {
      unloadEnabled = !deferredCommit;
    }
  }

  public void setEditorValue (Object newValue) throws IllegalFormatException
  {
    Object oldValue = formValue;

    // skip redundant sets
    if (newValue == oldValue)
      return;

    if (directEdit)
    {
      formValue = newValue;
    } else
    {
      try
      {
        formValue = Objects.cloneObject (newValue);
      } catch (CloneNotSupportedException ex)
      {
        Log.warn ("Failed to clone value " + formValue.getClass () +
                  " for commit",
                  this, ex);
      }
    }

    // manage property change listening
    if (oldValue instanceof PropertyEventSource)
      ((PropertyEventSource)oldValue).removePropertyChangeListener (this);

    if (newValue instanceof PropertyEventSource)
      ((PropertyEventSource)newValue).addPropertyChangeListener (this);

    loadEditors ();
  }

  public Object getEditorValue ()
  {
    if (directEdit)
    {
      return formValue;
    } else
    {
      try
      {
        return Objects.cloneObject (formValue);
      } catch (CloneNotSupportedException ex)
      {
        Log.warn ("Failed to clone value " + formValue.getClass (),
                  this, ex);

        return formValue;
      }
    }
  }

  public Component getEditorComponent ()
  {
    return null;
  }

  // FormEditorListener interface

  public void editCommitRequested (ChangeEvent e)
  {
    FormEditor editor = (FormEditor)e.getSource ();

    try
    {
      editor.commitEdits ();
    } catch (IllegalFormatException ex)
    {
      handleCommitError (ex);
    }
  }

  public void editCommitted (ChangeEvent e)
  {
    if (unloadEnabled)
    {
      FormEditor editor = (FormEditor)e.getSource ();

      // if editor does not directly edit its value need to explictly set
      // the value in the model.
      if (!editor.isDirectEdit ())
      {
        EditorEntry entry = findEntry (editor);
        unloadEditor (entry);
      }

      // if not directly editing, need to fire commit to trigger parent
      // to store new value
      if (!isDirectEdit ())
        fireEditCommitted ();
    }
  }

  public static void handleCommitError (IllegalFormatException ex)
  {
    FormEditor editor = null;

    if (ex.getSource () instanceof FormEditor)
      editor = (FormEditor)ex.getSource ();

    String inputDesc = "input";

    // try to use editor description in message if available
    if (editor != null && editor.getEditorDescription () != null)
      inputDesc = editor.getEditorDescription ();

    Component editorComponent = null;

    // try to place focus at originating component
    if (editor != null && editor.getEditorComponent () != null)
    {
      editorComponent = editor.getEditorComponent ();
      editorComponent.requestFocus ();
    }

    JOptionPane.showMessageDialog
      (editorComponent,
       "Problem with " + inputDesc + ": " + ex.getMessage (),
       "Input Error", JOptionPane.INFORMATION_MESSAGE);
  }

  // PropertyChangeListener interface

  public void propertyChange (PropertyChangeEvent e)
  {
    if (propertyTrackingEnabled)
    {
      for (int i = 0; i < editors.size (); i++)
      {
        EditorEntry entry = (EditorEntry)editors.get (i);

        if (entry.getProperty ().equals (e.getPropertyName ()))
        {
          try
          {
            loadEditor (entry);
          } catch (IllegalFormatException ex)
          {
            Log.alarm ("Error loading value for change of property " +
                       e.getPropertyName (), this, ex);
          }
        }
      }
    }
  }

  protected static final class EditorEntry
  {
    public String property;
    public FormEditor editor;
    public ValueConverter converter;
    /** cached type of property set by loadEditorValue. */
    public Class propertyClass;

    public EditorEntry (String property, FormEditor editor,
                        ValueConverter converter)
    {
      this.property = property;
      this.editor = editor;
      this.converter = converter;
    }

    public String getProperty ()
    {
      return property;
    }

    public FormEditor getEditor ()
    {
      return editor;
    }

    public ValueConverter getConverter ()
    {
      return converter;
    }
  }
}
