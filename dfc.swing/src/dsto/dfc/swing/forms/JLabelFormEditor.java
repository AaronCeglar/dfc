package dsto.dfc.swing.forms;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JLabel;

import dsto.dfc.util.IllegalFormatException;

import dsto.dfc.swing.icons.Iconic;
import dsto.dfc.swing.icons.IconicRegistry;
import dsto.dfc.swing.text.Text;
import dsto.dfc.swing.text.ValueConverter;

/**
 * A read-only editor binding for JLabel's.  Supports display of Icon,
 * String and Iconic values.
 *
 * @version $Revision$
 */
public class JLabelFormEditor
  extends AbstractFormEditor implements FormEditor
{
  /** Show Iconic name property as label text. */
  public static final int SHOW_TEXT = 1;
  /** Show Iconic icon property as label icon. */
  public static final int SHOW_SMALL_ICON = 2;
  /** Show Iconic largeIcon property as label icon. */
  public static final int SHOW_LARGE_ICON = 4;

  protected JLabel label;
  protected Object value;
  protected int iconicMode;

  public JLabelFormEditor (JLabel label)
  {
    this (label, null);
  }

  public JLabelFormEditor (JLabel label, ValueConverter converter)
  {
    super (converter);

    this.label = label;
    this.iconicMode = (SHOW_TEXT | SHOW_SMALL_ICON);
  }

  /**
   * Create a new JLabelFormEditor instance.
   *
   * @param label The label to bind.
   * @param iconicMode Used to resolve display of Iconic values.  See
   * docs for SHOW_TEXT, SHOW_SMALL_ICON and SHOW_LARGE_ICON for
   * details.
   * @param converter A value converter for conversion between the
   * value and String.
   * @see #SHOW_TEXT
   * @see #SHOW_LARGE_ICON
   * @see #SHOW_SMALL_ICON
   */
  public JLabelFormEditor (JLabel label, int iconicMode,
                           ValueConverter converter)
  {
    super (converter);

    this.label = label;
    this.iconicMode = iconicMode;
  }

  // FormEditor interface

  public boolean isDirectEdit ()
  {
    return false;
  }

  public void setEditorValue (Object newValue)
    throws IllegalFormatException
  {
    Iconic iconic =
      IconicRegistry.getInstance ().getIconicValue (newValue);

    if (iconic != null)
    {
      if ((iconicMode & SHOW_LARGE_ICON) != 0)
        label.setIcon (iconic.getLargeIcon ());
      else if ((iconicMode & SHOW_SMALL_ICON) != 0)
        label.setIcon (iconic.getIcon ());

      if ((iconicMode & SHOW_TEXT) != 0)
        label.setText (iconic.getName ());
    } else if (newValue instanceof Icon)
    {
      label.setIcon ((Icon)newValue);
    } else
    {
      label.setText
        ((String)Text.convertValue (newValue,
                                                 String.class,
                                                 converter));
    }

    value = newValue;
  }

  public void commitEdits ()
  {
    // zip
  }

  public Object getEditorValue ()
  {
    return value;
  }

  public Component getEditorComponent ()
  {
    return label;
  }
}
