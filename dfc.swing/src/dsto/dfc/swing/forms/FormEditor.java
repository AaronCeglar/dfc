package dsto.dfc.swing.forms;

import java.awt.Component;

import dsto.dfc.util.IllegalFormatException;

/**
 * Defines an editor for a value, similar in concept to a JFC cell
 * editor.  Editors support set/get of their current editorValue
 * property, as well as notification of changes to the value via
 * editCommitted events (see {@link FormEditorListener}).<p>
 *
 * When the value of the editor changes, the editor should immediately
 * fire an editCommitted event (eg see {@link
 * JCheckBoxFormEditor}). If an editor wishes to initiate a controlled
 * commit, where an error in the new value might need to be handled,
 * it should fire an editCommitRequested event instead: this will
 * signal its parent environment to call the editor's commitEdits ()
 * and either handle any IllegalFormatException encountered or commit
 * the new value.  Only editors that must handle invalid entries need
 * use this capability - for an example, see {@link
 * JTextFieldFormEditor}.
 *
 * @author Matthew
 * @version $Revision$
 */
public interface FormEditor
{
  /**
   * True if the editor directly edits the value passed in with
   * setValue ().  If false, the parent editor will be reponsible for
   * retrieving the value from the editor and storing it.
   */
  public boolean isDirectEdit ();

  /**
   * The preferred type of value for the editor.  Values conforming to
   * this type may be safely passed to {@link #setEditorValue}.  May
   * return java.lang.Object to indictate no preferred type.
   */
  public Class getPreferredValueType ();

  /**
   * Sets the value to be displayed/edited by the form editor.
   *
   * @param value The value to display.
   * @exception IllegalFormatException if the value was not in the
   * correct format for display by this editor. The source specified
   * by the exception should be this FormEditor instance or a child.
   */
  public void setEditorValue (Object value) throws IllegalFormatException;

  /**
   * Get the current value from the form editor.  At least one call to
   * setValue () must be made beforehand.
   */
  public Object getEditorValue ();

  /**
   * Get the component, if any, that acts as a UI for this editor.
   * May return null if not associated with a component.
   */
  public Component getEditorComponent ();

  /**
   * Get a short description of the value being edited for this editor
   * (eg 'host name').  May be null.
   */
  public String getEditorDescription ();

  /**
   * Commit any changes not already committed into the value of this
   * editor (eg a text editor will attempt to perform any uncommitted
   * text conversions at this point).  Only editors that need support
   * editing in non-atomic sessions (such as text fields) will
   * generally do anything when this method is called.  If commit is
   * made that has not been signalled with a previous editCommitted
   * event, the editor should fire an editCommitted event at this
   * point.  Composite editors, such as forms, will generally forward
   * this request to their child editors.
   *
   * @exception IllegalFormatException if the commit failed because of
   * an illegal input or other error.  The source specified by the
   * exception should be the FormEditor instance that caused the error
   * (ie either this editor or a child).
   */
  public void commitEdits () throws IllegalFormatException;

  public void addFormEditorListener (FormEditorListener l);

  public void removeFormEditorListener (FormEditorListener l);
}
