package dsto.dfc.swing.forms;

import java.awt.Component;
import java.beans.Customizer;

import javax.swing.JPanel;

import dsto.dfc.util.Disposable;
import dsto.dfc.util.IllegalFormatException;

import dsto.dfc.swing.text.ValueConverter;

/**
 * A form editor container that hosts FormEditor's bound to properties
 * of the editor value.  Use the {@link #addEditor(String,FormEditor)
 * addEditor} method to bind child editors to properties of the
 * panel's editor value.<p>
 *
 * Also implements the Beans Customizer interface, with the setObject
 * () method delegated to setEditorValue ().
 *
 * @author Matthew
 * @version $Revision$
 */
public class FormPanel extends JPanel implements Form, Customizer, Disposable
{
  private BasicForm form;

  public FormPanel ()
  {
    form = new BasicForm ();

    form.setFormEditorEventSource (this);
  }

  public void dispose ()
  {
    form.dispose ();
  }

  // Customizer interface

  public void setObject (Object bean)
  {
    try
    {
      setEditorValue (bean);
    } catch (IllegalFormatException ex)
    {
      throw new Error
        ("Value is not the correct format for this editor: " + ex);
    }
  }

  // Form interface

  /**
   * Default implementation: returns java.lang.Object to indicate no
   * preferred type.
   */
  public Class getPreferredValueType ()
  {
    return Object.class;
  }

  public boolean isDeferredCommit ()
  {
    return form.isDeferredCommit ();
  }

  public void setDeferredCommit (boolean newValue)
  {
    form.setDeferredCommit (newValue);
  }

  public void addEditor (String property, FormEditor editor)
  {
    form.addEditor (property, editor);
  }

  public void addEditor (String property, FormEditor editor,
                         ValueConverter converter)
  {
    form.addEditor (property, editor, converter);
  }

  public boolean removeEditor (FormEditor editor)
  {
    return form.removeEditor (editor);
  }

  // FormEditor interface

  public String getEditorDescription ()
  {
    return form.getEditorDescription ();
  }

  public boolean isDirectEdit ()
  {
    return form.isDirectEdit ();
  }

  public void setDirectEdit (boolean newValue)
  {
    form.setDirectEdit (newValue);
  }

  public void commitEdits ()
    throws IllegalFormatException
  {
    form.commitEdits ();
  }

  public void setEditorValue (Object newValue) throws IllegalFormatException
  {
    Object oldValue = getEditorValue ();

    form.setEditorValue (newValue);

    firePropertyChange ("editorValue", oldValue, newValue);
  }

  public Object getEditorValue ()
  {
    return form.getEditorValue ();
  }

  public Component getEditorComponent ()
  {
    return this;
  }

  public void addFormEditorListener (FormEditorListener l)
  {
    form.addFormEditorListener (l);
  }

  public void removeFormEditorListener (FormEditorListener l)
  {
    form.removeFormEditorListener (l);
  }

  /**
   * @deprecated Supports old typo, use fireEditCommitted instead.
   */
  public void fireEditComitted ()
  {
    fireEditCommitted ();
  }

  public void fireEditCommitted ()
  {
    form.fireEditCommitted ();
  }

  public void fireEditCommitRequested ()
  {
    form.fireEditCommitRequested ();
  }
}
