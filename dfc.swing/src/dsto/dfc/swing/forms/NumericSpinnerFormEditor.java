package dsto.dfc.swing.forms;

import java.awt.Component;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import dsto.dfc.swing.controls.NumericSpinner;
import dsto.dfc.swing.text.ValueConverter;

import dsto.dfc.util.Disposable;
import dsto.dfc.util.IllegalFormatException;

/**
 * A numeric spinner form editor (see {@link dsto.dfc.swing.controls.NumericSpinner}).
 *
 * @version $Revision$
 */
public class NumericSpinnerFormEditor
  extends AbstractFormEditor
  implements FormEditor, Disposable, ChangeListener
{
  private NumericSpinner numericSpinner;

  public NumericSpinnerFormEditor()
  {
    this(new NumericSpinner());
  }

  public NumericSpinnerFormEditor(NumericSpinner spinner)
  {
    this(spinner, null, null);
  }

  public NumericSpinnerFormEditor(NumericSpinner spinner, String Desc)
  {
    this(spinner, null, Desc);
  }

  public NumericSpinnerFormEditor(NumericSpinner spinner, ValueConverter converter, String Desc)
  {
    super(Desc, converter);
    numericSpinner = spinner;
    numericSpinner.getModel().addChangeListener(this);
  }

  public boolean isDirectEdit ()
  {
    return false;
  }

  public void setEditorValue (Object newValue) throws IllegalFormatException
  {
    if (newValue instanceof Integer)
    {
      numericSpinner.setValue(((Integer)(newValue)).intValue());
    } else
    {
      throw new IllegalFormatException (this, newValue, Integer.class);
    }
  }

  public void dispose ()
  {
    if (numericSpinner != null)
    {
      numericSpinner.getModel().removeChangeListener(this);
      numericSpinner = null;
    }
  }

  public void commitEdits ()
  {
    // zip
  }

  public Object getEditorValue ()
  {
    return new Integer (numericSpinner.getModel().getValue ());
  }

  public Component getEditorComponent ()
  {
    return numericSpinner;
  }

  // ChangeListener interface

  public void stateChanged (ChangeEvent e)
  {
    fireEditCommitted ();
  }
}