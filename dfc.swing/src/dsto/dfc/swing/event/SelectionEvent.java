package dsto.dfc.swing.event;

import java.util.EventObject;

/**
 * The event sent to SelectionListener's.
 *
 * @version $Revision$
 */
public class SelectionEvent extends EventObject
{
  private EventObject triggerEvent;

  /**
   * Construct a new SelectionEvent.
   *
   * @param source The source of the event.
   * @param triggerEvent The event that triggered this event (eg. a
   * ListSelectionEvent from a JTable).  May be null if this event was
   * not triggered by another.
   */
  public SelectionEvent (Object source, EventObject triggerEvent)
  {
    super (source);
    this.triggerEvent = triggerEvent;
  }

  /**
   * The event that triggered this event.  May be null if this event
   * was not triggered by another.
   */
  public EventObject getTriggerEvent ()
  {
    return triggerEvent;
  }
} 
