package dsto.dfc.swing.event;

import java.util.EventListener;

/**
 * Defines a listener for changes to a selection.
 *
 * @version $Revision$
 */
public interface SelectionListener extends EventListener
{
  /**
   * Indicates the selection has changed in some way.
   */
  public void selectionChanged (SelectionEvent e);
}
