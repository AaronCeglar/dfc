package dsto.dfc.swing.event;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.EventObject;

/**
 * Base implementation of SelectionEventSource.
 *
 * @version $Revision$
 */
public class BasicSelectionEventSource
  implements SelectionEventSource, Serializable, Cloneable
{
  private Object source;
  private transient ArrayList listeners;

  public BasicSelectionEventSource ()
  {
    // zip
  }

  public BasicSelectionEventSource (Object source)
  {
    this.source = source;
  }

  public synchronized void addSelectionListener (SelectionListener l)
  {
    if (listeners == null)
      listeners = new ArrayList ();

    listeners.add (l);
  }

  public synchronized void removeSelectionListener (SelectionListener l)
  {
    if (listeners != null)
      listeners.remove (l);
  }

  public Object clone () throws CloneNotSupportedException
  {
    BasicSelectionEventSource newObject =
      (BasicSelectionEventSource)super.clone ();

    newObject.listeners = null;

    return newObject;
  }

  public void fireSelectionChanged (EventObject trigger)
  {
    if (listeners != null)
    {
      SelectionEvent e = new SelectionEvent (source, trigger);
      SelectionListener [] theListeners;

      synchronized (this)
      {
        theListeners = new SelectionListener [listeners.size ()];
        theListeners = (SelectionListener [])listeners.toArray (theListeners);
      }

      for (int i = 0; i < theListeners.length; i++)
        theListeners [i].selectionChanged (e);
    }
  }
}
