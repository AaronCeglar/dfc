package dsto.dfc.swing.event;

/**
 * Defines an object that provides notification of selection changes
 * to SelectionListener's.
 *
 * @version $Revision$
 */
public interface SelectionEventSource
{
  public void addSelectionListener (SelectionListener l);

  public void removeSelectionListener (SelectionListener l);
}
