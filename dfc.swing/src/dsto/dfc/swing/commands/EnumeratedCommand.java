package dsto.dfc.swing.commands;

import dsto.dfc.util.EnumerationValue;
import dsto.dfc.util.PropertyEventSource;

/**
 * Defines a command whose behaviour 'selects' from a defined set of
 * enumerated values.
 *
 * @author Matthew
 * @version $Revision$
 *
 * @see dsto.dfc.util.EnumerationValue
 * @see AbstractEnumeratedCommand
 */
public interface EnumeratedCommand
  extends Command, EnumerationValue, PropertyEventSource
{
  /**
   * Set the values that will be 'selected' when execute() is next
   * called (this is usually called immediately before execute() by an
   * action listener attached to the appropriate menu item).  The
   * number of values in the array must not be more than specified by
   * getMaxSelectedValues().
   *
   * @see #getMaxSelectedValues
   */
  public void setNextSelectedValues (Object [] values);

  /**
   * Get the values that will be set by executing the command.
   *
   * @see #setNextSelectedValues
   */
  public Object [] getNextSelectedValues ();

  /**
   * The currently selected values.  If these values change, the
   * command should fire a property change event.  If
   * maxSelectedValues is 0, this should return null or an empty
   * array.
   */
  public Object [] getSelectedValues ();

  /**
   * Get the maximum number of values that may be selected at any one
   * time.  eg this may affect the way the menu items are rendered for
   * the command: if max selected values is 0, then vanilla
   * JMenuItem's are used for each value, if 1 JRadioButtonMenuItem's
   * are used and if >1 then JCheckBoxMenuItem's are used.
   *
   * @see #setNextSelectedValues
   */
  public int getMaxSelectedValues ();
}
