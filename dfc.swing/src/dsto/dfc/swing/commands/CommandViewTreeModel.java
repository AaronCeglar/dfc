package dsto.dfc.swing.commands;

import javax.swing.tree.TreePath;

import dsto.dfc.swing.tree.AbstractDfcTreeModel;
import dsto.dfc.swing.tree.DfcTreeModel;

public class CommandViewTreeModel
  extends AbstractDfcTreeModel implements DfcTreeModel, CommandViewNodeListener
{
  private CommandViewNode root;

  public CommandViewTreeModel (CommandViewNode root)
  {
    this.root = root;

    registerListener (root);
  }

  // TreeModel interface

  public Object getRoot ()
  {
    return root;
  }

  public Object getChild (Object parent, int index)
  {
    CommandViewNode node = (CommandViewNode)parent;

    return node.getChild (index);
  }

  public int getChildCount (Object parent)
  {
    CommandViewNode node = (CommandViewNode)parent;

    return node.getChildCount ();
  }

  public boolean isLeaf (Object entry)
  {
    return getChildCount (entry) == 0;
  }

  public int getIndexOfChild (Object parent, Object child)
  {
    CommandViewNode childNode = (CommandViewNode)child;

    return childNode.getIndex ();
  }

  public void valueForPathChanged (TreePath path, Object newValue)
  {
    // zip
  }

  // DfcTreeModel interface

  public Object getParent (Object entry)
  {
    CommandViewNode node = (CommandViewNode)entry;

    return node.getParent ();
  }

  protected void registerListener (CommandViewNode node)
  {
    node.addCommandViewNodeListener (this);

    for (int i = 0; i < node.getChildCount (); i++)
      registerListener (node.getChild (i));
  }

  protected void unregisterListener (CommandViewNode node)
  {
    node.removeCommandViewNodeListener (this);

    for (int i = 0; i < node.getChildCount (); i++)
      unregisterListener (node.getChild (i));
  }

  // CommandViewNodeListener interface

  public void childAdded (CommandViewNodeEvent e)
  {
    CommandViewNode node = e.getNode ();
    Object [] path = getPathForEntry (node.getParent ());

    fireTreeNodeInserted (this, path, e.getNewIndex (), node);

    registerListener (node);
  }

  public void childRemoved (CommandViewNodeEvent e)
  {
    CommandViewNode node = e.getNode ();
    Object [] path = getPathForEntry (node.getParent ());

    fireTreeNodeRemoved (this, path, e.getOldIndex (), node);

    unregisterListener (node);
  }

  public void childMoved (CommandViewNodeEvent e)
  {
    CommandViewNode node = e.getNode ();
    Object [] path = getPathForEntry (node.getParent ());

    fireTreeNodeRemoved (this, path, e.getOldIndex (), node);
    fireTreeNodeInserted (this, path, e.getNewIndex (), node);
  }

  public void commandChanged (CommandViewNodeEvent e)
  {
    CommandViewNode node = e.getSourceNode ();
    Object [] path = getPathForEntry (node.getParent ());

    fireTreeNodeChanged (this, path, node.getIndex (), node);
  }
}
