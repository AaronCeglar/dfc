package dsto.dfc.swing.commands;

import java.util.List;

import javax.swing.event.ChangeListener;

/**
 * Interface for commands that want to provide a custom menu UI.
 *
 * @version $Revision$
 */
public interface CustomMenuProvider
{
  /**
   * Create the menu items that represent the UI for the command.
   *
   * @return A new list of JComponent's to be added to the menu for
   * the command.
   */
  public List createMenuItems ();

  /**
   * Destroy menu items created by createMenuItems ().
   */
  public void destroyMenuItems (List items);

  /**
   * Custom menu providers should fire change events when the menu UI
   * needs to be updated.  This event will trigger any interested
   * listeners to destroy old menu items (destroyMenuItems ()) and
   * create new ones (createMenuItems ()).
   */
  public void addChangeListener (ChangeListener l);

  public void removeChangeListener (ChangeListener l);
}
