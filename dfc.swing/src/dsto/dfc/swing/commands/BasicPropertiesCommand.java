package dsto.dfc.swing.commands;

import javax.swing.Icon;

import dsto.dfc.swing.icons.ImageLoader;

/**
 * Base class for Properties type commands.
 *
 * @version $Revision$
 */
public class BasicPropertiesCommand extends AbstractMutableCommand
{
  public static final Icon ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/properties.gif");

  public BasicPropertiesCommand ()
  {
    super ();
  }

  public void execute ()
  {
    // zip
  }

  public String getName ()
  {
    return "edit.Properties";
  }

  public String getDescription ()
  {
    return "Show property customizer";
  }

  public boolean isInteractive ()
  {
    return true;
  }

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return "Edit.props";
    else if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return "props";
    else if (viewName.equals (CommandView.TOOLBAR_VIEW))
      return "props";
    else
      return null;
  }

  public char getMnemonic ()
  {
    return 'r';
  }

  public Icon getIcon ()
  {
    return ICON;
  }
}
