package dsto.dfc.swing.commands;

import java.util.HashMap;

import javax.swing.JMenu;
import javax.swing.JMenuBar;

import dsto.dfc.swing.controls.EtchedDividingBorder;

/**
 * Synchronizes a JFC menu bar with a CommandTree.
 *
 * @see dsto.dfc.swing.commands.CommandMenus
 *
 * @version $Revision$
 */
final class CommandMenuBarSynchronizer implements CommandViewNodeListener
{
  private CommandView commandView;
  /** The root node for the menubar */
  private CommandViewNode root;
  /** The menu bar we are managing */
  private JMenuBar menuBar;
  /** Maps watched tree nodes to their menus */
  private HashMap nodeToMenu = new HashMap (15);

  /**
   * Create a new instance.
   *
   * @param menuBar The menu to synchronise.
   * @param commandView The tree of commands.
   * @param root The root node of the tree to use for this menu.
   */
  public CommandMenuBarSynchronizer (JMenuBar menuBar,
                                     CommandView commandView,
                                     CommandViewNode root)
  {
    this.menuBar = menuBar;
    this.commandView = commandView;
    this.root = root;

    menuBar.setBorder(new EtchedDividingBorder());

    root.addCommandViewNodeListener (this);

    addEntries ();
  }

  /**
   * Remove all listeners and references.
   */
  public void dispose ()
  {
    root.removeCommandViewNodeListener (this);

    for (int i = 0; i < root.getChildCount (); i++)
    {
      CommandViewNode node = root.getChild (i);

      removeMenu (node);
    }
  }

  /**
   * Populate menu bar with entries.
   */
  private void addEntries ()
  {
    for (int i = 0; i < root.getChildCount (); i++)
    {
      CommandViewNode node = root.getChild (i);

      addMenu (node);
    }
  }

  /**
   * Add a menu to the menu bar.
   *
   * @param node The node containing the command to add.
   * @param nodeIndex The index of the node.
   */
  private void addMenu (CommandViewNode node)
  {
    int nodeIndex = node.getIndex ();

    // sanity check: only add groups to menu bar
    if (node.getCommand () != null)
      throw new IllegalArgumentException ("attempt to add a command as a menu");

    String menuName = node.getName ();
    JMenu menu = new JMenu (menuName);
    menu.setMnemonic (CommandRegistry.getMenuMnemonic (menuName));

    CommandMenuSynchronizer menuSyncher =
      new CommandMenuSynchronizer (menu.getPopupMenu (), commandView,
                                   node, menuBar);

    menu.putClientProperty ("menuSync", menuSyncher);

    menuBar.add (menu, nodeIndex);

    nodeToMenu.put (node, menu);
  }

  /**
   * Remove a menu to the menu bar.
   *
   * @param node The node containing the command to add.
   */
  private void removeMenu (CommandViewNode node)
  {
    JMenu menu = (JMenu)nodeToMenu.remove (node);

    CommandMenuSynchronizer menuSyncher =
      (CommandMenuSynchronizer)menu.getClientProperty ("menuSync");

    menuSyncher.dispose ();

    menu.putClientProperty ("menuSync", null);

    menuBar.remove (menu);
  }

  // CommandViewNodeListener interface

  public void childAdded (CommandViewNodeEvent e)
  {
    addMenu (e.getNode ());

    menuBar.revalidate ();
    menuBar.repaint ();
  }

  public void childRemoved (CommandViewNodeEvent e)
  {
    removeMenu (e.getNode ());

    menuBar.revalidate ();
    menuBar.repaint ();
  }

  public void childMoved (CommandViewNodeEvent e)
  {
    JMenu menu = (JMenu)menuBar.getComponent (e.getOldIndex ());
    menuBar.remove (e.getOldIndex ());
    menuBar.add (menu, e.getNewIndex ());
  }

  public void commandChanged (CommandViewNodeEvent e)
  {
    // should never see this
    throw new Error ("command event on a menu-level node");
  }
}
