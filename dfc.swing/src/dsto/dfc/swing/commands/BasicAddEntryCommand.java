package dsto.dfc.swing.commands;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.Icon;
import javax.swing.KeyStroke;

import dsto.dfc.swing.icons.ImageLoader;

/**
 * Base class for commands that add a new entry to a control.
 *
 * @version $Revision$
 */
public class BasicAddEntryCommand extends AbstractCommand
{
  private static final KeyStroke ACCELERATOR =
    KeyStroke.getKeyStroke (KeyEvent.VK_ENTER,
                            Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask ());

  private static final Icon ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/plus.gif");

  public BasicAddEntryCommand ()
  {
    setIcon (ICON);
  }

  public void execute ()
  {
    // zip
  }

  public Icon getIcon ()
  {
    return ICON;
  }

  public String getName ()
  {
    return "edit.Add Entry";
  }

  public String getDescription ()
  {
    return "Add a new entry";
  }

  public boolean isInteractive ()
  {
    return false;
  }

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return "edit";
    else if (viewName.equals (CommandView.TOOLBAR_VIEW))
      return "edit";
    else if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return "Edit.change";
    else
      return null;
  }

  public char getMnemonic ()
  {
    return 'a';
  }

  public KeyStroke getAccelerator ()
  {
    return ACCELERATOR;
  }
}
