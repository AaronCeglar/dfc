package dsto.dfc.swing.commands;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.AWTEventListener;
import java.awt.event.FocusEvent;

import javax.swing.SwingUtilities;

import dsto.dfc.swing.SwingSupport;

/**
 * Track focus changes across components in a window and sets command
 * view's active property based on focus heirachy.  This can be used
 * to resolve overloaded commands based on component focus.
 *
 * @author Matthew
 * @version $Revision$
 * @see CommandView#activate
 */
public class WindowFocusTracker implements AWTEventListener
{
  private Window window;
  private String [] views;
  private Component lastFocusedComponent = null;

  /**
   * Create a focus tracker for a given window.
   *
   * @param window The window to track focus in.
   * @param view The view type to manage (eg CommandView.MAIN_MENU_VIEW),
   */
  public WindowFocusTracker (Window window, String view)
  {
    this (window, new String [] {view});
  }

  /**
   * Create a focus tracker for a given window.
   *
   * @param window The window to track focus in.
   * @param views The view types to manage (eg CommandView.MAIN_MENU_VIEW),
   */
  public WindowFocusTracker (Window window, String [] views)
  {
    this.window = window;
    this.views = views;

    setFocusedComponent (window.getFocusOwner ());

    window.getToolkit ().addAWTEventListener
      (this, FocusEvent.FOCUS_EVENT_MASK);
  }

  public void dispose ()
  {
    if (window != null)
    {
      window.getToolkit ().removeAWTEventListener (this);

      views = null;
      window = null;
    }
  }

  protected void setFocusedComponent (Component newFocus)
  {
    Component commonParent = null;

    if (lastFocusedComponent != null && newFocus != null)
      commonParent = SwingSupport.findCommonParent (lastFocusedComponent, newFocus);
    else
      commonParent = window;

    if (lastFocusedComponent != null)
      setViewsActive (lastFocusedComponent, commonParent, false);

    if (newFocus != null)
      setViewsActive (newFocus, commonParent, true);

    lastFocusedComponent = newFocus;
  }

  /**
   * Walk down a chain of components, setting their command views
   * 'active' property.
   *
   * @param start The component (inclusive) to begin at.
   * @param stop The component (not inclusive) to end at.
   * @param active The new value of command view's active property.
   */
  protected void setViewsActive (Component start,
                                 Component stop, boolean active)
  {
    for (Component c = start; c != stop; c = c.getParent ())
    {
      if (c instanceof CommandSource)
      {
        CommandSource cmdSource = (CommandSource)c;

        for (int i = 0; i < views.length; i++)
        {
          CommandView view = cmdSource.getCommandView (views [i]);

          if (view != null)
            view.activate ();
        }
      }
    }
  }

  public void eventDispatched (AWTEvent e)
  {
    FocusEvent event = (FocusEvent)e;

//      if (event.getID () == FocusEvent.FOCUS_GAINED)
//        System.out.print ("focus gained");
//      else
//        System.out.print ("focus lost");

//      if (event.isTemporary ())
//        System.out.println (" (temp)");
//      else
//        System.out.println (" (perm)");

    if (event.getID () == FocusEvent.FOCUS_GAINED &&
        SwingUtilities.windowForComponent (event.getComponent ()) == window)
    {
      setFocusedComponent (event.getComponent ());
    }
  }
}
