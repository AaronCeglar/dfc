package dsto.dfc.swing.commands;

import dsto.dfc.swing.event.SelectionEvent;
import dsto.dfc.swing.event.SelectionEventSource;
import dsto.dfc.swing.event.SelectionListener;
import dsto.dfc.util.Disposable;

/**
 * Base class for commands that are affected by changes in a
 * SelectionEventSource (eg become disabled when nothing is selected).
 * Subclasses can override selectionChanged () to catch selection
 * changes in the source.
 *
 * @version $Revision$
 */
public abstract class AbstractSelectionBasedCommand
  extends AbstractMutableCommand implements SelectionListener, Disposable
{
  protected SelectionEventSource source;

  public AbstractSelectionBasedCommand (SelectionEventSource source)
  {
    this (source, null);
  }

  public AbstractSelectionBasedCommand (SelectionEventSource source,
                                        String iconName)
  {
    super (iconName);

    this.source = source;
    source.addSelectionListener (this);
  }

  protected AbstractSelectionBasedCommand ()
  {
    this.source = null;
  }

  public void dispose ()
  {
    if (source != null)
    {
      source.removeSelectionListener (this);
      source = null;
    }
  }

  // SelectionListener interface

  /**
   * SelectionListener interface implementation: does nothing.
   */
  public void selectionChanged (SelectionEvent e)
  {
    // zip
  }
}
