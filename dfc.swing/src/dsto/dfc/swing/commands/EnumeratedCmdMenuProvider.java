package dsto.dfc.swing.commands;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import dsto.dfc.swing.icons.Iconic;

/**
 * Implementation of CustomMenuProvider for EnumeratedCommand's.
 *
 * @author Matthew
 * @version $Revision$
 *
 * @see AbstractEnumeratedCommand
 * @see CustomMenuProvider
 */
public class EnumeratedCmdMenuProvider
  implements CustomMenuProvider, ActionListener
{
  protected EnumeratedCommand command;
  protected transient Vector changeListeners;

  /**
   * Create a menu provider for a given command.
   */
  public EnumeratedCmdMenuProvider (EnumeratedCommand command)
  {
    this.command = command;

    command.addPropertyChangeListener (new CommandListener ());
  }

  public boolean isSelected (Object value)
  {
    Object [] selectedValues = command.getSelectedValues ();

    for (int i = 0; i < selectedValues.length; i++)
    {
      if (selectedValues [i].equals (value))
        return true;
    }

    return false;
  }

  // CustomMenuProvider implementation

  public List createMenuItems ()
  {
    Object [] values = command.getEnumValues ();
    ArrayList items = new ArrayList (values.length);

    for (int i = 0; i < values.length; i++)
    {
      Object value = values [i];
      JMenuItem item = createMenuItem (value);

      items.add (item);

      // create and store synchronizer in first item
      if (i == 0)
      {
        Synchronizer syncher = new Synchronizer (items);
        command.addPropertyChangeListener (syncher);
        item.putClientProperty ("syncher", syncher);
      }
    }

    return items;
  }

  public void destroyMenuItems (List items)
  {
    for (int i = 0; i < items.size (); i++)
    {
      JMenuItem item = (JMenuItem)items.get (i);

      item.putClientProperty ("value", null);
      item.removeActionListener (this);

      // destroy synchronizer in first item
      if (i == 0)
      {
        Synchronizer syncher = (Synchronizer)item.getClientProperty ("syncher");
        command.removePropertyChangeListener (syncher);
        item.putClientProperty ("syncher", null);
      }
    }
  }

  protected JMenuItem createMenuItem (Object value)
  {
    JMenuItem item;
    Icon icon;
    String label;

    if (value instanceof Iconic)
    {
      Iconic iconicValue = (Iconic)value;

      label = iconicValue.getName ();
      icon = iconicValue.getIcon ();
    } else
    {
      icon = null;
      label = value.toString ();
    }

    if (command.getMaxSelectedValues () == 1)
      item = new JRadioButtonMenuItem (label);
    else if (command.getMaxSelectedValues () > 1)
      item = new JCheckBoxMenuItem (label);
    else
      item = new JMenuItem (label);

    item.setIcon (icon);

    if (command.getMaxSelectedValues () != 0)
      item.setSelected (isSelected (value));

    item.putClientProperty ("value", value);
    item.putClientProperty (Action.SHORT_DESCRIPTION, command.getDescription ());
    item.putClientProperty (Action.SMALL_ICON, icon);

    item.addActionListener (this);

    return item;
  }

  // ActionListener interface

  public void actionPerformed (ActionEvent e)
  {
    Object value =
      ((JComponent)e.getSource ()).getClientProperty ("value");

    command.setNextSelectedValues (new Object [] {value});
    command.execute ();
  }

  public synchronized void removeChangeListener(ChangeListener l)
  {
    if(changeListeners != null && changeListeners.contains(l))
    {
      Vector v = (Vector) changeListeners.clone();
      v.removeElement(l);
      changeListeners = v;
    }
  }

  public synchronized void addChangeListener(ChangeListener l)
  {
    Vector v = changeListeners == null ? new Vector(2) : (Vector) changeListeners.clone();
    if(!v.contains(l))
    {
      v.addElement(l);
      changeListeners = v;
    }
  }

  protected void fireStateChanged ()
  {
    fireStateChanged (new ChangeEvent (command));
  }

  protected void fireStateChanged (ChangeEvent e)
  {
    if (changeListeners != null)
    {
      Vector listeners = changeListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((ChangeListener) listeners.elementAt(i)).stateChanged(e);
      }
    }
  }

  /**
   * Synchronizes the current value of the command and the selected
   * checkbox menu item.
   */
  private final class Synchronizer implements PropertyChangeListener
  {
    private List items;

    public Synchronizer (List items)
    {
      this.items = items;
    }

    public void updateMenu ()
    {
      for (int i = 0; i < items.size (); i++)
      {
        JMenuItem item = (JMenuItem)items.get (i);

        Object value = item.getClientProperty ("value");

        if (command.getMaxSelectedValues () != 0)
          item.setSelected (isSelected (value));
      }
    }

    public void propertyChange (PropertyChangeEvent e)
    {
      if (e.getPropertyName ().equals ("selectedValues"))
      {
        updateMenu ();
      }
    }
  }

  /**
   * Listens for changes to this command's enumValues and fires
   * stateChanged events for CustomCommandProvider listeners.
   */
  class CommandListener implements PropertyChangeListener
  {
    // PropertyChangeListener interface

    public void propertyChange (PropertyChangeEvent e)
    {
      if (e.getPropertyName ().equals ("enumValues"))
        fireStateChanged ();
    }
  }
}
