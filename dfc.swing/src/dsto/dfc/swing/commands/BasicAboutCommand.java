package dsto.dfc.swing.commands;

import java.awt.Component;
import java.awt.Dialog;

/**
 * Show an about box.  Subclasses should override {@link #createAboutDialog}
 * to create the about box.
 *
 * @version $Revision$
 */
public class BasicAboutCommand extends AbstractCommand
{
  protected Component client;

  public BasicAboutCommand (Component client)
  {
    this.client = client;
  }

  public char getMnemonic ()
  {
    return 'a';
  }

  public void execute ()
  {
    Dialog box = createAboutDialog ();

    if (box != null)
    {
      box.setVisible (true);

      box.dispose ();
    }
  }

  /**
   * Override this to create the about box dialog.
   */
  protected Dialog createAboutDialog ()
  {
    return null;
  }

  public boolean isInteractive ()
  {
    return true;
  }

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return "Help.about";
    else
      return null;
  }

  public String getDescription ()
  {
    return "Show information about this application";
  }

  public String getName ()
  {
    return "help.About";
  }
}
