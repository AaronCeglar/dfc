package dsto.dfc.swing.commands;


/**
 * Defines an object (often a UI component) that exports one or more
 * {@link CommandView}'s.
 *
 * @version $Revision$
 *
 * @see CommandView
 */
public interface CommandSource
{
  /**
   * Get the command view instance for a given view name.
   *
   * @param viewName The name of the view to be retrieved.  Usually one of
   * CommandView.MAIN_MENU_VIEW, CommandView.CONTEXT_MENU_VIEW or
   * CommandView.TOOLBAR_VIEW, but custom views may be supported also.
   * @return The command view instance or null if view is not defined.
   */
  public CommandView getCommandView (String viewName);
}



