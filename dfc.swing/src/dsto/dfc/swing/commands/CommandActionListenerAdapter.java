package dsto.dfc.swing.commands;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Adapts a Command to be executed as an ActionListener.
 *
 * @version $Revision$
 */
public final class CommandActionListenerAdapter implements ActionListener
{
  protected Command command;

  public CommandActionListenerAdapter (Command command)
  {
    this.command = command;
  }

  public void actionPerformed (ActionEvent e)
  {
    if (command.isEnabled ())
      command.execute ();
  }
}
