package dsto.dfc.swing.commands;

import java.awt.event.KeyEvent;

import javax.swing.Icon;
import javax.swing.KeyStroke;

import dsto.dfc.swing.event.SelectionEventSource;
import dsto.dfc.swing.icons.ImageLoader;

/**
 * Basic delete command.
 *
 * @version $Revision$
 */
public class BasicDeleteCommand extends AbstractSelectionBasedCommand
{
  private static final Icon ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/delete.gif");
  private static final KeyStroke ACCELERATOR =
    KeyStroke.getKeyStroke (KeyEvent.VK_DELETE, 0);

  public BasicDeleteCommand (SelectionEventSource ui)
  {
    super (ui);
  }

  public void execute ()
  {
    // zip
  }

  public String getName ()
  {
    return "edit.Delete";
  }

  public Icon getIcon ()
  {
    return ICON;
  }

  public String getDescription ()
  {
    return "Delete the selected entry";
  }

  public boolean isInteractive ()
  {
    return false;
  }

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return "delete";
    else if (viewName.equals (CommandView.TOOLBAR_VIEW))
      return "delete";
    else if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return "Edit.change";
    else
      return null;
  }

  public char getMnemonic ()
  {
    return 'd';
  }

  public KeyStroke getAccelerator ()
  {
    return ACCELERATOR;
  }
}