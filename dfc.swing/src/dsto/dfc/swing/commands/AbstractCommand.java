package dsto.dfc.swing.commands;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.KeyStroke;

import dsto.dfc.swing.icons.ImageLoader;
import dsto.dfc.util.Copyable;

/**
 * Base class for commands.  Provides default implementations for some
 * methods.
 *
 * @version $Revision$
 */
public abstract class AbstractCommand
  implements Command, Copyable, ActionListener
{
  protected Icon icon;
  protected String displayName = null;

  public AbstractCommand ()
  {
    this (null);
  }

  public AbstractCommand (String iconName)
  {
    if (iconName != null)
    {
      // if a resource path is not specified, then append dfc commands path
      if (iconName.startsWith ("/"))
        icon = ImageLoader.getIcon (iconName);
      else
        icon = ImageLoader.getIcon ("/dsto/dfc/icons/" + iconName);
    }
  }

  public abstract void execute ();

  public abstract String getName ();

  public abstract String getDescription ();

  public abstract boolean isInteractive ();

  public abstract String getGroupInView (String viewName);

  public abstract char getMnemonic ();

  public KeyStroke getAccelerator ()
  {
    return null;
  }

  public String getDisplayName ()
  {
    return displayName != null ? displayName : getDefaultDisplayName ();
  }

  public void setDisplayName (String newDisplayName)
  {
    displayName = newDisplayName;
  }

  /**
   * Generates a display name based on the last component of the
   * command name.  eg "file.oc.Edit" becomes "Edit".
   */
  protected String getDefaultDisplayName ()
  {
    String name = getName ();

    int i = name.lastIndexOf ('.');

    if (i == -1)
      return name;
    else
      return name.substring (i + 1, name.length ());
  }

  /**
   * Returns the command name followed by empty brackets.
   */
  public String getLogString ()
  {
    return getName () + "()";
  }

  /**
   * Returns true.
   */
  public boolean isEnabled ()
  {
    return true;
  }

  /**
   * Returns false;
   */
  public boolean canReplace (Command command)
  {
    return false;
  }

  public Icon getIcon ()
  {
    return icon;
  }

  public void setIcon (Icon newIcon)
  {
    icon = newIcon;
  }

  public Icon getLargeIcon ()
  {
    return null;
  }

  // ActionListener interface

  public void actionPerformed (ActionEvent e)
  {
    execute ();
  }

  public Object clone ()
  {
    try
    {
      return super.clone ();
    } catch (CloneNotSupportedException ex)
    {
      throw new Error ("clone threw CloneNotSupportedException");
    }
  }
}
