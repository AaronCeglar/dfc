package dsto.dfc.swing.commands;

import java.util.List;

import javax.swing.event.ChangeListener;

import dsto.dfc.util.EnumerationValue;

/**
 * Abstract base class for commands that act to select one or more
 * values from an enumerated set (eg zoom levels, open windows, etc).
 * Provides an implementation of CustomMenuProvider (via delegation to
 * EnumeratedCmdMenuSynchronizer) that generates an appropriate set of
 * menu items for selecting the available enumerated values.
 *
 * @version $Revision$
 */
public abstract class AbstractEnumeratedCommand
  extends AbstractMutableCommand
  implements CustomMenuProvider, EnumeratedCommand
{
  private EnumeratedCmdMenuProvider menuProvider;

  public AbstractEnumeratedCommand ()
  {
    menuProvider = new EnumeratedCmdMenuProvider (this);
  }

  public boolean isSelected (Object value)
  {
    Object [] selectedValues = getSelectedValues ();

    for (int i = 0; i < selectedValues.length; i++)
    {
      if (selectedValues [i].equals (value))
        return true;
    }

    return false;
  }

  // EnumeratedCommand implementation

  public abstract EnumerationValue [] getEnumValues ();

  public abstract void setNextSelectedValues (Object [] values);

  public abstract Object [] getNextSelectedValues ();

  public abstract Object [] getSelectedValues ();

  public abstract int getMaxSelectedValues ();

  public abstract void execute ();

  public abstract String getName ();

  public abstract String getDescription ();

  public abstract boolean isInteractive ();

  public abstract String getGroupInView (String viewName);

  public abstract char getMnemonic ();

  // CustomMenuProvider implementation (delegated to
  // EnumeratedCmdMenuSynchronizer)

  public List createMenuItems ()
  {
    return menuProvider.createMenuItems ();
  }

  public void destroyMenuItems (List items)
  {
    menuProvider.destroyMenuItems (items);
  }

  public void removeChangeListener (ChangeListener l)
  {
    menuProvider.removeChangeListener (l);
  }

  public void addChangeListener (ChangeListener l)
  {
    menuProvider.addChangeListener (l);
  }
}
