package dsto.dfc.swing.commands;

import java.util.EventObject;

public class CommandViewNodeEvent extends EventObject
{
  private CommandViewNode node;
  private int oldIndex;
  private int newIndex;
  private Command oldCommand;
  private Command newCommand;

  public CommandViewNodeEvent (CommandViewNode source, CommandViewNode node)
  {
    this (source, node, -1, -1);
  }

  public CommandViewNodeEvent (CommandViewNode source, CommandViewNode node,
                               int newIndex)
  {
    this (source, node, -1, newIndex);
  }

  public CommandViewNodeEvent (CommandViewNode source, CommandViewNode node,
                               int oldIndex, int newIndex)
  {
    super (source);

    this.node = node;
    this.oldIndex = oldIndex;
    this.newIndex = newIndex;
  }

  public CommandViewNodeEvent (CommandViewNode source,
                               Command oldCommand, Command newCommand)
  {
    super (source);

    this.node = null;
    this.oldCommand = oldCommand;
    this.newCommand = newCommand;
    this.oldIndex = -1;
    this.newIndex = -1;
  }

  public CommandViewNode getSourceNode ()
  {
    return (CommandViewNode)getSource ();
  }

  public CommandViewNode getNode ()
  {
    return node;
  }

  public int getOldIndex ()
  {
    return oldIndex;
  }

  public int getNewIndex ()
  {
    return newIndex;
  }
  
  public Command getOldCommand ()
  {
    return oldCommand;
  }

  public Command getNewCommand ()
  {
    return newCommand;
  }
} 
