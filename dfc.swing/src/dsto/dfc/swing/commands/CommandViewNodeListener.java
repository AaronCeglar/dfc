package dsto.dfc.swing.commands;

import java.util.EventListener;

/**
 * Defines the events fired by CommandViewNode's.
 *
 * @version $Revision$
 */
public interface CommandViewNodeListener extends EventListener
{
  /**
   * Fired when a child is added to the node.
   *
   * @param e The event. e e.getNode () == new node, e.getOldIndex ()
   * == -1, e.getNewIndex () == index of new node.
   */
  public void childAdded (CommandViewNodeEvent e);

  /**
   * Fired when a child is removed from the node.
   *
   * @param e The event. e.getNode () == removed node, e.getOldIndex
   * () == old index of node, e.getNewIndex () == -1.
   */
  public void childRemoved (CommandViewNodeEvent e);

  /** 
   * Fired when a child is moved within the node.
   *
   * @param e The event. e.getNode () == moved node, e.getOldIndex ()
   * == old index of node, e.getNewIndex () == new index.
   */
  public void childMoved (CommandViewNodeEvent e);

  /** 
   * Fired when a child is moved within the node.
   *
   * @param e The event. e.getSourceNode () == node that changed,
   * e.getNode () == null, e.getOldCommand () == old command (may be
   * null), e.getNewCommand () == new command.
   */
  public void commandChanged (CommandViewNodeEvent e);
} 
