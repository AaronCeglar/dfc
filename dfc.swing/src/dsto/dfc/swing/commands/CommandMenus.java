package dsto.dfc.swing.commands;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JMenuBar;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.tree.TreePath;

import dsto.dfc.swing.controls.Geometry;

/**
 * Utility for generating menus from command views.
 *
 * @version $Revision$
 */
public final class CommandMenus
{
  private static final String CONTEXT_MOUSE_LISTENER_PROPERTY =
    "CommandMenus.CONTEXT_MOUSE_LISTENER";

  private CommandMenus ()
  {
    // cannot be instantiated
  }

  /**
   * Create a menu bar from a command view.
   */
  public static void makeMenuBar (JMenuBar menuBar,
                                  CommandView commandView)
  {
    new CommandMenuBarSynchronizer (menuBar, commandView,
                                    commandView.getTreeRoot ());
  }

  public static void makePopupMenu (JPopupMenu menu,
                                    CommandView commandView)
  {
    makePopupMenu (menu, commandView, commandView.getTreeRoot ());
  }

  public static void makePopupMenu (JPopupMenu menu,
                                    CommandView commandView,
                                    CommandViewNode node)
  {
    new CommandMenuSynchronizer (menu, commandView, node, null);
  }

  /**
   * Install a context menu based on a given command view to popup
   * when the right mouse button is pressed on a component.
   *
   * @param target The component to detect clicks on.  If target is a
   * JComponent, auto-attach-to-scrollpane is enabled.
   * @param view The view for the context menu.
   */
  public static void installContextMenu (Component target,
                                         CommandView view)
  {
    installContextMenu (target, view, true);
  }

  /**
   * Install a context menu based on a given command view to popup
   * when the right mouse button is pressed on a component.
   *
   * @param target The component to detect clicks on.
   * @param view The view for the context menu.
   * @param attachToScrollPane If true, enable auto-attach-to-scrollpane
   * mode where the context menu pops up on right clicks in a scroll
   * pane containing target as well as in the target itself.
   */
  public static void installContextMenu (Component target,
                                         CommandView view,
                                         boolean attachToScrollPane)
  {
    ContextMenuMouseListener mouseListener =
      new ContextMenuMouseListener (target, view);
    target.addMouseListener (mouseListener);

    if (target instanceof JComponent)
    {
      JComponent jtarget = (JComponent)target;

      jtarget.putClientProperty (CONTEXT_MOUSE_LISTENER_PROPERTY,
                                 mouseListener);

      if (attachToScrollPane)
      {
        jtarget.addAncestorListener
          (new ContextMenuAncestorListener (mouseListener));
      }
    }
  }

  /**
   * Show the context menu (if any) installed on a component with
   * installContextMenu().
   *
   * @param parent The component to show the menu for.
   * @param x The X coord of the menu.
   * @param y The Y coord of the menu.
   * @return True if the menu was displayed.
   */
  public static boolean showContextMenu (Component parent, int x, int y)
  {
    if (parent instanceof JComponent)
    {
      JComponent jparent = (JComponent)parent;

      ContextMenuMouseListener listener =
        (ContextMenuMouseListener)jparent.getClientProperty
          (CONTEXT_MOUSE_LISTENER_PROPERTY);

      if (listener != null)
      {
        listener.showMenu (parent, x, y);

        return true;
      }
    }

    return false;
  }

  /**
   * Show a context menu at a given suggested location, bumping menu to fit
   * on screen as necessary.
   *
   * @param parent The parent of the menu.
   * @param menu The menu to show. If this menu has no compoments, then this
   * method is a noop.
   * @param x The suggested X coord of the menu.
   * @param y The suggested Y coord of the menu.
   *
   * @see #getMenuLocation
   */
  public static void showContextMenu (Component parent, JPopupMenu menu,
                                      int x, int y)
  {
    if (menu.getComponentCount () > 0)
    {
      menu.setSize (menu.getPreferredSize ());

      Point location = getMenuLocation (parent, menu, x, y);

      menu.show (parent, location.x, location.y);
    }
  }

  /**
   * Compute best location for menu, bumping location from (x, y) if
   * necessary to fit menu on screen.
   */
  public static Point getMenuLocation (Component parent, JPopupMenu menu,
                                       int x, int y)
  {
    Point menuLocation = new Point (x, y);

    // convert menu rect to absolute screeb rect
    SwingUtilities.convertPointToScreen (menuLocation, parent);
    Rectangle menuRect =
      new Rectangle (menuLocation.x, menuLocation.y,
                     menu.getWidth (), menu.getHeight ());

    // get screen rectangle (minus a bit around the left and bottom
    // edges as a fudge for windows toolbars).
    Dimension screenSize = Toolkit.getDefaultToolkit ().getScreenSize ();
    Rectangle screenRect =
      new Rectangle (0, 0, screenSize.width - 10, screenSize.height - 50);

    // bump the menu rect to fit in screen
    Geometry.bumpRectangle (screenRect, menuRect);

    // update menuLocation to be parent-relative location of menuRect
    menuLocation.x = menuRect.x;
    menuLocation.y = menuRect.y;

    SwingUtilities.convertPointFromScreen (menuLocation, parent);

    return menuLocation;
  }

  public static Command getCommandForMenuItem (JComponent menuComponent)
  {
    return (Command)menuComponent.getClientProperty("command");
  }

  /**
   * Installed as a component mouse listener by installContextMenu ().
   * Listens for right mouse clicks and triggers popup menu.
   */
  private static final class ContextMenuMouseListener extends MouseAdapter
  {
    protected Component target;
    protected JPopupMenu menu;
    protected CommandView view;

    public ContextMenuMouseListener (Component target, CommandView view)
    {
      super ();
      this.target = target;
      this.view = view;
    }

    private void checkMenuCreated ()
    {
      if (menu == null)
      {
        menu = new JPopupMenu ();

        //makePopupMenu (menu, view, target);
        new CommandMenuSynchronizer (menu,
                                     view, view.getTreeRoot (), target);
      }
    }

    public void showMenu (Component parent, int x, int y)
    {
      checkMenuCreated ();

      Point menuLocation = getMenuLocation (parent, menu, x, y);

      if (menu.getComponentCount () > 0)
        menu.show (parent, menuLocation.x, menuLocation.y);
    }

    public void mousePressed (MouseEvent e)
    {
      if (SwingUtilities.isRightMouseButton (e) && target.isEnabled ())
      {
        // auto select logic for trees and tables
        if (e.getSource () == target)
        {
          if (target instanceof JTree)
            treeAutoSelect (e);
          else if (target instanceof JTable)
            tableAutoSelect (e);
          else if (target instanceof JList)
            listAutoSelect (e);
        }

        showMenu ((Component)e.getSource (), e.getX (), e.getY ());
      }
    }

    public void mouseClicked (MouseEvent e)
    {
      Command command = view.getDefaultCommand ();

      //execute default command (if any) on left mouse double-click.
      if (command != null &&
          SwingUtilities.isLeftMouseButton (e) &&
          e.getClickCount () == 2 &&
          target.isEnabled () &&
          command.isEnabled () &&
          command.getGroupInView (CommandView.CONTEXT_MENU_VIEW) != null)
      {
        command.execute ();
      }
    }

    /**
     * Handle tree auto select logic: right clicking on a node selects
     * that node if not already selected.
     */
    protected void treeAutoSelect (MouseEvent e)
    {
      JTree tree = (JTree)target;
      TreePath path = tree.getPathForLocation (e.getX (), e.getY ());

      if (path == null)
      {
        tree.clearSelection ();
      } else if (!tree.isPathSelected (path))
      {
        // if click was on a node and it's not already selected...
        Rectangle pathRect = tree.getPathBounds (path);

        // if click really is inside the path node, select it
        if (pathRect.contains (e.getX (), e.getY ()))
          tree.setSelectionPath (path);
      }
    }

    /**
     * Handle table auto select logic: right clicking on a row selects
     * that row if not already selected.
     */
    protected void tableAutoSelect (MouseEvent e)
    {
      JTable table = (JTable)target;
      int row = table.rowAtPoint (e.getPoint ());

      if (row == -1)
      {
        table.clearSelection ();
      } else if (!table.isRowSelected (row))
      {
        table.setRowSelectionInterval (row, row);
      }
    }

    protected void listAutoSelect (MouseEvent e)
    {
      JList list = (JList)target;
      int row = list.locationToIndex (e.getPoint ());

      if (row == -1)
      {
        list.getSelectionModel ().clearSelection ();
      } else if (!list.isSelectedIndex (row))
      {
        list.setSelectedIndex (row);
      }
    }
  }

  /**
   * Listens for a JComponent being added to a JScrollPane's viewport
   * and auto attaches a mouse listener to the viewport.  This is used
   * to auto-enable context menus on scroll pane's.
   */
  private static final class ContextMenuAncestorListener
    implements AncestorListener
  {
    private ContextMenuMouseListener listener;

    public ContextMenuAncestorListener (ContextMenuMouseListener listener)
    {
      this.listener = listener;
    }

    public void ancestorAdded (AncestorEvent e)
    {
      if (listener.target.getParent () instanceof JViewport)
        listener.target.getParent ().addMouseListener (listener);
    }

    public void ancestorRemoved (AncestorEvent e)
    {
      if (listener.target.getParent () instanceof JViewport)
        listener.target.getParent ().removeMouseListener (listener);
    }

    public void ancestorMoved (AncestorEvent e)
    {
      // don't care
    }
  }
}
