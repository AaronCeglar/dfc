package dsto.dfc.swing.commands;

import javax.swing.KeyStroke;

import dsto.dfc.swing.icons.Iconic;

/**
 * Defines a command that may be executed by the user.
 *
 * @version $Revision$
 */
public interface Command extends Iconic
{
  /**
   * Execute the command.
   */
  public void execute ();

  /**
   * The formal identifying name for this command (eg edit.Copy, file.Open).
   * This <em>must not change</em> during the lifetime of a command
   * instance.
   */
  public String getName ();

  /**
   * The name of the command as it should be displayed (eg on a menu).  May
   * be null in which case the command name (or some derivative) will be
   * used.
   */
  public String getDisplayName ();

  /**
   * A short description of the command, suitable for a tooltip or use
   * in a status panel.
   */
  public String getDescription ();

  /**
   * A string that is suitable to describe this command and its
   * parameters for logging purposes.
   */
  public String getLogString ();

  /**
   * True if the command is enabled, ie able to execute in the current
   * context.
   */
  public boolean isEnabled ();

  /**
   * True if this command will interact with the user when execute()
   * is called.
   */
  public boolean isInteractive ();

  /**
   * Get the preferred group that this command should appear in for a
   * given view.  This may be overridden by a setting in the
   * CommandRegistry.<p>
   *
   * If the command changes its group in any view it should generate a
   * groupInView property change event with the old value set to the old
   * group and the new value set to the new group.
   *
   * @param viewName The name of the view.
   * @return The group in the view, or null if this command does not
   * appear in the given view.
   */
  public String getGroupInView (String viewName);

  /**
   * True if this command can logically replace another.  Used during
   * command merging.
   */
  public boolean canReplace (Command command);

  /**
   * Get a character shortcut for the command (eg for use on a menu).  May
   * return 0 for no mnemonic.
   */
  public char getMnemonic ();

  /**
   * Get a keyboard accelerator for the command.  May return null for no
   * accelerator.
   */
  public KeyStroke getAccelerator ();
}

