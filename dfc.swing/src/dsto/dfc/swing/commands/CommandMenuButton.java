package dsto.dfc.swing.commands;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import dsto.dfc.swing.controls.ToolButton;
import dsto.dfc.swing.icons.ImageLoader;
import dsto.dfc.util.PropertyEventSource;

/**
 * A button that displays the children of a command node in a drop down menu
 * plus a button to execute the 'default' command on the group.
 * 
 * @author Mofeed
 * @version $Revision$
 */
public class CommandMenuButton extends JComponent
{
  public static final Icon PULLDOWN_ICON = ImageLoader
      .getIcon("/dsto/dfc/icons/arrow_dropdown.gif");

  protected MenuComponentListener menuComponentListener = new MenuComponentListener();

  protected ButtonListener buttonListener = new ButtonListener();

  protected ToolButton executeButton;

  protected ToolButton pulldownButton;

  protected JPopupMenu popupMenu;

  protected CommandMenuSynchronizer menuSyncher;

  protected ArrayList actionListeners = new ArrayList();

  protected ArrayList changeListeners = new ArrayList();

  protected GridBagLayout gridBagLayout1 = new GridBagLayout();

  /**
   * Construct the CommandMenuButton given a CommandView tree, and
   * CommandViewNode to start from.
   * 
   * @param commandView The command view tree.
   * @param node The command view node to start from.
   */
  public CommandMenuButton(CommandView commandView, CommandViewNode node)
  {
    jbInit();

    this.menuSyncher = new CommandMenuSynchronizer(popupMenu, commandView,
        node, null);

    setDefaultCommand(getFirstCommand());

    popupMenu.addPopupMenuListener(buttonListener);
    popupMenu.addContainerListener(menuComponentListener);

    // add action listeners to all the menu items so we know when
    // things change.
    for (int i = 0; i < popupMenu.getComponentCount(); i++)
    {
      if (popupMenu.getComponent(i) instanceof AbstractButton)
        ((AbstractButton) popupMenu.getComponent(i))
            .addActionListener(menuComponentListener);
    }

    executeButton.getModel().addChangeListener(buttonListener);
    pulldownButton.getModel().addChangeListener(buttonListener);
    executeButton.addActionListener(buttonListener);
  }

  private void jbInit()
  {
    executeButton = new ToolButton();
    pulldownButton = new ToolButton(null, PULLDOWN_ICON);
    popupMenu = new JPopupMenu();

    this.setBorder(BorderFactory.createEmptyBorder());

    executeButton.setFocusPainted(false);
    pulldownButton.setFocusPainted(false);

    this.setLayout(gridBagLayout1);

    this.add(executeButton, new GridBagConstraints(0, 0, 1, 1, 0.75, 1.0,
        GridBagConstraints.EAST, GridBagConstraints.VERTICAL, new Insets(0, 0,
            0, 0), 0, 0));
    this.add(pulldownButton, new GridBagConstraints(1, 0, 1, 1, 0.25, 1.0,
        GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0,
            0, 0), 0, 0));
    this.add(popupMenu);
  }

  public void dispose()
  {
    actionListeners.clear();
    changeListeners.clear();

    menuSyncher.dispose();

    remove(popupMenu);
  }

  /**
   * Go through the popup menu looking for the first command.
   * 
   * @return The first command that is found, or null if one is not found.
   */
  protected Command getFirstCommand()
  {
    JComponent component;
    for (int i = 0; i < popupMenu.getComponentCount(); i++)
    {
      component = (JComponent) popupMenu.getComponent(i);

      if (CommandMenus.getCommandForMenuItem(component) != null)
        return CommandMenus.getCommandForMenuItem(component);
    }
    return null;
  }

  /**
   * Given a command it will put it as the default by placing it in the execute
   * button.
   */
  protected void setDefaultCommand(Command command)
  {
    if (command != null)
    {
      executeButton.putClientProperty("command", command);
      
      // Sync state between default command the and execute button
      // If there is a previous listener, remove it.
      DefaultCommandListener listener = (DefaultCommandListener)executeButton.getClientProperty("defaultCommandListener");
      if (listener != null)
      {
        listener.detachListener();
      }
      
      // add new listener
      if (command instanceof PropertyEventSource)
      {
        listener = new DefaultCommandListener((PropertyEventSource)command, command);
      }
      

      if (command.getIcon() != null)
      {
        executeButton.setIcon(command.getIcon());
        executeButton.setText("");
      }
      else
      {
        executeButton.setIcon(null);
        executeButton.setText(command.getDisplayName());
      }
      executeButton.setEnabled(command.isEnabled());
      executeButton.setToolTipText(command.getDescription());
    }
  }

  /**
   * Override JComponent's getMaximumSize() to return getPreferredSize() or else
   * this component doesn't lay itself out properly!!! :-(
   */
  public Dimension getMaximumSize()
  {
    return getPreferredSize();
  }

  /**
   * Add an action listener.
   * 
   * @param l The action listener to send action events to.
   */
  public void addActionListener(ActionListener l)
  {
    actionListeners.add(l);
  }

  /**
   * Remove an action listener.
   * 
   * @param l The action listener to stop sending action events to.
   */
  public void removeActionListener(ActionListener l)
  {
    actionListeners.remove(l);
  }

  /**
   * CustomToolBarProvider interface.
   */
  public void removeChangeListener(ChangeListener l)
  {
    changeListeners.remove(l);
  }
  
  /**
   * Keeps the execute button in sync with the underlying command
   * @author karu
   */
  private class DefaultCommandListener implements PropertyChangeListener
  {
    
    private PropertyEventSource evtSource;
    private Command cmd;
    
    
    DefaultCommandListener(PropertyEventSource evtSource, Command cmd)
    {
      this.evtSource = evtSource;
      this.cmd = cmd;
      attachListener();
    }
    

    public void propertyChange(PropertyChangeEvent evt)
    {
      String property = evt.getPropertyName();
      if (property.equals("icon"))
      {
        executeButton.setIcon(cmd.getIcon());
      }
      else if (property.equals("text"))
      {
        executeButton.setText(cmd.getDisplayName());
      }
      else if (property.equals("enabled"))
      {
        executeButton.setEnabled(cmd.isEnabled());
      }
      
    }
    
    void attachListener()
    {
      evtSource.addPropertyChangeListener(this);
      executeButton.putClientProperty("defaultCommandListener", this);
    }
    
    void detachListener()
    {
      evtSource.removePropertyChangeListener(this);
      executeButton.putClientProperty("defaultCommandListener", null);
    }
    
  }

  /**
   * Listens to the execute and pulldown buttons and popup visibility.
   */
  class ButtonListener implements ChangeListener, PopupMenuListener,
      ActionListener
  {
    /** True if in state change event handler */
    private boolean inStateChange = false;

    /** True if updating button rollover state */
    private boolean inUpdate = false;

    /** True if this control is in logical rollover */
    private boolean rollover = false;

    /** True if rollover is forced due to popup menu being visible */
    private boolean forceRollover = false;

    protected void setRollover(boolean newRollover)
    {
      if (inUpdate)
        return;

      rollover = newRollover;

      updateButtonRollover();
    }

    /**
     * Sync button rollover with rollover and forceRollover state
     */
    protected void updateButtonRollover()
    {
      if (inUpdate)
        return;

      inUpdate = true;

      boolean doRollover = rollover || forceRollover;

      pulldownButton.getModel().setRollover(doRollover);
      executeButton.getModel().setRollover(doRollover);

      inUpdate = false;
    }

    protected void setMenuVisible()
    {
      forceRollover = true;

      if (!popupMenu.isVisible())
      {
        popupMenu.show(CommandMenuButton.this, 0, CommandMenuButton.this
            .getHeight());
      }

      updateButtonRollover();
    }

    /**
     * Called when executeButton is pressed.
     */
    public void actionPerformed(ActionEvent e)
    {
      Command command = CommandMenus.getCommandForMenuItem(executeButton);

      if (command.isEnabled())
        command.execute();
    }

    /**
     * Called when execute or pulldown button models change.
     */
    public void stateChanged(ChangeEvent e)
    {
      if (inUpdate || inStateChange)
        return;

      inStateChange = true;

      if (e.getSource() == pulldownButton.getModel())
      {
        // show menu when pulldown is pressed
        if (pulldownButton.getModel().isPressed())
        {
          if (!popupMenu.isVisible())
            setMenuVisible();
        }

        // if models out of sync: change logical rollover
        if (pulldownButton.getModel().isRollover() != executeButton.getModel()
            .isRollover())
        {
          setRollover(pulldownButton.getModel().isRollover());
        }
      }
      else
      {
        // if models out of sync: change logical rollover
        if (pulldownButton.getModel().isRollover() != executeButton.getModel()
            .isRollover())
        {
          setRollover(executeButton.getModel().isRollover());
        }
      }

      inStateChange = false;
    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e)
    {
      // zip
    }

    /**
     * This is called when the popup menu is about disappear. We use this to set
     * the border of the buttons to nothing.
     */
    public void popupMenuWillBecomeInvisible(PopupMenuEvent e)
    {
      forceRollover = false;

      updateButtonRollover();
    }

    public void popupMenuCanceled(PopupMenuEvent e)
    {
      // zip
    }
  }

  /**
   * Listens for menu changes and menu component actions.
   */
  class MenuComponentListener implements ActionListener,
      ContainerListener
  {
    public void actionPerformed(ActionEvent e)
    {
      JComponent menuComponent = (JComponent) e.getSource();
      Command command = CommandMenus.getCommandForMenuItem(menuComponent);
      setDefaultCommand(command);
    }

    public void componentAdded(ContainerEvent e)
    {
      Component component = e.getChild();

      if (component instanceof AbstractButton)
      {
        AbstractButton button = (AbstractButton) component;
        button.addActionListener(menuComponentListener);
      }

      // If it is the first component to be added then make it the
      // default. The first component to get added, gets added at
      // position 2 and not 1.
      if (popupMenu.getComponentCount() == 2)
        setDefaultCommand(getFirstCommand());
    }

    public void componentRemoved(ContainerEvent e)
    {
      Component component = e.getChild();

      if (component instanceof AbstractButton)
      {
        AbstractButton button = (AbstractButton) component;
        button.removeActionListener(menuComponentListener);
      }
    }
  }
}
