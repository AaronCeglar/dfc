package dsto.dfc.swing.commands;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;

/**
 * A host panel that shows a component in the center and a toolbar built from
 * the component's toolbar command view at the top (the component must
 * implement {@link CommandSource} and return a TOOLBAR_VIEW for this be
 * automatic).  The component can be then added to this panel using any one of
 * the Component.add () methods.  Any other command view types that the
 * component supports are exported. When using a component wrapped in a
 * JScrollPane, be sure that the component is added to the scroll pane before
 * being added to this panel or the toolbar commands will not be found.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class ToolbarPanel extends JPanel implements CommandSource
{
  protected Component hostedComponent;
  protected CommandView toolbarView = new CommandView (CommandView.TOOLBAR_VIEW);

  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private JToolBar toolbar = new JToolBar();

  public ToolbarPanel ()
  {
    jbInit ();

    CommandToolBars.makeToolBar (toolbar, toolbarView);
  }

  public CommandView getCommandView (String viewName)
  {
    if (viewName.equals (CommandView.TOOLBAR_VIEW))
      return toolbarView;
    else if (hostedComponent instanceof CommandSource)
      return ((CommandSource)hostedComponent).getCommandView (viewName);
    else
      return null;
  }

  public JToolBar getToolbar ()
  {
    return toolbar;
  }

  protected CommandView findHostedCommandView ()
  {
    if (hostedComponent instanceof CommandSource)
    {
      return((CommandSource)hostedComponent).getCommandView (CommandView.TOOLBAR_VIEW);
    } else if (hostedComponent instanceof JScrollPane)
    {
      Component component = ((JScrollPane)hostedComponent).getViewport ().getView ();

      if (component instanceof CommandSource)
        return((CommandSource)component).getCommandView (CommandView.TOOLBAR_VIEW);
    }

    return null;
  }

  protected void addImpl (Component component, Object constraints, int index)
  {
    if (component == toolbar)
    {
      super.addImpl (component, constraints, index);
    } else
    {
      if (hostedComponent != null)
      {
        CommandView view = findHostedCommandView ();

        if (view != null)
          toolbarView.removeView (view);

        remove (hostedComponent);
      }

      hostedComponent = component;

      CommandView view = findHostedCommandView ();

      if (view != null)
        toolbarView.addView (view);

      super.addImpl
        (hostedComponent,
         new GridBagConstraints (0, 1, 1, 1, 1.0, 1.0,
                                 GridBagConstraints.NORTHWEST,
                                 GridBagConstraints.BOTH,
                                 new Insets (3, 0, 0, 0), 0, 0), 1);
    }
  }

  private void jbInit ()
  {
    toolbar.setBorder (null);
    this.setLayout(gridBagLayout1);
    this.add(toolbar, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
  }
}