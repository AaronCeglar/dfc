package dsto.dfc.swing.commands;

import javax.swing.JToolBar;
import javax.swing.border.CompoundBorder;

import dsto.dfc.swing.controls.EtchedDividingBorder;

/**
 * Utility class for creating toolbars from command views.
 *
 * @version $Revision$
 */
public final class CommandToolBars
{
  /**
   * Key for indicating that a command should be shown with its command name
   * text on the toolbar.<p>
   *
   * eg:
   * <pre>
   *   commandView.setCommandProperty (command.getName (),
   *                                   CommandToolbars.SHOW_NAME_KEY,
   *                                   Boolean.TRUE);
   * </pre>
   */
  public static final Object SHOW_NAME_KEY = "toolbar_show_name";

  private CommandToolBars ()
  {
    // zip
  }

  /**
   * Create a toolbar from the specified command view.
   */
  public static void makeToolBar (JToolBar toolbar,
                                  CommandView commandView)
  {
    new CommandToolBarSynchronizer (toolbar, commandView,
                                    commandView.getTreeRoot());
  }

  /**
   * Add a divider to the bottom of the toolbar, similar to that used
   * at the bottom of a menu bar.
   */
  public static void addToolBarDivider (JToolBar toolBar)
  {
    toolBar.setBorder
      (new CompoundBorder (new EtchedDividingBorder (),
                           toolBar.getBorder ()));

  }

  /**
   * Shortcut to set the {@link #SHOW_NAME_KEY} command property to true.
   * You usually have to call this before adding the command to the view for
   * the toolbar UI to honor it.
   */
  public static void showName (CommandView view, Command command)
  {
    view.setCommandProperty (command.getName (), SHOW_NAME_KEY, Boolean.TRUE);
  }
}