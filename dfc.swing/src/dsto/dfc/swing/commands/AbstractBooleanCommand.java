package dsto.dfc.swing.commands;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.event.ChangeListener;

import dsto.dfc.swing.controls.ToolButton;
import dsto.dfc.swing.icons.Iconic;

/**
 * Base class for commands that can be 'on' or 'off'.  Provides custom
 * checkbox menu item creation to show command state.
 *
 * @version $Revision$
 */
public abstract class AbstractBooleanCommand 
  extends AbstractMutableCommand 
  implements CustomMenuProvider, CustomToolBarProvider
{
  private boolean value = false;

  public AbstractBooleanCommand ()
  {
    super ();
  }

  public AbstractBooleanCommand (String iconName)
  {
    super (iconName);
  }

  public boolean getValue ()
  {
    return value;
  }

  public void setValue (boolean newValue)
  {
    boolean oldValue = value;

    value = newValue;

    listeners.firePropertyChange ("value", oldValue, newValue);
  }

  public List createMenuItems ()
  {
    JCheckBoxMenuItem item =
      new JCheckBoxMenuItem (getDisplayName (), getValue ());
    Synchronizer syncher = new Synchronizer (item);

    if (getIcon () != null)
    {
      item.setIcon (getIcon ());
      item.putClientProperty (Action.SMALL_ICON, getIcon ());
    } else
    {
      item.setIcon (Iconic.NULL_ICON);
    }

    item.putClientProperty (Action.SHORT_DESCRIPTION, getDescription ());
    item.putClientProperty ("syncher", syncher);

    ArrayList items = new ArrayList (1);
    items.add (item);

    return items;
  }

  public void destroyMenuItems (List items)
  {
    JCheckBoxMenuItem item = (JCheckBoxMenuItem)items.get (0);
    Synchronizer syncher = (Synchronizer)item.getClientProperty ("syncher");

    syncher.dispose ();
  }

  public JComponent createToolBarComponent()
  {
    ToolButton item = new ToolButton ();
    item.setToggle (true);
    item.setSelected (getValue ());

    item.setFocusPainted (false);
    Synchronizer syncher = new Synchronizer (item);

    if (getIcon () != null)
    {
      item.setIcon (getIcon ());
      item.putClientProperty (Action.SMALL_ICON, getIcon ());
    } else
    {
      item.setIcon (Iconic.NULL_ICON);
    }

    item.setToolTipText (getDescription ());
    item.setMnemonic (getMnemonic ());

    item.putClientProperty (Action.SHORT_DESCRIPTION, getDescription ());
    item.putClientProperty ("syncher", syncher);

    return item;
  }

  public void destroyToolBarComponent(JComponent component)
  {
    ToolButton item = (ToolButton)component;
    Synchronizer syncher = (Synchronizer)item.getClientProperty ("syncher");

    syncher.dispose ();
  }

  public void addChangeListener (ChangeListener l)
  {
    // zip
  }

  public void removeChangeListener (ChangeListener l)
  {
    // zip
  }

  /**
   * Synchronizes a command and a menu item created for the command.
   */
  private final class Synchronizer
    implements PropertyChangeListener, ActionListener
  {
    private AbstractButton button;

    public Synchronizer (AbstractButton button)
    {
      this.button = button;

      addPropertyChangeListener (this);
      button.addActionListener (this);

      button.setEnabled (isEnabled ());
    }

    public void dispose ()
    {
      removePropertyChangeListener (this);
      button.removeActionListener (this);
    }

    public void propertyChange (PropertyChangeEvent e)
    {
      if (e.getPropertyName ().equals ("value"))
        button.setSelected (getValue ());

      else if (e.getPropertyName ().equals ("enabled"))
        button.setEnabled (isEnabled ());
    }

    public void actionPerformed (ActionEvent e)
    {
      setValue (button.isSelected ());
      execute ();
    }
  }
}
