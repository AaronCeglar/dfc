package dsto.dfc.swing.commands;

import dsto.dfc.util.PropertyEventSource;

/**
 * Use when you want a command that 'selects' from a defined set of
 * commands.
 *
 * @author Mofeed.
 */

public interface CompositeCommand extends Command, PropertyEventSource
{
  /**
   * Gets the currently selected command/s. If these values change, the composite
   * command should fire a property change event. If maxSelectedCommands = 0, this
   * should return an empty array.
   */
  public Command[] getSelectedCommands ();

  /**
   * Get the maximum number of commands that may be selected at any one time.
   * eg this may affect the way the menu items are rendered for the command: if
   * max selected values is 0, the normal JMenuItems are used for each value, if
   * 1, then JRadioButtonMenuItems are used and if > 1 then JCheckBoxMenuItems are
   * used.
   */
  public int getMaxSelectedCommands ();

  /**
   * Returns the complete list of defined 'selectable' commands.
   */
  public Command[] getCommands ();
}