package dsto.dfc.swing.commands;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

public class BasicCutCommand extends AbstractCommand
{
  private static final KeyStroke ACCELERATOR =
    KeyStroke.getKeyStroke (KeyEvent.VK_X,
                            Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask ());

  public BasicCutCommand ()
  {
    super ("edit_cut.gif");
  }

  public void execute ()
  {
    // zip
  }

  public String getName ()
  {
    return "edit.Cut";
  }

  public String getDescription ()
  {
    return "Move selection to the clipboard";
  }

  public boolean isInteractive ()
  {
    return false;
  }

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return "Edit.cnp";
    else if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return "cnp";
    else if (viewName.equals (CommandView.TOOLBAR_VIEW))
      return "cnp";
    else
      return null;
  }

  public char getMnemonic ()
  {
    return 't';
  }

  public KeyStroke getAccelerator ()
  {
    return ACCELERATOR;
  }
}
