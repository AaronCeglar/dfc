package dsto.dfc.swing.commands;

import javax.swing.JComponent;

/**
 * Binds all commands within a command view with their specified accelerator
 * @author karu
 *
 */
public class CommandKeyStrokeBinding
{
  public static void bind (JComponent parent, CommandView rootView)
  {
    for (int i = 0; i < rootView.getEntryCount (); i++)
    {
      Object entry = rootView.getEntry (i);
      if (entry instanceof Command)
      {
        Command command = (Command) entry;
        if (command.getAccelerator () != null)
        {
          parent.getInputMap (JComponent.WHEN_IN_FOCUSED_WINDOW).put (command.getAccelerator (), command.getName ());
          parent.getActionMap ().put (command.getName (), new CommandActionAdapter (command));
        }
      }
      else
      {
        CommandView view = (CommandView) entry;
        bind (parent, view);
      }
    }
  }
}
