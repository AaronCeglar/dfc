package dsto.dfc.swing.commands;

import javax.swing.JComponent;
import javax.swing.event.ChangeListener;

/**
 * Interface for commands that wish to provide a customized toolbar UI.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public interface CustomToolBarProvider
{
  /**
   * Create the Component that represent the UI for the command.
   *
   * @return A new Component to be added to the ToolBar for the command.
   */
  public JComponent createToolBarComponent ();

  /**
   * Destroy the Component created by createToolBarComponent ().
   */
  public void destroyToolBarComponent (JComponent component);

  /**
   * Custom tool bar component providers should fire change events
   * when the ToolBar UI needs to be updated.  This event will trigger
   * any interested listeners to destroy old Components
   * (destroyToolBarComponent ()) and create new ones
   * (createToolBarComponent ()).
   */
  public void addChangeListener (ChangeListener l);

  public void removeChangeListener (ChangeListener l);

}
