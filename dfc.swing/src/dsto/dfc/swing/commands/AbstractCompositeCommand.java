package dsto.dfc.swing.commands;

import java.util.List;

import javax.swing.Icon;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeListener;

/**
 * The AbstractCompositeCommand should be used when you want to have a list of
 * commands that are in some way linked together.
 *
 * @author Mofeed
 */

public abstract class AbstractCompositeCommand
  extends AbstractMutableCommand
  implements CompositeCommand, CustomMenuProvider
{
  private CompositeCmdMenuProvider menuProvider;

  /**
   * Default constructor creates a menu provider.
   */
  public AbstractCompositeCommand()
  {
    menuProvider = new CompositeCmdMenuProvider (this);
    setEnabled(true);
  }

  public abstract Command [] getSelectedCommands ();

  public abstract int getMaxSelectedCommands ();

  public abstract Command [] getCommands ();

  public abstract void execute ();

  public abstract String getName ();

  public abstract String getDescription ();

  public abstract boolean isInteractive ();

  public abstract String getGroupInView (String viewName);

  public abstract char getMnemonic ();

  public abstract Icon getIcon ();

  public String getLogString()
  {
    return getName() + "()";
  }

  public boolean canReplace(Command command)
  {
    return false;
  }

  public KeyStroke getAccelerator()
  {
    return null;
  }

  public Icon getLargeIcon()
  {
    return null;
  }

  public boolean isSelected (Command cmdChild)
  {
    Command [] selectedCmds = getSelectedCommands ();

    for (int i = 0; i < selectedCmds.length; i++)
    {
      if (selectedCmds [i].equals (cmdChild))
        return true;
    }
    return false;
  }

  // CustomMenuProvider implementation (delegated to
  // CompositeCmdMenuSynchronizer)

  public List createMenuItems ()
  {
    return menuProvider.createMenuItems ();
  }

  public void destroyMenuItems (List menuItems)
  {
    menuProvider.destroyMenuItems (menuItems);
  }

  public void removeChangeListener (ChangeListener l)
  {
    menuProvider.removeChangeListener (l);
  }

  public void addChangeListener (ChangeListener l)
  {
    menuProvider.addChangeListener (l);
  }
}
