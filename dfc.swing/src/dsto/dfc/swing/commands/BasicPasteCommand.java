package dsto.dfc.swing.commands;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

public class BasicPasteCommand extends AbstractCommand
{
  private static final KeyStroke ACCELERATOR =
    KeyStroke.getKeyStroke (KeyEvent.VK_V,
                            Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask ());

  public BasicPasteCommand ()
  {
    super ("edit_paste.gif");
  }

  public void execute ()
  {
    // zip
  }

  public String getName ()
  {
    return "edit.Paste";
  }

  public String getDescription ()
  {
    return "Paste contents of clipboard";
  }

  public boolean isInteractive ()
  {
    return false;
  }

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return "Edit.cnp";
    else if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return "cnp";
    else if (viewName.equals (CommandView.TOOLBAR_VIEW))
      return "cnp";
    else
      return null;
  }

  public char getMnemonic ()
  {
    return 'p';
  }

  public KeyStroke getAccelerator ()
  {
    return ACCELERATOR;
  }
}
