package dsto.dfc.swing.commands;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.AbstractButton;

import dsto.dfc.util.Disposable;
import dsto.dfc.util.PropertyEventSource;

/**
 * Synchronize a command with a JFC button.  Reflects text, icon and
 * enabled state of command on button and executes command on action
 * from button.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class CommandButtonSynchronizer
  implements ActionListener, PropertyChangeListener, Disposable
{
  private Command command;
  private AbstractButton button;
  private boolean showText;

  public CommandButtonSynchronizer (Command command, AbstractButton button)
  {
    this (command, button, true);
  }

  public CommandButtonSynchronizer (Command command, AbstractButton button,
                                    boolean showText)
  {
    this.command = command;
    this.button = button;
    this.showText = showText;

    button.addActionListener (this);

    if (command instanceof PropertyEventSource)
      ((PropertyEventSource)command).addPropertyChangeListener (this);

    updateButton ();
  }

  public void dispose ()
  {
    if (command != null)
    {
      button.removeActionListener (this);

      if (command instanceof PropertyEventSource)
        ((PropertyEventSource)command).removePropertyChangeListener (this);

      command = null;
    }
  }

  protected void updateButton ()
  {
    if (showText)
      button.setText (command.getDisplayName ());
    else
      button.setText (null);

    button.setIcon (command.getIcon ());
    button.setToolTipText (command.getDescription ());
    button.setEnabled (command.isEnabled ());
  }

  public void actionPerformed (ActionEvent e)
  {
    if (command.isEnabled ())
      command.execute ();
  }

  public void propertyChange (PropertyChangeEvent e)
  {
    updateButton ();
  }
}
