package dsto.dfc.swing.commands;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import dsto.dfc.util.PropertyEventSource;

/**
 * Implements the command menu provider interface for composite commands.
 * 
 * @author shahinm
 * @created 9th March 2001
 * @version $Revision$
 */
public class CompositeCmdMenuProvider implements CustomMenuProvider,
    ActionListener
{
  // The composite command that this menu provider will render the contents of.
  protected CompositeCommand compositeCmd;

  // A list of change listeners to notify when the things change.
  protected transient ArrayList changeListeners = new ArrayList();

  // button container to ensure toggle button exclusivity
  protected ButtonGroup buttonGroup;

  public CompositeCmdMenuProvider(CompositeCommand command)
  {
    this.compositeCmd = command;

    // Add a listener to listen to when a composite command changes its
    // child commands
    compositeCmd.addPropertyChangeListener(new CommandListener());
  }

  public boolean isSelected(Command childCmd)
  {
    Command[] selectedCommands = compositeCmd.getSelectedCommands();

    if (selectedCommands == null)
      return false;

    for (int i = 0; i < selectedCommands.length; i++)
    {
      if (selectedCommands[i].equals(childCmd))
        return true;
    }

    return false;
  }

  public List createMenuItems()
  {
    Command[] childCmds = compositeCmd.getCommands();
    ArrayList menuItems = new ArrayList(childCmds.length);
    buttonGroup = new ButtonGroup();

    for (int i = 0; i < childCmds.length; i++)
      menuItems.add(createMenuItem(buttonGroup, childCmds[i]));

    // Add a synchronizer to the composite command. This is used to keep
    // the currently selected commands and the menu in sync.
    new Synchronizer(compositeCmd, menuItems);

    return menuItems;
  }

  public void destroyMenuItems(List menuItems)
  {
    for (int i = 0; i < menuItems.size(); i++)
    {
      JMenuItem menuItem = (JMenuItem) menuItems.get(i);
      menuItem.putClientProperty("command", null);
      menuItem.removeActionListener(this);

      // detach property listener syncing menu item with underlying command
      // object
      SyncMenuItem sync = (SyncMenuItem) menuItem
          .getClientProperty("syncMenuItem");
      if (sync != null)
      {
        sync.detachListener();
      }
    }

    // Remove the synchronizer from the composite command.
    Synchronizer syncher = (Synchronizer) ((JComponent) menuItems.get(0))
        .getClientProperty("syncher");
    syncher.detachFrom(compositeCmd);

    buttonGroup = null;
  }

  /**
   * Create a menu item for the child command
   * 
   * @param itemButtonGroup the button group to add the menu item to.
   * @param childCmd the command that a menu item is to be created for.
   * @return The JMenuItem that represents the child command.
   */
  protected JMenuItem createMenuItem(ButtonGroup itemButtonGroup,
      Command childCmd)
  {
    JMenuItem menuItem;
    Icon icon = childCmd.getIcon();
    String label = childCmd.getDisplayName();

    if (compositeCmd.getMaxSelectedCommands() == 1)
    {
      menuItem = new JRadioButtonMenuItem(label);
      itemButtonGroup.add(menuItem);
    }
    else if (compositeCmd.getMaxSelectedCommands() > 1)
      menuItem = new JCheckBoxMenuItem(label);
    else
      menuItem = new JMenuItem(label);

    // put the menu item in the same state as the command
    menuItem.setIcon(icon);
    menuItem.setEnabled(childCmd.isEnabled());

    if (compositeCmd.getMaxSelectedCommands() != 0)
      menuItem.setSelected(isSelected(childCmd));

    // Use the menu items client property to set the association between the
    // command
    // the menu item.
    menuItem.putClientProperty("command", childCmd);
    menuItem.putClientProperty(Action.SHORT_DESCRIPTION, childCmd
        .getDescription());
    menuItem.putClientProperty(Action.SMALL_ICON, icon);

    // Listen to when people select the menu item.
    menuItem.addActionListener(this);

    // keep the item in sync with the underlying
    // command object
    if (childCmd instanceof PropertyEventSource)
    {
      new SyncMenuItem((PropertyEventSource) childCmd, childCmd, menuItem);
    }

    return menuItem;
  }

  // ActionListener interface
  public void actionPerformed(ActionEvent e)
  {
    // Get the command from the menu item's client property.
    Command childCmd = CommandMenus.getCommandForMenuItem((JComponent) e
        .getSource());

    if (childCmd.isEnabled())
      childCmd.execute();
  }

  public synchronized void removeChangeListener(ChangeListener l)
  {
    changeListeners.remove(l);
  }

  public synchronized void addChangeListener(ChangeListener l)
  {
    if (!changeListeners.contains(l))
      changeListeners.add(l);
  }

  protected void fireStateChanged()
  {
    fireStateChanged(new ChangeEvent(compositeCmd));
  }

  /**
   * Informs all the change listeners that the state of this menu provider has
   * changed.
   */
  protected void fireStateChanged(ChangeEvent e)
  {
    ArrayList newChangeListeners = (ArrayList) changeListeners.clone();
    Iterator i = newChangeListeners.iterator();
    while (i.hasNext())
    {
      ChangeListener l = (ChangeListener) i.next();
      l.stateChanged(e);
    }
  }

  /**
   * Keeps menu items in sync with mutable commands for properties 'enabled',
   * 'icon' and 'displayName'
   */
  private final class SyncMenuItem implements PropertyChangeListener
  {

    private PropertyEventSource evtSource;

    private JMenuItem item;

    private Command cmd;

    SyncMenuItem(PropertyEventSource evtSource, Command cmd, JMenuItem item)
    {
      this.cmd = cmd;
      this.item = item;
      this.evtSource = evtSource;
      attachListener();
    }

    public void propertyChange(PropertyChangeEvent evt)
    {
      String property = evt.getPropertyName();
      if (property.equals("enabled"))
      {
        item.setEnabled(cmd.isEnabled());
      }
      else if (property.equals("icon"))
      {
        item.setIcon(cmd.getIcon());
      }
      else if (property.equals("displayName"))
      {
        item.setText(cmd.getDisplayName());
      }
    }

    void attachListener()
    {
      evtSource.addPropertyChangeListener(this);
      item.putClientProperty("syncMenuItem", this);
    }

    void detachListener()
    {
      evtSource.removePropertyChangeListener(this);
      item.putClientProperty("syncMenuItem", null);
    }
  }

  /**
   * Synchronizes the current value of the command and the selected checkbox
   * menu item.
   */
  private final class Synchronizer implements PropertyChangeListener
  {
    private List menuItems;

    /**
     * Create a synchronizer for specified menu items from the specified
     * CompositeCommand and get the synchronizer to listen to the command for
     * property changes to update the menu items.
     * 
     * @param compositeCmd The CompositeCommand to which the menuItems belong.
     * @param menuItems The menuItems to keep in sync.
     */
    public Synchronizer(CompositeCommand compositeCmd, List menuItems)
    {
      this.menuItems = menuItems;

      // Add this synchronizer to the composite command. This is used to keep
      // the currently selected commands and the menu in sync.
      attachTo(compositeCmd);
    }

    /**
     * Attach this Synchronizer to the specified CompositeCommand to listen for
     * property changes to keep its GUI representation in sync.
     * 
     * @param cmd The CompositeCommand to listen to.
     */
    public void attachTo(CompositeCommand cmd)
    {
      cmd.addPropertyChangeListener(this);
      ((JComponent) menuItems.get(0)).putClientProperty("syncher", this);
    }

    /**
     * Detach from the specified CompositeCommand and stop listening to it for
     * property changes to keep its GUI representation in sync.
     * 
     * @param cmd The CompositeCommand currently being listened to.
     */
    public void detachFrom(CompositeCommand cmd)
    {
      cmd.removePropertyChangeListener(this);
      ((JComponent) menuItems.get(0)).putClientProperty("syncher", null);
    }

    /**
     * When called, goes through all of the menu items and sets their selection
     * accordeing to whether their associated command is selected or not.
     */
    public void updateMenu()
    {
      for (int i = 0; i < menuItems.size(); i++)
      {
        JMenuItem menuItem = (JMenuItem) menuItems.get(i);

        Command childCmd = CommandMenus.getCommandForMenuItem(menuItem);

        if (compositeCmd.getMaxSelectedCommands() != 0)
          menuItem.setSelected(isSelected(childCmd));
      }
    }

    public void propertyChange(PropertyChangeEvent e)
    {
      if (e.getPropertyName().equals("selectedValues"))
        updateMenu();
    }
  }

  /**
   * Listens for changes to this command's enumValues and fires stateChanged
   * events for CustomCommandProvider listeners.
   */
  class CommandListener implements PropertyChangeListener
  {
    public void propertyChange(PropertyChangeEvent e)
    {
      if (e.getPropertyName().equals("enumValues"))
        fireStateChanged();
    }
  }
}
