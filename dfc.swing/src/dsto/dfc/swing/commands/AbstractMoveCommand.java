package dsto.dfc.swing.commands;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.Icon;
import javax.swing.KeyStroke;

import dsto.dfc.swing.event.SelectionEvent;
import dsto.dfc.swing.event.SelectionEventSource;
import dsto.dfc.swing.icons.ImageLoader;

/**
 * Base class for commands that move items up/down within their host.
 * Subclasses that support auto enable/disable should override
 * {@link #updateEnabled} and remember to call it from their
 * constructor.
 *
 * @version $Revision$
 */
public abstract class AbstractMoveCommand extends AbstractSelectionBasedCommand
{
  public static final Icon MOVE_UP_ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/arrow_up.gif");
  public static final Icon MOVE_DOWN_ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/arrow_down.gif");

  public static final boolean MOVE_UP = true;
  public static final boolean MOVE_DOWN = false;

  protected boolean direction;

  /**
   * Create a new instance.
   *
   * @param client The client of the command.
   * @param direction The direction to move the entries (MOVE_UP or
   * MOVE_DOWN).
   */
  public AbstractMoveCommand (SelectionEventSource client,
                              boolean direction)
  {
    super (client);

    this.direction = direction;
  }

  public abstract void execute ();

  public String getName ()
  {
    if (direction == MOVE_UP)
      return "edit.Move Up";
    else
      return "edit.Move Down";
  }

  public Icon getIcon ()
  {
    if (direction == MOVE_UP)
      return MOVE_UP_ICON;
    else
      return MOVE_DOWN_ICON;
  }

  public String getDescription ()
  {
    if (direction == MOVE_UP)
      return "Move the current item up a step";
    else
      return "Move the current item down a step";
  }

  public boolean isInteractive ()
  {
    return false;
  }

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return "move";
    else if (viewName.equals (CommandView.TOOLBAR_VIEW))
      return "move";
    else if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return "Edit.move";
    else
      return null;
  }

  public char getMnemonic ()
  {
    return 0;
  }

  public KeyStroke getAccelerator ()
  {
    int keyEvent = (direction == MOVE_UP) ? KeyEvent.VK_UP : KeyEvent.VK_DOWN;

    return KeyStroke.getKeyStroke
      (keyEvent, Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask ());
  }

  public void selectionChanged (SelectionEvent e)
  {
    updateEnabled ();
  }

  /**
   * Override this to update the enabled property appopriately.
   */
  protected void updateEnabled ()
  {
    setEnabled (true);
  }
}
