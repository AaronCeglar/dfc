package dsto.dfc.swing.commands;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JSeparator;
import javax.swing.JToolBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import dsto.dfc.swing.controls.ToolButton;
import dsto.dfc.util.Debug;
import dsto.dfc.util.PropertyEventSource;

/**
 * Keeps the toolbar and the command view synchronized.  This is
 * heavily based on CommandViewMenuProvider.
 *
 * @author Mofeed.
 */
final class CommandToolBarSynchronizer
  implements PropertyChangeListener, CommandViewNodeListener,
             ActionListener, ChangeListener
{
  protected JToolBar toolBar;
  protected CommandView commandView;
  protected CommandViewNode root;
  protected HashMap nodeToToolBarButton = new HashMap();

  /**
   * Create a new instance, adding all the entries, and setting up
   * all the listeners.
   *
   * @param toolBar The JToolBar that you want syncronized.
   * @param commandView The CommandView that you want synchronized.
   * @param root The root CommandViewNode that you want to start from.
   */
  public CommandToolBarSynchronizer (JToolBar toolBar,
                                     CommandView commandView,
                                     CommandViewNode root)
  {
    this.toolBar = toolBar;
    this.commandView = commandView;
    this.root = root;

    toolBar.addPropertyChangeListener("orientation",
      new ToolBarOrientationChangeListener());

    // JDK 1.4 support: enable ToolButton's rollover effects
    toolBar.putClientProperty ("JToolBar.isRollover", Boolean.TRUE);

    addEntries ();

    commandView.addPropertyChangeListener(this);
    root.addCommandViewNodeListener(this);

    validateToolbar ();
  }

  /**
   * Remove all listeners, and then remove all the commands and
   * command groups.
   */
  public void dispose ()
  {
    root.removeCommandViewNodeListener (this);
    commandView.removePropertyChangeListener (this);

    for (int i = 0; i < root.getChildCount (); i++)
    {
      CommandViewNode node = root.getChild (i);

      if (node.getCommand () != null)
        removeCommand (node);
      else
        removeGroup (node, i);
    }
  }

  private void addEntries ()
  {
    int index = 0;

    for (int i = 0; i < root.getChildCount (); i++)
      index += addGroup (root.getChild (i), index);
  }

  private int addGroup (CommandViewNode groupNode, int toolBarIndex)
  {
    int currentToolBarIndex = toolBarIndex;
    int groupIndex = groupNode.getIndex ();
    JSeparator separator = new DfcToolBarSeparator();

    nodeToToolBarButton.put (groupNode, separator);

    separator.setVisible (groupIndex > 0);
    separator.setBackground (toolBar.getBackground ());
    toolBar.add (separator, currentToolBarIndex);
    currentToolBarIndex++;

    groupNode.addCommandViewNodeListener (this);

    for (int i = 0; i < groupNode.getChildCount (); i++)
    {
      CommandViewNode childNode = groupNode.getChild (i);

      if (childNode.getCommand () != null)
      {
        addCommand (childNode, currentToolBarIndex);
        currentToolBarIndex++;
      }
      else
      {
        // create command menu button.
        addCommandMenuButton (childNode, currentToolBarIndex);
        currentToolBarIndex++;
      }
    }

    CommandViewNode parent = groupNode.getParent ();
    if (groupIndex == 0 && parent.getChildCount () > 1)
    {
      JComponent followingSeparator =
        (JComponent)nodeToToolBarButton.get (parent.getChild (1));

      // followingSeparator may be null if group has not been
      // processed yet
      if (followingSeparator != null)
        followingSeparator.setVisible (true);
    }

    return currentToolBarIndex - toolBarIndex;
  }

  private void addCommand (CommandViewNode node, int toolBarIndex)
  {
    createToolBarComponent (node, toolBarIndex);

    watchCommand (node.getCommand ());
    node.addCommandViewNodeListener (this);
  }

  /**
   * Watch command for property changes if it generates property
   * change events.
   */
  private void watchCommand (Command command)
  {
    if (command instanceof PropertyEventSource)
      ((PropertyEventSource)command).addPropertyChangeListener (this);
  }

  /**
   * Unwatch command for property changes.
   */
  private void unwatchCommand (Command command)
  {
    if (command instanceof PropertyEventSource)
      ((PropertyEventSource)command).removePropertyChangeListener (this);
  }

  private void addCommandMenuButton (CommandViewNode node, int toolBarIndex)
  {
    CommandMenuButton commandMenuButton = new CommandMenuButton (commandView, node);

    toolBar.add(commandMenuButton, toolBarIndex);
    toolBar.validate();

    nodeToToolBarButton.put (node, commandMenuButton);
  }

  private JComponent createToolBarComponent (CommandViewNode commandNode, int toolBarIndex)
  {
    Command command = commandNode.getCommand ();

    if (command instanceof CustomToolBarProvider)
    {
      CustomToolBarProvider toolBarProvider = (CustomToolBarProvider) command;
      JComponent component = toolBarProvider.createToolBarComponent ();

      toolBar.add(component, toolBarIndex);

      nodeToToolBarButton.put(commandNode, component);

      toolBarProvider.addChangeListener (this);

      if (command instanceof PropertyEventSource)
        ((PropertyEventSource)command).addPropertyChangeListener(this);

      return component;
    } else
    {
      AbstractButton button = new ToolButton ();
      button.setFont (toolBar.getFont ());
      button.setBackground (toolBar.getBackground ());

      toolBar.add(button, toolBarIndex);
      nodeToToolBarButton.put(commandNode, button);

      // update display attributes
      updateDisplayName (command, button);
      updateIcon (command, button);
      updateEnabled (command, button);
      updateAccelerator (command, button);

      button.addActionListener(this);

      // store command reference in items client properties
      button.putClientProperty ("commandNode", commandNode);

      if (command instanceof PropertyEventSource)
        ((PropertyEventSource)command).addPropertyChangeListener(this);

      return button;
    }
  }

  private void updateAccelerator (Command command, AbstractButton button)
  {
    if (command.getAccelerator () != null)
      button.getInputMap ().put (command.getAccelerator (),
                                 new CommandActionAdapter (command));
  }

  private void updateDisplayName (Command command, AbstractButton button)
  {
    button.setToolTipText (command.getDescription ());
    button.setBorder (BorderFactory.createEmptyBorder (4,4,4,4));
    button.setFocusPainted (false);

    if (commandView.getCommandProperty
        (command.getName (), CommandToolBars.SHOW_NAME_KEY, false))
    {
      button.setText (CommandRegistry.getDisplayName (command));
    } else
    {
      button.setText (null);
    }
  }

  private void updateIcon (Command command, AbstractButton button)
  {
    Icon icon = command.getIcon ();

    button.putClientProperty (Action.SMALL_ICON, icon);

    if (icon == null)
      button.setText(command.getDisplayName());
    else
      button.setIcon(icon);
  }

  private void updateEnabled (Command command, AbstractButton button)
  {
    button.setEnabled (command.isEnabled ());
  }

  /**
   * Remove a command from the menu.
   *
   * @param node The command node to remove.
   */
  private void removeCommand (CommandViewNode node)
  {
    destroyToolBarComponent (node, node.getCommand ());
    unwatchCommand (node.getCommand ());
  }

  private JComponent destroyToolBarComponent (CommandViewNode node, Command command)
  {
    if (command instanceof CustomToolBarProvider)
    {
      CustomToolBarProvider toolBarProvider = (CustomToolBarProvider) command;

      JComponent component = (JComponent)nodeToToolBarButton.remove (node);

      // remove entry from toolBar
      toolBar.remove (component);

      // complete removal by calling toolbar provider's destroy method and
      // removing change listener
      toolBarProvider.destroyToolBarComponent (component);
      toolBarProvider.removeChangeListener(this);

      return component;
    } else
    {
      AbstractButton button = (AbstractButton)nodeToToolBarButton.get (node);

      toolBar.remove(button);
      nodeToToolBarButton.remove (node);

      button.removeActionListener(this);

      button.putClientProperty ("commandNode", null);

      return button;
    }
  }

  private void removeGroup (CommandViewNode groupNode, int groupIndex)
  {
    JSeparator separator =
      (JSeparator)nodeToToolBarButton.remove (groupNode);

    groupNode.removeCommandViewNodeListener (this);

    toolBar.remove (separator);

    for (int i = 0; i < groupNode.getChildCount (); i++)
    {
      CommandViewNode childNode = groupNode.getChild (i);

      if (childNode.getCommand () != null)
        removeCommand (childNode);
      else
        removeCommandMenuButton (childNode);
    }

    // if group was first node and there is following group, make
    // following group's separator invisible
    CommandViewNode parent = groupNode.getParent ();

    if (groupIndex == 0 && parent.getChildCount () > 0)
    {
      JSeparator firstSeparator =
        (JSeparator)nodeToToolBarButton.get (parent.getChild (0));

      firstSeparator.setVisible (false);
    }
  }

  /**
   * Remove a CommandMenuButton.
   *
   * @param node The node representing a command menu button.
   */
  private void removeCommandMenuButton (CommandViewNode node)
  {
    CommandMenuButton menuButton = (CommandMenuButton)nodeToToolBarButton.remove (node);
    toolBar.remove (menuButton);

    menuButton.dispose ();
  }

  /**
   * Find the logical node (ie same name) associated with a command
   * (must be a child of a group ie second level child of root).
   */
  private CommandViewNode findNodeForCommand (Command command)
  {
    // search groups
    for (int i = 0; i < root.getChildCount (); i++)
    {
      CommandViewNode child = root.getChild (i);
      CommandViewNode node = child.findChild (command.getName ());

      if (node != null)
        return node;
    }
    return null;
  }

  /**
   * Canonically calculate the correct index on the menu for a first
   * level (group) or second level (command or subToolBar) child of
   * root.
   *
   * @param node The node to find index for.
   * @return The toolBar index for node.
   */
  private int findToolBarIndex (CommandViewNode node)
  {
    int index = 0;

    for (int i1 = 0; i1 < root.getChildCount (); i1++)
    {
      CommandViewNode group = root.getChild (i1);

      if (node == group)
        return index;

      // increment index for separator
      index += 1;

      // scan group children
      for (int i2 = 0; i2 < group.getChildCount (); i2++)
      {
        CommandViewNode groupChild = group.getChild (i2);

        if (groupChild == node)
          return index;

        index++;
      }
    }

    return index;
  }

  /**
   * Move a group of buttons to match a group node move.  Assumes
   * group node has already moved.
   *
   * @param groupNode The group node that moved.
   * @param oldIndex The old index of the group node.
   * @param newIndex The new index of the group node.
   */
  private void moveGroup (CommandViewNode groupNode,
                          int oldIndex, int newIndex)
  {
    CommandViewNode parent = groupNode.getParent ();
    JSeparator separator =
      (JSeparator)nodeToToolBarButton.get (groupNode);
    ArrayList groupItems = new ArrayList ();

    groupItems.add (separator);
    toolBar.remove (separator);

    // remove child items and store in groupItems
    for (int i = 0; i < groupNode.getChildCount (); i++)
    {
      CommandViewNode child = groupNode.getChild (i);
      JComponent c = (JComponent)nodeToToolBarButton.get (child);
      groupItems.add (c);
      toolBar.remove (c);
    }

    // separator is visible if not the first group
    separator.setVisible (newIndex > 0);

    int newToolBarIndex = findToolBarIndex (groupNode);

    // insert buttons back into toolBar at new index
    for (int i = 0; i < groupItems.size (); i++)
      toolBar.add((Component)groupItems.get (i), newToolBarIndex + i);

    // if there are other groups, may need to fiddle separators
    if (parent.getChildCount () > 1)
    {
      // if group was first node make old following group's separator
      // invisible
      if (oldIndex == 0)
      {
        JSeparator followingSeparator =
          (JSeparator)nodeToToolBarButton.get (parent.getChild (0));

        followingSeparator.setVisible (false);
      }

      // if group is now first make its following group's separator visible
      if (newIndex == 0)
      {
        JSeparator followingSeparator =
          (JSeparator)nodeToToolBarButton.get (parent.getChild (1));

        followingSeparator.setVisible (true);
      }
    }
  }

  /**
   * Move buttons to match a group child (command or subToolBar)
   * move.  Assumes node has already moved.
   *
   * @param childNode The node that moved.
   */
  private void moveGroupChild (CommandViewNode childNode)
  {
    JComponent c = (JComponent)nodeToToolBarButton.get(childNode);
    // remove component from toolBar
    toolBar.remove(c);

    int newToolBarIndex = findToolBarIndex (childNode);
    // insert component at new index
    toolBar.add(c, newToolBarIndex);
  }

  /**
   * Validate and repaint toolbar if any components are on it, otherwise
   * hide it.
   */
  protected void validateToolbar ()
  {
    if (toolBar.getComponentCount () > 0)
    {
      toolBar.setVisible (true);
      toolBar.repaint ();
      toolBar.revalidate ();
    } else
    {
      toolBar.setVisible (false);
    }
  }

  // PropertyChangeListener Interface.
  public void propertyChange(PropertyChangeEvent e)
  {
    String name = e.getPropertyName ();

    Command command = (Command)e.getSource ();
    CommandViewNode node = findNodeForCommand (command);
    Object component = nodeToToolBarButton.get (node);

    if (component instanceof AbstractButton)
    {
      if (name.equals ("displayName") || name.equals ("interactive"))
        updateDisplayName (command, (AbstractButton)component);
      if (name.equals ("icon"))
        updateIcon (command, (AbstractButton)component);
      if (name.equals ("enabled"))
        updateEnabled (command, (AbstractButton)component);
    }
  }

  // CommandViewNodeListener Interface.

  public void childAdded (CommandViewNodeEvent e)
  {
    CommandViewNode node = e.getNode ();

    // check that a command hasn't popped up outside a group node
    Debug.assertTrue (node.getCommand () == null ||
                      node.getParent () != root,
                      "command '" + node + "' found in group context",
                      this);

    if (node.getParent () == root)
    {
      // If this is a first level child we create a group.
      addGroup (node, findToolBarIndex (node));
    }
    else if (node.getCommand () != null)
    {
      // Just a normal command
      addCommand (node, findToolBarIndex (node));
    }
    else
    {
      // Requires a composite command, because it is a second level group.
      addCommandMenuButton (node, findToolBarIndex (node));
    }

    validateToolbar ();
  }

  public void childRemoved (CommandViewNodeEvent e)
  {
    CommandViewNode node = e.getNode ();

    if (node.getParent () == root)
    {
      // If this is a first level child we destroy the group.
      removeGroup (node, e.getOldIndex ());
    }
    else if (node.getCommand () != null)
    {
      // Remove the command as it is just a normal command.
      removeCommand (node);
    }
    else
    {
      // Must be a command menu button then, therefore we remove it.
      removeCommandMenuButton (node);
    }

    validateToolbar ();
  }

  public void childMoved (CommandViewNodeEvent e)
  {
    CommandViewNode node = e.getNode ();

    if (node.getParent () == root)
      moveGroup (node, e.getOldIndex (), e.getNewIndex ());
    else
      moveGroupChild (e.getNode ());

    toolBar.repaint();
    toolBar.revalidate();
  }

  public void commandChanged(CommandViewNodeEvent e)
  {
    CommandViewNode node = e.getSourceNode ();
    JComponent component = (JComponent)nodeToToolBarButton.get(node);
    int toolBarIndex = toolBar.getComponentIndex (component);

    unwatchCommand (e.getOldCommand ());
    watchCommand (e.getNewCommand ());

    destroyToolBarComponent (node, e.getOldCommand ());
    createToolBarComponent (node, toolBarIndex);

    toolBar.repaint();
    toolBar.revalidate();
  }

  /**
   * Called when a button is pressed.
   */
  @SuppressWarnings("null")
  public void actionPerformed(ActionEvent e)
  {
    // Get the command from the button's client properties and execute it.
    AbstractButton button = (AbstractButton)e.getSource();
    CommandViewNode node =
      (CommandViewNode)button.getClientProperty ("commandNode");
    Command command = node.getCommand ();

    Debug.assertTrue (command != null, "no command for node", this);

    command.execute ();
  }

  /**
   * Called when custom command toolbar provider changes its contents.
   */
  public void stateChanged(ChangeEvent e)
  {
    Command command = (Command)e.getSource ();
    CommandViewNode node = findNodeForCommand (command);

    JComponent component = (JComponent)nodeToToolBarButton.get(node);

    // find index of toolBar
    int toolBarIndex = toolBar.getComponentIndex (component);

    destroyToolBarComponent(node, node.getCommand ());
    createToolBarComponent (node, toolBarIndex);
    toolBar.repaint();
    toolBar.revalidate();
  }

  /**
   * This is the seperator that is used on the toolbar, to show
   * graphically the seperation between different commands.
   */
  private class DfcToolBarSeparator extends JToolBar.Separator
  {
    private static final int SEP_WIDTH = 2;

    public DfcToolBarSeparator()
    {
      super (new Dimension (SEP_WIDTH,16));
      this.setOrientation(toolBar.getOrientation() == VERTICAL ? HORIZONTAL : VERTICAL);
    }

    @Override
    public String getUIClassID()
    {
      return "SeparatorUI";
    }

    @Override
    public Dimension getMaximumSize ()
    {
      if (getOrientation() == VERTICAL)
        return new Dimension (SEP_WIDTH, Integer.MAX_VALUE);
      else
        return new Dimension (Integer.MAX_VALUE, SEP_WIDTH);
    }

    @Override
    public Dimension getPreferredSize ()
    {
      if (getOrientation() == VERTICAL)
        return new Dimension (SEP_WIDTH,16);
      else
        return new Dimension (16,SEP_WIDTH);
    }
  }

  /**
   * This listens to when the orientation of the toolbar changes, and
   * informs all of the seperators to change from VERTICAL to
   * HORIZONTAL or vice-versa.
   */
  class ToolBarOrientationChangeListener implements PropertyChangeListener
  {
    public void propertyChange(PropertyChangeEvent e)
    {
      int count = toolBar.getComponentCount();
      int newOrientation = toolBar.getOrientation() == JSeparator.HORIZONTAL ?
                           JSeparator.VERTICAL : JSeparator.HORIZONTAL;

      for (int i=0; i<count; i++)
      {
        Component c = toolBar.getComponent(i);
        if (c instanceof DfcToolBarSeparator)
          ((JSeparator)c).setOrientation(newOrientation);
      }
    }
  }
}
