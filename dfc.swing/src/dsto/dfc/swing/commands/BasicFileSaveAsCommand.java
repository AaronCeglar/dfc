package dsto.dfc.swing.commands;

import java.awt.Component;
import java.awt.Event;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

/**
 * Base class for File "Save As..." commands.
 *
 * @version $Revision$
 */
public class BasicFileSaveAsCommand extends AbstractMutableCommand
{
  private static final KeyStroke ACCELERATOR =
    KeyStroke.getKeyStroke (KeyEvent.VK_S,
                            Event.SHIFT_MASK |
                              (Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask ()));

  protected Component owner;

  public BasicFileSaveAsCommand ()
  {
    this (null);
  }

  public BasicFileSaveAsCommand (Component owner)
  {
    super ();

    this.owner = owner;
  }

  /**
   * Provides a basic implementation: pops up a file chooser and calls
   * either saveFile () or dialogCancelled ().
   *
   * @see #initFileChooser
   * @see #saveFile
   * @see #dialogCancelled
   */
  public void execute ()
  {
    JFileChooser chooser = new JFileChooser ();
    chooser.setDialogTitle (CommandRegistry.getShortName (this));

    initFileChooser (chooser);

    Component parent = SwingUtilities.windowForComponent (owner);

    if (parent == null)
      parent = owner;

    int result = chooser.showSaveDialog (parent);

    if (result == JFileChooser.APPROVE_OPTION)
      saveFile (chooser);
    else
      dialogCancelled (chooser);
  }

  /**
   * Superclasses may override this to setup the file chooser before display.
   */
  protected void initFileChooser (JFileChooser chooser)
  {
    // zip
  }

  /**
   * Superclasses may override this to save to a file selected via the chooser.
   */
  protected void saveFile (JFileChooser chooser)
  {
    // zip
  }

  /**
   * Superclasses may override this to handle the case where the user cancels
   * the dialog.
   */
  protected void dialogCancelled (JFileChooser chooser)
  {
    // zip
  }

  protected void handleError ()
  {
    handleError (null);
  }

  protected void handleError (Throwable exception)
  {
    String message;

    if (exception != null)
      message = "Error while saving file: " + exception;
    else
      message  = "Error saving file";

    JOptionPane.showMessageDialog (owner, message,
                                   CommandRegistry.getShortName (this),
                                   JOptionPane.ERROR_MESSAGE);
  }

  public String getName ()
  {
    return "file.Save As";
  }

  public String getDescription ()
  {
    return "Save current file";
  }

  public boolean isInteractive ()
  {
    return true;
  }

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return "File.oc";
    else if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return "oc";
    else if (viewName.equals (CommandView.TOOLBAR_VIEW))
      return "oc";
    else
      return null;
  }

  public char getMnemonic ()
  {
    return 0;
  }

  public KeyStroke getAccelerator ()
  {
    return ACCELERATOR;
  }
}
