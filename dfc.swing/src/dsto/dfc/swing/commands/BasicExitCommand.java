package dsto.dfc.swing.commands;


/**
 * Base class for Exit commands.
 *
 * @version $Revision$
 */
public class BasicExitCommand extends AbstractCommand
{
  public BasicExitCommand ()
  {
    super ();
  }

  public void execute ()
  {
    System.exit (0);
  }

  public String getName ()
  {
    return "file.Exit";
  }

  public String getDescription ()
  {
    return "Exit the application";
  }

  public boolean isInteractive ()
  {
    return false;
  }

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return "File.exit";
    else if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return "exit";
    else if (viewName.equals (CommandView.TOOLBAR_VIEW))
      return "exit";
    else
      return null;
  }

  public char getMnemonic ()
  {
    return 'x';
  }
}