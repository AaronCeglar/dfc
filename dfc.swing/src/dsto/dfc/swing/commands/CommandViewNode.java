package dsto.dfc.swing.commands;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * A node used to build the merged command trees used by {@link
 * CommandView}'s.<p>
 *
 * WARNING: complicated attempt to explain the approach follows.  You
 * don't need to understand this to use command views, it's included
 * only to provide convincing evidence of how clever I've been
 * here.<p>
 *
 * Each node has zero or more 'references' (identified by Reference
 * instances stored in the references set).  These references identify
 * what node(s) from which subtrees have been merged to form this node
 * and also define this node's position in its parent.<p>
 *
 * Each reference has a major index (assigned by the CommandView
 * owning the tree) and a minor index (the index of the reference's
 * source node in its parent).  This forms a two-level indexing scheme
 * where 1.1 is 'lower' than 1.2, 1.5 is lower than 2.1, etc (see
 * {@link Reference#compareTo}).  A node's children are always
 * maintained in sorted order defined by their lowest reference within
 * their parent as references are added/removed.<p>
 *
 * Each node also has zero or more Command's associated with it (nodes
 * with no command are typically 'grouping' nodes).  In the case where
 * more than one command is associated with a node (eg in the case
 * where two merged subtrees contain the same logical command such as
 * 'edit.Copy'), this is termed command overloading. Overloading is
 * resolved by defining the top command on the 'commands' stack as the
 * active command.  The active command is automatically updated to be
 * the command associated with the last subnode reference added to
 * this node.  A client may also elect to 'activate' a certain command
 * by causing it to pop to the top of the stack (see {@link
 * #setCommand} and {@link CommandView#activate}).<p>
 *
 * @author Matthew
 * @version $Revision$
 *
 * @see Command
 * @see CommandView
 */
public final class CommandViewNode
{
  /** Unique name (within parent namespace) for this node */
  private String name;
  /** The parent of this node. */
  private CommandViewNode parent;
  /** The children of this node (CommandViewNode instances). */
  private List children = new ArrayList ();
  /** Commands represented by this node: first in list is active */
  private List commands = new ArrayList ();
  /** The subnode references associated with this node (Reference
      instances). */
  private SortedSet references = new TreeSet ();
  private ArrayList commandViewNodeListeners;

  /**
   * Create a root node.
   */
  public CommandViewNode ()
  {
    this (null, "");
  }

  /**
   * Create a node with a given name and no associated command.
   *
   * @param parent The node's parent.  The new node will not be
   * automatically added to the parent's child list (see connectParent
   * ()).
   * @param name The logical name for the node.
   */
  public CommandViewNode (CommandViewNode parent, String name)
  {
    this.name = name;
    this.parent = parent;
  }

  /**
   * Create a node with a given name and an associated command.
   *
   * @param parent The node's parent.  The new node will not be
   * automatically added to the parent's child list (see connectParent()).
   * @param command The initial command for the node
   * (command.getName()) becomes the node's name).
   */
  public CommandViewNode (CommandViewNode parent, Command command)
  {
    this.name = command.getName ();
    this.parent = parent;

    commands.add (command);
  }

  /**
   * Connect this node to its parent (ie add to child list).
   */
  public void connectParent ()
  {
    if (parent != null)
      parent.children.add (this);
  }

  /**
   * The logical name for this node which is unique within parent
   * namespace.
   */
  public String getName ()
  {
    return name;
  }

  /**
   * The currently active command (may be null if no command is
   * associated with this node).
   */
  public Command getCommand ()
  {
    if (commands.size () > 0)
      return (Command)commands.get (0);
    else
      return null;
  }

  /**
   * Set the currently active command.
   */
  public void setCommand (Command newCommand)
  {
    Command oldCommand = getCommand ();

    if (newCommand != oldCommand)
    {
      commands.remove (newCommand);
      commands.add (0, newCommand);

      fireCommandChanged
        (new CommandViewNodeEvent (this, oldCommand, newCommand));
    }
  }

  /**
   * The node's parent.
   */
  public CommandViewNode getParent ()
  {
    return parent;
  }

  /**
   * The node's index in its parent (-1 if not contained within parent
   * or parent is null).
   */
  public int getIndex ()
  {
    if (parent != null)
      return parent.children.indexOf (this);
    else
      return -1;
  }

  /**
   * The number of children of this node.
   */
  public int getChildCount ()
  {
    return children.size ();
  }

  /**
   * Get the child at a given index.
   *
   * @exception IndexOutOfBoundsException if index is not valid (>= 0
   * and < getChildCount ()).
   */
  public CommandViewNode getChild (int index)
    throws IndexOutOfBoundsException
  {
    return (CommandViewNode)children.get (index);
  }

  /**
   * Find a child with a matching logical name.
   *
   * @param childName The logical name to search for.
   * @return The matching child or null if not found.
   */
  public CommandViewNode findChild (String childName)
  {
    for (int i = 0; i < children.size (); i++)
    {
      CommandViewNode child = (CommandViewNode)children.get (i);

      if (child.getName ().equals (childName))
        return child;
    }

    return null;
  }

  /**
   * Find the equivalent node in this tree for a node in another tree.
   * Approach is to follow node to its parent, and then follow the
   * same path back upwards starting from this node.
   *
   * @param node The end node of the path.
   * @return The node, using this node as root, that is equivalent to
   * node (will either be this node or a child).
   */
  public CommandViewNode followNamePath (CommandViewNode node)
  {
    if (node == null)
      return null;              // no match
    else if (node.getParent () == null)
      return this;              // node is root => equivalent to this node
    else
    {
      CommandViewNode parentNode = followNamePath (node.getParent ());

      if (parentNode != null)
        return parentNode.findChild (node.getName ());
      else
        return null;
    }
  }

  /**
   * Find the root for this node.
   */
  public CommandViewNode findRoot ()
  {
    CommandViewNode root = this;

    while (root.getParent () != null)
      root = root.getParent ();

    return root;
  }

  /**
   * Merge a reference subnode as a child of this node.
   *
   * @param refNode The subnode to merge.
   * @param majorIndex The major index of the new reference.
   * @param minorIndex The minor index of the new reference.
   * @return The child node that was created/updated for the subnode.
   * @see #unmergeChild
   */
  public CommandViewNode mergeChild (CommandViewNode refNode,
                                     int majorIndex, int minorIndex)
  {
    // find or create child node with same logical name as refNode
    CommandViewNode childNode = findChild (refNode.getName ());

    if (childNode == null)
      childNode = new CommandViewNode (this, refNode.getName ());

    // add new reference to child
    childNode.addReference (new Reference (refNode, majorIndex, minorIndex));

    return childNode;
  }

  /**
   * Reverse the effect of mergeChild().
   *
   * @param refNode The reference subnode to remove.
   * @return The child node that was updated.
   * @see #mergeChild
   */
  public CommandViewNode unmergeChild (CommandViewNode refNode)
  {
    CommandViewNode childNode = findChild (refNode.getName ());
    Reference reference = childNode.findReference (refNode);

    childNode.removeReference (reference);

    return childNode;
  }

  /**
   * 'Remerge' an already exisiting subnode reference with a new minor
   * index.
   *
   * @param refNode The reference subnode to update.
   * @param newMinorIndex The new minor index for the reference.
   * @return The child node that was updated.
   * @see #mergeChild
   */
  public CommandViewNode remergeMinorIndex (CommandViewNode refNode,
                                            int newMinorIndex)
  {
    CommandViewNode childNode = findChild (refNode.getName ());

    Reference reference = childNode.findReference (refNode);

    // remove reference, change minorIndex, re-add
    childNode.references.remove (reference);
    reference.minorIndex = newMinorIndex;
    childNode.references.add (reference);

    childNode.updatePosition ();

    return childNode;
  }

  /**
   * Used by command view to adjust the major indexes of all subnode
   * references.  This must NOT change the relative ordering of the
   * references.
   *
   * @param startIndex The lower bound (inclusive) of the indexes to
   * change.
   * @param delta The amount to change the indexes by.
   * @see CommandView#addView
   * @see CommandView#removeView
   */
  public void shiftMajorIndexes (int startIndex, int delta)
  {
    for (Iterator i = references.iterator (); i.hasNext (); )
    {
      Reference ref = (Reference)i.next ();

      if (ref.majorIndex >= startIndex)
        ref.majorIndex += delta;
    }
  }

  /**
   * Add a new subnode reference and adjust position/currently active
   * command.
   *
   * @see #updatePosition
   */
  private void addReference (Reference ref)
  {
    // add command (if any)
    Command command = ref.refNode.getCommand ();

    if (command != null)
      addCommand (command);

    // add and update position
    references.add (ref);
    updatePosition ();
  }

  /**
   * Remove subnode reference and adjust position/currently active
   * command.
   *
   * @see #updatePosition
   */
  private void removeReference (Reference ref)
  {
    references.remove (ref);
    updatePosition ();

    Command command = ref.refNode.getCommand ();

    if (command != null)
      removeCommand (command);
  }

  /**
   * Update this node's position in its parent defined by its current
   * set of major/minor indexes.
   *
   * @see #resolveIndex
   */
  private void updatePosition ()
  {
    int oldIndex = getIndex ();

    if (oldIndex != -1)
      parent.children.remove (oldIndex);

    int newIndex = resolveIndex ();

    if (oldIndex == newIndex)
    {
      // nothing happens, put child back
      if (oldIndex != -1)
        parent.children.add (oldIndex, this);
    } else if (oldIndex == -1)
    {
      // child was not previously in parent: add
      parent.children.add (newIndex, this);

      parent.fireChildAdded
        (new CommandViewNodeEvent (parent, this, -1, newIndex));
    } else if (newIndex == -1)
    {
      // child no longer has a position in parent: remove
      parent.fireChildRemoved
        (new CommandViewNodeEvent (parent, this, oldIndex, -1));
    } else
    {
      // child has changed positions in parent: move
      parent.children.add (newIndex, this);

      parent.fireChildMoved
        (new CommandViewNodeEvent (parent, this, oldIndex, newIndex));
    }
  }

  /**
   * Add a command to the top of commands list and fire event if
   * active command has changed.
   */
  private void addCommand (Command newCommand)
  {
    Command oldCommand = getCommand ();

    commands.add (0, newCommand);

    if (oldCommand != newCommand)
      fireCommandChanged (new CommandViewNodeEvent (this, oldCommand, newCommand));
  }

  /**
   * Remove a command from the commands list and fire event if active
   * command has changed.
   */
  private void removeCommand (Command command)
  {
    Command oldCommand = getCommand ();
    commands.remove (command);
    Command newCommand = getCommand ();

    if (oldCommand != newCommand && newCommand != null)
      fireCommandChanged (new CommandViewNodeEvent (this, oldCommand, newCommand));
  }

  /**
   * resolve the index that this node should have due to its set of
   * reference major/minor index pairs.
   *
   * NOTE: the node must not be in the list of children when this is
   * called.
   *
   * @see #getLowestReference
   */
  private int resolveIndex ()
  {
    Reference highest = getLowestReference ();

    if (highest != null)
    {
      // search siblings for this node's correct relative position
      int index = 0;
      for ( ; index < parent.children.size (); index++)
      {
        CommandViewNode sibling = (CommandViewNode)parent.children.get (index);
        Reference siblingHighest = sibling.getLowestReference ();

        // stop if this node's relative index <= current sibling's
        if (highest.compareTo (siblingHighest) <= 0)
          break;
      }

      return index;
    } else
      return -1;
  }

  /**
   * Get the 'lowest' reference in the references set (ie the
   * reference that defines the node's position).
   */
  private Reference getLowestReference ()
  {
    if (references.size () > 0)
      return (Reference)references.first ();
    else
      return null;
  }

  /**
   * Find a reference with matching refNode.
   *
   * @param refNode The node to search for.
   * @return The matching reference or null if not found.
   */
  private Reference findReference (CommandViewNode refNode)
  {
    for (Iterator i = references.iterator (); i.hasNext (); )
    {
      Reference r = (Reference)i.next ();

      if (r.refNode == refNode)
        return r;
    }

    return null;
  }

  public synchronized void removeCommandViewNodeListener
    (CommandViewNodeListener l)
  {
    if (commandViewNodeListeners != null &&
        commandViewNodeListeners.contains (l))
    {
      ArrayList v = (ArrayList) commandViewNodeListeners.clone ();

      v.remove (l);
      commandViewNodeListeners = v;
    }
  }

  public synchronized void addCommandViewNodeListener
    (CommandViewNodeListener l)
  {
    ArrayList v =
      commandViewNodeListeners == null ? new ArrayList (2) :
                                         (ArrayList)commandViewNodeListeners.clone ();
    if (!v.contains (l))
    {
      v.add (l);
      commandViewNodeListeners = v;
    }
  }

  protected void fireChildAdded (CommandViewNodeEvent e)
  {
    if (commandViewNodeListeners != null)
    {
      ArrayList listeners = commandViewNodeListeners;
      int count = listeners.size ();
      for (int i = 0; i < count; i++)
      {
        ((CommandViewNodeListener) listeners.get (i)).childAdded (e);
      }
    }
  }

  protected void fireChildRemoved (CommandViewNodeEvent e)
  {
    if (commandViewNodeListeners != null)
    {
      ArrayList listeners = commandViewNodeListeners;
      int count = listeners.size ();
      for (int i = 0; i < count; i++)
      {
        ((CommandViewNodeListener) listeners.get (i)).childRemoved (e);
      }
    }
  }

  protected void fireChildMoved (CommandViewNodeEvent e)
  {
    if (commandViewNodeListeners != null)
    {
      ArrayList listeners = commandViewNodeListeners;
      int count = listeners.size ();
      for (int i = 0; i < count; i++)
      {
        ((CommandViewNodeListener) listeners.get (i)).childMoved (e);
      }
    }
  }

  protected void fireCommandChanged (CommandViewNodeEvent e)
  {
    if (commandViewNodeListeners != null)
    {
      ArrayList listeners = commandViewNodeListeners;
      int count = listeners.size ();
      for (int i = 0; i < count; i++)
      {
        ((CommandViewNodeListener) listeners.get (i)).commandChanged (e);
      }
    }
  }

  @Override
  public String toString ()
  {
    return name;
  }

  /**
   * Represents a subnode reference to a node.
   */
  private static final class Reference implements Comparable
  {
    public CommandViewNode refNode;
    public int majorIndex;
    public int minorIndex;

    public Reference (CommandViewNode refNode,
                      int majorIndex, int minorIndex)
    {
      this.refNode = refNode;
      this.majorIndex = majorIndex;
      this.minorIndex = minorIndex;
    }

    @Override
    public boolean equals (Object o)
    {
      if (o instanceof Reference)
      {
        Reference r = (Reference)o;

        return refNode == r.refNode &&
               majorIndex == r.majorIndex &&
               minorIndex == r.minorIndex;
      } else
        return false;
    }

    @Override
    public int hashCode ()
    {
      return 17 + refNode.hashCode () + majorIndex + minorIndex;
    }

    public int compareTo (Object o)
    {
      if (o instanceof Reference)
      {
        Reference r = (Reference)o;

        if (majorIndex != r.majorIndex)
          return majorIndex - r.majorIndex;
        else
          return minorIndex - r.minorIndex;
      } else
        return -1;
    }
  }
}
