package dsto.dfc.swing.commands;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import dsto.dfc.swing.icons.Iconic;
import dsto.dfc.util.Debug;
import dsto.dfc.util.PropertyEventSource;

/**
 * Synchronizes a JFC popup menu with a CommandViewNode tree.
 *
 * @see dsto.dfc.swing.commands.CommandMenus
 *
 * @version $Revision$
 */
final class CommandMenuSynchronizer
  implements PropertyChangeListener, CommandViewNodeListener,
             ActionListener, ChangeListener
{
  private CommandView commandView;
  /** The root node for the menu: all 1st level (eg groups) and 2nd
      level (commands and pull-rights) children become menu items. */
  private CommandViewNode root;
  /** The menu we are managing */
  private JPopupMenu menu;
  /** The target component: accelerators are installed here. */
  private Component target;
  /** Maps command tree nodes to their menu item(s) (either a single
    * Component or a List of Component's).*/
  private HashMap nodeToMenuItem = new HashMap (15);

  /**
   * Create a new instance.
   *
   * @param menu The menu to manage.
   * @param root The root of the command tree.
   * @param target The target component for the menu (may be null).
   * Accelerators are in installed/deinstalled on the target as
   * commands are added/removed.
   */
  public CommandMenuSynchronizer (JPopupMenu menu,
                                  CommandView commandView,
                                  CommandViewNode root,
                                  Component target)
  {
    this.commandView = commandView;
    this.menu = menu;
    this.root = root;
    this.target = target;

    addEntries ();

    commandView.addPropertyChangeListener (this);
    root.addCommandViewNodeListener (this);
  }

  /**
   * Remove all listeners and references created for menu
   * synchronisation.
   */
  public void dispose ()
  {
    root.removeCommandViewNodeListener (this);
    commandView.removePropertyChangeListener (this);

    for (int i = 0; i < root.getChildCount (); i++)
    {
      CommandViewNode node = root.getChild (i);

      if (node.getCommand () != null)
        removeCommand (node);
      else
        removeGroup (node, i);
    }
  }

  /**
   * Populate menu with entries from root.
   */
  private void addEntries ()
  {
    int index = 0;

    for (int i = 0; i < root.getChildCount (); i++)
      index += addGroup (root.getChild (i), index);
  }

  /**
   * Add a command to the menu.
   *
   * @param node The node containing the command to add.
   * @param node menuIndex The index where the menu item(s) for the
   * command should be added.
   * @return The number of menu items created.
   * @see #findMenuIndex
   */
  private int addCommand (CommandViewNode node, int menuIndex)
  {
    // create command menu item(s)
    List items = createMenuItems (node, menuIndex);

    // watch command and command's node
    watchCommand (node.getCommand ());
    node.addCommandViewNodeListener (this);

    return items.size ();
  }

  /**
   * Add a group to the menu.
   *
   * @param groupNode The node reprensenting a group (ie an immediate
   * child of root).
   * @param menuIndex The index where the menu item(s) for the group
   * should be added.
   * @return The number of menu items created.
   */
  private int addGroup (CommandViewNode groupNode, int menuIndex)
  {
    int currentMenuIndex = menuIndex;
    int groupIndex = groupNode.getIndex ();

    // create a separator for group and map as group's menu item
    JComponent separator = new JPopupMenu.Separator ();
    nodeToMenuItem.put (groupNode, separator);

    // separator is not visible if this is the first group in the menu
    separator.setVisible (groupIndex > 0);
    menu.insert (separator, currentMenuIndex);
    currentMenuIndex++;

    // watch group node
    groupNode.addCommandViewNodeListener (this);

    // for each group entry (ie each command or pullright)
    for (int i = 0; i < groupNode.getChildCount (); i++)
    {
      CommandViewNode childNode = groupNode.getChild (i);

      // if entry is a command
      if (childNode.getCommand () != null)
      {
        currentMenuIndex += addCommand (childNode, currentMenuIndex);
      } else
      {
        // create pullright menu
        addPullRight (childNode, currentMenuIndex);

        currentMenuIndex++;
      }
    }

    // if group is first in menu and there is a following group, make
    // the following group's separator visible.
    CommandViewNode parent = groupNode.getParent ();
    if (groupIndex == 0 && parent.getChildCount () > 1)
    {
      JComponent followingSeparator =
        (JComponent)nodeToMenuItem.get (parent.getChild (1));

      // followingSeparator may be null if group has not been
      // processed yet
      if (followingSeparator != null)
        followingSeparator.setVisible (true);
    }

    return currentMenuIndex - menuIndex;
  }

  /**
   * Create a pullright menu for a group-within-a-group (ie a child of
   * a group that is not a command node).
   *
   * @param group The pullright node.
   * @param menuIndex The index where the menu item(s) for the group
   * should be added.
   */
  private int addPullRight (CommandViewNode node, int menuIndex)
  {
    // create pullright menu
    JMenu submenu = new JMenu (node.getName ());
    submenu.setIcon (Iconic.NULL_ICON);

    CommandMenuSynchronizer submenuSyncher =
      new CommandMenuSynchronizer (submenu.getPopupMenu (), commandView,
                                   node, target);

    menu.insert (submenu, menuIndex);

    // store menu manager instance as a client property of menu item
    JComponent menuItem = (JComponent)menu.getComponent (menuIndex);
    menuItem.putClientProperty ("pullright_menu", submenuSyncher);

    nodeToMenuItem.put (node, menuItem);

    return 1;
  }

  /**
   * Remove a command from the menu.
   *
   * @param node The command node to remove.
   */
  private void removeCommand (CommandViewNode node)
  {
    destroyMenuItems (node, node.getCommand ());

    unwatchCommand (node.getCommand ());
  }

  /**
   * Remove a group from the menu.
   *
   * @param groupNode The group node to remove.
   * @param groupIndex The previous index of the group in root (in
   * case groupNode has already been removed from its parent).
   */
  private void removeGroup (CommandViewNode groupNode, int groupIndex)
  {
    JComponent separator = (JComponent)nodeToMenuItem.remove (groupNode);

    groupNode.removeCommandViewNodeListener (this);

    menu.remove (separator);

    // for each group entry...
    for (int i = 0; i < groupNode.getChildCount (); i++)
    {
      CommandViewNode childNode = groupNode.getChild (i);

      if (childNode.getCommand () != null)
        removeCommand (childNode);
      else
        removePullRight (childNode);
    }

    // if group was first node and there is following group, make
    // following group's separator invisible
    CommandViewNode parent = groupNode.getParent ();

    if (groupIndex == 0 && parent.getChildCount () > 0)
    {
      JComponent firstSeparator =
        (JComponent)nodeToMenuItem.get (parent.getChild (0));

      firstSeparator.setVisible (false);
    }
  }

  /**
   * Remove a pullright menu item.
   *
   * @param node The node reprensenting a pullright menu.
   */
  private void removePullRight (CommandViewNode node)
  {
    // remove menu item
    JMenuItem menuItem = (JMenuItem)nodeToMenuItem.remove (node);
    menu.remove (menuItem);

    // retrieve menu manager and dispose
    CommandMenuSynchronizer submenuSyncher =
      (CommandMenuSynchronizer)menuItem.getClientProperty ("pullright_menu");

    submenuSyncher.dispose ();
  }

  /**
   * Move a group of menu items to match a group node move.  Assumes
   * group node has already moved.
   *
   * @param groupNode The group node that moved.
   * @param oldIndex The old index of the group node.
   * @param newIndex The new index of the group node.
   */
  private void moveGroup (CommandViewNode groupNode,
                          int oldIndex, int newIndex)
  {
    CommandViewNode parent = groupNode.getParent ();
    JComponent separator = (JComponent)nodeToMenuItem.get (groupNode);
    ArrayList groupItems = new ArrayList ();

    groupItems.add (separator);
    menu.remove (separator);

    // remove child items and store in groupItems
    for (int i = 0; i < groupNode.getChildCount (); i++)
    {
      CommandViewNode child = groupNode.getChild (i);
      List items = getMenuItems (child);

      for (int j = 0; j < items.size (); j++)
      {
        Component c = (Component)items.get (j);
        groupItems.add (c);
        menu.remove (c);
      }
    }

    // separator is visible if not the first group
    separator.setVisible (newIndex > 0);

    int newMenuIndex = findMenuIndex (groupNode);

    // insert menu items back into menu at new index
    for (int i = 0; i < groupItems.size (); i++)
      menu.insert ((Component)groupItems.get (i), newMenuIndex + i);

    // if there are other groups, may need to fiddle separators
    if (parent.getChildCount () > 1)
    {
      // if group was first node make old following group's separator
      // invisible
      if (oldIndex == 0)
      {
        JComponent followingSeparator =
          (JComponent)nodeToMenuItem.get (parent.getChild (0));

        followingSeparator.setVisible (false);
      }

      // if group is now first make its following group's separator visible
      if (newIndex == 0)
      {
        JComponent followingSeparator =
          (JComponent)nodeToMenuItem.get (parent.getChild (1));

        followingSeparator.setVisible (true);
      }
    }
  }

  /**
   * Move menu items to match a group child (command or pullright)
   * move.  Assumes node has already moved.
   *
   * @param childNode The node that moved.
   */
  private void moveGroupChild (CommandViewNode childNode)
  {
    List menuItems = getMenuItems (childNode);

    // remove items from menu
    for (int i = 0; i < menuItems.size (); i++)
    {
      Component c = (Component)menuItems.get (i);
      menu.remove (c);
    }

    int newMenuIndex = findMenuIndex (childNode);

    // insert items at new index
    for (int i = 0; i < menuItems.size (); i++)
    {
      Component c = (Component)menuItems.get (i);
      menu.insert (c, newMenuIndex + i);
    }
  }

  /**
   * Create and register menu item(s) for a command.
   *
   * @param commandNode The command node to create items for.
   * @param menuIndex The index for the new items.
   * @return The items that were created.
   */
  private List createMenuItems (CommandViewNode commandNode, int menuIndex)
  {
    Command command = commandNode.getCommand ();
    List items;

    if (command instanceof CustomMenuProvider)
    {
      // delegate menu item creation to the CustomMenuProvider
      CustomMenuProvider menuProvider = (CustomMenuProvider)command;
      items = menuProvider.createMenuItems ();

      // add items to menu
      for (int i = 0; i < items.size (); i++)
        menu.add ((JComponent)items.get (i), menuIndex + i);

      nodeToMenuItem.put (commandNode, items);

      menuProvider.addChangeListener (this);
    } else
    {
      // no custom provider: create a single JMenuItem for command
      JMenuItem menuItem = new JMenuItem (command.getName ());
      menuItem.putClientProperty
        (Action.SHORT_DESCRIPTION, command.getDescription ());

      menu.add (menuItem, menuIndex);

      nodeToMenuItem.put (commandNode, menuItem);

      // default command is bolded
      if (command.equals (commandView.getDefaultCommand ()))
        menuItem.setFont (menuItem.getFont ().deriveFont (Font.BOLD));

      // update display attributes
      updateDisplayName (command, menuItem);
      updateIcon (command, menuItem);
      updateEnabled (command, menuItem);

      // accelerator
      if (command.getAccelerator () != null)
      {
        menuItem.setAccelerator (command.getAccelerator ());

        if (target instanceof JComponent)
        {
          // install accelerator

          ((JComponent)target).registerKeyboardAction
            (new CommandActionListenerAdapter (command),
             command.getAccelerator (),
             JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        }
      }

      if (command.getMnemonic () != 0)
        menuItem.setMnemonic (command.getMnemonic ());

      menuItem.addActionListener (this);

      // store command reference in items client properties
      menuItem.putClientProperty ("commandNode", commandNode);

      items = new ArrayList (1);
      items.add (menuItem);
    }

    return items;
  }

  /**
   * Destroy the menu items created for a command node.
   *
   * @param node The command node.
   * @param command The command that generated the items.  This is
   * needed because it may not be the same as node.getCommand () in
   * the case that the node's command has changed since
   * createMenuItems () was called.
   * @return The list of menu items that were destroyed.
   */
  private List destroyMenuItems (CommandViewNode node, Command command)
  {
    List items;

    if (command instanceof CustomMenuProvider)
    {
      CustomMenuProvider menuProvider = (CustomMenuProvider)command;
      items = (List)nodeToMenuItem.remove (node);

      // remove entries from menu
      for (int i = 0; i < items.size (); i++)
        menu.remove ((JComponent)items.get (i));

      // complete removal by calling menu provider's destroy method and
      // removing change listener
      menuProvider.destroyMenuItems (items);
      menuProvider.removeChangeListener (this);
    } else
    {
      JComponent menuItem = (JComponent)nodeToMenuItem.get (node);

      menu.remove (menuItem);
      nodeToMenuItem.remove (node);

      // if entry is a command, and has an accelerator registered, unregister
      if (command != null &&
          command.getAccelerator () != null &&
          target instanceof JComponent)
      {
        ((JComponent)target).unregisterKeyboardAction
          (command.getAccelerator ());
      }

      if (menuItem instanceof JMenuItem)
      {
        ((JMenuItem)menuItem).removeActionListener (this);

        // remove command reference from items client properties
        menuItem.putClientProperty ("commandNode", null);
      }

      items = new ArrayList (1);
      items.add (menuItem);
    }

    return items;
  }

  /**
   * Watch command for property changes if it generates property
   * change events.
   */
  private void watchCommand (Command command)
  {
    if (command instanceof PropertyEventSource)
      ((PropertyEventSource)command).addPropertyChangeListener (this);
  }

  /**
   * Unwatch command for property changes.
   */
  private void unwatchCommand (Command command)
  {
    if (command instanceof PropertyEventSource)
      ((PropertyEventSource)command).removePropertyChangeListener (this);
  }

  /**
   * Canonically calculate the correct index on the menu for a first
   * level (group) or second level (command or pullright) child of
   * root.
   *
   * @param node The node to find index for.
   * @return The menu index for node.
   */
  private int findMenuIndex (CommandViewNode node)
  {
    int index = 0;

    // scan groups
    for (int i1 = 0; i1 < root.getChildCount (); i1++)
    {
      CommandViewNode group = root.getChild (i1);

      if (node == group)
        return index;

      // increment index for separator
      index += 1;

      // scan group children
      for (int i2 = 0; i2 < group.getChildCount (); i2++)
      {
        CommandViewNode groupChild = group.getChild (i2);

        if (groupChild == node)
          return index;

        index += getMenuItemCount (groupChild);
      }
    }

    return index;
  }

  /**
   * Get the menu items associated with command node.
   */
  private List getMenuItems (CommandViewNode node)
  {
    Object items = nodeToMenuItem.get (node);

    if (items instanceof List)
      return (List)items;
    else
      return Collections.singletonList (items);
  }

  /**
   * Get the number of menu items associated with command node.
   */
  private int getMenuItemCount (CommandViewNode node)
  {
    Object menuItem = nodeToMenuItem.get (node);

    if (menuItem instanceof List)
      return ((List)menuItem).size ();
    else
      return 1;
  }

  /**
   * Find the logical node (ie same name) associated with a command
   * (must be a child of a group ie second level child of root).
   */
  private CommandViewNode findNodeForCommand (Command command)
  {
    // search groups
    for (int i = 0; i < root.getChildCount (); i++)
    {
      CommandViewNode child = root.getChild (i);

      CommandViewNode node = child.findChild (command.getName ());

      if (node != null)
        return node;
    }

    return null;
  }

  // command/menu item synchronization logic

  private void updateDisplayName (Command command, JMenuItem menuItem)
  {
    menuItem.setText (CommandRegistry.getDisplayName (command));
  }

  private void updateIcon (Command command, JMenuItem menuItem)
  {
    Icon icon = command.getIcon ();

    menuItem.putClientProperty (Action.SMALL_ICON, icon);

    if (icon == null)
      icon = Iconic.NULL_ICON;

    menuItem.setIcon (icon);
  }

  private void updateEnabled (Command command, JMenuItem menuItem)
  {
    menuItem.setEnabled (command.isEnabled ());
  }

  // ChangeListener interface

  /**
   * Called when a custom UI provider command changes and rebuilds
   * custom menu UI.
   */
  public void stateChanged (ChangeEvent e)
  {
    Command command = (Command)e.getSource ();
    CommandViewNode node = findNodeForCommand (command);
    List items = (List)nodeToMenuItem.get (node);
    JComponent firstItem = (JComponent)items.get (0);
    // find index of first menu item
    int menuIndex = menu.getComponentIndex (firstItem);

    // rebuild menu items
    Debug.assertTrue (menuIndex != -1, "custom command menu inconsistent", this);

    destroyMenuItems (node, node.getCommand ());
    createMenuItems (node, menuIndex);
  }

  // CommandViewNodeListener interface

  public void childAdded (CommandViewNodeEvent e)
  {
    CommandViewNode node = e.getNode ();

    // check that a command hasn't popped up outside a group node
    Debug.assertTrue (node.getCommand () == null ||
                      node.getParent () != root,
                      "command '" + node + "' found in group context",
                      this);

    if (node.getParent () == root)
      addGroup (node, findMenuIndex (node));
    else if (node.getCommand () != null)
      addCommand (node, findMenuIndex (node));
    else
      addPullRight (node, findMenuIndex (node));
  }

  public void childRemoved (CommandViewNodeEvent e)
  {
    CommandViewNode node = e.getNode ();

    if (node.getParent () == root)
      removeGroup (node, e.getOldIndex ());
    else if (node.getCommand () != null)
      removeCommand (node);
    else
      removePullRight (node);
  }

  public void childMoved (CommandViewNodeEvent e)
  {
    CommandViewNode node = e.getNode ();

    if (node.getParent () == root)
      moveGroup (node, e.getOldIndex (), e.getNewIndex ());
    else
      moveGroupChild (e.getNode ());
  }

  public void commandChanged (CommandViewNodeEvent e)
  {
    CommandViewNode node = e.getSourceNode ();
    List menuItems = getMenuItems (node);
    int menuIndex = menu.getComponentIndex ((Component)menuItems.get (0));

    unwatchCommand (e.getOldCommand ());
    watchCommand (e.getNewCommand ());

    destroyMenuItems (node, e.getOldCommand ());
    createMenuItems (node, menuIndex);
  }

  // PropertyChangeListener interface

  public void propertyChange (PropertyChangeEvent e)
  {
    String name = e.getPropertyName ();

    if (e.getSource () == commandView)
    {
      if (name.equals ("defaultCommand"))
      {
        // update fonts for changed default command
        Command oldDefault = (Command)e.getOldValue ();
        Command newDefault = (Command)e.getNewValue ();

        // make old default plain
        if (oldDefault != null && !(oldDefault instanceof CustomMenuProvider))
        {
          CommandViewNode node = findNodeForCommand (oldDefault);

          // command may not be visible in view any more (if its
          // command group has changed for example)
          if (node != null)
          {
            JMenuItem item = (JMenuItem)nodeToMenuItem.get (node);

            item.setFont (item.getFont ().deriveFont (Font.PLAIN));
          }
        }

        // make new default bold
        if (newDefault != null && !(newDefault instanceof CustomMenuProvider))
        {
          CommandViewNode node = findNodeForCommand (newDefault);

          // if command is visible in view...
          if (node != null)
          {
            JMenuItem item = (JMenuItem)nodeToMenuItem.get (node);

            item.setFont (item.getFont ().deriveFont (Font.BOLD));
          }
        }
      }
    } else
    {
      Command command = (Command)e.getSource ();
      CommandViewNode node = findNodeForCommand (command);
      Object menuItem = nodeToMenuItem.get (node);

      if (menuItem instanceof JMenuItem)
      {
        if (name.equals ("displayName") || name.equals ("interactive"))
          updateDisplayName (command, (JMenuItem)menuItem);
        if (name.equals ("icon"))
          updateIcon (command, (JMenuItem)menuItem);
        if (name.equals ("enabled"))
          updateEnabled (command, (JMenuItem)menuItem);
      }
    }
  }

  // ActionListener interface

  @SuppressWarnings("null") // we test for null explicitly
  public void actionPerformed (ActionEvent e)
  {
    JMenuItem menuItem = (JMenuItem)e.getSource ();
    CommandViewNode node =
      (CommandViewNode)menuItem.getClientProperty ("commandNode");
    Command command = node.getCommand ();

    Debug.assertTrue (command != null, "no command for node", this);

    command.execute ();
  }
}
