package dsto.dfc.swing.commands;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Action;

import dsto.dfc.util.BasicPropertyEventSource;
import dsto.dfc.util.PropertyEventSource;

/**
 * Adapts a Command instance to look like a JFC Action instance.
 *
 * @version $Revision$
 */
public class CommandActionAdapter
  extends BasicPropertyEventSource implements Action, PropertyChangeListener
{
  protected Command command;

  public CommandActionAdapter (Command command)
  {
    this.command = command;

    if (command instanceof PropertyEventSource)
      ((PropertyEventSource)command).addPropertyChangeListener (this);
  }

  // Action interface

  public void actionPerformed (ActionEvent e)
  {
    if (command.isEnabled ())
      command.execute ();
  }
  
  public Object getValue (String key)
  {
    if (key.equals (Action.NAME))
      return command.getDisplayName ();
    else if (key.equals (Action.SHORT_DESCRIPTION))
      return command.getDescription ();
    else if (key.equals (Action.SMALL_ICON))
      return command.getIcon ();
    else
      return null;
  }

  public void putValue (String key, Object value)
  {
    // zip
  }

  public void setEnabled (boolean b)
  {
    // zip
  }

  public boolean isEnabled ()
  {
    return command.isEnabled ();
  }

  // PropertyChangeListener interface

  public void propertyChange (PropertyChangeEvent e)
  {
    firePropertyChange (e.getPropertyName (),
                        e.getOldValue (), e.getNewValue ());
  }
}
