package dsto.dfc.swing.commands;

import java.beans.PropertyChangeListener;

import javax.swing.Icon;

import dsto.dfc.util.BasicPropertyEventSource;
import dsto.dfc.util.PropertyEventSource;

/**
 * Base class for commands that have changeable properties.  Provides
 * PropertyEventSource implementation and a mutable "enabled"
 * property.
 *
 * @version $Revision$
 */
public abstract class AbstractMutableCommand
  extends AbstractCommand implements PropertyEventSource
{
  private boolean enabled = true;
  protected transient BasicPropertyEventSource listeners =
    new BasicPropertyEventSource (this);

  public AbstractMutableCommand()
  {
    super ();
  }

  public AbstractMutableCommand (String iconName)
  {
    super (iconName);
  }

  public boolean isEnabled ()
  {
    return enabled;
  }

  public void setEnabled (boolean newValue)
  {
    boolean oldValue = enabled;
    enabled = newValue;

    listeners.firePropertyChange ("enabled", oldValue, newValue);
  }
  
  public void setIcon (Icon newIcon)
  {
    Icon oldIcon = icon;
    icon = newIcon;

    listeners.firePropertyChange ("icon", oldIcon, newIcon);
  }


  public void addPropertyChangeListener (PropertyChangeListener l)
  {
    listeners.addPropertyChangeListener (l);
  }

  public void removePropertyChangeListener (PropertyChangeListener l)
  {
    listeners.removePropertyChangeListener (l);
  }

  public Object clone ()
  {
    AbstractMutableCommand copy = (AbstractMutableCommand)super.clone ();

    copy.listeners = new BasicPropertyEventSource (copy);

    return copy;
  }
}
