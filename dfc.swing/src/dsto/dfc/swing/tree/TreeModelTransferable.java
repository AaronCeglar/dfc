package dsto.dfc.swing.tree;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import javax.swing.tree.TreeModel;

public final class TreeModelTransferable
  implements Transferable, ClipboardOwner
{
  public static final DataFlavor TREE_MODEL_FLAVOR =
    new DataFlavor (TreeModel.class, "TreeModel");

  private static final DataFlavor [] FLAVORS =
    new DataFlavor [] {TREE_MODEL_FLAVOR,
                       DataFlavor.stringFlavor};

  TreeModel treeModel;
  
  public TreeModelTransferable (TreeModel treeModel)
  {
    this.treeModel = treeModel;
  }

  public DataFlavor [] getTransferDataFlavors ()
  {
    return FLAVORS;
  }

  public boolean isDataFlavorSupported (DataFlavor flavor)
  {
    for (int i = 0; i < FLAVORS.length; i++)
    {
      if (FLAVORS [i].equals (flavor))
        return true;
    }

    return false;
  }

  public Object getTransferData (DataFlavor flavor)
    throws UnsupportedFlavorException, IOException
  {
    if (flavor.equals (TREE_MODEL_FLAVOR))
      return treeModel;
    else if (flavor.equals (DataFlavor.stringFlavor))
      return "Tree Model";
    else
      throw new UnsupportedFlavorException (flavor);
  }

  public void lostOwnership (Clipboard clipboard, Transferable contents)
  {
    treeModel = null;
  }
}
