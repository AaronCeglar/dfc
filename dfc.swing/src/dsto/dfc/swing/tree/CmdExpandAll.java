package dsto.dfc.swing.tree;

import java.awt.Event;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;
import javax.swing.tree.TreePath;

import dsto.dfc.swing.commands.AbstractCommand;
import dsto.dfc.swing.commands.CommandView;

/**
 * Expand currently selected node and all children.
 *
 * @version $Revision$
 */
public class CmdExpandAll extends AbstractCommand
{
  private static final KeyStroke ACCELERATOR =
    KeyStroke.getKeyStroke (KeyEvent.VK_E, Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask () | Event.SHIFT_MASK);

  protected DfcTree tree;

  public CmdExpandAll (DfcTree tree)
  {
    this.tree = tree;
  }

  public void execute ()
  {
    TreePath [] selected = tree.getSelectionPaths ();

    if (selected != null)
    {
      for (int i = 0; i < selected.length; i++)
        tree.expandAll (selected [i]);
    } else
    {
      tree.expandAll ();
    }
  }

  public String getName ()
  {
    return "tree.Expand All";
  }

  public String getDisplayName ()
  {
    return "Expand";
  }

  public String getDescription ()
  {
    return "Expand the selected entry";
  }

  public boolean isInteractive ()
  {
    return false;
  }

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return "tree";
    else if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return "View.tree";
    else
      return null;
  }

  public char getMnemonic ()
  {
    return 'x';
  }

  public KeyStroke getAccelerator ()
  {
    return ACCELERATOR;
  }
}
