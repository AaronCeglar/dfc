package dsto.dfc.swing.tree;

import java.awt.event.KeyEvent;

import javax.swing.Icon;
import javax.swing.KeyStroke;
import javax.swing.tree.TreePath;

import dsto.dfc.swing.commands.AbstractSelectionBasedCommand;
import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.swing.event.SelectionEvent;

/**
 * Rename the selected node.
 *
 * @version $Revision$
 */
public class CmdRename extends AbstractSelectionBasedCommand
{
  private static final KeyStroke ACCELERATOR =
    KeyStroke.getKeyStroke (KeyEvent.VK_F2, 0);

  protected DfcTree tree;

  public CmdRename (DfcTree tree)
  {
    super (tree);

    this.tree = tree;
  }

  public void execute ()
  {
    TreePath selectedPath = tree.getSelectionPath ();

    if (selectedPath != null)
      tree.startEditingAtPath (selectedPath);
  }

  public String getName ()
  {
    return "edit.Rename";
  }

  public Icon getIcon ()
  {
    return null;
  }

  public String getDescription ()
  {
    return "Rename the selected entry";
  }

  public boolean isInteractive ()
  {
    return false;
  }

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return "change";
    else if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return "Edit.change";
    else
      return null;
  }

  public char getMnemonic ()
  {
    return 'n';
  }

  public KeyStroke getAccelerator ()
  {
    return ACCELERATOR;
  }

  public void selectionChanged (SelectionEvent e)
  {
    updateEnabled ();
  }

  protected void updateEnabled ()
  {
    setEnabled (tree.getSelectionPath () != null);
  }
}
