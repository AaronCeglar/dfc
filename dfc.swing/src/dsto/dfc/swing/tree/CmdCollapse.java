package dsto.dfc.swing.tree;

import java.awt.Event;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import dsto.dfc.swing.commands.AbstractCommand;
import dsto.dfc.swing.commands.CommandView;

/**
 * Collapse currently selected node.
 *
 * @version $Revision$
 */
public class CmdCollapse extends AbstractCommand
{
  private static final KeyStroke ACCELERATOR =
    KeyStroke.getKeyStroke (KeyEvent.VK_C,
                            Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask () | Event.SHIFT_MASK);

  protected DfcTree tree;

  public CmdCollapse (DfcTree tree)
  {
    this.tree = tree;
  }

  public void execute ()
  {
    TreePath [] selected = tree.getSelectionPaths ();

    if (selected != null)
    {
      for (int i = 0; i < selected.length; i++)
        tree.collapsePath (selected [i]);
    } else
    {
      TreeModel model = tree.getModel ();
      Object root = model.getRoot ();
      Object [] path = new Object [2];
      path [0] = root;

      for (int i = model.getChildCount (root) - 1; i >= 0; i--)
      {
        path [1] = model.getChild (root, i);

        tree.collapsePath (new TreePath (path));
      }
    }
  }

  public String getName ()
  {
    return "tree.Collapse";
  }

  public String getDescription ()
  {
    return "Collapse the selected entry";
  }

  public boolean isInteractive ()
  {
    return false;
  }

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return "tree";
    else if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return "View.tree";
    else
      return null;
  }

  public char getMnemonic ()
  {
    return 'x';
  }

  public KeyStroke getAccelerator ()
  {
    return ACCELERATOR;
  }
}
