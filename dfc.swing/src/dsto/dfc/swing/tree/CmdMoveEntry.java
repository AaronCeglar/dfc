package dsto.dfc.swing.tree;

import javax.swing.tree.TreePath;

import dsto.dfc.swing.commands.AbstractMoveCommand;

/**
 * A command that moves tree entries up/down within their parent.
 *
 * @version $Revision$
 */
public class CmdMoveEntry extends AbstractMoveCommand
{
  private DfcTree tree;

  /**
   * Create a new instance.
   *
   * @param tree The tree to operate on.
   * @param direction The direction to move the entries (MOVE_UP or
   * MOVE_DOWN).
   */
  public CmdMoveEntry (DfcTree tree, boolean direction)
  {
    super (tree, direction);

    this.tree = tree;

    updateEnabled ();
  }

  public void execute ()
  {
    if (tree.getSelectionCount () == 1 &&
        tree.getModel () instanceof DfcTreeModel)
    {
      TreePath selectPath = tree.getSelectionPath ();
      DfcTreeModel model = (DfcTreeModel)tree.getModel ();
      Object selected = selectPath.getLastPathComponent ();
      Object parent = model.getParent (selected);

      if (parent != null)
      {
        int currentIndex = model.getIndexOfChild (parent, selected);
        int newIndex = getNextIndex (model, parent, currentIndex);

        model.moveEntry (selected, parent, newIndex);

        // select moved entry
        TreePath newPath = new TreePath (model.getPathForEntry (selected));
        tree.setSelectionPath (newPath);
      }
    }
  }

  protected void updateEnabled ()
  {
    boolean enabled = false;
    TreePath selectPath = tree.getSelectionPath ();

    if (selectPath != null && tree.getModel () instanceof DfcTreeModel)
    {
      DfcTreeModel model = (DfcTreeModel)tree.getModel ();
      Object selected = selectPath.getLastPathComponent ();
      Object parent = model.getParent (selected);

      if (parent != null)
      {
        int currentIndex = model.getIndexOfChild (parent, selected);
        int newIndex = getNextIndex (model, parent, currentIndex);

        enabled = newIndex != -1 &&
                  model.canMoveEntry (selected, parent, newIndex);
      }
    }

    setEnabled (enabled);
  }

  protected int getNextIndex (DfcTreeModel model, Object parent, int index)
  {
    int newIndex = -1;

    if (direction == MOVE_UP)
    {
      if (index > 0)
        newIndex = index - 1;
    } else
    {
      if (index < model.getChildCount (parent) - 1)
        newIndex = index + 1;
    }

    return newIndex;
  }
}
