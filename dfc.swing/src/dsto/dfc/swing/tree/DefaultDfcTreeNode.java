package dsto.dfc.swing.tree;

import javax.swing.tree.DefaultMutableTreeNode;

import dsto.dfc.util.Copyable;
import dsto.dfc.util.Disposable;

/**
 * Default impelementation of DfcTreeNode.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class DefaultDfcTreeNode
  extends DefaultMutableTreeNode
  implements DfcTreeNode, Disposable, Copyable
{
  public DefaultDfcTreeNode ()
  {
    super ();
  }

  public DefaultDfcTreeNode (Object userObject)
  {
    super (userObject);
  }

  public void dispose ()
  {
    if (userObject instanceof Disposable)
    {
      ((Disposable)userObject).dispose ();

      if (children != null)
      {
        for (int i = 0; i < children.size (); i++)
        {
          Object child = children.elementAt (i);

          if (child instanceof Disposable)
            ((Disposable)child).dispose ();
        }
      }
    }

    userObject = null;
    children = null;
  }

  public boolean isMutable ()
  {
    return true;
  }

  public Object getUserObject ()
  {
    return userObject;
  }

  public DfcTreeNode findChild (Object childUserObject)
  {
    for (int i = 0; i < getChildCount (); i++)
    {
      DfcTreeNode node = (DfcTreeNode)getChildAt (i);

      if (node.getUserObject ().equals (childUserObject))
        return node;
    }

    return null;
  }
}
