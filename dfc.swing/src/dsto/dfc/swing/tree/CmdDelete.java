package dsto.dfc.swing.tree;

import java.util.List;

import dsto.dfc.logging.Log;
import dsto.dfc.swing.commands.BasicDeleteCommand;
import dsto.dfc.swing.event.SelectionEvent;
import dsto.dfc.util.Disposable;

/**
 * Delete currently selected node and all children.
 *
 * @version $Revision$
 */
public class CmdDelete extends BasicDeleteCommand
{
  private DfcTree tree;

  public CmdDelete (DfcTree tree)
  {
    super (tree);

    this.tree = tree;
  }

  public void execute ()
  {
    if (tree.getSelectionCount () > 0 &&
        tree.getModel () instanceof DfcTreeModel)
    {
      try
      {
        List entries = tree.getSelectedEntries ();
        int [] rows = tree.deleteRows (tree.getSelectionRows ());

        // dispose any disposable entries
        for (int i = 0; i < entries.size (); i++)
        {
          Object entry = entries.get (i);

          if (entry instanceof Disposable)
            ((Disposable)entry).dispose ();
        }

        // select previous row
        int row = rows [0];

        if (row > 0)
          tree.setSelectionRow (row - 1);

      } catch (UnsupportedOperationException ex)
      {
        Log.warn ("tree model vetoed delete operation", this, ex);
      }
    }
  }

  public String getDescription ()
  {
    return "Delete the selected entry and its children";
  }

  public void selectionChanged (SelectionEvent e)
  {
    updateEnabled ();
  }

  protected void updateEnabled ()
  {
    Object selected = tree.getLastSelectedPathComponent ();

    if (selected != null && tree.getModel () instanceof DfcTreeModel)
      setEnabled (((DfcTreeModel)tree.getModel ()).canRemoveEntry (selected));
    else
      setEnabled (false);
  }
}