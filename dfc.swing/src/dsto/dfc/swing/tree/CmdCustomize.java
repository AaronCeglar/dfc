package dsto.dfc.swing.tree;

import dsto.dfc.swing.event.SelectionEvent;
import dsto.dfc.swing.event.SelectionListener;
import dsto.dfc.swing.forms.AbstractShowCustomizerCommand;

/**
 * Base implementation of the properties command that attempts to show
 * the selected tree entry's FormEditor-compliant customizer.
 *
 * @author Matthew
 * @version $Revision$
 */
public class CmdCustomize
  extends AbstractShowCustomizerCommand implements SelectionListener
{
  private DfcTree tree;

  public CmdCustomize (DfcTree tree)
  {
    super (tree);

    this.tree = tree;

    tree.addSelectionListener (this);
  }

  public boolean isDirectEdit ()
  {
    return true;
  }

  public String getDescription ()
  {
    return "Show the properties of the selected item";
  }

  public Object getCustomizerValue ()
  {
    if (tree.getSelectionCount () == 1)
      return tree.getSelectionPath ().getLastPathComponent ();
    else
      return null;
  }

  public void setCustomizerValue (Object newValue)
  {
    // zip: value is edited directly
  }

  public void selectionChanged (SelectionEvent e)
  {
    customizerValueChanged ();
  }
}
