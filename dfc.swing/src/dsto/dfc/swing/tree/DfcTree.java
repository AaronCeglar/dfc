package dsto.dfc.swing.tree;

import java.awt.Component;
import java.awt.Point;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EventObject;
import java.util.List;

import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeCellEditor;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import dsto.dfc.logging.Log;
import dsto.dfc.swing.commands.Command;
import dsto.dfc.swing.commands.CommandMenus;
import dsto.dfc.swing.commands.CommandSource;
import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.swing.dnd.ClipboardEvent;
import dsto.dfc.swing.dnd.ClipboardListener;
import dsto.dfc.swing.dnd.ClipboardManager;
import dsto.dfc.swing.dnd.CmdCopy;
import dsto.dfc.swing.dnd.CmdCut;
import dsto.dfc.swing.dnd.CmdPaste;
import dsto.dfc.swing.dnd.CnpProvider;
import dsto.dfc.swing.dnd.DfcMouseDragGestureRecognizer;
import dsto.dfc.swing.dnd.DragDropComponent;
import dsto.dfc.swing.dnd.DragSourceAdapter;
import dsto.dfc.swing.dnd.DropTargetAdapter;
import dsto.dfc.swing.dnd.MissingDataTransferable;
import dsto.dfc.swing.event.BasicSelectionEventSource;
import dsto.dfc.swing.event.SelectionEventSource;
import dsto.dfc.swing.event.SelectionListener;
import dsto.dfc.swing.icons.Iconic;
import dsto.dfc.swing.icons.IconicFolder;
import dsto.dfc.swing.text.selection.PatternSelector;
import dsto.dfc.swing.text.selection.Selector;

import dsto.dfc.util.Disposable;
import dsto.dfc.util.PropertyEventSource;
import dsto.dfc.util.WeakEventListener;

/**
 * DFC'ified JTree, adding support for operations based on the
 * extended DfcTreeModel.  Includes support for:<br>
 *
 * <ul>
 *   <li>copy and paste (for DfcTreeModel only)
 *   <li>drag and drop (for DfcTreeModel only)
 *   <li>{@link Iconic} and
 *       {@link IconicFolder} item display.
 *   <li>{@link SelectionEventSource} support.
 * </ul>
 *
 * @author Matthew
 * @version $Revision$
 *
 * @see DfcTreeModel
 */
public class DfcTree extends JTree
  implements CommandSource, SelectionEventSource,
             CnpProvider, DragDropComponent,
             PropertyEventSource, Disposable
{
  private boolean cnpCommandsEnabled = false;
  private CommandView contextMenuView = new CommandView (CommandView.CONTEXT_MENU_VIEW);
  private CommandView mainMenuView = new CommandView (CommandView.MAIN_MENU_VIEW);
  private CommandView toolbarView = new CommandView (CommandView.TOOLBAR_VIEW);
  private WeakEventListener clipboardListener;
  private transient MyListener myListener;
  private transient BasicSelectionEventSource selectionListeners;
  /** True if drag and drop has been enabled (see {@link
      #enableDragAndDrop}) */
  private boolean dndEnabled = false;
  /** The entry that is currently being dragged in a D&D operation
      (null in none in progress). */
  private Object draggedEntry = null;
  private transient boolean cnpCopyEnabled = false;
  private transient boolean cnpCutEnabled = false;
  private transient boolean cnpPasteEnabled = true;

  public DfcTree ()
  {
    this (new DefaultDfcTreeModel ());
  }

  public DfcTree (TreeModel model)
  {
    super (model);

    // setup renderer and editor
    MyTreeCellRenderer renderer = new MyTreeCellRenderer ();
    setCellRenderer (renderer);
    setCellEditor (new MyTreeCellEditor (renderer));

    // make tree display connection lines between parents and children
    putClientProperty ("JTree.lineStyle", "Angled");

    // install selection listener
    myListener = new MyListener ();
    getSelectionModel ().addTreeSelectionListener (myListener);

    // install clipboard listener (weak so that we can still be GC'd)
    clipboardListener = WeakEventListener.createListener
      (ClipboardListener.class, ClipboardManager.class, myListener);

    initCommands ();

    CommandMenus.installContextMenu (this, contextMenuView);
  }

  private void initCommands ()
  {
    CmdExpandAll cmdExpandAll = new CmdExpandAll (this);
    CmdCollapse cmdCollapse = new CmdCollapse (this);

    // context menu
    contextMenuView.addCommand (cmdExpandAll);
    contextMenuView.addCommand (cmdCollapse);
  }

  public void dispose ()
  {
    TreeModel model = getModel ();

    if (model instanceof Disposable)
    {
      setModel (null);

      ((Disposable)model).dispose ();
    }
    
    if (clipboardListener != null)
    {
      clipboardListener.dispose ();
      
      clipboardListener = null;
    }
  }

  public void enableDragAndDrop ()
  {
    if (!dndEnabled)
    {
      // NOTE: using DfcMouseDragGestureRecognizer instead of system
      // default to work around bug (see DfcMouseDragGestureRecognizer
      // header comment for full description).
      DfcMouseDragGestureRecognizer recognizer =
        new DfcMouseDragGestureRecognizer (this,
                                           DnDConstants.ACTION_COPY_OR_MOVE);
      new DragSourceAdapter (this, recognizer);

      new DropTargetAdapter (this);

      dndEnabled = true;
    }
  }

  public void enableEditCommands ()
  {
    if (contextMenuView.findCommand ("edit.Delete") == null)
    {
      CmdDelete deleteCommand = new CmdDelete (this);
      CmdRename renameCommand = new CmdRename (this);

      contextMenuView.addCommand (deleteCommand);
      contextMenuView.addCommand (renameCommand);

      mainMenuView.addCommand (deleteCommand);
      mainMenuView.addCommand (renameCommand);
    }
  }

  public void disableEditCommands ()
  {
    CmdDelete deleteCommand =
      (CmdDelete)contextMenuView.removeCommand ("edit.Delete");
    CmdRename renameCommand =
      (CmdRename)contextMenuView.removeCommand ("edit.Rename");

    mainMenuView.removeCommand (deleteCommand);
    mainMenuView.removeCommand (renameCommand);

    deleteCommand.dispose ();
    renameCommand.dispose ();
  }

  public void setCnpCommandsEnabled (boolean newValue)
  {
    if (cnpCommandsEnabled != newValue)
    {
      cnpCommandsEnabled = newValue;

      if (cnpCommandsEnabled)
      {
        Command cutCommand = new CmdCut (this);
        Command copyCommand = new CmdCopy (this);
        Command pasteCommand = new CmdPaste (this);

        contextMenuView.addCommand (cutCommand, 0);
        contextMenuView.addCommand (copyCommand, 1);
        contextMenuView.addCommand (pasteCommand, 2);

        mainMenuView.addCommand (cutCommand, 0);
        mainMenuView.addCommand (copyCommand, 1);
        mainMenuView.addCommand (pasteCommand, 2);

        toolbarView.addCommand (cutCommand, 0);
        toolbarView.addCommand (copyCommand, 1);
        toolbarView.addCommand (pasteCommand, 2);
      } else
      {
        contextMenuView.removeCommand ("edit.Cut");
        contextMenuView.removeCommand ("edit.Copy");
        contextMenuView.removeCommand ("edit.Paste");

        mainMenuView.removeCommand ("edit.Cut");
        mainMenuView.removeCommand ("edit.Copy");
        mainMenuView.removeCommand ("edit.Paste");

        toolbarView.removeCommand ("edit.Cut");
        toolbarView.removeCommand ("edit.Copy");
        toolbarView.removeCommand ("edit.Paste");
      }

      firePropertyChange ("cnpCommandsEnabled",
                          !cnpCommandsEnabled, cnpCommandsEnabled);
    }
  }

  public boolean getCnpCommandsEnabled ()
  {
    return cnpCommandsEnabled;
  }

  public void setEditable (boolean flag)
  {
    boolean oldFlag = isEditable ();

    super.setEditable (flag);

    if (oldFlag != flag)
    {
      if (flag)
        enableEditCommands ();
      else
        disableEditCommands ();
    }
  }

  public void selectEntry (Object entry)
  {
    setSelectionPath
      (new TreePath (getPathForEntry (getModel (), entry)));
  }

  /**
   * If the current model is a DfcTreeModel return it, otherwise return null.
   */
  public DfcTreeModel getDfcTreeModel ()
  {
    if (getModel () instanceof DfcTreeModel)
      return (DfcTreeModel)getModel ();
    else
      return null;
  }

  public void setSelectionModel (TreeSelectionModel newSelModel)
  {
    TreeSelectionModel oldModel = getSelectionModel ();

    if (oldModel != null)
      oldModel.removeTreeSelectionListener (myListener);

    newSelModel.addTreeSelectionListener (myListener);

    super.setSelectionModel (newSelModel);
  }

  public void expandChildren (TreePath parentPath, Object [] children)
  {
    Object [] childPath = new Object [parentPath.getPathCount () + 1];
    System.arraycopy (parentPath.getPath (), 0,
                      childPath, 0, parentPath.getPathCount ());

    for (int i = 0; i < children.length; i++)
    {
      childPath [parentPath.getPathCount ()] = children [i];

      expandAll (new TreePath (childPath));
    }
  }

  /**
   * Expand all nodes in the tree.
   */
    public void expandAll ()
    {
      expandAll (Integer.MAX_VALUE);
    }

  /**
   * Expand all nodes in the tree up to a given level (eg level == 1
   * would expand only the root node).
   */
  public void expandAll (int maxLevel)
  {
    expandAll (new TreePath (getModel ().getRoot ()), maxLevel);
  }

  /**
   * Expand all nodes in the tree starting from a given entry.
   */
  public void expandAll (TreePath path)
  {
    expandAll (path, Integer.MAX_VALUE);
  }

  /**
   * Expand all nodes in the tree from a given path up to a given
   * level (eg level == 1 would expand only the entry itself, level ==
   * 2 would expand the entry and its children etc).
   */
  public void expandAll (TreePath path, int maxLevel)
  {
    expandAll (path, maxLevel, PatternSelector.SELECT_ALL);
  }

  /**
   * Expand all nodes in the tree from a given path up to a given
   * level.  Expansion can be further controlled by a selector.
   *
   * @param path The path to start expanding from.
   * @param maxLevel The number of levels to expand (eg level == 1
   * would expand only the entry itself, level == 2 would expand the
   * entry and its children etc).
   * @param selector Only paths that are matched by this selector will
   * be expanded.  The matches () method of the selector is passed a
   * TreePath instance.
   */
  public void expandAll (final TreePath path, final int maxLevel,
                         final Selector selector)
  {
    if (SwingUtilities.isEventDispatchThread ())
    {
      validate ();
      doExpandAll (path, maxLevel, selector);
    } else
    {
      // need to schedule expand for execution in Swing thread
      SwingUtilities.invokeLater (new Runnable ()
      {
        public void run ()
        {
          validate ();
          doExpandAll (path, maxLevel, selector);
        }
      });
    }
  }

  protected void doExpandAll (TreePath path, int levelCount, Selector selector)
  {
    if (levelCount == 0 || !selector.matches (path))
      return;

    levelCount--;

    Object [] newPath = new Object [path.getPathCount () + 1];
    System.arraycopy (path.getPath (), 0, newPath, 0, path.getPathCount ());

    expandPath (path);

    Object entry = path.getLastPathComponent ();

    for (int i = getModel ().getChildCount (entry) - 1; i >= 0; i--)
    {
      newPath [newPath.length - 1] = getModel ().getChild (entry, i);
      doExpandAll (new TreePath (newPath), levelCount, selector);
    }
  }

  /**
   * Delete a number of rows from the model, ensuring correct deletion
   * order.
   *
   * @param rows The rows to delete.
   * @return The rows in the reverse order to which they were deleted.
   * @exception UnsupportedOperationException if the current model is
   * not a DfcTreeModel or if the model vetoed a delete operation.
   */
  public int [] deleteRows (int [] rows)
    throws UnsupportedOperationException
  {
    try
    {
      DfcTreeModel model = (DfcTreeModel)getModel ();

      // make a copy of the rows in sorted in ascending order
      rows = rows.clone ();
      Arrays.sort (rows);

      for (int i = rows.length - 1; i >= 0; i--)
        model.removeEntry (getPathForRow (rows [i]).getLastPathComponent ());

    } catch (ClassCastException ex)
    {
      throw new UnsupportedOperationException
        ("either non-DfcTreeModel or model vetoed delete");
    }

    return rows;
  }

  public Object getSelectedEntry ()
  {
    if (getSelectionCount () == 0)
      return null;
    else
      return getSelectionPath ().getLastPathComponent ();
  }

  public void setSelectedEntry (Object entry)
  {
    TreePath path = new TreePath (getPathForEntry (this.getModel (), entry));

    setSelectionPath (path);
  }

  public void addSelectedEntry (Object entry)
  {
    TreePath path = new TreePath (getPathForEntry (this.getModel (), entry));

    addSelectionPath (path);
  }

  public List getSelectedEntries ()
  {
    if (getSelectionCount () == 0)
      return Collections.EMPTY_LIST;
    else
    {
      ArrayList selection = new ArrayList (getSelectionCount ());
      TreePath [] paths = getSelectionPaths ();

      for (int i = 0; i < paths.length; i++)
        selection.add (paths [i].getLastPathComponent ());

      return selection;
    }
  }

  public static Object [] getPathForEntry (TreeModel model, Object entry)
  {
    return Trees.getPathForEntry (model, entry);
  }

  // SelectionEventSource interface

  public void addSelectionListener (SelectionListener l)
  {
    if (selectionListeners == null)
      selectionListeners = new BasicSelectionEventSource (this);

    selectionListeners.addSelectionListener (l);
  }

  public void removeSelectionListener (SelectionListener l)
  {
    if (selectionListeners != null)
      selectionListeners.removeSelectionListener (l);
  }

  // CommandSource interface

  public CommandView getCommandView (String viewName)
  {
    if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return contextMenuView;
    else if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return mainMenuView;
    else if (viewName.equals (CommandView.TOOLBAR_VIEW))
      return toolbarView;
    else
      return null;
  }

  // CnpProvider interface

  public boolean isCnpCopyEnabled ()
  {
    DfcTreeModel model = getDfcTreeModel ();
    Object selected = getLastSelectedPathComponent ();

    if (model != null && model.isMutable () && selected != null)
      return model.canCopyEntry (selected);
    else
      return false;
  }

  public boolean isCnpCutEnabled ()
  {
    DfcTreeModel model = getDfcTreeModel ();
    Object selected = getLastSelectedPathComponent ();

    if (selected != null && model != null && model.isMutable ())
    {
      return model.canRemoveEntry (selected) &&
             model.canCopyEntry (selected);
    } else
    {
      return false;
    }
  }

  public boolean isCnpPasteEnabled (Transferable transferable)
  {
    DfcTreeModel model = getDfcTreeModel ();

    if (model != null && model.isMutable ())
    {
      Object parent = getLastSelectedPathComponent ();

      if (parent == null)
        parent = model.getRoot ();

      int index = model.getChildCount (parent);

      return transferable != null &&
             model.canPasteEntry (parent, index, transferable);
    } else
    {
      return false;
    }
  }

  public Transferable cnpCopy ()
    throws UnsupportedOperationException, CloneNotSupportedException
  {
    DfcTreeModel model = getDfcTreeModel ();
    Object selected = getLastSelectedPathComponent ();

    if (selected != null && model != null)
    {
      return model.copyEntry (selected);
    } else
    {
      throw new UnsupportedOperationException ();
    }
  }

  public Transferable cnpCut ()
    throws UnsupportedOperationException, CloneNotSupportedException
  {
    DfcTreeModel model = getDfcTreeModel ();
    Object selected = getLastSelectedPathComponent ();

    if (selected != null && model != null && model.isMutable ())
    {
      Transferable transferable = model.copyEntry (selected);
      model.removeEntry (selected);

      return transferable;
    } else
    {
      throw new UnsupportedOperationException ();
    }
  }

  public boolean cnpPaste (Transferable transferable)
    throws UnsupportedOperationException,
           UnsupportedFlavorException,
           CloneNotSupportedException,
           IOException
  {
    DfcTreeModel model = getDfcTreeModel ();

    if (model != null && model.isMutable ())
    {
      Object parent = getLastSelectedPathComponent ();

      if (parent == null)
        parent = model.getRoot ();

      int index = model.getChildCount (parent);

      model.pasteEntry (parent, index, transferable);

      // expand and select new pasted subtree

      Object child = model.getChild (parent, index);
      TreePath pastedPath = getSelectionPath ().pathByAddingChild (child);

      expandAll (pastedPath);
      setSelectionPath (pastedPath);

      return true;
    }

    throw new UnsupportedOperationException ();
  }

  protected final void updateCnpEnabled ()
  {
    boolean copyEnabled = isCnpCopyEnabled ();
    boolean cutEnabled = isCnpCutEnabled ();
    boolean pasteEnabled = isCnpPasteEnabled (ClipboardManager.getClipboardContents ());

    if (copyEnabled != cnpCopyEnabled)
    {
      cnpCopyEnabled = copyEnabled;
      firePropertyChange ("cnpCopyEnabled", !cnpCopyEnabled, cnpCopyEnabled);
    }

    if (cutEnabled != cnpCutEnabled)
    {
      cnpCutEnabled = cutEnabled;
      firePropertyChange ("cnpCutEnabled", !cnpCutEnabled, cnpCutEnabled);
    }

    if (pasteEnabled != cnpPasteEnabled)
    {
      cnpPasteEnabled = pasteEnabled;
      firePropertyChange ("cnpPasteEnabled", !cnpPasteEnabled, cnpPasteEnabled);
    }
  }

  // DragDropComponent interface

  public int getSupportedDragActions ()
  {
    if (isEditable())
      return DnDConstants.ACTION_COPY_OR_MOVE;
    else
      return DnDConstants.ACTION_COPY;
  }

  public int getSupportedDropActions ()
  {
    if (isEditable())
      return DnDConstants.ACTION_COPY_OR_MOVE;
    else
      return DnDConstants.ACTION_NONE;

  }

  public boolean isDragOK (DragGestureEvent e)
  {
    Point dragStart = e.getDragOrigin ();
    DfcTreeModel model = getDfcTreeModel ();
    TreePath draggedPath = getPathForLocation (dragStart.x, dragStart.y);

    if (model != null && draggedPath != null)
    {
      Object entry = draggedPath.getLastPathComponent ();

      boolean canCopy = model.canCopyEntry (entry);
      boolean canDelete = model.canRemoveEntry (entry);

      if ((e.getDragAction () & DnDConstants.ACTION_MOVE) != 0)
        return model.isMutable () && canCopy && canDelete;
      else if ((e.getDragAction () & DnDConstants.ACTION_COPY) != 0)
        return canCopy;
    }

    return false;
  }

  public Transferable startDrag (DragGestureEvent e)
  {
    Point dragStart = e.getDragOrigin ();
    Transferable transferable = null;
    DfcTreeModel model = getDfcTreeModel ();
    TreePath draggedPath = getPathForLocation (dragStart.x, dragStart.y);

    if (model != null && draggedPath != null)
    {
      draggedEntry = draggedPath.getLastPathComponent ();

      try
      {
        if (model.canCopyEntry (draggedEntry))
          transferable = model.copyEntry (draggedEntry);
      } catch (CloneNotSupportedException ex)
      {
        Log.diagnostic ("Failed clone during start drag", this, ex);
      }
    }

    return transferable;
  }

  public void endDrag (DragSourceDropEvent e, boolean moveData)
  {
    DfcTreeModel model = getDfcTreeModel ();

    // delete entry if requested to move data
    if (moveData && model != null && draggedEntry != null)
      model.removeEntry (draggedEntry);

    draggedEntry = null;
  }

  public boolean isDropOK (DropTargetDragEvent e)
  {
    Point dropPoint = e.getLocation ();
    DfcTreeModel model = getDfcTreeModel ();
    TreePath dropPath = getPathForLocation (dropPoint.x, dropPoint.y);

    if (model != null && dropPath != null && model.isMutable ())
    {
      boolean isADescendent = false; // default (copy)
      if (this.getSelectionPath () != null &&
          (e.getDropAction () == DnDConstants.ACTION_MOVE ||  // test for move
           e.getDropAction () == DnDConstants.ACTION_NONE))    // or no modifiers
        isADescendent = this.getSelectionPath ().isDescendant (dropPath);

      Object dropParent = dropPath.getLastPathComponent ();
      int index = model.getChildCount (dropParent);

      // use a bogus MissingDataTransferable since this occurs before
      // the data has actually been copied.
      Transferable transferable =
        new MissingDataTransferable (e.getCurrentDataFlavors ());

      return !isADescendent &&
              model.canPasteEntry (dropParent, index, transferable);
    }

    return false;
  }

  public void executeDrop (DropTargetDropEvent e)
  {
    Point dropPoint = e.getLocation ();
    DfcTreeModel model = getDfcTreeModel ();
    TreePath dropPath = getPathForLocation (dropPoint.x, dropPoint.y);

    if (model != null && dropPath != null)
    {
      Object dropParent = dropPath.getLastPathComponent ();
      int index = model.getChildCount (dropParent);

      try
      {
        if (model.canPasteEntry (dropParent, index, e.getTransferable ()))
        {
          model.pasteEntry (dropParent, index, e.getTransferable ());

          Object pastedEntry = // find the pasted entry
            model.getChild(dropParent, model.getChildCount (dropParent) - 1);

          this.expandAll (new TreePath(getPathForEntry(model, pastedEntry)));
          this.setSelectedEntry (pastedEntry);
        }
      } catch (Exception ex)
      {
        Log.warn ("drop failed", this, ex);
      }
    }
  }

  public void showDragUnderFeedback (boolean dropOK, DropTargetDragEvent e)
  {
    // zip
  }

  public void hideDragUnderFeedback ()
  {
    // zip
  }

  protected void selectionChanged (TreeSelectionEvent e)
  {
    if (selectionListeners != null)
      selectionListeners.fireSelectionChanged (e);
  }

  // Tree cell renderer (adds support for Iconic values)

  /**
   * Extends default tree cell renderer to add support for Iconic
   * values.
   */
  protected class MyTreeCellRenderer extends DefaultTreeCellRenderer
  {
    /**
     * Updates the current open, closed and leaf icons to match a
     * given value.
     */
    public void updateIcons (Object value)
    {
      // if value is not iconic, but is a node, use its user object instead
      if (value instanceof DfcTreeNode && !(value instanceof Iconic))
        value = ((DfcTreeNode)value).getUserObject ();

      if (value instanceof Iconic)
      {
        Iconic iconicValue = (Iconic)value;

        closedIcon = iconicValue.getIcon ();
        leafIcon = closedIcon;

        if (value instanceof IconicFolder)
          openIcon = ((IconicFolder)value).getOpenIcon ();
        else
          openIcon = iconicValue.getIcon ();
      } else
      {
        closedIcon = getDefaultClosedIcon ();
        openIcon = getDefaultOpenIcon ();
        leafIcon = getDefaultLeafIcon ();
      }
    }

    public Component getTreeCellRendererComponent
      (JTree tree, Object value, boolean sel, boolean expanded, boolean leaf,
       int row, boolean cellHasFocus)
    {
      updateIcons (value);

      return super.getTreeCellRendererComponent
        (tree, value, sel, expanded, leaf, row, cellHasFocus);
    }
  }

  /**
   * Extension of DefaultTreeCellEditor to support value-based icons
   * used in MyTreeCellRenderer
   */
  protected class MyTreeCellEditor extends DefaultTreeCellEditor
  {
    public MyTreeCellEditor (MyTreeCellRenderer renderer)
    {
      super (DfcTree.this, renderer);
    }

    /**
     * Override determineOffset() to call updateIcons() on
     * MyTreeCellRenderer before setting up editor.
     */
    protected void determineOffset (JTree theTree, Object value,
                                    boolean isSelected, boolean expanded,
                                    boolean leaf, int row)
    {
      if (renderer instanceof MyTreeCellRenderer)
      {
        MyTreeCellRenderer myRenderer = (MyTreeCellRenderer)renderer;

        myRenderer.updateIcons (value);
      }

      super.determineOffset (theTree, value, isSelected, expanded, leaf, row);
    }

    /**
     * Disable annoying auto-edit feature that triggers an edit 1
     * second after click on already selected item.
     */
    protected boolean shouldStartEditingTimer (EventObject event)
    {
      return false;
    }
  }

  /**
   * Listens for selection model and clipboard changes and updates
   * cnpCopyCutEnabled.
   */
  class MyListener implements TreeSelectionListener, ClipboardListener
  {
    public void valueChanged (TreeSelectionEvent e)
    {
      updateCnpEnabled ();

      selectionChanged (e);
    }

    public void clipboardChanged (ClipboardEvent e)
    {
      updateCnpEnabled ();
    }
  }
}
