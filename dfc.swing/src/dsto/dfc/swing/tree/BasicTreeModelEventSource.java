package dsto.dfc.swing.tree;

import java.io.Serializable;
import java.util.Vector;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;

import dsto.dfc.util.Disposable;

/**
 * Supports TreeModelEvent firing and listener registration.
 *
 * @version $Revision$
 */
public class BasicTreeModelEventSource
  implements Disposable, Serializable
{
  private transient Vector treeModelListeners;

  public void dispose ()
  {
    treeModelListeners = null;
  }

  public int getTreeModelListenerCount ()
  {
    if (treeModelListeners != null)
      return treeModelListeners.size ();
    else
      return 0;
  }

  public synchronized void removeTreeModelListener (TreeModelListener l)
  {
    if(treeModelListeners != null && treeModelListeners.contains(l))
    {
      Vector v = (Vector) treeModelListeners.clone();
      v.removeElement(l);
      treeModelListeners = v;
    }
  }

  public synchronized void addTreeModelListener (TreeModelListener l)
  {
    if (l == null)
      throw new Error ("Attempt to add null tree model listener");

    Vector v = treeModelListeners == null ? new Vector(2) : (Vector) treeModelListeners.clone();
    if(!v.contains(l))
    {
      v.addElement(l);
      treeModelListeners = v;
    }
  }

  public void fireTreeNodeChanged (Object source, Object [] path,
                                   int index, Object entry)
  {
    fireTreeNodesChanged (new TreeModelEvent (source, path,
                                              new int [] {index},
                                              new Object [] {entry}));
  }

  public final void fireTreeNodesChanged (TreeModelEvent e)
  {
    if(treeModelListeners != null)
    {
      Vector listeners = treeModelListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((TreeModelListener) listeners.elementAt(i)).treeNodesChanged(e);
      }
    }
  }

  public void fireTreeNodeInserted (Object source, Object [] path,
                                    int index, Object entry)
  {
    fireTreeNodesInserted (new TreeModelEvent (source, path,
                                              new int [] {index},
                                              new Object [] {entry}));
  }

  public final void fireTreeNodesInserted (TreeModelEvent e)
  {
    if(treeModelListeners != null)
    {
      Vector listeners = treeModelListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((TreeModelListener) listeners.elementAt(i)).treeNodesInserted(e);
      }
    }
  }

  public void fireTreeNodeRemoved (Object source, Object [] path,
                                   int index, Object entry)
  {
    fireTreeNodesRemoved (new TreeModelEvent (source, path,
                                              new int [] {index},
                                              new Object [] {entry}));
  }

  public final void fireTreeNodesRemoved (TreeModelEvent e)
  {
    if(treeModelListeners != null)
    {
      Vector listeners = treeModelListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((TreeModelListener) listeners.elementAt(i)).treeNodesRemoved(e);
      }
    }
  }

  public void fireTreeStructureChanged (Object source, Object [] path)
  {
    fireTreeStructureChanged (new TreeModelEvent (source, path, null, null));
  }

  public final void fireTreeStructureChanged (TreeModelEvent e)
  {
    if(treeModelListeners != null)
    {
      Vector listeners = treeModelListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((TreeModelListener) listeners.elementAt(i)).treeStructureChanged(e);
      }
    }
  }
}
