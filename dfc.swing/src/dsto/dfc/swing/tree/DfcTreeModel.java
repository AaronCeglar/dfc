package dsto.dfc.swing.tree;

import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import javax.swing.tree.TreeModel;

/**
 * Extends the JFC TreeModel to include capabilities such as adding
 * and removing entries plus copy and paste support.
 *
 * @version $Revision$
 */
public interface DfcTreeModel extends TreeModel
{
  /**
   * True if the tree can be changed.  The can* methods indicate what
   * operations are permitted.
   */
  public boolean isMutable ();

  /**
   * Get the parent for a given entry, or null if the entry is the
   * root.
   */
  public Object getParent (Object entry);

  /**
   * Get the path for a given entry.
   *
   * @return the path for the entry or null if entry does not exist in
   * the tree.
   */
  public Object [] getPathForEntry (Object entry);
  
  /**
   * Test if a child can be added at a given index.
   *
   * @param parent The parent entry.
   * @param entry The child entry.
   * @param index The index to add the child at.  Use -1 to add child
   * at end.
   */
  public boolean canAddEntry (Object parent, Object entry, int index);
  
  /**
   * Add a child to parent at a given index.
   *
   * @param parent The parent entry.
   * @param entry The child entry.
   * @param index The index to add the child at.  Use -1 to add child
   * at end.
   */
  public void addEntry (Object parent, Object entry, int index)
    throws UnsupportedOperationException, IndexOutOfBoundsException;

  /**
   * Test if an entry may be removed.
   */
  public boolean canRemoveEntry (Object entry);
  
  /**
   * Remove an entry and any children.
   */
  public void removeEntry (Object entry)
    throws UnsupportedOperationException;


  public boolean canMoveEntry (Object entry,
                               Object newParent, int newIndex);
  
  /**
   * Move a child from one location to another.  In the case where the
   * old and new parents are the same, newIndex refers to the index
   * space before any changes has been made.
   */
  public void moveEntry (Object entry,
                         Object newParent, int newIndex)
    throws UnsupportedOperationException, IndexOutOfBoundsException;

  // Copy and paste support

  /**
   * Test if a transferable may be pasted.
   *
   * @param parent The parent entry to paste at.
   * @param index The index of the new entry in parent (may be -1 to
   * add last).
   * @param transferable The data to paste. 
   */
  public boolean canPasteEntry (Object parent, int index,
                                Transferable transferable);

  /**
   * Paste transferable data into the tree.
   *
   * @param parent The parent entry to paste at.
   * @param index The index of the new entry in parent (may be -1 to
   * add last).
   * @param transferable The data to paste.
   */
  public void pasteEntry (Object parent, int index,
                          Transferable transferable)
    throws UnsupportedOperationException,
           UnsupportedFlavorException, CloneNotSupportedException,
           IOException;

  /**
   * Test if an entry may be copied into a transferable.
   *
   * @param entry The entry to copy.
   */
  public boolean canCopyEntry (Object entry);

  /**
   * Copy an entry into a transferable.
   *
   * @param entry The entry to copy.
   */
  public Transferable copyEntry (Object entry)
    throws UnsupportedOperationException, CloneNotSupportedException;
}
