package dsto.dfc.swing.tree;

import java.io.PrintStream;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import dsto.dfc.util.Disposable;
import dsto.dfc.util.Objects;

/**
 * General utility methods for trees.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class Trees
{
  private Trees ()
  {
    // zip
  }

  /**
   * Get the path to a given entry in the tree model.  If model is a
   * DfcTreeModel, its getPathForEntry () method is used, otherwise
   * uses inefficient generic algorithm.
   */
  public static Object [] getPathForEntry (TreeModel model, Object entry)
  {
    if (model instanceof DfcTreeModel)
      return ((DfcTreeModel)model).getPathForEntry (entry);
    else
      return stoopidGetPathForEntry (new TreePath (model.getRoot ()), model,
                                     entry).getPath ();
  }

  private static TreePath stoopidGetPathForEntry (TreePath parentPath,
                                                  TreeModel model,
                                                  Object entry)
  {
    Object parent = parentPath.getLastPathComponent ();

    if (entry.equals (parent))
      return parentPath;
    else
    {
      for (int i = model.getChildCount (parent); i >= 0; i--)
      {
        TreePath newPath =
          parentPath.pathByAddingChild (model.getChild (parent, i));

        TreePath matchedPath = stoopidGetPathForEntry (newPath, model, entry);

        if (matchedPath != null)
          return matchedPath;
      }

      return null;
    }
  }

  /**
   * Clone a tree of nodes.
   *
   * @param root The tree node to copy from.
   *
   * @return The root of the cloned node tree.
   */
  public static MutableTreeNode cloneNodes (TreeNode root)
    throws CloneNotSupportedException
  {
    try
    {
      MutableTreeNode newRoot =
        (MutableTreeNode)Objects.cloneObject (root);

      // clone children
      for (int i = 0; i < root.getChildCount (); i++)
      {
        TreeNode child = root.getChildAt (i);
        newRoot.insert (cloneNodes (child), i);
      }

      return newRoot;
    } catch (ClassCastException ex)
    {
      // assume we hit a TreeNode
      throw new CloneNotSupportedException ("Cannot clone tree with non-mutable nodes");
    }
  }
  
  /**
   * Calls dispose on all {@link dsto.dfc.util.Disposable} nodes starting from
   * a given root node.
   */
  public static void disposeNodes (TreeNode root)
  {
    // dispose children
    for (int i = 0; i < root.getChildCount (); i++)
    {
      TreeNode child = root.getChildAt (i);
      
      disposeNodes (child);
    }

    if (root instanceof Disposable)
      ((Disposable)root).dispose ();
  }

  /**
   * @see #dumpTree(TreeModel,PrintStream)
   */
  public static void dumpTree (TreeModel tree)
  {
    dumpTree (tree, System.out);
  }

  /**
   * Dump a debug print of a tree model.
   *
   * @param tree The tree model to dump.
   * @param str The stream to dump to.
   */
  public static void dumpTree (TreeModel tree, PrintStream str)
  {
    dumpTree (tree, tree.getRoot (), str, 0);
  }

  private static void dumpTree (TreeModel tree, Object root,
                                PrintStream str, int level)
  {
    // ident
    for (int i = 0; i < level; i++)
      str.print ("  ");

    str.print ((root == null ? "[null]" : root.toString ()));
    str.println (" (" + tree.getChildCount (root) + " children)");

    for (int i = 0; i < tree.getChildCount (root); i++)
    {
      dumpTree (tree, tree.getChild (root, i), str, level + 1);
    }
  }

  /**
   * A debugging TreeModelListener that prints events from a tree
   * model to System.out.
   */
  public static class TreeTracer implements TreeModelListener
  {
    public TreeTracer (TreeModel model)
    {
      model.addTreeModelListener (this);
    }

    public void printPath (Object [] path)
    {
      for (int i = 0; i < path.length; i++)
      {
        System.out.print ("<");
        System.out.print (path [i].toString ());
        System.out.print ("> ");
      }

      System.out.println ();
    }

    public void printChildren (TreeModelEvent e)
    {
      int [] indices = e.getChildIndices ();

      for (int i = 0; i < indices.length; i++)
      {
        Object child = e.getChildren () [i];

        System.out.print ("  [" + indices [i] + "] ");
        System.out.println (child == null ? "[null]" : child.toString ());
      }
    }

    public void treeNodesChanged (TreeModelEvent e)
    {
      System.out.println ("treeNodesChanged");

      System.out.print ("  path: ");
      printPath (e.getPath ());

      printChildren (e);
    }

    public void treeNodesInserted (TreeModelEvent e)
    {
      System.out.println ("treeNodesInserted");

      System.out.print ("  path: ");
      printPath (e.getPath ());

      printChildren (e);
    }

    public void treeNodesRemoved (TreeModelEvent e)
    {
      System.out.println ("treeNodesRemoved");

      System.out.print ("  path: ");
      printPath (e.getPath ());

      printChildren (e);
    }

    public void treeStructureChanged (TreeModelEvent e)
    {
      System.out.println ("treeStructureChanged");

      System.out.print ("  path: ");
      printPath (e.getPath ());
    }
  }
}
