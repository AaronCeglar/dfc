package dsto.dfc.swing.tree;

import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.event.EventListenerList;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;

import dsto.dfc.util.Copyable;
import dsto.dfc.util.Objects;

/**
 * An implementation of a node-based tree DfcTreeModel using Swing's
 * DefaultTreeModel as the base.  Unlike DefaultDfcTreeModel, entries
 * in this tree are TreeNode's (or more usefully {@link DfcTreeNode})
 * - the data entries are stored in the userObject property of each
 * node.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class DfcSwingTreeModel
  extends DefaultTreeModel implements DfcTreeModel, Copyable
{
  private boolean mutable = true;

  public DfcSwingTreeModel ()
  {
    super (new DefaultDfcTreeNode ());
  }

  public DfcSwingTreeModel (TreeNode root)
  {
    super (root);
  }
  
  /**
   * Calls dispose on all {@link dsto.dfc.util.Disposable} nodes in the model.
   *
   * @see Trees#disposeNodes(TreeNode)
   */
  public void dispose ()
  {
    Trees.disposeNodes (getRootNode ());
  }

  public DfcTreeNode getRootNode ()
  {
    return (DfcTreeNode)getRoot ();
  }

  /**
   * Shortcut to add a user object to a node in the tree.
   *
   * @param parent The node to add the new child node to.
   * @param entry The entry (user object) for the new child.
   * @return The new node (node.getUserObject () == entry).
   */
  public DfcTreeNode addChild (MutableTreeNode parent, Object entry)
  {
    return addChild (parent, entry, parent.getChildCount ());
  }

  /**
   * Shortcut to add a user object to a node in the tree.
   *
   * @param parent The node to add the new child node to.
   * @param entry The entry (user object) for the new child.
   * @param index The index to insert the child at.
   * @return The new node (node.getUserObject () == entry).
   */
  public DfcTreeNode addChild (MutableTreeNode parent,
                               Object entry, int index)
  {
    DfcTreeNode newNode = new DefaultDfcTreeNode (entry);

    insertNodeInto (newNode, parent, index);

    return newNode;
  }

  /**
   * Remove all children of the root node.
   *
   * @see #removeAllChildren
   */
  public void removeAll ()
  {
    removeAllChildren ((MutableTreeNode)root);
  }

  /**
   * Remove all children of a node.
   *
   * @see #removeAll
   */
  public void removeAllChildren (MutableTreeNode node)
  {
    Object [] nodePath = getPathToRoot (node);
    int [] indexes = new int [] {0};
    Object [] childNodes = new Object [1];

    while (node.getChildCount () > 0)
    {
      childNodes [0] = node.getChildAt (0);
      node.remove (0);

      fireTreeNodesRemoved (this, nodePath, indexes, childNodes);
    }
  }

  /**
   * Like getPathToRoot() except that the returned path is defined in
   * terms of the user objects associated with the nodes.  This can be
   * used, for example, to create a 'logical' tree path that works
   * across two tree models with similar content but different nodes.
   *
   * @see #makeNodePath
   */
  public Object [] getObjectPathToRoot (DfcTreeNode node)
  {
    ArrayList path = new ArrayList ();

    for (DfcTreeNode n = node; n != null; n = (DfcTreeNode)n.getParent ())
      path.add (0, n.getUserObject ());

    return path.toArray ();
  }

  /**
   * Convert a path defined in terms of user objects to one in this
   * tree defined in terms of tree node's.
   *
   * @exception IllegalArgumentException if objectPath did not match a
   * path in the tree.
   *
   * @see #getObjectPathToRoot
   * @see #makeObjectPath
   */
  public DfcTreeNode [] makeNodePath (Object [] objectPath)
    throws IllegalArgumentException
  {
    ArrayList nodePath = new ArrayList ();
    DfcTreeNode node = getRootNode ();

    nodePath.add (node);

    for (int i = 1; i < objectPath.length; i++)
    {
      node = node.findChild (objectPath [i]);

      if (node == null)
        throw new IllegalArgumentException ("Path not in tree");

      nodePath.add (node);
    }

    DfcTreeNode [] nodePathArray = new DfcTreeNode [nodePath.size ()];
    nodePath.toArray (nodePathArray);

    return nodePathArray;
  }

  /**
   * Create a path defined in terms of user objects from a path defined by
   * nodes.  This can be used, for example, to create a 'logical' tree path
   * that works across two tree models with similar content but different
   * node instances.
   *
   * @see #makeNodePath
   * @exception ClassCastException if an entry in nodePath is not a
   * DfcTreeNode.
   */
  public static Object [] makeObjectPath (Object [] nodePath)
    throws ClassCastException
  {
    Object [] path = new Object [nodePath.length];

    for (int i = 0; i < nodePath.length; i++)
    {
      path [i] = ((DfcTreeNode)nodePath [i]).getUserObject ();
    }

    return path;
  }

  /**
   * Convenience method to fire the correct event when a
   * node has changed.
   */
  protected void fireTreeNodeChanged (DfcTreeNode node)
  {
    Object [] path = getPathForEntry (node.getParent ());
    int index = getIndexOfChild (node.getParent (), node);

    fireTreeNodesChanged (this, path, new int [] {index}, new Object [] {node});
  }

  // DfcTreeModel interface

  public boolean isMutable ()
  {
    return mutable;
  }

  public void setMutable (boolean newValue)
  {
    mutable = newValue;
  }

  public Object getParent (Object entry)
  {
    return ((TreeNode)entry).getParent ();
  }

  public Object [] getPathForEntry (Object entry)
  {
    return getPathToRoot ((TreeNode)entry);
  }

  public boolean canAddEntry (Object parent, Object entry, int index)
  {
    return (parent instanceof MutableTreeNode) &&
           (entry instanceof MutableTreeNode);
  }

  public void addEntry (Object parent, Object entry, int index)
    throws UnsupportedOperationException, IndexOutOfBoundsException
  {
    if (canAddEntry (parent, entry, index))
    {
      insertNodeInto ((MutableTreeNode)entry, (MutableTreeNode)parent, index);
    } else
    {
      throw new UnsupportedOperationException ();
    }
  }

  public boolean canRemoveEntry (Object entry)
  {
    TreeNode parent = ((TreeNode)entry).getParent ();

    return (entry instanceof MutableTreeNode) &&
           (parent instanceof MutableTreeNode);
  }

  public void removeEntry (Object entry) throws UnsupportedOperationException
  {
    if (canRemoveEntry (entry))
    {
      removeNodeFromParent ((MutableTreeNode)entry);
    } else
    {
      throw new UnsupportedOperationException ();
    }
  }

  public boolean canMoveEntry (Object entry, Object newParent, int newIndex)
  {
    return canRemoveEntry (entry) &&
           canAddEntry (newParent, entry, newIndex);
  }

  public void moveEntry (Object entry, Object newParent, int newIndex)
    throws UnsupportedOperationException, IndexOutOfBoundsException
  {
    if (canMoveEntry (entry, newParent, newIndex))
    {
      MutableTreeNode childNode = (MutableTreeNode)entry;

      removeNodeFromParent (childNode);
      insertNodeInto (childNode, (MutableTreeNode)newParent, newIndex);
    } else
    {
      throw new UnsupportedOperationException ();
    }
  }

  public boolean canPasteEntry (Object parent, int index,
                                Transferable transferable)
  {
    return (parent instanceof MutableTreeNode) &&
      transferable.isDataFlavorSupported (TreeModelTransferable.TREE_MODEL_FLAVOR);
  }

  public void pasteEntry (Object parent, int index, Transferable transferable)
    throws UnsupportedOperationException, UnsupportedFlavorException,
           CloneNotSupportedException, IOException
  {
    TreeModel model =
      (TreeModel)transferable.getTransferData (TreeModelTransferable.TREE_MODEL_FLAVOR);
    TreeModel newModel = (TreeModel)Objects.cloneObject (model);

    MutableTreeNode parentNode = (MutableTreeNode)parent;

    if (index == -1)
      index = parentNode.getChildCount ();

    insertNodeInto ((MutableTreeNode)newModel.getRoot (),
                    parentNode, index);
  }

  public boolean canCopyEntry (Object entry)
  {
    return true;
  }

  public Transferable copyEntry (Object entry)
    throws UnsupportedOperationException, CloneNotSupportedException
  {
    TreeModel newModel = clone ((TreeNode)entry);

    return new TreeModelTransferable (newModel);
  }

  // clone support

  public Object clone () throws CloneNotSupportedException
  {
    return clone ((TreeNode)getRoot ());
  }

  /**
   * Clone a subtree of this model.
   *
   * @param node The root node in this tree to copy from.
   * @return A new copy of the subtree.  Nodes within the subtree
   * should be cloned also.
   * @exception CloneNotSupportedException if the tree or any of its
   * entries are not cloneable.
   */
  public DfcSwingTreeModel clone (TreeNode node)
    throws CloneNotSupportedException
  {
    DfcSwingTreeModel newModel = (DfcSwingTreeModel)super.clone ();

    newModel.listenerList = new EventListenerList ();
    newModel.root = DfcSwingTreeModel.cloneNodes ((TreeNode)getRoot ());

    return newModel;
  }

  /**
   * Clone a tree of nodes.
   *
   * @see Trees#cloneNodes
   */
  public static MutableTreeNode cloneNodes (TreeNode root)
    throws CloneNotSupportedException
  {
    return Trees.cloneNodes (root);
  }
}
