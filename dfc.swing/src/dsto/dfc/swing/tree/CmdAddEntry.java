package dsto.dfc.swing.tree;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.Icon;
import javax.swing.KeyStroke;
import javax.swing.tree.TreePath;

import dsto.dfc.logging.Log;
import dsto.dfc.swing.commands.AbstractSelectionBasedCommand;
import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.swing.event.SelectionEvent;
import dsto.dfc.swing.icons.ImageLoader;
import dsto.dfc.util.Objects;

/**
 * Add a new entry as a child of the currently selected entry.
 *
 * @version $Revision$
 */
public class CmdAddEntry extends AbstractSelectionBasedCommand
{
  private static final Icon ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/plus.gif");
  private static final KeyStroke ACCELERATOR =
    KeyStroke.getKeyStroke (KeyEvent.VK_ENTER,
                            Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask ());

  private DfcTree tree;
  private Object entryTemplate;
  private boolean editAfterAdd = true;

  public CmdAddEntry (DfcTree tree)
  {
    this (tree, null);
  }

  public CmdAddEntry (DfcTree tree, Cloneable entryTemplate)
  {
    super (tree);

    this.tree = tree;
    this.entryTemplate = entryTemplate;
  }

  protected CmdAddEntry ()
  {
    tree = null;
  }

  /**
   * Set whether cell editor is invoked on a new entry.
   */
  public void setEditAfterAdd (boolean newValue)
  {
    editAfterAdd = newValue;
  }

  public boolean isEditAfterAdd ()
  {
    return editAfterAdd;
  }

  public void execute ()
  {
    Object selected = tree.getLastSelectedPathComponent ();

    if (selected != null && tree.getModel () instanceof DfcTreeModel)
    {
      try
      {
        DfcTreeModel model = (DfcTreeModel)tree.getModel ();
        int index = model.getChildCount (selected);

        Object newValue = createNewEntry (selected, index);

        model.addEntry (selected, newValue, index);

        // select new entry
        TreePath entryPath = new TreePath (model.getPathForEntry (newValue));
        tree.setSelectionPath (entryPath);

        if (entryAdded (entryPath))
        {
          if (isEditAfterAdd ())
           tree.startEditingAtPath (entryPath);
        } else
        {
          // entryAdded () method cancelled add operation: remove
          // newValue from model.
          model.removeEntry (newValue);
        }
      } catch (UnsupportedOperationException ex)
      {
        Log.warn ("Model vetoed add entry command", this, ex);
      } catch (CloneNotSupportedException ex)
      {
        Log.warn ("Failed to clone new entry", this, ex);
      }
    }
  }

  public String getName ()
  {
    return "edit.Add Entry";
  }

  public Icon getIcon ()
  {
    return ICON;
  }

  public String getDescription ()
  {
    return "Add a new child to the currently selected tree entry";
  }

  public boolean isInteractive ()
  {
    return false;
  }

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return "change";
    else if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return "Edit.change";
    else if (viewName.equals (CommandView.TOOLBAR_VIEW))
      return "change";
    else
      return null;
  }

  public char getMnemonic ()
  {
    return 'd';
  }

  public KeyStroke getAccelerator ()
  {
    return ACCELERATOR;
  }

  public void selectionChanged (SelectionEvent e)
  {
    setEnabled (updateEnabled ());
  }

  /**
   * Create a new entry to be added to the tree.  The default
   * implementation is to return a clone of entryTemplate, but
   * subclasses may choose to override this.
   *
   * @param selectedEntry The currently selected model entry.
   * @param selectedIndex The index of the selected entry.
   * @return A new value to be added to the tree model.
   * @exception CloneNotSupportedException if a new value could not be
   * created.
   */
  protected Object createNewEntry (Object selectedEntry, int selectedIndex)
    throws CloneNotSupportedException
  {
    return Objects.cloneObject (entryTemplate);
  }

  /**
   * Called after a new entry has been added to the model.  Can be
   * used to perform post-add customisation of entry (eg popup a
   * customisation dialog).
   *
   * @param newEntryPath The path to the new entry.
   * @return True if entry should be added, false if add operation
   * should be cancelled.
   */
  protected boolean entryAdded (TreePath newEntryPath)
  {
    return true;
  }

  /**
   * Return true if the command should be enabled in the current tree
   * context.  By default this is called by selectionChanged () and
   * the result used in a call to setEnabled ().  Superclasses may
   * choose to override this, especially if they define
   * createNewEntry() also.
   *
   * @return True if the command should be enabled.
   */
  protected boolean updateEnabled ()
  {
    Object selected = tree.getLastSelectedPathComponent ();

    if (entryTemplate == null)
      return true;
    else if (selected != null && tree.getModel () instanceof DfcTreeModel)
    {
      int index = tree.getModel ().getChildCount (selected);

      return ((DfcTreeModel)tree.getModel ()).canAddEntry (selected, entryTemplate, index);
    } else
      return false;
  }
}
