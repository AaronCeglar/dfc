package dsto.dfc.swing.tree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import java.io.IOException;
import java.io.Serializable;

import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;

import javax.swing.event.TreeModelEvent;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import dsto.dfc.util.Copyable;
import dsto.dfc.util.Objects;

/**
 * Default implementation of DfcTreeModel using HashMap and
 * ArrayList.<p>
 *
 * Note that the entries in this tree model are the data items
 * themselves, which are internally mapped to tree node structure
 * (see {@link #getNodeForEntry}).  This is convenient in many
 * cases, but means that objects cannot appear more than once in
 * the model.  If this is a problem, see {@link DfcSwingTreeModel}
 * for a fully node-based model that avoids this problem.
 *
 * @version $Revision$
 */
public class DefaultDfcTreeModel
  extends BasicTreeModelEventSource
  implements DfcTreeModel, Copyable, Serializable
{
  /** Map node entries to their corresponding node instance in this tree */
  private HashMap entryToNode = new HashMap ();
  /** the root node of this tree */
  private Node root;
  private boolean mutable = true;

  public DefaultDfcTreeModel ()
  {
    this ("");
  }

  public DefaultDfcTreeModel (Object rootEntry)
  {
    this.root = new Node (rootEntry);

    entryToNode.put (rootEntry, root);
  }

  public DefaultDfcTreeModel (TreeModel srcModel, Object srcRoot)
    throws CloneNotSupportedException
  {
    this ();

    copyFromSubtree (root, 0, srcModel, srcRoot);
  }
  
  /**
   * Get the root node of the tree.
   */
  public Node getRootNode ()
  {
    return root;
  }

  public boolean isMutable ()
  {
    return mutable;
  }

  public void setMutable (boolean newValue)
  {
    mutable = newValue;
  }

  public Object getParent (Object entry)
  {
    Node node = getNodeForEntry (entry);

    if (node == root)
      return null;
    else if (node != null)
      return node.getParent ().getEntry ();
    else
      throw new Error ("entry not in tree");
  }

  public final Object [] getPathForNode (Node node)
  {
    ArrayList pathList = new ArrayList ();

    for (Node n = node; n != null; n = n.getParent ())
      pathList.add (n.getEntry ());

    Collections.reverse (pathList);

    return pathList.toArray ();
  }

  public final Node getNodeForPath (Object [] path)
  {
    // follow path to discover parent node
    Node node = root;

    for (int i = 1; i < path.length; i++)
      node = node.getChild (path [i]);

    return node;
  }

  public Node getNodeForEntry (Object entry)
  {
    return (Node)entryToNode.get (entry);
  }

  public TreePath getTreePathForEntry (Object entry)
  {
    Object [] pathArray = getPathForEntry (entry);

    return new TreePath (pathArray);
  }

  public Object [] getPathForEntry (Object entry)
  {
    if (entry.equals (root.getEntry ()))
      return new Object [] {root.getEntry ()};
    else
    {
      Node node = (Node)entryToNode.get (entry);

      if (node != null)
        return getPathForNode (node);
      else
        return null;
    }
  }

  public Node addChild (Node parent, Object entry)
  {
    return addChild (parent, entry, parent.getChildCount ());
  }

  public Node addChild (Node parent, Object entry, int index)
  {
    Node node = new Node (entry);
    parent.add (node, index);

    entryToNode.put (entry, node);

    fireTreeNodesInserted
      (new TreeModelEvent (this, getPathForNode (parent),
                             new int [] {index},
                             new Object [] {entry}));

    return node;
  }

  /**
   * Remove all entries from tree.
   */
  public void removeAll ()
  {
    removeAllChildren (root);
  }

  /**
   * Remove all children of a node.
   */
  public void removeAllChildren (Node node)
  {
    Object [] nodePath = getPathForNode (node);

    while (node.getChildCount () > 0)
    {
      Node childNode = node.remove (0);

      fireTreeNodesRemoved
        (new TreeModelEvent (this, nodePath,
                             new int [] {0},
                             new Object [] {childNode.getEntry ()}));

      removeEntryMappings (childNode);
    }
  }

  // TreeModel interface

  public Object getRoot ()
  {
    return root.getEntry ();
  }

  public Object getChild (Object parent, int index)
  {
    Node parentNode = (Node)entryToNode.get (parent);

    return parentNode.getChild (index).getEntry ();
  }

  public int getChildCount (Object parent)
  {
    Node parentNode = (Node)entryToNode.get (parent);

    if (parentNode == null)
      return 0;
    else
      return parentNode.getChildCount ();
  }

  public boolean isLeaf (Object entry)
  {
    Node node = (Node)entryToNode.get (entry);

    return node == null || node.getChildCount () == 0;
  }

  public void valueForPathChanged (TreePath path, Object newValue)
  {
    // zip
  }

  public int getIndexOfChild (Object parent, Object child)
  {
    Node parentNode = (Node)entryToNode.get (parent);

    return parentNode.getIndexOfEntry (child);
  }

  // DfcTreeModel interface

  public boolean canAddEntry (Object parent, Object entry, int index)
  {
    return true;
  }

  public void addEntry (Object parent, Object child, int index)
    throws UnsupportedOperationException
  {
    Node parentNode = (Node)entryToNode.get (parent);

    if (index == -1)
      index = parentNode.getChildCount ();

    if (parentNode != null)
    {
      Node node = new Node (child);
      entryToNode.put (child, node);
      parentNode.add (node, index);

      fireTreeNodesInserted
        (new TreeModelEvent (this, getPathForNode (parentNode),
                             new int [] {index},
                             new Object [] {child}));

    } else
      throw new IllegalArgumentException ("parent does not exist in tree");
  }

  public boolean canRemoveEntry (Object entry)
  {
    return true;
  }

  public void removeEntry (Object entry)
    throws UnsupportedOperationException
  {
    Node node = (Node)entryToNode.get (entry);

    if (node != null)
      removeNode (node);
  }

  public void removeNode (Node node)
  {
    int index = node.getIndex ();
    node.getParent ().remove (index);

    fireTreeNodesRemoved
      (new TreeModelEvent (this, getPathForNode (node.getParent ()),
                           new int [] {index},
                           new Object [] {node.getEntry ()}));

    removeEntryMappings (node);
  }

  /**
   * Remove all entry to node mappings for a node tree.
   */
  protected final void removeEntryMappings (Node node)
  {
    entryToNode.remove (node.getEntry ());

    for (int i = 0; i < node.getChildCount (); i++)
      removeEntryMappings (node.getChild (i));
  }

  public boolean canMoveEntry (Object entry,
                               Object newParent, int newIndex)
  {
    return true;
  }

  public void moveEntry (Object entry,
                         Object newParent, int newIndex)
    throws UnsupportedOperationException, IndexOutOfBoundsException
  {
    Object oldParent = getParent (entry);
    int oldIndex = getIndexOfChild (oldParent, entry);
    Node oldParentNode = (Node)entryToNode.get (oldParent);
    Node newParentNode = (Node)entryToNode.get (newParent);

    if (oldParent != null && newParent != null)
    {
      Node node = oldParentNode.getChild (oldIndex);
      oldParentNode.remove (oldIndex);

      fireTreeNodesRemoved
        (new TreeModelEvent (this, getPathForNode (oldParentNode),
                             new int [] {oldIndex},
                             new Object [] {node.getEntry ()}));

      newParentNode.add (node, newIndex);

      fireTreeNodesInserted
        (new TreeModelEvent (this, getPathForNode (newParentNode),
                             new int [] {newIndex},
                             new Object [] {node.getEntry ()}));

    } else
      throw new IllegalArgumentException ("parent does not exist in tree");
  }

  // Copy and paste support

  public boolean canPasteEntry (Object parent, int index,
                                Transferable transferable)
  {
    return transferable.isDataFlavorSupported (TreeModelTransferable.TREE_MODEL_FLAVOR);
  }

  public void pasteEntry (Object parent, int index,
                          Transferable transferable)
    throws UnsupportedOperationException,
           UnsupportedFlavorException, CloneNotSupportedException,
           IOException
  {
    TreeModel model =
      (TreeModel)transferable.getTransferData (TreeModelTransferable.TREE_MODEL_FLAVOR);
    TreeModel newModel = (TreeModel)Objects.cloneObject (model);

    if (index == -1)
      index = getNodeForEntry (parent).getChildCount ();

    addSubtree (parent, index, newModel, newModel.getRoot ());
  }

  public boolean canCopyEntry (Object entry)
  {
    return true;
  }

  public Transferable copyEntry (Object entry)
    throws UnsupportedOperationException, CloneNotSupportedException
  {
    TreeModel newModel = clone (entry);

    return new TreeModelTransferable (newModel);
  }

  // clone support

  public Object clone ()
    throws CloneNotSupportedException
  {
    return clone (root.getEntry ());
  }

  /**
   * Clone a subtree of this model.
   *
   * @param srcRoot The root node in this tree to copy from.
   * @return A new copy of the subtree.  Nodes within the subtree should be
   * cloned also.
   * @exception CloneNotSupportedException if the tree or any of its entries
   * are not cloneable.
   */
  public DfcTreeModel clone (Object srcRoot)
    throws CloneNotSupportedException
  {
    DefaultDfcTreeModel newModel = (DefaultDfcTreeModel)super.clone ();
    Node srcRootNode = getNodeForEntry (srcRoot);

    newModel.entryToNode = new HashMap ();
    newModel.root = newModel.cloneNodes (srcRootNode);

    return newModel;
  }

  /**
   * Clone a tree of nodes and add them to this models entry to node
   * map.
   *
   * @return The root of the cloned node tree.
   */
  protected final Node cloneNodes (Node node)
    throws CloneNotSupportedException
  {
    Object newEntry;

    try
    {
      newEntry = Objects.cloneObject (node.getEntry ());
    } catch (Exception ex)
    {
      throw new CloneNotSupportedException ();
    }

    Node newNode = new Node (newEntry);
    entryToNode.put (newEntry, newNode);

    // clone children
    for (int i = 0; i < node.getChildCount (); i++)
    {
      Node child = node.getChild (i);
      newNode.add (cloneNodes (child));
    }

    return newNode;
  }

  // Other utility methods

  /**
   * Copy a subtree into this model.  Only one treeNodesInserted event
   * is fired for the subtree root node.
   *
   * @param trgRoot The node in this tree to add children to.
   * @param trgIndex The index in trgRoot to add the new subtree at.
   * @param srcModel The source model to copy from.
   * @param srcRoot The root node in the source model to copy children
   * from.
   */
  public void addSubtree (Object trgRoot, int trgIndex,
                          TreeModel srcModel, Object srcRoot)
    throws CloneNotSupportedException
  {
    Node trgRootNode = (Node)entryToNode.get (trgRoot);

    // copy the subtree into the model
    copyFromSubtree (trgRootNode, trgIndex, srcModel, srcRoot);

    // fire event
    fireTreeNodesInserted
      (new TreeModelEvent (this, getPathForNode (trgRootNode),
                           new int [] {trgIndex},
                           new Object [] {srcRoot}));
  }

  /**
   * Copy a subtree from a TreeModel into this model.
   *
   * @param trgRoot The root node to add new subtree to.
   * @param trgIndex The Index to add the subtree at.
   * @param srcModel The model to copy from.
   * @param srcRoot The root entry in srcModel to start copying at.
   */
  protected final void copyFromSubtree (Node trgRoot, int trgIndex,
                                        TreeModel srcModel, Object srcRoot)
    throws CloneNotSupportedException
  {
    Object newEntry;

    try
    {
      newEntry = Objects.cloneObject (srcRoot);
    } catch (Exception ex)
    {
      throw new CloneNotSupportedException ();
    }

    Node trgChild = new Node (newEntry);
    entryToNode.put (newEntry, trgChild);

    trgRoot.add (trgChild, trgIndex);

    int srcChildCount = srcModel.getChildCount (srcRoot);

    for (int srcIndex = 0; srcIndex < srcChildCount; srcIndex++)
    {
      Object srcChild = srcModel.getChild (srcRoot, srcIndex);

      copyFromSubtree (trgChild, srcIndex, srcModel, srcChild);
    }
  }

  /**
   * A node in the tree.  Can be used (via getRootNode () and
   * getNodeForEntry ()) as a slightly faster way to scan through the
   * tree than the TreeModel interface.
   */
  public static final class Node implements Serializable
  {
    private Node parent;
    private Object entry;
    private ArrayList children = new ArrayList (5);

    /**
     * Create a root node.
     */
    protected Node ()
    {
      this (new String (""));
    }

    /**
     * Create a node with associated view entry.
     */
    protected Node (Object entry)
    {
      this.parent = null;
      this.entry = entry;
    }

    public Node getParent ()
    {
      return parent;
    }

    /**
     * The index of this node in parent.
     */
    public int getIndex ()
    {
      if (parent == null)
        return 0;
      else
        return parent.children.indexOf (this);
    }

    /**
     * The entry associated with this node.
     */
    public Object getEntry ()
    {
      return entry;
    }

    /**
     * Add a child node at its appropriate position in the child list.
     */
    protected void add (Node child, int index)
    {
      children.add (index, child);

      child.parent = this;
    }

    protected int add (Node child)
    {
      children.add (child);

      child.parent = this;

      return children.size () - 1;
    }

    protected boolean remove (Node child)
    {
      return children.remove (child);
    }

    protected Node remove (int index)
    {
      return (Node)children.remove (index);
    }

    /**
     * Get the first child with matching entry, or null if no such
     * child exists.
     */
    public Node getChild (Object childEntry)
    {
      int index = getIndexOfEntry (childEntry);

      return index == -1 ? null : (Node)children.get (index);
    }

    public Node getChild (int index)
    {
      return (Node)children.get (index);
    }

    public int getChildCount ()
    {
      return children.size ();
    }

    public int getIndexOfChild (Node child)
    {
      return children.indexOf (child);
    }

    /**
     * Get index of child node with matching entry, or -1 if none found.
     */
    public int getIndexOfEntry (Object childEntry)
    {
      for (int i = 0; i < children.size (); i++)
      {
        Node child = (Node)children.get (i);

        if (child.getEntry ().equals (childEntry))
          return i;
      }

      return -1;
    }
  }
}
