package dsto.dfc.swing.tree;

import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.tree.TreePath;

/**
 * Base class for clients implementing DfcTreeModel.
 *
 * @version $Revision$
 */
public abstract class AbstractDfcTreeModel
  extends BasicTreeModelEventSource implements DfcTreeModel
{
  public boolean isAncestor (Object ancestor, Object entry)
  {
    if (ancestor.equals (entry))
      return true;
    else if (entry != null)
      return isAncestor (ancestor, getParent (entry));
    else
      return false;
  }
  
  // TreeModel interface

  public abstract Object getRoot ();

  public abstract Object getChild (Object parent, int index);

  public abstract int getChildCount (Object parent);

  public abstract boolean isLeaf (Object entry);

  public abstract int getIndexOfChild (Object parent, Object child);

  public void valueForPathChanged (TreePath path, Object newValue)
  {
    // zip
  }

  // DfcTreeModel interface

  public abstract Object getParent (Object entry);

  /**
   * Basic implementation using getRoot () and getParent ().  Superclasses
   * may want to provide a more efficient implementation.
   */
  public Object [] getPathForEntry (Object entry)
  {
    if (entry.equals (getRoot ()))
      return new Object [] {getRoot ()};
    else
    {
      ArrayList pathList = new ArrayList ();

      for (Object e = entry; e != null; e = getParent (e))
        pathList.add (e);

      Collections.reverse (pathList);

      return pathList.toArray ();
    }
  }

  public boolean isMutable ()
  {
    return false;
  }

  public boolean canAddEntry (Object parent, Object entry, int index)
  {
    return false;
  }
  
	public void addEntry (Object parent, Object entry, int index)
    throws UnsupportedOperationException, IndexOutOfBoundsException
  {
    throw new UnsupportedOperationException ();
  }

  public boolean canRemoveEntry (Object entry)
  {
    return false;
  }

  public void removeEntry (Object entry)
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException ();
  }

  public boolean canMoveEntry (Object entry,
                               Object newParent, int newIndex)
  {
    return false;
  }

	public void moveEntry (Object entry,
                         Object newParent, int newIndex)
    throws UnsupportedOperationException, IndexOutOfBoundsException
  {
    throw new UnsupportedOperationException ();
  }

  // Copy and paste support

  public boolean canPasteEntry (Object parent, int index,
                                Transferable transferable)
  {
    return false;
  }

  public void pasteEntry (Object parent, int index,
                          Transferable transferable)
    throws UnsupportedOperationException,
           UnsupportedFlavorException, CloneNotSupportedException,
           IOException
  {
    throw new UnsupportedOperationException ();
  }

  public boolean canCopyEntry (Object entry)
  {
    return false;
  }

  public Transferable copyEntry (Object entry)
    throws UnsupportedOperationException, CloneNotSupportedException
  {
    throw new UnsupportedOperationException ();
  }
}
