package dsto.dfc.swing.tree;

import javax.swing.tree.MutableTreeNode;

/**
 * The node type used in node-based DFC tree models.  Amongst other
 * things, it works around Sun's brilliant idea of not providing a
 * getUserObject () method on the MutableTreeNode interface.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public interface DfcTreeNode extends MutableTreeNode
{
  /**
   * Test if the node can be changed.
   */
  public boolean isMutable ();

  /**
   * Get the user object associated with this node.
   */
  public Object getUserObject ();


  /**
   * Set the user object associated with this node.
   */
  public void setUserObject (Object userObject);

  
  /**
   * Find the first child with a matching user object.
   *
   * @return The matching child or null if no match found.
   */
  public DfcTreeNode findChild (Object userObject);
}
