package dsto.dfc.swing;

import java.awt.Component;
import java.awt.Container;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JSplitPane;
import javax.swing.border.Border;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;

import dsto.dfc.util.Beans;

import dsto.dfc.logging.Log;

/**
 * General user interface support routines.
 *
 * @version $Revision$
 */
public final class SwingSupport
{
  private SwingSupport ()
  {
    // zip
  }

  /**
   * Remove all borders from a split pane, including the special
   * border used on the divider in JDK >= 1.3.
   */
  public static void killSplitPaneBorder (JSplitPane splitPane)
  {
    splitPane.setBorder (null);

    BasicSplitPaneUI ui = (BasicSplitPaneUI)splitPane.getUI ();
    BasicSplitPaneDivider divider = ui.getDivider ();

    // dynamically try to do:
    //    divider.setBorder (BorderFactory.createEmptyBorder ());
    try
    {
      Method setBorderMethod =
        BasicSplitPaneDivider.class.getMethod ("setBorder",
                                               new Class [] {Border.class});

      setBorderMethod.invoke
        (divider, new Object [] {BorderFactory.createEmptyBorder ()});
    } catch (NoSuchMethodException ex)
    {
      // OK, must be JDK <= 1.2.2
    } catch (Exception ex)
    {
      // not supposed to happen
      Log.internalError ("Eeek!", SwingSupport.class, ex);
    }
  }

  /**
   * Find the closest parent container for two components.
   *
   * @return The closest shared parent or null if components are not
   * in a common tree.
   */
  public static Container findCommonParent (Component component1,
                                            Component component2)
  {
    boolean foundCommonParent = false;
    Component c1 = component1;
    Container p1 = c1.getParent ();
    Component c2 = null;
    Container p2 = null;

    while (!foundCommonParent && p1 != null)
    {
      c2 = component2;
      p2 = c2.getParent ();

      while (!foundCommonParent && p2 != null)
      {
        if (p1 == p2)
          foundCommonParent = true;
        else
        {
          c2 = p2;
          p2 = c2.getParent ();
        }
      }

      if (!foundCommonParent)
      {
        c1 = p1;
        p1 = c1.getParent();
      }
    }

    if (foundCommonParent)
      return p1;                // c1 and c2 point to component1 and component2
    else
      return null;
  }


  /**
   * Set the action associated with a button.  If using JDK >= 1.3.0,
   * this will simply translate to a setAction () call on the button
   * itself.
   */
  public static void setAction (AbstractButton button, Action action)
  {
    try
    {
      // set action property on button
      Beans.setPropertyValue (button, "action", action);
    } catch (NoSuchMethodException ex)
    {
      // setAction () not available (ie JDK <= 1.2.2), use workaround
      action.addPropertyChangeListener
        (new ActionPropertySynchronizer (button, action));
      button.addActionListener (action);
    } catch (InvocationTargetException ex)
    {
      throw new Error ("Exception while in setAction (): " + ex);
    }
  }

  /**
   * Sychronizes relevant properties of an Action with an
   * AbstractButton.  Uses an (untested) weak reference trick to try
   * to auto-remove this class as a listener to the action when the
   * button is marked for GC (shamelessly ripped from Sun's
   * AbstractButton code).
   */
  private static final class ActionPropertySynchronizer
    implements PropertyChangeListener
  {
    private static ReferenceQueue buttonRefQueue;

    private Action action;
    private OwnedWeakReference buttonRef;

    public ActionPropertySynchronizer (AbstractButton button, Action action)
    {
      this.action = action;

      setButton (button);
      updateButtonAppearance ();
    }

    public Action getAction ()
    {
      return action;
    }

    public AbstractButton getButton ()
    {
      return (AbstractButton)buttonRef.get ();
    }

    /**
     * Update button's properties to match the action.
     */
    protected void updateButtonAppearance ()
    {
      AbstractButton button = getButton ();

      button.setIcon ((Icon)action.getValue (Action.SMALL_ICON));
      button.setText ((String)action.getValue (Action.NAME));
      button.setToolTipText ((String)action.getValue (Action.SHORT_DESCRIPTION));
      if (action.getValue (Action.MNEMONIC_KEY) != null)
        button.setMnemonic (((Character)action.getValue (Action.MNEMONIC_KEY)).charValue ());
      button.setEnabled (action.isEnabled ());
    }

    public void propertyChange (PropertyChangeEvent e)
    {
      updateButtonAppearance ();
    }

    public void setButton (AbstractButton button)
    {
      if (buttonRefQueue == null)
      {
        buttonRefQueue = new ReferenceQueue ();
      }

      // check if button has been marked for GC: if so remove myself
      // as a property change listener on the action.
      OwnedWeakReference r;

      while ((r = (OwnedWeakReference)buttonRefQueue.poll ()) != null)
      {
        ActionPropertySynchronizer oldSynchronizer =
          (ActionPropertySynchronizer)r.getOwner ();
        Action oldAction = oldSynchronizer.getAction ();

        if (oldAction != null)
          oldAction.removePropertyChangeListener (oldSynchronizer);
      }

      buttonRef = new OwnedWeakReference (button, buttonRefQueue, this);
    }
  }

  private static final class OwnedWeakReference extends WeakReference
  {
    private Object owner;

    public OwnedWeakReference (Object target, ReferenceQueue queue, Object owner)
    {
      super (target, queue);

      this.owner = owner;
    }

    public Object getOwner ()
    {
      return owner;
    }
  }
}
