package dsto.dfc.swing.icons;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.Serializable;

import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import dsto.dfc.logging.Log;

/**
 * An icon that is loaded from a resource.  The icon image is lazy
 * loaded to speed up application initialization.
 *
 * Unfortunately the Swing designers have implemented auto-greying of
 * icons only for ImageIcon's (by hardcoding calls to
 * {@link ImageIcon#getImage()}: see {@link AbstractButton} for an
 * example), so this class has to extend ImageIcon and override all its methods
 * for icon disabling to work. If Sun ever fix this, the 'extends ImageIcon' may
 * be removed.
 *
 * @version $Revision$
 */
public class ResourceImageIcon
  extends ImageIcon implements Icon, Serializable, ImageObserver
{
  private static final long serialVersionUID = -164868331247932275L;

  protected String resourceName;
  protected transient Image image;
  protected transient int imageWidth = -1; // -1 = not loaded
  protected transient int imageHeight = -1;
  protected transient boolean failedToLoadImage = false;

  public ResourceImageIcon (String resourceName)
  {
    this.resourceName = resourceName;
  }

  private final void checkImageLoaded ()
  {
    if (imageWidth == -1)
    {
      image = ImageLoader.getImage (resourceName);

      if (image == null)
      {
        Log.alarm ("Failed to load icon: " + resourceName, this);

        failedToLoadImage = true;

        // construct a default, red icon
        imageWidth = 16;
        imageHeight = 16;

//        image = new BufferedImage (16, 16, BufferedImage.TYPE_INT_BGR);
        image = ImageLoader.getFrame ().createImage (imageWidth, imageHeight); // better - uses platform peer
        Graphics g = ((BufferedImage) image).createGraphics ();
        g.setColor (Color.red);
        g.fillRect (0, 0, imageWidth, imageHeight);

      } else
      {
        imageWidth = image.getWidth (this);
        imageHeight = image.getHeight (this);
      }
    }
  }

  @Override
  public synchronized void paintIcon (Component c, Graphics g, int x, int y)
  {
    checkImageLoaded ();

    if (getImageObserver () == null)
      g.drawImage (image, x, y, c);
    else
      g.drawImage (image, x, y, getImageObserver ());
  }

  @Override
  public int getIconWidth ()
  {
    checkImageLoaded ();

    if (imageWidth >= 0)
      return imageWidth;
    else
      return 16;
  }

  @Override
  public int getIconHeight ()
  {
    checkImageLoaded ();

    if (imageHeight >= 0)
      return imageHeight;
    else
      return 16;
  }

  public boolean imageUpdate (Image img, int infoflags,
                              int x, int y, int width, int height)
  {
    // don't care
    return true;
  }

  // ImageIcon overrides

  @Override
  public int getImageLoadStatus ()
  {
    checkImageLoaded ();

    if (failedToLoadImage)
      return MediaTracker.ERRORED;
    else
      return MediaTracker.COMPLETE;
  }

  @Override
  public Image getImage ()
  {
    checkImageLoaded ();

    return image;
  }

  @Override
  public void setImage (Image image)
  {
    this.image = image;
  }
}
