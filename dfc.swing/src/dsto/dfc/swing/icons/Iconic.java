package dsto.dfc.swing.icons;

import javax.swing.Icon;


/**
 * Defines an object that supports iconic display (name, icon).  An
 * alternative, dynamic way to add iconic display for objects is to register
 * an {@link IconicFactory} for the object type with {@link IconicRegistry}.
 *
 * @version $Revision$
 */
public interface Iconic
{
  public static final Icon NULL_ICON = new NullIcon (16, 16);

  /**
   * The small (16x16) icon for the object.  May be null if no small
   * icon is available.
   */
   public Icon getIcon ();

  /**
   * The human-readable name for the object.
   */
   public String getName ();

  /**
   * The large (32x32) icon.
   *
   * @return The large icon, or null if no large icon is available.
   */
   public Icon getLargeIcon ();
}
