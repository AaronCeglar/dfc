package dsto.dfc.swing.icons;

import java.util.HashMap;

import javax.swing.Icon;

/**
 * A registry mapping class types to their {@link IconicFactory}'s.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class IconicRegistry
{
  private static final IconicRegistry INSTANCE = new IconicRegistry ();

  protected HashMap factories;

  public IconicRegistry ()
  {
    this.factories = new HashMap ();
  }

  /**
   * The default shared instance.
   */
  public static IconicRegistry getInstance ()
  {
    return INSTANCE;
  }

  /**
   * Register an icon that applies to all instances of a given type.
   *
   * @param type The type of value.
   * @param icon The icon to use for all instances of type.
   *
   * @see #register(Class,IconicFactory)
   */
  public void register (Class type, String name, Icon icon)
  {
    register (type, new SimpleIconic (name, icon));
  }

  /**
   * Register an iconic value that applies to all instances of a given type.
   *
   * @param type The type of value.
   * @param iconic The iconic value to use for all instances of type.
   *
   * @see #register(Class,IconicFactory)
   */
  public void register (Class type, Iconic iconic)
  {
    factories.put (type, new SimpleFactory (iconic));
  }

  /**
   * Register a factory for a given type.
   *
   * @param type The type.  Subclasses of this type are not matched.
   * @param factory The factory for the type.  May be null to clear the factory
   * for the type.
   *
   * @see #register(Class, Iconic)
   */
  public void register (Class type, IconicFactory factory)
  {
    factories.put (type, factory);
  }

  /**
   * Remove the registered factory for a given type.
   */
  public void unregister (Class type)
  {
    factories.remove (type);
  }

  /**
   * Get the factory for a given value.  This is a shortcut to get (Class):
   * only the type of the value is significant
   *
   * @param value The value.
   * @return The factory, or null if none.
   */
  public IconicFactory get (Object value)
  {
    return get (value.getClass ());
  }

  /**
   * Get the factory for a given type.
   *
   * @param type The type.
   * @return The factory, or null if none.
   */
  public IconicFactory get (Class type)
  {
    return (IconicFactory)factories.get (type);
  }

  /**
   * Get an Iconic value.  If value if Iconic, this simply returns the value,
   * otherwise an attempt to find a factory is made and the the Iconic value
   * from that is returned.
   *
   * @param value The value.
   * @return The iconic value, or null if value is not Iconic and no factory
   * is found.
   */
  public Iconic getIconicValue (Object value)
  {
    if (value == null)
    {
      return null;
    } else if (value instanceof Iconic)
    {
      return (Iconic)value;
    } else
    {
      IconicFactory factory = get (value.getClass ());

      if (factory == null)
        return null;
      else
        return factory.getIconicValue (value);
    }
  }

  private static final class SimpleFactory implements IconicFactory
  {
    private Iconic iconic;

    public SimpleFactory (Iconic iconic)
    {
      this.iconic = iconic;
    }

    public Iconic getIconicValue (Object value)
    {
      return iconic;
    }
  }
}