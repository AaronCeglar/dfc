package dsto.dfc.swing.icons;

import javax.swing.Icon;

/**
 * Simple Iconic data class.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class SimpleIconic implements Iconic
{
  private String name;
  private Icon icon;
  private Icon largeIcon;

  public SimpleIconic (String name)
  {
    this (name, null);
  }

  public SimpleIconic (String name, Icon icon)
  {
    this (name, icon, null);
  }

  public SimpleIconic (String name, Icon icon, Icon largeIcon)
  {
    this.name = name;
    this.icon = icon;
    this.largeIcon = largeIcon;
  }

  public String getName ()
  {
    return name;
  }

  public Icon getIcon ()
  {
    return icon;
  }

  public Icon getLargeIcon ()
  {
    return largeIcon;
  }

  public String toString ()
  {
    return name;
  }
}