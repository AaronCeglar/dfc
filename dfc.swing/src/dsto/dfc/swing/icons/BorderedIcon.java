package dsto.dfc.swing.icons;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Panel;

import javax.swing.Icon;
import javax.swing.border.Border;

/**
 * An icon that displays a border around an encapsulated icon.
 *
 * @version $Revision$
 */
public class BorderedIcon implements Icon
{
  Icon icon;
  Border border;
  Insets insets;

  static final Panel dummyComponent = new Panel ();

  public BorderedIcon (Icon icon, Border border)
  {
    this.icon = icon;
    this.border = border;
    this.insets = border.getBorderInsets (dummyComponent);
  }

  public Icon getIcon ()
  {
    return icon;
  }

  public void setIcon (Icon icon)
  {
    this.icon = icon;
  }

  public void paintIcon (Component c, Graphics g, int x, int y)
  {
    border.paintBorder (c, g, x, y, getIconWidth (), getIconHeight ());
    icon.paintIcon (c, g, x + insets.left, y + insets.top);
  }

  public int getIconWidth ()
  {
    return icon.getIconWidth () + insets.left + insets.right;
  }

  public int getIconHeight ()
  {
    return icon.getIconHeight () + insets.top + insets.bottom;
  }

  public void setBorder (Border newBorder)
  {
    this.border = newBorder;
    
    this.insets = border.getBorderInsets (dummyComponent);
  }
}
