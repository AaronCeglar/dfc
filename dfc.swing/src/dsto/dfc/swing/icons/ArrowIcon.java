package dsto.dfc.swing.icons;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.io.Serializable;

import javax.swing.Icon;

/**
 * A dynamically drawn auto-scaling arrow icon.
 *
 * @author  Matthew Phillips
 * @version $Revision$
 */
public class ArrowIcon
  implements Icon, Serializable
{
  public static final int UP = 0;
  public static final int DOWN = 1;
  public static final int LEFT = 2;
  public static final int RIGHT = 3;

  protected int direction;
  protected int width;
  protected int height;
  protected Color color;

  /**
   * Creates a new icon with the arrow pointing in the direction
   * specified.
   *
   * @param direction The direction to draw the arrow.
   */
  public ArrowIcon (int direction, int height)
  {
    this.direction = direction;
    this.color = null;
    setIconHeight (height);
  }

  public ArrowIcon (int direction, int height, Color color)
  {
    this.direction = direction;
    this.color = color;
    setIconHeight (height);
  }

  public void setColor (Color color)
  {
    this.color = color;
  }

  public Color getColor ()
  {
    return color;
  }

  public static int getHeightForWidth (int width)
  {
    return (width + 1) / 2;
  }

  /**
   * Draws the icon.
   */
  public void paintIcon (Component component, Graphics graphics,
                         int x, int y)
  {
    int xoffset = (width / 2) + x;

    graphics.setColor (color == null ? component.getForeground () : color);

    if (direction == UP)
    {
      for (int i = 0; i < height; i++)
      {
        graphics.drawLine (xoffset - i, i + y,
                           xoffset + i, i + y);
      }
    } else if (direction == DOWN)
    {
      int yoffset = height - 1 + y;
      for (int i = 0; i < height; i++)
      {
        graphics.drawLine (xoffset - i, yoffset - i,
                           xoffset + i, yoffset - i);
      }
    } else if (direction == LEFT)
    {
      xoffset = (width / 2) + x - 4;
      int yoffset = height - 4 + y;
      for (int i = 0; i < height; i++)
      {
        graphics.drawLine (xoffset + i, yoffset + i,
                           xoffset + i, yoffset - i);
      }
    } else
    {
      xoffset = (width / 2) + x + 2;
      int yoffset = height - 4 + y;
      for (int i = 0; i < height; i++)
      {
        graphics.drawLine (xoffset - i, yoffset + i,
                           xoffset - i, yoffset - i);
      }
    }
  }

  public int getIconWidth () {return width;}

  public int getIconHeight () {return height;}

  public int getDirection () {return direction;}

  public void setIconHeight (int height)
  {
    this.height = height;
    this.width = height * 2 - 1;
  }
}
