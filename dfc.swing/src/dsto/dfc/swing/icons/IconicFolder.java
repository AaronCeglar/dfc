package dsto.dfc.swing.icons;

import javax.swing.Icon;

/**
 * An iconic object that is also a container of some sort (such as a
 * folder or group) that supports different icons depending on whether
 * it is logically 'open' or 'closed'.
 *
 * @version $Revision$
 */
public interface IconicFolder extends Iconic
{
  /**
   * The normal (16x16) icon to use when folder is displayed as open.
   * The closed icon is assumed to be the value of getIcon ().
	 */
	public Icon getOpenIcon ();

  /**
   * The large (32x32) icon to use when folder is displayed as open.
   * The closed icon is assumed to be the value of getLargeIcon ().
	 */
  public Icon getLargeOpenIcon ();
}
