package dsto.dfc.swing.icons;

import java.awt.Component;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 * This is used if you want to display a group of icons one after the
 * other horizontally across, and also want it treated as an Icon so
 * that it may be used on many type of components, including buttons,
 * and labels.
 *
 * @version $Revision$
 */
public class FlowIcon implements Icon
{
  private static final int DEFAULT_ICON_GAP = 2;

  protected ArrayList icons = new ArrayList ();
  protected int width = 0;
  protected int height = 0;
  protected int gap = DEFAULT_ICON_GAP;

  public FlowIcon ()
  {
    this (DEFAULT_ICON_GAP);
  }

  public FlowIcon (int gap)
  {
    this.gap = gap;
  }

  public FlowIcon (Icon icon1, Icon icon2)
  {
    addIcon (icon1);
    addIcon (icon2);
  }

  public void setGap (int newGap)
  {
    gap = newGap;
    width = height = -1;
  }

  public int getGap ()
  {
    return gap;
  }

  public void addIcon (Icon icon)
  {
    icons.add (icon);
    width = -1;
    height = -1;
  }

  public void removeIcon (Icon icon)
  {
    icons.remove (icon);
    width = -1;
    height = -1;
  }

  /**
   * Removes all the icons. ie when asked to paint, nothing will be painted.
   */
  public void removeAllIcons ()
  {
    icons.clear();
    width = 0;
    height = 0;
  }

  public void paintIcon (Component c, Graphics g, int x, int y)
  {
    int xoffset=0;

    for (int i = 0; i < icons.size (); i++)
    {
      Icon icon = (Icon)icons.get (i);

      icon.paintIcon (c, g, x + xoffset, y);
      // We add 2 here so that the icons aren't right next to each other.
      // Maybe we should make this a setable value.....
      xoffset += icon.getIconWidth () + gap;
    }
  }

  public int getIconWidth ()
  {
    if (width == -1)
    {
      width = 0;

      for (int i = 0; i < icons.size (); i++)
      {
        Icon icon = (Icon)icons.get (i);

        width += icon.getIconWidth();
      }

      // add gaps
      width += Math.max (0, gap * (icons.size () - 1));
    }

    return width;
  }

  public int getIconHeight ()
  {
    if (height == -1)
    {
      height = 0;

      for (int i = 0; i < icons.size (); i++)
      {
        Icon icon = (Icon)icons.get (i);

        height = Math.max (height, icon.getIconHeight ());
      }
    }

    return height;
  }

  /**
   * This is so that you easily test this class. Just run it.
   */
  public static void main (String [] args)
  {
    FlowIcon flowIcon = new FlowIcon();
    flowIcon.addIcon(ImageLoader.getIcon("/dsto/dfc/icons/file_open.gif"));
    flowIcon.addIcon(ImageLoader.getIcon("/dsto/dfc/icons/file_new.gif"));
    flowIcon.addIcon(ImageLoader.getIcon("/dsto/dfc/icons/delete.gif"));

    JButton button = new JButton(flowIcon);
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.getContentPane().add(button);
    frame.setVisible (true);
  }
}
