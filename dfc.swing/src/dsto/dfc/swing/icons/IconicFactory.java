package dsto.dfc.swing.icons;


/**
 * Defines an object that can supply {@link Iconic} representations for
 * objects.
 *
 * @author Matthew Phillips
 * @version $Revision$
 *
 * @see IconicRegistry
 */
public interface IconicFactory
{
  /**
   * Get the equivalent Iconic value for a given value.
   */
  public Iconic getIconicValue (Object value);
}