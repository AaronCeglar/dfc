package dsto.dfc.swing.icons;

import java.awt.Component;
import java.awt.Graphics;
import java.io.Serializable;

import javax.swing.Icon;

/**
 * An icon that displays nothing.  Useful for filling in blank areas.
 *
 * @version $Revision$
 */
public class NullIcon implements Icon, Serializable
{
  protected int width;
  protected int height;

  public NullIcon (int width, int height)
  {
    this.width = width;
    this.height = height;
  }

  public void paintIcon (Component c, Graphics g, int x, int y)
  {
    // zip
  }

  public int getIconWidth ()
  {
    return width;
  }

  public int getIconHeight ()
  {
    return height;
  }
}
