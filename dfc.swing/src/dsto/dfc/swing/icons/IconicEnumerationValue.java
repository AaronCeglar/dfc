package dsto.dfc.swing.icons;

import java.io.Serializable;

import javax.swing.Icon;

import dsto.dfc.util.EnumerationValue;
import dsto.dfc.util.StringEnumerationValue;

/**
 * Base class for enumerated types that have an iconic representation.
 *
 * @version $Revision$
 */
public abstract class IconicEnumerationValue
  extends StringEnumerationValue implements Iconic, Serializable
{
  private static final long serialVersionUID = 552967879101787413L;

  /** Icon is marked transient on the assumption that StringEnumerationValue's
    * readResolve () will do a resolution to the singleton instance in
    * getEnumValues ().
    */
  protected transient Icon icon;

  protected IconicEnumerationValue ()
  {
    this ("", (Icon)null);
  }

  /**
   * Create new instance with a null icon.
   *
   * @param name The logical name of the this value.
   */
  public IconicEnumerationValue (String name)
  {
    this (name, (Icon)null);
  }

  /**
   * Create an instance with associated string and icon
   * representation.
   *
   * @param name The logical name of the this value.
   * @param iconRes The resource name of the icon to associate with
   * this value.
   */
  public IconicEnumerationValue (String name, String iconRes)
  {
    this (name, ImageLoader.getIcon (iconRes));
  }

  /**
   * Create an instance with associated string and icon
   * representation.
   *
   * @param name The logical name of this value.
   * @param icon The icon to associate with this value.
   */
  public IconicEnumerationValue (String name, Icon icon)
  {
    super (name);
    this.icon = icon;
  }

  /**
   * Create an instance with associated string and icon
   * representation.
   *
   * @param name The logical name of this value.
   * @param text The human readable text for the value.
   * @param icon The icon to associate with this value.
   */
  public IconicEnumerationValue (String name, String text, Icon icon)
  {
    super (name, text);

    this.icon = icon;
  }

  public IconicEnumerationValue (String name, String text, String iconRes)
  {
    this (name, text, ImageLoader.getIcon (iconRes));
  }

  public abstract EnumerationValue [] getEnumValues ();

  public Icon getIcon ()
  {
    return icon;
  }

  public Icon getLargeIcon ()
  {
    return null;
  }
}
