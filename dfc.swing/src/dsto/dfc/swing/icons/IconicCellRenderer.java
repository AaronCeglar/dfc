package dsto.dfc.swing.icons;

import javax.swing.JComponent;
import javax.swing.JLabel;

import dsto.dfc.swing.controls.AbstractCellRenderer;
import dsto.dfc.util.EnumerationValue;

/**
 * Cell renderer for Iconic values.
 *
 * @version $Revision$
 */
public class IconicCellRenderer extends AbstractCellRenderer
{
  public JComponent getRendererComponent (JComponent client,
                                          Object value,
                                          int row, int column,
                                          boolean isSelected,
                                          boolean hasFocus,
                                          boolean expanded,
                                          boolean leaf)
  {
    JLabel label = getLabel ();
    Iconic iconicValue = IconicRegistry.getInstance ().getIconicValue (value);

    if (iconicValue != null)
    {
      label.setIcon (iconicValue.getIcon ());

      // EnumerationValue getName () and Iconic getName () aren't really the same
      // thing (one is an ID, the other is a human readable name). Use
      // toString () for EnumerationValue's "name"
      if (iconicValue instanceof EnumerationValue)
        label.setText (iconicValue.toString ());
      else
        label.setText (iconicValue.getName ());
    } else
    {
      label.setText (value != null ? value.toString () : null);
      label.setIcon (null);
    }

    return label;
  }
}
