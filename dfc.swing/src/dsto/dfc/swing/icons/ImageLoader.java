package dsto.dfc.swing.icons;

import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.Window;
import java.io.File;
import java.net.URL;

import javax.swing.Icon;

import dsto.dfc.logging.Log;

/**
 * Provides image and icon creation services.
 * 
 * @version $Revision$
 */
public final class ImageLoader
{
  private static final Component DUMMY_COMPONENT = new Container();

  private static Frame dummyWindowFrame = null;

  private static Window dummyWindow = null;

  private ImageLoader()
  {
    // cannot be instantiated
  }

  public static Icon getIcon(String resourceName)
  {
    return new ResourceImageIcon(resourceName);
  }

  public static Image getImage(URL url, Toolkit tk)
  {
    Image image = null;

    try
    {
      image = tk.getImage(url);
      if (image != null)
        waitForImage(image);
    }
    catch (Exception e)
    {
      Log.warn("Exception while loading image '" + url.toString() + "'",
          ImageLoader.class, e);
    }

    return image;
  }

  public static Image getImage(String imageResourceName, Toolkit tk)
  {
    java.net.URL url = ImageLoader.class.getResource(imageResourceName);

    Image image = null;

    if (url == null)
    {
      // resource not found, try to use image name without path as fallback
      // for JBuilder 3.0 deployment bug where resources from other projects
      // are imported with no package path.

      File file = new File(imageResourceName);

      url = ImageLoader.class.getResource("/" + file.getName());
    }

    if (url != null)
    {
      image = getImage(url, tk);

      if (image == null)
        Log.alarm("Failed to load image from resource '" + imageResourceName
            + "'", ImageLoader.class);
    }
    else
      Log.alarm("No image resource named '" + imageResourceName + "'",
          ImageLoader.class);

    return image;
  }

  public static Image getImage(String imageResourceName)
  {
    return getImage(imageResourceName, java.awt.Toolkit.getDefaultToolkit());
  }

  public static Image getImage(String imageResourceName, Component comp)
  {
    return getImage(imageResourceName, comp.getToolkit());
  }

  public static boolean waitForImage(Image image)
  {
    if (image == null)
      return false;
    if (image.getWidth(null) > 0)
      return true;

    MediaTracker m = new MediaTracker(DUMMY_COMPONENT);

    m.addImage(image, 1);

    try
    {
      m.waitForID(1);
    }
    catch (InterruptedException e)
    {
      e.printStackTrace();
    }

    return !m.isErrorID(1);
  }

  /**
   * For those times when you just need a graphics context and don't care where
   * it came from.
   * 
   * @return A new graphics context. Actually drawing on it will have zero
   *         effect, but it may be used for other things such as retrieving font
   *         info, etc.
   */
  public static Graphics createGraphics()
  {
    if (dummyWindow == null)
    {
      // create an offscreen dummy frame and window

      if (dummyWindowFrame == null)
        dummyWindowFrame = new Frame();

      dummyWindow = new Window(dummyWindowFrame);

      dummyWindow.setBounds(-10, -10, 1, 1);
      dummyWindow.setVisible(true);
    }

    return dummyWindow.getGraphics();
  }

  /**
   * For those times that you need a Frame and don't care where it came from.
   * 
   * @return A frame. Do not even think of making it visible, moving it, etc.
   */
  public static Frame getFrame()
  {
    if (dummyWindowFrame == null)
      dummyWindowFrame = new Frame();

    return dummyWindowFrame;
  }
}
