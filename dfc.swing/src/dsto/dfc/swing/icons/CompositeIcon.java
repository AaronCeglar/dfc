package dsto.dfc.swing.icons;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.Icon;

import dsto.dfc.swing.controls.Geometry;

/**
 * A composite icon made by overlaying one icon over another.
 *
 * @version $Revision$
 */
public class CompositeIcon implements Icon
{
  public static final int TOP_LEFT = Geometry.TOP_LEFT;
  public static final int MID_TOP = Geometry.MID_TOP;
  public static final int TOP_RIGHT = Geometry.TOP_RIGHT;
  public static final int MID_RIGHT = Geometry.MID_RIGHT;
  public static final int BOTTOM_RIGHT = Geometry.BOTTOM_RIGHT;
  public static final int MID_BOTTOM = Geometry.MID_BOTTOM;
  public static final int BOTTOM_LEFT = Geometry.BOTTOM_LEFT;
  public static final int MID_LEFT = Geometry.MID_LEFT;
  public static final int CENTER = Geometry.CENTER;

  protected Icon icon1;
  protected Icon icon2;
  protected int alignment;

  /**
   * Make an icon that consists of one icon overlayed on another.
   *
   * @param icon1 The lower icon.
   * @param icon2 The upper icon.
   * @param alignment The alignment of the lower icon relative to the
   * upper. One of the TOP_LEFT, TOP_RIGHT, CENTER, etc constants.
   */
  public CompositeIcon (Icon icon1, Icon icon2, int alignment)
  {
    this.icon1 = icon1;
    this.icon2 = icon2;
    this.alignment = alignment;
  }

  public Icon getIcon1 ()
  {
    return icon1;
  }

  public void setIcon1 (Icon newValue)
  {
    icon1 = newValue;
  }

  public Icon getIcon2 ()
  {
    return icon2;
  }

  public void setIcon2 (Icon newValue)
  {
    icon2 = newValue;
  }

  public void paintIcon (Component c, Graphics g, int x, int y)
  {
    Dimension icon1Size = new Dimension (icon1.getIconWidth (), icon1.getIconHeight ());
    Dimension icon2Size = new Dimension (icon2.getIconWidth (), icon2.getIconHeight ());

    Point icon2Offset =
      Geometry.offsetForAligment (icon1Size, icon2Size, alignment);

    icon1.paintIcon (c, g, x, y);
    icon2.paintIcon (c, g, x + icon2Offset.x, y + icon2Offset.y);
  }

  public int getIconWidth ()
  {
    return Math.max (icon1.getIconWidth (), icon2.getIconWidth ());
  }

  public int getIconHeight ()
  {
    return Math.max (icon1.getIconHeight (), icon2.getIconHeight ());
  }
}
