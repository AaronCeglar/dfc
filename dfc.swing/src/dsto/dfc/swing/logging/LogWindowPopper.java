package dsto.dfc.swing.logging;

import java.awt.Component;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Iterator;

import javax.swing.JDialog;

import dsto.dfc.collections.CollectionEvent;
import dsto.dfc.collections.CollectionListener;
import dsto.dfc.logging.Log;
import dsto.dfc.logging.LogEvent;
import dsto.dfc.logging.LogMessageBuffer;
import dsto.dfc.util.BasicPropertyEventSource;

/**
 * Manages auto popup of the log window when selected types of log
 * events occur.
 *
 * @author Matthew Phillips
 * @version $Revision$
 *
 * @see LogWindowPopperEditor
 * @see CmdShowLogWindow
 */
public class LogWindowPopper
  extends BasicPropertyEventSource implements CollectionListener, WindowListener
{
  private static final int WARNING = 0;
  private static final int ALARM = 1;
  private static final int OTHER = 2;
  private static final int INFO = 3;

  private Component client;
  private LogWindow window;
  private LogMessageBuffer messageBuffer;
  private boolean popupOnAlarm = true;
  private boolean popupOnWarning = true;
  private boolean popupOnInfo = false;
  private boolean popupOnOther = false;
  private boolean disposeOnClose = false;

  public LogWindowPopper (Component client)
  {
    this (client, new LogMessageBuffer ());
  }

  /**
   * Create a new instance that will display the log window showing a
   * buffer of messages.
   *
   * @param client The client for the window (may be null).
   * @param messageBuffer The message buffer to display.
   */
  public LogWindowPopper (Component client,
                          LogMessageBuffer messageBuffer)
  {
    this.client = client;
    this.messageBuffer = messageBuffer;

    messageBuffer.addCollectionListener (this);
  }

  /**
   * Set true if log window should be disposed when closed and re-created next
   * use.
   */
  public void setDisposeOnClose (boolean newDisposeOnClose)
  {
    disposeOnClose = newDisposeOnClose;
  }

  public boolean isDisposeOnClose ()
  {
    return disposeOnClose;
  }

  /**
   * Force the log window to be visible, if not already.
   */
  public void showLogWindow ()
  {
    ensureWindowCreated ();

    window.setVisible (true);
    window.toFront ();
    window.requestFocus ();
  }

  /**
   * Get the log window if created, null otherwise.
   *
   * @see #ensureWindowCreated
   */
  public LogWindow getLogWindow ()
  {
    return window;
  }

  /**
   * Get the buffer that the window is displaying.
   */
  public LogMessageBuffer getBuffer ()
  {
    return messageBuffer;
  }
  
  public void ensureWindowCreated ()
  {
    if (window == null)
    {
      window = new LogWindow (client, messageBuffer, false);

      if (disposeOnClose)
      {
        window.setDefaultCloseOperation (JDialog.DISPOSE_ON_CLOSE);
        window.addWindowListener (this);
      } else
      {
        window.setDefaultCloseOperation (JDialog.HIDE_ON_CLOSE);
      }

      window.setPopper (this);
    }
  }

  public void setPopupOnAlarm (boolean newValue)
  {
    boolean oldValue = popupOnAlarm;

    popupOnAlarm = newValue;

    firePropertyChange ("popupOnAlarm", oldValue, newValue);
  }

  public boolean isPopupOnAlarm ()
  {
    return popupOnAlarm;
  }

  public void setPopupOnWarning (boolean newPopupOnWarning)
  {
    boolean oldValue = popupOnWarning;

    popupOnWarning = newPopupOnWarning;

    firePropertyChange ("popupOnWarning", oldValue, newPopupOnWarning);
  }

  public boolean isPopupOnWarning ()
  {
    return popupOnWarning;
  }

  public void setPopupOnInfo (boolean newPopupOnInfo)
  {
    boolean oldValue = popupOnInfo;

    popupOnInfo = newPopupOnInfo;

    firePropertyChange ("popupOnInfo", oldValue, newPopupOnInfo);
  }

  public boolean isPopupOnInfo ()
  {
    return popupOnInfo;
  }

  public void setPopupOnOther (boolean newPopupOnOther)
  {
    boolean oldValue = popupOnOther;

    popupOnOther = newPopupOnOther;

    firePropertyChange ("popupOnOther", oldValue, newPopupOnOther);
  }

  public boolean isPopupOnOther ()
  {
    return popupOnOther;
  }

  protected static int getEventClass (int eventType)
  {
    switch (eventType)
    {
      case Log.ALARM:
      case Log.INTERNAL_ERROR:
        return ALARM;
      case Log.WARNING:
        return WARNING;
      case Log.INFO:
        return INFO;
      default:
        return OTHER;
    }
  }

  // CollectionListener interface (listening to log message buffer)

  public void elementsAdded (CollectionEvent e)
  {
    for (Iterator i = e.getElements ().iterator (); i.hasNext (); )
    {
      LogEvent event = (LogEvent)i.next ();
      int eventClass = getEventClass (event.getType ());

      if (popupOnAlarm && eventClass == ALARM ||
          popupOnWarning && eventClass == WARNING ||
          popupOnInfo && eventClass == INFO ||
          popupOnOther && eventClass == OTHER)
      {
        boolean windowWasShowing = window != null && window.isVisible ();

        /** @todo check whether this is swing thread before doing this */
        showLogWindow ();

        // if window just popped up, select the event that caused it
        if (!windowWasShowing)
          window.setSelectedEvent (event);

        break;
      }
    }
  }

  protected void handleClose ()
  {
    if (disposeOnClose)
    {
      window.removeWindowListener (this);
      window = null;
    }
  }

  // Window listener

  public void elementsRemoved (CollectionEvent e)
  {
    // zip
  }

  public void windowOpened (WindowEvent e)
  {
    // zip
  }

  public void windowClosing (WindowEvent e)
  {
    handleClose ();
  }

  public void windowClosed (WindowEvent e)
  {
    handleClose ();
  }

  public void windowIconified (WindowEvent e)
  {
    // zip
  }

  public void windowDeiconified (WindowEvent e)
  {
    // zip
  }

  public void windowActivated (WindowEvent e)
  {
    // zip
  }

  public void windowDeactivated (WindowEvent e)
  {
    // zip
  }
}
