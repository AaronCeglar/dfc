package dsto.dfc.swing.logging;

import java.awt.Color;
import java.awt.SystemColor;
import java.util.Iterator;

import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Element;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import dsto.dfc.collections.CollectionEvent;
import dsto.dfc.collections.CollectionListener;
import dsto.dfc.logging.Log;
import dsto.dfc.logging.LogEvent;
import dsto.dfc.logging.LogMessageBuffer;

/**
 * Displays log messages in a formatted text pane.
 *
 * @version $Revision$
 */
public class LogPanel extends JTextPane
{
  protected static final MutableAttributeSet DATE_ATTR = new SimpleAttributeSet ();
  protected static final MutableAttributeSet ERROR_TYPE_ATTR = new SimpleAttributeSet ();
  protected static final MutableAttributeSet WARNING_TYPE_ATTR = new SimpleAttributeSet ();
  protected static final MutableAttributeSet NORMAL_TYPE_ATTR = new SimpleAttributeSet ();
  protected static final MutableAttributeSet MESSAGE_ATTR = new SimpleAttributeSet ();
  protected static final AttributeSet NORMAL_ATTR = SimpleAttributeSet.EMPTY;
  
  protected LogMessageBuffer messageBuffer;
  protected DefaultStyledDocument document = new DefaultStyledDocument ();

  static
  {
    StyleConstants.setForeground (DATE_ATTR, Color.black);
    StyleConstants.setForeground (ERROR_TYPE_ATTR, Color.red);
    StyleConstants.setBold (ERROR_TYPE_ATTR, true);
    StyleConstants.setForeground (WARNING_TYPE_ATTR, Color.magenta);
    StyleConstants.setBold (WARNING_TYPE_ATTR, true);
    StyleConstants.setForeground (NORMAL_TYPE_ATTR, Color.black);
    StyleConstants.setBold (NORMAL_TYPE_ATTR, true);
  }
  
  public LogPanel ()
  {
    this (new LogMessageBuffer ());
  }

  public LogPanel (LogMessageBuffer messageBuffer)
  {
    this.messageBuffer = messageBuffer;

    messageBuffer.addCollectionListener (new BufferListener ());
    
    try 
    {
      jbInit();
    } catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * Add a log event to the text pane.
   */
  protected void addEvent (final LogEvent event)
  {
    if (SwingUtilities.isEventDispatchThread ())
    {
      try
      {
        // add the date
        document.insertString (document.getLength (),
                               Log.getDateString (event.getTime ()), DATE_ATTR);
        document.insertString (document.getLength (), ": ", NORMAL_ATTR);
        

        // pick text attribute for event type
        AttributeSet typeAttr;
        
        switch (event.getType ())
        {
        case Log.ALARM:
        case Log.INTERNAL_ERROR:
          typeAttr = ERROR_TYPE_ATTR;
          break;
        case Log.WARNING:
          typeAttr = WARNING_TYPE_ATTR;
          break;
        default:
          typeAttr = NORMAL_TYPE_ATTR;
        }

        // add event string
        document.insertString (document.getLength (),
                             Log.getTypeString (event.getType ()), typeAttr);
        document.insertString (document.getLength (), ": ", NORMAL_ATTR);

        // display message
        document.insertString (document.getLength (),
                               event.getMessage (), MESSAGE_ATTR);

        // display exception
        if (event.getException () != null)
        {
          document.insertString (document.getLength (), ": ", NORMAL_ATTR);
          document.insertString (document.getLength (), "exception", NORMAL_TYPE_ATTR);
          document.insertString (document.getLength (), ": ", NORMAL_ATTR);
          document.insertString (document.getLength (),
                                 event.getException ().getMessage (), NORMAL_ATTR);
        }
        
        // add newline
        document.insertString (document.getLength (), "\n", MESSAGE_ATTR);
      } catch (BadLocationException ex)
      {
        ex.printStackTrace ();
      }
    } else
    {
      SwingUtilities.invokeLater (new Runnable ()
      {
        public void run ()
        {
          addEvent (event);
        }
      });
    }
  }

  /**
   * Remove the text for an event from the text pane.
   */
  protected void removeEvent (final int index)
  {
    if (SwingUtilities.isEventDispatchThread ())
    {
      // element = paragraph for index
      Element element = document.getDefaultRootElement ().getElement (index);

      int startIndex = element.getStartOffset ();
      int endIndex = element.getEndOffset ();

      try
      {
        document.remove (startIndex, endIndex - startIndex);
      } catch (BadLocationException ex)
      {
        ex.printStackTrace ();
      }
    } else
    {
      SwingUtilities.invokeLater (new Runnable ()
      {
        public void run ()
        {
          removeEvent (index);
        }
      });
    }
  }

  private void jbInit () throws Exception
  {
    this.setDocument(document);
    this.setSelectionColor(SystemColor.textHighlight);
    this.setEditable(false);
  }

  class BufferListener implements CollectionListener
  {
    public void elementsAdded (CollectionEvent e)
    {
      for (Iterator i = e.getElements ().iterator (); i.hasNext (); )
        addEvent ((LogEvent)i.next ());
    }

    public void elementsRemoved (CollectionEvent e)
    {
      for (int index = e.getEndIndex (); index >= e.getStartIndex (); index--)
        removeEvent (index);
    }
  }
}
