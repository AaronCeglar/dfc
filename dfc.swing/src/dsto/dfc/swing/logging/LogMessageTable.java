package dsto.dfc.swing.logging;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.util.Date;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import dsto.dfc.logging.Log;
import dsto.dfc.logging.LogEvent;
import dsto.dfc.logging.LogMessageBuffer;
import dsto.dfc.swing.icons.ImageLoader;
import dsto.dfc.swing.icons.NullIcon;
import dsto.dfc.swing.table.DfcTable;
import dsto.dfc.util.Disposable;

/**
 * Display a LogMessageBuffer in a table view.
 *
 * @see LogMessageTableModel
 *
 * @author Matthew
 * @version $Revision$
 */
public class LogMessageTable extends DfcTable implements Disposable
{
  protected static final Icon INFO_ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/info16.gif");
  protected static final Icon ALARM_ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/stop16.gif");
  protected static final Icon WARN_ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/exclamation16.gif");
  protected static final Icon NO_ICON = new NullIcon (16, 16);

  BorderLayout borderLayout1 = new BorderLayout ();

  public LogMessageTable ()
  {
    this (new LogMessageBuffer ());
  }

  public void dispose ()
  {
    ((LogMessageTableModel)getModel ()).dispose ();
  }

  public LogMessageTable (LogMessageBuffer messages)
  {
    super ();

    setMessages (messages);

    try
    {
      jbInit ();
    } catch(Exception ex)
    {
      ex.printStackTrace ();
    }
  }

  public void setMessages (LogMessageBuffer newBuffer)
  {
    setModel (new LogMessageTableModel (newBuffer));

    TableColumn column = getColumn ("Type");
    column.setMinWidth (80);
    column.setCellRenderer (new TypeCellRenderer ());
    column = getColumn ("Time");
    column.setCellRenderer (new DateCellRenderer ());

    column = getColumn ("Message");
    column.setMinWidth (180);

    sizeColumnsToFit ();
  }

  public LogMessageBuffer getMessages ()
  {
    return ((LogMessageTableModel)getModel ()).getMessages ();
  }

  public static Color getColorForEventType (int type)
  {
    switch (type)
    {
    case Log.ALARM: return Color.red;
    case Log.INTERNAL_ERROR: return Color.red;
    case Log.WARNING: return Color.magenta;
    case Log.TRACE: return Color.blue;
    case Log.INFO: return Color.blue;
    case Log.DIAGNOSTIC: return Color.green;
    default: return Color.black;
    }
  }

  public static Icon getIconForEvent (LogEvent event)
  {
    switch (event.getType ())
    {
      case Log.ALARM:
      case Log.INTERNAL_ERROR:
        return ALARM_ICON;
      case Log.WARNING:
        return WARN_ICON;
      case Log.INFO:
      case Log.TRACE:
      case Log.DIAGNOSTIC:
        return INFO_ICON;
      default:
        return null;
    }
  }

  private void jbInit () throws Exception
  {
    setShowGrid (false);
    setAutoResizeMode (AUTO_RESIZE_OFF);
    setRowMargin (3);
    setRowHeight (20);

    this.setLayout (borderLayout1);
  }

  class TypeCellRenderer extends DefaultTableCellRenderer
  {
    public Component getTableCellRendererComponent
      (JTable table, Object value,
       boolean selected, boolean hasFocus, int row, int column)
    {
      super.getTableCellRendererComponent
        (table, value, selected, hasFocus, row, column);

      LogEvent event = getMessages ().getEvent (row);

      Icon icon = getIconForEvent (event);

      if (icon == null)
        icon = NO_ICON;

      setIcon (icon);

      return this;
    }
  }

  class DateCellRenderer extends DefaultTableCellRenderer
  {
    public Component getTableCellRendererComponent
      (JTable table, Object value,
       boolean selected, boolean hasFocus, int row, int column)
    {
      super.getTableCellRendererComponent
        (table, value, selected, hasFocus, row, column);

      setHorizontalAlignment (JLabel.RIGHT);
      setText (Log.getDateString ((Date)value));

      return this;
    }
  }
}
