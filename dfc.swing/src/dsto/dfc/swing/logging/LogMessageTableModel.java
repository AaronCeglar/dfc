package dsto.dfc.swing.logging;

import java.util.Date;

import javax.swing.table.AbstractTableModel;

import dsto.dfc.collections.CollectionEvent;
import dsto.dfc.collections.CollectionListener;
import dsto.dfc.logging.Log;
import dsto.dfc.logging.LogEvent;
import dsto.dfc.logging.LogMessageBuffer;
import dsto.dfc.util.Disposable;

/**
 * Adapts a LogMessageBuffer to the Swing TableModel interface.
 *
 * @see LogMessageTable
 *
 * @author Matthew
 * @version $Revision$
 */
public class LogMessageTableModel
  extends AbstractTableModel implements CollectionListener, Disposable
{
  private LogMessageBuffer messages;

  public LogMessageTableModel ()
  {
    this (new LogMessageBuffer ());
  }

  public LogMessageTableModel (LogMessageBuffer messages)
  {
    this.messages = messages;

    messages.addCollectionListener (this);
  }

  public void dispose ()
  {
    messages.removeCollectionListener (this);
  }

  public LogMessageBuffer getMessages ()
  {
    return messages;
  }

  // TableModel interface

  public int getRowCount ()
  {
    return messages.getEventCount ();
  }

  public int getColumnCount ()
  {
    return 4;
  }

  public String getColumnName (int columnIndex)
  {
    switch (columnIndex)
    {
    case 0: return "Time";
    case 1: return "Type";
    case 2: return "Message";
    case 3: return "Source";
    default: return null;
    }
  }

  public Class getColumnClass (int columnIndex)
  {
    switch (columnIndex)
    {
    case 0: return Date.class;
    case 1: return String.class;
    case 2: return String.class;
    case 3: return String.class;
    default: return null;
    }
  }

  public Object getValueAt (int rowIndex, int columnIndex)
  {
    LogEvent event = messages.getEvent (rowIndex);

    switch (columnIndex)
    {
    case 0: return event.getTime ();
    case 1: return Log.getTypeString (event.getType ());
    case 2: return event.getMessage ();
    case 3:
      Object source = event.getSource ();

      if (source instanceof Class)
        return ((Class)source).getName ();
      else
        return event.getSource ().getClass ().getName ();
    default: return null;
    }
  }

  // CollectionListener interface

  public void elementsAdded (CollectionEvent e)
  {
    fireTableRowsInserted (e.getStartIndex (), e.getEndIndex ());
  }

  public void elementsRemoved (CollectionEvent e)
  {
    fireTableRowsDeleted (e.getStartIndex (), e.getEndIndex ());
  }
}
