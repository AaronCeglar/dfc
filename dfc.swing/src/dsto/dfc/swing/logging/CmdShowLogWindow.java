package dsto.dfc.swing.logging;

import dsto.dfc.swing.commands.AbstractCommand;
import dsto.dfc.swing.commands.CommandView;

/**
 * Shows the log window managed by a LogWindowPopper instance.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class CmdShowLogWindow extends AbstractCommand
{
  private LogWindowPopper popper;

  public CmdShowLogWindow (LogWindowPopper popper)
  {
    this.popper = popper;
  }

  public void execute ()
  {
    popper.showLogWindow ();
  }

  public String getName ()
  {
    return "log.Show Log";
  }

  public String getDescription ()
  {
    return "Show the log window";
  }

  public boolean isInteractive ()
  {
    return true;
  }

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return "Help.logging";
    else if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return "logging";
    else if (viewName.equals (CommandView.TOOLBAR_VIEW))
      return "logging";
    else
      return null;
  }

  public char getMnemonic ()
  {
    return 'l';
  }
}
