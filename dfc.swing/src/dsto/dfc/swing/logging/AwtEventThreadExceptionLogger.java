package dsto.dfc.swing.logging;

import dsto.dfc.logging.Log;

/**
 * Sends unhandled exceptions in the AWT event loop to the system log.
 *
 * @author Matthew Phillips
 * @version $Revision$
 *
 */
public final class AwtEventThreadExceptionLogger
{
  public void handle (Throwable exception)
  {
    Log.alarm ("Unhandled exception while processing AWT event",
               this, exception);
  }
}
