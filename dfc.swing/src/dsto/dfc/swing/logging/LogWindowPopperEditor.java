package dsto.dfc.swing.logging;

import java.awt.FlowLayout;

import javax.swing.JCheckBox;
import javax.swing.JLabel;

import dsto.dfc.swing.forms.FormPanel;
import dsto.dfc.swing.forms.JCheckBoxFormEditor;

/**
 * Edits the properties of a LogWindowPopper.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class LogWindowPopperEditor extends FormPanel
{
  private FlowLayout flowLayout1 = new FlowLayout();
  private JLabel jLabel1 = new JLabel();
  private JCheckBox alarmCheckbox = new JCheckBox();
  private JCheckBox warningCheckbox = new JCheckBox();
  private JCheckBox infoCheckbox = new JCheckBox();
  private JCheckBox otherCheckbox = new JCheckBox();

  public LogWindowPopperEditor ()
  {
    try
    {
      jbInit ();
    } catch(Exception ex)
    {
      ex.printStackTrace();
    }

    // setup bindings
    addEditor ("popupOnAlarm", new JCheckBoxFormEditor (alarmCheckbox));
    addEditor ("popupOnWarning", new JCheckBoxFormEditor (warningCheckbox));
    addEditor ("popupOnInfo", new JCheckBoxFormEditor (infoCheckbox));
    addEditor ("popupOnOther", new JCheckBoxFormEditor (otherCheckbox));
  }

  private void jbInit () throws Exception
  {
    jLabel1.setText("Auto display log on:");
    this.setLayout(flowLayout1);
    alarmCheckbox.setText("Alarm");
    warningCheckbox.setText("Warning");
    infoCheckbox.setText("Information");
    flowLayout1.setAlignment(FlowLayout.LEFT);
    otherCheckbox.setText("Other");
    this.add(jLabel1, null);
    this.add(alarmCheckbox, null);
    this.add(warningCheckbox, null);
    this.add(infoCheckbox, null);
    this.add(otherCheckbox, null);
  }
}
