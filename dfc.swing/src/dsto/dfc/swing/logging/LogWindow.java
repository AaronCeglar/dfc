package dsto.dfc.swing.logging;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import dsto.dfc.logging.Log;
import dsto.dfc.logging.LogEvent;
import dsto.dfc.logging.LogMessageBuffer;
import dsto.dfc.swing.controls.DfcDialog;
import dsto.dfc.swing.event.SelectionEvent;
import dsto.dfc.util.Debug;
import dsto.dfc.util.Disposable;

/**
 * Displays a browsable table of log events and optionally shows
 * settings for auto popup on error and warning message events.
 *
 * @version $Revision$
 *
 * @see LogWindowPopper
 * @see LogMessageTable
 */
public class LogWindow extends DfcDialog implements Disposable
{
  private LogMessageTable logMessageTable = new LogMessageTable();
  private LogWindowPopperEditor popperEditor = new LogWindowPopperEditor();
  private LogMessageBuffer messageBuffer;
  private JPanel mainPanel = new JPanel();
  private BorderLayout borderLayout1 = new BorderLayout();
  private JPanel buttonPanel = getButtonPanel ();
  private JButton closeButton = new JButton();
  private JButton clearButton = new JButton();
  private JButton saveButton = new JButton(); 
  private JFileChooser fileChooser = new JFileChooser();
  private File currentFile;
  private JPanel messagesPanel = new JPanel();
  private JScrollPane logMessageScrollPane = new JScrollPane();
  private JScrollPane messageScrollPane = new JScrollPane();
  private JLabel jLabel2 = new JLabel();
  private JTextArea messageField = new JTextArea();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private JPanel exceptionPanel = new JPanel();
  private JTextArea exceptionField = new JTextArea();
  private JScrollPane exceptionScrollPane = new JScrollPane();
  private JLabel exceptionLabel = new JLabel();
  private GridBagLayout gridBagLayout2 = new GridBagLayout();

  public LogWindow ()
  {
    this (null, false);
  }

  public LogWindow (Component client, boolean modal)
  {
    this (client, new LogMessageBuffer (), modal);
  }

  public LogWindow (Component client, LogMessageBuffer messageBuffer,
                    boolean modal)
  {
    super (client, "Log", modal);

    this.messageBuffer = messageBuffer;

    buttonPanel.removeAll ();

    logMessageTable.setMessages (messageBuffer);

    try
    {
      jbInit ();
      setDialogPanel (mainPanel);
      pack ();
    } catch (Exception ex)
    {
      ex.printStackTrace ();
    }

    setDefaultCloseOperation (JDialog.HIDE_ON_CLOSE);

    // select first message if possible
    if (messageBuffer.getEventCount () > 0)
      setSelectedEvent (messageBuffer.getEvent (0));
    else
      setException (null);

    if (!java.beans.Beans.isDesignTime ())
      popperEditor.setVisible (false);
  }

  public void dispose ()
  {
    logMessageTable.dispose ();
    popperEditor.dispose ();

    super.dispose ();
  }

  /**
   * Set the popper being used to popup this window.  This will
   * connect the popper to the popup settigs editor displayed at the
   * bottom of the window.
   */
  public void setPopper (LogWindowPopper popper)
  {
    popperEditor.setEditorValue (popper);
    popperEditor.setVisible (true);
  }

  public void setSelectedEvent (LogEvent selectedEvent)
  {
    int index = messageBuffer.indexOfEvent (selectedEvent);

    Debug.assertTrue (index != -1, "attempt to select event not in log", this);

    if (logMessageTable.getSelectedRow () != index)
      logMessageTable.setRowSelectionInterval (index, index);

    messageField.setText (selectedEvent.getMessage ());

    setException (selectedEvent.getException ());
  }

  protected void setException (Throwable exception)
  {
    if (exception != null)
    {
      StringWriter stackTraceWriter = new StringWriter ();
      PrintWriter printWriter = new PrintWriter (stackTraceWriter);

      Log.printException (printWriter, exception);

      printWriter.close ();

      exceptionField.setText (stackTraceWriter.toString ());
      exceptionField.setCaretPosition (0);

      exceptionField.setEnabled (true);
      exceptionLabel.setEnabled (true);
    } else
    {
      exceptionField.setText ("<< No Trace >>");
      exceptionField.setEnabled (false);
      exceptionLabel.setEnabled (false);
    }
  }

  protected void logMessageTable_selectionChanged (SelectionEvent e)
  {
    if (logMessageTable.getSelectedRowCount () == 1)
    {
      try
      {
        LogEvent selectedEvent =
          messageBuffer.getEvent (logMessageTable.getSelectedRow ());

        setSelectedEvent (selectedEvent);
      } catch (IndexOutOfBoundsException ex)
      {
        /** @todo work out why deselecting a row sometimes causes this */
      }
    }
  }

  protected void closeButton_actionPerformed (ActionEvent e)
  {
    setVisible (false);
  }

  protected void clearButton_actionPerformed (ActionEvent e)
  {
    messageBuffer.clear ();
  }


  protected void saveButton_actionPerformed (ActionEvent e)
  {
    saveLog();
  }

  /**
   * Prompt user to select a file
   */
  private File getLogFile ()
  {
    if (currentFile == null)
    {
      try
      {
        currentFile = new File (System.getProperty ("user.home"));
      } catch (SecurityException ex)
      {
        currentFile = new File (".");
      }
    }
    
    fileChooser.setCurrentDirectory (currentFile);
    int option = fileChooser.showSaveDialog (this);

    if (option == JFileChooser.APPROVE_OPTION)
    {
      File saveFile = fileChooser.getSelectedFile (); 
      
      return saveFile;
    } else 
    {
      return null;	
    }	 
  }
  
  /**
   * Write log message(s) to file
   */
  private void saveLog ()
  { 
    File saveFile = getLogFile ();

    if (saveFile == null) return;  // exit this method if no file was selected

    // make sure file ends in (.log)
    if (!saveFile.getName ().endsWith (".log"))
      saveFile = new File (saveFile.getAbsolutePath () + ".log");

    if (saveFile.exists ())  // prompt user that selected file has existed
    {
      String message = "Would you like to overwrite this file?";
      String title = "File '" + saveFile.getName() + "' has existed";
      int option =
        JOptionPane.showConfirmDialog (this, message, title,
                                       JOptionPane.YES_NO_OPTION);
      if (option == JOptionPane.NO_OPTION)
        return;		
    }
    
    try
    {
      FileWriter fw = new FileWriter (saveFile);
      PrintWriter pw = new PrintWriter (fw);	
      int numOfLogEvent = messageBuffer.getEventCount ();
    
      pw.println ("Writing to log on: " + new Date (System.currentTimeMillis ()));
      pw.println ("There are " + numOfLogEvent + " log message(s)");
      pw.println ();
      
      messageBuffer.write (pw);
    
      fw.close ();
      pw.close ();
      
      currentFile = saveFile;

      String message = "Successfully written to file: " + saveFile.getName ();
      String title = "Save Log";
      JOptionPane.showMessageDialog (this, message, title,
                                     JOptionPane.INFORMATION_MESSAGE);

    } catch (IOException ioe)
    {
      String message = "An error has occured while writing log to file: " + saveFile.getName();
      String title = "Save";
      JOptionPane.showMessageDialog (this, message, title,
                                     JOptionPane.ERROR_MESSAGE);
    } catch (SecurityException se)
    {
      String message = "No writing allowed in this environment";
      String title = "Save";
      JOptionPane.showMessageDialog (this, message, title,
                                     JOptionPane.ERROR_MESSAGE);
    }
  }

  private void jbInit () throws Exception
  {
    mainPanel.setLayout(borderLayout1);
    saveButton.setText("Save");
    saveButton.addActionListener(new java.awt.event.ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          saveButton_actionPerformed(e);
        }
      });
    closeButton.setText("Close");
    closeButton.addActionListener(new java.awt.event.ActionListener()
      {

        public void actionPerformed(ActionEvent e)
        {
          closeButton_actionPerformed(e);
        }
      });
    mainPanel.setMinimumSize(new Dimension(500, 400));
    mainPanel.setPreferredSize(new Dimension(500, 400));
    clearButton.setMnemonic('C');
    clearButton.setText("Clear");
    clearButton.addActionListener(new java.awt.event.ActionListener()
      {

        public void actionPerformed(ActionEvent e)
        {
          clearButton_actionPerformed(e);
        }
      });
    messagesPanel.setLayout(gridBagLayout1);
    jLabel2.setText("Message:");
    messageField.setLineWrap(true);
    messageField.setWrapStyleWord(true);
    logMessageScrollPane.getViewport ().setBackground (UIManager.getColor ("Table.background"));
    logMessageScrollPane.getViewport ().setOpaque (true);
    logMessageTable.addSelectionListener(new dsto.dfc.swing.event.SelectionListener()
      {

        public void selectionChanged(SelectionEvent e)
        {
          logMessageTable_selectionChanged(e);
        }
      });
    exceptionLabel.setText("Exception Trace:");
    exceptionPanel.setLayout(gridBagLayout2);
    mainPanel.add(messagesPanel, BorderLayout.CENTER);
    messagesPanel.add(logMessageScrollPane, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.6
                                                                   ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    messagesPanel.add(messageScrollPane, new GridBagConstraints(0, 2, 1, 1, 1.0, 0.1
                                                                ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(3, 0, 0, 0), 0, 0));
    messageScrollPane.getViewport().add(messageField, null);
    messagesPanel.add(jLabel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
                                                      ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(12, 0, 0, 0), 0, 0));
    messagesPanel.add(exceptionPanel, new GridBagConstraints(0, 3, 1, 1, 1.0, 0.3
                                                             ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(12, 0, 0, 0), 0, 0));
    exceptionPanel.add(exceptionLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
                                                              ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    exceptionPanel.add(exceptionScrollPane, new GridBagConstraints(0, 1, GridBagConstraints.REMAINDER, 1, 1.0, 1.0
                                                                   ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    exceptionScrollPane.getViewport().add(exceptionField, null);
    mainPanel.add(popperEditor, BorderLayout.SOUTH);
    logMessageScrollPane.getViewport().add(logMessageTable, null);
    buttonPanel.add(saveButton, null);
    buttonPanel.add(clearButton, null);
    buttonPanel.add(closeButton, null);
  }
}
