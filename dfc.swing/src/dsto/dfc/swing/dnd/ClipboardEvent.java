package dsto.dfc.swing.dnd;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.Transferable;
import java.util.EventObject;

/**
 * Fired by the ClipboardManager when the clipboard changes.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class ClipboardEvent extends EventObject
{
  protected Transferable contents;

  public ClipboardEvent (Clipboard clipboard, Transferable contents)
  {
    super (clipboard);

    this.contents = contents;
  }

  public Clipboard getClipboard ()
  {
    return (Clipboard)getSource ();
  }

  public Transferable getContents ()
  {
    return contents;
  }
}
