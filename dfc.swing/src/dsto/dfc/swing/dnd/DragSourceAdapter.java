package dsto.dfc.swing.dnd;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragGestureRecognizer;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceContext;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.dnd.InvalidDnDOperationException;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.TooManyListenersException;

import dsto.dfc.logging.Log;

/**
 * A drag source listener that manages drag gestures for a
 * {@link DragComponent}.
 *
 * @version $Revision$
 */
public class DragSourceAdapter
  implements DragSourceListener, DragGestureListener
{
  private static DragComponent currentDragComponent = null;

  protected DragSource dragSource = DragSource.getDefaultDragSource ();
  protected DragComponent component;

  protected Cursor copyDropCursor = DragSource.DefaultCopyDrop;
  protected Cursor copyNoDropCursor = DragSource.DefaultCopyNoDrop;
  protected Cursor moveDropCursor = DragSource.DefaultMoveDrop;
  protected Cursor moveNoDropCursor = DragSource.DefaultMoveNoDrop;
  protected Cursor linkDropCursor = DragSource.DefaultLinkDrop;
  protected Cursor linkNoDropCursor = DragSource.DefaultLinkNoDrop;

  public DragSourceAdapter (DragComponent component,
                            DragGestureRecognizer recognizer)
  {
    this.component = component;

    try
    {
      recognizer.addDragGestureListener (this);
    } catch (TooManyListenersException ex)
    {
      Log.warn ("Could not attach drag gesture listener: D&D will be disabled",
                 this, ex);
    }
  }

  public DragSourceAdapter (DragComponent component)
  {
    this.component = component;

    dragSource.createDefaultDragGestureRecognizer
      ((Component)component, component.getSupportedDragActions (), this);
  }

  public static DragComponent getCurrentDragComponent ()
  {
    return currentDragComponent;
  }

  public void dragGestureRecognized (DragGestureEvent e)
  {
    if (filterTriggerEvent (e.getTriggerEvent ()) &&
        component.isDragOK (e))
    {
      Transferable transferable = component.startDrag (e);

      try
      {
        dragSource.startDrag (e, null, transferable, this);
        currentDragComponent = component;
      } catch (InvalidDnDOperationException ex)
      {
        Log.alarm ("Drag and drop error", this, ex);
      }
    }
  }

  public void dragEnter (DragSourceDragEvent e)
  {
    updateDragOverFeedback (e);
  }

  public void dragOver (DragSourceDragEvent e)
  {
    updateDragOverFeedback (e);
  }

  public void dropActionChanged (DragSourceDragEvent e)
  {
    updateDragOverFeedback (e);
  }

  public void dragExit (DragSourceEvent e)
  {
    DragSourceContext context = e.getDragSourceContext ();
    context.setCursor (null); // DragSourceContext bug hack
    context.setCursor (copyNoDropCursor);
  }

  public void dragDropEnd (DragSourceDropEvent e)
  {
    boolean moveData = false;

    if (e.getDropSuccess ())
    {
      int dragActions = component.getSupportedDragActions ();

      // this is the action selected by the drop target
      if ((e.getDropAction () & DnDConstants.ACTION_MOVE) != 0 &&
          (dragActions & DnDConstants.ACTION_MOVE) != 0)
      {
        moveData = true;
      }
    }

    component.endDrag (e, moveData);

    currentDragComponent = null;
  }

  protected void updateDragOverFeedback (DragSourceDragEvent e)
  {
    DragSourceContext context = e.getDragSourceContext ();
    int dropAction = e.getDropAction ();
    // action is requested actions masked by what target supports
    int action = e.getDropAction () & e.getTargetActions ();
    Cursor cursor = null;

    if (action == DnDConstants.ACTION_NONE)
    {
      // no drop is possible
      if ((dropAction & DnDConstants.ACTION_LINK) == DnDConstants.ACTION_LINK)
        cursor = linkNoDropCursor;
      else if ((dropAction & DnDConstants.ACTION_MOVE) == DnDConstants.ACTION_MOVE)
        cursor = moveNoDropCursor;
      else
        cursor = copyNoDropCursor;
    } else
    {
      // drop is possible
      if ((action & DnDConstants.ACTION_LINK) == DnDConstants.ACTION_LINK)
        cursor = linkDropCursor;
      else if ((action & DnDConstants.ACTION_MOVE) == DnDConstants.ACTION_MOVE)
        cursor = moveDropCursor;
      else
        cursor = copyDropCursor;
    }

    context.setCursor (null); // DragSourceContext bug hack
    context.setCursor (cursor);
  }

  /**
   * Filter drag trigger events.
   */
  protected boolean filterTriggerEvent (InputEvent e)
  {
    if (e instanceof MouseEvent)
    {
      MouseEvent me = (MouseEvent)e;

      // for some reason, JFC under JDK 1.2.2 decides that
      // double-clicks should start a drag: correct this misconception
      if (me.getID () == MouseEvent.MOUSE_PRESSED && me.getClickCount () > 1)
        return false;
    }

    return true;
  }
}
