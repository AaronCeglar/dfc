package dsto.dfc.swing.dnd;

import java.awt.Component;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;

/**
 * A drop target listener manages drop actions for a {@link
 * DropComponent}.
 *
 * @version $Revision$
 */
public class DropTargetAdapter implements DropTargetListener
{
  protected DropComponent component;

  public DropTargetAdapter (DropComponent component)
  {
    this.component = component;

    /** @todo handle case where component changes its supportedDropActions */
    new DropTarget ((Component)component,
                      component.getSupportedDropActions (), this, true);
  }

  protected void updateDragFeedBack (DropTargetDragEvent e)
  {
    if (component.isDropOK (e))
    {
      component.showDragUnderFeedback (true, e);
      e.acceptDrag (e.getDropAction ());
    } else
    {
      component.showDragUnderFeedback (false, e);
      e.rejectDrag ();
    }
  }

  public void dragEnter (DropTargetDragEvent e)
  {
    updateDragFeedBack (e);
  }

  public void dragOver (DropTargetDragEvent e)
  {
    updateDragFeedBack (e);
  }

  public void dropActionChanged (DropTargetDragEvent e)
  {
    updateDragFeedBack (e);
  }

  public void dragExit (DropTargetEvent e)
  {
    component.hideDragUnderFeedback ();
  }

  public void drop (DropTargetDropEvent e)
  {
    int sourceActions = e.getSourceActions ();

    if ((sourceActions & component.getSupportedDropActions ()) == 0)
    {
      e.rejectDrop ();
      component.hideDragUnderFeedback ();
    } else
    {
      try
      {
        e.acceptDrop (e.getDropAction ());

        component.executeDrop (e);
        e.getDropTargetContext().dropComplete (true);
        component.hideDragUnderFeedback ();
      } catch (Throwable ex)
      {
        ex.printStackTrace ();
        e.rejectDrop();

        component.hideDragUnderFeedback ();
      }
    }
  }
}
