package dsto.dfc.swing.dnd;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/**
 * Defines a Transferable where data is unavailable.
 *
 * @version $Revision$
 */
public class MissingDataTransferable implements Transferable
{
  DataFlavor [] flavors;

  public MissingDataTransferable (DataFlavor [] flavors)
  {
    this.flavors = flavors;
  }

  public DataFlavor [] getTransferDataFlavors ()
  {
    return flavors;
  }

  public boolean isDataFlavorSupported (DataFlavor flavor)
  {
    for (int i = 0; i < flavors.length; i++)
    {
      if (flavors [i].equals (flavor))
        return true;
    }

    return false;
  }

  /**
   * Always throws IOException.
   */
  public Object getTransferData (DataFlavor flavor)
    throws UnsupportedFlavorException, IOException
  {
    throw new IOException ("Data not available");
  }

  public String toString ()
  {
    StringBuffer str = new StringBuffer ();

    str.append (getClass ().getName ()).append ("[flavors={");

    for (int i = 0; i < flavors.length; i++)
      str.append (flavors [i].toString ()).append (" ");

    str.append ("}]");

    return str.toString ();
  }
}
