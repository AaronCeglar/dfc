package dsto.dfc.swing.dnd;

import java.awt.datatransfer.DataFlavor;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Stores an object as a serialized blob of data.  Useful for D&D
 * transferables where the target might not know about the object
 * class but may support transfer of blobs.
 *
 * @author Matthew
 * @version $Revision$
 */
public class RawSerializedObject implements Serializable
{
  private static final long serialVersionUID = 1;

  public static final DataFlavor RAW_SERIALIZED_OBJECT_FLAVOR =
    new DataFlavor (RawSerializedObject.class, "Raw Java Serialized Object");

  private byte [] rawData;

  public RawSerializedObject (byte [] rawData)
  {
    this.rawData = rawData;
  }

  public RawSerializedObject (Object object) throws IOException
  {
    rawData = serializeObject (object);
  }

  public byte [] getRawData ()
  {
    return rawData;
  }

  public Object reconstituteObject ()
    throws ClassNotFoundException, IOException
  {
    return deserializeObject (rawData);
  }

  public static byte [] serializeObject (Object object)
    throws IOException
  {
    ByteArrayOutputStream byteStream = new ByteArrayOutputStream (9216);
    ObjectOutputStream objectStream = new ObjectOutputStream (byteStream);

    objectStream.writeObject (object);
    objectStream.close ();

    return byteStream.toByteArray ();
  }

  public static Object deserializeObject (byte [] data)
    throws IOException, ClassNotFoundException
  {
    ByteArrayInputStream byteStream = new ByteArrayInputStream (data);
    ObjectInputStream objectStream = new ObjectInputStream (byteStream);

    Object object = objectStream.readObject ();
    objectStream.close ();

    return object;
  }
}
