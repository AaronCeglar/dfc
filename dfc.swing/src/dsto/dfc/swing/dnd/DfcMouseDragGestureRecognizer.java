package dsto.dfc.swing.dnd;

import java.awt.Component;
import java.awt.Point;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.MouseDragGestureRecognizer;
import java.awt.event.MouseEvent;

/**
 * A simple mouse-based drag gesture recogniser that only triggers
 * drag and drop actions on MOUSE_DRAGGED events.  This can be used to
 * work around a problem with the mouse gesture recognisers in JDK
 * 1.2.2 and 1.3 that get confused in the case of a
 * MOUSE_DOWN/MOUSE_EXIT (ie case where MOUSE_UP is not seen after a
 * MOUSE_DOWN).  This happens, for example, when a JTable pops up an
 * inline editor component (eg a checkbox) in response to a mouse down
 * event.
 *
 * @version $Revision$
 */
public class DfcMouseDragGestureRecognizer extends MouseDragGestureRecognizer
{
  protected MouseEvent lastPress = null;
  protected boolean dragging = false;

  public DfcMouseDragGestureRecognizer (Component component,
                                        int action)
  {
    this (DragSource.getDefaultDragSource (), component, action, null);
  }

  public DfcMouseDragGestureRecognizer (DragSource dragSource,
                                        Component component,
                                        int action)
  {
    this (dragSource, component, action, null);
  }

  public DfcMouseDragGestureRecognizer (DragSource dragSource,
                                        Component component,
                                        int action,
                                        DragGestureListener dgListener)
  {
    super (dragSource, component, action, dgListener);
  }

  public void mousePressed (MouseEvent e)
  {
    events.add (e);
    lastPress = e;
  }

  public void mouseReleased (MouseEvent e)
  {
    resetRecognizer ();
  }

  public void mouseDragged (MouseEvent e)
  {
    if (lastPress != null && !dragging &&
        distance (e.getPoint (), lastPress.getPoint ()) >= 5)
    {
      dragging = true;

      int actions;

      if (component instanceof DragComponent)
        actions = ((DragComponent)component).getSupportedDragActions ();
      else
        actions = DnDConstants.ACTION_COPY_OR_MOVE;

      boolean ctrlPressed = (e.getModifiers () & MouseEvent.CTRL_MASK) != 0;
      int action = ctrlPressed ? DnDConstants.ACTION_COPY : DnDConstants.ACTION_MOVE;

      // if action is move, but component does not support it, fall back
      // to copy
      if (action == DnDConstants.ACTION_MOVE &&
           (actions & DnDConstants.ACTION_MOVE) == 0 &&
           (actions & DnDConstants.ACTION_COPY) != 0)
      {
        action = DnDConstants.ACTION_COPY;
      }

      // mask out unsupported actions
      action &= actions;

      if (action != DnDConstants.ACTION_NONE)
      {
        events.add (e);

        fireDragGestureRecognized (action, lastPress.getPoint ());
      }
    }
  }

  public void resetRecognizer ()
  {
    super.resetRecognizer();
    lastPress = null;
    dragging = false;
  }

  protected static final int distance (Point p1, Point p2)
  {
    int dx = p1.x - p2.x;
    int dy = p1.y - p2.y;

    return (int)Math.sqrt ((dx * dx + dy * dy));
  }
}
