package dsto.dfc.swing.dnd;

import java.awt.Toolkit;
import java.awt.datatransfer.Transferable;
import java.awt.event.KeyEvent;

import javax.swing.Icon;
import javax.swing.KeyStroke;

import dsto.dfc.swing.icons.ImageLoader;

/**
 * Cut from a {@link CnpProvider} to the system clipboard.
 *
 * @version $Revision$
 */
public class CmdCut extends AbstractCnpCommand
{
  public static final Icon ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/edit_cut.gif");

  private static final KeyStroke ACCELERATOR =
    KeyStroke.getKeyStroke (KeyEvent.VK_X,
                            Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask ());

  public CmdCut (CnpProvider source)
  {
    super (source, "cnpCutEnabled");
  }

  public void execute ()
  {
    if (!source.isCnpCutEnabled ())
      return;

    Transferable transferable;

    try
    {
      transferable = source.cnpCut ();
    } catch (UnsupportedOperationException ex)
    {
      transferable = null;
    } catch (CloneNotSupportedException ex)
    {
      transferable = null;
    }

    if (transferable != null)
      ClipboardManager.setClipboardContents (transferable);
  }

  public String getName ()
  {
    return "edit.Cut";
  }

  public Icon getIcon ()
  {
    return ICON;
  }

  public String getDescription ()
  {
    return "Move the selected item to the clipboard";
  }

  public char getMnemonic ()
  {
    return 't';
  }

  public KeyStroke getAccelerator ()
  {
    return ACCELERATOR;
  }
}
