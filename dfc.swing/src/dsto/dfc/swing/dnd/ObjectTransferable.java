package dsto.dfc.swing.dnd;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.lang.reflect.Array;

import dsto.dfc.util.Disposable;

/**
 * Allows any object or array of objects to be stored as a
 * Transferable.  Automatically supports highest-fidelity transfer
 * by choosing best supported flavors.
 *
 * @version $Revision$
 */
public class ObjectTransferable implements Transferable, Disposable
{
  public static final DataFlavor RAW_SERIALIZED_OBJECT_FLAVOR =
    RawSerializedObject.RAW_SERIALIZED_OBJECT_FLAVOR;

  private Object [] values;
  private DataFlavor [] flavors;
  private DataFlavor baseFlavor;
  private DataFlavor bestFlavor;
  private DataFlavor arrayFlavor;

  /**
   * Creates a transferable with a single value.  Note that the
   * representation class will be <code>value.getClass ()</code>.  To
   * use a different representation class (eg an interface of value),
   * use the array version of this constructor instead and ensure the
   * array's component class type is the desired interface type.
   */
  public ObjectTransferable (Object value, String humanName)
  {
    this ((Object [])Array.newInstance (value.getClass (), 1), humanName);

    values [0] = value;
  }

  /**
   * Disposes all disposable values.
   */
  public void dispose ()
  {
    if (values != null)
    {
      for (int i = 0; i < values.length; i++)
      {
        Object value = values [i];

        if (value instanceof Disposable)
          ((Disposable)value).dispose ();
      }

      values = null;
      flavors = null;
    }
  }

  /**
   * Create a transferable containing a list of values.  The
   * representation class of the supported flavors will be
   * array.getComponentType ().
   */
  public ObjectTransferable (Object [] values, String humanName)
  {
    /** @todo support javaJVMLocalObject flavor? */
    this.values = values;

    Class commonClass = findCommonClass (values);

    this.arrayFlavor = new DataFlavor (values.getClass (), humanName);
    this.baseFlavor =
      new DataFlavor (values.getClass ().getComponentType (), humanName);

    if (commonClass.equals (values.getClass ().getComponentType ()))
    {
      // could not find a more-specific common class for values =>
      // best flavor = base flavor

      this.bestFlavor = baseFlavor;

      this.flavors =
        new DataFlavor []
          {baseFlavor, arrayFlavor, RAW_SERIALIZED_OBJECT_FLAVOR};
    } else
    {
      // found a more specific common class as best flavor

      this.bestFlavor = new DataFlavor (commonClass, humanName);

      this.flavors =
        new DataFlavor []
          {bestFlavor, baseFlavor, arrayFlavor,
           RAW_SERIALIZED_OBJECT_FLAVOR};
    }
  }

  /**
   * Find the most specific class common to a set of values. Only accepts
   * non-null values.
   */
  @SuppressWarnings("null") // cannot be null - java.lang.Object will appear
  protected static Class findCommonClass (Object [] values)
  {
    Class commonClass = null;

    for (int i = 0; i < values.length; i++)
    {
      Class c = values [i].getClass ();

      if (commonClass == null)
        commonClass = c;
      else
      {
        while (!commonClass.isAssignableFrom (c))
        {
          commonClass = commonClass.getSuperclass ();
        }
      }
    }

    return commonClass;
  }

  public DataFlavor [] getTransferDataFlavors ()
  {
    return flavors;
  }

  public boolean isDataFlavorSupported (DataFlavor flavor)
  {
    for (int i = 0; i < flavors.length; i++)
    {
      if (flavors [i].equals (flavor))
        return true;
    }

    return false;
  }

  public Object getTransferData (DataFlavor flavor)
    throws UnsupportedFlavorException, IOException
  {
    if (flavor.equals (bestFlavor) || flavor.equals (baseFlavor))
    {
      return values [0];
    } else if (flavor.equals (arrayFlavor))
    {
      // array type
      return values;
    } else if (flavor.equals (RAW_SERIALIZED_OBJECT_FLAVOR))
    {
      return new RawSerializedObject (values [0]);
    } else
    {
      throw new UnsupportedFlavorException (flavor);
    }
  }

  // This code is an experiment in relaxing DataFlavor.equals to
  // consider flavors with interface-level compatiblity to be equal.
//    protected static boolean isCompatibleFlavor (DataFlavor refFlavor,
//                                                 DataFlavor flavor)
//    {
//      // if flavors not exactly equal, test if it's because classes were not
//      // equal
//      if (!refFlavor.equals (flavor) && refFlavor.isMimeTypeEqual (flavor))
//      {
//        Class refClass = refFlavor.getRepresentationClass ();
//        Class clazz = flavor.getRepresentationClass ();

//        return refClass != null && refClass.isAssignableFrom (clazz);
//      } else
//      {
//        return true;
//      }
//    }
}
