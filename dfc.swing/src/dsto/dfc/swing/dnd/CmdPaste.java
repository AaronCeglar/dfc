package dsto.dfc.swing.dnd;

import java.awt.Toolkit;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.KeyEvent;
import java.io.IOException;

import javax.swing.Icon;
import javax.swing.KeyStroke;

import dsto.dfc.logging.Log;
import dsto.dfc.swing.icons.ImageLoader;

/**
 * Paste from the system clipboard to a {@link CnpProvider}.
 *
 * @version $Revision$
 */
public class CmdPaste extends AbstractCnpCommand
{
  public static final Icon ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/edit_paste.gif");

  private static final KeyStroke ACCELERATOR =
    KeyStroke.getKeyStroke (KeyEvent.VK_V,
                            Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask ());

  public CmdPaste (CnpProvider source)
  {
    super (source, "cnpPasteEnabled");
  }

  public void execute ()
  {
    Transferable transferable = ClipboardManager.getClipboardContents ();

    if (!source.isCnpPasteEnabled (transferable))
      return;

    try
    {
      source.cnpPaste (transferable);
    } catch (UnsupportedOperationException ex)
    {
      Log.warn ("Paste operation not supported", this, ex);
    } catch (UnsupportedFlavorException ex)
    {
      Log.warn ("Unsupported pasted data type", this, ex);
    } catch (IOException ex)
    {
      Log.warn ("IO error during paste", this, ex);
    } catch (CloneNotSupportedException ex)
    {
      Log.warn ("Clone error during paste", this, ex);
    }
  }

  public String getName ()
  {
    return "edit.Paste";
  }

  public Icon getIcon ()
  {
    return ICON;
  }

  public String getDescription ()
  {
    return "Insert the contents of the clipboard at the current point";
  }

  public char getMnemonic ()
  {
    return 'p';
  }

  public KeyStroke getAccelerator ()
  {
    return ACCELERATOR;
  }
}
