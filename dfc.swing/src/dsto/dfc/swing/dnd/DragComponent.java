package dsto.dfc.swing.dnd;

import java.awt.datatransfer.Transferable;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragSourceDropEvent;

/**
 * Defines a component that supports drag actions. Classes
 * implementing this interface must also extend java.awt.Component.
 * Classes that implement this interface may then use an instance of
 * {@link DragSourceAdapter} to manage drag events via method calls to
 * this interface.
 *
 * @version $Revision$
 */
public interface DragComponent
{
  /**
   * Return a mask of possible drag actions supported by this
   * component eg DnDConstants.ACTION_COPY_OR_MOVE.  Note that the
   * component may veto any particular drag attempt from
   * {@link #isDragOK}.
   */
  public int getSupportedDragActions ();

  /**
   * Return true if starting a drag in the given context is possible.
   *
   * @param e The drag context.
   */
  public boolean isDragOK (DragGestureEvent e);

  /**
   * Start a drag operation.
   *
   * @param e The gesture that started the drag.
   * @return The transferable data, or null if no data available.
   */
  public Transferable startDrag (DragGestureEvent e);

  public void endDrag (DragSourceDropEvent e, boolean moveData);
}
