package dsto.dfc.swing.dnd;

import java.util.EventListener;

/**
 * Listener interface for ClipboardManager events.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public interface ClipboardListener extends EventListener
{
  public void clipboardChanged (ClipboardEvent e);
}
