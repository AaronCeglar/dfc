package dsto.dfc.swing.dnd;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
import java.security.AccessControlException;
import java.util.Vector;

import dsto.dfc.util.Disposable;

/**
 * Provides centralised clipboard management.
 *
 * @version $Revision$
 */
public final class ClipboardManager
{
  private static final ClipboardOwner clipboardOwner =
    new ManagerClipboardOwner ();
  private static Clipboard clipboard = null;
  private static Vector clipboardListeners;

  static
  {
    try
    {
      clipboard = Toolkit.getDefaultToolkit ().getSystemClipboard ();
    } catch (AccessControlException e)
    {
      // don't have access to system clipboard, use a local one instead
      clipboard = new Clipboard ("DFC Local Clipboard");
    }
  }

  private ClipboardManager ()
  {
    // zip
  }

  /**
   * Set the current clipboard's contents.  Any existing contents will
   * be disposed.
   */
  public static void setClipboardContents (Transferable contents)
  {
    Transferable oldContents = getClipboardContents ();

    // dispose old contents
    if (oldContents instanceof Disposable)
      ((Disposable)oldContents).dispose ();

    clipboard.setContents (contents, clipboardOwner);

    fireClipboardChanged (new ClipboardEvent (clipboard, contents));
  }

  public static Transferable getClipboardContents ()
  {
    return clipboard.getContents (clipboardOwner);
  }

  public static synchronized void removeClipboardListener (ClipboardListener l)
  {
    if (clipboardListeners != null && clipboardListeners.contains(l))
    {
      Vector v = (Vector) clipboardListeners.clone();
      v.removeElement(l);
      clipboardListeners = v;
    }
  }

  public static synchronized void addClipboardListener (ClipboardListener l)
  {
    Vector v = clipboardListeners == null ? new Vector(2) : (Vector) clipboardListeners.clone();

    if (!v.contains(l))
    {
      v.addElement(l);
      clipboardListeners = v;
    }
  }

  protected static void fireClipboardChanged (ClipboardEvent e)
  {
    if (clipboardListeners != null)
    {
      Vector listeners = clipboardListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((ClipboardListener) listeners.elementAt(i)).clipboardChanged(e);
      }
    }
  }

  protected static void lostOwnership (Clipboard oldClipboard, Transferable contents)
  {
    // we lost track of the clipboard
    fireClipboardChanged (new ClipboardEvent (oldClipboard, contents));

    // TODO: start a clipboard monitor thread in case a non-Java
    // app changes clipboard?
  }

  static class ManagerClipboardOwner implements ClipboardOwner
  {
    public void lostOwnership (Clipboard oldClipboard, Transferable contents)
    {
      ClipboardManager.lostOwnership (oldClipboard, contents);
    }
  }
}
