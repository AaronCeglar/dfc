package dsto.dfc.swing.dnd;

import java.awt.Toolkit;
import java.awt.datatransfer.Transferable;
import java.awt.event.KeyEvent;

import javax.swing.Icon;
import javax.swing.KeyStroke;

import dsto.dfc.swing.icons.ImageLoader;

/**
 * Copy from a {@link CnpProvider} to the system clipboard.
 *
 * @version $Revision$
 */
public class CmdCopy extends AbstractCnpCommand
{
  public static final Icon ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/edit_copy.gif");

  private static final KeyStroke ACCELERATOR =
    KeyStroke.getKeyStroke (KeyEvent.VK_C,
                            Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask ());

  public CmdCopy (CnpProvider source)
  {
    super (source, "cnpCopyEnabled");
  }

  public void execute ()
  {
    if (!source.isCnpCopyEnabled ())
      return;

    Transferable transferable;

    try
    {
      transferable = source.cnpCopy ();
    } catch (UnsupportedOperationException ex)
    {
      transferable = null;
    } catch (CloneNotSupportedException ex)
    {
      transferable = null;
    }

    if (transferable != null)
      ClipboardManager.setClipboardContents (transferable);
  }

  public String getName ()
  {
    return "edit.Copy";
  }

  public Icon getIcon ()
  {
    return ICON;
  }

  public String getDescription ()
  {
    return "Copy the selected item to the clipboard";
  }

  public char getMnemonic ()
  {
    return 'c';
  }

  public KeyStroke getAccelerator ()
  {
    return ACCELERATOR;
  }
}
