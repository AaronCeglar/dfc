package dsto.dfc.swing.dnd;

import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;

/**
 * Defines a component that supports drop actions.  Implementing
 * classes must also extend java.awt.Component.  Classes that
 * implement this interface generally use an instance of {@link
 * DropTargetAdapter} to manage drop events via method calls to this
 * interface.
 *
 * @version $Revision$
 *
 * @see DropTargetAdapter
 * @see DragSourceAdapter
 */
public interface DropComponent
{
  /**
   * Return a mask of actions supported by this component eg
   * DnDConstants.ACTION_COPY_OR_MOVE.  Note that the component may
   * veto any particular drop attempt from {@link #isDropOK}.
   */
  public int getSupportedDropActions ();
 
  /**
   * Return true if a drop in the context in a DropTargetDragEvent is
   * possible.
   *
   * @param e The drop context.
   */
  public boolean isDropOK (DropTargetDragEvent e);

  /**
   * Display any drag under (ie displayed under the mouse in the
   * component) feedback.
   *
   * @param dropOK The value of isDropOK () when the mouse first
   * entered the component.
   * @param e The drop context.
   */
  public void showDragUnderFeedback (boolean dropOK, DropTargetDragEvent e);

  /**
   * Remove any drag under feedback displayed by showDragUnderFeedback
   * ().
   */
  public void hideDragUnderFeedback ();

  /**
   * Execute a drop action.
   */
  public void executeDrop (DropTargetDropEvent e);
}
