package dsto.dfc.swing.dnd;

/**
 * Covenience interface for components that implement both
 * DragComponent and DropComponent.
 *
 * @version $Revision$
 */
public interface DragDropComponent extends DragComponent, DropComponent
{
  // zip
}
