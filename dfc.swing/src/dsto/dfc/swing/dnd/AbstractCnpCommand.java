package dsto.dfc.swing.dnd;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import dsto.dfc.swing.commands.AbstractMutableCommand;
import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.util.PropertyEventSource;

/**
 * Abstract base class for clipboard based commands operating on a
 * CnpProvider.
 *
 * @version $Revision$
 */
public abstract class AbstractCnpCommand extends AbstractMutableCommand
  implements PropertyChangeListener, ClipboardOwner
{
  private String property;
  protected CnpProvider source;

  /**
   * Create a new instance.
   *
   * @param source The CnpProvider being operated on.
   * @param property The (boolean) property of the CnpProvider that
   * indicates whether this command is active or not (usually one of
   * the 'cnp*' properties).
   */
  public AbstractCnpCommand (CnpProvider source, String property)
  {
    this.source = source;
    this.property = property;

    updateEnabled ();

    if (source instanceof PropertyEventSource)
    {
      // setup enable/disable on cn* property

      ((PropertyEventSource)source).addPropertyChangeListener (this);
    }
  }

  public abstract void execute ();

  public abstract String getName ();

  public abstract String getDescription ();

  public abstract char getMnemonic ();

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return "Edit.cnp";
    else if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return "cnp";
    else if (viewName.equals (CommandView.TOOLBAR_VIEW))
      return "cnp";
    else
      return null;
  }

  public boolean isInteractive ()
  {
    return false;
  }

  /**
   * Update enabled status of command based on current value of the selected
   * cnp* property.
   */
  protected void updateEnabled ()
  {
    boolean enabled;
    Transferable transferable = ClipboardManager.getClipboardContents ();

    if (property.equals ("cnpPasteEnabled"))
      enabled = transferable != null && source.isCnpPasteEnabled (transferable);
    else if (property.equals ("cnpCopyEnabled"))
      enabled = source.isCnpCopyEnabled ();
    else
      enabled = source.isCnpCutEnabled ();

    setEnabled (enabled);
  }

  // PropertyChangeListener interface

  public void propertyChange (PropertyChangeEvent e)
  {
    if (e.getPropertyName ().equals (property))
    {
      boolean newValue = ((Boolean)e.getNewValue ()).booleanValue ();

      setEnabled (newValue);
    }
  }

  // ClipboardOwner interface

  public void lostOwnership (Clipboard clipboard, Transferable contents)
  {
    updateEnabled ();
  }
}
