package dsto.dfc.swing.dnd;

import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/**
 * Defines a object that supports cut, copy and paste operations.
 *
 * @version $Revision$
 *
 * @see CmdCut
 * @see CmdCopy
 * @see CmdPaste
 */
public interface CnpProvider
{
  /**
   * True if a copy operation is possible.  Implementors of this method
   * who also implement PropertyEventSource are assumed to provide property
   * change notification.
   */
  public boolean isCnpCopyEnabled ();

  /**
   * True if a cut operation is possible.  Implementors of this method
   * who also implement PropertyEventSource are assumed to provide property
   * change notification.
   */
  public boolean isCnpCutEnabled ();

  /**
   * True if a paste operation with a given transferable is possible. 
   */
  public boolean isCnpPasteEnabled (Transferable transferable); 

  /**
   * Perform copy operation.
   *
   * @return Copied transferable or null if nothing is available to be copied
   * (eg because there is nothing selected)
   * @exception UnsupportedOperationException if copy operation not supported.
   * @exception CloneNotSupportedException if the data could not be cloned.
   */
  public Transferable cnpCopy ()
    throws UnsupportedOperationException, CloneNotSupportedException;

  /**
   * Perform cut operation.
   *
   * @return Cut transferable or null if nothing is available to be cut
   * (eg because there is nothing selected)
   * @exception UnsupportedOperationException if cut operation not supported.
   * @exception CloneNotSupportedException if the data could not be cloned.
   */
  public Transferable cnpCut ()
    throws UnsupportedOperationException, CloneNotSupportedException;

  /**
   * Perform paste operation.
   *
   * @param transferable The data to be pasted.
   * @return True is transferable was pasted, false if not (eg because there is
   * no selection to define where pasted item goes).
   * @exception UnsupportedOperationException if paste operation not supported.
   * @exception UnsupportedFlavorException if transferable does not contain a
   * supported data flavor for pasting.
   * @exception CloneNotSupportedException if the data could not be cloned.
   * @exception IOException if IO exception occurs during data transfer.
   */
  public boolean cnpPaste (Transferable transferable)
    throws UnsupportedOperationException,
           UnsupportedFlavorException,
           CloneNotSupportedException,
           IOException;
}
