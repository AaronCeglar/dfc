package dsto.dfc.swing.list;

import javax.swing.ListModel;

/**
 * Extension of Swing ListModel to support row add/remove/replace.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public interface DfcListModel extends ListModel
{
  /**
   * Test if this model is mutable (can be changed).
   *
   * @return True if the list can be changed.
   */
  public boolean isMutable ();

  /**
   * Find the first occurrence of an entry and return its index.
   *
   * @return The index of entry or -1 if not found.
   */
  public int indexOf (Object entry);

  /**
   * Test if an entry can be replaced.
   *
   * @param entry The entry to add.
   * @param row The row to overwrite.
   * @return True if entry can be replaced.
   *
   * @see #replaceEntry
   */
  public boolean canReplaceEntry (Object entry, int row);

  /**
   * Replace an entry can at a given row.
   *
   * @param entry The entry to add.
   * @param row The row to overwrite.
   * @return The old value at row.
   *
   * @exception IndexOutOfBoundsException if row is out of bounds.
   * @exception UnsupportedOperationException if entry is not allowed to be
   * replaced.
   *
   * @see #canReplaceEntry
   */
  public Object replaceEntry (Object entry, int row)
    throws IndexOutOfBoundsException, UnsupportedOperationException;

  /**
   * Test if an entry can be added at a given row.
   *
   * @param entry The entry to add.
   * @param row The row to add the new entry.
   * @return True if entry can be added at row.
   *
   * @see #addEntry
   */
  public boolean canAddEntry (Object entry, int row);

  /**
   * Add an entry can at a given row.
   *
   * @param entry The entry to add.
   * @param row The row to add the new entry.
   *
   * @exception IndexOutOfBoundsException if row is out of bounds.
   * @exception UnsupportedOperationException if entry is not allowed to be
   * added.
   *
   * @see #canAddEntry
   */
  public void addEntry (Object entry, int row)
    throws IndexOutOfBoundsException, UnsupportedOperationException;

  /**
   * Test if an entry can be removed.
   *
   * @param row The row to be removed.
   * @return True if row can be removed.
   *
   * @see #removeEntry
   */
  public boolean canRemoveEntry (int row);

  /**
   * Remove an entry at a given row.
   *
   * @param row The row to remove.
   * @return The entry that was removed.
   *
   * @exception IndexOutOfBoundsException if row is out of bounds.
   * @exception UnsupportedOperationException if entry is not allowed to be
   * removed.
   *
   * @see #canRemoveEntry
   */
  public Object removeEntry (int row)
    throws IndexOutOfBoundsException, UnsupportedOperationException;
}
