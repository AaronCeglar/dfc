package dsto.dfc.swing.list;

import java.util.EventObject;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import dsto.dfc.swing.commands.CommandMenus;
import dsto.dfc.swing.commands.CommandSource;
import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.swing.event.BasicSelectionEventSource;
import dsto.dfc.swing.event.SelectionEventSource;
import dsto.dfc.swing.event.SelectionListener;

/**
 * An extended javax.swing.JList.  Supports the {@link
 * dsto.dfc.swing.commands} framework.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class DfcList
  extends JList implements SelectionEventSource, CommandSource
{
  protected CommandView contextCommandView =
    new CommandView (CommandView.CONTEXT_MENU_VIEW);
  protected CommandView toolbarCommandView =
    new CommandView (CommandView.TOOLBAR_VIEW);
  private BasicSelectionEventSource selectionListeners =
    new BasicSelectionEventSource (this);
  private Listener listener = new Listener ();

  public DfcList ()
  {
    this (new DefaultListModel ());
  }

  public DfcList (ListModel model)
  {
    super (model); // I'm a model, you'll know what I mean...

    CommandMenus.installContextMenu (this, contextCommandView);

    // JList does not call setSelectionModel (), so add listener here
    getSelectionModel ().addListSelectionListener (listener);
  }

  public void setSelectionModel (ListSelectionModel newModel)
  {
    ListSelectionModel oldModel = getSelectionModel ();

    super.setSelectionModel (newModel);

    if (oldModel != null)
      oldModel.removeListSelectionListener (listener);

    if (newModel != null)
      newModel.addListSelectionListener (listener);
  }

  public CommandView getCommandView (String viewName)
  {
    if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return contextCommandView;
    else if (viewName.equals (CommandView.TOOLBAR_VIEW))
      return toolbarCommandView;
    else
      return null;
  }

  public void addSelectionListener (SelectionListener l)
  {
    selectionListeners.addSelectionListener (l);
  }

  public void removeSelectionListener (SelectionListener l)
  {
    selectionListeners.removeSelectionListener (l);
  }

  protected void fireSelectionChanged (EventObject trigger)
  {
    selectionListeners.fireSelectionChanged (trigger);
  }

  class Listener implements ListSelectionListener
  {
    public void valueChanged (ListSelectionEvent e)
    {
      fireSelectionChanged (e);
    }
  }
}
