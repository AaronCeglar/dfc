package dsto.dfc.swing.list;

import java.util.List;

import javax.swing.AbstractListModel;

import dsto.dfc.collections.CollectionEvent;
import dsto.dfc.collections.CollectionListener;
import dsto.dfc.collections.MonitoredList;
import dsto.dfc.util.Disposable;
import dsto.dfc.util.Objects;

/**
 * Adapts a List to look like a Swing ListModel.  Implementes the
 * DfcListModel interface to support row add and remove. Supports automatic
 * update for dynamic List's ({@link dsto.dfc.collections.MonitoredList}).
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class ListModelAdapter
  extends AbstractListModel implements DfcListModel, CollectionListener, Disposable
{
  protected List list;
  boolean copyOnEdit;

  public ListModelAdapter (List list)
  {
    this (list, false);
  }

  /**
   * Create a new instance.
   *
   * @param list The list to adapt to the ListModel interface.
   * @param copyOnEdit True if the list should be copied (via clone ())
   * before any edit operations.
   */
  public ListModelAdapter (List list, boolean copyOnEdit)
  {
    this.list = list;
    this.copyOnEdit = copyOnEdit;

    if (list instanceof MonitoredList)
    {
      ((MonitoredList)list).addCollectionListener (this);
    }

    if (list == null)
      throw new Error ();
  }

  public void dispose ()
  {
    if (list != null)
    {
      if (list instanceof MonitoredList)
      {
        ((MonitoredList)list).removeCollectionListener (this);
      }

      list = null;
    }
  }

  public int getSize ()
  {
    return list.size ();
  }

  public Object getElementAt (int index)
  {
    return list.get (index);
  }

  // DfcListModel interface

  public boolean isMutable ()
  {
    return true;
  }

  public int indexOf (Object entry)
  {
    return list.indexOf (entry);
  }

  public boolean canReplaceEntry (Object entry, int row)
  {
    return true;
  }

  public Object replaceEntry (Object entry, int row)
    throws IndexOutOfBoundsException, UnsupportedOperationException
  {
    maybeCopyList ();

    Object value = list.set (row, entry);

    fireContentsChanged (this, row, row);

    return value;
  }

  public boolean canAddEntry (Object entry, int row)
  {
    return true;
  }

  public void addEntry (Object entry, int row)
    throws IndexOutOfBoundsException, UnsupportedOperationException
  {
    maybeCopyList ();

    list.add (row, entry);

    fireIntervalAdded (this, row, row);
  }

  public boolean canRemoveEntry (int row)
  {
    return true;
  }

  public Object removeEntry (int row)
    throws IndexOutOfBoundsException, UnsupportedOperationException
  {
    maybeCopyList ();

    Object value = list.remove (row);

    fireIntervalRemoved (this, row, row);

    return value;
  }

  protected void maybeCopyList ()
   throws UnsupportedOperationException
  {
    if (copyOnEdit)
    {
      try
      {
        list = (List)Objects.cloneObject (list);
      } catch (CloneNotSupportedException ex)
      {
        throw new UnsupportedOperationException ("List is not cloneable: " + list.getClass ());
      }
    }
  }

  // CollectionListener interface

  public void elementsAdded (CollectionEvent e)
  {
    fireIntervalAdded (this, e.getStartIndex (), e.getEndIndex ());
  }

  public void elementsRemoved (CollectionEvent e)
  {
    fireIntervalRemoved (this, e.getStartIndex (), e.getEndIndex ());
  }
}