package dsto.dfc.swing.list;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import javax.swing.AbstractListModel;

import dsto.dfc.collections.CollectionEvent;
import dsto.dfc.collections.CollectionListener;
import dsto.dfc.collections.MonitoredCollection;
import dsto.dfc.util.DefaultComparator;
import dsto.dfc.util.Disposable;

/**
 * Presents any collection as a sorted DfcListModel.  If the
 * collection implements MonitoredCollection, changes are automatically
 * reflected in the list model.
 *
 * @version $Revision$
 */
public class SortedListModelAdapter
  extends AbstractListModel
  implements DfcListModel, CollectionListener, Disposable
{
  protected Collection collection;
  protected ArrayList sortedCollection;
  protected Comparator comparator;
  protected boolean mutable;

  /**
   * Create a list model that sorts Comparable items.
   */
  public SortedListModelAdapter (Collection collection)
  {
    this (collection, DefaultComparator.instance ());
  }

  /**
   * Create a list model that sorts the collection using a specified
   * comparator.
   */
  public SortedListModelAdapter (Collection collection, Comparator comparator)
  {
    this.collection = collection;
    this.sortedCollection = new ArrayList (collection);
    this.comparator = comparator;
    this.mutable = true;

    if (collection instanceof MonitoredCollection)
      ((MonitoredCollection)collection).addCollectionListener (this);

    Collections.sort (sortedCollection, comparator);
  }

  public void dispose ()
  {
    if (collection != null)
    {
      if (collection instanceof MonitoredCollection)
        ((MonitoredCollection)collection).removeCollectionListener (this);

      collection = null;

      // we deliberately do not null sortedCollection, because under some
      // circumstances, JList calls getSize () after disposal during focus
      // lost.
    }
  }

  /**
   * Add a new entry at its correct sorted index in the list.
   *
   * @param entry The new entry.
   * @throws UnsupportedOperationException if list is not mutable.
   */
  public void addEntry (Object entry)
    throws UnsupportedOperationException
  {
    addEntry (entry, getSortedIndex (entry));
  }

  protected int getSortedIndex (Object listItem)
  {
    for (int i = 0; i < sortedCollection.size (); i++)
    {
      Object item = sortedCollection.get (i);

      if (comparator.compare (listItem, item) <= 0)
        return i;
    }

    return sortedCollection.size ();
  }

  /**
   * For use when not using a MonitoredCollection in the list: indicate that
   * elements have been added to the underlying collection.
   *
   * @param c The collection of new elements.
   */
  public void elementsAdded (Collection c)
  {
    for (Iterator i = c.iterator (); i.hasNext (); )
    {
      Object item = i.next ();
      int index = getSortedIndex (item);

      sortedCollection.add (index, item);

      fireIntervalAdded (this, index, index);
    }
  }

  /**
   * For use when not using a MonitoredCollection in the list: indicate that
   * elements have been removed from the underlying collection.
   *
   * @param c The deleted elements.
   */
  public void elementsRemoved (Collection c)
  {
    for (Iterator i = c.iterator (); i.hasNext (); )
    {
      Object item = i.next ();
      int index = sortedCollection.indexOf (item);

      sortedCollection.remove (index);

      fireIntervalRemoved (this, index, index);
    }
  }

  // DfcListModel interface

  public boolean isMutable ()
  {
    return mutable;
  }

  public void setMutable (boolean newValue)
  {
    this.mutable = newValue;
  }

  public int indexOf (Object entry)
  {
    return sortedCollection.indexOf (entry);
  }

  public boolean canReplaceEntry (Object entry, int row)
  {
    return mutable && getSortedIndex (entry) == row;
  }

  public Object replaceEntry (Object entry, int row)
    throws IndexOutOfBoundsException, UnsupportedOperationException
  {
    checkAddReplace (entry, row);

    Object oldEntry = sortedCollection.get (row);

    collection.remove (oldEntry);
    collection.add (entry);

    if (!(collection instanceof MonitoredCollection))
      sortedCollection.set (row, entry);

    fireContentsChanged (this, row, row);

    return oldEntry;
  }

  public boolean canAddEntry (Object entry, int row)
  {
    return mutable && getSortedIndex (entry) == row;
  }

  public void addEntry (Object entry, int row)
    throws IndexOutOfBoundsException, UnsupportedOperationException
  {
    checkAddReplace (entry, row);

    collection.add (entry);

    if (!(collection instanceof MonitoredCollection))
      sortedCollection.add (row, entry);

    fireIntervalAdded (this, row, row);
  }

  public boolean canRemoveEntry (int row)
  {
    return mutable;
  }

  public Object removeEntry (int row)
    throws IndexOutOfBoundsException, UnsupportedOperationException
  {
    checkRemove (row);

    Object entry = sortedCollection.get (row);

    collection.remove (entry);

    if (!(collection instanceof MonitoredCollection))
      sortedCollection.remove (row);

    fireIntervalRemoved (this, row, row);

    return entry;
  }

  protected void checkAddReplace (Object entry, int row)
    throws UnsupportedOperationException
  {
    if (!mutable || getSortedIndex (entry) != row)
      throw new UnsupportedOperationException ();
  }

  protected void checkRemove (int row)
    throws UnsupportedOperationException
  {
    if (!mutable)
      throw new UnsupportedOperationException ();
  }

  // ListModel interface

  public int getSize ()
  {
    return sortedCollection.size ();
  }

  public Object getElementAt (int index)
  {
    return sortedCollection.get (index);
  }

  // CollectionListener interface

  public void elementsAdded (CollectionEvent e)
  {
    elementsAdded (e.getElements ());
  }

  public void elementsRemoved (CollectionEvent e)
  {
    elementsRemoved (e.getElements ());
  }
}
