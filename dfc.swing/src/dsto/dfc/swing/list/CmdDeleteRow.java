package dsto.dfc.swing.list;

import javax.swing.JOptionPane;

import dsto.dfc.swing.commands.BasicDeleteCommand;
import dsto.dfc.swing.event.SelectionEvent;

/**
 * Deletes the currently selected row of a list using that is
 * displaying a {@link DfcListModel}.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class CmdDeleteRow extends BasicDeleteCommand
{
  protected DfcList list;
  protected boolean confirmDelete;

  public CmdDeleteRow (DfcList list)
  {
    super (list);

    this.list = list;
    this.confirmDelete = false;

    setEnabled (list.getSelectedIndex () != -1);
  }

  /**
   * Enable/disable user confirmation dialog before delete.
   */
  public void setConfirmDelete (boolean newValue)
  {
    confirmDelete = newValue;
  }

  public boolean isConfirmDelete ()
  {
    return confirmDelete;
  }

  public void execute ()
  {
    if (list.getModel () instanceof DfcListModel)
    {
      DfcListModel model = (DfcListModel)list.getModel ();
      int [] indexes = list.getSelectedIndices ();

      if (checkDeleteOk ())
      {
        list.getSelectionModel ().clearSelection ();

        for (int i = indexes.length - 1; i >= 0; i--)
        {
          if (model.canRemoveEntry (indexes [i]))
            model.removeEntry (indexes [i]);
        }
      }
    }
  }

  /**
   * Return true if it is OK to delete items (check with user if confirmDelete
   * is true).
   */
  protected boolean checkDeleteOk ()
  {
    if (!confirmDelete)
      return true;

    int result =
      JOptionPane.showConfirmDialog
        (list, "Are you sure you want to delete the selected items?",
         "Delete", JOptionPane.YES_NO_OPTION);

    return result == JOptionPane.YES_OPTION;
  }

  public void selectionChanged (SelectionEvent e)
  {
    setEnabled (list.getSelectedIndex () != -1);
  }
}
