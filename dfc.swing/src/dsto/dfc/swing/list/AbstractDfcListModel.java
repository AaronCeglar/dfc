package dsto.dfc.swing.list;

import javax.swing.AbstractListModel;

/**
 * An abstract implementation of an immutable DfcListModel.  All mutator
 * methods throw UnsupportedOperationException.  Clients need to implement
 * getSize () and getElementAt ().
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public abstract class AbstractDfcListModel
  extends AbstractListModel implements DfcListModel
{
  protected AbstractDfcListModel ()
  {
    // zip
  }

  public abstract int getSize ();

  public abstract Object getElementAt (int index);

  public boolean isMutable()
  {
    return false;
  }

  public int indexOf (Object entry)
  {
    for (int i = 0; i < getSize (); i++)
    {
      if (getElementAt (i).equals (entry))
        return i;
    }

    return -1;
  }

  public boolean canReplaceEntry (Object entry, int row)
  {
    return false;
  }

  public Object replaceEntry(Object entry, int row)
    throws IndexOutOfBoundsException, UnsupportedOperationException
  {
    throw new UnsupportedOperationException ("List is immutable");
  }

  public boolean canAddEntry (Object entry, int row)
  {
    return false;
  }

  public void addEntry(Object entry, int row) throws IndexOutOfBoundsException, UnsupportedOperationException
  {
    throw new UnsupportedOperationException ("List is immutable");
  }

  public boolean canRemoveEntry (int row)
  {
    return false;
  }

  public Object removeEntry (int row) throws IndexOutOfBoundsException, UnsupportedOperationException
  {
    throw new UnsupportedOperationException ("List is immutable");
  }
}