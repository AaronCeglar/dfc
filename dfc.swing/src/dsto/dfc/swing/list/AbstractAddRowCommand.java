package dsto.dfc.swing.list;

import dsto.dfc.swing.commands.BasicAddEntryCommand;

/**
 * Base class for "Add New Row"-type commands.  Subclasses must
 * implement {@link #createNewEntry} and may choose to override
 * addEntry () and getIndexForNewEntry ().
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public abstract class AbstractAddRowCommand extends BasicAddEntryCommand
{
  protected DfcList list;

  public AbstractAddRowCommand (DfcList list)
  {
    this.list = list;
  }

  /**
   * Executes command by calling createNewEntry (), and then adding
   * the entry at the index specified by getIndexForNewEntry () by
   * calling addEntry ().
   */
  public void execute ()
  {
    if (list.getModel () instanceof DfcListModel)
    {
      Object newEntry = createNewEntry ();

      if (newEntry == null)
        return;

      DfcListModel model = (DfcListModel)list.getModel ();
      int index = getIndexForNewEntry (newEntry);

      if (addEntry (model, newEntry, index))
      {
        // select and edit entry

        // handle perverse models that do not actually add the item at the
        // entry specified.
        int newIndex = model.indexOf (newEntry);

        if (newIndex != -1)
        {
          list.setSelectedIndex (newIndex);
          list.ensureIndexIsVisible (newIndex);
        }
      }
    }
  }

  /**
   * Subclasses may override this to change the way the entry is added to
   * a model.
   *
   * @return True if the entry was added.
   */
  protected boolean addEntry (DfcListModel model, Object entry, int index)
  {
    if (model.canAddEntry (entry, index))
    {
      model.addEntry (entry, index);

      return true;
    } else
    {
      return false;
    }
  }

  /**
   * Subclasses must override this to create a new entry for the list eg
   * via showing a dialog.
   *
   * @return A new entry to be added to the list, or null if no entry should
   * be added.
   */
  protected abstract Object createNewEntry ();

  /**
   * Get the index for an entry to the list - subclasses may choose to
   * override this.  Default is to return the end of the list.
   */
  protected int getIndexForNewEntry (Object newEntry)
  {
    return list.getModel ().getSize ();
  }
}
