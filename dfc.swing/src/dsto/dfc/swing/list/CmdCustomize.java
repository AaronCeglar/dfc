package dsto.dfc.swing.list;

import dsto.dfc.swing.event.SelectionEvent;
import dsto.dfc.swing.event.SelectionListener;
import dsto.dfc.swing.forms.AbstractShowCustomizerCommand;

/**
 * Base implementation of the properties command that attempts to show
 * the selected list entry's FormEditor-compliant customizer.
 *
 * @author Matthew
 * @version $Revision$
 */
public class CmdCustomize
  extends AbstractShowCustomizerCommand implements SelectionListener
{
  private DfcList list;

  public CmdCustomize (DfcList list)
  {
    super (list);

    this.list = list;

    list.addSelectionListener (this);
  }

  public boolean isDirectEdit ()
  {
    return true;
  }

  public String getDescription ()
  {
    return "Show the properties of the selected item";
  }

  public Object getCustomizerValue ()
  {
    return list.getSelectedValue ();
  }

  public void setCustomizerValue (Object newValue)
  {
    // zip: value is edited directly
  }

  public void selectionChanged (SelectionEvent e)
  {
    customizerValueChanged ();
  }
}
