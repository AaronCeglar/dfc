package dsto.dfc.swing;

import java.awt.Component;

import javax.swing.SwingUtilities;

import dsto.dfc.databeans.PropertyEvent;
import dsto.dfc.databeans.PropertyListener;

/**
 * A DataObject property listener adapter that re-routes property
 * change events that occur outside the UI thread into the UI thread
 * by using SwingUtilities.invokeLater ().
 * 
 * @author Matthew Phillips
 */
public class UIPropertyListener implements PropertyListener
{
  protected PropertyListener listener;

  /**
   * Shortcut to create a listener for a component that is also the
   * property listener.
   * 
   * @param control The control: must implement
   *          {@link PropertyListener}.
   */
  public UIPropertyListener (Component control)
  {
    this ((PropertyListener)control);
  }
  
  /**
   * Create a new instance.
   * 
   * @param listener The listener that will be called by this one,
   *          guaranteed from the UI thread.
   */
  public UIPropertyListener (PropertyListener listener)
  {
    this.listener = listener;
  }

  public void propertyValueChanged (PropertyEvent e)
  {
    if (SwingUtilities.isEventDispatchThread ())
      listener.propertyValueChanged (e);
    else
      SwingUtilities.invokeLater (new Callback (e));
  }
  
  private class Callback implements Runnable
  {
    private final PropertyEvent e;

    public Callback (PropertyEvent e)
    {
      this.e = e;
    }
   
    public void run ()
    {
      listener.propertyValueChanged (e);
    }
  }
}
