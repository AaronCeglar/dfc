package dsto.dfc.swing.status;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.AncestorEvent;

import dsto.dfc.swing.icons.Iconic;
import dsto.dfc.swing.icons.NullIcon;

/**
 * A status bar control that displays the current status of a StatusManager.
 * If no status manager is explictly set, the control finds/creates one for
 * the current window using {@link StatusManager#getOrCreateStatusManager}.
 * Also auto-creates a {@link MenuStatusAdapter} to connect the status manager
 * with menu status changes.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class StatusBar extends JComponent
{
  /** NOTE: when no icon is displayed message label uses DEFAULT_ICON as a
   *  filler to stop resizing the status bar. */
  private static final Icon DEFAULT_ICON = new NullIcon (0, 16);

  private StatusManager manager;
  private StatusManagerListener managerListener = new StatusManagerListener ();
  private JLabel messageLabel = new JLabel();
  private BorderLayout borderLayout1 = new BorderLayout();
  private Border border1;

  public StatusBar ()
  {
    try
    {
      jbInit ();
    } catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public void setStatusManager (StatusManager newManager)
  {
    if (manager != null)
      manager.removePropertyChangeListener (managerListener);

    this.manager = newManager;

    manager.addPropertyChangeListener (managerListener);

    statusChanged ();

    // auto-create an adapter for menu selections and log events
    new MenuStatusAdapter (manager);
    new LogStatusAdapter (manager);
  }

  protected void statusChanged ()
  {
    if (SwingUtilities.isEventDispatchThread ())
      doUpdateStatus ();
    else
    {
      // not in swing thread, update later
      SwingUtilities.invokeLater (new Runnable ()
      {
        public void run ()
        {
          doUpdateStatus ();
        }
      });
    }
  }

  protected void doUpdateStatus ()
  {
    Object status = manager.getStatus ();

    if (status instanceof Iconic)
    {
      Iconic iconic = (Iconic)status;

      if (iconic.getIcon () instanceof NullIcon)
        messageLabel.setIcon (DEFAULT_ICON);
      else
        messageLabel.setIcon (iconic.getIcon ());

      messageLabel.setText (iconic.getName ());
    } else
    {
      messageLabel.setIcon (DEFAULT_ICON);
      messageLabel.setText (status == null ? "" : status.toString ());
    }

    // this is here so that messages that indicate progress appear
    // straight away.  This can go away when real progress indicators
    // are available.
    messageLabel.paintImmediately
      (0, 0, messageLabel.getWidth (), messageLabel.getHeight ());
  }

  protected void componentShown ()
  {
    // auto-attach to status manager if none explicitly set
    if (manager == null)
      setStatusManager (StatusManager.getOrCreateStatusManager (this));
  }

  private void jbInit () throws Exception
  {
    border1 = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134)),BorderFactory.createEmptyBorder(3,6,3,6));
    this.setBorder(border1);
    this.addAncestorListener(new javax.swing.event.AncestorListener()
    {
      public void ancestorAdded(AncestorEvent e)
      {
        this_ancestorAdded(e);
      }
      public void ancestorRemoved(AncestorEvent e)
      {
        // zip
      }
      public void ancestorMoved(AncestorEvent e)
      {
        // zip
      }
    });
    this.addComponentListener(new java.awt.event.ComponentAdapter()
    {
      public void componentShown(ComponentEvent e)
      {
        StatusBar.this.componentShown ();
      }
    });
    this.setLayout (borderLayout1);
    messageLabel.setMinimumSize(new Dimension(50, 17));
    messageLabel.setText("Test");
    messageLabel.setIcon (DEFAULT_ICON);
    this.add(messageLabel, BorderLayout.CENTER);
  }

  /**
   * Called when this is added to a component hierachy.
   */
  protected void this_ancestorAdded(AncestorEvent e)
  {
    componentShown ();
  }

  class StatusManagerListener implements PropertyChangeListener
  {
    public void propertyChange (PropertyChangeEvent e)
    {
      statusChanged ();
    }
  }
}