package dsto.dfc.swing.status;

import java.awt.Component;
import java.awt.Frame;
import java.awt.Window;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import dsto.dfc.util.BasicPropertyEventSource;

/**
 * Manages a stack of status messages.  Used as the underlying 'model'
 * for the StatusBar control.
 *
 * @see StatusBar
 * @see MenuStatusAdapter
 * @see LogStatusAdapter
 */
public class StatusManager extends BasicPropertyEventSource
{
  private ArrayList entries = new ArrayList ();
  private boolean softStatus = false;

  /**
   * Use this when the status bar is not put inside a JFrame and therefore there
   * is not .getClientProperty()/.putClientProperty() methods.
   */
  private static HashMap frameToManager = new HashMap ();

  public StatusManager ()
  {
    pushStatus ("Ready");
  }

  /**
   * Get or create a status manager for the window containing a given
   * component.
   */
  public static StatusManager getOrCreateStatusManager (Component c)
    throws IllegalArgumentException
  {
    Window window = SwingUtilities.windowForComponent (c);

    if (window instanceof JFrame)
    {
      JFrame frame = (JFrame)window;

      StatusManager manager =
        (StatusManager)frame.getRootPane ().getClientProperty (StatusManager.class);

      if (manager == null)
      {
        manager = new StatusManager ();
        frame.getRootPane ().putClientProperty (StatusManager.class, manager);
      }

      return manager;
    } else if (window instanceof Frame)
    {
      Frame frame = (Frame)window;

      StatusManager manager =
        (StatusManager) frameToManager.get(frame);

      if (manager == null)
      {
        manager = new StatusManager ();
        frameToManager.put (frame, manager);
      }

      return manager;
    } else
      throw new IllegalArgumentException ("Cannot get status manager on non-Frame");
  }

  public void pushStatus (Object status)
  {
    pushStatus (status, false);
  }

  /**
   * Push a new status object (usually a String or Iconic value) onto
   * the status stack.  Fires a 'status' property change event
   *
   * @param status The new status.
   * @param soft If true, the new status is marked 'soft' and will be
   * automatically replaced by the next call to pushStatus ().
   */
  public void pushStatus (Object status, boolean soft)
  {
    Object oldStatus = getStatus ();

    if (softStatus)
      popStatus ();

    softStatus = soft;

    entries.add (status);

    firePropertyChange ("status", oldStatus, status);
  }

  /**
   * Pop the current status object off the stack. Fires a 'status'
   * property change event
   *
   * @return The old status (or null if the stack is empty).
   */
  public Object popStatus ()
  {
    if (entries.size () > 0)
    {
      Object oldStatus = getStatus ();

      entries.remove (entries.size () - 1);
      softStatus = false;

      firePropertyChange ("status", oldStatus, getStatus ());

      return oldStatus;
    } else
      return null;
  }

  /**
   * The current top of the status stack (null if the stack is empty).
   *
   * @see #getStatusMessage
   */
  public Object getStatus ()
  {
    if (entries.size () > 0)
      return entries.get (entries.size () - 1);
    else
      return null;
  }

  /**
   * The string value of the top of the status stack ("" if the stack
   * is empty).
   *
   * @see #getStatus
   */
  public String getStatusMessage ()
  {
    if (entries.size () > 0)
      return entries.get (entries.size () - 1).toString ();
    else
      return "";
  }
}
