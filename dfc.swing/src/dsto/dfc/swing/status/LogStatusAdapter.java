package dsto.dfc.swing.status;

import dsto.dfc.logging.Log;
import dsto.dfc.logging.LogEvent;
import dsto.dfc.logging.LogListener;
import dsto.dfc.swing.icons.SimpleIconic;
import dsto.dfc.swing.logging.LogMessageTable;

/**
 * Sends log events to a status manager.
 *
 * @author Matthew Phillips
 * @version $Revision$
 *
 * @see StatusManager
 * @see dsto.dfc.logging.Log
 * @see dsto.dfc.logging.LogEvent
 */
public class LogStatusAdapter implements LogListener
{
  private StatusManager manager;

  public LogStatusAdapter (StatusManager manager)
  {
    this.manager = manager;

    Log.addLogListener (this);
  }

  public void dispose ()
  {
    Log.removeLogListener (this);
  }

  // LogListener interface

  public void messageReceived (LogEvent e)
  {
    int type = e.getType ();

    if (type != Log.TRACE && type != Log.PERFORMANCE && type != Log.DIAGNOSTIC)
    {
      String message = e.getMessage ();

      // add exception to message
      if (e.getException () != null)
        message += " (" + e.getException () + ")";

      SimpleIconic status =
        new SimpleIconic (message, LogMessageTable.getIconForEvent (e));

      manager.pushStatus (status, true);
    }
  }
}
