package dsto.dfc.swing.status;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.MenuElement;
import javax.swing.MenuSelectionManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import dsto.dfc.swing.icons.SimpleIconic;

/**
 * Listens to the global MenuSelectionManager class and auto sets/unsets the
 * current status of the StatusManager to the description associated with
 * the currently selected menu item.
 *
 * @author Matthew Phillips
 * @version $Revision$
 *
 * @see StatusManager
 * @see StatusBar
 */
public class MenuStatusAdapter implements ChangeListener
{
  private StatusManager manager;
  private boolean showingStatus = false;
  private JComponent lastSelected = null;

  public MenuStatusAdapter (StatusManager manager)
  {
    this.manager = manager;

    MenuSelectionManager.defaultManager ().addChangeListener (this);
  }

  // ChangeListener interface (menu selection changed)

  public void stateChanged (ChangeEvent e)
  {
    MenuElement [] selectedPath =
      MenuSelectionManager.defaultManager ().getSelectedPath ();
    JComponent selected = null;
    Object status = null;

    // try to extract a menu item description
    if (selectedPath != null &&
        selectedPath.length > 0 &&
        selectedPath [selectedPath.length - 1] instanceof JComponent)
    {
      selected = (JComponent)selectedPath [selectedPath.length - 1];

      String description =
        (String)selected.getClientProperty (Action.SHORT_DESCRIPTION);

      Icon icon = (Icon)selected.getClientProperty (Action.SMALL_ICON);

      if (icon != null)
        status = new SimpleIconic (description, icon);
      else
        status = description;
    }

    if (status != null)
    {
      // catch multiple selections that occur when context menu pops up
      if (selected != lastSelected)
      {
        lastSelected = selected;

        if (showingStatus)
          manager.popStatus ();

        showingStatus = true;
        manager.pushStatus (status);
      }
    } else if (showingStatus)
    {
      showingStatus = false;
      lastSelected = null;

      manager.popStatus ();
    }
  }
}