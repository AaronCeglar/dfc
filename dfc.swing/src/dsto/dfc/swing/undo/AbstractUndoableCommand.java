package dsto.dfc.swing.undo;

import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;

import dsto.dfc.swing.commands.AbstractMutableCommand;
import dsto.dfc.swing.commands.CommandView;

/**
 * Abstract super class for undo and redo commands. Provides exception handling
 * and cursor listening behaviour.
 *
 * @author Peter J Smet
 * @version $Revision$
 */
public abstract class AbstractUndoableCommand extends AbstractMutableCommand
  implements EditExceptionPolicy
{
  protected String               name;
  protected MonitoredUndoManager undoManager;
  protected CursorUpdatePolicy   cursorUpdatePolicy;
  protected EditExceptionHandler exceptionHandler = EditExceptionHandler.createStandardHandler ();

  /**
   * Used by subclasses to register the {@link MonitoredUndoManager} being
   * listened to and construct the cursor update policy.
   */
  public AbstractUndoableCommand (MonitoredUndoManager undoManager)
  {
    this.undoManager   = undoManager;
    cursorUpdatePolicy = new CursorUpdatePolicy (undoManager, this);
  }

  /** Implement this method with the command's behaviour. */
  public abstract void execute (UndoableEdit edit);

  /** Implement this method to return the edit in question. */
  public abstract UndoableEdit selectedEdit ();

  /** Implement this method to indicate whether the command is enabled or not. */
  public abstract void updateEnabled ();

  /**
   * Implement this command to determine how to skip an edit which causes an
   * exception.
   */
  public abstract void skip ();

  /** Tries to re-execute the undo or redo. */
  public void retry ()
  {
    execute ();
  }

  /** Override this method to add behaviour when pausing. */
  public void pause ()
  {
    // zip
  }

  /** Template method, should not be overridden */
  public void execute ()
  {
    try
    {
      execute (selectedEdit ());
    }
    catch (CannotUndoException ex)
    {
      handleNonExecutableEdit ();
    }
    catch (CannotRedoException ex)
    {
      handleNonExecutableEdit ();
    }
    finally
    {
      exceptionHandler.skipAll (false);
    }
  }

  /** Template method, should not be overridden. */
  protected int handleNonExecutableEdit ()
  {
    String errorMessage = "Error in " + getName ();
    return exceptionHandler.handleNonExecutableEdit (errorMessage, this);
  }

  /**
   * The boolean <i>listen</i> indicates whether the command should allow
   * itself to be enable-able by listening to cursor changes.
   */
  public void listenForEnablingEvents (boolean listen)
  {
    if (listen)
    {
      listenForEnablingEvents ();
      updateEnabled ();
    }
    else
    {
      stopListeningForEnablingEvents  ();
      setEnabled (false);
    }
  }

  /** Start listening for enabling cursor changes. */
  protected void listenForEnablingEvents ()
  {
    cursorUpdatePolicy.listenForCursorChanges ();
  }

  /** Stop listening for enabling cursor changes. */
  protected void stopListeningForEnablingEvents ()
  {
    cursorUpdatePolicy.stopListeningForCursorChanges ();
  }

  public boolean isInteractive ()
  {
    return false;
  }

  public String getName ()
  {
    return name;
  }

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return "Edit.undoRedo";

    return "undoRedo";
  }
}