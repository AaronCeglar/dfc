package dsto.dfc.swing.undo;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Policy for refreshing the enabled state of an {@link AbstractUndoableCommand}
 * when the cursor of the {@link MonitoredUndoManager} changes.
 *
 * @author Peter J Smet
 * @version $Revision$
 */
class CursorUpdatePolicy implements PropertyChangeListener
{
  /** The {@link MonitoredUndoManager} being listened to for cursor changes. */
  MonitoredUndoManager cursorEventSource;

  /** The {@link AbstractUndoableCommand} to update. */
  AbstractUndoableCommand commandToUpdate;

  /**
   * Constructs a instance which listens to the
   * <i>cursorEventSource</i> for cursor changes and updates the
   * <i>commandToUpdate</i> when it hears them. This constructor automatically
   * activates the policy.
   */
  public CursorUpdatePolicy (MonitoredUndoManager cursorEventSource,
                             AbstractUndoableCommand commandToUpdate)
  {
    this.cursorEventSource = cursorEventSource;
    this.commandToUpdate   = commandToUpdate;
    listenForCursorChanges ();
  }

  protected void listenForCursorChanges () // prevent multiple listener additions
  {
    cursorEventSource.removePropertyChangeListener (this);
    cursorEventSource.addPropertyChangeListener    (this);
  }

  protected void stopListeningForCursorChanges ()
  {
    cursorEventSource.removePropertyChangeListener (this);
  }

  public void propertyChange (PropertyChangeEvent evt)
  {
    if (evt.getPropertyName ().endsWith ("Cursor"))
      commandToUpdate.updateEnabled ();
  }
}