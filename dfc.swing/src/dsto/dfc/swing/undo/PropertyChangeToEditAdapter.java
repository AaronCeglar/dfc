package dsto.dfc.swing.undo;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.undo.UndoableEditSupport;

/**
 * Class which listens for PropertyChangeEvents and fires PropertyEdits when
 * it hears them.
 *
 * @author Peter J Smet
 * @version $Revision$
 */
public class PropertyChangeToEditAdapter extends UndoableEditSupport
  implements PropertyChangeListener
{
  public void propertyChange (PropertyChangeEvent evt)
  {
    postEdit (new PropertyEdit (evt));
  }
}