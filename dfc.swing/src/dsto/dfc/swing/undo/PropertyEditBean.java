package dsto.dfc.swing.undo;

/**
 * Bean PropertyEdit class for Bean serialisation support. Subclass this class
 * if you want your property edits to be bean serialisable. Getter and setter
 * methods simply get and set the appropriate properties.
 *
 * @author Peter J Smet
 * @version $Revision$
 */
public class PropertyEditBean extends PropertyEdit
{
  private static final String DESCRIPTION =
    "Bean-serialisable form of a PropertyEdit, which records changes" +
    " in a property of a source object";

  /** Vanilla constructor for bean serialisation support. */
  public PropertyEditBean ()
  {
    // zip
  }

  /** Need a value constructor because constructors are not inherited. */
  public PropertyEditBean (Object source, String propertyName, Object oldValue, Object newValue)
  {
    super (source, propertyName, oldValue, newValue);
  }

  // getters and setters provided for serialization/deserialization support only
  // use at own risk

  public Object getSource ()
  {
    return source;
  }

  public void setSource (Object newSource)
  {
    source = newSource;
  }

  public void setOldValue (Object newOldValue)
  {
    oldValue = newOldValue;
  }

  public Object getOldValue ()
  {
    return oldValue;
  }

  public void setNewValue (Object newNewValue)
  {
    newValue = newNewValue;
  }

  public Object getNewValue ()
  {
    return newValue;
  }

  public String getDescription ()
  {
    return DESCRIPTION;
  }
}