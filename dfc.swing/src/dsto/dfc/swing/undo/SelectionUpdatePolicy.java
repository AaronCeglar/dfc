package dsto.dfc.swing.undo;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * Provides an {@link AbstractUndoableCommand} with the capability to listen to
 * {@link javax.swing.event.ListSelectionEvent}s from an {@link EditListView}.
 *
 * @author Peter J Smet
 * @version $Revision$
 */
class SelectionUpdatePolicy implements ListSelectionListener
{
  /** The {@link EditListView} being listened to. */
  EditListView selectionEventSource;

  /** The {@link AbstractUndoableCommand} which needs to be updated. */
  AbstractUndoableCommand commandToUpdate;

  public SelectionUpdatePolicy (EditListView selectionEventSource,
                                AbstractUndoableCommand commandToUpdate)
  {
    this.selectionEventSource = selectionEventSource;
    this.commandToUpdate      = commandToUpdate;
    listenForSelectionChanges ();
  }

  /**
   * The commandToUpdate invokes this method to start listening for
   * {@link javax.swing.event.ListSelectionEvent}s.
   */
  protected void listenForSelectionChanges () // prevent multiple listener additions
  {
    selectionEventSource.removeListSelectionListener (this);
    selectionEventSource.addListSelectionListener    (this);
  }

  /**
   * The commandToUpdate invokes this method to stop listening for
   * {@link javax.swing.event.ListSelectionEvent}s.
   */
  protected void stopListeningForSelectionChanges ()
  {
    selectionEventSource.removeListSelectionListener (this);
  }

  // ListSelectionListener interface

  /**
   * Invoked when the selection in the selectionEventSource has
   * changed. This, in turn, updates the commandToUpdate.
   */
  public void valueChanged (ListSelectionEvent e)
  {
    commandToUpdate.updateEnabled ();
  }
}