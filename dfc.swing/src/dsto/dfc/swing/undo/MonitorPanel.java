package dsto.dfc.swing.undo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.metal.MetalScrollBarUI;

import dsto.dfc.swing.SwingSupport;
import dsto.dfc.swing.commands.Command;
import dsto.dfc.swing.commands.CommandActionAdapter;
import dsto.dfc.swing.commands.CommandMenus;
import dsto.dfc.swing.commands.CommandSource;
import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.swing.panels.ActivePanelEdit;
import dsto.dfc.swing.panels.StandardCommandViews;

/**
 * Hosts list of edits and allows display/selection using a {@link
 * ParametersPanel}.
 *
 * @author Peter J Smet
 * @author Derek Weber
 * @version $Revision$
 */
public class MonitorPanel extends JPanel implements CommandSource
{
  protected MonitoredUndoManager undoManager;
  protected CmdUndo              undoCmd;
  protected CmdRedo              redoCmd;
  protected CmdUndoTo            undoToCmd;
  protected CmdRedoTo            redoToCmd;
  protected EditListView         editListView;
  protected ParametersPanel      parametersPanel;
  protected StandardCommandViews cmdViews           = new StandardCommandViews ();

  protected JScrollPane          editListScrollPane = new JScrollPane ();
  protected JButton              undoButton         = new JButton (CmdUndo.ICON);
  protected JButton              redoButton         = new JButton (CmdRedo.ICON);
  protected JCheckBox            lockPanelsOption   = new JCheckBox ("Lock Active Panel");
  protected JPanel               bottomPanel        = new JPanel ();
  protected JPanel               buttonPanel        = new JPanel (new GridLayout (2, 2));


  // JBuilder GUI stuff
  protected JSplitPane    splitPane      = new JSplitPane (JSplitPane.VERTICAL_SPLIT, true);
  protected BorderLayout  borderLayout1  = new BorderLayout ();
  protected GridBagLayout gridBagLayout1 = new GridBagLayout ();
  protected GridLayout    gridLayout1    = new GridLayout ();
  protected TitledBorder  titledBorder1;
  protected TitledBorder  titledBorder2;
  protected Border        border1;
  protected Border        border2;
  protected Border        border3;
  protected Border        border4;
  protected Border        border5;

  /**
   * Creates a {@link MonitorPanel} which displays the {@link
   * javax.swing.undo.UndoableEdit}s heard by <i>undoManager</i>.
   */
  public MonitorPanel (MonitoredUndoManager undoManager)
  {
    super (new BorderLayout (5, 5));

    this.undoManager = undoManager;
    editListView     = new EditListView (undoManager);
    parametersPanel  = new ParametersPanel (editListView);

    initialiseCommands ();

    try
    {
      jbInit ();
    }
    catch (Exception e)
    {
      e.printStackTrace ();
    }
  }

  public CommandView getCommandView (String viewName)
  {
    return cmdViews.getCommandView (viewName);
  }

  /**
   * Initialises the {@link MonitorPanel}'s commands, including the {@link
   * CmdUndo} and {@link CmdRedo} in the control panel, and the {@link CmdUndoTo}
   * and {@link CmdRedoTo} in the {@link EditListView}'s context menu.
   */
  public void initialiseCommands ()
  {
    undoCmd   = new CmdUndo   (undoManager);
    redoCmd   = new CmdRedo   (undoManager);

    undoToCmd = new CmdUndoTo (editListView, undoManager);
    redoToCmd = new CmdRedoTo (editListView, undoManager);

    bindCommandToButton (undoCmd, undoButton);
    bindCommandToButton (redoCmd, redoButton);

    CommandView contextMenu = getCommandView (CommandView.CONTEXT_MENU_VIEW);
    contextMenu.addCommand (undoToCmd);
    contextMenu.addCommand (redoToCmd);

    CommandMenus.installContextMenu (editListView, contextMenu);
  }

  /**
   * If <i>lock</i> is true, then undoing or redoing panel switching edits will
   * have no effect (which would hide the {@link MonitorPanel}).
   */
  public void lockActivePanelDuringPlayback (boolean lock)
  {
    ActivePanelEdit.PANELS_UNLOCKED = ! lock;
  }

  public Command getUndoCommand ()
  {
    return undoCmd;
  }

  public Command getRedoCommand ()
  {
    return redoCmd;
  }

  public EditListView getEditListView ()
  {
    return editListView;
  }

  public ParametersPanel getParametersPanel ()
  {
    return parametersPanel;
  }

  public MonitoredUndoManager getUndoManager ()
  {
    return undoManager;
  }

  /** Attaches a <i>command</i> to a <i>button</i>. */
  protected void bindCommandToButton (Command command, AbstractButton button)
  {
    SwingSupport.setAction (button, new CommandActionAdapter (command));
    button.setText (null);
  }

  // ----- JBuilder method -----------------------------------------------------

  private void jbInit () throws Exception
  {
    border1 = BorderFactory.createEtchedBorder (Color.white, new Color (148, 145, 140));
    titledBorder1 = new TitledBorder (border1, "Edits");
    border2 = BorderFactory.createCompoundBorder (new TitledBorder (BorderFactory.createEtchedBorder (Color.white, new Color (148, 145, 140)), "  Edits  "),BorderFactory.createEmptyBorder(0,0,3,0));
    border3 = new TitledBorder (BorderFactory.createEtchedBorder (Color.white, new Color (148, 145, 140)), "Edit Parameters");
    border4 = BorderFactory.createCompoundBorder (border3, BorderFactory.createEmptyBorder (12,0,0,0));
    border5 = BorderFactory.createEtchedBorder (Color.white, new Color (148, 145, 140));
    titledBorder2 = new TitledBorder (BorderFactory.createEtchedBorder (Color.white, new Color (148, 145, 140)), "  Edit Parameters  ");
    this.setLayout (borderLayout1);
    setPreferredSize (new Dimension (150, 400));

    editListScrollPane.setVerticalScrollBarPolicy (JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    editListScrollPane.setBorder (border2);

    addBordersToScrollBars ();

    bottomPanel.setLayout (gridBagLayout1);
    parametersPanel.setBorder (titledBorder2);
    buttonPanel.setLayout (gridLayout1);
    gridLayout1.setColumns (2);
    gridLayout1.setHgap (3);
    gridLayout1.setVgap (3);

    setUpLockPanelsOption ();

    bottomPanel.add(lockPanelsOption,
      new GridBagConstraints (0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets (0, 0, 0, 0), 0, 0));

    bottomPanel.add(parametersPanel, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(1, 0, 0, 0), 0, 0));
    bottomPanel.add(buttonPanel, new GridBagConstraints (0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets (0, 0, 0, 0), 0, 0));

    buttonPanel.add(undoButton, null);
    buttonPanel.add(redoButton, null);

    editListScrollPane.getViewport ().add (editListView);

    setUpSplitPane ();
  }

  private void addBordersToScrollBars ()
  {
    addBorderToScrollBar (editListScrollPane.getHorizontalScrollBar ());
    addBorderToScrollBar (editListScrollPane.getVerticalScrollBar   ());
  }

  private void addBorderToScrollBar (JScrollBar scrollBar)
  {
    scrollBar.putClientProperty (MetalScrollBarUI.FREE_STANDING_PROP, Boolean.TRUE);
  }

  private void setUpSplitPane ()
  {
    SwingSupport.killSplitPaneBorder (splitPane);
    add (splitPane, BorderLayout.CENTER);
    splitPane.add (editListScrollPane, JSplitPane.TOP);
    splitPane.add (bottomPanel, JSplitPane.BOTTOM);
    splitPane.setDividerLocation (300);
  }

  private void setUpLockPanelsOption ()
  {
    lockPanelsOption.setSelected (! ActivePanelEdit.PANELS_UNLOCKED);
    lockPanelsOption.setToolTipText ("Locks active panel during undo/redo operations");
    lockPanelsOption.setHorizontalAlignment (SwingConstants.CENTER);
    lockPanelsOption.addActionListener (new ActionListener ()
    {
      public void actionPerformed (ActionEvent e)
      {
        lockActivePanelDuringPlayback (lockPanelsOption.isSelected ());
      }
    });
  }
}
