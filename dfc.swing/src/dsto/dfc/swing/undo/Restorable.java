package dsto.dfc.swing.undo;

import java.util.HashMap;

import dsto.dfc.swing.panels.PanelManager;

/**
 * Some {@link javax.swing.undo.UndoableEdit} must execute within a context.
 * This context cannot be saved and restored, and must therefore be explicitly
 * set on such edits.<p>
 *
 * Deserialized edits that rely on context are not valid until this context has
 * been restored. Example: {@link PanelManager}.<p>
 *
 * Each type of {@linl javax.swing.undo.UndoableEdit} is responsible for
 * restoring its context from the {@link java.util.HashMap}. For example:
 * <pre>
 *   context.get ("panelManager");
 * </pre>
 * <p>
 *
 * @author  Peter J Smet
 * @version $Revision$
 */
public interface Restorable
{
  public void restoreContext (HashMap context);
}