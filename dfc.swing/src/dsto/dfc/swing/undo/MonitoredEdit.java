package dsto.dfc.swing.undo;

import java.io.Serializable;

import javax.swing.undo.UndoableEdit;

import dsto.dfc.swing.icons.Iconic;
import dsto.dfc.util.Copyable;

/**
 * An extended undoable edit.  This interface exists to provide any
 * InVision-specific functionality required for using events to
 * generate processes.  3rd party components may only generate
 * UndoableEdits, so it cannot be assumed that we only deal with
 * MonitoredEdits.
 *
 * @version $Revision$
 */
public interface MonitoredEdit
  extends UndoableEdit, Restorable, Serializable, Copyable, Iconic
{
  /**
   * Get the (human readable) names of the parameters to this command.
   * May be null or empty to indicate no parameters.
   *
   * @see #getParameterValues
   */
  public String [] getParameterNames ();

  /**
   * Get the parameter values of this command.
   * May be null or empty to indicate no parameters.
   *
   * @see #getParameterNames
   */
  public Object [] getParameterValues ();

  /**
   * Returns a human-readable description of what the edit does.
   */
  public String getDescription ();
}
