package dsto.dfc.swing.undo;

import java.awt.Event;
import java.awt.event.KeyEvent;

import javax.swing.Icon;
import javax.swing.KeyStroke;
import javax.swing.undo.UndoableEdit;

import dsto.dfc.swing.icons.ImageLoader;

/**
 * Command to invoke a single redo () on the {@link MonitoredUndoManager}.
 *
 * @author Derek Weber
 * @version $Revision$
 */
public class CmdRedo extends AbstractUndoableCommand
{
  public static final Icon ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/redo.gif");

  public CmdRedo (MonitoredUndoManager undoManager)
  {
    super (undoManager);
    name = "Redo";
    setEnabled (! undoManager.redoCursorIsNull ());
  }

  public Icon getIcon ()
  {
    return ICON;
  }

  public char getMnemonic ()
  {
    return 'r';
  }

  public void execute (UndoableEdit edit)
  {
    undoManager.redoTo (edit);
  }

  /** Returns the next edit to be redone. */
  public UndoableEdit selectedEdit ()
  {
    return undoManager.getRedoCursor ();
  }

  public String getDescription ()
  {
    return "Redo the last undone change";
  }

  /**
   * This checks that the selected edit is in the {@link MonitoredUndoManager}'s
   * redo list.
   */
  public void updateEnabled ()
  {
    setEnabled (undoManager.inRedoList (selectedEdit ()));
  }

  public void skip ()
  {
    undoManager.skipRedo ();
  }

  /** The keyboard accelerator is Ctrl-R. */
  public KeyStroke getAccelerator ()
  {
    return KeyStroke.getKeyStroke (KeyEvent.VK_R, Event.CTRL_MASK);
  }
}