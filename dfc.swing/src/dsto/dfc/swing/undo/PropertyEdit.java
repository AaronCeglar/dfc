package dsto.dfc.swing.undo;

import java.beans.PropertyChangeEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;

import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;

import dsto.dfc.util.Beans;

/**
 * Used to undo property changes.
 *
 * @author Peter J Smet
 * @author Derek Weber
 * @version $Revision$
 */
public class PropertyEdit extends AbstractMonitoredEdit
{
  static String description =
    "Records a change in the value of a property on a source object";

  /** The source of the property change. */
  public transient Object source;
  /** The name of the changed property. */
  public           String propertyName;
  /** The old value of the property. */
  public transient Object oldValue;
  /** The new value of the property. */
  public transient Object newValue;

  /** For bean serialisation only */
  public PropertyEdit ()
  {
    // zip
  }

  /** Constructs a {@link PropertyEdit} with the supplied information. */
  public PropertyEdit (Object source,
                       String propertyName,
                       Object oldValue,
                       Object newValue)
  {
    super (new String[] { "old " + propertyName, "new " + propertyName },
           new Object[] { oldValue, newValue });
    this.source       = source;
    this.propertyName = propertyName;
    this.oldValue     = oldValue;
    this.newValue     = newValue;
  }

  /**
   * Constructs a {@link PropertyEdit} from the {@link
   * java.beans.PropertyChangeEvent}.
   */
  public PropertyEdit (PropertyChangeEvent event)
  {
    this (event.getSource (),
          event.getPropertyName (),
          event.getOldValue (),
          event.getNewValue ());
  }

  // Bean methods

  public String getPropertyName ()
  {
    return propertyName;
  }

  public void setPropertyName (String propertyName)
  {
    this.propertyName = propertyName;
  }

  /** Returns a description of this type of {@link javax.swing.undo.UndoableEdit}. */
  public String getDescription ()
  {
    return description;
  }

  // end Bean methods

  /**
   * Attempts to set the {@link #propertyName} property of {@link #source} to
   * <i>value</i> and throws <i>undoRedoException</i> if it does not succeed.
   * Used by {@link #undo} and {@link #redo}.
   */
  protected void setPropertyValue (Object value, RuntimeException undoRedoException)
  {
    try {
      Beans.setPropertyValue (source, propertyName, value);
    }
    catch (NoSuchMethodException ex)
    {
      throw undoRedoException;
    }
    catch (InvocationTargetException ex)
    {
      throw undoRedoException;
    }
  }

  /** Sets the {@link #source}'s <i>propertyName</i> value to <i>oldValue</i>. */
  public void undo () throws CannotUndoException
  {
    super.undo ();
    setPropertyValue (oldValue, new CannotUndoException ());
  }

  /** Sets the {@link #source}'s <i>propertyName</i> value to <i>newValue</i>. */
  public void redo () throws CannotRedoException
  {
    super.redo ();
    setPropertyValue (newValue, new CannotRedoException ());
  }

  /**
   * Returns a presentation name of the form:
   * <pre>
   *   &quot;{@link #source} " change " {@link #propertyName} [{@link #oldValue}} -> {@link #newValue}]@quot;
   * </pre>
   * If the {@link java.lang.Object#toString} methods of the {@link #source},
   * {@link #oldValue}, or {@link #newValue} are memory references, then their
   * class name is displayed instead for readability purposes.
   */
  public String getPresentationName ()
  {
    String nameOfSource = presentationNameForSource    (source);
    String nameOfOld    = presentationNameForParameter (oldValue);
    String nameOfNew    = presentationNameForParameter (newValue);

    return nameOfSource + " change " + propertyName + " [" +
           nameOfOld + " -> " + nameOfNew + "]";
  }

  private boolean nameIsSuitableForPresentation (Object nameable)
  {
    return ! nameIsDefaultToStringImplementation (nameable.toString ());
  }

  private boolean nameIsDefaultToStringImplementation (String name)
  {
    return name.indexOf ('@') > 0;
  }

  private String presentationNameForParameter (Object object)
  {
    if (nameIsSuitableForPresentation (object))
      return object.toString ();

    return "";
  }

  private String presentationNameForSource (Object object)
  {
    if (nameIsSuitableForPresentation (object))
      return object.toString ();

    return className (object);
  }

  private String className (Object object)
  {
    return stripPackagePrefix (object.getClass ().toString ());
  }

  private String stripPackagePrefix (String packageAndClassName)
  {
    int startClassName = packageAndClassName.lastIndexOf ('.') + 1;
    return packageAndClassName.substring (startClassName);
  }

  public String getUndoPresentationName ()
  {
    return getPresentationName ();
  }

  public String getRedoPresentationName ()
  {
    return getPresentationName ();
  }

  // Serializable methods

  private void writeObject (ObjectOutputStream out) throws IOException
  {
    out.defaultWriteObject ();
    writeSource (out);
    writeValue  (oldValue, out);
    writeValue  (newValue, out);
  }

  /**
   * Override this method if your {@link #source} is not serialisable. Make sure
   * that only one {@link java.lang.Object} is written to the <i>out</i> stream,
   * however.
   */
  protected void writeSource (ObjectOutputStream out) throws IOException
  {
    out.writeObject (source);
  }

  /** Override with value specific code if <i>value</i> is not serialisable. */
  protected void writeValue (Object value, ObjectOutputStream out)
    throws IOException
  {
    out.writeObject (value);
  }

  private void readObject (ObjectInputStream in)
    throws IOException, ClassNotFoundException
  {
    in.defaultReadObject ();
    source   = in.readObject ();
    oldValue = in.readObject ();
    newValue = in.readObject ();
  }
}