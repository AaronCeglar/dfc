package dsto.dfc.swing.undo;

import java.util.List;

import javax.swing.Icon;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.UndoableEdit;

import dsto.dfc.swing.icons.ImageLoader;

/**
 * Groups a number of edits. Design driver is to allow edits executed as
 * part of an InVision process to be grouped together, but could be
 * used for many organisational purposes.
 *
 * @author Peter J Smet
 * @author Derek Weber
 * @version $Revision$
 */
public class MonitoredEditGroup extends AbstractUndoableEdit
{
  private static final String DEFAULT_NAME = "<unnamed>";
  public  static Icon  ICON = ImageLoader.getIcon ("/dsto/dfc/icons/folder_closed.gif");

  private String        name;
  private EditListModel edits;
  private boolean       inProgress = true;

  private transient UndoableEdit  lastEdit;

  /** Constructor for bean serialisation. */
  public MonitoredEditGroup ()
  {
    this (new EditListModel (), DEFAULT_NAME);
  }

  public MonitoredEditGroup (String name)
  {
    this (new EditListModel (), name);
  }

  public MonitoredEditGroup (EditListModel list)
  {
    this (list, DEFAULT_NAME);
  }

  /**
   * Creates a {@link MonitoredEditGroup} with the name <i>name</i> with the
   * {@link javax.swing.undo.UndoableEdit}s in <i>list</i>.
   */
  public MonitoredEditGroup (EditListModel list, String name)
  {
    edits = list;
    this.name = name;
  }

  /**
   * Prevents the further additions of {@link javax.swing.undo.UndoableEdit}s to
   * this {@link MonitoredEditGroup}.
   */
  public void end ()
  {
    inProgress = false;
  }

  /**
   * Returns true if {@link javax.swing.undo.UndoableEdit}s can still be added
   * to this {@link MonitoredEditGroup} and false otherwise.
   */
  public boolean isInProgress ()
  {
    return inProgress;
  }

  /** Returns true if any of its component edits are significant. */
  public boolean isSignificant ()
  {
    for (int i = 0; i < getEditCount (); i++)
    {
      if (getEdit (i).isSignificant ())
        return true;
    }
    return false;
  }

  /**
   * Currently invokes {@link javax.swing.undo.UndoableEdit#undo} on each
   * component {@link javax.swing.undo.UndoableEdit} in reverse order.
   */
  public void undo ()
  {
    for (int i = getEditCount () - 1; i >= 0; i--) // go backwards
      getEdit (i).undo ();
  }

  /**
   * Currently invokes {@link javax.swing.undo.UndoableEdit#redo} on each
   * component {@link javax.swing.undo.UndoableEdit} in order.
   */
  public void redo ()
  {
    for (int i = 0; i < getEditCount (); i++)
      getEdit (i).redo ();
  }

  /** Attempts to merge <i>edit</i> with the last edit. */
  protected boolean mergedEditWithLastEdit (UndoableEdit edit)
  {
    return lastEdit != null && lastEdit.addEdit (edit);
  }

  /** Add an edit to the group. */
  public boolean addEdit (UndoableEdit edit)
  {
    if (!inProgress)
      return false;

    if ( mergedEditWithLastEdit (edit) )
      return true;

    edits.add (edit);
    lastEdit = edit;

    return true;
  }

  /**
   * Indicates that this edit is no longer required and subsequently cannot be
   * used.
   */
  public void die ()
  {
    for (int i = getEditCount () - 1; i >= 0; i--)
    {
      getEdit (i).die ();
      edits.remove (i);
    }
    super.die ();
  }

  /**
   * Access method for the name property.
   *
   * @return The current value of the name property.
   */
  public String getName ()
  {
    return name;
  }

  /**
   * Sets the value of the name property.
   *
   * @param newName The new value of the name property
   */
  public void setName (String newName)
  {
    this.name = newName;
  }

  /**
   * Access method for the edit list.
   *
   * @return The current value of the edits property
   */
  public List getEdits ()
  {
    return edits.getEdits ();
  }

  /**
   * Access method for the number of edits held in the {@link MonitoredEditGroup}
   * at this time.
   *
   * @return The number of edits held.
   */
  public int getEditCount ()
  {
    return edits.getSize ();
  }

  /**
   * Accesses the {@link javax.swing.undo.UndoableEdit} at the
   * <code>index<code>th position.
   *
   * @throws IndexOutOfBoundsException If the index is not within the valid range.
   */
  public UndoableEdit getEdit (int index) throws IndexOutOfBoundsException
  {
    return (UndoableEdit) edits.get (index);
  }

  public String getPresentationName ()
  {
    return getName ();
  }

  public String getUndoPresentationName ()
  {
    return getPresentationName ();
  }

  public String getRedoPresentationName ()
  {
    return getPresentationName ();
  }

  public Icon getIcon ()
  {
    return ICON;
  }
}
