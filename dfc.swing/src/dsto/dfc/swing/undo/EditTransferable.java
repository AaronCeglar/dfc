package dsto.dfc.swing.undo;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Facilitates the dragging and dropping of a {@link java.util.List} of {@link
 * javax.swing.undo.UndoableEdit}s.
 *
 * @author Peter J Smet
 * @author Derek Weber
 * @version $Revision$
 */
public class EditTransferable implements Transferable
{
  /**
   * Note that the class that the DataFlavor is created with cannot be
   * List.class. It must be ArrayList.class (or possibly some other non-abstract
   * class).
   */
  public static final DataFlavor EDIT_LIST_FLAVOR =
    new DataFlavor ("application/x-java-file-list;class=java.util.List", "edit");

  public static final DataFlavor [] FLAVORS = { EDIT_LIST_FLAVOR };
  List edits = new ArrayList ();

  public EditTransferable (List edits)
  {
    this.edits = edits;
  }

  public DataFlavor [] getTransferDataFlavors ()
  {
    return FLAVORS;
  }

  public boolean isDataFlavorSupported (DataFlavor flavor)
  {
    return flavor.equals (EDIT_LIST_FLAVOR);
  }

  public Object getTransferData (DataFlavor flavor)
    throws UnsupportedFlavorException, IOException
  {
    if (isDataFlavorSupported (flavor))
      return edits;

    throw new UnsupportedFlavorException (flavor);
  }
}