package dsto.dfc.swing.undo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import java.io.IOException;

import java.awt.Component;
import java.awt.Font;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JList;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.undo.UndoableEdit;

import dsto.dfc.util.Objects;

import dsto.dfc.logging.Log;

import dsto.dfc.swing.controls.Geometry;
import dsto.dfc.swing.dnd.DragComponent;
import dsto.dfc.swing.dnd.DragSourceAdapter;
import dsto.dfc.swing.dnd.DropComponent;
import dsto.dfc.swing.dnd.DropTargetAdapter;
import dsto.dfc.swing.icons.CompositeIcon;
import dsto.dfc.swing.icons.Iconic;
import dsto.dfc.swing.icons.ImageLoader;

/**
 * List view of an {@link EditListModel}, with a custom renderer which
 * highlights not only the {@link javax.swing.undo.UndoableEdit} in the list,
 * but also the undo cursor of the {@link MonitoredUndoManager} using the edit
 * list. Supports drop'n'drag of {@link MonitoredEdit}s.
 *
 * @author Peter J Smet
 * @author Derek Weber
 * @version $Revision$
 */
public class EditListView extends JList
  implements ListDataListener, PropertyChangeListener, DragComponent, DropComponent
{
  protected MonitoredUndoManager undoManager;
  protected boolean dragNDropEnabled = false;

  /** For bean serialisation. */
  public EditListView ()
  {
    this (new MonitoredUndoManager ());
  }

  public EditListView (MonitoredUndoManager undoManager)
  {
    this (undoManager.getEdits (), undoManager);
  }

  /**
   * Constructs an {@link EditListView} using the edits managed by the {@link
   * MonitoredUndoManager} <i>undoManager</i>.
   */
  public EditListView (ListModel listModel, MonitoredUndoManager undoManager)
  {
    super    (listModel);
    setModel (listModel);
    this.undoManager = undoManager;
    undoManager.addPropertyChangeListener (this);
    setSelectionMode (ListSelectionModel.SINGLE_INTERVAL_SELECTION);

    new DragSourceAdapter (this);
    new DropTargetAdapter (this);

    setCellRenderer (new ShowUndoCursorRenderer ());
  }

  /**
   * Changes the {@link javax.swing.ListModel} that the {@link EditListView}
   * uses.
   */
  public void setModel (ListModel listModel)
  {
    getModel ().removeListDataListener (this);
    super.setModel (listModel);
    getModel ().addListDataListener (this);
  }

  /**
   * Returns the currently selected edit or the {@link
   * MonitoredUndoManager#NULL_EDIT}.
   */
  public UndoableEdit getSelectedEdit ()
  {
    if ( getSelectedValue () == null )
      return MonitoredUndoManager.NULL_EDIT;

    return (UndoableEdit) getSelectedValue ();
  }

  /** Returns an array of the currently selected edits. */
  @SuppressWarnings ("deprecation")
  public List getSelectedEdits ()
  {
    return Arrays.asList (getSelectedValues ());
  }

  /** For programmatically setting an edit as selected. */
  public void setSelectedEdit (UndoableEdit edit)
  {
    setSelectedValue (edit, true);
  }

  /** Returns a list of clones of the selected edits. */
  public List cloneSelectedEdits ()
  {
    return cloneEditList (getSelectedEdits ());
  }

  // PropertyChangeListener Interface

  public void propertyChange (PropertyChangeEvent evt)
  {
    if (evt.getPropertyName ().equals ("edits"))
      setModel ((ListModel) evt.getNewValue ());
  }

  // ListDataListener Interface

  public void intervalAdded (ListDataEvent e)
  {
    ensureIndexIsVisible (e.getIndex1 ());
  }

  public void intervalRemoved (ListDataEvent e)
  {
    clearSelection ();
  }

  public void contentsChanged (ListDataEvent e)
  {
    ensureIndexIsVisible (e.getIndex1 ());
  }

  //---------- DragNDropSupport--------------

  public void setDragNDropEnabled (boolean enabled)
  {
    dragNDropEnabled = enabled;
  }

  public boolean isDragNDropEnabled ()
  {
    return dragNDropEnabled;
  }

  // DragComponent Interface

  public int getSupportedDragActions ()
  {
    return DnDConstants.ACTION_COPY_OR_MOVE;
  }

  public boolean isDragOK (DragGestureEvent e)
  {
    return true;
  }

  public Transferable startDrag (DragGestureEvent e)
  {
    return new EditTransferable (getSelectedEdits ());
  }

  public void endDrag (DragSourceDropEvent e, boolean moveData)
  {
    // zip
  }

  // DropComponent Interface

  public int getSupportedDropActions ()
  {
    return DnDConstants.ACTION_COPY_OR_MOVE;
  }

  public boolean isDropOK (DropTargetDragEvent e)
  {
    return isDragNDropEnabled () &&
           e.isDataFlavorSupported (editFlavor ());
  }

  public void showDragUnderFeedback (boolean dropOK, DropTargetDragEvent e)
  {
    // zip
  }

  public void hideDragUnderFeedback ()
  {
    // zip
  }

  public void executeDrop (DropTargetDropEvent dtde)
  {
    List droppedEdits   = extractEditsFromDropEvent (dtde);
    List clonedEdits    = cloneEditList (droppedEdits);
    int  insertionIndex = indexForDropLocation (dtde.getLocation ());

    boolean dragNDropWithinSameList = undoManager.containsAllEdits (droppedEdits);
    boolean move = dtde.getDropAction () == DnDConstants.ACTION_MOVE;

    if (move && dragNDropWithinSameList)
    {
      undoManager.handleMoveEdits (insertionIndex, droppedEdits);
      selectFirstDroppedEdit (droppedEdits);
    }
    else
    {
      undoManager.insertEdits (insertionIndex, clonedEdits);
      selectFirstDroppedEdit (clonedEdits);
    }
  }

  private List extractEditsFromDropEvent (DropTargetDropEvent dtde)
  {
    try
    {
      if (isDragNDropEnabled ())
        return (List) dtde.getTransferable ().getTransferData (editFlavor ());

      return Collections.EMPTY_LIST;
    }
    catch (UnsupportedFlavorException invalidFlavor)
    {
      return Collections.EMPTY_LIST;
    }
    catch (IOException transferProblem)
    {
      throw new RuntimeException ("DragNDropIOException");
    }
  }

  private List cloneEditList (List originalList)
  {
    ArrayList cloneList = new ArrayList ();

    for (int i = 0; i < originalList.size (); i++)
      try
      {
        cloneList.add ( cloneEdit (originalList.get (i)) );
      }
      catch (CloneNotSupportedException ex)
      {
        Log.internalError ("Cannot clone this edit", originalList.get (i));
      }

    return cloneList;
  }

  private Object cloneEdit (Object edit) throws CloneNotSupportedException
  {
    return Objects.cloneObject (edit);
  }

  private void selectFirstDroppedEdit (List droppedEdits)
  {
    if (droppedEdits.isEmpty ())
      return;

    UndoableEdit firstDroppedEdit = (UndoableEdit) droppedEdits.get (0);
    setSelectedEdit (firstDroppedEdit);
  }

  private int indexForDropLocation (Point dropLocation)
  {
    final int halfLabelHeight = 10;
    Point offsetDropLocation  = new Point (dropLocation.x, dropLocation.y + halfLabelHeight);
    int insertionIndex        = locationToIndex (offsetDropLocation);
    int endOfList             = undoManager.getEditCount ();

    return insertionIndex < 0 ? endOfList : insertionIndex;
  }

  private DataFlavor editFlavor ()
  {
    return EditTransferable.EDIT_LIST_FLAVOR;
  }

  // ----- inner class -------------------------------------------------------

  /**
   * Renders the undo cursor in bold, which is independent of user selection,
   * and displays edit cells as their icons followed by their presentation
   * names.
   */
  class ShowUndoCursorRenderer extends DefaultListCellRenderer
  {
    Icon editIcon      = AbstractMonitoredEdit.ICON;
    Icon selectedIcon  = ImageLoader.getIcon ("/dsto/dfc/icons/arrow_right.gif");

    Font plainFont     = new Font ("Arial", Font.PLAIN, 12);
    Font boldFont      = new Font ("Arial", Font.BOLD,  12);

    public Component getListCellRendererComponent (JList list,
                                                   Object value,
                                                   int index,
                                                   boolean selected,
                                                   boolean hasFocus)
    {
      super.getListCellRendererComponent (list, value, index, selected, hasFocus);

      UndoableEdit edit = (UndoableEdit) value;
      setText (edit.getPresentationName ());

      UndoableEdit editCursor = undoManager.getUndoCursor ();

      boolean editCursorIsSelected = (editCursor == edit);

      if ( editCursorIsSelected )
        setFont (boldFont);
      else
        setFont (plainFont);

      setIcon (edit, editCursorIsSelected);

      return this;
    }

    private void setIcon (UndoableEdit edit, boolean editCursorIsSelected)
    {
      Icon icon = editIcon;

      if (edit instanceof Iconic)
        icon = ((Iconic) edit ).getIcon ();

      if (editCursorIsSelected)
        icon = new CompositeIcon (icon, selectedIcon, Geometry.CENTER);

      setIcon (icon);
    }
  }
}