package dsto.dfc.swing.undo;

/**
 * Interface for components wishing to determine how to react to an exception
 * caused by undoing or redoing an {@link javax.swing.undo.UndoableEdit}.
 *
 * @author Peter J Smet
 * @version $Revision$
 */
public interface EditExceptionPolicy
{
  /** The user decides to skip the offending edit. */
  public void skip ();

  /** The user decides to retry the offending edit. */
  public void retry ();

  /** The user decides to pause the undo or redo at this point. */
  public void pause ();
}
