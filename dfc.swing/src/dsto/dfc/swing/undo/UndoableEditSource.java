package dsto.dfc.swing.undo;

import javax.swing.event.UndoableEditListener;

/**
 * An interface for components wishing to generate {@link
 * javax.swing.undo.UndoableEdit}s.
 *
 * @author Derek Weber
 * @version $Revision$
 */
public interface UndoableEditSource
{
  public void addUndoableEditListener (UndoableEditListener listener);

  public void removeUndoableEditListener (UndoableEditListener listener);
}