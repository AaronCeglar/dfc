package dsto.dfc.swing.undo;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.undo.UndoableEdit;

/**
 * Shows details of a monitored edit, including its parameters in a table.
 *
 * @author Peter J Smet
 * @author Derek Weber
 * @version $Revision$
 */
public class ParametersPanel extends JPanel implements ListSelectionListener
{
  /** Maximum number of parameters displayable for a single edit. */
  public  static int       MAX_PARAMETERS = 10;
  private static Object[]  CLEAR_ALL_DATA = new Object [MAX_PARAMETERS];

  /** The {@link javax.swing.undo.UndoableEdit} selected in the {@link EditListView}. */
  protected UndoableEdit edit;
  /** A link to the {@link EditListView} where the edit is displayed. */
  protected EditListView editListView;

  protected JScrollPane tableScroller   = new JScrollPane ();
  protected JTable      parametersTable = new JTable (new Object [MAX_PARAMETERS][2],
                                                      new Object [] {"Name", "Value"});

  /**
   * Creates a {@link ParametersPanel} which listens to <i>editListView</i> for
   * changes in edit selection.
   */
  public ParametersPanel (EditListView editListView)
  {
    this.editListView = editListView;
    editListView.addListSelectionListener (this);
    setEditable (false);

    setLayout (new GridBagLayout ());
    GridBagConstraints constraints =
      new GridBagConstraints (0, 0, 1, 1, 1.0, 1.0,
          GridBagConstraints.CENTER,
          GridBagConstraints.BOTH,
          new Insets (0, 0, 0, 0), 0, 0);
    add (tableScroller, constraints);
    tableScroller.getViewport ().add (parametersTable);

    setEdit (MonitoredUndoManager.NULL_EDIT);
  }

  /** Accessor for the selected {@link javax.swing.undo.UndoableEdit}. */
  public UndoableEdit getEdit ()
  {
    return edit;
  }

  /**
   * Change the edit managed to <i>newEdit</i> and redisplay the parameter table.
   * Only {@link MonitoredEdit}s can have their parameters displayed.
   */
  public void setEdit (UndoableEdit newEdit)
  {
    edit = newEdit;

    clearTable ();

    boolean editCanBeDisplayed = edit instanceof MonitoredEdit;

    parametersTable.setVisible (editCanBeDisplayed);

    if (editCanBeDisplayed)
      displayParameters ((MonitoredEdit) edit);
  }

  /**
   * Sets the property of the parameters table which allows its cells to be
   * edited.
   */
  public void setEditable (boolean editable)
  {
    parametersTable.setEnabled (editable);
  }

  /** Fill the table with the <i>edit</i>'s parameters and their values. */
  protected void displayParameters (MonitoredEdit newEdit)
  {
    displayParameters (newEdit.getParameterNames (), newEdit.getParameterValues ());
  }

  /** Clear the table's contents. */
  protected void clearTable ()
  {
     displayParameters (CLEAR_ALL_DATA, CLEAR_ALL_DATA);
  }

  /** Display in the table the parameter <i>names</i> and <i>values</i>. */
  protected void displayParameters (Object[] names, Object[] values)
  {
    final int parameterLimit = Math.min (names.length, MAX_PARAMETERS);
    final int nameColumn     = 0;
    final int valueColumn    = 1;

    for (int row = 0; row < parameterLimit; row++)
    {
      parametersTable.setValueAt (names [row], row, nameColumn );
      parametersTable.setValueAt (values[row], row, valueColumn);
    }
  }

  // ListSelectionListener Interface

  /** Updates the parameters table when the selected edit changes. */
  public void valueChanged (ListSelectionEvent e)
  {
    UndoableEdit selectedEdit = editListView.getSelectedEdit ();
    setEdit (selectedEdit);
  }
}
