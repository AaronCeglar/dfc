package dsto.dfc.swing.undo;

import java.awt.Event;
import java.awt.event.KeyEvent;

import javax.swing.Icon;
import javax.swing.KeyStroke;
import javax.swing.undo.UndoableEdit;

import dsto.dfc.swing.icons.ImageLoader;

/**
 * Command to invoke a single undo () on the {@link MonitoredUndoManager}.
 *
 * @author Derek Weber
 * @version $Revision$
 */
public class CmdUndo extends AbstractUndoableCommand
{
  public static final Icon ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/undo.gif");

  public CmdUndo (MonitoredUndoManager undoManager)
  {
    super (undoManager);
    name = "Undo";
    setEnabled (! undoManager.undoCursorIsNull ());
  }

  public void execute (UndoableEdit edit)
  {
    undoManager.undoTo (edit);
  }

  public char getMnemonic ()
  {
    return 'u';
  }

  public Icon getIcon ()
  {
    return ICON;
  }

  /** Returns the undo cursor. */
  public UndoableEdit selectedEdit ()
  {
    return undoManager.getUndoCursor ();
  }

  public String getDescription ()
  {
    return "Undo the last change";
  }

  /**
   * This checks that the selected edit is in the {@link MonitoredUndoManager}'s
   * undo list.
   */
  public void updateEnabled ()
  {
    setEnabled (undoManager.inUndoList (selectedEdit ()));
  }

  public void skip ()
  {
    undoManager.skipUndo ();
  }

  /** The keyboard accelerator is Ctrl-Z. */
  public KeyStroke getAccelerator ()
  {
    return KeyStroke.getKeyStroke (KeyEvent.VK_Z, Event.CTRL_MASK);
  }
}