package dsto.dfc.swing.undo;

import javax.swing.undo.UndoableEdit;

/**
 * Command class to invoke redoTo () on a {@link MonitoredUndoManager}.
 *
 * @author Derek Weber
 * @author Peter J Smet
 * @version $Revision$
 */
public class CmdRedoTo extends CmdRedo
{
  /** The {@link EditListView} that this command is associated with. */
  EditListView editListView;

  /** Determines how to react to changes in selection in the {@link EditListView}). */
  SelectionUpdatePolicy selectionUpdatePolicy;

  public CmdRedoTo (EditListView editListView, MonitoredUndoManager undoManager)
  {
    super (undoManager);
    name                  = "Redo To";
    this.editListView     = editListView;
    exceptionHandler      = EditExceptionHandler.createExtendedHandler ();
    selectionUpdatePolicy = new SelectionUpdatePolicy (editListView, this);
  }

  public String getDescription ()
  {
    return "Redo all edits up to the selected edit";
  }

  /** Returns the currently selected edit. */
  public UndoableEdit selectedEdit ()
  {
    return editListView.getSelectedEdit ();
  }

  /**
   * Skips the current edit causing an exception, but checks to see if any more
   * edits are to be redone after it.
   */
  public void skip ()
  {
    boolean moreEditsToFollow = selectedEdit () != undoManager.getRedoCursor ();

    super.skip ();

    if (moreEditsToFollow)
      execute ();
  }

  protected void listenForEnablingEvents ()
  {
    cursorUpdatePolicy.listenForCursorChanges ();
    selectionUpdatePolicy.listenForSelectionChanges ();
  }

  protected void stopListeningForEnablingEvents ()
  {
    cursorUpdatePolicy.stopListeningForCursorChanges ();
    selectionUpdatePolicy.stopListeningForSelectionChanges ();
  }
}