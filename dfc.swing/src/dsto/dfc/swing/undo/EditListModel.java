package dsto.dfc.swing.undo;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.undo.UndoableEdit;

/**
 * Implementation of a {@link javax.swing.JList} {@link javax.swing.ListModel}
 * that stores {@link javax.swing.undo.UndoableEdit}s, for use by the {@link
 * MonitoredUndoManager}. The {@link EditListModel} fire {@link
 * javax.swing.event.ListDataEvent}s.
 *
 * @author Derek Weber
 * @author Peter J Smet
 * @version $Revision$
 * @see MonitoredUndoManager
 * @see javax.swing.ListModel
 */
public class EditListModel implements ListModel, Serializable
{
  /** For managing {@link javax.swing.event.ListDataListener}s. */
  protected transient EventListenerList listenerList = new EventListenerList ();

  /** The list of {@link javax.swing.undo.UndoableEdit}s. */
  protected List list = new ArrayList ();

  /** Empty constructor for bean serialisation. */
  public EditListModel ()
  {
    // zip
  }

  /**
   * Constructs an {@link EditListModel} from the list of edits provided. It
   * pulls the edits out of the list, however, rather than simply using the
   * {@link java.util.List} provided.
   */
  public EditListModel (List edits)
  {
    list.addAll (edits);
  }

  /**
   * Returns a list containing the {@link EditListModel}'s edits. <em>NB</em>
   * Please don't modify this list or these edits. Cannot return a Collections
   * unmodifiable list because of serialisation issues.
   */
  public List getEdits ()
  {
    return list;
  }

  /** Only to be used by the Deserializer. */
  public void setEdits (List edits)
  {
    list = edits;
  }

  public Object getElementAt (int index)
  {
    return get (index);
  }

  public void clear ()
  {
    if ( isEmpty () )
      return;

    int lastIndex = getSize () - 1;
    list.clear ();
    fireIntervalRemoved (this, 0, lastIndex);
  }

  public boolean isEmpty ()
  {
    return list.isEmpty ();
  }

  public int getSize ()
  {
    return list.size ();
  }

  public boolean contains (Object o)
  {
    return list.contains (o);
  }

  public boolean add (Object o)
  {
    boolean added = list.add (o);
    if (added)
      fireIntervalAdded (this, indexOf (o), indexOf (o));
    return added;
  }

  public Object get (int index)
  {
    return list.get (index);
  }

  public UndoableEdit getEdit (int index)
  {
    return (UndoableEdit) get (index);
  }

  public Object set (int index, Object element)
  {
    Object returnedObject = list.set (index, element);
    fireContentsChanged (element, index, index);
    return returnedObject;
  }

  public void add (int index, Object element)
  {
    list.add (index, element);
    fireIntervalAdded (element, index, index);
  }

  public Object remove (int index)
  {
    Object removed = list.remove (index);
    fireIntervalRemoved (this, index, index);
    return removed;
  }

  public void remove (Object editToBeRemoved)
  {
    remove (indexOf (editToBeRemoved));
  }

  public void removeAll (List editsToBeRemoved)
  {
    for (int i = 0; i < editsToBeRemoved.size (); i++)
      remove (editsToBeRemoved.get (i));
  }

  public boolean doesNotContainAll (List edits)
  {
    return ! containsAll (edits);
  }

  public boolean containsAll (List edits)
  {
    return list.containsAll (edits);
  }

  public int indexOf (Object o)
  {
    return list.indexOf (o);
  }

  /**
   * Add a listener to the list that's notified each time a change
   * to the data model occurs.
   *
   * @param l the ListDataListener
   */
  public void addListDataListener (ListDataListener l) {
    listenerList.add (ListDataListener.class, l);
  }


  /**
   * Remove a listener from the list that's notified each time a
   * change to the data model occurs.
   *
   * @param l the ListDataListener
   */
  public void removeListDataListener (ListDataListener l) {
    listenerList.remove (ListDataListener.class, l);
  }


  /**
   * EditListModel subclasses must call this method <b>after</b>
   * one or more elements of the list change.  The changed elements
   * are specified by a closed interval index0, index1, i.e. the
   * range that includes both index0 and index1.  Note that
   * index0 need not be less than or equal to index1.
   *
   * @param source The ListModel that changed, typically "this".
   * @param index0 One end of the new interval.
   * @param index1 The other end of the new interval.
   * @see EventListenerList
   */
  protected void fireContentsChanged (Object source, int index0, int index1)
  {
    Object[] listeners = listenerList.getListenerList ();
    ListDataEvent e = null;

    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == ListDataListener.class) {
        if (e == null) {
          e = new ListDataEvent (source, ListDataEvent.CONTENTS_CHANGED, index0, index1);
        }
        ((ListDataListener)listeners[i+1]).contentsChanged (e);
      }
    }
  }


  /**
   * EditListModel subclasses must call this method <b>after</b>
   * one or more elements are added to the model.  The new elements
   * are specified by a closed interval index0, index1, i.e. the
   * range that includes both index0 and index1.  Note that
   * index0 need not be less than or equal to index1.
   *
   * @param source The ListModel that changed, typically "this".
   * @param index0 One end of the new interval.
   * @param index1 The other end of the new interval.
   * @see EventListenerList
   */
  protected void fireIntervalAdded (Object source, int index0, int index1)
  {
    Object[] listeners = listenerList.getListenerList ();
    ListDataEvent e = null;

    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == ListDataListener.class) {
        if (e == null) {
          e = new ListDataEvent (source, ListDataEvent.INTERVAL_ADDED, index0, index1);
        }
        ((ListDataListener)listeners[i+1]).intervalAdded (e);
      }
    }
  }


  /**
   * EditListModel subclasses must call this method <b>after</b>
   * one or more elements are removed from the model.  The new elements
   * are specified by a closed interval index0, index1, i.e. the
   * range that includes both index0 and index1.  Note that
   * index0 need not be less than or equal to index1.
   *
   * @param source The ListModel that changed, typically "this".
   * @param index0 One end of the new interval.
   * @param index1 The other end of the new interval.
   * @see EventListenerList
   */
  protected void fireIntervalRemoved (Object source, int index0, int index1)
  {
    Object[] listeners = listenerList.getListenerList ();
    ListDataEvent e = null;

    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == ListDataListener.class) {
        if (e == null) {
          e = new ListDataEvent(source, ListDataEvent.INTERVAL_REMOVED, index0, index1);
        }
        ((ListDataListener)listeners[i+1]).intervalRemoved (e);
      }
    }
  }

  /**
   * Return an array of all the listeners of the given type that
   * were added to this model.
   *
   * @returns all of the objects recieving <em>listenerType</em> notifications
   *          from this model
   *
   * @since 1.3
   */
  public EventListener[] getListeners (Class listenerType) {
    return listenerList.getListeners (listenerType);
  }

  // Serializable methods

  private void readObject (ObjectInputStream in)
    throws IOException, ClassNotFoundException
  {
    in.defaultReadObject ();
    listenerList = new EventListenerList ();
  }
}