package dsto.dfc.swing.undo;

import javax.swing.undo.UndoableEdit;

/**
 * Command to invoke undoTo () on a {@link MonitoredUndoManager}.
 *
 * @author Derek Weber
 * @author Peter J Smet
 * @version $Revision$
 */
public class CmdUndoTo extends CmdUndo
{
  /** The {@link EditListView} that this command is associated with. */
  EditListView editListView;

  /** Determines how to react to changes in selection in the {@link EditListView}). */
  SelectionUpdatePolicy selectionUpdatePolicy;

  public CmdUndoTo (EditListView editListView, MonitoredUndoManager undoManager)
  {
    super (undoManager);
    name                  = "Undo To";
    this.editListView     = editListView;
    exceptionHandler      = EditExceptionHandler.createExtendedHandler ();
    selectionUpdatePolicy = new SelectionUpdatePolicy (editListView, this);
  }

  public String getDescription ()
  {
    return "Undo all edits up to the selected edit";
  }

  public UndoableEdit selectedEdit ()
  {
    return editListView.getSelectedEdit ();
  }

  /**
   * Skips the edit causing the exception and continues undoing, but checks to
   * see if the exception-causing edit is the last to be undone - if so it stops.
   */
  public void skip ()
  {
    boolean moreEditsToFollow = selectedEdit () != undoManager.getUndoCursor ();

    super.skip ();

    if (moreEditsToFollow)
      execute ();
  }

  protected void listenForEnablingEvents ()
  {
    cursorUpdatePolicy.listenForCursorChanges ();
    selectionUpdatePolicy.listenForSelectionChanges ();
  }

  protected void stopListeningForEnablingEvents ()
  {
    cursorUpdatePolicy.stopListeningForCursorChanges ();
    selectionUpdatePolicy.stopListeningForSelectionChanges ();
  }
}