package dsto.dfc.swing.undo;

import java.awt.Component;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;

import dsto.dfc.swing.panels.PanelManager;
import dsto.dfc.swing.panels.PanelManagerFinder;
import dsto.dfc.util.BasicPropertyEventSource;

/**
 * Provides the same functions as the Swing UndoManager, plus
 * provides functionality to skip non-undoable
 * edits in the middle of the history list. Note that the non-undoable edit and
 * subsequent edits are removed if they are skipped because they are rendered
 * invalid.
 *
 * @version $Revision$
 */
public class MonitoredUndoManager extends BasicPropertyEventSource
  implements UndoableEditListener
{
  /**
   * The default limit placed on the number of undo's.
   */
  public  final static int                  DEFAULT_LIMIT = 50;

  /**
   * Null object pattern - the cursors will point to valid edits or NULL_EDIT.
   */
  public  final static UndoableEdit         NULL_EDIT     = new NullEdit ();

  /** Null object pattern. */
  private final static UndoableEditListener NULL_RECORDER = new UndoableEditListener ()
  {
    public void undoableEditHappened (UndoableEditEvent e)
    {
      // zip
    }
  };

  /** The maximum number of edits stored in the undo list.                    */
  protected int                  limit                = DEFAULT_LIMIT;
  /** The list of edits.                                                      */
  protected EditListModel        edits                = new EditListModel ();
  /** The index at which to place the next edit added to the manager.         */
  protected int                  indexOfNextAdd;   // = indexOfUndoCursor () + 1
  /** The last edit which can be undone in the edit list.                     */
  public    UndoableEdit         undoCursor           = NULL_EDIT;
  /** The next edit to be redone in the edit list.                            */
  public    UndoableEdit         redoCursor           = NULL_EDIT;
  /** Thread lock.                                                            */
  protected Object               undoRedoSemaphore    = new Object ();
  private   UndoableEditListener editEventRecorder    = NULL_RECORDER;

  /**
   * Get or create a monitored undomanager for the window containing a given
   * component.
   */
  public static MonitoredUndoManager getOrCreateUndoManager (Component c)
  {
    try
    {
      PanelManager panelManager = PanelManagerFinder.findPanelManagerForComponent (c);
      JFrame       iveWindow    = (JFrame) SwingUtilities.windowForComponent (panelManager);
      JRootPane    pane         = iveWindow.getRootPane ();

      return (MonitoredUndoManager) pane.getClientProperty (MonitoredUndoManager.class);
    }
    catch (NullPointerException iveWindowNotFound)
    {
      return new MonitoredUndoManager ();
    }
  }

  /**
   * The maximum number of edits that
   * will be kept on the stack.
   *
   * @return The maximum number of edits.
   */
  public int getLimit ()
  {
    return limit;
  }

  /**
   * Sets the maximum number of edits kept on the stack.  Any existing
   * edits are discarded.
   *
   * @param newLimit The new maximum edit limit.
   */
  public void setLimit (int newLimit)
  {
    discardAllEdits ();
    limit = newLimit;
  }

  /** Returns the list of edits maintained by the {@link MonitoredUndoManager}. */
  public EditListModel getEdits ()
  {
    return edits;
  }

  /**
   * Removes all edits from the undo stack and calls {@link
   * javax.swing.undo.UndoableEdit#die} for each edit.
   */
  public synchronized void discardAllEdits ()
  {
    dropEdits (0, lastIndex ());
  }

  /**
   * True if the manager can execute an undo operation.
   *
   */
  public synchronized boolean canUndo ()
  {
    return undoCursor.canUndo ();
  }

  /**
   * True if the manager can execute a redo operation.
   */
  public synchronized boolean canRedo ()
  {
    return redoCursor.canRedo ();
  }

  /**
   * Get the human readable name for the next edit that will be undone
   * by {@link #undo}.
   *
   * @return The human readable name for the next edit to be undone,
   * or "Undo" if no edit can currently be undone.
   *
   * @see #canUndo
   */
  public synchronized String getUndoPresentationName ()
  {
    return undoCursor.getUndoPresentationName ();
  }

  /**
   * Get the human readable name for the next edit that will be redone
   * by redo ().
   *
   * @return The human readable name for the next edit to be redone,
   * or "Redo" if no edit can currently be redone.
   *
   * @see #canRedo
   */
  public synchronized String getRedoPresentationName ()
  {
    return redoCursor.getRedoPresentationName ();
  }

  /**
   * Get the name of the next edit to be undone using its
   * getPresentationName () method.
   *
   * @return The result of calling getPresentationName () on the next
   * undo edit or "" if no edit can currently be undone.
   */
  public synchronized String getPresentationName ()
  {
    return getUndoPresentationName ();
  }

  /**
   * Get an edit for a given index into the undo stack.
   *
   * @param i The index of the edit.
   * @return The edit at index i or the NULL_EDIT if i is outside the range of
   *         the undo stack.
   * @see #getEditCount
   */
  public UndoableEdit getEditOrNullEdit (int i)
  {
    if (i < 0 || i > lastIndex ())
      return NULL_EDIT;

    return (UndoableEdit) edits.get (i);
  }

  /**
   * The number of edits on the undo stack.
   *
   * @return The number of edits.
   */
  public int getEditCount ()
  {
    return edits.getSize ();
  }

  /**
   * Sets the current cursor to <code>newEdit</code> which has not been undone.
   */
  protected void setUndoCursor (UndoableEdit newCursor)
  {
    UndoableEdit oldCursor = undoCursor;
    undoCursor = newCursor;
    firePropertyChange ("undoCursor", oldCursor, newCursor);
    edits.fireContentsChanged (this , indexOfUndoCursor (), indexOfUndoCursor ());
  }

  /**
   * Returns the undo cursor which points to the last edit not undone. If you
   * call redo () on it, it will barf because it hasn't been undone. You may call
   * undo () on it happily, at which time the cursor will shift to the previous
   * edit.
   *
   * @return The edit which will be undone next.
   */
  public UndoableEdit getUndoCursor ()
  {
    return undoCursor;
  }

  protected void setRedoCursor (UndoableEdit newCursor)
  {
    UndoableEdit oldCursor = redoCursor;
    redoCursor = newCursor;
    firePropertyChange ("redoCursor", oldCursor, newCursor);
  }

  /** Returns the next edit in line to be redone. */
  public UndoableEdit getRedoCursor ()
  {
    return redoCursor;
  }

  /** Returns true if <i>edit</i> is known to the {@link MonitoredUndoManager}. */
  public boolean containsEdit (UndoableEdit edit)
  {
    return edits.contains (edit);
  }

  /**
   * Returns true if all edits in <i>someEdits</i> are known to the {@link
   * MonitoredUndoManager} and false otherwise.
   */
  public boolean containsAllEdits (List someEdits)
  {
    return edits.containsAll (someEdits);
  }

  /** Returns the index of the last edit in the list. */
  public int lastIndex ()
  {
    return getEditCount () - 1;
  }

  private boolean mergedEditWithLastEdit (UndoableEdit edit)
  {
    UndoableEdit lastAddedEdit = getEditOrNullEdit (indexOfNextAdd - 1);
    boolean merged = lastAddedEdit.addEdit (edit);
    if ( merged && ! (lastAddedEdit instanceof MonitoredEditGroup) )
      edit.die ();

    return merged;
  }

  /**
   * Internal test to determine if {@link #edits} is full and edits need to be
   * dropped before more can be added.
   */
  protected boolean editQueueIsFull ()
  {
    return indexOfNextAdd == limit;
  }

  private boolean editRejected (UndoableEdit edit)
  {
    return (edit == null || containsEdit (edit));
  }

  /**
   * Add an edit to list at position of undoCursor,
   * and advance cursor.
   * Will cause all redoable edits to be discarded.
   * The undoRedoSemaphore ensures that no edits will
   * be added while the undoManager is in the middle
   * of an undo or redo operation.
   *
   * @param edit The edit to be added to the stack.
   */
  public void addEdit (UndoableEdit edit)
  {
    if (limit < 1)
      return;
    
    synchronized (undoRedoSemaphore)
    {
      if (editRejected (edit))
        return;

      setRedoCursor (NULL_EDIT);

      if (mergedEditWithLastEdit (edit))
        return;

      if (editQueueIsFull ())
        dropFirstEdit ();
      else
        dropEditsBeyondUndoCursor ();

      edits.add (edit);
      indexOfNextAdd++;

      setUndoCursor (edit);
    }
  }

  /** Insert <i>newEdit</i> at <i>index</i>. Handles moving of cursors. */
  public boolean insertEdit (int index, UndoableEdit newEdit)
  {
    if (index < 0 || index > getEditCount () ||
        editRejected (newEdit))
      return false;

    edits.add (index, newEdit);

    if (index <= indexOfUndoCursor ())
      indexOfNextAdd++;
    else if (index == indexOfUndoCursor () + 1)
      setRedoCursor (newEdit);

    return true;
  }

  /**
   * Inserts the edits in <i>newEdits</i> at <i>index</i>. It handles moving the
   * cursors appropriately.
   */
  public void insertEdits (int index, Collection newEdits)
  {
    Iterator i = newEdits.iterator ();

    while (i.hasNext ())
      if (insertEdit (index, (UndoableEdit) i.next ()))
        index++;
  }

  /** Moves the cursors one step forward. */
  protected void moveCursorsForward ()
  {
    setUndoCursor (redoCursor);
    setRedoCursor (getEditOrNullEdit (indexOfRedoCursor () + 1));
  }

  /** Moves the cursors one step backwards. */
  protected void moveCursorsBackward ()
  {
    setRedoCursor (undoCursor);
    setUndoCursor (getEditOrNullEdit (indexOfUndoCursor () - 1));
  }

  /**
   * Redo the next available redoable edit.
   *
   * @exception CannotRedoException if there are no redoable edits.
   */
  public void redo () throws CannotRedoException
  {
    synchronized (undoRedoSemaphore)
    {
      try
      {
        redoCursor.redo ();
        indexOfNextAdd++;
        moveCursorsForward ();
      }
      catch (Exception ex)
      {
        throw new CannotRedoException ();
      }
    }
  }

  /** Redo the edits up to and including <i>endEdit</i>. */
  public void redoTo (UndoableEdit endEdit) throws CannotRedoException
  {
    if (missingFromRedoList (endEdit))
      throw new CannotRedoException ();

    while (endEdit != redoCursor)
      redo ();

    redo ();
  }

  /** Undoes the edit at the undo cursor and shifts the cursor backwards one step. */
  public void undo () throws CannotUndoException
  {
    synchronized (undoRedoSemaphore)
    {
      try
      {
        undoCursor.undo ();
        indexOfNextAdd--;
        moveCursorsBackward ();
      }
      catch (Exception ex)
      {
        throw new CannotUndoException ();
      }
    }
  }

  /** Undoes the edits up to and including <i>endEdit</i>. */
  public void undoTo (UndoableEdit endEdit) throws CannotUndoException
  {
    if ( missingFromUndoList (endEdit) )
      throw new CannotUndoException ();

    while (endEdit != getUndoCursor ())
      undo ();

    undo ();
  }

  /**
   * Skips a non-undoable edit, moving the undo cursor to the previous
   * edit and removing the non-undoable edit and the invalid edits after it.
   */
  public void skipUndo ()
  {
    if (undoCursorIsNull ()) // we're at the top of the undo stack (can't skip)
      return;

    indexOfNextAdd = indexOfUndoCursor ();
    moveCursorsBackward ();
    handleSkipUndoRamifications ();
  }

  /**
   * Drops all edits beyond the undo cursor when an undo is skipped. This is
   * because the edits after the undo cursor are regarded as unstable.
   */
  protected void handleSkipUndoRamifications ()
  {
    dropEditsBeyondUndoCursor ();
  }

  /** Skip the current redoable edit at move the cursors forward one step. */
  public void skipRedo ()
  {
    if (redoCursorIsNull ())
      return;

    moveCursorsForward ();
    indexOfNextAdd++;
  }

  /** Returns true if <i>edit</i> is in the undo list and false otherwise. */
  public boolean inUndoList (UndoableEdit edit)
  {
    return containsEdit (edit) && (indexOfEdit (edit) <= indexOfUndoCursor ());
  }

  /** Returns true if <i>edit</i> is not in the undo list. Convenience method. */
  public boolean missingFromUndoList (UndoableEdit edit)
  {
    return ! inUndoList (edit);
  }

  /** Returns true if <i>edit</i> is in the redo list and false otherwise. */
  public boolean inRedoList (UndoableEdit edit)
  {
     return containsEdit (edit) && missingFromUndoList (edit);
  }

  /** Returns true if <i>edit</i> is not in the redo list. Convenience method. */
  public boolean missingFromRedoList (UndoableEdit edit)
  {
    return ! inRedoList (edit);
  }

  /** Returns the index of <i>edit</i> if it's known, and -1 if it isn't. */
  public int indexOfEdit (UndoableEdit edit)
  {
    return edits.indexOf (edit);
  }

  /**
   * Returns the index of the undo cursor. If it's -1, then the undo list is
   * empty.
   */
  public int indexOfUndoCursor ()
  {
    return indexOfEdit (getUndoCursor ());
  }

  /**
   * Returns the index of the redo cursor. If it's -1, then the redo list is
   * empty.
   */
  public int indexOfRedoCursor ()
  {
    return indexOfEdit (getRedoCursor ());
  }

  /**
   * UndoableEditListener interface implementation. Calls {@link #addEdit} to
   * add the edit to the stack.
   */
  public void undoableEditHappened (UndoableEditEvent e)
  {
    addEdit (e.getEdit ());
    editEventRecorder.undoableEditHappened (e);
  }

  /** Drops the first edit in the edit list and handles the moving of cursors. */
  public void dropFirstEdit ()
  {
    dropEdit (0);
  }

  private void dropEditsBeyondUndoCursor ()
  {
    dropEdits (indexOfNextAdd, lastIndex ());
  }

  /** Drops the edit at <i>index</i> and handles moving of the cursors. */
  protected void dropEdit (int index)
  {
    if (index < 0 || index >= limit)
      return;

    if (index == indexOfUndoCursor ())
      setUndoCursor (getEditOrNullEdit (index - 1));

    if (index == indexOfRedoCursor ())
      setRedoCursor (getEditOrNullEdit (index + 1));

    UndoableEdit unwantedEdit = (UndoableEdit) edits.remove (index);
    unwantedEdit.die ();

    if (index < indexOfNextAdd)
      indexOfNextAdd--;
  }

  /**
   * Drop all edits between two indices. Does not adjust index and assumes valid
   * indices.
   *
   * @param start The first index.
   * @param end The second index. start <= end or this method does nothing.
   */
  protected void dropEdits (int start, int end)
  {
    for (int i = end; i >= start; i--)
      dropEdit (i);
  }

  /** Drops the <i>unwantedEdit</i> and handles the moving of cursors. */
  public void dropEdit (UndoableEdit unwantedEdit)
  {
    dropEdit (indexOfEdit (unwantedEdit));
  }

  /** Drops the collection of <i>unwantedEdits</i> and handles updating of cursors. */
  public void dropEdits (Collection unwantedEdits)
  {
    for (Iterator it = unwantedEdits.iterator (); it.hasNext (); )
      dropEdit ((UndoableEdit) it.next ());
  }

  /**
   * By invoking this method, the <i>editEventRecorder</i> will 'hear' every
   * {@link javax.swing.undo.UndoableEdit} that the {@link MonitoredUndoManager}
   * 'hears'. Only one editEventRecorder can be registered with the {@link
   * MonitoredUndoManager} at any one time.
   */
  public void broadcastEditEventsTo (UndoableEditListener newEditEventRecorder)
  {
    this.editEventRecorder = newEditEventRecorder;
  }

  /** Deregisters the currently registered editEventRecorder. */
  public void stopBroadcastingEditEvents ()
  {
    editEventRecorder = NULL_RECORDER;
  }

  /** Returns true if the undo cursor is the {@link #NULL_EDIT}. */
  public boolean undoCursorIsNull ()
  {
    return getUndoCursor () == NULL_EDIT;
  }

  /** Returns true if the redo cursor is the {@link #NULL_EDIT}. */
  public boolean redoCursorIsNull ()
  {
    return getRedoCursor () == NULL_EDIT;
  }

  /**
   * For drop'n'drag support of edits. This implementation does not support
   * the movement of edits within its list.
   */
  protected void handleMoveEdits (int insertionIndex, List droppedEdits)
  {
    // should not move edits
  }

  // ----- inner classes -------------------------------------------------------

  static class NullEdit extends AbstractUndoableEdit
  {
    public boolean canUndo            () { return false;       }
    public boolean canRedo            () { return false;       }
    public String getPresentationName () { return "NULL_EDIT"; }
  }
}



