package dsto.dfc.swing.undo;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import dsto.dfc.logging.Log;

/**
 * Policy for handling exceptions and interrogating user for options. The user
 * is asked whether he or she wants to skip the edit causing the exception, or
 * skip all the edits after the exception as well, or retry the edit, or just
 * pause.
 *
 * @author Peter J Smet
 * @version $Revision$
 */
public class EditExceptionHandler extends JPanel
{
  /** Skip all edits past the offending edit.                   */
  protected static final int  SKIP_ALL = 3;
  /** Skip only the offending edit and then continue as normal. */
  protected static final int  SKIP     = 2;
  /** Retry the offending edit.                                 */
  protected static final int  RETRY    = 1;
  /** Pause with the cursor where it is.                        */
  protected static final int  PAUSE    = 0;

  private boolean   skipAll = false;
  private Object [] options;

  public EditExceptionHandler (Object [] exceptionHandlingOptions)
  {
    super ();
    options = exceptionHandlingOptions;
  }

  // Factory methods

  /**
   * Factory method for creating an {@link EditExceptionHandler} with the pause,
   * retry, and skip options.
   */
  public static EditExceptionHandler createStandardHandler ()
  {
    Object [] standardOptions = new Object [] {"Pause", "Retry edit", "Skip edit"};
    return new EditExceptionHandler (standardOptions);
  }

  /**
   * Factory method for creating an {@link EditExceptionHandler} with the
   * <i>pause</i>, <i>retry</i>, <i>skip</i>, and <i>skip all</i> options.
   */
  public static EditExceptionHandler createExtendedHandler ()
  {
    Object [] extendedOptions = new Object [] {"Pause", "Retry edit", "Skip edit", "Skip All"};
    return new EditExceptionHandler (extendedOptions);
  }

  protected int howWillUserHandleException (String questionDialogTitle)
  {
    Log.diagnostic (questionDialogTitle, this);

    if (skipAll)
      return SKIP;

    String message =
      "This action cannot be executed. This may be due to invalid parameters. " +
      "Would you like to:";

    int decision = JOptionPane.showOptionDialog
      (this, message, questionDialogTitle, JOptionPane.YES_NO_CANCEL_OPTION,
       JOptionPane.QUESTION_MESSAGE, null, options, options [0]);

    skipAll (decision == SKIP_ALL);

    return decision;
  }

  /**
   * Factory method to get the user's decision as to how to handle an
   * exception-causing edit.
   *
   * @param errorMessage        The message to display to the user.
   * @param editExceptionPolicy The policy determining what to do based on the
   *                            user's response to the <i>errorMessage</i>.
   */
  public int handleNonExecutableEdit (String errorMessage,
                                      EditExceptionPolicy editExceptionPolicy)
  {
    int response = howWillUserHandleException (errorMessage);

    if (response == RETRY)
      editExceptionPolicy.retry ();

    if (response == SKIP || response == SKIP_ALL)
      editExceptionPolicy.skip ();

    if (response == PAUSE)
      editExceptionPolicy.pause ();

    return response;
  }

  /**
   * If <i>skip</i> is true then all edits will be skipped, otherwise only the
   * offending edit will be skipped.
   */
  public void skipAll (boolean skip)
  {
    skipAll = skip;
  }
}
