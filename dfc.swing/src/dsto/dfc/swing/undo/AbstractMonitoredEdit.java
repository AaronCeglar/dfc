package dsto.dfc.swing.undo;

import java.util.HashMap;

import javax.swing.Icon;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;

import dsto.dfc.swing.icons.ImageLoader;

/**
 * Abstract implementation of the MonitoredEdit interface.
 *
 * @author  Derek Weber
 * @version $Revision$
 */
public abstract class AbstractMonitoredEdit extends AbstractUndoableEdit
  implements MonitoredEdit
{
  public static Icon ICON = ImageLoader.getIcon ("/dsto/dfc/icons/cog.png");

  protected boolean alive = true;

  protected String[] parameterNames;
  protected Object[] parameterValues;

  /** For bean serialisation only */
  public AbstractMonitoredEdit ()
  {
    this (new String[0], new Object[0]);
  }

  public AbstractMonitoredEdit (String[] parameterNames, Object[] parameterValues)
  {
    this.parameterNames  = parameterNames;
    this.parameterValues = parameterValues;
  }

  // Bean methods

  public String[] getParameterNames ()
  {
    return parameterNames;
  }

  public void setParameterNames (String[] parameterNames)
  {
    this.parameterNames = parameterNames;
  }

  public Object[] getParameterValues ()
  {
    return parameterValues;
  }

  public void setParameterValues (Object[] parameterValues)
  {
    this.parameterValues = parameterValues;
  }

  public String getUndoPresentationName ()
  {
    return getPresentationName ();
  }

  public String getRedoPresentationName ()
  {
    return getPresentationName ();
  }

  public abstract String getDescription ();

  // end Bean methods

  public void die () {
    alive = false;
  }

  public void undo () throws CannotUndoException
  {
    if (!canUndo ())
      throw new CannotUndoException ();
  }

  public boolean canUndo ()
  {
    return alive;
  }

  public void redo () throws CannotRedoException
  {
    if (!canRedo ())
      throw new CannotRedoException ();
  }

  public boolean canRedo ()
  {
    return alive;
  }

  // Restorable interface

  public void restoreContext (HashMap context)
  {
    // zip
  }

  // Copyable interface

  public Object clone () throws CloneNotSupportedException
  {
    return super.clone ();
  }

  // Iconic interface

  public Icon getIcon ()
  {
    return ICON;
  }

  public String getName ()
  {
    return getPresentationName ();
  }

  public Icon getLargeIcon ()
  {
    return getIcon ();
  }
}