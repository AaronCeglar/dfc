package dsto.dfc.swing.controls;

import java.awt.Font;

import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.metal.DefaultMetalTheme;

/**
 * The DFC idea of how Metal should look.
 *
 * @version $Revision$
 */
public class DfcMetalTheme extends DefaultMetalTheme
{
  private static final ColorUIResource BLACK = new ColorUIResource (0, 0, 0);
  
  private FontUIResource controlFont = null;
  
  public String getName ()
  {
    return "DFC Metal Theme";
  }

  // make label and menu fonts non-bold

  public FontUIResource getControlTextFont ()
  {
	  if (controlFont == null)
    {
      try
      {
		    controlFont =
          new FontUIResource (Font.getFont ("swing.plaf.metal.controlFont",
                              new Font ("Dialog", Font.PLAIN, 12)));
	    } catch (Exception e)
      {
		    controlFont = new FontUIResource ("Dialog", Font.PLAIN, 12);
	    }
    }

	  return controlFont;
  }

  public FontUIResource getMenuTextFont ()
  {
	  return getControlTextFont ();
  }
  
  // make system text (eg labels) non purple

  public ColorUIResource getSystemTextColor ()
  {
    return BLACK;
  }
}
