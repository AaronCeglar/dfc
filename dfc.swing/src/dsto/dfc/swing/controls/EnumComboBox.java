package dsto.dfc.swing.controls;

import javax.swing.DefaultComboBoxModel;

import dsto.dfc.util.EnumerationValue;

/**
 * Simple extension of DfcComboBox to add support for editing
 * EnumerationValue-based values.  If a value implementing the EnumerationValue
 * interface is set as the selected value, the enumerated co-values
 * are loaded into the combo list.
 *
 * @see dsto.dfc.util.EnumerationValue
 *
 * @version $Revision$
 */
public class EnumComboBox extends DfcComboBox
{
  private Object [] enumValues = null;

  public EnumComboBox ()
  {
    // zip
  }

  /**
   * Update the list model with the most recent enum values.
   */
  public void updateListModel ()
  {
    /** @todo find a way to stop this firing action events */
    EnumerationValue enumValue = (EnumerationValue)getSelectedItem ();
    enumValues = enumValue.getEnumValues ();

    DefaultComboBoxModel newModel = new DefaultComboBoxModel (enumValues);

    int index = newModel.getIndexOf (enumValue);

    setModel (newModel);

    // restore selection
    setSelectedIndex (index);
  }

  public void setSelectedItem (Object item)
  {
    // if item is an enumeration, load its values into the list
    if (item instanceof EnumerationValue)
    {
      EnumerationValue enumValue = (EnumerationValue)item;
      Object [] newValues = enumValue.getEnumValues ();

      // if values have changed...
      if (newValues != enumValues)
      {
        enumValues = newValues;

        setModel (new DefaultComboBoxModel (newValues));
      }
    }

    super.setSelectedItem (item);
  }
}
