package dsto.dfc.swing.controls;

import java.beans.PropertyEditorSupport;

public class ButtonLayoutVertAlignmentPropertyEditor extends PropertyEditorSupport
{
  private static String[] resourceStrings = {"ALIGN_CENTER", "ALIGN_TOP", "ALIGN_BOTTOM", };
  private static int[] intValues = { ButtonLayout.ALIGN_CENTER, ButtonLayout.ALIGN_TOP, ButtonLayout.ALIGN_BOTTOM, };
  private static String[] sourceCodeStrings = { "ButtonLayout.ALIGN_CENTER", "ButtonLayout.ALIGN_TOP", "ButtonLayout.ALIGN_BOTTOM", };

  public String[] getTags()
  {
    return resourceStrings;
  }

  public String getJavaInitializationString()
  {
    Object value = getValue();
    for (int i = 0; i < intValues.length; i++) {
    if (value.equals(new Integer(intValues[i])))
    {
      return sourceCodeStrings[i];
    }
    }
    return null;
  }

  public String getAsText()
  {
    Object value = getValue();
    for (int i = 0; i < intValues.length; i++) {
    if (value.equals(new Integer(intValues[i])))
    {
      return resourceStrings[i];
    }
    }
    return null;
  }

  public void setAsText(String text) throws IllegalArgumentException
  {
    for (int i = 0; i < resourceStrings.length; i++) {
    if (text.equals(resourceStrings[i]))
    {
      setValue(new Integer(intValues[i]));
      return;
    }
    }
    throw new IllegalArgumentException();
  }
}
