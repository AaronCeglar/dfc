package dsto.dfc.swing.controls;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JTextPane;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.text.Document;
import javax.swing.text.StyledDocument;

/**
 * DFC custom tool tip manager: for when TooltipManager is just not cutting it.
 *
 * @author Mofeed
 * @author Matthew
 * @version $Revision$
 */
public class DfcToolTipManager
{
  private static final Border TIP_BORDER = BorderFactory.createCompoundBorder
        (UIManager.getBorder ("ToolTip.border"), BorderFactory.createEmptyBorder (1, 3, 1, 3));

  private static final int TIP_LOCATION_OFFSET_X = 15;
  private static final int TIP_LOCATION_OFFSET_Y = 15;

  protected WindowPopup tipWindow;
  protected JTextPane tip;
  protected Component component;
  protected Timer delayTimer;
  protected Timer popdownTimer;
  protected Point absLocation;

  public DfcToolTipManager (Component component)
  {
    this.component = component;
  }
  
  public void dispose ()
  {
    if (tipWindow != null)
    {
      hideTipWindow ();
      tipWindow.removeNotify ();
      tipWindow.dispose ();
      
      tipWindow = null;
    }
  }

  private JTextPane createTip (StyledDocument document)
  {
    JTextPane newTip = new JTextPane (document);

    newTip.setBackground (UIManager.getColor ("ToolTip.background"));
    newTip.setBorder (TIP_BORDER);
    newTip.setCursor (null);

    return newTip;
  }

  protected void showTipWindow (Document doc, Point location)
  {
    // maximum width of tooltip before text starts to wrap = screen width / 3
    int maxTooltipWidth =
      Toolkit.getDefaultToolkit ().getScreenSize ().width / 3;
    Window parentWindow = getParentWindow ();

    if (absLocation == null)
    {
      absLocation = SwingUtilities.convertPoint (component, location, parentWindow);
      absLocation.x += parentWindow.getX ();
      absLocation.y += parentWindow.getY ();
    }

    tipWindow = getWindowPopup ();
    tipWindow.getContentPane().removeAll ();

    tip = createTip ((StyledDocument)doc);

    tip.setSize (maxTooltipWidth, 1000);
    tipWindow.getContentPane().add (tip);
    Dimension prefSize = tipWindow.getPreferredSize ();

    // pack () causes problems (get width incorrect), but we need the
    // height, especially since tooltips on applets need to account
    // for the warning text
    tipWindow.pack ();
    prefSize.height = tipWindow.getHeight ();

    // restrict tooltip to max width
    prefSize.width = Math.min (maxTooltipWidth, prefSize.width);

    tipWindow.setSize (prefSize.width, prefSize.height);
    tipWindow.show (absLocation.x, absLocation.y);

    bumpWindows (tipWindow.getBounds ());
  }

  public void showTipWindow (Document doc, Point location,
                             int startTime, int durationTime)
  {
    hideTipWindow ();

    if (startTime == 0)
    {
      showTipWindow (doc, location);
    } else
    {
      delayTimer =
        new Timer (startTime,
                   new InsideTimerAction (doc, location, durationTime));
      delayTimer.setRepeats (false);
      delayTimer.start ();
    }
  }

  public void hideTipWindow ()
  {
    if (delayTimer != null)
    {
      delayTimer.stop ();

      delayTimer = null;
    }

    if (popdownTimer != null)
    {
      popdownTimer.stop ();

      popdownTimer = null;
    }

    if (tipWindow != null)
      tipWindow.setVisible (false);
  }

  public Document getToolTipDoc ()
  {
    return tip.getDocument();
  }

  public void setToolTipText (String s)
  {
    tip.setText (s);
  }

  public void setToolTipWindowLocation (Point p)
  {
    Window parentWindow = getParentWindow ();

    absLocation = SwingUtilities.convertPoint (component, p, parentWindow);

    absLocation.x += parentWindow.getX () + TIP_LOCATION_OFFSET_X;
    absLocation.y += parentWindow.getY () + TIP_LOCATION_OFFSET_Y;

    if (tipWindow != null )
    {
      /* We want to use a new rectangle with the absLocation because it contains where we want
       * to move, and not where we are.
       * Therefore this actually does two things; first it makes sure the tipWindow will still be
       * in the right place, and secondly it actually moves th window. (the setBounds bit does this).
       */
      bumpWindows (new Rectangle(absLocation.x, absLocation.y, tipWindow.getWidth(), tipWindow.getHeight()));
    }

  }

  /**
   * Get or create a WindowPopup for the tooltip.
   */
  private WindowPopup getWindowPopup ()
  {
    Window currentParent = getParentWindow ();

    // if tip window not created, or created with a different parent
    // window, create a new one
    if (tipWindow == null ||
        currentParent != tipWindow.getOwner ())
    {
      return new WindowPopup (currentParent);
    } else
    {
      return tipWindow;
    }
  }

  /**
   * Get the parent window that should be used for the tooltip
   * popup window.
   */
  private Window getParentWindow ()
  {
    return SwingUtilities.windowForComponent (this.component);
  }

  /* Make sure that the tipWindow is always contained within the parent Window.*/

  private void bumpWindows (Rectangle childRect)
  {
    Rectangle parentWindowBounds = getParentWindowRectangle(childRect);

    WindowLayout.bumpRectangle (parentWindowBounds, childRect);
    tipWindow.setBounds(childRect);
  }

  private Rectangle getParentWindowRectangle (Rectangle childRect)
  {
    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    GraphicsDevice[] gs = ge.getScreenDevices();
    for (int j = 0; j < gs.length; j++)
    {
      GraphicsDevice gd = gs[j];
      GraphicsConfiguration[] gc = gd.getConfigurations();
      for (int i = 0; i< gc.length; i++)
      {
        Rectangle gcBounds = gc[i].getBounds();
        if (gcBounds.contains(childRect.x, childRect.y))
          return gcBounds;
      }
    }

    // This should never happen.
    return new Rectangle (0,0,0,0);
  }

  private static class WindowPopup extends JWindow
  {
    boolean firstShow = true;

    public WindowPopup (Window parent)
    {
      super (parent);
    }

    public void show (int x, int y)
    {
      this.setLocation (x, y);
      this.setVisible (true);

      /** This hack is to workaround a bug on Solaris where the windows does not really show
       *  the first time
       *  It causes a side effect of MS JVM reporting IllegalArumentException: null source
       *  fairly frequently - also happens if you use HeavyWeight JPopup, ie JComboBox
       */
      if(firstShow)
      {
        this.setVisible (false);
        this.setVisible(true);
        firstShow = false;
      }
    }

    public void setVisible (boolean value)
    {
      super.setVisible (value);
      
      if (value == false)
      {
        /*
         * We need to call removeNotify() here because hide() does
         * something only if Component.visible is true. When the app
         * frame is miniaturized, the parent frame of this frame is
         * invisible, causing AWT to believe that this frame is
         * invisible and causing hide() to do nothing
         */
        removeNotify();
      }
    }
  }

  protected final class InsideTimerAction implements ActionListener
  {
    private Document doc;
    private Point location;
    private int duration;

    public InsideTimerAction (Document doc, Point location, int duration)
    {
      this.doc = doc;
      this.location = location;
      this.duration = duration;
    }

    public void actionPerformed (ActionEvent e)
    {
      showTipWindow (doc, location);

      if (duration != 0)
      {
        popdownTimer =
          new Timer (duration, new DurationTimeAction ());

        popdownTimer.setRepeats (false);
        popdownTimer.start ();
      }
    }
  }

  protected final class DurationTimeAction implements ActionListener
  {
    public void actionPerformed (ActionEvent e)
    {
      hideTipWindow ();
    }
  }
}


