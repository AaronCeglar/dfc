package dsto.dfc.swing.controls;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

import javax.swing.RepaintManager;


/**
 *  A simple utility class that lets you very simply print
 *  an arbitrary component. Just pass the component to the
 *  PrintUtility.printComponent. The component you want to
 *  print doesn't need a print method and doesn't have to
 *  implement any interface or do anything special at all.
 *  If you are going to be printing many times, it is marginally more
 *  efficient to first do the following:
 *    PrintUtility printHelper = new PrintUtilities (theComponent);
 *  then later do printHelper.print (). But this is a very tiny
 *  difference, so in most cases just do the simpler
 *  PrintUtilities.printComponent (componentToBePrinted).
 *
 *  Taken from 7/99 Marty Hall, http://www.apl.jhu.edu/~hall/java/
 *  May be freely used or adapted.
 *
 * @author Luke Marsh
 * @version $Revision$
 */
public class PrintUtility implements Printable
{

  private Component componentToBePrinted;
  private PageFormat pageFormat;


  /**
   * Initializes variables.
   * 
   * @param componentToBePrinted The component that is to be printed.
   * @param pageFormat Used to obtain the current PageLayout settings stored in
   * the page format.
   */
  public PrintUtility (Component componentToBePrinted, PageFormat pageFormat)
  {
    this.componentToBePrinted = componentToBePrinted;
	this.pageFormat = pageFormat;
  }


  /**
   * Calls the print method.
   * 
   * @param c The component to be printed.
   * @param pageFormat Used to obtain the current PageLayout settings stored in
   * the page format.
   */
  public static void printComponent (Component c, PageFormat pageFormat)
  {
    new PrintUtility (c, pageFormat).print ();
  }


  /**
   * Opens up a print dialog and prints if the user clicks ok.
   */
  public void print()
  {
    PrinterJob printJob = PrinterJob.getPrinterJob ();
    printJob.setPrintable (this, pageFormat);
    if (printJob.printDialog ())
    {
      try
      {
        printJob.print ();
      }
      catch (PrinterException pe)
      {
        System.out.println ("Error printing: " + pe);
      }
    }
  }


  /**
   * Draws the component to be printed.
   * 
   * @param g The Graphics object.
   * @param myPageFormat The current page format.
   * @param pageIndex The current page index.
   * @return whether the page exists or not.
   */
  public int print (Graphics g, PageFormat myPageFormat, int pageIndex)
  {
    if (pageIndex > 0)
    {
      return (NO_SUCH_PAGE);
    }
    else
    {
      Graphics2D g2d = (Graphics2D) g;
      g2d.translate (myPageFormat.getImageableX (), myPageFormat.getImageableY ());
      if (myPageFormat.getOrientation() == 0) // Landscape
      {
		g2d.scale (myPageFormat.getImageableHeight() / componentToBePrinted.getSize ().getWidth (), 
				myPageFormat.getImageableHeight() / componentToBePrinted.getSize ().getHeight ());
      }
      else
      {
		g2d.scale (myPageFormat.getImageableWidth() / componentToBePrinted.getSize ().getWidth (), 
				myPageFormat.getImageableWidth() / componentToBePrinted.getSize ().getHeight ());
      }
      componentToBePrinted.setForeground (Color.white);
      disableDoubleBuffering (componentToBePrinted);
      componentToBePrinted.paint (g2d);
      enableDoubleBuffering (componentToBePrinted);
      return (PAGE_EXISTS);
    }
  }


  /**
   * The speed and quality of printing suffers dramatically if
   * any of the containers have double buffering turned on.
   * So this turns if off globally.
   * 
   * @see #enableDoubleBuffering
   */
  public static void disableDoubleBuffering (Component c)
  {
    RepaintManager currentManager = RepaintManager.currentManager (c);
    currentManager.setDoubleBufferingEnabled (false);
  }


  /**
   * Re-enables double buffering globally.
   */
  public static void enableDoubleBuffering (Component c)
  {
    RepaintManager currentManager = RepaintManager.currentManager (c);
    currentManager.setDoubleBufferingEnabled (true);
  }
}