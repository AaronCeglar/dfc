package dsto.dfc.swing.controls;

import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ComboBoxEditor;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;

import dsto.dfc.swing.icons.NullIcon;

/**
 * TextField-like control with an icon display for showing an
 * appropriate icon to the left of the text. Implements the
 * ComboBoxEditor interface so it can be used in a combo box.
 *
 * @version $Revision$
 */
public class IconTextField extends JPanel implements ComboBoxEditor
{
  private static final Icon EMPTY_ICON = new NullIcon (16, 16);

  protected boolean actionOnLoseFocus = false;
  protected GridBagLayout gridBagLayout1 = new GridBagLayout();
  protected JLabel iconLabel = new JLabel();
  protected MyTextField textField = new MyTextField();

  public IconTextField ()
  {
    Listener listener = new Listener ();

    textField.addFocusListener (listener);
    textField.addKeyListener (listener);
    textField.addActionListener (listener);
    textField.setRequestFocusEnabled (true);
    this.setRequestFocusEnabled (true);

    textField.setFont (getFont ());

    if (!UIManager.getLookAndFeel ().getID ().equals ("Windows"))
        setBorder (UIManager.getBorder ("TextField.border"));

    jbInit ();
  }

  public void updateUI ()
  {
    super.updateUI ();

    if (textField != null)
    {
      setBackground (UIManager.getColor ("TextField.background"));
      setOpaque (true);
      iconLabel.setOpaque (false);

      if (!UIManager.getLookAndFeel ().getID ().equals ("Windows"))
        setBorder (UIManager.getBorder ("TextField.border"));
    }
  }

  public void setFont (Font newFont)
  {
    super.setFont (newFont);

    if (textField != null)
      textField.setFont (newFont);
  }

  public void setText (String newValue)
  {
    textField.setText (newValue);
    updateIcon ();
  }

  public String getText ()
  {
    return textField.getText ();
  }

  public int getColumns ()
  {
    return textField.getColumns ();
  }

  public void setColumns (int newValue)
  {
    textField.setColumns (newValue);
  }

  public void setEditable (boolean newValue)
  {
    textField.setEditable (newValue);
    setBorder (textField.getBorder ());

    setEnabled (newValue);
    if (newValue)
    {
      setBackground (UIManager.getColor ("TextField.background"));
      setBorder (UIManager.getBorder ("TextField.border"));
    } else
    {
      // why isn't there a TextField.disabledBackground key?
      if (UIManager.getLookAndFeel ().getID ().equals ("Windows"))
        setBackground (UIManager.getColor ("TextField.background"));
      else
        setBackground (UIManager.getColor ("Label.background"));

      JTextField dummy = new JTextField ();
      setBorder (dummy.getBorder ());
    }
  }

  public boolean isEditable ()
  {
    return textField.isEditable ();
  }

  public void requestFocus ()
  {
    textField.requestFocus ();
  }

  /**
   * Control whether action event is posted when editor loses focus
   * (false by default). This enables "auto commit" of values when
   * the user changes focus but can cause problems where selections
   * from the drop down list fail have any effect, possibly due to
   * some sort of swing bug.
   */
  public void setActionOnLoseFocus (boolean newValue)
  {
    actionOnLoseFocus = newValue;
  }

  public boolean getActionOnLoseFocus ()
  {
    return actionOnLoseFocus;
  }

  public Icon getIcon ()
  {
    if (iconLabel.getIcon () != EMPTY_ICON)
      return iconLabel.getIcon ();
    else
      return null;
  }

  public void setIcon (Icon newValue)
  {
    if (newValue != null)
      iconLabel.setIcon (newValue);
    else
      iconLabel.setIcon (EMPTY_ICON);
  }

  /**
   * Override this to update the icon to match the current state of the
   * control.
   */
  public void updateIcon ()
  {
    // zip
  }

  /**
   * Called when the text field is edited. Subclasses may choose to override
   * this. Default is to clear the displayed icon.
   *
   * @see #textEditFinished
   */
  protected void textEdited ()
  {
    iconLabel.setIcon (EMPTY_ICON);
  }

  /**
   * Called when the user has finished editing the text field.
   *
   * @see #textEdited
   */
  protected void textEditFinished ()
  {
    updateIcon ();
  }

  private void jbInit ()
  {
    this.setLayout(gridBagLayout1);
    iconLabel.setIcon(EMPTY_ICON);
    iconLabel.setOpaque (false);
    this.setBackground(UIManager.getColor ("TextField.background"));
    this.setOpaque (true);
    textField.setBorder(null);
    textField.setOpaque (false);
    this.add(iconLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    this.add(textField, new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 2, 0, 0), 0, 0));
  }

  // ComboBoxEditor interface

  public Component getEditorComponent ()
  {
    return this;
  }

  public void setItem (Object anObject)
  {
    textField.setText (anObject == null ? "" : anObject.toString ());
    updateIcon ();
  }

  public Object getItem ()
  {
    return textField.getText ();
  }

  public void selectAll ()
  {
    textField.selectAll ();
    // basic PLAF editor does this, so we do it too
    textField.requestFocus ();
  }

  public void addActionListener (ActionListener l)
  {
    textField.addActionListener (l);
  }

  public void removeActionListener (ActionListener l)
  {
    textField.removeActionListener (l);
  }

  static class MyTextField extends JTextField
  {
    public void updateUI ()
    {
      super.updateUI ();

      setBorder (null);
      setOpaque (false);
    }
  }

  class Listener implements ActionListener, FocusListener, KeyListener
  {
    public void actionPerformed (ActionEvent e)
    {
      textEditFinished ();
    }

    public void focusGained (FocusEvent e)
    {
      // zip
    }

    public void focusLost (FocusEvent e)
    {
      if (actionOnLoseFocus)
        textField.postActionEvent ();

      textEditFinished ();
    }

    public void keyTyped (KeyEvent e)
    {
      // zip
    }

    public void keyPressed (KeyEvent e)
    {
      if (!e.isActionKey () &&
          e.getKeyCode () != KeyEvent.VK_ENTER)
      {
        textEdited ();
      }
    }

    public void keyReleased (KeyEvent e)
    {
      // zip
    }
  }
}