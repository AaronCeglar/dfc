package dsto.dfc.swing.controls;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

/**
 * Utilties for geometric calculations.
 *
 * @version $Revision$
 */
public final class Geometry
{
  public static final int TOP_LEFT = 0;
  public static final int MID_TOP = 1;
  public static final int TOP_RIGHT = 2;
  public static final int MID_RIGHT = 3;
  public static final int BOTTOM_RIGHT = 4;
  public static final int MID_BOTTOM = 5;
  public static final int BOTTOM_LEFT = 6;
  public static final int MID_LEFT = 7;
  public static final int CENTER = 8;

  private Geometry()
  {
    // zip
  }

  /**
   * Calculate offset of a child region from a parent region necessary
   * to align child region.
   *
   * @param parentSize Size of parent region.
   * @param childSize Size of child region;
   * @param alignment Alignment of child relative to parent.
   */
  public static Point offsetForAligment (Dimension parentSize,
                                         Dimension childSize,
                                         int alignment)
  {
    Point location = new Point();

    switch (alignment)
    {
      case TOP_LEFT :
      case BOTTOM_LEFT :
      case MID_LEFT :
        location.x = 0;
        break;
      case MID_TOP :
      case CENTER :
      case MID_BOTTOM :
        location.x = Math.max(0, (parentSize.width - childSize.width) / 2);
        break;
      default :
        location.x = Math.max(0, parentSize.width - childSize.width);
    }

    switch (alignment)
    {
      case TOP_LEFT :
      case MID_TOP :
      case TOP_RIGHT :
        location.y = 0;
        break;
      case MID_LEFT :
      case CENTER :
      case MID_RIGHT :
        location.y = Math.max(0, (parentSize.height - childSize.height) / 2);
        break;
      default :
        location.y = Math.max(0, parentSize.height - childSize.height);
    }

    return location;
  }

  /*
   * Make sure that the childRect is in the parentRect. and move it into it if not.
   */
  public static void bumpRectangle(Rectangle parentRect, Rectangle childRect)
  {
    int x_diff =
      (childRect.x + childRect.width) - (parentRect.x + parentRect.width);
    int y_diff =
      (childRect.y + childRect.height) - (parentRect.y + parentRect.height);

    /* To far right */
    if (x_diff > 0)
      childRect.x -= x_diff;

    /* To far down */
    if (y_diff > 0)
      childRect.y -= y_diff;

    /* To far left */
    if (childRect.x < parentRect.x)
      childRect.x = parentRect.x;

    /* To far up */
    if (childRect.y < parentRect.y)
      childRect.y = parentRect.y;
  }
}
