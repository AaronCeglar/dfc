package dsto.dfc.swing.controls;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class contains an List of colors, and its associated name.
 *
 * @author       Luke Marsh
 * @version      $Revision$
 *
 * @see ColorSchemePanel
 * @see ColorChooserPanel
 * @see ColorChooserCombo
 */
public class ColorScheme
{

  // Predefined Schemes.
  // The color names came from:
  // http://www.two4u.com/color/big-table.html
  // and the scheme patterns from:
  // http://www.graphicdesignbasics.com/article1005.html
  public static final List PASTEL_COLORS = Arrays.asList (new Color []
  {
    new Color (239, 214, 214), // MistyRose2
    new Color (254, 203, 154), // PeachPuff1
    new Color (245, 249, 187), // Khaki1
    new Color (197, 255, 197), // DarkSeaGreen1
    new Color (189, 204, 239), // LightSteelBlue2
    new Color (251, 173, 241), // Pink
    new Color (199, 157, 199), // Thistle
  });

  public static final List PASTEL_COLORS_NAMES = Arrays.asList (new String []
  {
    "Misty Rose", "Peach Puff", "Lemon", "Sea Green", "Steel Blue",
    "Pink", "Thistle"
  });

  public static final List METALLIC_COLORS = Arrays.asList (new Color []
  {
    new Color (255, 255, 254), // White
    new Color (185, 211, 238), // SlateGrey2
    new Color (159, 182, 205), // SlateGrey3
    new Color (119, 136, 153), // LightSlateGrey
    new Color (82, 139, 139), // DarkSlateGrey4
    new Color (143, 188, 143), // DarkSeaGreen3
    new Color (170, 170, 170), // Grey
    new Color (105, 105, 105), // DimGrey
    new Color (0, 1, 0) // Black
  });

  public static final List METALLIC_COLORS_NAMES = Arrays.asList (new String []
  {
    "White", "Light Blue", "Slate Grey", "Medium Grey", "Slate Green",
    "Forest Green", "Grey", "Dim Grey", "Black"
  });

  public static final List PRIMARY_COLORS = Arrays.asList (new Color []
  {
    new Color (255, 0, 0), // Red1
    new Color (255, 165, 0), // Orange1
    new Color (255, 255, 0), // Yellow1
    new Color (0, 255, 0), // Green1
    new Color (0, 0, 255), // Blue1
    Color.cyan, // Cyan
    new Color (155, 48, 255), // Purple1
    Color.magenta, // Magenta
    new Color (238, 130, 238), // Violet
    Color.white, // White
    Color.lightGray, // Light Gray
    Color.gray, // Gray
    Color.darkGray, // Dark Gray
    Color.black, // Black
    new Color (0.01f, 0.01f, 0.01f) // Default
  });

  public static final List PRIMARY_COLORS_NAMES = Arrays.asList (new String []
  {
    "Red", "Orange", "Yellow", "Green", "Blue", "Cyan", "Purple", "Magenta",
    "Violet", "White", "Light Gray", "Gray", "Dark Gray", "Black", "Default"
  });

  public static final List AQUAMARINE_COLORS = Arrays.asList (new Color []
  {
    new Color (137, 255, 232), // Aquamarine1
    new Color (102, 205, 170), // Aquamarine3
    new Color (69, 139, 116), // Aquamarine4
    new Color (120, 182, 255), // SteelBlue2
    new Color (0, 10, 250), // Blue
    new Color (0, 137, 200), // Turquoise3
    new Color (48, 255, 128) // Seagreen2
  });

  public static final List AQUAMARINE_COLORS_NAMES = Arrays.asList (new String []
  {
    "Aquamarine1", "Aquamarine3", "Aquamarine4", "Steel Blue", "Blue",
    "Turquoise", "Sea Green"
  });

  public static final List DARK_COLORS = Arrays.asList (new Color []
  {
    new Color (139, 24, 24), // Firebrick4
    new Color (139, 139, 0), // Yellow4
    new Color (0, 100, 0), // DarkGreen
    new Color (109, 43, 183), // Purple4
    new Color (139, 0, 139), // Magenta4
    new Color (0, 0, 132), // Navy
    new Color (74, 74, 74) // Grey29
  });

  public static final List DARK_COLORS_NAMES = Arrays.asList (new String []
  {
    "Firebrick", "Yellow", "Dark Green", "Purple",
    "Magenta", "Navy", "Dark Grey"
  });

  public static final List MOUNTAIN_COLORS = Arrays.asList (new Color []
  {
    new Color (164, 211, 238), // LightSkyBlue2
    new Color (162, 181, 255), // LightSteelBlue3
    new Color (79, 148, 205), // SteelBlue3
    new Color (0, 94, 160), // DeepSkyBlue4
    new Color (221, 160, 221), // Plum
    new Color (205, 41, 144), // Maroon3
    new Color (149, 5, 149), // Magenta4
    new Color (0, 0, 138), // Navy
  });

  public static final List MOUNTAIN_COLORS_NAMES = Arrays.asList (new String []
  {
    "Sky Blue", "Pale Steel Blue", "Steel Blue", "Deep Sky Blue",
    "Plum", "Maroon", "Magenta", "Navy"
  });

  public static final List CONTINENT_COLORS = Arrays.asList (new Color []
  {
    new Color (164, 211, 238), // LightSkyBlue2
    new Color (80, 148, 205), // SteelBlue3
    new Color (0, 94, 159), // DeepSkyBlue4
    new Color (26, 149, 90), // SeaGreen
    new Color (0, 205, 0), // Green3
    new Color (152, 251, 152), // PaleGeen
    new Color (107, 142, 35), // OliveDrab
    new Color (218, 165, 32), // Goldenrod
    new Color (160, 82, 45), // Sienna
    new Color (1, 0, 0) // Black
  });

  public static final List CONTINENT_COLORS_NAMES = Arrays.asList (new String []
  {
    "Sky Blue", "Steel Blue", "Deep Sky Blue", "Sea Green", "Green",
    "Pale Green", "Olive Drab", "Goldenrod", "Sienna", "Black"
  });

  public static final List GARDEN_COLORS = Arrays.asList (new Color []
  {
    new Color (139, 69, 19), // SaddleBrown
    new Color (255, 254, 0), // Yellow
    new Color (255, 140, 0), // DarkOrange
    new Color (0, 139, 0), // Green4
    new Color (127, 255, 0), // Chartreuse
    new Color (176, 226, 255), // LightSkyBlue1
    new Color (255, 0, 255), // Magenta1
    new Color (159, 121, 238), // MediumPurple2
  });

  public static final List GARDEN_COLORS_NAMES = Arrays.asList (new String []
  {
    "Saddle Brown", "Yellow", "Dark Orange", "Green", "Chartreuse",
    "Sky Blue", "Pink", "Purple"
  });

  public static final List YELLOWTOORANGE_COLORS = Arrays.asList (new Color []
  {
    new Color (205, 205, 0), // Yellow3
    new Color (254, 255, 0), // Yellow2
    new Color (245, 175, 34), // GoldenRod2
    new Color (240, 124, 0), // Orange2
    new Color (239, 75, 0), // DarkOrange1
    new Color (173, 72, 5), // Chocolate1
    new Color (115, 82, 11), // DarkGoldenrod4
  });

  public static final List YELLOWTOORANGE_COLORS_NAMES = Arrays.asList
    (new String []
  {
    "Dull Yellow", "Yellow", "Gold", "Orange", "Dark Orange",
    "Chocolate", "Brown"
  });

  public static final List CHRISTMAS_COLORS = Arrays.asList (new Color []
  {
    new Color (214, 214, 214), // Grey84
    new Color (255, 185, 15), // DarkGoldenrod1
    new Color (254, 0, 0), // Red
    new Color (205, 38, 38), // Firebrick3
    new Color (139, 0, 0), // Red4
    new Color (139, 0, 139), // Magenta4
    new Color (0, 90, 0), // DarkGreen
    new Color (0, 149, 0), // Green2
    new Color (0, 238, 0), // Green4
  });

  public static final List CHRISTMAS_COLORS_NAMES = Arrays.asList (new String []
  {
    "Light Grey", "Goldenrod", "Red", "Firebrick", "Dark Red",
    "Magenta", "Dark Green", "Green", "Light Green"
  });

  public static final ColorScheme PASTELS =
    new ColorScheme ("Pastels", PASTEL_COLORS, PASTEL_COLORS_NAMES);

  public static final ColorScheme METALLIC =
    new ColorScheme ("Metallic", METALLIC_COLORS, METALLIC_COLORS_NAMES);

  public static final ColorScheme PRIMARY =
    new ColorScheme ("Primary", PRIMARY_COLORS, PRIMARY_COLORS_NAMES);

  public static final ColorScheme AQUAMARINE =
    new ColorScheme ("Aquamarine", AQUAMARINE_COLORS, AQUAMARINE_COLORS_NAMES);

  public static final ColorScheme DARK =
    new ColorScheme ("Dark", DARK_COLORS, DARK_COLORS_NAMES);

  public static final ColorScheme MOUNTAIN =
    new ColorScheme ("Mountain", MOUNTAIN_COLORS, MOUNTAIN_COLORS_NAMES);

  public static final ColorScheme CONTINENT =
    new ColorScheme ("Continent", CONTINENT_COLORS, CONTINENT_COLORS_NAMES);

  public static final ColorScheme GARDEN =
    new ColorScheme ("Garden", GARDEN_COLORS, GARDEN_COLORS_NAMES);

  public static final ColorScheme YELLOWTOORANGE = new ColorScheme
    ("Desert", YELLOWTOORANGE_COLORS, YELLOWTOORANGE_COLORS_NAMES);

  public static final ColorScheme CHRISTMAS = new ColorScheme
    ("Christmas", CHRISTMAS_COLORS, CHRISTMAS_COLORS_NAMES);

  public static final List DEFAULT_SCHEMES = Arrays.asList (new ColorScheme []
    {
      PASTELS, METALLIC, PRIMARY, AQUAMARINE, MOUNTAIN, DARK, CONTINENT,
      GARDEN, YELLOWTOORANGE, CHRISTMAS
    }
  );

  private List colors;  /** The List of Color. */
  private List colorNames; /** The name of each color. */
  private String name;  /** The name of the color scheme. */

  /**
   * Initializes parameters. The Color List is set to a size of 500 in
   * this default constructor.
   */
  public ColorScheme ()
  {
    this ("", new ArrayList (), new ArrayList ());
  }


  /**
   * Takes two arguments to intialize the global variables.
   */
  public ColorScheme (String n, List c, List cNames)
  {
    colors = c;
    colorNames = cNames;
    name = n;
  }


  /**
   * Returns the name of the List.
   */
  public String getName ()
  {
    return name;
  }


  /**
   * Returns the List of Colors.
   */
  public List getColors ()
  {
    return colors;
  }


  /**
   * Returns the List of Color Names.
   */
  public List getColorNames ()
  {
    return colorNames;
  }


  /**
   * Takes the String n as a parameter and
   * sets it to the name String.
   */
  public void setName (String n)
  {
    name = n;
  }


  /**
   * Takes an List of Color c and sets it to the
   * Color List colors.
   */
  public void setColors (List c)
  {
    colors = c;
  }


  /**
   * Takes an List of Strings cNames and sets it to the
   * ColorNames List.
   */
  public void setColorNames (List cNames)
  {
    colorNames = cNames;
  }


  /**
   * Returns the name of a specific color or its RGB value if it doesn't exist.
   */
  public static String getNameColor (Color color)
  {
    String nameColor = "";
    for (int i = 0; i < DEFAULT_SCHEMES.size (); i++)
    {
      // Search for the selectedColor in all of the schemes.
      List colors = ((ColorScheme) DEFAULT_SCHEMES.get (i)).getColors ();
      for (int j = 0; j < colors.size (); j++)
      {
        // Search the ArrayList of colors to match a color and get its name.
        if (color.equals (colors.get (j)))
        {
          ColorScheme scheme = (ColorScheme) DEFAULT_SCHEMES.get (i);
          nameColor = scheme.getColorNames ().get (j).toString ();
          break;
        }
      }

      if (!nameColor.equals (""))
      {
        break;
      }
    }

    if (nameColor.equals (""))
    {
      nameColor = "[" + color.getRed () + ", " + color.getGreen ()
              + ", " + color.getBlue () + "]";
    }
    return nameColor;
  }
}