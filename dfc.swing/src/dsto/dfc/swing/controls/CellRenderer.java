package dsto.dfc.swing.controls;

import javax.swing.JComponent;
import javax.swing.ListCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.TreeCellRenderer;

/**
 * Defines a unified cell renderer interface for objects that can
 * render in tables, trees and lists.
 *
 * @see dsto.dfc.swing.controls.AbstractCellRenderer
 *
 * @version $Revision$
 */
public interface CellRenderer
  extends ListCellRenderer, TableCellRenderer, TreeCellRenderer
{
  /**
   * The unified renderer component access method.  Clients may either
   * call this method to obtain a renderer component, or one of the
   * list, table or tree cell renderer access methods.
   */
  public JComponent getRendererComponent (JComponent client,
                                          Object value,
                                          int row, int column,
                                          boolean isSelected,
                                          boolean hasFocus,
                                          boolean expanded,
                                          boolean leaf);
} 
