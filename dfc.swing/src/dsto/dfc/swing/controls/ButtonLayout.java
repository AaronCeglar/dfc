package dsto.dfc.swing.controls;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;

/**
 * Lays out a row of horizontal buttons neatly.
 *
 * @version $Revision$
 */
public class ButtonLayout implements LayoutManager2
{
  public static final int ALIGN_LEFT = 0;
  public static final int ALIGN_RIGHT = 1;
  public static final int ALIGN_CENTER = 2;
  public static final int ALIGN_TOP = ALIGN_LEFT;
  public static final int ALIGN_BOTTOM = ALIGN_RIGHT;

  public static final int DEFAULT_HGAP = 5;
  public static final int DEFAULT_VERT_ALIGNMENT = ALIGN_BOTTOM;
  public static final int DEFAULT_HORIZ_ALIGNMENT = ALIGN_RIGHT;

  protected int hgap = DEFAULT_HGAP;
  protected int horizAlignment = DEFAULT_HORIZ_ALIGNMENT;
  protected int vertAlignment = DEFAULT_VERT_ALIGNMENT;

  public void setHGap (int hgap)
  {
    this.hgap = hgap;
  }

  public int getHGap ()
  {
    return hgap;
  }

  public void setVertAlignment (int newVertAlignment)
  {
    vertAlignment = newVertAlignment;
  }

  public int getVertAlignment ()
  {
    return vertAlignment;
  }

  public void setHorizAlignment (int newHorizAlignment)
  {
    horizAlignment = newHorizAlignment;
  }

  public int getHorizAlignment ()
  {
    return horizAlignment;
  }

  protected Dimension computeLayout (Container container,
                                     boolean placeComponents)
  {
    /* compute available layout area */

    Dimension preferredSize = new Dimension (0, 0);

    Insets insets = container.getInsets ();
    Dimension layoutArea = container.getSize ();
    layoutArea.width -= insets.left + insets.right;
    layoutArea.height -= insets.top + insets.bottom;

    /* find max preferred component size */

    Dimension maxPreferredSize = new Dimension (0, 0);
    int visibleEntries = 0;

    for (int i = 0; i < container.getComponentCount (); i++)
    {
      Component component = container.getComponent (i);

      if (!component.isVisible ())
        continue;

      visibleEntries++;
      Dimension componentSize = component.getPreferredSize ();

      // update max width/height
      maxPreferredSize.width =
        Math.max (componentSize.width, maxPreferredSize.width);
      maxPreferredSize.height =
        Math.max (componentSize.height, maxPreferredSize.height);
    }

    /* compute preferred size (not taking insets into account) from
       max preferred size and hgap */

    preferredSize.width = maxPreferredSize.width * visibleEntries;
    preferredSize.width += hgap * (visibleEntries - 1);
    preferredSize.height = maxPreferredSize.height;

    /* compute horiz alignment offset from preferredSize.width before
       insets are added */

    int horizOffset =
      computeAlignmentOffset (layoutArea.width, preferredSize.width,
                              horizAlignment);

    /* add insets to preferredSize */

    preferredSize.width += insets.left + insets.right;
    preferredSize.height += insets.top + insets.bottom;

    /* assign component positions and sizes */

    if (placeComponents)
    {
      for (int x = horizOffset + insets.left, i = 0;
           i < container.getComponentCount (); i++)
      {
        Component component = container.getComponent (i);

        if (!component.isVisible ())
          continue;

        Dimension componentSize = component.getPreferredSize ();

        int y = insets.top +
          computeAlignmentOffset (layoutArea.height, componentSize.height,
                                  vertAlignment);
        component.setLocation (x, y);
        component.setSize (maxPreferredSize.width, componentSize.height);

        x += maxPreferredSize.width + hgap;
      }
    }

    return preferredSize;
  }

  public void layoutContainer (Container parent)
  {
    computeLayout (parent, true);
  }

  public Dimension maximumLayoutSize (Container target)
  {
    return new Dimension (Integer.MAX_VALUE, Integer.MAX_VALUE);
  }

  public Dimension preferredLayoutSize (Container parent)
  {
    return computeLayout (parent, false);
  }

  public Dimension minimumLayoutSize (Container parent)
  {
    return preferredLayoutSize (parent);
  }

  public void addLayoutComponent (Component comp, Object constraints)
  {
    // zio
  }

  public void addLayoutComponent (String name, Component comp)
  {
    // zip
  }

  public void removeLayoutComponent (Component comp)
  {
    // zip
  }

  public float getLayoutAlignmentX (Container target)
  {
    return 0.0f;
  }

  public float getLayoutAlignmentY (Container target)
  {
    return 0.0f;
  }

  public void invalidateLayout (Container target)
  {
    //z ip
  }

  /**
   * Compute offset to place a contained extent for a given alignment.
   *
   * @param containerExtent The extent that the sub-extent will be
   *        placed in.
   * @param extent The contained sub-extent,
   * @param alignment The extent alignment.  One of the ALIGN_* constants.
   * @return The offset required to align the sub-extent.
   */
  protected static int computeAlignmentOffset (int containerExtent,
                                               int extent, int alignment)
  {
    switch (alignment)
    {
    case ALIGN_LEFT:
      return 0;
    case ALIGN_CENTER:
      return Math.max ((containerExtent - extent) / 2, 0);
    case ALIGN_RIGHT:
      return Math.max (containerExtent - extent, 0);
    }

    return 0;
  }
}
