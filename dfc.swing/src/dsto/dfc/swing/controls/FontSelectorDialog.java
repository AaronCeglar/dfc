package dsto.dfc.swing.controls;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
/**
 * Display a FontSelector control and a preview label in a dialog.
 *
 * @version $Revision$
 */
public class FontSelectorDialog extends DfcOkCancelDialog
{
  private Font selectedFont = new Font ("Dialog", Font.PLAIN, 12);
  private JPanel contentPanel = new JPanel();
  private FontSelector fontSelector = new FontSelector();
  private JLabel previewLabel = new JLabel();
  private Border border2;
  private GridBagLayout gridBagLayout1 = new GridBagLayout();

  public FontSelectorDialog (Component client, String title, boolean modal)
  {
    super (client, title, modal);

    setDialogPanel (contentPanel);

    try
    {
      jbInit();
      pack();
    } catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public FontSelectorDialog()
  {
    this (null, "", false);
  }

  public Font getSelectedFont ()
  {
    return selectedFont;
  }

  public void setSelectedFont (Font font)
  {
    Font oldValue = selectedFont;

    selectedFont = font;

    fontSelector.setSelectedFont (selectedFont);

    firePropertyChange ("selectedFont", oldValue, selectedFont);
  }

  public boolean saveResult ()
  {
    setSelectedFont (fontSelector.getSelectedFont ());

    return true;
  }

  void jbInit() throws Exception
  {
    border2 = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new java.awt.Color(134, 134, 134)),"Preview"),BorderFactory.createEmptyBorder(5,5,10,5));
    contentPanel.setLayout(gridBagLayout1);
    previewLabel.setBorder(border2);
    previewLabel.setMaximumSize(new Dimension(500, 60));
    previewLabel.setMinimumSize(new Dimension(500, 60));
    previewLabel.setPreferredSize(new Dimension(250, 60));
    previewLabel.setHorizontalAlignment(SwingConstants.CENTER);
    previewLabel.setText("The quick brown fox jumps over the lazy dog");
    previewLabel.setForeground (Color.black);
    fontSelector.setPreviewLabel(previewLabel);
    this.setTitle("Select Font");
    contentPanel.add(fontSelector, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    contentPanel.add(previewLabel, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
  }
}

