package dsto.dfc.swing.controls;

import dsto.dfc.util.BasicPropertyEventSource;

/**
 * This class is used to represent a colour gradient. It is used
 * by the GradientBar and the GradientPanel.
 *
 * When ever any of the gradients properties are changed, a PropertyChange is
 * fired. If you want to know when certain props change you can simply add yourself
 * as a PropertyChangeListener to the Gradient.
 */

public class Gradient extends BasicPropertyEventSource
{
  private float initialHue;
  private float colourSpan;
  private float saturation;
  private float brightness;
  private boolean reverse;

  /**
   * This contructor sets all the paramaters to 0.0f. Ie you shouldn't using
   * this one. This is there where people prefer to use setter methods on the
   * object.
   */
  public Gradient()
  {
    this(0.0f, 0.0f, 0.0f, 0.0f, false);
  }

  /**
   * @param initialHue This is the starting colour. Where your spectrum ranges
   *                   from 0.0f to 1.0f (going through the rainbow colours).
   * @param colourSpan This is the width of the spectrum you want to use ranging
   *                   from 0.0f to 1.0f.
   * @param saturation This is the saturation of the colours.
   * @param brightness This is the brightness of the colours.
   *
   */
  public Gradient(float initialHue, float colourSpan,
                  float saturation, float brightness, boolean reverse)
  {
    this.initialHue = initialHue;
    this.colourSpan = colourSpan;
    this.saturation = saturation;
    this.brightness = brightness;
    this.reverse = reverse;
  }

  public float getColourSpan ()
  {
    return colourSpan;
  }

  public void setColourSpan (float newColourSpan)
  {
    float oldColourSpan = this.colourSpan;
    this.colourSpan = newColourSpan;
    this.firePropertyChange("colourSpan", oldColourSpan, newColourSpan);
  }

  public float getInitialHue ()
  {
    return initialHue;
  }

  public void setInitialHue (float newInitialHue)
  {
    float oldInitialHue = this.initialHue;
    this.initialHue = newInitialHue;
    this.firePropertyChange("initialHue", oldInitialHue, newInitialHue);
  }

  public float getSaturation ()
  {
    return saturation;
  }

  public void setSaturation (float newSaturation)
  {
    float oldSaturation = this.saturation;
    this.saturation = newSaturation;
    this.firePropertyChange("saturation", oldSaturation, newSaturation);
  }

  public float getBrightness ()
  {
    return brightness;
  }

  public void setBrightness (float newBrightness)
  {
    float oldBrightness = this.brightness;
    this.brightness = newBrightness;
    this.firePropertyChange("brightness", oldBrightness, newBrightness);
  }

  public boolean isReverse ()
  {
    return reverse;
  }

  public void setReverse (boolean newReverse)
  {
    boolean oldReverse = this.reverse;
    this.reverse = newReverse;
    this.firePropertyChange("reverse", oldReverse, newReverse);
  }
}