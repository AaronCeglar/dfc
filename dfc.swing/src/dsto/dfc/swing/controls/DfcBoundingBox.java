package dsto.dfc.swing.controls;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.BoundedRangeModel;
import javax.swing.GrayFilter;
import javax.swing.JComponent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * This component can be used to graphically define the size of a rectangle, using
 * only two points; The topLeft, and bottomRight.
 * You can also invert the Y-Axis so that it becomes a cartesian plane instead of
 * your usual computer screen type coordinate system (ie 0,0 is the top left corner,
 * and going down the screen is an increase in Y coordinate.)
 * You can also set a background image by using setBackgroundImage(Image).
 */

public class DfcBoundingBox extends JComponent
{
  private Circle topLeft;
  private Circle bottomRight;
  private BoundedRangeModel topLeftXRangeModel;
  private BoundedRangeModel topLeftYRangeModel;
  private BoundedRangeModel bottomRightXRangeModel;
  private BoundedRangeModel bottomRightYRangeModel;
  private int adjustX, adjustY;
  private Image backgroundImage = null, disabledBackgroundImage = null;
  protected boolean invertYAxis = false;

  /**
   * @param x1 Top left X-Axis bounded range model.
   * @param y1 Top Left Y-Axis bounded range model.
   * @param x2 Bottom Right X-Axis bounded range model.
   * @param y2 Bottom Right Y-Axis bounded range model.
   */
  public DfcBoundingBox(BoundedRangeModel x1, BoundedRangeModel y1,
                        BoundedRangeModel x2, BoundedRangeModel y2)
  {
    this(x1, y1, x2, y2, false);
  }

  /**
   * @param x1 Top left X-Axis bounded range model.
   * @param y1 Top Left Y-Axis bounded range model.
   * @param x2 Bottom Right X-Axis bounded range model.
   * @param y2 Bottom Right Y-Axis bounded range model.
   * @param invertYAxis If set to true, then the Y-Axis will be inverted.
   */
  public DfcBoundingBox(BoundedRangeModel x1, BoundedRangeModel y1,
                        BoundedRangeModel x2, BoundedRangeModel y2, boolean invertYAxis)
  {
    this.invertYAxis = invertYAxis;
    this.setLayout(null);
    topLeftXRangeModel = x1;
    topLeftYRangeModel = y1;
    bottomRightXRangeModel = x2;
    bottomRightYRangeModel = y2;
    topLeft = new Circle (10, 10, topLeftXRangeModel, topLeftYRangeModel);
    bottomRight = new Circle (10, 10, bottomRightXRangeModel, bottomRightYRangeModel);
    if (topLeftXRangeModel.getMinimum() <= 0 )
    {
      adjustX = Math.abs(topLeftXRangeModel.getMinimum());
      adjustY = Math.abs(topLeftYRangeModel.getMinimum());
    }else
    {
      adjustX = -(topLeftXRangeModel.getMinimum());
      adjustY = -(topLeftYRangeModel.getMinimum());
    }
    this.add(topLeft);
    this.add(bottomRight);
  }

  /**
   * retrieve the background image. If there isn't one, it returns null;
   *
   * @return The background image.
   */
  public Image getBackgroundImage()
  {
    return backgroundImage;
  }

  /**
   * Enable or disable the component.
   *
   * @param enabled If true, will enable the component, and vice-versa.
   */
  public void setEnabled(boolean enabled)
  {
    super.setEnabled(enabled);
    topLeft.setEnabled(enabled);
    bottomRight.setEnabled(enabled);
  }

  /**
   * Set the background image, to the specified iamge.
   *
   * @param image The image to use as the background image. The image will be
   * scaled to fit.
   */
  public void setBackgroudImage (Image image)
  {
    this.backgroundImage = image;
    this.disabledBackgroundImage = GrayFilter.createDisabledImage(backgroundImage);
  }

  /**
   * Overwriting JComponents paintComponent method, so that we can add some lines,
   * and a red outline which highlights the selected region.
   *
   * @param g The graphics context that we use to draw onto.
   */
  protected void paintComponent(Graphics g)
  {
    Insets insets = getInsets ();
    g.translate (insets.left, insets.top);

    if (this.isEnabled())
      g.setColor(Color.black);
    else
      g.setColor(Color.gray);

    if (backgroundImage != null)
    {
      if (this.isEnabled())
        g.drawImage(backgroundImage, 0, 0, this.getWidth(), this.getHeight(), null);
      else
        g.drawImage(disabledBackgroundImage, 0, 0, this.getWidth(), this.getHeight(), null);
    }

    topLeft.setBottomRightConstraint(bottomRight.getLocation());
    bottomRight.setTopLeftContstraint(topLeft.getLocation());

    g.drawLine(topLeftXRangeModel.getValue()+adjustX, 0, topLeftXRangeModel.getValue()+adjustX, getHeight());
    g.drawLine(bottomRightXRangeModel.getValue()+adjustX, 0, bottomRightXRangeModel.getValue()+adjustX, getHeight());

    if (invertYAxis)
    {
      g.drawLine(0, (-topLeftYRangeModel.getValue())+adjustY, getWidth(), (-topLeftYRangeModel.getValue())+adjustY);
      g.drawLine(0, (-bottomRightYRangeModel.getValue())+adjustY, getWidth(), (-bottomRightYRangeModel.getValue())+adjustY);

      if (this.isEnabled())
        g.setColor(Color.red);
      else
        g.setColor(Color.pink);

      g.drawRect(topLeftXRangeModel.getValue()+adjustX, (-topLeftYRangeModel.getValue())+adjustY,
          (bottomRightXRangeModel.getValue()+adjustX) - (topLeftXRangeModel.getValue()+adjustX),
          ((-bottomRightYRangeModel.getValue())+adjustY) - ((-topLeftYRangeModel.getValue())+adjustY));
    }
    else
    {
      g.drawLine(0, topLeftYRangeModel.getValue()+adjustY, getWidth(), topLeftYRangeModel.getValue()+adjustY);
      g.drawLine(0, bottomRightYRangeModel.getValue()+adjustY, getWidth(), bottomRightYRangeModel.getValue()+adjustY);

      if (this.isEnabled())
        g.setColor(Color.red);
      else
        g.setColor(Color.pink);

      g.drawRect(topLeftXRangeModel.getValue()+adjustX, topLeftYRangeModel.getValue()+adjustY,
          (bottomRightXRangeModel.getValue()+adjustX) - (topLeftXRangeModel.getValue()+adjustX),
          (bottomRightYRangeModel.getValue()+adjustY) - (topLeftYRangeModel.getValue()+adjustY));
    }

    g.translate (-insets.left, -insets.top);
  }

  private int getRangeModelLength(BoundedRangeModel rangeModel)
  {
    if (rangeModel.getMinimum() < 0 && rangeModel.getMaximum() > 0)
      return Math.abs(rangeModel.getMinimum()) + rangeModel.getMaximum();
    else if (rangeModel.getMinimum() > 0 && rangeModel.getMaximum() < 0)
      return Math.abs(rangeModel.getMaximum()) + rangeModel.getMinimum();
    else if (rangeModel.getMinimum() > 0 && rangeModel.getMaximum() > rangeModel.getMinimum())
      return rangeModel.getMaximum() - rangeModel.getMinimum();
    else if (rangeModel.getMinimum() > 0 && rangeModel.getMaximum() < rangeModel.getMinimum())
      return rangeModel.getMinimum() - rangeModel.getMaximum();
    else
      return -1;
  }

  /**
   * Returns the maximum size of this component as the lengths of the range models.
   *
   * @return The max size of the component as a Dimension.
   */
  public Dimension getMaximumSize()
  {
    return new Dimension(getRangeModelLength(topLeftXRangeModel)+1,
                         getRangeModelLength(topLeftYRangeModel)+1);
  }

  /**
   * Returns the minimum size of this component as the lengths of the range models
   *
   * @return The min size of the component as a Dimension.
   */
  public Dimension getMinimumSize()
  {
    return new Dimension(getRangeModelLength(topLeftXRangeModel)+1,
                         getRangeModelLength(topLeftYRangeModel)+1);
  }

  /**
   * Returns the preferred size of this component as the lengths of the range models.
   *
   * @return The preferred size of the component as a Dimension.
   */
  public Dimension getPreferredSize()
  {
    return new Dimension(getRangeModelLength(topLeftXRangeModel)+1,
                         getRangeModelLength(topLeftYRangeModel)+1);
  }

  /**
   * This is an inner class, which defines the circles which are used by the main
   * component to represent the top left and bottom right of the rectangle.
   * You are also allowed to drag the circles.
   */
  private class Circle extends Component implements MouseMotionListener, ChangeListener
  {
    private int width, height;
    private Point topLeftConstraint = null;
    private Point bottomRightConstraint = null;
    private BoundedRangeModel xModel, yModel;
    private int circleAdjustX, circleAdjustY;

    /**
     * One and only constructor, which sets adds itself as a change listener to
     * the bounded range models, so that it can keep track of where the circles
     * should be.
     *
     * @param width The width of the circles.
     * @param height The height of the circles.
     * @param xModel The bounded range model representing the x-axis.
     * @param yModel The bounded range model representing the y-axis.
     */
    public Circle (int width, int height, BoundedRangeModel xModel, BoundedRangeModel yModel)
    {
      this.width = width;
      this.height = height;
      this.setSize(width+1, height+1);
      this.xModel = xModel;
      this.yModel = yModel;
      xModel.addChangeListener(this);
      yModel.addChangeListener(this);
      circleAdjustX = Math.abs(xModel.getMinimum());
      circleAdjustY = Math.abs(yModel.getMinimum());

      if (invertYAxis)
        setLocation(xModel.getValue()+circleAdjustX, (-yModel.getValue())+circleAdjustY);
      else
        setLocation(xModel.getValue()+circleAdjustX, yModel.getValue()+circleAdjustY);

      this.addMouseMotionListener(this);
    }

    /**
     * Overwriting the paint method on Component, so that we can draw a circle.
     *
     * @param g The graphics context which we draw the circle onto.
     */
    public void paint (Graphics g)
    {
      if (!this.isEnabled())
        g.setColor(Color.gray);

      Insets insets = getParent().getInsets ();
      g.translate(insets.left,insets.top);
      g.setClip(0,0,width, height);
      g.drawOval(0,0,width, height);
      g.translate(-insets.left, -insets.top);
    }

    /**
     * Enable or disable the component. This means disabled/enabling mouse dragging.
     *
     * @param enabled If set to true the circle will be disabled.
     */
    public void setEnabled (boolean enabled)
    {
      super.setEnabled(enabled);

      if (enabled)
        addMouseMotionListener(this);
      else
        removeMouseMotionListener(this);

      getParent().repaint();
    }

    /**
     * Set the location of the circle.
     *
     * @param p The new location.
     */
    public void setLocation (Point p)
    {
      setLocation(p.x, p.y);
    }

    /**
     * Set the location of the circle.
     *
     * @param x The new X location.
     * @param y The new Y location.
     */
    public void setLocation (int x, int y)
    {
      super.setLocation(x - width/2, y - height/2);
    }

    /**
     * Set the top left constraint to a point, so that the bottom right may not
     * go further up, and to the left than this point.
     *
     * @param constraint The constraint point.
     */
    public void setTopLeftContstraint (Point constraint)
    {
      this.topLeftConstraint = constraint;
    }

    /**
     * Set the bottom right constraint to a point, so that the top left point may
     * not go any further below, and to the right than this point.
     *
     * @param constraint The constraint point.
     */
    public void setBottomRightConstraint (Point constraint)
    {
      this.bottomRightConstraint = constraint;
    }

    // MouseMotionListener Interface.

    /**
     * This method is called when ever the mouse is dragged while inside this component.
     *
     * @param e The mouse event corresponding to the mousing being dragged.
     */
    public void mouseDragged(MouseEvent e)
    {
      int newX = this.getX() + e.getX();
      int newY = this.getY() + e.getY();

      if (topLeftConstraint != null &&
          (newX < (topLeftConstraint.getX() + width/2) || newY < (topLeftConstraint.getY() + height/2)))
        return;

      if (bottomRightConstraint != null &&
          (newX > (bottomRightConstraint.getX() + width/2) || newY > (bottomRightConstraint.getY() + height/2)))
        return;

      getParent().repaint();

      xModel.setValue(newX - circleAdjustX);
      if (invertYAxis)
        yModel.setValue((-newY) + circleAdjustY);
      else
        yModel.setValue(newY - circleAdjustY);
    }

    /**
     * This method is called when ever the mouse is moved. (regardless of mouse button events).
     *
     * @param e The mouse event corresponding to the mouse being moved.
     */
    public void mouseMoved(MouseEvent e)
    {
      // zip
    }

    // ChangeListener Interface.

    /**
     * This method is called when ever the state of the bounded range models changes.
     *
     * @param e The change event corresponding to the models changing.
     */
    public void stateChanged(ChangeEvent e)
    {
      if (invertYAxis)
        setLocation (new Point(xModel.getValue()+circleAdjustX, (-yModel.getValue())+circleAdjustY));
      else
        setLocation (new Point(xModel.getValue()+circleAdjustX, yModel.getValue()+circleAdjustY));

      getParent().repaint();
    }
  }
}
