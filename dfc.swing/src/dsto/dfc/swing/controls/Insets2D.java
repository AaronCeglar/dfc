package dsto.dfc.swing.controls;

import java.io.Serializable;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import dsto.dfc.util.Copyable;
import dsto.dfc.util.IllegalFormatException;

import dsto.dfc.swing.text.Convertable;
import dsto.dfc.swing.text.ValueConverter;

/**
 * Represents insets in a floating point space.
 *
 * <p>This class follows the immutable pattern: do not modify if more than
 * one reference may exist.</p>
 *
 * @version $Revision$
 */
public final class Insets2D implements Convertable, Copyable, Serializable
{
  private static final long serialVersionUID = -4554124394444212948L;

  private static final String USAGE = "Insets format is: TOP, LEFT, BOTTOM, RIGHT";

  public static final ValueConverter CONVERTER = new Converter ();

  private float top;
  private float left;
  private float bottom;
  private float right;

  public Insets2D ()
  {
    top = 0;
    left = 0;
    bottom= 0;
    right = 0;
  }

  public Insets2D (float top, float left, float bottom, float right)
  {
    this.top = top;
    this.left = left;
    this.bottom = bottom;
    this.right = right;
  }

  public Insets2D (String insets) throws IllegalFormatException
  {
    StringTokenizer tokenizer = new StringTokenizer (insets, " ,\n\r\t");

    try
    {
      top = Float.parseFloat (tokenizer.nextToken ());
      left = Float.parseFloat (tokenizer.nextToken ());
      bottom = Float.parseFloat (tokenizer.nextToken ());
      right = Float.parseFloat (tokenizer.nextToken ());
    } catch (NoSuchElementException ex)
    {
      throw new IllegalFormatException (this, "Not enough numbers: " + USAGE,
                                        insets, Insets2D.class);
    } catch (NumberFormatException ex)
    {
      throw new IllegalFormatException
        (this, "Illegal number entered (" + ex.getMessage () + "): " + USAGE,
        insets, Insets2D.class);
    }
  }

  public float getTop ()
  {
    return top;
  }

  /**
   * Do not use after creation.
   */
  public void setTop (float newValue)
  {
    top = newValue;
  }

  public float getLeft ()
  {
    return left;
  }

  /**
   * Do not use after creation.
   */
  public void setLeft (float newValue)
  {
    left = newValue;
  }

  public float getBottom ()
  {
    return bottom;
  }

  /**
   * Do not use after creation.
   */
  public void setBottom (float newValue)
  {
    bottom = newValue;
  }

  public float getRight ()
  {
    return right;
  }

  /**
   * Do not use after creation.
   */
  public void setRight (float newValue)
  {
    right = newValue;
  }

  public ValueConverter getConverter ()
  {
    return CONVERTER;
  }

  public String toString ()
  {
    StringBuffer buffer = new StringBuffer (14);

    buffer.append (Float.toString (top));
    buffer.append (", ");
    buffer.append (Float.toString (left));
    buffer.append (", ");
    buffer.append (Float.toString (bottom));
    buffer.append (", ");
    buffer.append (Float.toString (right));

    return buffer.toString ();
  }

  public Object clone () throws CloneNotSupportedException
  {
    return super.clone ();
  }

  static class Converter implements ValueConverter
  {
    public Object convertTo (Object value, Class form)
      throws IllegalFormatException
    {
      if (form.equals (String.class) && value instanceof Insets2D)
      {
        return value.toString ();
      } else if (form.equals (Insets2D.class) &&
                 value instanceof String)
      {
        return new Insets2D ((String)value);
      } else
        throw new IllegalFormatException (this, value, form);
    }
  }
}
