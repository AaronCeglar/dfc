package dsto.dfc.swing.controls;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import dsto.dfc.logging.Log;

import dsto.dfc.swing.icons.ImageLoader;


/**
 * This class can be used to add print preview capabilities to an application.
 * Taken from Tony Johnson who took it from the book
 * http://manning.spindoczine.com/sbe/ by
 * Matthew Robinson and Pavel Vorobiev, Ph.D.
 *
 * @author Luke Marsh
 * @version $Revision$
 */
public class PrintPreview extends JPanel
{

  protected Component componentToBePrinted;
  protected Printable printable;
  protected JPanel m_preview;
  protected PageFormat pageFormat;


  /**
   * Create an empty PrintPreview
   * 
   * @param pageFormat Used to obtain the current PageLayout settings
   *          stored in the page format.
   */
  public PrintPreview (Component componentToBePrinted,
      PageFormat pageFormat)
  {
    setLayout (new BorderLayout ());
    setPreferredSize (new Dimension (600, 400));
    add (createToolBar (), BorderLayout.NORTH);
    this.componentToBePrinted = componentToBePrinted;
    this.pageFormat = pageFormat;
    JScrollPane ps = new JScrollPane ();
    m_preview = new JPanel (new FlowLayout ());
    ps.setViewportView (m_preview);
    add (ps, BorderLayout.CENTER);
  }


 /**
  * Set a target Printable using the default PageFormat.
  * Printables are only suitable for printing a small number of pages, for more
  * pages use a Pageable instead.
  * 
  * @param target The Printable to preview
  */
  public void setPrintable (Printable target)
  {
    printable = target;
    m_preview.removeAll ();
    m_preview.add (new PagePreview (10, 0));
  }


 /**
  * Create a standard dialog that wraps the PrintPreview
  * 
  * @param owner The owner of the dialog
  * @param title The title of the dialog
  */
  public JDialog createDialog (Component owner, String title)
  {
    Window w = (Window) SwingUtilities.getAncestorOfClass (Window.class, owner);
    JDialog dlg = w instanceof Frame ? new JDialog
        ((Frame) w, title, true) : new JDialog ((Dialog) w, title, true);
    dlg.setContentPane (this);
    dlg.setDefaultCloseOperation (WindowConstants.DISPOSE_ON_CLOSE);
    dlg.setSize (new Dimension (620, 915));
    return dlg;
  }


  /**
   * Called to create the toolbar. Override to customize the toolbar.
   */
  protected JToolBar createToolBar ()
  {
    JToolBar tb = new JToolBar ();
    tb.add (createPrintButton ());
    tb.addSeparator ();
    tb.add (createScaleChooser ());
    tb.addSeparator ();
    tb.add (createCloseButton ());
    tb.setFloatable (false); // Doesn't work in a dialog
    return tb;
  }


  /**
   * Creates the Close button
   */
  protected JComponent createCloseButton ()
  {
    return new JButton ("Close")
    {
      public void fireActionPerformed (ActionEvent e)
      {
		JDialog dlg = (JDialog) SwingUtilities.getAncestorOfClass (JDialog.class, this);
		dlg.dispose ();
      }
    };
  }


  /**
   * Creates the Print Button
   */
  protected JComponent createPrintButton ()
  {
    Icon icon = ImageLoader.getIcon ("/dsto/dfc/icons/file_print.gif");
    return new JButton ("Print", icon)
    {
      public void fireActionPerformed (ActionEvent e)
      {
	    PrintUtility.printComponent (componentToBePrinted, pageFormat); 
		JDialog dlg = (JDialog) SwingUtilities.getAncestorOfClass (JDialog.class, this);
		dlg.dispose ();
      }
    };
  }


  /**
   * Create the Scale chooser
   */
  protected JComponent createScaleChooser ()
  {
    String[] scales = { "10 %", "25 %", "50 %", "75 %", "100 %",
                        "150 %", "175 %", "200 %", "300 %" };
    JComboBox m_cbScale = new JComboBox (scales)
    {
      public void fireActionEvent ()
      {
        String str = getSelectedItem ().toString ();
        if (str.endsWith ("%")) str = str.substring (0, str.length () - 1);
        str = str.trim ();
        int scale = 0;
        try
        {
          scale = Integer.parseInt (str);
        }
        catch (NumberFormatException ex)
        {
          return;
        }

        Component[] comps = m_preview.getComponents();
        for (int k = 0; k < comps.length; k++)
        {
          if (! (comps[k] instanceof PagePreview))continue;
          PagePreview pp = (PagePreview) comps[k];
          pp.setScale (scale);
        }
        m_preview.revalidate ();
      }
    };
    m_cbScale.setMaximumSize (m_cbScale.getPreferredSize ());
    m_cbScale.setEditable (true);
    return m_cbScale;
  }


  /**
   * Taken from Tony Johnson who took it from the book
   * http://manning.spindoczine.com/sbe/ by
   * Matthew Robinson and Pavel Vorobiev, Ph.D.
   *
   * @author Luke Marsh
   * @version $Revision$
   */
  private class PagePreview extends JPanel
  {

    private int m_page;
    private double m_scale, m_h, m_w;


    /**
     * Initializes variables.
     * 
     * @param scale The scale of the page.
     * @param page The number of pages.
     */
    public PagePreview (double scale, int page)
    {
      m_page = page;
      scale = 100 / scale;
      m_scale = scale;
      m_w = pageFormat.getWidth () / scale;
      m_h = pageFormat.getHeight () / scale;
      setBorder (BorderFactory.createLineBorder (Color.black));
    }


    /**
     * Changes the scale.
     * 
     * @param scale The scale of the page.
     */
    public void setScale (double scale)
    {
      scale = 100 / scale;
      m_scale = scale;
      m_w = pageFormat.getWidth () / scale;
      m_h = pageFormat.getHeight () / scale;
      repaint ();
    }


    /**
     * Returns the preferred size of the dialog.
     * 
     * @return The preferred size.
     */
    public Dimension getPreferredSize ()
    {
      Insets ins = getInsets ();
      return new Dimension ((int) m_w+ins.left+ins.right, (int) m_h+ins.top+ins.bottom);
    }


    /**
     * Draws the preview of what is to be printed in the dialog.
     * 
     * @param g The Graphics object.
     */
    public void paint (Graphics g)
    {
      g.setColor (Color.white);
      g.fillRect (0, 0, getWidth (), getHeight ());
      paintBorder (g);
      try
      {
        Graphics2D g2 = (Graphics2D) g;
        g2.addRenderingHints (new RenderingHints (RenderingHints.KEY_TEXT_ANTIALIASING,
                                                RenderingHints.VALUE_TEXT_ANTIALIAS_ON));
        g2.scale (1 / m_scale, 1 / m_scale);
		printable.print (g, pageFormat, m_page);
      }
      catch (PrinterException ex)
      {
        Log.diagnostic ("Error during printview generation", this, ex);
      }
    }
  }
}