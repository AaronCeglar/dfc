package dsto.dfc.swing.controls;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;

/**
 * Provides utilities for placing windows relative to each other and
 * to the screen.
 *
 * @version $Revision$
 */
public final class WindowLayout
{
  public static final int DEFAULT_DISTANCE = 30;

  public static final int TOP_LEFT = 0;
  public static final int MID_TOP = 1;
  public static final int TOP_RIGHT = 2;
  public static final int MID_RIGHT = 3;
  public static final int BOTTOM_RIGHT = 4;
  public static final int MID_BOTTOM = 5;
  public static final int BOTTOM_LEFT = 6;
  public static final int MID_LEFT = 7;
  public static final int CENTER = 8;

  public static final int NORTH = 100;
  public static final int SOUTH = 101;
  public static final int EAST = 102;
  public static final int WEST = 103;
  public static final int NORTH_EAST = 104;
  public static final int SOUTH_EAST = 105;
  public static final int SOUTH_WEST = 106;
  public static final int NORTH_WEST = 107;

  private WindowLayout()
  {
    // zip
  }

  /**
   * Places a window at a specified alignmnent on the screen.
   *
   * @param window The window to place.
   * @param position The position of the window (TOP_LEFT, CENTER,
   * etc).
   */
  public static void placeWindow (Window window, int position)
  {
    Point location = new Point ();
    Dimension windowSize = window.getSize ();
    Dimension screenSize = Toolkit.getDefaultToolkit ().getScreenSize ();

    switch (position)
    {
    case TOP_LEFT:
    case BOTTOM_LEFT:
    case MID_LEFT:
      location.x = 0;
      break;
    case MID_TOP:
    case CENTER:
    case MID_BOTTOM:
      location.x = Math.max (0, (screenSize.width - windowSize.width) / 2);
      break;
    default:
      location.x = Math.max (0, screenSize.width - windowSize.width);
    }

    switch (position)
    {
    case TOP_LEFT:
    case MID_TOP:
    case TOP_RIGHT:
      location.y = 0;
      break;
    case MID_LEFT:
    case CENTER:
    case MID_RIGHT:
      location.y = Math.max (0, (screenSize.height - windowSize.height) / 2);
      break;
    default:
      location.y = Math.max (0, screenSize.height - windowSize.height);
    }

    placeWindow (window, location);
  }

  public static void placeWindow (Window parent, int fromPoint,
                                  Window child, int toPoint)
  {
    placeWindow (parent, fromPoint, child, toPoint, SOUTH_EAST,
                 DEFAULT_DISTANCE);
  }

  /**
   * Places a window at a default distance.  See placeWindow
   * below.
   */
  public static void placeWindow (Window parent, int fromPoint,
                                  Window child, int toPoint,
                                  int direction)
  {
    placeWindow (parent, fromPoint, child, toPoint, direction,
                 DEFAULT_DISTANCE);
  }

  /**
   * Places a child window relative to a parent (or the screen) using
   * an imaginary strut connecting 8 points on the parent and child
   * window's
   * borders.
   *
   * @param parent The parent window.  May be null in which case the
   * screen dimensions are used.
   * @param fromPoint The point on the parent to connect the strut.
   * One of TOP_RIGHT, BOTTOM_LEFT, etc.
   * @param child The child window.
   * @param toPoint The point on the child to connect the strut.  See
   * fromPoint for possible values.
   * @param direction The direction of the strut.
   * @param distance The length of the strut.
   */
  public static void placeWindow (Window parent, int fromPoint,
                                  Window child, int toPoint,
                                  int direction, int distance)
  {
    Dimension parentSize;
    Point parentLocation;

    if (parent == null)
    {
      parentSize = Toolkit.getDefaultToolkit ().getScreenSize ();
      parentLocation = new Point (0, 0);
    } else
    {
      parentSize = parent.getSize ();
      parentLocation = parent.getLocation ();
    }

    Dimension childSize = child.getSize ();
    Point childLocation = getOffsetForPoint (parentSize, fromPoint);
    childLocation.x += parentLocation.x;
    childLocation.y += parentLocation.y;

    switch (direction)
    {
    case NORTH:
      childLocation.y -= distance;
      break;
    case SOUTH:
      childLocation.y += distance;
      break;
    case EAST:
      childLocation.x += distance;
      break;
    case WEST:
      childLocation.x -= distance;
      break;
    case NORTH_EAST:
      childLocation.x += distance;
      childLocation.y -= distance;
      break;
    case SOUTH_EAST:
      childLocation.x += distance;
      childLocation.y += distance;
      break;
    case NORTH_WEST:
      childLocation.x -= distance;
      childLocation.y -= distance;
      break;
    case SOUTH_WEST:
      childLocation.x -= distance;
      childLocation.y += distance;
      break;
    }

    Point toDelta = getOffsetForPoint (childSize, toPoint);
    childLocation.x -= toDelta.x;
    childLocation.y -= toDelta.y;

    placeWindow (child, childLocation);
  }

  /**
   * Align a child window relative to its parent.
   *
   * @param parent The parent window.
   * @param child The child window.
   * @param position The child's alignment relative to parent.
   */
  public static void placeWindow (Window parent, Window child, int position)
  {
    Point location = new Point ();
    Dimension childSize = child.getSize ();
    Dimension parentSize = parent.getSize ();
    Point parentLoc = parent.getLocation ();

    switch (position)
    {
    case TOP_LEFT:
    case BOTTOM_LEFT:
    case MID_LEFT:
      location.x = parentLoc.x;
      break;
    case MID_TOP:
    case CENTER:
    case MID_BOTTOM:
      location.x =
        Math.max (parentLoc.x,
                  parentLoc.x + (parentSize.width - childSize.width) / 2);
      break;
    default:
      location.x =
        Math.max (parentLoc.x,
                  parentLoc.x + parentSize.width - childSize.width);
    }

    switch (position)
    {
    case TOP_LEFT:
    case MID_TOP:
    case TOP_RIGHT:
      location.y = parentLoc.y;
      break;
    case MID_LEFT:
    case CENTER:
    case MID_RIGHT:
      location.y =
        Math.max (parentLoc.y,
                  parentLoc.y + (parentSize.height - childSize.height) / 2);
      break;
    default:
      location.y =
        Math.max (parentLoc.y,
                  parentLoc.y + parentSize.height - childSize.height);
    }

    placeWindow (child, location);
  }

  /**
   * Place a window, adjusting so it does not go outside the screen.
   */
  public static void placeWindow (Window window, Point location)
  {
    Rectangle windowRect = new Rectangle (location, window.getSize ());
    Rectangle screenRect =
      new Rectangle (new Point (0, 0),
                     Toolkit.getDefaultToolkit ().getScreenSize ());

    // fudge factor: no windows within 20 px of bottom of screen
    screenRect.height -= 20;

    Geometry.bumpRectangle (screenRect, windowRect);

    window.setLocation (windowRect.x, windowRect.y);
  }

  protected static Point getOffsetForPoint (Dimension shape, int connector)
  {
    Point point = new Point ();

    switch (connector)
    {
    case TOP_LEFT:
      point.x = 0; point.y = 0;
      break;
    case MID_LEFT:
      point.x = 0; point.y = shape.height / 2;
      break;
    case BOTTOM_LEFT:
      point.x = 0; point.y = shape.height - 1;
      break;
    case MID_BOTTOM:
      point.x = shape.width / 2; point.y = shape.height - 1;
      break;
    case BOTTOM_RIGHT:
      point.x = shape.width - 1; point.y = shape.height - 1;
      break;
    case MID_RIGHT:
      point.x = shape.width - 1; point.y = shape.height / 2;
      break;
    case TOP_RIGHT:
      point.x = shape.width - 1; point.y = 0;
      break;
    case MID_TOP:
      point.x = shape.width / 2; point.y = 0;
    }

    return point;
  }

  /*
   * @deprecated Use (@link dsto.dfc.util.Geometry} instead.
   */
  public static void bumpRectangle (Rectangle parentRect, Rectangle childRect)
  {
    Geometry.bumpRectangle (parentRect, childRect);
  }
}
