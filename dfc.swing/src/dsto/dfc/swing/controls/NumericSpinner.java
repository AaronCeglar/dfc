package dsto.dfc.swing.controls;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.Serializable;

import javax.swing.BoundedRangeModel;
import javax.swing.ButtonModel;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import dsto.dfc.swing.icons.ArrowIcon;

/**
 * A spinner component for numeric values stored in a
 * BoundedRangeModel object.  Provides a text field for numeric entry
 * and increment/decrement buttons.
 *
 * @see javax.swing.BoundedRangeModel
 * @author Matthew Phillips
 * @version $Revision$
 */
public class NumericSpinner extends JComponent
{
  protected static final int MIN_ARROW_HEIGHT = 3;
  protected static final int MIN_BUTTON_WIDTH = 17;

  protected static final Insets buttonMargin = new Insets (1, 1, 1, 1);

  protected BoundedRangeModel model;
  protected JTextField textField;
  protected ArrowButton upButton, downButton;
  protected int increment = 1;
  protected transient boolean changing = false;
  protected BoundedRangeModelChangeListener boundedRangeModelChangeListener;

  /**
   * Creates a default spinner with min = 0, max = 100, value = 50.
   */
  public NumericSpinner ()
  {
    super ();
    this.model = new DefaultBoundedRangeModel ();
    model.setMinimum (0);
    model.setMaximum (100);
    model.setValue (50);

    init ();
  }

  /**
   * Creates a spinner using <code>model</code> as the model.
   *
   * @see #getModel
   * @see #setModel
   */
  public NumericSpinner (BoundedRangeModel model)
  {
    super ();
    this.model = model;
    init ();
  }

  /**
   * Creates a numeric spinner with a default model intialised to
   * minimum, maximum and value.
   *
   * @see #getModel
   * @see #setModel
   */
  public NumericSpinner (int minimum, int maximum, int value)
  {
    super ();
    this.model = new DefaultBoundedRangeModel ();
    model.setMinimum (minimum);
    model.setMaximum (maximum);
    model.setValue (value);

    init ();
  }

  /**
   * Add an ActionListener, by passing it onto the TextField.
   */
  public void addActionListener (ActionListener l)
  {
    textField.addActionListener (l);
  }
  /**
   * Remove the ActionListener, by telling the TextField to remove it.
   */
  public void removeActionListener (ActionListener l)
  {
    textField.removeActionListener(l);
  }

  /**
   * Set the textfield and the arrow buttons to be enabled/disabled
   *
   * @param enabled Enable the textfield and arrow buttons.
   */
  public void setEnabled (boolean enabled)
  {
    textField.setEnabled(enabled);
    upButton.setEnabled(enabled);
    downButton.setEnabled(enabled);
  }

  /**
   * Get the current value.
   *
   * @return The current value.
   */
  public int getValue () { return model.getValue (); }

  /**
   * Set the current value.
   *
   * @param value The new value.
   */
  public void setValue (int value) { model.setValue (value); }

  /**
   * Get the current minimum value.
   *
   * @return The current minimum value.
   */
  public int getMinimum () { return model.getMinimum (); }

  /**
   * Set the current minimum value.
   *
   * @param minimum The new minimum value.
   */
  public void setMinimum (int minimum) { model.setMinimum (minimum); }

  /**
   * Get the current maximum value.
   *
   * @return The current maximum value.
   */
  public int getMaximum () { return model.getMaximum (); }

  /**
   * Set the current model value.
   *
   * @param maximum The new maximum value.
   */
  public void setMaximum (int maximum) { model.setMaximum (maximum); }

  /**
   * Returns the amount by which the value is increased/decreased by
   * clicking on the arrow buttons.
   *
   * @return The current increment value.
   * @see #setIncrement
   */
  public int getIncrement () { return increment; }

  /**
   * Sets the amount by which the value is increased/decreased by
   * clicking on the arrow buttons.
   *
   * @param increment The new increment value.
   * @see #getIncrement
   */
  public void setIncrement (int increment) { this.increment = increment; }

  /**
   * Returns the model currently being used by the spinner.
   *
   * @return The current model.
   * @see #setModel
   */
  public BoundedRangeModel getModel () { return model; }

  /**
   * Sets the model to be used by the spinner.
   *
   * @param model The new model.
   * @see #getModel
   */
  public synchronized void setModel (BoundedRangeModel model)
  {
    model.removeChangeListener (boundedRangeModelChangeListener);
    this.model = model;
    model.addChangeListener (boundedRangeModelChangeListener);
    loadTextFieldFromModel ();
  }

  public void setToolTipText (String text) { textField.setToolTipText (text); }

  //////////////////// NumericSpinner implementation
  /**
   * Performs common initialisation of the spinner.
   */
  protected void init ()
  {
    setLayout (new LayoutManager ());
    int nColumns = Integer.toString (model.getMaximum ()).length ();

    textField = new JTextField (nColumns);
    loadTextFieldFromModel ();

    upButton = new ArrowButton ();
    downButton = new ArrowButton ();

    add (textField);
    add (upButton);
    add (downButton);

    ButtonChangeListener buttonChangeListener = new ButtonChangeListener ();
    upButton.getModel ().addChangeListener (buttonChangeListener);
    downButton.getModel ().addChangeListener (buttonChangeListener);

    boundedRangeModelChangeListener = new BoundedRangeModelChangeListener ();
    model.addChangeListener (boundedRangeModelChangeListener);

    // add textField listeners
    TextFieldListener textListener = new TextFieldListener ();
    textField.addActionListener (textListener);
    textField.addFocusListener (textListener);
  }

  /**
   * Perform an increment operation.
   *
   * @param newIncrement The amount to increase/decrease the current
   * value by.
   */
  protected synchronized void doIncrement (int newIncrement)
  {
    try
    {
      model.setValue (model.getValue () + newIncrement);
    } catch (IllegalArgumentException exception)
    {
      // OK, we just ignore doIncrement ()'s that are invalid
    }
  }

  /**
   * Loads the text field value from the model.  Sets
   * <code>changing</code> to prevent circular change event
   * propagation.
   *
   * @see #loadModelFromTextField
   */
  protected synchronized void loadTextFieldFromModel ()
  {
    if (!changing)
    {
      changing = true;
      textField.setText (Integer.toString (model.getValue ()));
      changing = false;
    }
  }

  /**
   * Loads the model from the text field.  Sets <code>changing</code>
   * to prevent circular change event propagation.
   *
   * @see #loadTextFieldFromModel
   */
  protected synchronized void loadModelFromTextField ()
  {
    if (!changing)
    {
      changing = true;

      try
      {
        int newValue = Integer.parseInt (textField.getText ());
        model.setValue (newValue);
      } catch (NumberFormatException exception)
      {
        // ignore invalid values
      }

      changing = false;
      loadTextFieldFromModel ();
    }
  }

  //////////////////// ArrowButton class
  /**
   * A small extension of JButton to implement arrow buttons.
   */
  protected class ArrowButton extends JButton
  {
    public ArrowButton ()
    {
      super ();
      init ();
    }

    public ArrowButton (Icon icon)
    {
      super (icon);
      init ();
    }

    protected void init ()
    {
      setFocusPainted (false);
      setMargin (buttonMargin);
    }

    public boolean isFocusTraversable ()
    {
      return false;
    }
  }

  //////////////////// Listener for text field
  protected class TextFieldListener
    implements ActionListener, FocusListener, Serializable
  {
    public void actionPerformed (ActionEvent event)
    {
      loadModelFromTextField ();
    }

    public void focusGained (FocusEvent event)
    {
      // don't care
    }

    public void focusLost (FocusEvent event)
    {
      loadModelFromTextField ();
    }
  }

  //////////////////// ChangeListener for BoundedRangeModel
  protected class BoundedRangeModelChangeListener
    implements ChangeListener, Serializable
  {
    public void stateChanged (ChangeEvent event)
    {
      loadTextFieldFromModel ();
    }
  }

  //////////////////// ChangeListener for button models
  /**
   * Listens for changes to the arrow button models and timer ticks
   * that signal auto repeats when buttons are held down.
   */
  protected class ButtonChangeListener
    implements ChangeListener, ActionListener, Serializable
  {
    public ButtonChangeListener ()
    {
      autoRepeatTimer = new Timer (100, this);
      autoRepeatTimer.setInitialDelay (300);
    }

    public void stateChanged (ChangeEvent event)
    {
      ButtonModel buttonModel = (ButtonModel)event.getSource ();

      if (buttonModel.isPressed ())
      {
        if (upButton.getModel () == buttonModel)
          currentIncrement = increment;
        else
          currentIncrement = -increment;

        doIncrement (currentIncrement);
        autoRepeatTimer.restart ();
      } else
      {
        autoRepeatTimer.stop ();
      }
    }

    // called by autoRepeatTimer
    public void actionPerformed (ActionEvent event)
    {
      doIncrement (currentIncrement);
    }

    protected Timer autoRepeatTimer;
    protected int currentIncrement;
  }

  //////////////////// LayoutManager
  /**
   * Custom layout manager for NumericSpinner.  Auto-sizes buttons and
   * text field for best effect based on desired height of text field.
   */
  protected class LayoutManager
    implements java.awt.LayoutManager, Serializable
  {
    protected Dimension smallestButtonSize;
    protected int currentArrowHeight;

    public LayoutManager ()
    {
      ArrowButton smallestButton =
        new ArrowButton (new ArrowIcon (ArrowIcon.UP, MIN_ARROW_HEIGHT));
      smallestButtonSize = smallestButton.getMinimumSize ();

      // smallest button width is MIN_BUTTON_WIDTH
      smallestButtonSize.width =
        Math.max (smallestButtonSize.width, MIN_BUTTON_WIDTH);
    }

    public void addLayoutComponent (String name, Component component)
    {
      // not needed
    }

    public void removeLayoutComponent (Component component)
    {
      // not needed
    }

    public Dimension minimumLayoutSize (Container parent)
    {
      Dimension size = textField.getPreferredSize ();

      int minimumButtonHeight = smallestButtonSize.height * 2;

      if (size.height <= minimumButtonHeight)
      {
                                // if textField is smaller than min button height...
        size.height = minimumButtonHeight;
        size.width += smallestButtonSize.width;
        currentArrowHeight = MIN_ARROW_HEIGHT;
      } else
      {
                                // need to increase button size
        ArrowIcon testIcon = new ArrowIcon (ArrowIcon.UP, MIN_ARROW_HEIGHT);
        ArrowButton testButton = new ArrowButton (testIcon);
        Dimension currentButtonSize;

        currentArrowHeight = MIN_ARROW_HEIGHT;

                                // find min arrow height that results in a buttons that are high enough
        do
        {
          currentArrowHeight++;
          testIcon.setIconHeight (currentArrowHeight);
          testButton.setIcon (testIcon); // in case testButton doesn't notice change!
          currentButtonSize = testButton.getMinimumSize ();
        } while (currentButtonSize.height * 2 < size.height);

        size.height = currentButtonSize.height * 2;
        size.width += Math.max (currentButtonSize.width, MIN_BUTTON_WIDTH);
      }

      return size;
    }

    public Dimension preferredLayoutSize (Container parent)
    {
      return minimumLayoutSize (parent);
    }

    public void layoutContainer (Container parent)
    {
      // size components
      upButton.setIcon (new ArrowIcon (ArrowIcon.UP, currentArrowHeight));
      downButton.setIcon (new ArrowIcon (ArrowIcon.DOWN, currentArrowHeight));
      Dimension buttonSize = upButton.getMinimumSize ();
      buttonSize.width = Math.max (buttonSize.width, MIN_BUTTON_WIDTH);
      upButton.setSize (buttonSize);
      downButton.setSize (buttonSize);

      Dimension textSize = new Dimension (getSize ().width - buttonSize.width,
                                          buttonSize.height * 2);
      textField.setSize (textSize);

      // locate components
      Insets insets = getInsets ();
      textField.setLocation (insets.left, insets.top);
      int buttonX = textField.getSize ().width + insets.left;
      upButton.setLocation (buttonX, insets.top);
      downButton.setLocation (buttonX, upButton.getSize ().height + insets.top);
    }
  }
}
