package dsto.dfc.swing.controls;

import java.awt.Component;
import java.util.EventObject;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.tree.TreeCellEditor;

/**
 * Abstract base class for cell editors based on a JPanel which
 * provides automatic implementation of TableCellEditor and
 * TreeCellEditor.  Superclasses implement getEditorComponent () and
 * getCellEditorValue ().
 *
 * @version $Revision$
 */
public abstract class AbstractCellEditor
  extends JPanel implements TableCellEditor, TreeCellEditor
{
  private transient Vector cellEditorListeners;
  
  public abstract JComponent getEditorComponent (JComponent client,
                                                 Object value,
                                                 int row, int column,
                                                 boolean isSelected,
                                                 boolean expanded,
                                                 boolean leaf);

  public abstract Object getCellEditorValue ();

  // TableCellEditor and TreeCellEditor interfaces

  public Component getTableCellEditorComponent (JTable table, Object value,
                                                boolean isSelected,
                                                int row, int column)
  {
    return getEditorComponent (table, value, row, column,
                               isSelected, false, false);
  }

  public Component getTreeCellEditorComponent (JTree tree, Object value,
                                               boolean isSelected,
                                               boolean expanded,
                                               boolean leaf, int row)
  {
    return getEditorComponent (tree, value, row, 0,
                               isSelected, expanded, leaf);
  }

  public boolean isCellEditable (EventObject anEvent)
  {
    return true;
  }

  public boolean shouldSelectCell (EventObject anEvent)
  {
    return true;
  }

  public boolean stopCellEditing ()
  {
    fireEditingStopped ();
    
    return true;
  }

  public void cancelCellEditing ()
  {
    fireEditingCanceled ();
  }

  public synchronized void removeCellEditorListener (CellEditorListener l)
  {
    if(cellEditorListeners != null && cellEditorListeners.contains(l))
    {
      Vector v = (Vector) cellEditorListeners.clone();
      v.removeElement(l);
      cellEditorListeners = v;
    }
  }

  public synchronized void addCellEditorListener (CellEditorListener l)
  {
    Vector v = cellEditorListeners == null ? new Vector(2) : (Vector) cellEditorListeners.clone();
    if(!v.contains(l))
    {
      v.addElement(l);
      cellEditorListeners = v;
    }
  }

  protected void fireEditingCanceled ()
  {
    if (cellEditorListeners != null)
    {
      ChangeEvent e = new ChangeEvent (this);
      Vector listeners = cellEditorListeners;
      
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((CellEditorListener) listeners.elementAt(i)).editingCanceled(e);
      }
    }
  }

  protected void fireEditingStopped ()
  {
    if (cellEditorListeners != null)
    {
      ChangeEvent e = new ChangeEvent (this);
      Vector listeners = cellEditorListeners;

      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((CellEditorListener) listeners.elementAt(i)).editingStopped(e);
      }
    }
  }
} 