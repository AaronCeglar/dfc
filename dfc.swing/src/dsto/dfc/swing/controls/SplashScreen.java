package dsto.dfc.swing.controls;

import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import dsto.dfc.swing.icons.ImageLoader;

/**
 * A window for displaying a splash screen during application loading.
 *
 * @version $Revision$
 */
public class SplashScreen extends Window implements ActionListener
{
  private Image image;
  private String versionLine = "";
  private Insets insets;
  private Timer timer;
  private boolean timerExpired = false;
  private boolean popdownCalled = false;

  public SplashScreen (Frame parent)
  {
    this (parent, null);
  }

  public SplashScreen (Frame parent, Image image)
  {
    this (parent, image, 3);
  }

  /**
   * Create a splash screen displaying an image.
   *
   * @param parent The parent of the splash screen window.  May be null.
   * @param image The image to display.  May be null for no image.
   * @param secsVisible The minimum number of seconds to stay visible.
   * If popdown () is called before this amount of time has passed,
   * the popdown is delayed until the time is completed.
   */
  public SplashScreen (Frame parent, Image image, int secsVisible)
  {
    super (parent == null ? ImageLoader.getFrame () : parent);

    this.image = image;
    this.timer = new Timer (secsVisible * 1000, this);

    setSize (getPreferredSize ());

    setInsets (new Insets (5, 3, 5, 3));

    WindowLayout.placeWindow (this, WindowLayout.CENTER);
  }

  public void setInsets (Insets newValue)
  {
    this.insets = newValue;
  }

  public Insets getInsets ()
  {
    return insets;
  }

  /**
   * Set the text displayed at the bottom right corner. The text is displayed
   * in the current font and foreground colour, inset by the current right and
   * bottom insets (see setInsets ()).
   */
  public void setVersionLine (String versionLine)
  {
    this.versionLine = versionLine;
  }

  public void paint (Graphics g)
  {
    Dimension splashSize = getSize ();
    FontMetrics fm = g.getFontMetrics ();
    int stringLength = fm.stringWidth (versionLine);

    int posX = splashSize.width - stringLength - insets.right;
    int posY = splashSize.height - insets.bottom;

    if (image != null)
      g.drawImage (image, 0, 0, this);

    g.setColor (getForeground ());
    g.drawString (versionLine, posX, posY);
  }

  public Dimension getPreferredSize ()
  {
    if (image != null)
      return new Dimension (image.getWidth (this), image.getHeight (this));
    else
      return new Dimension (400, 200);
  }

  public synchronized void popup ()
  {
     setVisible (true);
     toFront ();
     timer.start ();
  }

  public synchronized void popdown ()
  {
    if (timerExpired)
    {
      setVisible (false);
      dispose ();
    } else
      popdownCalled = true;
  }

  // ActionListener interface: called by timer

  public synchronized void actionPerformed (ActionEvent event)
  {
    timerExpired = true;
    timer.stop();

    if (popdownCalled)
      popdown ();
  }
}
