package dsto.dfc.swing.controls;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;

public class ButtonPanelBeanInfo extends SimpleBeanInfo
{
  Class beanClass = ButtonPanel.class;
  String iconColor16x16Filename;
  String iconColor32x32Filename;
  String iconMono16x16Filename;
  String iconMono32x32Filename;

  public PropertyDescriptor[] getPropertyDescriptors()
  {
    try 
    {
      PropertyDescriptor _HGap = new PropertyDescriptor("HGap", beanClass, "getHGap", "setHGap");
      _HGap.setDisplayName("");
      _HGap.setShortDescription("Horizontal gap between buttons");
      
      PropertyDescriptor _horizAlignment = new PropertyDescriptor("horizAlignment", beanClass, "getHorizAlignment", "setHorizAlignment");
      _horizAlignment.setShortDescription("Horizontal alignment of buttons");
      _horizAlignment.setPropertyEditorClass(ButtonLayoutHorizAlignmentPropertyEditor.class);

      PropertyDescriptor _vertAlignment = new PropertyDescriptor("vertAlignment", beanClass, "getVertAlignment", "setVertAlignment");
      _vertAlignment.setShortDescription("Vertical alignment of buttons");
      _vertAlignment.setPropertyEditorClass(ButtonLayoutVertAlignmentPropertyEditor.class);
      
      PropertyDescriptor[] pds = new PropertyDescriptor[] {
        _HGap,
        _horizAlignment,
        _vertAlignment,
      };
      return pds;
    }
    catch (IntrospectionException ex)
    {
      ex.printStackTrace();
      return null;
    }
  }

  public java.awt.Image getIcon(int iconKind)
  {
    switch (iconKind) {
    case BeanInfo.ICON_COLOR_16x16:
      return iconColor16x16Filename != null ? loadImage(iconColor16x16Filename) : null;
    case BeanInfo.ICON_COLOR_32x32:
      return iconColor32x32Filename != null ? loadImage(iconColor32x32Filename) : null;
    case BeanInfo.ICON_MONO_16x16:
      return iconMono16x16Filename != null ? loadImage(iconMono16x16Filename) : null;
    case BeanInfo.ICON_MONO_32x32:
      return iconMono32x32Filename != null ? loadImage(iconMono32x32Filename) : null;
    }
    return null;
  }

  public BeanInfo[] getAdditionalBeanInfo()
  {
    Class superclass = beanClass.getSuperclass();
    try 
    {
      BeanInfo superBeanInfo = Introspector.getBeanInfo(superclass);
      return new BeanInfo[] { superBeanInfo };
    }
    catch (IntrospectionException ex)
    {
      ex.printStackTrace();
      return null;
    }
  }
}


