package dsto.dfc.swing.controls;

import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.border.Border;

/**
 * A font selector control suitable for use inside forms and tables
 * etc.  Displays the font name, size and style (using a
 * FontCellRenderer) plus a ... button that displays a font selector
 * dialog.
 *
 * @version $Revision$
 */
public class FontPopupSelector extends AbstractPopupEditor
{
  /** The border around the font cell renderer */
  private static final Border BORDER =
    BorderFactory.createCompoundBorder (BorderFactory.createEtchedBorder (), BorderFactory.createEmptyBorder (0, 5, 0, 5));
  
  private Font font;

  public FontPopupSelector ()
  {
    super ();

    FontCellRenderer renderer = new FontCellRenderer ();
    setRendererBorder (BORDER);
    
    setRenderer (renderer);
    setSelectedFont (getFont ());
  }

  protected void setValue (Object value)
  {
    super.setValue (value);
    
    Font oldFont = font;

    font = (Font)value;

    firePropertyChange ("selectedFont", oldFont, font);
  }

  /**
   * Set the font inside the selector.
   */
  public void setSelectedFont (Font newFont)
  {
    setValue (newFont);
  }

  public Font getSelectedFont ()
  {
    return font;
  }

  // AbstractPopupEditor implementation

  public Object getCellEditorValue ()
  {
    return getSelectedFont ();
  }

  public boolean showPopup ()
  {
    FontSelectorDialog dialog =
      new FontSelectorDialog (this, "Select Font", true);

    dialog.setSelectedFont (font);
    dialog.setVisible (true);

    if (dialog.isCancelled ())
    {
      dialog.dispose ();

      return false;
    } else
    {
      setSelectedFont (dialog.getSelectedFont ());
      dialog.dispose ();

      return true;
    }
  }
}
