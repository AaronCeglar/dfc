package dsto.dfc.swing.controls;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.border.Border;

/**
 * Cell renderer for AWT Font values.
 *
 * @see dsto.dfc.swing.forms.FontLabelEditor
 *
 * @version $Revision$
 */
public class FontCellRenderer extends AbstractCellRenderer
{
  private Border border;

  /**
   * Set the border around the renderer component.
   */
  public void setBorder (Border border)
  {
    this.border = border;
  }

  public JComponent getRendererComponent (JComponent client,
                                          Object value,
                                          int row, int column,
                                          boolean isSelected,
                                          boolean hasFocus,
                                          boolean expanded,
                                          boolean leaf)
  {
    JLabel label = getLabel ();

    if (value instanceof Font)
    {
      Font font = (Font)value;

      String style;

      if (font.getStyle () == Font.BOLD)
        style = "Bold";
      else if (font.getStyle () == Font.ITALIC)
        style = "Italic";
      else if (font.getStyle () == (Font.BOLD | Font.ITALIC))
        style = "Bold Italic";
      else
        style = "Regular";

      StringBuffer fontText = new StringBuffer (40);

      fontText.append (font.getFamily ());
      fontText.append (" ").append (font.getSize ()).append ("pt ");
      fontText.append (style);

      label.setText (fontText.toString ());
      label.setForeground (Color.black);
      if (border != null)
        label.setBorder (border);
    }

    return label;
  }
}
