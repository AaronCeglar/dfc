package dsto.dfc.swing.controls;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import javax.swing.UIManager;

import dsto.dfc.util.Disposable;
import dsto.dfc.util.EnumerationValue;
import dsto.dfc.util.IllegalFormatException;

import dsto.dfc.swing.forms.AbstractFormEditorComponent;
import dsto.dfc.swing.icons.FlowIcon;
import dsto.dfc.swing.icons.Iconic;
import dsto.dfc.swing.icons.IconicRegistry;

/**
 * A control that presents a multiple choice selection as a set of
 * radio buttons.  Supports auto-setup of choices when possible values
 * are defined by an {@link dsto.dfc.util.EnumerationValue} and is also a
 * {@link dsto.dfc.swing.forms.FormEditor}.
 *
 * @author Matthew
 * @version $Revision$
 */
public class RadioBox
  extends AbstractFormEditorComponent implements Disposable, ActionListener
{
  /** Lay out radio buttons horizontally. */
  public static final int LAYOUT_HORIZONTAL = 0;
  /** Lay out radio buttons vertically. */
  public static final int LAYOUT_VERTICAL = 1;

  /** The currently selected value.  Also the form editor value. */
  private Object selectedValue;
  /** The possible values that may be selected. */
  private Object [] possibleValues = null;
  /** The buttons created: buttons [i] for possibleValues [i]. See
      @{link #createRadioButtons}. */
  private JRadioButton [] buttons = null;
  /** The group used to ensure only one button is selected. */
  private ButtonGroup buttonGroup = null;
  /** The button layout: either LAYOUT_VERTICAL or LAYOUT_HORIZONTAL. */
  private int orientation = -1;

  /**
   * Create radio box with no selectable options.  Either use {@link
   * #setPossibleValues} or use editor values implementing the {@link
   * EnumerationValue} interface to set the possible values.
   */
  public RadioBox ()
  {
    this (null);
  }

  public RadioBox (Object [] possibleValues)
  {
    this (possibleValues, LAYOUT_VERTICAL);
  }

  /**
   * Create a new radio box.
   *
   * @param possibleValues The selectable values displayed as radio
   * buttons in the box.  May be null.
   * @param orientation The orientation of the buttons (LAYOUT_VERTICAL or
   * LAYOUT_HORIZONTAL).
   */
  public RadioBox (Object [] possibleValues, int orientation)
  {
    this.possibleValues = possibleValues;

    setButtonOrientation (orientation);

    if (possibleValues != null)
      createRadioButtons ();
  }

  /**
   * Set the possible values that can be selected from.  Each value
   * will be displayed as a radio button.
   *
   * @param newValues The new set of possible values.
   */
  public void setPossibleValues (Object [] newValues)
  {
    Object [] oldValues = possibleValues;

    this.possibleValues = newValues;

    firePropertyChange ("possibleValues", oldValues, newValues);

    createRadioButtons ();
  }

  public Object [] getPossibleValues ()
  {
    return possibleValues;
  }

  /**
   * Convenience method to present possibleValues property as a String
   * array for visual bean builders.
   */
  public void setPossibleStringValues (String [] newValues)
  {
    setPossibleValues (newValues);
  }

  public String [] getPossibleStringValues ()
  {
    if (possibleValues instanceof String [])
      return (String [])possibleValues;
    else
      return null;
  }

  public String getSelectedStringValue ()
  {
    if (selectedValue instanceof String)
      return (String)selectedValue;
    else
      return null;
  }

  public void setSelectedStringValue (String value)
    throws IllegalFormatException
  {
    setEditorValue (value);
  }

  /**
   * Set the orientation of the radio buttons.
   *
   * @param newValue Either LAYOUT_HORIZONTAL or LAYOUT_VERTICAL.
   */
  public void setButtonOrientation (int newValue)
  {
    if (newValue != orientation)
    {
      orientation = newValue;

      if (orientation == LAYOUT_VERTICAL)
        setLayout (new BoxLayout (this, BoxLayout.Y_AXIS));
        //setLayout (new VerticalFlowLayout (VerticalFlowLayout.LEFT, 0, 0, true, false));
      else
        setLayout (new FlowLayout (FlowLayout.LEFT, 3, 0));
    }
  }

  public int getButtonOrientation ()
  {
    return orientation;
  }

  public void dispose ()
  {
    destroyRadioButtons ();
  }

  public void setEnabled (boolean enabled)
  {
    super.setEnabled (enabled);

    for (int i=0; i < buttons.length; i++)
      buttons [i].setEnabled (enabled);
  }

  public void requestFocus ()
  {
    JRadioButton selectedButton = getSelectedButton ();

    if (selectedButton != null)
      selectedButton.requestFocus ();
    else
      super.requestFocus ();
  }

  public JRadioButton getSelectedButton ()
  {
    if (buttons != null)
    {
      for (int i = 0; i < buttons.length; i++)
      {
        if (buttons [i].isSelected ())
          return buttons [i];
      }
    }

    return null;
  }

  protected void createRadioButtons ()
  {
    if (buttons != null)
      destroyRadioButtons ();

    buttons = new JRadioButton [possibleValues.length];
    buttonGroup = new ButtonGroup ();

    for (int i = 0; i < possibleValues.length; i++)
    {
      Object value = possibleValues [i];
      JRadioButton button;

      // create radio button, using Iconic interface if possible
      Iconic iconicValue = IconicRegistry.getInstance ().getIconicValue (value);

      if (iconicValue != null)
      {
        button = new JRadioButton (iconicValue.getName ());

        FlowIcon icon =
          new FlowIcon (UIManager.getIcon ("RadioButton.icon"), iconicValue.getIcon ());

        button.setIcon (icon);
        button.setSelectedIcon (icon);
      } else
      {
        button = new JRadioButton (value.toString ());
      }

      // configure button and add as child control
      button.setEnabled (isEnabled ());
      button.putClientProperty ("RadioBox.Value", value);
      button.addActionListener (this);

      buttons [i] = button;
      buttonGroup.add (button);

      add (button);
    }
  }

  protected void destroyRadioButtons ()
  {
    if (buttons != null)
    {
      for (int i = 0; i < buttons.length; i++)
      {
        JRadioButton button = buttons [i];

        remove (button);
        button.removeActionListener (this);
        buttonGroup.remove (button);
      }

      buttonGroup = null;
      buttons = null;
    }
  }

  /**
   * Find the index of a value in the array of possible values.
   */
  protected int findIndexInPossibleValues (Object value)
  {
    for (int i = 0; i < possibleValues.length; i++)
    {
      Object v = possibleValues [i];

      if (v.equals (value))
        return i;
    }

    return -1;
  }

  // FormEditor interface

  public boolean isDirectEdit ()
  {
    return false;
  }

  /**
   * Set the editor value (ie the current selected value).
   *
   * @param newValue The new value.  If newValue implements the {@link
   * EnumerationValue} interface, then the possible values for this control
   * are automatically set to its enumValues property.
   */
  public void setEditorValue (Object newValue) throws IllegalFormatException
  {
    // auto-set possibleValues from EnumerationValue's
    if (newValue instanceof EnumerationValue)
    {
      EnumerationValue enumeration = (EnumerationValue)newValue;

      if (enumeration.getEnumValues () != possibleValues)
        setPossibleValues (enumeration.getEnumValues ());
    }

    if (possibleValues == null)
      throw new Error ("Cannot set editor value until possibleValues are set");

    Object oldValue = selectedValue;

    this.selectedValue = newValue;

    firePropertyChange ("editorValue", oldValue, newValue);

    // find index of new value
    int valueIndex = findIndexInPossibleValues (newValue);

    // set selected radio button
    if (valueIndex != -1)
      buttons [valueIndex].setSelected (true);
  }

  public Object getEditorValue ()
  {
    return selectedValue;
  }

  public void commitEdits ()
  {
    fireEditCommitted ();
  }

  // ActionListener interface

  public void actionPerformed (ActionEvent e)
  {
    JRadioButton button = (JRadioButton)e.getSource ();
    Object value = button.getClientProperty ("RadioBox.Value");

    // update editor value and request commit
    selectedValue = value;

    fireEditCommitted ();
  }
}
