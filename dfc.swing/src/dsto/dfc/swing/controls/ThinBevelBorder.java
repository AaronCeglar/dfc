package dsto.dfc.swing.controls;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;

import javax.swing.border.BevelBorder;

/**
 * This extends BevelBorder and overrides the paint methods so that
 * we can get thin borders instead of the usual thick ones.
 */

public class ThinBevelBorder extends BevelBorder
{

  public ThinBevelBorder(int bevelType)
  {
    super(bevelType);
  }

  protected void paintRaisedBevel(Component c, Graphics g, int x, int y,
                                  int width, int height)
  {
    Color oldColor = g.getColor();
    int h = height;
    int w = width;

    g.translate(x, y);

    g.setColor(getHighlightOuterColor(c));
    g.drawLine(0, 0, 0, h-1);
    g.drawLine(1, 0, w-1, 0);

    g.setColor(getShadowOuterColor(c));
    g.drawLine(1, h-1, w-1, h-1);
    g.drawLine(w-1, 1, w-1, h-2);

    g.translate(-x, -y);
    g.setColor(oldColor);
  }

  protected void paintLoweredBevel(Component c, Graphics g, int x, int y,
                                      int width, int height)
  {
    Color oldColor = g.getColor();
    int h = height;
    int w = width;

    g.translate(x, y);

    g.setColor(getShadowInnerColor(c));
    g.drawLine(0, 0, 0, h-1);
    g.drawLine(1, 0, w-1, 0);

    g.setColor(getHighlightOuterColor(c));
    g.drawLine(1, h-1, w-1, h-1);
    g.drawLine(w-1, 1, w-1, h-2);

    g.translate(-x, -y);
    g.setColor(oldColor);
  }

  public Insets getBorderInsets (Component c)
  {
    return new Insets (1, 1, 1, 1);
  }

  public Insets getBorderInsets (Component c, Insets insets)
  {
    insets.left = insets.top = insets.right = insets.bottom = 1;

    return insets;
  }
}