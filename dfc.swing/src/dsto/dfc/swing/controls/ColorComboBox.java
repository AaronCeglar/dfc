package dsto.dfc.swing.controls;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.EventObject;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.tree.TreeCellEditor;

/**
 * Extends JFC combo box to allow selection from a list of colors, or
 * from a color selection dialog.
 *
 * @version $Revision$
 */
public class ColorComboBox
  extends JComboBox implements TableCellEditor, TreeCellEditor
{
  public static final Color [] DEFAULT_COLORS =
    new Color [] {Color.red, Color.orange, Color.yellow, Color.green,
                  Color.blue, Color.cyan, Color.magenta, Color.pink,
                  Color.white, Color.lightGray, Color.gray, Color.darkGray,
                  Color.black, new Color (0.01f, 0.01f, 0.01f)};

  /** True if selecting 'other' colour should trigger colour select dialog */
  protected transient boolean otherEnabled = true;
  private transient Vector cellEditorListeners;
  private transient Vector actionListeners;

  public ColorComboBox ()
  {
    this (Color.red);
  }

  public ColorComboBox (Color color)
  {
    super (new DefaultComboBoxModel (DEFAULT_COLORS));

    setRenderer (new Renderer ());

    setSelectedColor (color);

    super.addActionListener (new MyActionListener ());
  }

  /**
   * Override setSelectedItem () to auto add color as 'other' color if
   * it not already in the list.
   */
  public void setSelectedItem (Object item)
  {
    DefaultComboBoxModel model = (DefaultComboBoxModel)getModel ();
    int index = model.getIndexOf (item);

    if (index == -1)
    {
      // set Other color to be selected color
      model.removeElementAt (model.getSize () - 1);
      model.addElement (item);
    }

    super.setSelectedItem (item);
  }

  /**
   * Set the selected color.  Use this method rather then
   * setSelectedItem () to ensure the color dialog does not popup.
   *
   * @param color a value of type 'Color'
   */
  public void setSelectedColor (Color color)
  {
    otherEnabled = false;  // ensure color popup does not trigger
    setSelectedItem (color);
    otherEnabled = true;
  }

  public Color getSelectedColor ()
  {
    return (Color)getSelectedItem ();
  }

  // Override add and remove action listener so that we manage the
  // listener list.  This allows us to postpone action notification in
  // the case that the 'other' color is selected.

  public synchronized void removeActionListener (ActionListener l)
  {
    if(actionListeners != null && actionListeners.contains(l))
    {
      Vector v = (Vector) actionListeners.clone();
      v.removeElement(l);
      actionListeners = v;
    }
  }

  public synchronized void addActionListener (ActionListener l)
  {
    Vector v = actionListeners == null ? new Vector(2) : (Vector) actionListeners.clone();
    if(!v.contains(l))
    {
      v.addElement(l);
      actionListeners = v;
    }
  }

  // TableCellEditor and TreeCellEditor interfaces

  public Component getTableCellEditorComponent (JTable table, Object value,
                                                boolean isSelected,
                                                int row, int column)
  {
    setSelectedColor ((Color)value);

    return this;
  }

  public Component getTreeCellEditorComponent (JTree tree, Object value,
                                               boolean isSelected,
                                               boolean expanded,
                                               boolean leaf, int row)
  {
    setSelectedColor ((Color)value);
        
    return this;
  }

  public Object getCellEditorValue ()
  {
    return getSelectedItem ();
  }

  public boolean isCellEditable (EventObject e)
  {
    if (e instanceof MouseEvent)
      return ((MouseEvent)e).getClickCount () > 1;
    else
      return true;
  }

  public boolean shouldSelectCell (EventObject e)
  {
    return true;
  }

  public boolean stopCellEditing ()
  {
    fireEditingStopped (new ChangeEvent (this));
    return true;
  }

  public void cancelCellEditing ()
  {
    fireEditingCanceled (new ChangeEvent (this));
  }

  public synchronized void removeCellEditorListener (CellEditorListener l)
  {
    if(cellEditorListeners != null && cellEditorListeners.contains(l))
    {
      Vector v = (Vector) cellEditorListeners.clone();
      v.removeElement(l);
      cellEditorListeners = v;
    }
  }

  public synchronized void addCellEditorListener (CellEditorListener l)
  {
    Vector v = cellEditorListeners == null ? new Vector(2) : (Vector) cellEditorListeners.clone();
    if(!v.contains(l))
    {
      v.addElement(l);
      cellEditorListeners = v;
    }
  }

  protected void fireEditingCanceled (ChangeEvent e)
  {
    if(cellEditorListeners != null)
    {
      Vector listeners = cellEditorListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((CellEditorListener) listeners.elementAt(i)).editingCanceled(e);
      }
    }
  }

  protected void fireEditingStopped (ChangeEvent e)
  {
    if(cellEditorListeners != null)
    {
      Vector listeners = cellEditorListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((CellEditorListener) listeners.elementAt(i)).editingStopped(e);
      }
    }
  }

  protected void fireActionPerformed (ActionEvent e)
  {
    if(actionListeners != null)
    {
      Vector listeners = actionListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((ActionListener) listeners.elementAt(i)).actionPerformed(e);
      }
    }
  }

  /**
   * Listens for action events and handles dialog popup on selection
   * of 'other' color.
   */
  class MyActionListener implements ActionListener
  {
    public void actionPerformed (ActionEvent e)
    {
      int lastIndex = getModel ().getSize () - 1;

      if (otherEnabled && getSelectedIndex () == lastIndex)
      {
        // if last item (ie the "Other..." color) popup a color selector

        Color color =
          JColorChooser.showDialog (ColorComboBox.this, "Select Color", getSelectedColor ());

        if (color != null)
        {
          setSelectedColor (color);
          stopCellEditing ();
          fireActionPerformed (new ActionEvent (this, ActionEvent.ACTION_PERFORMED, ""));
        }
      } else
      {
        stopCellEditing ();
        fireActionPerformed (new ActionEvent (this, ActionEvent.ACTION_PERFORMED, ""));
      }
    }
  }

  /**
   * Simple extension of color cell renderer to set label for 'other' color
   * to "Other..." in bold font.
   */
  class Renderer extends ColorCellRenderer
  {
    Font boldFont;

    public Renderer ()
    {
      boldFont = getFont ().deriveFont (Font.BOLD);
    }

    public JComponent getRendererComponent (JComponent client,
                                            Object value,
                                            int row, int column,
                                            boolean isSelected,
                                            boolean hasFocus,
                                            boolean expanded,
                                            boolean leaf)
    {
      JLabel label =
        (JLabel)super.getRendererComponent (client, value, row, column,
                                            isSelected, hasFocus,
                                            expanded, leaf);

      // set label for last item to be "Other..."
      if (row == getModel ().getSize () - 1)
      {
        label.setText ("Other...");
        label.setFont (boldFont);
      }

      return label;
    }
  }
}
