package dsto.dfc.swing.controls;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;

import javax.swing.border.EtchedBorder;

/**
 * A border that draws an etched line at the bottom of the component.
 * Useful to display a dividing line between components.
 *
 * @version $Revision$
 */
public class EtchedDividingBorder extends EtchedBorder
{
  /**
   * Creates a lowered etched border whose colors will be derived
   * from the background color of the component passed into
   * the paintBorder method.
   */
  public EtchedDividingBorder ()
  {
    this (LOWERED);
  }

  /**
   * Creates an etched border with the specified etch-type
   * whose colors will be derived
   * from the background color of the component passed into
   * the paintBorder method.
   * @param etchType the type of etch to be drawn by the border
   */
  public EtchedDividingBorder (int etchType)
  {
    this (etchType, null, null);
  }

  /**
   * Creates a lowered etched border with the specified highlight and
   * shadow colors.
   * @param highlight the color to use for the etched highlight
   * @param shadow the color to use for the etched shadow
   */
  public EtchedDividingBorder (Color highlight, Color shadow)
  {
    this (LOWERED, highlight, shadow);
  }

  /**
   * Creates an etched border with the specified etch-type,
   * highlight and shadow colors.
   * @param etchType the type of etch to be drawn by the border
   * @param highlight the color to use for the etched highlight
   * @param shadow the color to use for the etched shadow
   */
  public EtchedDividingBorder (int etchType, Color highlight, Color shadow)
  {
    super (etchType, highlight, shadow);
  }

  /**
   * Paints the border for the specified component with the 
   * specified position and size.
   * @param c the component for which this border is being painted
   * @param g the paint graphics
   * @param x the x position of the painted border
   * @param y the y position of the painted border
   * @param width the width of the painted border
   * @param height the height of the painted border
   */
  public void paintBorder (Component c, Graphics g,
                           int x, int y, int width, int height)
  {
    int w = width;
    int h = height;
    int ydelta = y + h - 2;
	
    g.translate (x, ydelta);
	
    g.setColor (etchType == LOWERED? getShadowColor(c) : getHighlightColor(c));
    g.drawLine (0, 0, w - 1, 0);
	
    g.setColor (etchType == LOWERED? getHighlightColor(c) : getShadowColor(c));
    g.drawLine (0, 1, w - 1, 1);
	
    g.translate (-x, -ydelta);
  }

  /**
   * Returns the insets of the border.
   * @param c the component for which this border insets value applies
   */
  public Insets getBorderInsets (Component c)
  {
    return new Insets (0, 0, 2, 0);
  }
}
