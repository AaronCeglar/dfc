package dsto.dfc.swing.controls;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.Border;

/**
 * Abstract base class for editors that use the [editor][...] pattern
 * to a display a value and provide a popup dialog to edit the value.
 * Subclasses implement showPopup () and getCellEditorValue ().
 *
 * @version $Revision$
 */
public abstract class AbstractPopupEditor extends AbstractCellEditor
{
  public static final int EXPAND_NONE = GridBagConstraints.NONE;
  public static final int EXPAND_HORIZONTAL = GridBagConstraints.HORIZONTAL;
  public static final int EXPAND_VERTICAL = GridBagConstraints.VERTICAL;
  public static final int EXPAND_BOTH = GridBagConstraints.BOTH;

  /** Layout constraints attached to the renderer component */
  private GridBagConstraints RENDERER_CONSTRAINTS =
    new GridBagConstraints (0, 0, 1, 1, 1.0, 1.0,
                           GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                           new Insets (0, 0, 0, 3), 0, 0);

  private int expandRenderer = EXPAND_BOTH; 
  private Border border;
  private CellRenderer renderer;
  private JComponent rendererComponent;
  private JButton popupButton = new JButton();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();

  public AbstractPopupEditor ()
  {
    this (null);
  }

  /**
   * Create a new editor.
   *
   * @param renderer The renderer to be displayed to the left of the
   * [...] button.
   */
  public AbstractPopupEditor (CellRenderer renderer)
  {
    this.renderer = renderer;

    updateRendererConstraints ();

    // create a dummy renderer component for now
    rendererComponent = new JLabel ();

    try
    {
      jbInit ();
    } catch (Exception e)
    {
      e.printStackTrace ();
    }
  }

  /**
   * Superclasses should implement this to display the popup
   * dialog for the editor.
   *
   * @return True if the popup was OK'd, false if the popup was cancelled.
   */
  public abstract boolean showPopup ();

  /**
   * Superclasses should implement this to return the value of the
   * cell editor.
   */
  public abstract Object getCellEditorValue ();

  public CellRenderer getRenderer ()
  {
    return renderer;
  }

  public void setExpandRenderer (int mode)
  {
    expandRenderer = mode;

    updateRendererConstraints ();
  }

  public int getExpandRenderer ()
  {
    return expandRenderer;
  }

  /**
   * Set the border around the renderer component.  The border is not
   * used when this component is used inside a JTable.
   */
  public void setRendererBorder (Border border)
  {
    this.border = border;
  }

  public void setRenderer (CellRenderer renderer)
  {
    this.renderer = renderer;
  }

  /**
   * Returns this, after configuring the renderer component.
   */
  public JComponent getEditorComponent (JComponent client,
                                        Object value,
                                        int row, int column,
                                        boolean isSelected,
                                        boolean expanded,
                                        boolean leaf)
  {
    rendererComponent =
      renderer.getRendererComponent (client, value, row, column, isSelected,
                                     false, expanded, leaf);

    setValue (value);

    // make renderer component fit same font as this
    rendererComponent.setFont (getFont ());

    // ignore border if nonen set or being used inside a JTable
    if (border != null && !(client instanceof JTable))
      rendererComponent.setBorder (border);
    else
      rendererComponent.setBorder (BorderFactory.createEmptyBorder (0, 3, 0, 0));

    // replace old renderer with new
    remove (0);
    add (rendererComponent, RENDERER_CONSTRAINTS, 0);

    return this;
  }

  public void setFont (Font font)
  {
    super.setFont (font);

    // make rendererComponent fit font-wise
    if (rendererComponent != null)
      rendererComponent.setFont (font);
  }

  /**
   * Update the GridBagConstraint used for the renderer component to reflect
   * the current expandRenderer setting.
   */
  protected void updateRendererConstraints ()
  {
    RENDERER_CONSTRAINTS =
      new GridBagConstraints (0, 0, 1, 1, 1.0, 1.0,
                              GridBagConstraints.CENTER, expandRenderer,
                              new Insets (0, 0, 0, 3), 0, 0);

    // if a renderer component is installed, update it
    if (renderer != null && rendererComponent != null)
    {
      remove (0);
      add (rendererComponent, RENDERER_CONSTRAINTS, 0);
    }
  }

  /**
   * Set the value displayed by the renderer.
   */
  protected void setValue (Object value)
  {
    rendererComponent =
      renderer.getRendererComponent (this, value, 0, 0, false, false, false, false);

    if (border != null)
      rendererComponent.setBorder (border);

    remove (0);
    add (rendererComponent, RENDERER_CONSTRAINTS, 0);
  }

  protected void popupButton_actionPerformed (ActionEvent e)
  {
    if (showPopup () == false)
      fireEditingCanceled ();
    else
      fireEditingStopped ();
  }

  private void jbInit () throws Exception
  {
    this.setLayout(gridBagLayout1);
    popupButton.setRolloverEnabled(true);
    popupButton.setText("...");
    popupButton.setMargin (new Insets (2, 4, 2, 4));
    popupButton.setToolTipText ("Click to change value");
    popupButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        popupButton_actionPerformed(e);
      }
    });
    this.add(rendererComponent, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    this.add(popupButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
  }
}
