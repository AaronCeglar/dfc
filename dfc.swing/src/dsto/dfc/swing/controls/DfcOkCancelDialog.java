package dsto.dfc.swing.controls;

import java.awt.Component;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.event.EventListenerList;

/**
 * Base class for Apply, OK, Cancel type dialogs.
 *
 * @version $Revision$
 */
public class DfcOkCancelDialog extends DfcDialog
{
  public static final KeyStroke OK_ACCELERATOR =
    KeyStroke.getKeyStroke (KeyEvent.VK_ENTER, 0);

  private boolean cancelled = false;
  private ButtonPanel buttonPanel = getButtonPanel ();
  private JButton applyButton = new JButton();
  private JButton okButton = new JButton();
  private JButton cancelButton = new JButton();
  protected ActionListener okListener;
  protected EventListenerList listeners = new EventListenerList ();
  
  public DfcOkCancelDialog ()
  {
    this (null, "Dialog", false);
  }

  public DfcOkCancelDialog (Component client, String title, boolean modal)
  {
    super (client, title, modal);

    getButtonPanel ().removeAll ();

    applyButton.setVisible (!modal);

    try
    {
      jbInit ();
    } catch (Exception e)
    {
      e.printStackTrace ();
    }

    okListener = new ActionListener ()
    {
      public void actionPerformed (ActionEvent e)
      {
        ok ();
      }
    };

    enableOkAccelerator(true);
  }

  public void enableOkAccelerator(boolean enable)
  {
    JComponent contentPane = (JComponent)getContentPane ();
    if (enable) 
    {
      contentPane.registerKeyboardAction
        (okListener,
         OK_ACCELERATOR,
         JComponent.WHEN_IN_FOCUSED_WINDOW);
    }
    else 
    {
      contentPane.unregisterKeyboardAction(OK_ACCELERATOR);
    }
    
  }
  
  public JButton getApplyButton ()
  {
    return applyButton;
  }

  public JButton getOkButton ()
  {
    return okButton;
  }

  public JButton getCancelButton ()
  {
    return cancelButton;
  }

  /**
   * Cancel the dialog.
   */
  public void cancel ()
  {
    cancelled = true;

    setVisible (false);
  }

  /**
   * OK the dialog.
   */
  public void ok ()
  {
    if (saveResult ())
    {
      cancelled = false;

      setVisible (false);
    }
  }

  public void apply ()
  {
    saveResult ();

    cancelled = false;
  }

  /**
   * Save the result in the dialog.
   *
   * @return True if result was saved.
   */
  public boolean saveResult ()
  {
    return true;
  }

  /**
   * True if the dialog was cancelled.
   */
  public boolean isCancelled ()
  {
    return cancelled;
  }

  public void setCancelled (boolean newValue)
  {
    cancelled = newValue;
  }

  public void addActionListener (ActionListener l)
  {
    listeners.add (ActionListener.class, l);
  }
  
  public void removeActionListener (ActionListener l)
  {
    listeners.remove (ActionListener.class, l);
  }
  
  protected void fireActionEvent (String command)
  {
    Object [] l = listeners.getListenerList ();
    ActionEvent e = null;
    
    for (int i = l.length - 2; i >= 0; i -= 2)
    {
      if (l [i] == ActionListener.class)
      {
        // Lazily create the event:
        if (e == null)
          e = new ActionEvent (this, Event.ACTION_EVENT, command);
          
        ((ActionListener)l [i + 1]).actionPerformed (e);
      }
    }
  }
  
  private void jbInit() throws Exception
  {
    applyButton.setMnemonic('A');
    applyButton.setText("Apply");
    applyButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        button_actionPerformed(e);
      }
    });
    okButton.setText("OK");
    okButton.setDefaultCapable (true);
    okButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        button_actionPerformed(e);
      }
    });
    cancelButton.setText("Cancel");
    cancelButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        button_actionPerformed(e);
      }
    });
    buttonPanel.add(okButton, null);
    buttonPanel.add(cancelButton, null);
    buttonPanel.add(applyButton, null);
  }

  protected void button_actionPerformed (ActionEvent e)
  {
    if (e.getSource () == cancelButton)
      cancel ();
    else if (e.getSource () == okButton)
      ok ();
    else
      apply ();
      
    fireActionEvent (e.getActionCommand ());
  }
}
