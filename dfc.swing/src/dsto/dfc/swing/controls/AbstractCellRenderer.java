package dsto.dfc.swing.controls;

import java.awt.Color;
import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.tree.DefaultTreeCellRenderer;

/**
 * Base for classes that want to implement a cell renderer that can be
 * used interchangably for lists, tables and trees: classes implement
 * one method (getRendererComponent ()) to provide a renderer for all
 * 3 interfaces.  Provides support for setting colours, borders etc
 * based on behaviour of default renderers.
 *
 * @version $Revision$
 */
public abstract class AbstractCellRenderer implements CellRenderer
{
  private JLabel templateLabel;
  private DefaultTableCellRenderer defaultTableCellRenderer;
  private DefaultListCellRenderer defaultListCellRenderer;
  private DefaultTreeCellRenderer defaultTreeCellRenderer;

  /**
   * The current background colour that should be used.
   */
  public Color getBackground ()
  {
    return templateLabel.getBackground ();
  }

  /**
   * The current foreground colour that should be used.
   */
  public Color getForeground ()
  {
    return templateLabel.getForeground ();
  }

  /**
   * The current border that should be used.
   */
  public Border getBorder ()
  {
    return templateLabel.getBorder ();
  }

  /**
   * Get a label guaranteed to have correct colours and borders set.
   */
  public JLabel getLabel ()
  {
    if (templateLabel == null)
      templateLabel = new JLabel ();

    return templateLabel;
  }

  /**
   * Return the renderer component.
   */
  public abstract JComponent getRendererComponent (JComponent client,
                                                   Object value,
                                                   int row, int column,
                                                   boolean isSelected,
                                                   boolean hasFocus,
                                                   boolean expanded,
                                                   boolean leaf);

  // TreeCellRenderer interface

  public Component getTreeCellRendererComponent (JTree tree, Object value,
                                                 boolean selected,
                                                 boolean expanded,
                                                 boolean leaf, int row,
                                                 boolean hasFocus)
  {
    if (defaultTreeCellRenderer == null)
      defaultTreeCellRenderer = new DefaultTreeCellRenderer ();

    templateLabel =
      (JLabel)defaultTreeCellRenderer.getTreeCellRendererComponent
        (tree, value, selected, expanded, leaf, row, hasFocus);

    return getRendererComponent (tree, value, row, 0, selected, hasFocus,
                                 expanded, leaf);
  }

  // TableCellRenderer interface

  public Component getTableCellRendererComponent (JTable table, Object value,
                                                  boolean isSelected,
                                                  boolean hasFocus,
                                                  int row, int column)
  {
    if (defaultTableCellRenderer == null)
      defaultTableCellRenderer = new DefaultTableCellRenderer ();

    templateLabel =
      (JLabel)defaultTableCellRenderer.getTableCellRendererComponent
        (table, value, isSelected, hasFocus, row, column);

    return getRendererComponent (table, value, row, column, isSelected,
                                 hasFocus, false, true);
  }

  // ListCellRenderer interface

  public Component getListCellRendererComponent (JList list, Object value,
                                                 int index, boolean isSelected,
                                                 boolean hasFocus)
  {
    if (defaultListCellRenderer == null)
      defaultListCellRenderer = new DefaultListCellRenderer ();

    templateLabel =
      (JLabel)defaultListCellRenderer.getListCellRendererComponent
        (list, value, index, isSelected, hasFocus);

    return getRendererComponent (list, value, index, 0, isSelected, hasFocus,
                                 false, true);
  }
}
