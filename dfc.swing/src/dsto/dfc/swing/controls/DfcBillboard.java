package dsto.dfc.swing.controls;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.JComponent;

/**
 * <p>DfcBillboard will let you place images in a certain layout, and
 * a certain alignment, using dsto.dfc.swing.controls.BillBoard, in a JComponent.
 * This is pretty much a small wrapper around BillBoard.
 * For example:</p>
 *
 * <pre>
 *   addImage (image, DfcBillboard.LAYOUT_SINGLE,
 *                    DfcBillboard.ALIGNMENT_BOTTOM_RIGHT);
 * </pre>
 *
 * <p>Alignment is obviously not necessary when using LAYOUT_TILED,
 * and this is why there is two addImage(...) methods.</p>
 *
 * @version $Revision$
 */

public class DfcBillboard extends JComponent
{
  public final static int LAYOUT_SINGLE = BillBoard.LAYOUT_SINGLE;
  public final static int LAYOUT_TILED = BillBoard.LAYOUT_TILED;
  public final static int LAYOUT_STRETCHED_NOASPECT = BillBoard.LAYOUT_STRETCHED_NOASPECT;
  public final static int LAYOUT_STRETCHED_ASPECT = BillBoard.LAYOUT_STRETCHED_ASPECT;

  public static final int ALIGNMENT_TOP_LEFT = BillBoard.ALIGNMENT_TOP_LEFT;
  public static final int ALIGNMENT_MID_TOP = BillBoard.ALIGNMENT_MID_TOP;
  public static final int ALIGNMENT_TOP_RIGHT = BillBoard.ALIGNMENT_TOP_RIGHT;
  public static final int ALIGNMENT_MID_RIGHT = BillBoard.ALIGNMENT_MID_RIGHT;
  public static final int ALIGNMENT_BOTTOM_RIGHT = BillBoard.ALIGNMENT_BOTTOM_RIGHT;
  public static final int ALIGNMENT_MID_BOTTOM = BillBoard.ALIGNMENT_MID_BOTTOM;
  public static final int ALIGNMENT_BOTTOM_LEFT = BillBoard.ALIGNMENT_BOTTOM_LEFT;
  public static final int ALIGNMENT_MID_LEFT = BillBoard.ALIGNMENT_MID_LEFT;
  public static final int ALIGNMENT_CENTER = BillBoard.ALIGNMENT_CENTER;

  private BillBoard billboard = new BillBoard();

  public DfcBillboard()
  {
    billboard.setBackgroundColor (getBackground());
  }

  /*
   * Add an Image to the Billboard.
   *
   * @param img The image to add.
   * @param layout The layout to use, ie DfcBillboard.LAYOUT_*
   */
  public void addImage (Image img, int layout)
  {
    addImage(img, layout, ALIGNMENT_TOP_LEFT);
  }

  /*
   * Add an Image to the Billboard.
   *
   * @param img The image to add.
   * @param layout The layout to use, ie DfcBillboard.LAYOUT_*
   * @param alignment The alignment to use, ie DfcBillboard.ALIGNMENT_*
   */
  public void addImage (Image img, int layout, int alignment)
  {
    billboard.addImage (img, layout, alignment);
    repaint();
  }

  /*
   * Remove an Image from the Billboard.
   *
   * @param img The image you want to remove.
   */
  public void removeImage (Image img)
  {
    billboard.removeImage (img);
    repaint();
  }

  /*
   * Override the paintComponent method of JComponent, so that we can
   * actually do the image drawing, etc...  Goes through the array of
   * images in the order they were given and paints them on the this
   * components surface with the right layout and alignment (if
   * necessary).
   *
   * @param g The Graphics Context.
   */
  protected void paintComponent (Graphics g)
  {
    g = g.create ();

    Insets insets = getInsets ();
    Dimension size = getSize ();
    size.width -= insets.left + insets.right;
    size.height -= insets.top + insets.bottom;

    billboard.setSize (size);

    g.clipRect (insets.left, insets.top, size.width, size.height);
    g.translate (insets.left, insets.top);
    billboard.paint (g);
    g.translate (-insets.left, -insets.top);
  }
}
