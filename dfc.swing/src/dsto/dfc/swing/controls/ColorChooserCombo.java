package dsto.dfc.swing.controls;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.plaf.metal.MetalComboBoxIcon;

import dsto.dfc.swing.forms.AbstractFormEditorComponent;
import dsto.dfc.util.IllegalFormatException;

/**
 * A JPanel that contains a MetalComboBoxButton which when clicked opens up a
 * ColorChooserPanel. When a color is chosen a property change is fired to
 * alert the parent of a new color.
 *
 * @author       Luke Marsh
 * @version      $Revision$
 *
 * @see ColorScheme
 * @see ColorSchemePanel
 * @see ColorChooserPanel
 */
public class ColorChooserCombo extends AbstractFormEditorComponent
  implements PropertyChangeListener, ActionListener
{

  protected boolean popupShown;
  protected boolean initialize;
  protected JPopupMenu colorPopup;
  protected GridBagLayout gridBagLayout1;
  protected ColorChooserPanel colorChooser;
  protected Color selectedColor;
  protected String selectedColorName;
  protected MetalComboBoxButton comboButton;


  /**
   * Initializes variables.
   */
  public ColorChooserCombo()
  {
    gridBagLayout1 = new GridBagLayout ();
    colorPopup = new JPopupMenu ("Color Chooser");

    colorChooser = new ColorChooserPanel ();
    selectedColor = new Color (123, 123, 124);
    selectedColorName = "";
    popupShown = false;
    initialize = true;

    MetalComboBoxIcon icon = new MetalComboBoxIcon ();
    comboButton = new MetalComboBoxButton ("", icon);
    colorPopup.add (colorChooser);
    setColor (selectedColor);
    jbInit ();
  }


  /**
   * Assigns the event new value to selectedColor.
   */
  public void propertyChange (PropertyChangeEvent evt)
  {
    if (evt.getPropertyName ().equals ("color"))
    {
      colorPopup.setVisible (false);
      popupShown = false;
      comboButton.remove (colorPopup);
      colorChooser.disarm ();
      setColor ((Color) evt.getNewValue ());
      fireEditCommitted ();
    }
  }


  /**
   * Get the currently selected Color.
   */
  public Color getColor ()
  {
    return selectedColor;
  }


  /**
   * Set the selected color to c.
   * Goes through the default color schemes to check if the color set
   * has an associated name.
   */
  public void setColor (Color c)
  {
    Color oldColor = selectedColor;
    selectedColor = c;
    String name = "";

    List colorSchemes = colorChooser.getColorSchemeList ();
    for (int i = 0; i < colorSchemes.size (); i++)
    {
      // Search for the selectedColor in all of the schemes.
      List colors = ((ColorScheme) colorSchemes.get (i)).getColors ();
      for (int j = 0; j < colors.size (); j++)
      {
        // Search the ArrayList of colors to match a color and get its name.
        if (selectedColor.equals (colors.get (j)))
        {
          ColorScheme scheme = (ColorScheme) colorSchemes.get (i);
          name = scheme.getColorNames ().get (j).toString ();
          setColorName (name);
          if (initialize == true)
          {
            colorChooser.setScheme (1);
            initialize = false;
          }
          else
          {
            colorChooser.setScheme (i - 1);
          }
          break;
        }
      }

      if (!name.equals (""))
      {
        break;
      }
    }

    if (name.equals (""))
    {
      name = "[" + selectedColor.getRed () + ", " + selectedColor.getGreen ()
              + ", " + selectedColor.getBlue () + "]";
      setColorName (name);
    }

    comboButton.setIcon (new ColourSwatchIcon (selectedColor));
    firePropertyChange ("color", oldColor, selectedColor);
  }


  /**
   * Get the currently selected Color's Name.
   */
  public String getColorName ()
  {
    return selectedColorName;
  }


  /**
   * Set the selected color's name.
   */
  public void setColorName (String n)
  {
    selectedColorName = n;
    comboButton.setText (selectedColorName + " ");
  }


  /**
   * Implements abstract method in dsto.dfc.swing.forms.AbstractFormEditor.
   * This control is not direct-editable.
   */
  public boolean isDirectEdit ()
  {
    return false;
  }


  /**
   * Implements abstract method in dsto.dfc.swing.forms.AbstractFormEditor.
   * Sets value to the current Color, and throws an exception if value is not
   * a real Color.
   */
  public void setEditorValue (Object value) throws IllegalFormatException
  {
    try
    {
      setColor ((Color) value);
    }
    catch (ClassCastException ex)
    {
      throw new IllegalFormatException (this, value, Color.class);
    }
  }


  /**
   * Implements abstract method in dsto.dfc.swing.forms.AbstractFormEditor.
   * Returns the current color.
   */
  public Object getEditorValue () throws IllegalFormatException
  {
    return getColor ();
  }


  /**
   * Implements abstract method in dsto.dfc.swing.forms.AbstractFormEditor.
   * Commit the selection.
   */
  public void commitEdits () throws IllegalFormatException
  {
    // zip
  }


  /**
   * When Someone chooses a color on the ColorSchemePanel hide the
   * ColorChooserPanel.
   */
  public void actionPerformed (ActionEvent e)
  {
    if (e.getActionCommand ().equals ("Color Chosen"))
    {
      colorPopup.setVisible (false);
      popupShown = false;
      comboButton.remove (colorPopup);
      colorChooser.disarm ();
    }
  }

  // Overrides JComponent methods.

  /**
   * Sets the background color for comboButton.
   */
  public void setBackground (Color bg)
  {
    comboButton.setBackground (bg);
  }


  /**
   * Sets the foreground color for comboButton.
   */
  public void setForeground (Color fg)
  {
    comboButton.setForeground (fg);
  }


  /**
   * Sets the font for comboButton.
   */
  public void setFont (Font font)
  {
    comboButton.setFont (font);
  }


  /**
   * Sets the opaque property for comboButton.
   */
  public void setOpaque (boolean isOpaque)
  {
    comboButton.setOpaque (isOpaque);
  }


  /**
   * Sets the ToolTipText for comboButton.
   */
  public void setToolTipText (String text)
  {
    comboButton.setToolTipText (text);
  }

  /**
   * Overrides JComponents method, so that we can enable/disable the comboButton.
   */
  public void setEnabled (boolean aFlag)
  {
    comboButton.setEnabled(aFlag);
  }

  /**
   * Overrides JComponents method and hides this component first.
   */
  public void setVisible (boolean aFlag)
  {
    if (aFlag == false)
    {
      colorPopup.setVisible (false);
      super.setVisible (aFlag);
    }
    else
    {
      super.setVisible (aFlag);
    }
  }


  /**
   * Overrides JComponents method and hides this component first.
   */
  public void removeNotify ()
  {
    colorPopup.setVisible (false);
    super.removeNotify ();
  }


  /**
   * Initializes components.
   */
  private void jbInit ()
  {
    this.setLayout (gridBagLayout1);

    comboButton.getModel ().addActionListener (new PopupButtonListener ());
    comboButton.setHorizontalAlignment (SwingConstants.LEFT);

    setMinimumSize (new Dimension (105, 27));
    setPreferredSize (new Dimension (123, 27));

    Insets m = new Insets (2, 0, 2, 12);
    comboButton.setMargin (m);

    this.add (comboButton, new GridBagConstraints (0, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets
            (0, 0, 0, 0), 0, 0));

    colorChooser.addPropertyChangeListener (this);
    colorChooser.getColorSchemePanel ().addActionListener (this);
  }


  /**
   * Shows a ColorChooserPanel when the button is clicked
   */
  private class PopupButtonListener implements ActionListener
  {
    /**
     * Initializes colorCombo.
     */
    public PopupButtonListener ()
    {
      // zip
    }


    /**
     * Shows a ColorChooserPanel immediately below the button.
     */
    public void actionPerformed (ActionEvent e)
    {
      if (popupShown == true)
      {
        colorPopup.setVisible (false);
        popupShown = false;
        comboButton.remove (colorPopup);
        colorChooser.disarm ();
      }
      else
      {
        Dimension d = comboButton.getSize ();
        Dimension d2 = colorChooser.getPreferredSize ();
        int y = (int) d2.getHeight ();
        int x = (int) d.getWidth ();
        d2 = new Dimension (x, y);
        colorChooser.setPreferredSize (d2);
        y = comboButton.getHeight ();
        colorPopup = new JPopupMenu ("Color Chooser");
        colorPopup.add (colorChooser);
        colorChooser.setSelectedButton (selectedColor);
        colorPopup.show (comboButton, 0, y);
        popupShown = true;
      }
    }
  }
}