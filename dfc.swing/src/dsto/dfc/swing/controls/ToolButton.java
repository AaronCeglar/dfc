package dsto.dfc.swing.controls;

import java.awt.Insets;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.DefaultButtonModel;
import javax.swing.Icon;
import javax.swing.JToggleButton;
import javax.swing.plaf.ButtonUI;

/**
 * A toolbar'ish button with a narrow bevelled border and sexy
 * rollover effects.  Can be set to act as a normal button or as a
 * toggle.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class ToolButton extends AbstractButton
{
  protected static final Insets DEFAULT_MARGIN = new Insets (2, 4, 2, 4);

  private boolean toggle;

  public ToolButton ()
  {
    this (null, null);
  }

  public ToolButton (String text, Icon icon)
  {
    super ();

    setBorder (BorderFactory.createEmptyBorder (0, 0, 0, 0));

    setRolloverEnabled (true);
    setModel (new DefaultButtonModel ());


    init (text, icon);

    setMargin (DEFAULT_MARGIN);
  }

  public void setMargin (Insets margin)
  {
    ToolButtonUI currentUI = (ToolButtonUI) getUI ();

    currentUI.updateBorder (margin);

    super.setMargin (margin);

    setBorder (currentUI.getBorder (this));
  }

  /**
   * Override.
   */
  public void updateUI ()
  {
    setUI (new ToolButtonUI ());
  }

  public String getUIClassID ()
  {
    return "ToolButtonUI";
  }

  public ButtonUI getUI ()
  {
    return (ToolButtonUI) ui;
  }

  public void setUI (ToolButtonUI newUI)
  {
    super.setUI (newUI);
  }

  /**
   * Set whether button is in toggle mode. NOTE: this may change the
   * button model.
   */
  public void setToggle (boolean newToggle)
  {
    if (newToggle != toggle)
    {
      boolean oldValue = toggle;

      toggle = newToggle;

      firePropertyChange ("toggle", oldValue, newToggle);

      setModel (toggle ? new JToggleButton.ToggleButtonModel () :
                         new DefaultButtonModel ());
    }
  }

  public boolean isToggle ()
  {
    return toggle;
  }
}
