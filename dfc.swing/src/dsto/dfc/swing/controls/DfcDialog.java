package dsto.dfc.swing.controls;

import java.awt.Component;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import dsto.dfc.swing.commands.AbstractCommand;
import dsto.dfc.swing.commands.Command;
import dsto.dfc.util.Disposable;

/**
 * Simple extension of a JFC JDialog to manage use of dialog with any
 * sort of window rather than just Frame's.
 *
 * @version $Revision$
 */
public class DfcDialog extends JDialog
{
  public static final int ALIGN_CENTER_CLIENT = 0;
  public static final int ALIGN_CENTER_SCREEN = 1;
  public static final int ALIGN_NONE = 2;

  public static final KeyStroke CANCEL_ACCELERATOR =
    KeyStroke.getKeyStroke (KeyEvent.VK_ESCAPE, 0);

  private int alignMode = ALIGN_CENTER_CLIENT;
  private CmdCancel cancelCommand = new CmdCancel ();
  private Component client;
  private JPanel mainPanel = new JPanel();
  private ButtonPanel buttonPanel = new ButtonPanel();
  private Component dialogPanel = new JPanel ();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private JButton cancelButton = new JButton();

  /**
   * For beans compliance only.
   */
  public DfcDialog ()
  {
    super ();
  }

  public DfcDialog (Component client, String title, boolean modal)
  {
    super (getFrameForClient (client), title, modal);

    this.client = client;

    try
    {
      jbInit ();
    } catch (Exception e)
    {
      e.printStackTrace ();
    }

    JComponent contentPane = (JComponent)getContentPane ();

    contentPane.registerKeyboardAction
      (cancelCommand, CANCEL_ACCELERATOR,
       JComponent.WHEN_IN_FOCUSED_WINDOW);

    setDefaultCloseOperation (DISPOSE_ON_CLOSE);
  }

  public void dispose ()
  {
    if (getDialogPanel () instanceof Disposable)
      ((Disposable)getDialogPanel ()).dispose ();

    mainPanel.removeAll ();

    super.dispose ();
  }

  public void setAlignMode (int newMode)
  {
    alignMode = newMode;
  }

  public int getAlignMode ()
  {
    return alignMode;
  }

  public ButtonPanel getButtonPanel ()
  {
    return buttonPanel;
  }

  public Component getDialogPanel ()
  {
    return dialogPanel;
  }

  public void setDialogPanel (Component panel)
  {
    mainPanel.remove (dialogPanel);

    if (panel != null)
    {
      mainPanel.add
        (panel, new GridBagConstraints (0, 0, 1, 1, 1.0, 1.0,
                                        GridBagConstraints.CENTER,
                                        GridBagConstraints.BOTH,
                                        new Insets(5, 5, 5, 5), 0, 0));
    }

    mainPanel.revalidate ();

    dialogPanel = panel;
  }

  /**
   * @deprecated Use getDialogPanel () instead.
   */
  public Component getContentPanel ()
  {
    return getDialogPanel ();
  }

  /**
   * Set the panel that displays the dialog contents.
   *
   * @deprecated Use setDialogPanel () instead.
   */
  public void setContentPanel (JPanel panel)
  {
    setDialogPanel (panel);
  }

  public Command getCancelCommand ()
  {
    return cancelCommand;
  }

  public JButton getCancelButton ()
  {
    return cancelButton;
  }

  public void cancel ()
  {
    setVisible (false);
  }

  public void setVisible (boolean visible)
  {
    boolean oldValue = isVisible ();

    if (oldValue != visible)
    {
      if (visible)
      {
        placeWindow ();

        super.setVisible (visible);
      } else
      {
        super.setVisible (visible);

        if (getDefaultCloseOperation () == JDialog.DISPOSE_ON_CLOSE)
        {
          dispose ();
        }
      }
    }
  }

  protected void placeWindow ()
  {
    Window clientWindow;

    if (client instanceof Window)
      clientWindow = (Window)client;
    else if (client != null)
      clientWindow = SwingUtilities.windowForComponent (client);
    else
      clientWindow = null;

    // handle window alignment
    if (alignMode == ALIGN_CENTER_CLIENT && clientWindow != null)
      WindowLayout.placeWindow (clientWindow, this, WindowLayout.CENTER);
    else if (alignMode != ALIGN_NONE)
      WindowLayout.placeWindow (this, WindowLayout.CENTER);
  }

  private static Frame getFrameForClient (Component client)
  {
    if (client instanceof Frame)
      return (Frame)client;
    else
    {
      if (client != null)
      {
        Window window = SwingUtilities.windowForComponent (client);

        while (window != null && !(window instanceof Frame))
          window = window.getOwner ();

        return (Frame)window;
      } else
        return null;
    }
  }

  protected void this_windowClosing (WindowEvent e)
  {
    cancel ();
  }

  protected void cancelButton_actionPerformed (ActionEvent e)
  {
    cancel ();
  }

  private void jbInit() throws Exception
  {
    mainPanel.setLayout(gridBagLayout1);

    this.addWindowListener(new java.awt.event.WindowAdapter()
    {

      public void windowClosing(WindowEvent e)
      {
        this_windowClosing(e);
      }
    });
    cancelButton.setActionCommand("Cancel");
    cancelButton.setMnemonic('C');
    cancelButton.setText("Close");
    cancelButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    mainPanel.add(buttonPanel, new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    buttonPanel.add(cancelButton, null);
    mainPanel.add(dialogPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
    this.getContentPane ().add (mainPanel);
  }

  protected class CmdCancel extends AbstractCommand
  {
    public void execute ()
    {
      cancel ();
    }

    public String getName ()
    {
      return "dialog.Cancel";
    }

    public String getDescription ()
    {
      return "Cancel dialog";
    }

    public boolean isInteractive ()
    {
      return false;
    }

    public String getGroupInView (String viewName)
    {
      return null;
    }

    public char getMnemonic ()
    {
      return 'c';
    }
  }
}
