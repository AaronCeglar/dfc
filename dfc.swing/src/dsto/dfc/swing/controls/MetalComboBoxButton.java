package dsto.dfc.swing.controls;

import java.awt.Graphics;
import java.awt.Insets;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.plaf.metal.MetalComboBoxIcon;

/**
 * A modified version of javax.swing.plaf.metal.MetalComboBoxButton. It only
 * contains the button itself with the down arrow icon. Can be used as a button
 * to open up panels while looking like a combo box.
 *
 * @author      Luke Marsh
 * @version     $Revision$
 */
public class MetalComboBoxButton extends JButton
{

  private Icon comboIcon;

  /**
   * Sets the initial text on the button to null and initializes comboIcon
   * a new MetalComboBoxIcon ().
   */
  public MetalComboBoxButton ()
  {
    this ("", new MetalComboBoxIcon ());
  }


  /**
   * Sets the initial text on the button and initializes comboIcon to i.
   */
  public MetalComboBoxButton (String s, Icon i)
  {
    this.setText (s);
    comboIcon = i;
  }


  /**
   * Draws the button with the icon on the right of the button.
   */
  @Override
  public void paintComponent (Graphics g)
  {
    // Paint the button as usual.
    super.paintComponent (g);
    Insets insets = getInsets ();

    int width = getWidth () - (insets.left + insets.right);
    int height = getHeight () - (insets.top + insets.bottom);

    if (height <= 0 || width <= 0)
    {
      return;
    }

    int left = insets.left;
    int right = left + (width - 1);
    int iconLeft = right;

    // Paint the icon
    if (comboIcon != null)
    {
      int iconHeight = comboIcon.getIconHeight ();
      int iconTop = 0;

      iconLeft = right;
      iconTop = (getHeight () / 2) - (iconHeight / 2);

      comboIcon.paintIcon (this, g, iconLeft, iconTop);
    }
  }
}