package dsto.dfc.swing.controls;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.ImageObserver;
import java.io.InvalidObjectException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.Icon;

import dsto.dfc.swing.icons.IconicEnumerationValue;
import dsto.dfc.swing.icons.ImageLoader;
import dsto.dfc.util.EnumerationValue;

/**
 * <p>BillBoard will let you place images in a certain layout, and
 * a certain alignment.  For example:</p>
 *
 * <pre>
 *   addImage (image, BillBoard.LAYOUT_SINGLE,
 *                    BillBoard.ALIGNMENT_BOTTOM_RIGHT);
 * </pre>
 *
 * <p>Alignment is obviously not necessary when using LAYOUT_TILED,
 * and this is why there is two addImage(...) methods.</p>
 *
 * @version $Revision$
 */

public class BillBoard implements ImageObserver
{
  public final static int LAYOUT_SINGLE = 0;
  public final static int LAYOUT_TILED = 1;
  public final static int LAYOUT_STRETCHED_NOASPECT = 2;
  public final static int LAYOUT_STRETCHED_ASPECT = 3;

  public final static int ALIGNMENT_TOP_LEFT = Geometry.TOP_LEFT;
  public final static int ALIGNMENT_MID_TOP = Geometry.MID_TOP;
  public final static int ALIGNMENT_TOP_RIGHT = Geometry.TOP_RIGHT;
  public final static int ALIGNMENT_MID_RIGHT = Geometry.MID_RIGHT;
  public final static int ALIGNMENT_BOTTOM_RIGHT = Geometry.BOTTOM_RIGHT;
  public final static int ALIGNMENT_MID_BOTTOM = Geometry.MID_BOTTOM;
  public final static int ALIGNMENT_BOTTOM_LEFT = Geometry.BOTTOM_LEFT;
  public final static int ALIGNMENT_MID_LEFT = Geometry.MID_LEFT;
  public final static int ALIGNMENT_CENTER = Geometry.CENTER;

  public final static EnumerationValue TILED =
    new LayoutEnum (LAYOUT_TILED, ALIGNMENT_TOP_LEFT, "Tiled",
    ImageLoader.getIcon("/dsto/dfc/icons/tiled_icon.gif"));
  public final static EnumerationValue CENTERED =
    new LayoutEnum (LAYOUT_SINGLE, ALIGNMENT_CENTER, "Centered",
    ImageLoader.getIcon("/dsto/dfc/icons/center_icon.gif"));
  public final static EnumerationValue STRETCHED =
    new LayoutEnum (LAYOUT_STRETCHED_NOASPECT, ALIGNMENT_TOP_LEFT, "Stretched",
    ImageLoader.getIcon("/dsto/dfc/icons/stretched_icon.gif"));
  public final static EnumerationValue STRETCHED_ASPECT =
    new LayoutEnum (LAYOUT_STRETCHED_ASPECT, ALIGNMENT_CENTER, "Stretched with Aspect",
    ImageLoader.getIcon("/dsto/dfc/icons/stretched_aspect_icon.gif"));
  public final static EnumerationValue TOP_LEFT =
    new LayoutEnum (LAYOUT_SINGLE, ALIGNMENT_TOP_LEFT, "Top left",
    ImageLoader.getIcon("/dsto/dfc/icons/topleft_icon.gif"));
  public final static EnumerationValue TOP_CENTER =
    new LayoutEnum(LAYOUT_SINGLE, ALIGNMENT_MID_TOP, "Top center",
    ImageLoader.getIcon("/dsto/dfc/icons/topcenter_icon.gif"));
  public final static EnumerationValue TOP_RIGHT =
    new LayoutEnum(LAYOUT_SINGLE, ALIGNMENT_TOP_RIGHT, "Top right",
    ImageLoader.getIcon("/dsto/dfc/icons/topright_icon.gif"));
  public final static EnumerationValue CENTER_LEFT =
    new LayoutEnum(LAYOUT_SINGLE, ALIGNMENT_MID_LEFT, "Center left",
    ImageLoader.getIcon("/dsto/dfc/icons/centerleft_icon.gif"));
  public final static EnumerationValue CENTER_RIGHT =
    new LayoutEnum(LAYOUT_SINGLE, ALIGNMENT_MID_RIGHT, "Center right",
    ImageLoader.getIcon("/dsto/dfc/icons/centerright_icon.gif"));
  public final static EnumerationValue BOTTOM_LEFT =
    new LayoutEnum(LAYOUT_SINGLE, ALIGNMENT_BOTTOM_LEFT, "Bottom left",
    ImageLoader.getIcon("/dsto/dfc/icons/bottomleft_icon.gif"));
  public final static EnumerationValue BOTTOM_CENTER =
    new LayoutEnum(LAYOUT_SINGLE, ALIGNMENT_MID_BOTTOM, "Bottom center",
    ImageLoader.getIcon("/dsto/dfc/icons/bottomcenter_icon.gif"));
  public final static EnumerationValue BOTTOM_RIGHT =
    new LayoutEnum(LAYOUT_SINGLE, ALIGNMENT_BOTTOM_RIGHT, "Bottom right",
    ImageLoader.getIcon("/dsto/dfc/icons/bottomright_icon.gif"));

  public final static EnumerationValue [] ENUM_VALUES = {TILED, CENTERED, STRETCHED,
          STRETCHED_ASPECT, TOP_LEFT, TOP_CENTER, TOP_RIGHT, CENTER_LEFT,
          CENTER_RIGHT, BOTTOM_LEFT, BOTTOM_CENTER, BOTTOM_RIGHT};

  protected ArrayList imageArray = new ArrayList();
  protected Color backgroundColor = Color.green;
  protected Dimension size = new Dimension (1, 1);

  public BillBoard ()
  {
    // zip
  }

  public void setBackgroundColor (Color c)
  {
    backgroundColor = c;
  }

  public Color getBackgroundColor ()
  {
    return backgroundColor;
  }

  public void setSize (Dimension size)
  {
    this.size = size;

    for (int i = 0; i < imageArray.size (); i++)
    {
      ImageHolder holder = (ImageHolder)imageArray.get (i);

      holder.updateRect (size);
    }
  }

  public Dimension getSize ()
  {
    return size;
  }

  /*
   * Add an Image to the Billboard.
   *
   * @param image The image to add.
   * @param layout The layout to use, ie DfcBillboard.LAYOUT_*
   * @param alignment The alignment to use, ie DfcBillboard.ALIGNMENT_*
   */
  public void addImage (Image image, int layout, int alignment)
  {
    ImageHolder imageHolder;

    switch (layout)
    {
    case LAYOUT_SINGLE :
      imageHolder = new ImageLayoutSingle (image, layout, alignment);
      break;
    case LAYOUT_STRETCHED_NOASPECT :
      imageHolder = new ImageLayoutStretchedNoAspect (image, layout, alignment);
      break;
    case LAYOUT_STRETCHED_ASPECT :
      imageHolder = new ImageLayoutStretchedAspect (image, layout, alignment);
      break;
    case LAYOUT_TILED :
      imageHolder = new ImageLayoutTiled (image, layout, alignment);
      break;
    default :
      throw new Error ("Unknown Layout");
    }

    imageArray.add (imageHolder);

    imageHolder.updateRect (size);
  }

  public void addImage (Image image, EnumerationValue layoutAlignment)
  {
    LayoutEnum alignment = (LayoutEnum)layoutAlignment;

    addImage (image, alignment.getLayout (), alignment.getAlignment ());
  }

  /*
   * Remove an Image from the Billboard.
   *
   * @param image The image you want to remove.
   */
  public void removeImage (Image image)
  {
    Iterator i = imageArray.iterator();
    ImageHolder temp;

    for (temp = null; i.hasNext() ; )
    {
      temp = (ImageHolder)i.next();
      if (temp.getImage() == image)
      {
        imageArray.remove(temp);
        break;
      }
    }
  }

  public void removeAllImages ()
  {
    imageArray.clear();
  }


  public void paint (Graphics g)
  {
    Graphics2D g2 = (Graphics2D)g;
    Iterator i = imageArray.iterator();

    while (i.hasNext())
    {
      ImageHolder imageHolder = (ImageHolder)i.next();
      imageHolder.paint (g2, size);
    }
  }

  /* the ImageObserver interface */

  public boolean imageUpdate (Image image, int infoflags,
                              int x, int y, int width, int height)
  {
    return true;
  }

  /*
   * Used to store the layout and alignment along with the image to be
   * painted.
   */
  private abstract class ImageHolder implements ImageObserver
  {
    protected Image image;
    protected int layout;
    protected int alignment;
    protected Dimension imageSize;
    protected Rectangle rect = new Rectangle ();

    /*
     * The constructor.
     * @param image The image.
     * @param layout The layout of the image, ie DfcBillboard.LAYOUT_*
     * @param alignment The Alignment of the image, ie DfcBillboard.ALIGNMENT_*
     */
    public ImageHolder (Image image, int layout, int alignment)
    {
      this.image = image;
      this.layout = layout;
      this.alignment = alignment;
      this.imageSize = new Dimension (image.getWidth (this),
                                      image.getHeight (this));
    }

    public Image getImage ()
    {
      return image;
    }

    @SuppressWarnings ("unused")
    public int getLayout ()
    {
      return layout;
    }

    public int getAlignment ()
    {
      return alignment;
    }

    public void paint (Graphics2D g,
                       Dimension displaySize)
    {
      if (imageSize.width == rect.width &&
          imageSize.height == rect.height)
      {
        g.drawImage (image, rect.x, rect.y, this);

      } else
      {
        g.drawImage (image, rect.x, rect.y,
                     rect.width, rect.height, backgroundColor,
                     this);

      }
    }

    /**
     * Update the display rectangle (rect) to the current required size.
     */
    public abstract void updateRect (Dimension displaySize);

    public boolean imageUpdate (Image i, int infoflags,
                                int x, int y, int width, int height)
    {
      return true;
    }
  }

  private final class ImageLayoutSingle extends ImageHolder
  {
    public ImageLayoutSingle (Image image, int layout, int alignment)
    {
      super (image, layout, alignment);
    }

    @Override
    public void updateRect (Dimension displaySize)
    {
      Point location =
        Geometry.offsetForAligment (size, imageSize, getAlignment());

      rect.x = location.x;
      rect.y = location.y;
      rect.width = imageSize.width;
      rect.height = imageSize.height;
    }
  }

  private final class ImageLayoutStretchedNoAspect extends ImageHolder
  {
    public ImageLayoutStretchedNoAspect (Image image, int layout, int alignment)
    {
      super (image, layout, alignment);
    }

    @Override
    public void updateRect (Dimension displaySize)
    {
      rect.x = 0;
      rect.y = 0;
      rect.width = displaySize.width;
      rect.height = displaySize.height;
    }
  }

  private final class ImageLayoutStretchedAspect extends ImageHolder
  {
    public ImageLayoutStretchedAspect (Image image, int layout, int alignment)
    {
      super (image, layout, alignment);
    }

    @Override
    public void updateRect (Dimension displaySize)
    {
      double imageAspectRatio, screenAspectRatio;
      int newWidth, newHeight;

      imageAspectRatio = (double)imageSize.width / (double)imageSize.height;
      screenAspectRatio = displaySize.getWidth () / displaySize.getHeight ();

      if (imageAspectRatio < screenAspectRatio)
      {
        newWidth = (int)(imageSize.width /
                         ((double)imageSize.height / (double)displaySize.height));
        newHeight = displaySize.height;
      } else
      {
        newWidth = displaySize.width;
        newHeight = (int)(imageSize.height /
                          ((double)imageSize.width / displaySize.width));
      }

      Point location = Geometry.offsetForAligment
        (displaySize, new Dimension (newWidth, newHeight), getAlignment());

      rect.x = location.x;
      rect.y = location.y;
      rect.width = newWidth;
      rect.height = newHeight;
    }
  }

  /**
   * The reason we have left alot of the this code commented out, is because it took
   * so long to work out. The reason we don't use it, is because it turns out to be
   * alot slower than the original code. We are hoping that maybe in future versions
   * of JDK they can speed it up.
   */

//  // This method returns a buffered image with the contents of an image
//  public static BufferedImage toBufferedImage(Image image) {
//      if (image instanceof BufferedImage) {
//          return (BufferedImage)image;
//      }
//
//      // Determine if the image has transparent pixels; for this method's
//      // implementation, see e665 Determining If an Image Has Transparent Pixels
//      boolean hasAlpha = hasAlpha(image);
//
//      // Create a buffered image with a format that's compatible with the screen
//      BufferedImage bimage = null;
//      GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
//      try {
//          // Determine the type of transparency of the new buffered image
//          int transparency = Transparency.OPAQUE;
//          if (hasAlpha) {
//              transparency = Transparency.BITMASK;
//          }
//
//          // Create the buffered image
//          GraphicsDevice gs = ge.getDefaultScreenDevice();
//          GraphicsConfiguration gc = gs.getDefaultConfiguration();
//          bimage = gc.createCompatibleImage(
//              image.getWidth(null), image.getHeight(null), transparency);
//      } catch (HeadlessException e) {
//          // The system does not have a screen
//      }
//
//      if (bimage == null) {
//          // Create a buffered image using the default color model
//          int type = BufferedImage.TYPE_INT_RGB;
//          if (hasAlpha) {
//              type = BufferedImage.TYPE_INT_ARGB;
//          }
//          bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), type);
//      }
//
//      // Copy image to buffered image
//      Graphics g = bimage.createGraphics();
//
//      // Paint the image onto the buffered image
//      g.drawImage(image, 0, 0, null);
//      g.dispose();
//
//      return bimage;
//  }

//  // This method returns true if the specified image has transparent pixels
//  public static boolean hasAlpha(Image image) {
//      // If buffered image, the color model is readily available
//      if (image instanceof BufferedImage) {
//          BufferedImage bimage = (BufferedImage)image;
//          return bimage.getColorModel().hasAlpha();
//      }
//
//      // Use a pixel grabber to retrieve the image's color model;
//      // grabbing a single pixel is usually sufficient
//       PixelGrabber pg = new PixelGrabber(image, 0, 0, 1, 1, false);
//      try {
//          pg.grabPixels();
//      } catch (InterruptedException e) {
//      }
//
//      // Get the image's color model
//      ColorModel cm = pg.getColorModel();
//      return cm.hasAlpha();
//  }

  private final class ImageLayoutTiled extends ImageHolder
  {
//    TexturePaint texturePaint;
    public ImageLayoutTiled (Image image, int layout, int alignment)
    {
      super (image, layout, alignment);
//      Rectangle2D anchor = new Rectangle2D.Float(0,0, image.getWidth(this), image.getHeight(this));
//      BufferedImage bufferedImage = toBufferedImage(image);
//      texturePaint = new TexturePaint(bufferedImage, anchor);
    }

    @Override
    public void updateRect (Dimension displaySize)
    {
      rect.x = 0;
      rect.y = 0;
      rect.width = displaySize.width;
      rect.height = displaySize.height;
    }

    @Override
    public void paint (Graphics2D g,
                       Dimension displaySize)
    {
//      Paint oldPaint = g.getPaint();
//      g.setPaint(texturePaint);
//      g.fillRect(0,0, (int)displaySize.getWidth(), (int)displaySize.getHeight());
//      g.setPaint(oldPaint);
      int horizPics, vertPics;
      int horizIdx, vertIdx, newStartX, newStartY;

      // We need to add 0.5 here so that we make sure it rounds up and not down.
      horizPics = (int)Math.round ((displaySize.getWidth () / imageSize.getWidth ()) + 0.5);
      vertPics = (int)Math.round ((displaySize.getHeight () / imageSize.getHeight ()) + 0.5);

      for (horizIdx = 0; horizIdx < horizPics; horizIdx++)
      {
        newStartX = horizIdx * imageSize.width;

        for (vertIdx = 0; vertIdx < vertPics; vertIdx++)
        {
          newStartY = vertIdx * imageSize.height;

          g.drawImage (image, newStartX, newStartY,
                       imageSize.width, imageSize.height, this);
        }
      }
    }
  }

  /**
   * This is used to store the layout and the alignment in
   */

  public final static class LayoutEnum extends IconicEnumerationValue
  {
    private int layout, alignment;

    protected LayoutEnum (int layout, int alignment, String desc, Icon icon)
    {
      super (desc, icon);
      this.layout = layout;
      this.alignment = alignment;
    }

    public int getLayout ()
    {
      return layout;
    }

    public int getAlignment ()
    {
      return alignment;
    }

    @Override
    public EnumerationValue [] getEnumValues ()
    {
      return ENUM_VALUES;
    }

    @Override
    protected Object readResolve () throws InvalidObjectException
    {
      return super.readResolve();
    }
  }
}
