package dsto.dfc.swing.controls;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JSeparator;

/**
 * Similar to JSeparator, but displays some title text to the left.
 * 
 * @author Matthew Phillips
 * @version $Revision$
 */
public class JTitledSeparator extends JComponent
{
  private String title;
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private JLabel label = new JLabel();
  private JSeparator separator = new JSeparator();

  public JTitledSeparator()
  {
    setTitle ("Titled Separator");
    try
    {
      jbInit ();
    } catch (Exception e)
    {
      e.printStackTrace ();
    }

    label.setFont (label.getFont ().deriveFont (Font.BOLD));
  }

  private void jbInit () throws Exception
  {
    label.setForeground(new Color(0, 0, 144));
    label.setText ("Titled Separator");

    this.setLayout(gridBagLayout1);
    this.add(label, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    this.add(separator, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(3, 3, 0, 0), 0, 0));
  }

  public void setTitle (String newTitle)
  {
    String oldTitle = title;
    title = newTitle;
    label.setText (title);
    firePropertyChange ("title", oldTitle, newTitle);
  }

  public String getTitle ()
  {
    return title;
  }
}
