package dsto.dfc.swing.controls;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.io.Serializable;

import javax.swing.Icon;

import dsto.dfc.util.Copyable;

/**
 * An icon that displays a rectangular coloured swatch.
 *
 * @version $Revision$
 */
public class ColourSwatchIcon implements Icon, Serializable, Copyable
{
  protected int width;
  protected int height;
  protected Color colour;

  public ColourSwatchIcon ()
  {
    this (null);
  }

  public ColourSwatchIcon (Color colour)
  {
    this (colour, 16, 16);
  }

  public ColourSwatchIcon (Color colour, int width, int height)
  {
    this.width = width;
    this.height = height;
    this.colour = colour;
  }

  public void setColor (Color colour)
  {
    this.colour = colour;
  }

  public void paintIcon (Component c, Graphics g, int x, int y)
  {
    g.setColor (Color.black);
    g.drawRect (x, y, width - 1, height - 1);

    if (colour != null)
      g.setColor (colour);
    else
      g.setColor (c.getBackground ());

    g.fillRect (x + 1, y + 1, width - 2, height - 2);
  }

  public int getIconWidth ()
  {
    return width;
  }

  public int getIconHeight ()
  {
    return height;
  }

  public Object clone () throws CloneNotSupportedException
  {
    return super.clone ();
  }
}
