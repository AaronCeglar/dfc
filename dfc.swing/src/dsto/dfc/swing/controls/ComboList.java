package dsto.dfc.swing.controls;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Vector;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * A Windows-style combo list: like a combo box but list is permanently
 * displayed, not a popup.
 *
 * @version $Revision$ 
 */
public class ComboList extends JPanel
  implements ListSelectionListener, ListDataListener, ActionListener, FocusListener
{
  private JTextField textField = new JTextField();
  private JScrollPane scrollPane = new JScrollPane();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private JList list = new JList();
  private ComboBoxModel model;
  private transient Vector actionListeners;

  public ComboList (Object [] items)
  {
    this (new DefaultComboBoxModel (items));
  }

  public ComboList ()
  {
    this (new DefaultComboBoxModel ());
  }

  public ComboList (ComboBoxModel model)
  {
    list.addListSelectionListener (this);
    textField.addActionListener (this);
    textField.addFocusListener (this);
    this.addFocusListener (this);

    setModel (model);

    try
    {
      jbInit();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public void setModel (ComboBoxModel newModel)
  {
    ComboBoxModel oldModel = model;

    model = newModel;

    if (oldModel != null)
      oldModel.removeListDataListener (this);

    list.setModel (newModel);

    if (newModel.getSelectedItem () != null)
      textField.setText (newModel.getSelectedItem ().toString ());
    else
      textField.setText ("");

    newModel.addListDataListener (this);
  }

  public void setEntries (String [] list)
  {
    setModel (new DefaultComboBoxModel (list));
  }

  public String [] getEntries ()
  {
    String [] entries = new String [model.getSize ()];

    try
    {
      for (int i = 0; i < model.getSize (); i++)
      {
        entries [i] = (String)model.getElementAt (i);
      }
    } catch (ClassCastException ex)
    {
      return new String [0];
    }

    return entries;
  }

  public void setListData (Object [] data)
  {
    setModel (new DefaultComboBoxModel (data));
  }

  public Object getSelectedItem ()
  {
    return model.getSelectedItem ();
  }

  public void setSelectedItem (Object item)
  {
    model.setSelectedItem (item);
  }

  public void setVisibleRowCount (int visibleRowCount)
  {
    list.setVisibleRowCount (visibleRowCount);
  }

  public int getVisibleRowCount ()
  {
    return list.getVisibleRowCount ();
  }

  // FocusListener interface

  public void focusGained (FocusEvent e)
  {
    if (e.getSource () == this)
      textField.requestFocus ();
  }

  public void focusLost (FocusEvent e)
  {
    model.setSelectedItem (textField.getText ());
  }

  // ActionListener interface
  
  public void actionPerformed (ActionEvent e)
  {
    model.setSelectedItem (textField.getText ());
  }

  // ListSelectionListener interface
  
  public void valueChanged (ListSelectionEvent e)
  {
    model.setSelectedItem (list.getSelectedValue ());
  }

  // ListDataModelListener interface

  public void intervalAdded (ListDataEvent e)
  {
    // zip
  }

  public void intervalRemoved (ListDataEvent e)
  {
    // zip
  }

  public void contentsChanged (ListDataEvent e)
  {
    textField.setText (model.getSelectedItem ().toString ());
    fireActionPerformed (new ActionEvent (this, ActionEvent.ACTION_PERFORMED, ""));
  }

  protected void fireActionPerformed (ActionEvent e)
  {
    if(actionListeners != null)
    {
      Vector listeners = actionListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((ActionListener) listeners.elementAt(i)).actionPerformed(e);
      }
    }
  }

  private void jbInit() throws Exception
  {
    this.setLayout(gridBagLayout1);
    textField.setMargin(new Insets(2, 2, 2, 2));
    textField.setColumns(8);
    list.setMinimumSize(new Dimension(100, 80));
    this.add(textField, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
    this.add(scrollPane, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(2, 10, 0, 0), 0, 0));
    scrollPane.getViewport().add(list, null);
  }

  public synchronized void removeActionListener(ActionListener l)
  {
    if(actionListeners != null && actionListeners.contains(l))
    {
      Vector v = (Vector) actionListeners.clone();
      v.removeElement(l);
      actionListeners = v;
    }
  }

  public synchronized void addActionListener(ActionListener l)
  {
    Vector v = actionListeners == null ? new Vector(2) : (Vector) actionListeners.clone();
    if(!v.contains(l))
    {
      v.addElement(l);
      actionListeners = v;
    }
  }
}
