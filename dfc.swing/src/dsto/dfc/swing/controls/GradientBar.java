package dsto.dfc.swing.controls;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JComponent;

/**
 * The GradientBar is a simple JComponent which represents the {@link dsto.dfc.swing.controls.Gradient}
 *
 * It has a default preferred size of 50x15.
 *
 * This fires a PropertyChange when ever the {@link dsto.dfc.swing.controls.Gradient} is
 * replaced.
 */

public class GradientBar extends JComponent implements PropertyChangeListener
{
  private Gradient gradient;

  public GradientBar (float initialHue, float colourSpan,
                      float saturation, float brightness, boolean reverse)
  {
    this(new Gradient(initialHue, colourSpan, saturation, brightness, reverse));
  }

  public GradientBar (Gradient gradient)
  {
    this.gradient = gradient;
    gradient.addPropertyChangeListener(this);
  }

  public Dimension getPreferredSize ()
  {
    return new Dimension(50, 15);
  }

  public void paint (Graphics g)
  {
    Graphics2D g2 = (Graphics2D)g;

    // This causes the graphics context to do Bi-Cubic interpolation on the
    // painted lines. ie it makes it look very pretty :-)
    g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                        RenderingHints.VALUE_INTERPOLATION_BICUBIC);

    int width = this.getWidth();
    int height = this.getHeight();


    // Now, we go through all the lines and work out what colour it should be according
    // to the {@link dsto.dfc.swing.controls.Gradient}. If the gradient specifies that it should be
    // reversed, then the lines are simply drawn from right to left, instead of left to
    // right.
    for (int i=0; i<width; i++)
    {
      float scale = (float)i/(float)width;
      float hue = gradient.getInitialHue() + (scale * gradient.getColourSpan());
      g2.setColor(new Color(Color.HSBtoRGB(hue , gradient.getSaturation(), gradient.getBrightness())));
      if (gradient.isReverse())
        g2.drawLine(width - i, 0, width - i, height);
      else
        g2.drawLine(i, 0, i, height);
    }
  }

  public Gradient getGradient ()
  {
    return gradient;
  }

  public void setGradient (Gradient newGradient)
  {
    Gradient oldGradient = this.gradient;
    this.gradient.removePropertyChangeListener(this);
    this.gradient = newGradient;
    this.gradient.addPropertyChangeListener(this);
    firePropertyChange("gradient", oldGradient, newGradient);
  }

  public void propertyChange(PropertyChangeEvent evt)
  {
    this.repaint();
  }

//  public static void main(String[] args)
//  {
//    GradientBar gradientBar = new GradientBar (0.1f, 0.1f, 1.0f, 1.0f, false);
//    JFrame frame = new JFrame();
//    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//    frame.getContentPane().add(gradientBar);
//    frame.validate();
//    frame.show();
//  }
}