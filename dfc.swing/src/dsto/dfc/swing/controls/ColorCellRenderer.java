package dsto.dfc.swing.controls;

import java.awt.Color;

import javax.swing.JComponent;
import javax.swing.JLabel;

/**
 * A generic cell renderer for AWT Color values.
 *
 * @version $Revision$
 */
public class ColorCellRenderer extends AbstractCellRenderer
{

  private ColourSwatchIcon icon = new ColourSwatchIcon ();

  /**
   * Get an english name for a color if defined, otherwise return "[R, G, B]"
   * form.
   */
  public static final String getColorName (Color color)
  {
    String colorText = ColorScheme.getNameColor (color);

    return colorText;
  }

  public JComponent getRendererComponent (JComponent client,
                                          Object value,
                                          int row, int column,
                                          boolean isSelected,
                                          boolean hasFocus,
                                          boolean expanded,
                                          boolean leaf)
  {
    JLabel label = getLabel ();

    if (value instanceof Color)
    {
      Color color = (Color)value;

      icon.setColor (color);
      label.setIcon (icon);
      label.setText (getColorName (color));
    }

    return label;
  }
}
