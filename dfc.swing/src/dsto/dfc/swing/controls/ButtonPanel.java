package dsto.dfc.swing.controls;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 * A panel for displaying a row of buttons using a ButtonLayout.
 *
 * @version $Revision$
 */
public class ButtonPanel extends JPanel
{
  ButtonLayout buttonLayout = new ButtonLayout();

  public ButtonPanel()
  {
    try
    {
      jbInit();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  private void jbInit () throws Exception
  {
    this.setLayout (buttonLayout);
    this.setBorder (BorderFactory.createEmptyBorder (5, 5, 5, 5));
  }

  public void setHGap (int hgap)
  {
    buttonLayout.setHGap (hgap);
  }

  public int getHGap ()
  {
    return buttonLayout.getHGap ();
  }

  public void setVertAlignment (int newVertAlignment)
  {
    buttonLayout.setVertAlignment (newVertAlignment);
  }

  public int getVertAlignment ()
  {
    return buttonLayout.getVertAlignment ();
  }

  public void setHorizAlignment (int newHorizAlignment)
  {
    buttonLayout.setHorizAlignment (newHorizAlignment);
  }

  public int getHorizAlignment ()
  {
    return buttonLayout.getHorizAlignment ();
  }
}

