package dsto.dfc.swing.controls;

import javax.swing.Icon;

import dsto.dfc.swing.icons.IconicFolder;
import dsto.dfc.swing.icons.ImageLoader;

/**
 * Utility base class for IconicFolder's.
 *
 * @version $Revision$
 */
public abstract class AbstractIconicFolder implements IconicFolder
{
  public static final Icon OPEN_ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/folder_open.gif");
  public static final Icon CLOSED_ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/folder_closed.gif");
  public static final Icon LARGE_OPEN_ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/folder_open_large.gif");
  public static final Icon LARGE_CLOSED_ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/folder_closed_large.gif");

  public Icon getOpenIcon ()
  {
    return OPEN_ICON;
  }

  public Icon getIcon ()
  {
    return CLOSED_ICON;
  }

  public Icon getLargeIcon ()
  {
    return LARGE_OPEN_ICON;
  }

  public Icon getLargeOpenIcon ()
  {
    return LARGE_CLOSED_ICON;
  }
  
  public abstract String getName ();
}
