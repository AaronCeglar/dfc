package dsto.dfc.swing.controls;

import java.util.EventObject;
import java.util.Vector;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.tree.TreeCellEditor;

import dsto.dfc.util.IllegalFormatException;

import dsto.dfc.swing.text.Convertable;
import dsto.dfc.swing.text.Text;
import dsto.dfc.swing.text.ValueConverter;

/**
 * Adapts a JTextField to the TableCellEditor and TreeCellEditor
 * interfaces.  Supports use of a ValueConverter and the
 * Objects.convertValue () method to provide auto data
 * conversion to/from text and the form used by the model.
 *
 * @version $Revision$
 */
public class JTextFieldEditorAdapter
  implements TableCellEditor, TreeCellEditor,
             ActionListener, FocusListener
{
  private JTextField textField;
  private Class valueClass;
  private ValueConverter converter;
  private boolean listening = false;
  private int clickCountToEdit = 2;
  private transient Object oldValue;
  private transient Vector cellEditorListeners;

  /**
   * Create an adapter with no data conversion (ie text is copied
   * directly to model).
   */
  public JTextFieldEditorAdapter (JTextField textField)
  {
    this (textField, String.class);
  }

	/**
   * Create an adapter that converts data from text field to values of
   * a given type.
   *
   * @param textField The text field to use for editing.
   * @param valueClass The class of value to return to the model.
   */
  public JTextFieldEditorAdapter (JTextField textField,
                                  Class valueClass)
  {
    this (textField, valueClass, null);
  }

  /**
   * Create an adapter that converts data from text field to values of
   * a given type.
   *
   * @param textField The text field to use for editing.
   * @param valueClass The class of value to return to the model.
   * @param converter The converter instance to use for conversion
   * to/from text and the model form.
   */
  public JTextFieldEditorAdapter (JTextField textField,
                                  Class valueClass,
                                  ValueConverter converter)
  {
    this.textField = textField;
    this.valueClass = valueClass;
    this.converter = converter;
  }

  public Class getValueClass ()
  {
    return valueClass;
  }

  public void setValueClass (Class newValueClass)
  {
    valueClass = newValueClass;
    converter = null;
  }

  public int getClickCountToEdit ()
  {
    return clickCountToEdit;
  }

  public void setClickCountToEdit (int newValue)
  {
    clickCountToEdit = newValue;
  }

  // ActionListener interface

  public void actionPerformed (ActionEvent e)
  {
    stopCellEditing ();
  }

  // FocusListener interface

  public void focusGained (FocusEvent e)
  {
    // zip
  }

  public void focusLost (FocusEvent e)
  {
    stopCellEditing ();
  }

  public Component startEditor (Object value)
  {
    if (!listening)
    {
      listening = true;

      textField.addActionListener (this);
      textField.addFocusListener (this);
    }

    try
    {
      ValueConverter c = converter;

      // if no converter set, and value is Convertable...
      if (c == null && value instanceof Convertable)
        c = ((Convertable)value).getConverter ();

      oldValue = value;
       
      textField.setText
        ((String)Text.convertValue (value, String.class, c));
    } catch (IllegalFormatException ex)
    {
      // fall back on good ol' toString ()

      textField.setText (value.toString ());
    }

    return textField;
  }

  // TableCellEditor and TreeCellEditor interfaces

  public Component getTableCellEditorComponent (JTable table, Object value,
                                                boolean isSelected,
                                                int row, int column)
  {
    return startEditor (value);
  }

  public Component getTreeCellEditorComponent (JTree tree, Object value,
                                               boolean isSelected,
                                               boolean expanded,
                                               boolean leaf, int row)
  {
    return startEditor (value);
  }

  public Object getCellEditorValue ()
  {
    try
    {
      ValueConverter c = converter;

      // if no converter set, and old value is Convertable...
      if (c == null && oldValue instanceof Convertable)
        c = ((Convertable)oldValue).getConverter ();

      return Text.convertValue (textField.getText (),
                                 valueClass, c);
    } catch (IllegalFormatException ex)
    {
      JOptionPane.showMessageDialog
        (textField, "Illegal format: " + ex.getMessage (), "Input Error",
         JOptionPane.WARNING_MESSAGE);

      return oldValue;
    }
  }

  public boolean isCellEditable (EventObject e)
  {
    if (e instanceof MouseEvent)
    {
      MouseEvent me = (MouseEvent)e;

      return me.getClickCount () == clickCountToEdit;
    } else
      return true;
  }

  public boolean shouldSelectCell (EventObject e)
  {
    return true;
  }

  public boolean stopCellEditing ()
  {
    if (listening)
    {
      listening = false;

      textField.removeActionListener (this);
      textField.removeFocusListener (this);
    }

    fireEditingStopped (new ChangeEvent (this));
    
    return true;
  }

  public void cancelCellEditing ()
  {
    if (listening)
    {
      listening = false;

      textField.removeActionListener (this);
      textField.removeFocusListener (this);
    }
    
    fireEditingCanceled (new ChangeEvent (this));
  }

  public synchronized void removeCellEditorListener (CellEditorListener l)
  {
    if (cellEditorListeners != null && cellEditorListeners.contains(l))
    {
      Vector v = (Vector) cellEditorListeners.clone();
      v.removeElement(l);
      cellEditorListeners = v;
    }
  }

  public synchronized void addCellEditorListener (CellEditorListener l)
  {
    Vector v = cellEditorListeners == null ? new Vector(2) : (Vector) cellEditorListeners.clone();
    if(!v.contains(l))
    {
      v.addElement(l);
      cellEditorListeners = v;
    }
  }

  protected void fireEditingCanceled (ChangeEvent e)
  {
    if(cellEditorListeners != null)
    {
      Vector listeners = cellEditorListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((CellEditorListener) listeners.elementAt(i)).editingCanceled(e);
      }
    }
  }

  protected void fireEditingStopped (ChangeEvent e)
  {
    if(cellEditorListeners != null)
    {
      Vector listeners = cellEditorListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((CellEditorListener) listeners.elementAt(i)).editingStopped(e);
      }
    }
  }
}
