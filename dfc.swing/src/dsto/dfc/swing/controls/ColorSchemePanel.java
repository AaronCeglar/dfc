package dsto.dfc.swing.controls;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JPanel;

/**
 * Displays a toggle button with ColorSwatchIcon for each color in the
 * ColorScheme that was passed to it. Collaborates with ColorChooserPanel to
 * display the different schemes.
 *
 * @author       Luke Marsh
 * @version      $Revision$
 *
 * @see ColorChooserPanel
 * @see ColorScheme
 * @see ColorChooserCombo
 */
public class ColorSchemePanel extends JPanel
{

  protected transient Vector actionListeners;
  protected GridLayout gridLayout1;
  protected ArrayList buttonList;
  protected ArrayList iconButtonListeners;
  protected ColorScheme colorScheme;
  protected Color selectedColor;

  /**
   * Initializes variables and calls jbInit ().
   */
  public ColorSchemePanel ()
  {
    selectedColor = Color.black;
    jbInit ();
    setScheme (ColorScheme.PASTELS);
  }


  /**
   * Unselectes the toggle buttons.
   */
  public void retoggle ()
  {
    for (int i = 0; i < buttonList.size (); i++)
    {
      ((ToolButton) buttonList.get (i)).setSelected (false);
    }
  }


  /**
   * Returns the current Scheme.
   */
  public ColorScheme getScheme ()
  {
    return colorScheme;
  }


  /**
   * Assigns the current scheme and loads it.
   */
  public void setScheme (ColorScheme colorScheme)
  {
    this.colorScheme = colorScheme;
    loadScheme ();
  }


  /**
   * Removes an actionListener from the list.
   */
  public synchronized void removeActionListener (ActionListener l)
  {
    if (actionListeners != null && actionListeners.contains (l))
    {
      Vector v = (Vector) actionListeners.clone ();
      v.removeElement (l);
      actionListeners = v;
    }
  }


  /**
   * Adds an actionListener to the list.
   */
  public synchronized void addActionListener (ActionListener l)
  {
    Vector v = actionListeners == null ? new Vector (2) : (Vector)
    actionListeners.clone ();
    if (!v.contains (l))
    {
      v.addElement (l);
      actionListeners = v;
    }
  }


  /**
   * Sets the "More Colors..." button's icon to the color
   * chosen in the JColorChooser.
   */
  public void setSelectedButton (Color color)
  {
    Color temp = Color.black;
    for (int i = 0; i < buttonList.size (); i++)
    {
      temp = ((IconButtonListener) iconButtonListeners.get (i)).getColor ();
      if (temp.equals (color))
      {
        ((ToolButton) buttonList.get (i)).setSelected (true);
      }
    }
  }


  /**
   * Sets the Color.
   */
  public void setColor (Color c)
  {
    selectedColor = c;
  }


  /**
   * Assigns the number of rows required to fit all of the buttons.
   * Removes buttons from the panel, then adds the new ones.
   */
  protected void loadScheme ()
  {
    ArrayList colors = new ArrayList (colorScheme.getColors ());
    ArrayList colorNames = new ArrayList (colorScheme.getColorNames ());

    if (colors.size () <= 5)
    {
      gridLayout1.setRows (1);
    }
    else if (colors.size () <= 10)
    {
      gridLayout1.setRows (2);
    }
    else if (colors.size () <= 15)
    {
      gridLayout1.setRows (3);
    }
    else
    {
      gridLayout1.setRows (4);
    }

    buttonList = new ArrayList ();
    iconButtonListeners = new ArrayList ();

    // Remove all the buttons contained in the CCP Panel, including the
    // actionListeners associated with the buttons.
    for (int i = 0; i < buttonList.size  (); i++)
    {
      ((ToolButton) buttonList.get (i)).removeActionListener
      ((IconButtonListener) iconButtonListeners.get (i));
    }
    removeAll ();

    for (int j = 0; j < colors.size (); j++)
    {
      buttonList.add (new ToolButton
      ("", new ColourSwatchIcon ((Color) colors.get (j), 14, 14)));
    }

    for (int k = 0; k < colors.size (); k++)
    {
      // Add buttons to the CCP Panel and gives them actionListeners.
      ToolButton toolButton = (ToolButton) buttonList.get (k);
      Color color = (Color) colors.get (k);
      String colorName = colorNames.get (k).toString ();
      toolButton.setToggle (true);
      toolButton.setToolTipText (colorName);
      IconButtonListener iBL = new IconButtonListener (color);
      iconButtonListeners.add (iBL);
      toolButton.addActionListener (iBL);
      this.add (toolButton);
    }
    repaint ();
  }


  /**
   * Fires action events to all listeners.
   */
  protected void fireActionPerformed (ActionEvent e)
  {
    if (actionListeners != null)
    {
      Vector listeners = actionListeners;
      int count = listeners.size ();
      for (int i = 0; i < count; i++)
      {
        ((ActionListener) listeners.elementAt (i)).actionPerformed (e);
      }
    }
  }


  /**
   * Sets the layout.
   */
  private void jbInit ()
  {
    gridLayout1 = new GridLayout ();
    gridLayout1.setColumns (5);
    this.setLayout (gridLayout1);
  }

  /*
   * Allow access by inner class.
   */
  @Override
  public void firePropertyChange (String propertyName,
                                  Object oldValue, Object newValue)
  {
    super.firePropertyChange (propertyName, oldValue, newValue);
  }

  /**
   * A listener class for all of the colored Icon buttons.
   */
  private class IconButtonListener implements ActionListener
  {

    private Color color;

    /**
     * Assigns the passed Color to the color variable.
     */
    public IconButtonListener (Color c)
    {
      color = c;
    }


    /**
     * When one of the colored icon buttons is clicked, it first deselects
     * every button except itself. It then assigns the chosen color to
     * selectedColor.
     */
    public void actionPerformed (ActionEvent e)
    {
      Color temp = Color.black;
      selectedColor = new Color (0, 1, 3);
      // set correct button selected
      for (int i = 0; i < buttonList.size (); i++)
      {
        temp = ((IconButtonListener) iconButtonListeners.get (i)).getColor ();
        if (!temp.equals (color))
        {
          ((ToolButton) buttonList.get (i)).setSelected (false);
        }
        else
        {
          if (!((ToolButton) buttonList.get (i)).isSelected ())
          {
            ((ToolButton) buttonList.get (i)).setSelected (true);
          }
        }
      }
      fireActionPerformed (new ActionEvent (this, ActionEvent.ACTION_PERFORMED, "Color Chosen"));
      firePropertyChange ("color", selectedColor, color);
      selectedColor = color;
    }


    /**
     * Returns the color of this icon.
     */
    protected Color getColor ()
    {
      return color;
    }
  }
}