package dsto.dfc.swing.controls;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultButtonModel;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import dsto.dfc.swing.forms.AbstractFormEditorComponent;
import dsto.dfc.swing.icons.ArrowIcon;
import dsto.dfc.util.IllegalFormatException;

/**
 * This is the main Panel. Contains a ColorSchemePanel and other components. It
 * is the User Interface for the Color Scheme Chooser that displays different
 * schemes of color as ToolButtons. It can be placed inside any Frame or other
 * Panel to be used. Whenever a color is selected a propertyChangeEvent is
 * fired with the property name "color".
 *
 * @author       Luke Marsh
 * @version      $Revision$
 *
 * @see ColorScheme
 * @see ColorSchemePanel
 * @see ColorChooserCombo
 */
public class ColorChooserPanel extends AbstractFormEditorComponent
  implements PropertyChangeListener
{

  protected int currentScheme;
  protected ToolButton colorsToolButton;
  protected JPanel titlePanel;
  protected ToolButton BackwardToolButton;
  protected ToolButton ForwardToolButton;
  protected JLabel colorSchemeName;
  protected ColorSchemePanel selectorPanel;
  protected GridBagLayout gridBagLayout1;
  protected BorderLayout borderLayout1;
  protected Color selectedColor;
  protected List colorschemeList;

  /**
   * Initializes variables and creates the predefined color schemes.
   */
  public ColorChooserPanel ()
  {
    colorschemeList = ColorScheme.DEFAULT_SCHEMES;
    jbInit ();
  }


  /**
   * Uses a user defined List of colorschemeList instead of the predefined
   * ones.
   */
  public ColorChooserPanel (List colorschemeList)
  {
    this.colorschemeList = colorschemeList;
    jbInit ();
  }


  /**
   * Return the List of colorschemeList.
   */
  public List getColorSchemeList ()
  {
    return colorschemeList;
  }


  /**
   * Assigns the event new value to selectedColor.
   */
  public void propertyChange (PropertyChangeEvent evt)
  {
    if (evt.getPropertyName ().equals ("color"))
    {
      setColor ((Color)evt.getNewValue ());
      fireEditCommitted ();
    }
  }


  /**
   * Return the chosen color.
   */
  public Color getColor ()
  {
    return selectedColor;
  }


  /**
   * Sets the selected color to c.
   */
  public void setColor (Color c)
  {
    Color oldColor = selectedColor = new Color (0, 1, 3);
    selectedColor = c;
    firePropertyChange ("color", oldColor, selectedColor);
  }


  /**
   * Sets the scheme to be loaded as the scheme the current color belongs to.
   */
  public void setScheme (int current)
  {
    currentScheme = current;
    displayNextScheme ("forward");
  }


  /**
   * Calls the ColorSchemePanel to set the selected color button.
   */
  public void setSelectedButton (Color color)
  {
    selectorPanel.setSelectedButton (color);
  }


  // FormEditor interface

  /**
   * Implements abstract method in dsto.dfc.swing.forms.AbstractFormEditor.
   * This control is not direct-editable.
   */
  public boolean isDirectEdit ()
  {
    return false;
  }


  /**
   * Implements abstract method in dsto.dfc.swing.forms.AbstractFormEditor.
   * Sets value to the current Color, and throws an exception if value is not
   * a real Color.
   */
  public void setEditorValue (Object value) throws IllegalFormatException
  {
    try
    {
      setColor ((Color) value);
    }
    catch (ClassCastException ex)
    {
      throw new IllegalFormatException (this, value, Color.class);
    }
  }


  /**
   * Implements abstract method in dsto.dfc.swing.forms.AbstractFormEditor.
   * Returns the current color.
   */
  public Object getEditorValue () throws IllegalFormatException
  {
    return getColor ();
  }


  /**
   * Implements abstract method in dsto.dfc.swing.forms.AbstractFormEditor.
   * Commit the selection.
   */
  public void commitEdits () throws IllegalFormatException
  {
    // zip
  }


  /**
   * Returns the instance of ColorSchemePanel.
   */
  public ColorSchemePanel getColorSchemePanel ()
  {
    return selectorPanel;
  }


  /**
   * Determines if the next or previous scheme should be displayed, and
   * Calls displayScheme (currentScheme) on the ColorSchemePanel.
   *
   * @param direction Can be backward or forward.
   */
  public void displayNextScheme (String direction)
  {
    if (direction.equals ("forward"))
    {
      currentScheme = (currentScheme + 1) % colorschemeList.size ();
      changeName (((ColorScheme) colorschemeList.get (currentScheme)).getName ());
      selectorPanel.setScheme ((ColorScheme) colorschemeList.get (currentScheme));
    }
    else if (direction.equals ("backward"))
    {
      if (currentScheme <= 0)
      {
        currentScheme = colorschemeList.size () - 1;
      }
      else
      {
        currentScheme--;
      }
      changeName (((ColorScheme) colorschemeList.get (currentScheme)).getName ());
      selectorPanel.setScheme ((ColorScheme) colorschemeList.get (currentScheme));
    }
  }


  /**
   * Sets the "More Colors..." button's icon to the color
   * chosen in the JColorChooser.
   */
  protected void setColorButton ()
  {
    colorsToolButton.setIcon (new ColourSwatchIcon (selectedColor, 14, 14));
  }


  /**
   * Disarms the "Other..." button and calls disarm () on the
   * ColorSchemePanel.
   */
  protected void disarm ()
  {
    DefaultButtonModel model = new DefaultButtonModel ();
    colorsToolButton.setModel (model);
    model.setArmed (false);
  }


  /**
   * Sets up layouts, adds components to main panel. It also adds
   * actionListeners and calls displayNextScheme ("forward") on the instance of
   * ColorSchemePanel.
   */
  private void jbInit ()
  {
    colorsToolButton = new ToolButton ("Other...", null);
    titlePanel = new JPanel ();

    ArrowIcon arrow = new ArrowIcon (2, 5);
    BackwardToolButton = new ToolButton ("", arrow);
    arrow = new ArrowIcon (3, 5);
    ForwardToolButton = new ToolButton ("", arrow);

    colorSchemeName = new JLabel ();
    gridBagLayout1 = new GridBagLayout ();
    selectorPanel = new ColorSchemePanel ();
    borderLayout1 = new BorderLayout ();
    selectedColor = Color.black;
    currentScheme = -1;

    this.setLayout (gridBagLayout1);

    float fontSize = colorSchemeName.getFont ().getSize2D();
    fontSize--;
    colorSchemeName.setFont (colorSchemeName.getFont ().deriveFont (fontSize));

    colorSchemeName.setToolTipText ("");
    colorSchemeName.setHorizontalAlignment (SwingConstants.CENTER);
    colorSchemeName.setHorizontalTextPosition (SwingConstants.CENTER);
    colorSchemeName.setText ("Scheme Name");
    titlePanel.setLayout (borderLayout1);

    fontSize = colorsToolButton.getFont ().getSize2D ();
    fontSize--;
    colorsToolButton.setFont (colorsToolButton.getFont ().deriveFont (fontSize));

    colorsToolButton.setBorder (null);
    colorsToolButton.setText ("Other...");
    colorsToolButton.setToggle (true);
    titlePanel.setBorder (BorderFactory.createEtchedBorder ());
    selectorPanel.setBorder (BorderFactory.createEtchedBorder ());
    this.add (colorsToolButton, new GridBagConstraints (0, 2, 1, 1, 1.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets (0, 0, 0, 0), 0, 0));

    this.add (titlePanel, new GridBagConstraints (0, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets (0, 0, 0, 0), 0, 0));

    titlePanel.add (BackwardToolButton, BorderLayout.WEST);
    titlePanel.add (colorSchemeName, BorderLayout.CENTER);
    titlePanel.add (ForwardToolButton, BorderLayout.EAST);

    this.add (selectorPanel, new GridBagConstraints (0, 1, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets (0, 0, 0, 0), 0, 0));

    colorsToolButton.addActionListener (new ColorButtonListener
    (selectorPanel));
    BackwardToolButton.addActionListener (new BackwardButtonListener ());
    ForwardToolButton.addActionListener (new ForwardButtonListener ());
    selectorPanel.addPropertyChangeListener (this);

    displayNextScheme ("forward");
  }


  /**
   * Changes the name of the colorSchemeLabel.
   */
  private void changeName (String name)
  {
    colorSchemeName.setText (name);
  }


  /**
   * Opens up a JColorChooser when the "More Colors..." button is pressed.
   */
  private class ColorButtonListener implements ActionListener
  {

    private boolean isSelected;
    private ColorSchemePanel CSP;

    /**
     * Assigns this classes parent to CCP.
     */
    public ColorButtonListener (ColorSchemePanel csp)
    {
      CSP = csp;
      isSelected = false;
      CSP.addActionListener (this);
    }


    /**
     * Opens a JColorChooser
     */
    public void actionPerformed (ActionEvent e)
    {
      if (e.getActionCommand ().equals ("Color Chosen"))
      {
        isSelected = false;
      }
      else
      {
        Color temp = JColorChooser.showDialog
          (selectorPanel, "Color Chooser", selectedColor);

        if (temp != null)
        {
          isSelected = true;
          setColor (temp);
          selectorPanel.setColor (temp);
          setColorButton ();
          fireEditCommitted ();

          if (!colorsToolButton.isSelected ())
          {
            colorsToolButton.setSelected (true);
          }

          selectorPanel.retoggle ();
        }
        else
        {
          colorsToolButton.setSelected (isSelected);
        }
      }
    }
  }

  /**
   * Displays the previous scheme.
   */
  private class BackwardButtonListener implements ActionListener
  {

    /**
     * Does Nothing.
     */
    public BackwardButtonListener ()
    {
      // zip
    }


    /**
     * Calls displayNextScheme ("backward") on the ColorSchemePanel class.
     */
    public void actionPerformed (ActionEvent e)
    {
      displayNextScheme ("backward");
    }
  }


  /**
   * Displays the next scheme.
   */
  private class ForwardButtonListener implements ActionListener
  {

    /**
     * Does Nothing.
     */
    public ForwardButtonListener ()
    {
      // zip
    }


    /**
     * Calls displayNextScheme ("forward") on the ColorSchemePanel class.
     */
    public void actionPerformed (ActionEvent e)
    {
      displayNextScheme ("forward");
    }
  }
}