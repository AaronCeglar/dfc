package dsto.dfc.swing.controls;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.io.Serializable;

import javax.swing.Icon;

import dsto.dfc.util.Copyable;

/**
 * An icon that displays a small rectangular coloured swatch under an
 * icon.  Can be used for icons like bucket fill that display the
 * current fill colour at the bottom.
 *
 * @version $Revision$
 */
public class CompositeColorSwatchIcon
  implements Icon, Serializable, Copyable
{
  protected static final int ICON_WIDTH = 16;
  protected static final int ICON_HEIGHT = 16;
  
  protected Color color;
  protected Icon smallIcon;

  /**
   * Create a new icon from a smaller icon and a colour.
   *
   * @param smallIcon The icon to be displayed above the swatch.  Must
   * be 16x12 or smaller (swatch is 16x4 pixels).
   * @param color The color to display in the swatch.
   */
  public CompositeColorSwatchIcon (Icon smallIcon, Color color)
  {
    setSmallIcon (smallIcon);
    setColor (color);
  }

  /**
   * Set the color for the swatch.  May be null to indicate swatch is
   * transparent.
   */
  public void setColor (Color color)
  {
    this.color = color;
  }

  /**
   * Set icon displayed at the top.  Should be 16x12 or smaller.
   */
  public void setSmallIcon (Icon smallIcon)
  {
    this.smallIcon = smallIcon; 
  }

  public Icon getSmallIcon ()
  {
    return smallIcon;
  }

  public void paintIcon (Component c, Graphics g, int x, int y)
  {
    // paint icon at top, centered horizontally 
    smallIcon.paintIcon (c, g,
                         x + (ICON_WIDTH - smallIcon.getIconWidth ()) / 2, y);

    // paint swatch    
    if (color != null)
      g.setColor (color);
    else
      g.setColor (c.getBackground ());

    g.fillRect (x, y + 12, ICON_WIDTH, 4);
  }

  public int getIconWidth ()
  {
    return ICON_WIDTH;
  }

  public int getIconHeight ()
  {
    return ICON_HEIGHT;
  }

  public Object clone () throws CloneNotSupportedException
  {
    return super.clone ();
  }
}
