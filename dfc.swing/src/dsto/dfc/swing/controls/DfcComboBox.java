package dsto.dfc.swing.controls;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.util.EventObject;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.tree.TreeCellEditor;

import dsto.dfc.swing.icons.IconicCellRenderer;

/**
 * Simple extension to JComboBox, adds auto renderer selection and
 * ability to use as a cell editor in tables and trees.
 *
 * @version $Revision$
 */
public class DfcComboBox
  extends JComboBox implements TableCellEditor, TreeCellEditor
{
  private transient Vector cellEditorListeners;

  public DfcComboBox ()
  {
    super ();
    setRenderer (new IconicCellRenderer ());

    Listener listener = new Listener ();
    addActionListener (listener);
    addFocusListener (listener);
  }

  public DfcComboBox (Object[] items)
  {
    super(items);
    setRenderer (new IconicCellRenderer ());

    Listener listener = new Listener ();
    addActionListener (listener);
    addFocusListener (listener);
  }

  // TableCellEditor and TreeCellEditor interfaces

  public Component getTableCellEditorComponent (JTable table, Object value,
                                                boolean isSelected,
                                                int row, int column)
  {
    setSelectedItem (value);

    return this;
  }

  public Component getTreeCellEditorComponent (JTree tree, Object value,
                                               boolean isSelected,
                                               boolean expanded,
                                               boolean leaf, int row)
  {
    setSelectedItem (value);

    return this;
  }

  public Object getCellEditorValue ()
  {
    return getSelectedItem ();
  }

  public boolean isCellEditable (EventObject e)
  {
    if (e instanceof MouseEvent)
      return ((MouseEvent)e).getClickCount () > 1;
    else
      return true;
  }

  public boolean shouldSelectCell (EventObject e)
  {
    return true;
  }

  public boolean stopCellEditing ()
  {
    fireEditingStopped (new ChangeEvent (this));

    return true;
  }

  public void cancelCellEditing ()
  {
    fireEditingCanceled (new ChangeEvent (this));
  }

  public synchronized void removeCellEditorListener (CellEditorListener l)
  {
    if (cellEditorListeners != null && cellEditorListeners.contains(l))
    {
      Vector v = (Vector) cellEditorListeners.clone();
      v.removeElement(l);
      cellEditorListeners = v;
    }
  }

  public synchronized void addCellEditorListener (CellEditorListener l)
  {
    Vector v = cellEditorListeners == null ? new Vector(2) : (Vector) cellEditorListeners.clone();
    if(!v.contains(l))
    {
      v.addElement(l);
      cellEditorListeners = v;
    }
  }

  protected void fireEditingCanceled (ChangeEvent e)
  {
    if(cellEditorListeners != null)
    {
      Vector listeners = cellEditorListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((CellEditorListener) listeners.elementAt(i)).editingCanceled(e);
      }
    }
  }

  protected void fireEditingStopped (ChangeEvent e)
  {
    if(cellEditorListeners != null)
    {
      Vector listeners = cellEditorListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((CellEditorListener) listeners.elementAt(i)).editingStopped(e);
      }
    }
  }

  class Listener
    implements ActionListener, FocusListener
  {
    public void actionPerformed (ActionEvent e)
    {
      stopCellEditing ();
    }

    public void focusGained (FocusEvent e)
    {
      // zip
    }

    public void focusLost (FocusEvent e)
    {
      // NOTE: this is not getting called for some reason: see bug 0000082
      stopCellEditing ();
    }
  }
}
