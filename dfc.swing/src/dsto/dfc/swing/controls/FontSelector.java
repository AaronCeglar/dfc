package dsto.dfc.swing.controls;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * A font selector panel.
 *
 * @see dsto.dfc.swing.forms.FontSelectorEditor
 *
 * @version $Revision$
 */
public class FontSelector extends JPanel
{
  public static final String [] SIZES =
    new String [] {"6", "8", "10", "12", "14", "16", "18", "20",
                   "22", "24", "28", "32", "36", "40"};

  public static final String [] STYLES =
    new String [] {"Regular", "Italic", "Bold", "Bold Italic"};

  private Font font;
  private boolean loading = false;
  private JLabel previewLabel = null;
  private JLabel fontLabel = new JLabel();
  private JLabel styleLabel = new JLabel();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private ComboList fontList = new ComboList ();
  private ComboList styleList = new ComboList ();
  private JLabel sizeLabel = new JLabel ();
  private ComboList sizeList = new ComboList ();

  public FontSelector()
  {
    sizeList.setListData (SIZES);
    styleList.setListData (STYLES);
    fontList.setListData
      (GraphicsEnvironment.getLocalGraphicsEnvironment ().getAvailableFontFamilyNames ());

    setSelectedFont (getFont ());

    try
    {
      jbInit ();
    } catch (Exception ex)
    {
      ex.printStackTrace ();
    }
  }

  public void setSelectedFont (Font newValue)
  {
    Font oldValue = font;

    font = newValue;

    loadToFields ();

    if (previewLabel != null)
    {
      // The following is a bizarre workaround hack for a known bug
      // (4209476) in JDK's up to and including 1.2.2.  If the same
      // family and size are used to create 2 fonts, the second font
      // ignores the style info: the formula below forces the size to
      // change in an invisible way.
      Font dummyFont = font.deriveFont ((float) (font.getSize2D () + 0.0001 * font.getStyle ()));
      previewLabel.setFont (dummyFont);
    }

    firePropertyChange ("selectedFont", oldValue, newValue);
  }

  public Font getSelectedFont ()
  {
    return font;
  }

  public void setPreviewLabel (JLabel label)
  {
    previewLabel = label;

    if (previewLabel != null)
    {
      previewLabel.setFont (getSelectedFont ());
    }
  }

  public JLabel getPreviewLabel ()
  {
    return previewLabel;
  }

  protected void loadToFields ()
  {
    loading = true;

    String fontFamily = font.getFamily ();
    fontList.setSelectedItem (fontFamily);

    if (font.getStyle () == Font.BOLD)
      styleList.setSelectedItem ("Bold");
    else if (font.getStyle () == Font.ITALIC)
      styleList.setSelectedItem ("Italic");
    else if (font.getStyle () == (Font.BOLD | Font.ITALIC))
      styleList.setSelectedItem ("Bold Italic");
    else
      styleList.setSelectedItem ("Regular");

    sizeList.setSelectedItem (Integer.toString (font.getSize ()));

    loading = false;
  }

  protected void loadFromFields ()
  {
    String familyStr = (String)fontList.getSelectedItem ();
    String styleStr = (String)styleList.getSelectedItem ();
    String sizeStr = (String)sizeList.getSelectedItem ();

    // style
    int style = Font.PLAIN;
    if (styleStr.equalsIgnoreCase ("Bold"))
      style = Font.BOLD;
    else if (styleStr.equalsIgnoreCase ("Italic"))
      style = Font.ITALIC;
    else if (styleStr.equalsIgnoreCase ("Bold Italic"))
      style = (Font.ITALIC | Font.BOLD);

    // size
    int size;

    try
    {
      size = Integer.parseInt (sizeStr);
    } catch (NumberFormatException ex)
    {
      size = 12;
    }

    Font newFont = new Font (familyStr, style, size);

    setSelectedFont (newFont);
  }

  private void jbInit () throws Exception
  {
    fontLabel.setDisplayedMnemonic('F');
    fontLabel.setLabelFor(fontList);
    fontLabel.setText("Font:");
    this.setLayout(gridBagLayout1);
    styleLabel.setDisplayedMnemonic('Y');
    styleLabel.setLabelFor(styleList);
    styleLabel.setText("Style:");
    fontList.setMinimumSize(new Dimension(150, 100));
    fontList.setVisibleRowCount(5);
    fontList.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        list_actionPerformed();
      }
    });
    styleList.setMinimumSize(new Dimension(100, 100));
    styleList.setVisibleRowCount(5);
    styleList.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        list_actionPerformed();
      }
    });
    sizeLabel.setDisplayedMnemonic('S');
    sizeLabel.setLabelFor(sizeList);
    sizeLabel.setText("Size:");
    sizeList.setMinimumSize(new Dimension(80, 100));
    sizeList.setVisibleRowCount(5);
    sizeList.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        list_actionPerformed();
      }
    });
    this.add(fontLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    this.add(styleLabel, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    this.add(fontList, new GridBagConstraints(0, 1, 1, 1, 0.5, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 0, 0, 5), 0, 0));
    this.add(styleList, new GridBagConstraints(2, 1, 1, 1, 0.3, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 0, 0, 5), 0, 0));
    this.add(sizeLabel, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    this.add(sizeList, new GridBagConstraints(3, 1, 1, 1, 0.2, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 0, 0, 0), 0, 0));
  }

  void list_actionPerformed ()
  {
    if (!loading)
    {
      loading = true;

      loadFromFields ();

      loading = false;
    }
  }
}