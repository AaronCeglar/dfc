package dsto.dfc.swing.controls;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.border.Border;
import javax.swing.plaf.basic.BasicButtonUI;

/**
 * The UI for {@link ToolButton} in all L&F schemes.
 *
 * @author Matthew Phillips
 * @version $Revision$
 *
 * @see ToolButton
 */
public class ToolButtonUI extends BasicButtonUI
{
//  protected static final Border MARGIN =
//    BorderFactory.createEmptyBorder (2, 2, 2, 2);
//
//  protected static final Border ROLLOVER_BORDER =
//    BorderFactory.createCompoundBorder
//      (new ThinBevelBorder (ThinBevelBorder.RAISED), MARGIN);
//
//  protected static final Border PRESSED_BORDER =
//    BorderFactory.createCompoundBorder
//      (new ThinBevelBorder (ThinBevelBorder.LOWERED), MARGIN);
//
//  protected static final Border NORMAL_BORDER =
//    BorderFactory.createEmptyBorder (4, 4, 4, 4);

  protected Border normalBorder = null;
  protected Border pressedBorder = null;
  protected Border rolloverBorder = null;

  /** Whether showing the button as pressed. */
  protected boolean showPressed = false;

  public ToolButtonUI ()
  {
    defaultTextShiftOffset = 1;
  }

  protected void updateBorder (Insets margin)
  {
    rolloverBorder =
      BorderFactory.createCompoundBorder
        (new ThinBevelBorder (ThinBevelBorder.RAISED),
         BorderFactory.createEmptyBorder
           (margin.top, margin.left, margin.bottom, margin.right));

    pressedBorder =
      BorderFactory.createCompoundBorder
        (new ThinBevelBorder (ThinBevelBorder.LOWERED),
         BorderFactory.createEmptyBorder (margin.top, margin.left,
                                          margin.bottom, margin.right));

    normalBorder =
      BorderFactory.createEmptyBorder
        (margin.top + 2, margin.left + 2, margin.bottom + 2, margin.right + 2);
  }

  /**
   * Override BasicButtonUI.paint () to show pressed and rollover
   * effects.
   */
  public void paint (Graphics g, JComponent c)
  {
    ToolButton button = (ToolButton)c;
    ButtonModel model = button.getModel ();

    if (normalBorder == null)
      updateBorder (button.getMargin ());

    // pressed if in toggle mode and button selected OR pressed and
    // rollover
    showPressed =
      (button.isToggle () && model.isSelected ()) ||
      (model.isPressed () && model.isRollover ());

    // fill background with lighter colour for pressed toggle button
    if (showPressed && button.isToggle ())
    {
      g.setColor (brighten (button.getBackground ()));
      g.fillRect (0, 0, button.getWidth (), button.getHeight ());
    }

    super.paint (g, c);

    // draw appopriate border
    Border border = getBorder (button);

    paintBorder (g, button, border);
  }

  protected Border getBorder (ToolButton button)
  {
    ButtonModel model = button.getModel ();

    showPressed =
      (button.isToggle () && model.isSelected ()) ||
      (model.isPressed () && model.isRollover ());

    if (showPressed)
      return pressedBorder;
    else if (model.isRollover ())
      return rolloverBorder;
    else
      return normalBorder;
  }

  /**
   * Need to override since BasicButtonUI.paintBorder () does not
   * respect textShiftOffset unless it thinks button is pressed.  This
   * is a slightly hacked version of the source from JDK 1.3.
   */
  protected void paintIcon (Graphics g, JComponent c, Rectangle iconRect)
  {
    AbstractButton b = (AbstractButton) c;
    ButtonModel model = b.getModel();
    Icon icon = b.getIcon();
    Icon tmpIcon = null;

    if (icon == null)
      return;

    if (!model.isEnabled ())
    {
      if (model.isSelected())
        tmpIcon = b.getDisabledSelectedIcon ();
      else
        tmpIcon = b.getDisabledIcon ();
    } else if(model.isPressed() && model.isArmed())
    {
      tmpIcon = b.getPressedIcon ();
    } else if (b.isRolloverEnabled() && model.isRollover())
    {
      if (model.isSelected())
        tmpIcon = b.getRolloverSelectedIcon();
      else
        tmpIcon = b.getRolloverIcon();
    } else if (model.isSelected())
    {
      tmpIcon = b.getSelectedIcon ();
    }

    if (tmpIcon != null)
      icon = tmpIcon;

    icon.paintIcon (c, g, iconRect.x + getTextShiftOffset (),
                    iconRect.y + getTextShiftOffset ());
  }

  protected void paintBorder (Graphics g, ToolButton button, Border border)
  {
    border.paintBorder (button, g, 0, 0,
                        button.getWidth (), button.getHeight ());
  }

  protected void paintButtonPressed (Graphics g, AbstractButton b)
  {
    setTextShiftOffset ();
  }

  /**
   * Override to prevent clearing offset when in pressed mode.  This
   * jiggers BasicButtonUI.paint into working correctly.
   */
  protected void clearTextShiftOffset ()
  {
    if (!showPressed)
      super.clearTextShiftOffset ();
    else
    {
      defaultTextShiftOffset = 1;
      setTextShiftOffset ();
    }
  }

  /**
   * Brighten a given colour by 10%.
   */
  protected static Color brighten (Color c)
  {
    float [] hsb =
      Color.RGBtoHSB (c.getRed (), c.getGreen (), c.getBlue (), null);

    return new Color (Color.HSBtoRGB (hsb [0], hsb [1], hsb [2] * 1.1f));
  }
}
