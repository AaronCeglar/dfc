package dsto.dfc.swing.controls;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import dsto.dfc.swing.forms.FormPanel;
import dsto.dfc.util.IllegalFormatException;

/**
 * This is a panel which can be used to customize a {@link dsto.dfc.swing.controls.Gradient}.
 * It contains a 4 slider bars, each representing the Initial Hue (colour), Colour Width,
 * Colour Saturation, or Colour Brightness.
 * It can also contain a checkbox, for the user to decide if they want the colour spectrum
 * reversed.
 */

public class GradientPanel extends FormPanel implements ChangeListener, ItemListener, PropertyChangeListener
{
  private boolean reversable;
  private GradientBar gradientBar;
  private JSlider initialHueSlider;
  private JSlider colourSpanSlider;
  private JSlider saturationSlider;
  private JSlider brightnessSlider;
  private JCheckBox reverseButton;
  BorderLayout borderLayout = new BorderLayout();
  JPanel sliderPanel = new JPanel();
  GridBagLayout gridBagLayout1 = new GridBagLayout();

  /**
   * @param isReversable If you want the colour spectrum reversed,
   *          then set this to true.
   */
  public GradientPanel (boolean isReversable)
  {
    try
    {
      jbInit ();
    } catch (Exception e)
    {
      e.printStackTrace ();
    }

    this.reversable = isReversable;
    gradientBar.addPropertyChangeListener ("gradient", this);
  }

  private void jbInit ()
  {
    sliderPanel.setLayout(borderLayout);

    gradientBar = new GradientBar (0.1f, 0.1f, 1.0f, 1.0f, false);
    initialHueSlider = new JSlider (0, 100, (int)(gradientBar.getGradient().getInitialHue()*100.0f));
    colourSpanSlider = new JSlider (0, 100, (int)(gradientBar.getGradient().getColourSpan()*100.0f));
    saturationSlider = new JSlider (0, 100, (int)(gradientBar.getGradient().getSaturation()*100.0f));
    brightnessSlider = new JSlider (0, 100, (int)(gradientBar.getGradient().getBrightness()*100.0f));

    initialHueSlider.setOrientation(JSlider.HORIZONTAL);
    colourSpanSlider.setOrientation(JSlider.HORIZONTAL);
    saturationSlider.setOrientation(JSlider.VERTICAL);
    brightnessSlider.setOrientation(JSlider.VERTICAL);

    initialHueSlider.setToolTipText("Initial colour");
    colourSpanSlider.setToolTipText("Colour width");
    saturationSlider.setToolTipText("Saturation of colour");
    brightnessSlider.setToolTipText("Brightness of colour");

    reverseButton = new JCheckBox();

    reverseButton.setSelected(gradientBar.getGradient().isReverse());

    initialHueSlider.addChangeListener(this);
    colourSpanSlider.addChangeListener(this);
    saturationSlider.addChangeListener(this);
    brightnessSlider.addChangeListener(this);
    reverseButton.addItemListener(this);

    reverseButton.setText("Reverse Colour : ");
    reverseButton.setHorizontalTextPosition(SwingConstants.LEADING);

    this.setLayout(gridBagLayout1);
    sliderPanel.add(initialHueSlider, BorderLayout.NORTH);
    sliderPanel.add(saturationSlider, BorderLayout.EAST);
    sliderPanel.add(brightnessSlider, BorderLayout.WEST);
    sliderPanel.add(colourSpanSlider, BorderLayout.SOUTH);
    sliderPanel.add(gradientBar, BorderLayout.CENTER);

    if (reversable)
      this.add(reverseButton, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.1
            ,GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL, new Insets(0, 6, 6, 6), 0, 0));


    this.add(sliderPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.9
            ,GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(6, 6, 6, 6), 0, 0));

  }

  /**
   * Overriding the {@link dsto.dfc.swing.forms.FormPanel} method so that we can update
   * the {@link dsto.dfc.swing.controls.GradientBar} with the new values.
   */
  public void setEditorValue(Object value) throws dsto.dfc.util.IllegalFormatException
  {
    if (value instanceof Gradient)
      gradientBar.setGradient((Gradient)value);
    else
      throw new IllegalFormatException(this, value, Gradient.class);
  }

  public Object getEditorValue ()
  {
    return gradientBar.getGradient();
  }

  // PropertyChangeListener interface. (For when the the {@link dsto.dfc.swing.controls.Gradient}
  // gets changed.
  public void propertyChange(PropertyChangeEvent evt)
  {
    reverseButton.setSelected(gradientBar.getGradient().isReverse());
    colourSpanSlider.setValue((int)(gradientBar.getGradient().getColourSpan()*100.0f));
    initialHueSlider.setValue((int)(gradientBar.getGradient().getInitialHue()*100.0f));
    brightnessSlider.setValue((int)(gradientBar.getGradient().getBrightness()*100.0f));
    saturationSlider.setValue((int)(gradientBar.getGradient().getSaturation()*100.0f));
  }

  // ChangeListener interface. (For JSlider's)
  public void stateChanged(ChangeEvent e)
  {
    JSlider slider = (JSlider)e.getSource();

    if (slider == initialHueSlider)
      gradientBar.getGradient().setInitialHue (slider.getValue()/100.0f);
    else if (slider == colourSpanSlider)
      gradientBar.getGradient().setColourSpan (slider.getValue()/100.0f);
    else if (slider == saturationSlider)
      gradientBar.getGradient().setSaturation (slider.getValue()/100.0f);
    else if (slider == brightnessSlider)
      gradientBar.getGradient().setBrightness (slider.getValue()/100.0f);

  }

  // ItemListener interface. (For the checkbox)
  public void itemStateChanged(ItemEvent e)
  {
    gradientBar.getGradient().setReverse (reverseButton.isSelected());
  }

//  public static void main(String[] args)
//  {
//    GradientPanel gradientPanel = new GradientPanel(true);
//    JFrame frame = new JFrame();
//    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//    frame.getContentPane().add(gradientPanel);
//    frame.pack();
//    frame.show();
//  }
}