package dsto.dfc.swing.net.content.application;

import java.io.IOException;
import java.io.InputStream;
import java.net.ContentHandler;
import java.net.URLConnection;

import dsto.dfc.net.DfcContentHandlerFactory;
import dsto.dfc.net.DfcFileNameMap;

import dsto.dfc.swing.persistence.Deserializer;

/**
 * URL content handler for application/x-java-object content ie
 * XML serialized objects.  Call {@link #register} to enable
 * this as a system-wide content handler.
 *
 * @version $Revision$
 */
public class x_java_object extends ContentHandler
{
  public static String IOBJECT_MIME_TYPE = "application/x-java-object";

  public x_java_object()
  {
    // zip
  }

  /**
   * Add another extention to be a content handler for.
   */
  public static void registerExtension (String extention)
  {
    DfcFileNameMap.registerTypeForExtension (extention, IOBJECT_MIME_TYPE);
  }

  /**
   * Enable this class as the system-wide content handler for
   * application/x-java-object content.
   */
  public static void register ()
  {
    DfcFileNameMap.registerTypeForExtension ("iobject", IOBJECT_MIME_TYPE);
    DfcContentHandlerFactory.registerHandler
      (IOBJECT_MIME_TYPE, "dsto.dfc.swing.net.content", new x_java_object ());
  }

  /**
   * Use (@link dsto.dfc.persistence.Deserializer} to return an Object
   * as the URL content.
   */
  public Object getContent(URLConnection connection) throws IOException
  {
    return getObjectFromURL(connection);
  }

  /**
   * Use {@link dsto.dfc.swing.persistence.Deserializer} to return an Object
   * as the URL content. This is static so that programs do not have to
   * register as a URL Handler to get the Object. We do this because some
   * programs are not allowed to register themselves as URLHandlers. ie Applets.
   */
  public static Object getObjectFromURL (URLConnection connection) throws IOException
  {
    InputStream inputStream = connection.getInputStream();
    Object javaObject = null;

    try {
      javaObject = Deserializer.read (inputStream);
    }catch (ClassNotFoundException e)
    {
      e.printStackTrace();
    }

    return javaObject;
  }
}