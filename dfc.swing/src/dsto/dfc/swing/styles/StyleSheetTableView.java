package dsto.dfc.swing.styles;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.util.EventObject;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import dsto.dfc.swing.commands.AbstractSelectionBasedCommand;
import dsto.dfc.swing.commands.Command;
import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.swing.controls.AbstractCellRenderer;
import dsto.dfc.swing.controls.ColorChooserCombo;
import dsto.dfc.swing.event.SelectionEvent;
import dsto.dfc.swing.forms.AbstractFormEditor;
import dsto.dfc.swing.forms.FormEditor;
import dsto.dfc.swing.table.DfcTable;
import dsto.dfc.util.IllegalFormatException;

/**
 * Views/edits a StyleSheet in a table view.
 *
 * @see StyleSheetTableModel
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class StyleSheetTableView extends DfcTable
{
  protected StyleSheet base;
  protected FormEditorAdapter formEditor = null;

  public StyleSheetTableView ()
  {
    this (new StyleSheetTableModel ());
  }

  /**
   * Creates a new <code>StyleSheetTableView</code> instance.
   *
   * @param base A style sheet containing default values for
   * styles. These are the values shown when no value is set for a
   * style in the sheet being edited.
   * @param sheet The sheet to edit.
   */
  public StyleSheetTableView (StyleSheet base, StyleSheet sheet)
  {
    this (new StyleSheetTableModel (sheet));

    this.base = base;
  }

  public StyleSheetTableView (StyleSheetTableModel model)
  {
    super (model);

    setupColumns ();

    setBackground (Color.white);
    setAutoResizeMode (JTable.AUTO_RESIZE_LAST_COLUMN);
    setGridColor (Color.gray);
    setRowHeight (new ColorChooserCombo ().getPreferredSize ().height + 2);
    setIntercellSpacing (new Dimension (3, 3));

    updateEditCommands ();
  }

  private void setupColumns ()
  {
    setAutoCreateColumnsFromModel (false);
    TableColumnModel cm = getColumnModel ();

    // delete existing columns
    while (cm.getColumnCount () > 0)
      cm.removeColumn (cm.getColumn (0));

    // create cell renderers and editors
    StyleRenderer styleRenderer = new StyleRenderer ();
    ValueRenderer valueRenderer = new ValueRenderer ();
    ValueEditor valueEditor = new ValueEditor ();

    // create and add columns
    TableColumn styleColumn =
      new TableColumn (0, 40, styleRenderer, null);
    TableColumn valueColumn =
      new TableColumn (1, 40, valueRenderer, valueEditor);

    addColumn (styleColumn);
    addColumn (valueColumn);
  }

  public Dimension getPreferredSize ()
  {
    Dimension prefSize = super.getPreferredSize ();

    prefSize.width = Math.max (prefSize.width, 300);

    return prefSize;
  }

  /**
   * Get the form editor for this component. This class does not
   * implement FormEditor directly since the getEditorComponent ()
   * methods of FormEditor and JTable clash.
   */
  public FormEditor getFormEditor ()
  {
    if (formEditor == null)
      formEditor = new FormEditorAdapter ();

    return formEditor;
  }

  public StyleSheet getStyleSheet ()
  {
    return ((StyleSheetTableModel)getModel ()).getStyleSheet ();
  }

  public Style getStyleAtRow (int row)
  {
    return ((StyleSheetTableModel)getModel ()).getStyleAtRow (row);
  }

  /**
   * Set the style sheet containing default values for styles. These
   * are the values shown when no value is set for a style in the
   * sheet being edited.
   */
  public void setBase (StyleSheet newValue)
  {
    base = newValue;
  }

  protected void updateEditCommands ()
  {
    CommandView contextView = getCommandView (CommandView.CONTEXT_MENU_VIEW);

    Command clearCommand = new CmdClearValue (this);

    contextView.addCommand (clearCommand);
  }

  /**
   * The renderer for the style column.
   */
  class StyleRenderer extends AbstractCellRenderer
  {
    private Font boldFont;

    public JComponent getRendererComponent (JComponent client,
                                            Object value,
                                            int row, int column,
                                            boolean isSelected,
                                            boolean hasFocus,
                                            boolean expanded,
                                            boolean leaf)
    {
      if (boldFont == null)
        boldFont = getFont ().deriveFont (Font.BOLD);

      JLabel label = getLabel ();
      Style style = getStyleAtRow (row);
      StyleSheet sheet = getStyleSheet ();

      label.setIcon (style.getIcon ());
      label.setText (style.getText ());

      // if style has a value, make name bold
      if (sheet.get (style) != null)
        label.setFont (boldFont);

      label.setHorizontalAlignment (JLabel.RIGHT);

      return label;
    }
  }

  /**
   * The renderer for the value column.
   */
  class ValueRenderer implements TableCellRenderer
  {
    public Component getTableCellRendererComponent (JTable table, Object value,
                                                    boolean isSelected,
                                                    boolean hasFocus,
                                                    int row, int column)
    {
      Component renderer;
      StyleSheet styleSheet = getStyleSheet ();
      Style style = getStyleAtRow (row);
      Object styleValue = styleSheet.get (style);

      if (styleValue == null)
        styleValue = base.get (style);

      renderer = getDefaultRenderer (style.getType ()).
        getTableCellRendererComponent
          (StyleSheetTableView.this, styleValue, isSelected, hasFocus, row, column);

      return renderer;
    }
  }

  /**
   * Editor for the value column.  Inherits from DefaultCellEditor for
   * base functionality and pretends to use a text field as the editor
   * component. getTableCellEditorComponent () determines which editor
   * is actually used based on style and then redirects all
   * TableCellEditor calls to that editor until editing is stopped.
   */
  private final class ValueEditor
    extends DefaultCellEditor implements TableCellEditor, CellEditorListener
  {
    private TableCellEditor editor;

    public ValueEditor ()
    {
      super (new JTextField ());

      setClickCountToStart (1);
    }

    public Component getTableCellEditorComponent (JTable table, Object value,
                                                  boolean isSelected,
                                                  int row, int column)
    {
      Style style = getStyleAtRow (row);

      // remove listener from previous editor
      if (editor != null)
        editor.removeCellEditorListener (this);

      // if no value, use default
      if (value == null)
        value = base.get (style);

      editor = getDefaultEditor (style.getType ());

      Component tableEditorComponent =
        editor.getTableCellEditorComponent
          (StyleSheetTableView.this, value, isSelected, row, column);

      // customize recalcitrant combo boxes to fit in
      if (tableEditorComponent instanceof JComboBox)
      {
        tableEditorComponent.setBackground (getBackground ());
        tableEditorComponent.setFont (getFont ());
      }

      // install listener
      editor.addCellEditorListener (this);

      return tableEditorComponent;
    }

    // CellEditorListener interface

    public void editingStopped (ChangeEvent e)
    {
      editor.removeCellEditorListener (this);

      fireEditingStopped ();

      if (formEditor != null)
        formEditor.fireEditCommitted ();
    }

    public void editingCanceled (ChangeEvent e)
    {
      editor.removeCellEditorListener (this);

      fireEditingCanceled ();
    }

    // DefaultCellEditor overrides to redirect to actual editor

    public boolean shouldSelectCell (EventObject e)
    {
      return editor.shouldSelectCell (e);
    }

    public void cancelCellEditing ()
    {
      editor.cancelCellEditing ();
    }

    public boolean stopCellEditing ()
    {
      return editor.stopCellEditing ();
    }

    public Object getCellEditorValue ()
    {
      return editor.getCellEditorValue ();
    }
  }

  /**
   * The "Clear Value" command.
   */
  public static final class CmdClearValue
    extends AbstractSelectionBasedCommand implements TableModelListener
  {
    private static final KeyStroke ACCELERATOR =
      KeyStroke.getKeyStroke (KeyEvent.VK_DELETE, 0);

    protected StyleSheetTableView editor;

    public CmdClearValue (StyleSheetTableView editor)
    {
      super (editor);

      this.editor = editor;

      editor.getModel ().addTableModelListener (this);

      updateEnabled ();
    }

    public void execute ()
    {
      StyleSheetTableModel model = (StyleSheetTableModel)editor.getModel ();
      int [] rows = editor.getSelectedRows ();

      for (int i = 0; i < rows.length; i++)
        model.clearValue (rows [i]);

      updateEnabled ();
    }

    public String getName ()
    {
      return "edit.Clear Value";
    }

    public String getDescription ()
    {
      return "Clear the current style value";
    }

    public boolean isInteractive ()
    {
      return false;
    }

    public String getGroupInView (String viewName)
    {
      return "style";
    }

    public KeyStroke getAccelerator ()
    {
      return ACCELERATOR;
    }

    public char getMnemonic ()
    {
      return 'l';
    }

    public void tableChanged (TableModelEvent e)
    {
      updateEnabled ();
    }

    public void selectionChanged (SelectionEvent e)
    {
      updateEnabled ();
    }

    private void updateEnabled ()
    {
      if (editor.getSelectedRowCount () == 1)
      {
        StyleSheet styleSheet = editor.getStyleSheet ();
        Object value =
          styleSheet.get (editor.getStyleAtRow (editor.getSelectedRow ()));

        setEnabled (value != null);
      } else
      {
        // assume that in one a multi row selection there is something to clear
        setEnabled (editor.getSelectedRowCount () > 1);
      }
    }
  }

  protected class FormEditorAdapter
    extends AbstractFormEditor implements TableModelListener
  {
    // FormEditor interface

    public boolean isDirectEdit ()
    {
      return true;
    }

    public Class getPreferredValueType ()
    {
      return StyleSheet.class;
    }

    public void setEditorValue (Object value) throws IllegalFormatException
    {
      try
      {
        StyleSheetTableModel model = (StyleSheetTableModel)getModel ();

        model.removeTableModelListener (this);

        model.setStyleSheet ((StyleSheet)value);

        model.addTableModelListener (this);
      } catch (ClassCastException ex)
      {
        throw new IllegalFormatException (this, value, StyleSheet.class);
      }
    }

    public Object getEditorValue ()
    {
      return ((StyleSheetTableModel)getModel ()).getStyleSheet ();
    }

    public Component getEditorComponent ()
    {
      return StyleSheetTableView.this;
    }

    public String getEditorDescription ()
    {
      return null;
    }

    public void commitEdits () throws IllegalFormatException
    {
      if (isEditing ())
        stopEditing ();
    }

    public void tableChanged (TableModelEvent e)
    {
      if (e.getType () == TableModelEvent.UPDATE)
        fireEditCommitted ();
    }
  }
}
