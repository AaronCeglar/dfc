package dsto.dfc.swing.styles;

import java.awt.Component;

import javax.swing.JLabel;

import dsto.dfc.swing.forms.FormEditor;
import dsto.dfc.swing.forms.FormEditorListener;
import dsto.dfc.util.IllegalFormatException;

/**
 * Presents a simple, non-editable summary view of a StyleSheet.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class StyleSheetLabelView extends JLabel implements FormEditor
{
  protected StyleSheet styleSheet;

  public StyleSheetLabelView ()
  {
    // zip
  }

  protected void updateDisplay ()
  {
    setText (styleSheet.getSummary ());
  }

  // FormEditor interface

  public boolean isDirectEdit ()
  {
    return true;
  }

  public Class getPreferredValueType ()
  {
    return StyleSheet.class;
  }

  public void setEditorValue (Object value) throws IllegalFormatException
  {
    try
    {
      styleSheet = (StyleSheet)value;

      updateDisplay ();
    } catch (ClassCastException ex)
    {
      throw new IllegalFormatException (this, value, StyleSheet.class);
    }
  }

  public Object getEditorValue ()
  {
    return styleSheet;
  }

  public Component getEditorComponent ()
  {
    return this;
  }

  public String getEditorDescription ()
  {
    return null;
  }

  public void commitEdits () throws IllegalFormatException
  {
    // zip
  }

  public void addFormEditorListener (FormEditorListener l)
  {
    // zip
  }

  public void removeFormEditorListener (FormEditorListener l)
  {
    // zip
  }
}
