package dsto.dfc.swing.styles;

import java.awt.Color;
import java.awt.Font;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * A sheet of styles and their values. Clients will generally create a
 * subclass of this class in which they declare a set of static final
 * style instances (possibly based on AbstractStyle). See {@link
 * test.ui.style_sheet.TestStyleSheetView} for an example.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class StyleSheet extends HashMap
{
  private static final long serialVersionUID = 7204920258767391001L;

  protected Style [] styles;

  /**
   * Creates an empty style sheet with no styles.
   */
  public StyleSheet ()
  {
    this (new Style [0]);
  }

  /**
   * Create an empty style sheet with a given set of acceptable styles.
   *
   * @param styles The acceptable styles.
   */
  public StyleSheet (Style [] styles)
  {
    this.styles = styles;
  }

  /**
   * Create a style sheet that is a copy of another.
   */
  public StyleSheet (StyleSheet sheet)
  {
    super (sheet);

    this.styles = sheet.styles;
  }

  /**
   * Get the accepted styles for this sheet.
   */
  public Style [] getStyles ()
  {
    return styles;
  }

  public boolean hasValue (Style style)
  {
    return get (style) != null;
  }

  /**
   * Merge another sheet with this sheet, copying all non-null values from sheet
   * into this sheet.
   */
  public void merge (StyleSheet sheet)
  {
    for (Iterator i = sheet.entrySet ().iterator (); i.hasNext (); )
    {
      Map.Entry entry = (Map.Entry)i.next ();

      if (entry.getValue () != null)
        put (entry.getKey (), entry.getValue ());
    }
  }

  /**
   * Shortcut to get a color value from the sheet.
   */
  public Color getColor (Style key)
  {
    return (Color)get (key);
  }

  /**
   * Shortcut to get a font value from the sheet.
   */
  public Font getFont (Style key)
  {
    return (Font)get (key);
  }

  /**
   * Shortcut to get an int value from the sheet.
   *
   * @param key The key for the value.
   * @param defaultValue The value returned when there is no value for key.
   * @return The int value of the style, or defaultValue when there is
   * no value for the style.
   */
  public int getInt (Style key, int defaultValue)
  {
    Integer value = (Integer)get (key);

    return value == null ? defaultValue : value.intValue ();
  }

  /**
   * Get a text summary of the styles that have values
   * (eg "Fill Colour, Font").
   */
  public String getSummary ()
  {
    StringBuffer buff = new StringBuffer ();

    for (int i = 0; i < styles.length; i++)
    {
      Style style = styles [i];

      if (get (style) != null)
      {
        if (buff.length () > 0)
          buff.append (", ");

        buff.append (style.getText ());
      }
    }

    if (buff.length () == 0)
      buff.append ("[default]");

    return buff.toString ();
  }

}
