package dsto.dfc.swing.styles;

import dsto.dfc.swing.table.AbstractDfcTableModel;

/**
 * Adapts a StyleSheet to the JFC TableModel interface.  Presents the
 * sheet as a two column table (Style, Value).
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class StyleSheetTableModel extends AbstractDfcTableModel
{
  protected StyleSheet styleSheet;

  public StyleSheetTableModel ()
  {
    this (new StyleSheet ());
  }

  public StyleSheetTableModel (StyleSheet styleSheet)
  {
    this.styleSheet = styleSheet;
  }

  public StyleSheet getStyleSheet ()
  {
    return styleSheet;
  }

  public void setStyleSheet (StyleSheet newValue)
  {
    styleSheet = newValue;

    fireTableDataChanged ();
  }

  public Style getStyleAtRow (int row)
  {
    return styleSheet.getStyles () [row];
  }

  public void clearValue (int row)
  {
    Style style = styleSheet.getStyles () [row];

    styleSheet.remove (style);

    fireTableRowsUpdated (row, row);
  }

  // TableModel interface

  public String getColumnName (int column)
  {
    return column == 0 ? "Style" : "Value";
  }

  public Class getColumnClass (int column)
  {
    return column == 0 ? Style.class : Object.class;
  }

  public int getColumnCount ()
  {
    return 2;
  }

  public Object getValueAt (int row, int col)
  {
    Style style = styleSheet.getStyles () [row];

    return col == 0 ? style : styleSheet.get (style);
  }

  public int getRowCount ()
  {
    return styleSheet.getStyles ().length;
  }

  public boolean isCellEditable (int row, int column)
  {
    return column == 1;
  }

  public void setValueAt (Object value, int row, int column)
  {
    if (column == 1)
    {
      Style style = styleSheet.getStyles () [row];

      styleSheet.put (style, value);

      fireTableRowsUpdated (row, row); // fire row changed to update style label
    } else
    {
      throw new UnsupportedOperationException ("Cannot edit style column");
    }
  }
}
