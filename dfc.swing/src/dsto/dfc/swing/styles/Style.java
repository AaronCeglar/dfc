package dsto.dfc.swing.styles;

import javax.swing.Icon;

import dsto.dfc.swing.icons.Iconic;

/**
 * A logical display style.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public interface Style extends Iconic
{
  /**
   * The type of value for the style.
   */
  public Class getType ();

  /**
   * The user readable name of the style.
   */
  public String getText ();

  /**
   * A short (tooltip length) description of the style.
   */
  public String getDescription ();

  public Icon getIcon ();

  public Icon getLargeIcon ();
}
