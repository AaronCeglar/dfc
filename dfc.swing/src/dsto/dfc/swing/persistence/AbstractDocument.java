package dsto.dfc.swing.persistence;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import dsto.dfc.util.BasicPropertyEventSource;
import dsto.dfc.util.Beans;
import dsto.dfc.util.Disposable;
import dsto.dfc.util.PropertyEventSource;

/**
 * A base implementation of Document which implements the high-level
 * bahaviour of a document without being specific to a particular persistent
 * storage mechanism.  If a model contained within this document emits either
 * property change or change events, these are used to automatically raise the
 * changed flag.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public abstract class AbstractDocument
  extends BasicPropertyEventSource
  implements Document, PropertyChangeListener, ChangeListener, Disposable
{
  protected boolean changed;

  public AbstractDocument ()
  {
    this.changed = false;
  }

  public abstract String getType ();

  /**
   * Return a new model instance.
   */
  protected abstract Object createModelInstance ();

  /**
   * Prompt the user for a location (probably in a persistent storage system)
   * to save the model in.  This location will be passed to {@link #readModel},
   * {@link #writeModel} and {@link #setLocation}.
   *
   * @param mode The mode the selection is being made in: either "Open" or
   * "Save".
   * @return A 'location' object, pointing to the selected storage location.
   * Null indicates no location was selected.
   */
  protected abstract Object selectLocation (String mode);

  /**
   * The abstract location in persistent storage where the document is
   * to be saved.
   */
  protected abstract void setLocation (Object newLocation);

  protected abstract Object getLocation ();

  /**
   * Read a new model object from a persistent store location.
   *
   * @param location The location the persistent store to read from.
   * @return The new model read from the store.  Returning null indicates
   * that the operation usually open ()) should be cancelled.
   * @exception IOException if an error occurred while reading the model.
   *
   * @see #selectLocation
   */
  protected abstract Object readModel (Object location)
    throws IOException;

  /**
   * Write a model object to a persistent store location.
   *
   * @param location The location the persistent store to write to.
   * @param model The model to write.
   * @exception IOException if an error occurred while writing the model.
   *
   * @see #selectLocation
   */
  protected abstract void writeModel (Object location, Object model)
    throws IOException;

  public abstract Component getClient ();

  public abstract Object getModel ();

  /**
   * Set the model contained by this document.
   *
   * <p>If the model emits either property change or change events, these
   * are used to automatically raise the changed flag.</p>
   */
  public void setModel (Object newModel)
    throws IllegalArgumentException
  {
    Object oldModel = getModel ();

    basicSetModel (newModel);

    if (oldModel != null)
      unregisterModelListener (oldModel);

    if (newModel != null)
      registerModelListener (newModel);

    setChanged (false);

    firePropertyChange ("model", oldModel, newModel);
  }

  /**
   * Subclasses should overrride this to actually change the model returned
   * by getModel ().  This is called as part of the setModel ()
   * implementation
   *
   * @exception IllegalArgumentException if the model is invalid (eg not of
   * the correct type).  If this is thrown, setModel () will revert the model
   * to the original instance and re-throw the exception.
   */
  protected abstract void basicSetModel (Object newModel)
    throws IllegalArgumentException;


  public boolean isChanged ()
  {
    return changed;
  }

  public void setChanged (boolean newValue)
  {
    boolean oldValue = changed;

    changed = newValue;

    firePropertyChange ("changed", oldValue, newValue);
  }

  public Object newModel ()
  {
    Object newModel = createModelInstance ();

    setModel (newModel);

    setChanged (false);

    return newModel;
  }

  public boolean open ()
  {
    Object location;

    if ((location = selectLocation ("Open")) != null)
    {
      try
      {
        Object newModel = readModel (location);

        if (newModel != null)
        {
          setModel (newModel);

          setLocation (location);

          return true;
        } else
        {
          // readModel () returns null if operation should be cancelled

          return false;
        }
      } catch (IOException ex)
      {
        handleOpenError (ex);
      }
    }

    return false;
  }

  public boolean save ()
  {
    if (!isLocationSelected ())
      return saveAs ();
    else
      return write (getLocation ());
  }

  public boolean saveAs ()
  {
    Object location;

    if ((location = selectLocation ("Save")) != null)
      return write (location);
    else
      return false;
  }

  protected boolean write (Object location)
  {
    try
    {
      writeModel (location, getModel ());

      setChanged (false);
      setLocation (location);

      return true;
    } catch (IOException ex)
    {
      handleSaveError (ex);

      return false;
    }
  }

  public boolean checkSaveChanges ()
  {
    if (isChanged ())
    {
      Component window = SwingUtilities.windowForComponent (getClient ());
      String name = getName ();

      if (name == null || name.length () == 0)
        name = "<unnamed>";

      if (window == null)
        window = getClient ();

     int result = JOptionPane.showConfirmDialog
        (window, "The " + getType ().toLowerCase () + " \"" + name + "\"" +
           " has changed since it was last saved.\nDo you want to save it?",
         getType () + " Changed", JOptionPane.YES_NO_CANCEL_OPTION);

      if (result == JOptionPane.YES_OPTION)
        return save ();
      else if (result == JOptionPane.NO_OPTION)
        return true;
      else
        return false;
    } else
      return true;
  }

  protected boolean isLocationSelected ()
  {
    return getLocation () != null;
  }

  protected void handleOpenError (Throwable ex)
  {
    String message = "Error opening document";

    if (ex != null)
      message += " (" + ex + ")";

    JOptionPane.showMessageDialog
      (getClient (), message, "Open", JOptionPane.ERROR_MESSAGE);
  }

  protected void handleSaveError (Throwable ex)
  {
    String message = "Error saving document";

    if (ex != null)
      message += " (" + ex + ")";

    JOptionPane.showMessageDialog
      (getClient (), message, "Save", JOptionPane.ERROR_MESSAGE);
  }

  protected void unregisterModelListener (Object model)
  {
    if (model instanceof PropertyEventSource)
    {
      ((PropertyEventSource)model).removePropertyChangeListener (this);
    } else
    {
      Beans.removeListener (PropertyChangeListener.class, model, this);
    }

    Beans.removeListener (ChangeListener.class, model, this);
  }

  protected void registerModelListener (Object model)
  {
    if (model instanceof PropertyEventSource)
    {
      ((PropertyEventSource)model).addPropertyChangeListener (this);
    } else
    {
      Beans.addListener (PropertyChangeListener.class, model, this);
    }


    Beans.addListener (ChangeListener.class, model, this);
  }

  public void dispose ()
  {
    if (getModel () != null)
    {
      unregisterModelListener (getModel ());
      basicSetModel (null);
    }
  }

  protected void modelPropertyChanged (PropertyChangeEvent e)
  {
    setChanged (true);
  }

  protected void modelStateChanged (ChangeEvent e)
  {
    setChanged (true);
  }

  // ChangeListener interface

  /**
   * ChangeListener implementation: do not override this, override
   * modelStateChanged instead.
   */
  public void stateChanged (ChangeEvent e)
  {
    modelStateChanged (e);
  }

  // PropertyChangeListener interface

  /**
   * PropertyChangeListener implementation: do not override this, override
   * modelPropertyChanged instead.
   */
  public void propertyChange (PropertyChangeEvent e)
  {
    modelPropertyChanged (e);
  }
}