package dsto.dfc.swing.persistence;

import dsto.dfc.swing.commands.BasicFileSaveAsCommand;

/**
 * Basic command to execute the saveAs() method on a {@link Document}.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class BasicDocumentSaveAsCommand extends BasicFileSaveAsCommand
{
  private Document document;

  public BasicDocumentSaveAsCommand (Document document)
  {
    super (document.getClient ());

    this.document = document;
  }

  public void execute ()
  {
    document.saveAs ();
  }

  public String getDescription ()
  {
    return "Save the document to a new location";
  }
}
