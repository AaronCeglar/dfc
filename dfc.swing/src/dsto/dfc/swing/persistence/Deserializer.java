package dsto.dfc.swing.persistence;

import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectStreamConstants;

import java.util.zip.GZIPInputStream;

import org.xml.sax.SAXException;

import fr.dyade.koala.xml.kbml.KBMLDeserializer;

/**
 * Support for deserializing objects in various formats.
 *
 * @author Matthew.
 * @version $Revision$
 *
 * @see Serializer
 */
public final class Deserializer
{
  private Deserializer ()
  {
    // zip
  }

  public static Object read (File file)
    throws FileNotFoundException, ClassNotFoundException, IOException
  {
    return read (file, null);
  }

  public static Object read (File file, SerializedFormat format)
    throws FileNotFoundException, ClassNotFoundException, IOException
  {
    return read (file.getPath (), format);
  }

  public static Object read (String fileName)
    throws FileNotFoundException, ClassNotFoundException, IOException
  {
    return read (fileName, null);
  }

  /**
   * Read a serialized object from a file, autodetecting serialized
   * format.
   *
   * @param fileName The file to read from.
   * @param format The detected format is stored here if non-null.
   * @return The deserialized object.
   */
  public static Object read (String fileName, SerializedFormat format)
    throws FileNotFoundException, ClassNotFoundException, IOException
  {
    FileInputStream input = null;

    try
    {
      input = new FileInputStream (fileName);

      return read (input, format);
    } finally
    {
      if (input != null)
        input.close ();
    }
  }

  public static Object read (InputStream stream)
    throws IOException, ClassNotFoundException
  {
    return read (stream, null);
  }

  /**
   * Read an object from an input stream.  Autodetects format for
   * XML (KBML) and Java object serialized object streams, with optional
   * compression.
   *
   * @param stream The stream to read from.
   * @param format The detected format of the stream will be recorded
   * in this if it is not null.
   *
   * @return The deserialized object.
   * @exception IOException if an error occurs
   * @exception ClassNotFoundException if an error occurs
   */
  public static Object read (InputStream stream, SerializedFormat format)
    throws IOException, ClassNotFoundException
  {
    if (format == null)
      format = new SerializedFormat ();

    InputStream input = openAutoDetect (stream, format);

    return readObject (input, format);
  }

  /**
   * Read an object from an input stream using the correct reader for the
   * given format.  To read with stream format autodetection, use
   * {@link #read(InputStream,SerializedFormat)}.
   *
   * @param input The stream to read from.  Note that if using binary mode,
   * and input is already an ObjectInput instance, it is used directly.
   * @param format The format of the stream.
   *
   * @return The deserialized object.
   * @exception IOException if an error occurs
   * @exception ClassNotFoundException if an error occurs
   */
  public static Object readObject (InputStream input, SerializedFormat format)
    throws IOException, ClassNotFoundException
  {
    Object object = null;

    if (format.getType ().equals (SerializedFormat.FORMAT_XML))
      object = readKbml (input);
    else if (format.getType ().equals (SerializedFormat.FORMAT_BINARY))
      object = readSerialized (input);
    else
      throw new IOException ("Unrecognised stream format: " + format.getType ());

    return object;
  }

  private static Object readSerialized (InputStream input)
    throws IOException, ClassNotFoundException
  {
    ObjectInput objectInput;
    
    // it's a serialized object stream
    if (input instanceof ObjectInput)
      objectInput = (ObjectInput)input;
    else
      objectInput = new ObjectInputStream (input);
    
    return objectInput.readObject ();
  }

  private static Object readKbml (InputStream input)
    throws ClassNotFoundException, IOException
  {
    // it's a KBML archive
    try
    {
      KBMLDeserializer deserializer = new KBMLDeserializer (input);
    
      return deserializer.readBean ();
    } catch (SAXException ex)
    {
      if (ex.getException () != null)
      {
        ex.getException ().printStackTrace ();
    
        throw new IOException ("Exception during XML parsing: " + ex.getException ());
      } else
      {
        throw new IOException (ex.getMessage ());
      }
    } catch (InstantiationException ex)
    {
      throw new IOException ("Error instantiating class: " + ex);
    } catch (IllegalAccessException ex)
    {
      throw new IOException ("Error accessing class: " + ex);
    }
  }

  /**
   * Open a data stream, auto detecting format and adding a decompressor
   * if stream is compressed.
   *
   * @param format Auto detected format is stored here.
   * @return A stream ready for data reading.  Stream is guaranteed to be
   * buffered and support mark ()/reset ().
   */
  public static InputStream openAutoDetect (InputStream stream,
                                            SerializedFormat format)
    throws IOException
  {
    InputStream input =
      stream.markSupported () ? stream : new BufferedInputStream (stream);

    // read magic number to see if GZIP'd
    input.mark (10);
    format.setCompressed (readUShort (input) == GZIPInputStream.GZIP_MAGIC);
    input.reset ();

    // read header from input
    byte [] header = new byte [5];
    input.mark (10);

    if (format.isCompressed ())
    {
      // GZIPInputStream does not support mark/reset (even though its
      // markSupported () claims otherwise), so open temporary
      // compressed stream to read header.
      GZIPInputStream zipInput = new GZIPInputStream (input);

      zipInput.read (header);
    } else
    {
      input.read (header);
    }

    input.reset ();

    // switch input to compressed stream now
    if (format.isCompressed ())
      input = new BufferedInputStream (new GZIPInputStream (input));

    // is KBML
    String headerStr = new String (header);

    if (headerStr.equals ("<?xml") || headerStr.equals ("<kbml"))
    {
      format.setType (SerializedFormat.FORMAT_XML);
    } else if (makeShort (header) == ObjectStreamConstants.STREAM_MAGIC)
    {
      format.setType (SerializedFormat.FORMAT_BINARY);
    } else
    {
      throw new IOException ("Unrecognised stream format");
    }

    return input;
  }

  /**
   * Reads unsigned short in Intel byte order.
   */
  private static int readUShort (InputStream in) throws IOException
  {
    int b = readUByte (in);

    return (readUByte (in) << 8) | b;
  }

  /**
   * Reads unsigned byte.
   */
  private static int readUByte (InputStream in) throws IOException
  {
    int b = in.read ();

    if (b == -1)
      throw new EOFException();

    return b;
  }

  /**
   * Make a short int from two bytes.
   */
  private static short makeShort (byte [] bytes)
  {
    return (short)(((bytes [0] & 0xFF) << 8) + ((bytes [1] & 0xFF) << 0));
  }
}
