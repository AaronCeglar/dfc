package dsto.dfc.swing.persistence;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.xml.sax.SAXException;

import fr.dyade.koala.xml.kbml.KBMLDeserializer;
import fr.dyade.koala.xml.kbml.KBMLSerializer;

/**
 * Serializer that uses KBML IO.
 * 
 * @author mpp
 * @version $Revision$
 */
public class KbmlSerializer implements ISerializer
{
  private static final KbmlSerializer INSTANCE = new KbmlSerializer ();
  
  public static KbmlSerializer getInstance ()
  {
    return INSTANCE;
  }
  
  public KbmlSerializer ()
  {
    super ();
  }

  public void serialize (OutputStream output, Object object) throws IOException
  {
    KBMLSerializer serializer = new KBMLSerializer (output);

    serializer.writeKBMLStartTag ();
    serializer.writeBean (object);
    serializer.writeKBMLEndTag ();
    
    serializer.flush ();
  }

  public Object deserialize (InputStream input)
    throws IOException, ClassNotFoundException
  {
    try
    {
      KBMLDeserializer deserializer = new KBMLDeserializer (input);
    
      return deserializer.readBean ();
    } catch (SAXException ex)
    {
      if (ex.getException () != null)
      {
        ex.getException ().printStackTrace ();
    
        throw new IOException ("Exception during XML parsing: " + ex.getException ());
      } else
      {
        throw new IOException (ex.getMessage ());
      }
    } catch (InstantiationException ex)
    {
      throw new IOException ("Error instantiating class: " + ex);
    } catch (IllegalAccessException ex)
    {
      throw new IOException ("Error accessing class: " + ex);
    }
  }
}
