package dsto.dfc.swing.persistence;

import java.awt.Component;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JFileChooser;

import dsto.dfc.swing.commands.BasicFileSaveCommand;

/**
 * Base implementation for commands that save objects using
 * Serializer.  The UI is customized by initFileChooser() which sets
 * up an accessory panel ({@link SerializeAccessoryPanel}).  The
 * saveFile () method will usually call save () to save an object
 * using the options specified in the chooser.
 *
 * @version $Revision$
 *
 * @see SerializeAccessoryPanel
 */
public class BasicSerializeCommand extends BasicFileSaveCommand
{
  public BasicSerializeCommand ()
  {
    this (null);
  }

  public BasicSerializeCommand (Component owner)
  {
    super (owner);
  }

  /**
   * Sets up the accessory panel.
   */
  protected void initFileChooser (JFileChooser chooser)
  {

    super.initFileChooser (chooser);

    SerializeAccessoryPanel accessoryPanel = new SerializeAccessoryPanel ();
      chooser.setAccessory (accessoryPanel);
    chooser.putClientProperty ("save_accessory", accessoryPanel);
  }

  /**
   * Save an object using the settings in the chooser.
   *
   * @param object The object to save.
   * @param chooser The chooser.
   */
  protected void save (Object object, JFileChooser chooser)
    throws FileNotFoundException, IOException
  {
    save (object, chooser, chooser.getSelectedFile ());
  }

  /**
   * Save an object to a specified file, using the option settings in
   * the chooser.
   *
   * @param object The object to save.
   * @param chooser The chooser.
   * @param file The file to save to.
   */
  protected void save (Object object, JFileChooser chooser, File file)
    throws FileNotFoundException, IOException
  {
    SerializeAccessoryPanel accessoryPanel =
      (SerializeAccessoryPanel)chooser.getClientProperty ("save_accessory");

    if (accessoryPanel.getType ().equals ("XML"))
    {
      Serializer.saveAsKBML (object, file.getPath (),
                             accessoryPanel.isCompress ());
    } else
    {
      Serializer.saveAsBinary (object, file.getPath (),
                               accessoryPanel.isCompress ());
    }
  }
}
