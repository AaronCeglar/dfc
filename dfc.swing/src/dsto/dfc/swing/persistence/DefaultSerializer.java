package dsto.dfc.swing.persistence;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

/**
 * Default serializer that uses Java serialization.
 * 
 * @author mpp
 * @version $Revision$
 */
public class DefaultSerializer implements ISerializer
{
  private static final DefaultSerializer INSTANCE = new DefaultSerializer ();
  
  public static DefaultSerializer getInstance ()
  {
    return INSTANCE;
  }
  
  public void serialize (OutputStream output, Object object)
    throws IOException
  {
    ObjectOutputStream objectOutput = new ObjectOutputStream (output);

    objectOutput.writeObject (object);

    objectOutput.flush ();
  }

  public Object deserialize (InputStream input)
    throws IOException, ClassNotFoundException
  {
    ObjectInput objectInput;
    
    // it's a serialized object stream
    if (input instanceof ObjectInput)
      objectInput = (ObjectInput)input;
    else
      objectInput = new ObjectInputStream (input);
    
    return objectInput.readObject ();
  }
}
