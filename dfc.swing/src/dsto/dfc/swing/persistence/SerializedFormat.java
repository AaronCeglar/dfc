package dsto.dfc.swing.persistence;

/**
 * Stores format options for serialization.
 *
 * @author Matthew Phillips
 * @version $Revision$
 *
 * @see Deserializer
 */
public class SerializedFormat
{
  public static final String FORMAT_XML = "XML";
  public static final String FORMAT_BINARY = "Binary";
  public static final String FORMAT_UNKNOWN = "Unknown";

  private boolean compressed;
  private String type;

  public SerializedFormat ()
  {
    this.compressed = false;
    this.type = FORMAT_XML;
  }

  public SerializedFormat (String type, boolean compressed)
  {
    this.type = type;
    this.compressed = compressed;
  }

  public String getType ()
  {
    return type;
  }

  public void setType (String newType)
  {
    type = newType;
  }

  public void setCompressed (boolean newCompressed)
  {
    compressed = newCompressed;
  }

  public boolean isCompressed ()
  {
    return compressed;
  }
}
