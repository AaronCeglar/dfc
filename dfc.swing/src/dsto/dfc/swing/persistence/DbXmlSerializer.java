package dsto.dfc.swing.persistence;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import org.jdom.JDOMException;

import dsto.dfc.databeans.io.XmlInput;
import dsto.dfc.databeans.io.XmlOutput;

import dsto.dfc.logging.Log;


/**
 * Plugs DataBeans XML IO into the dsto.dfc.persistence framework.
 * 
 * @author mpp
 * @version $Revision$
 */
public class DbXmlSerializer implements ISerializer
{
  private XmlInput xmlInput;
  private XmlOutput xmlOutput;

   public DbXmlSerializer ()
   {
    this (null);
   }
   
  /**
   * @param xmlReaderClass The class name of the SAX XMLReader.
   */
  public DbXmlSerializer (String xmlReaderClass)
  {
    this.xmlInput = new XmlInput (xmlReaderClass);
    this.xmlOutput = new XmlOutput ();    
  }

  public void serialize (OutputStream output, Object object) throws IOException
  {
    OutputStreamWriter writer = new OutputStreamWriter (output);
    
    xmlOutput.write (writer, object, true);
    
    writer.flush ();
  }

  public Object deserialize (InputStream input)
    throws IOException, ClassNotFoundException
  {
    InputStreamReader reader = new InputStreamReader (input);
    
    try
    {
      return xmlInput.read (reader);
    } catch (JDOMException ex)
    {
      Log.diagnostic ("XML parse error", this, ex);
      
      throw new IOException ("XML parse error: " + ex.getMessage ());
    }
  }
  
  public XmlInput getXmlInput ()
  {
    return xmlInput;
  }

  public XmlOutput getXmlOutput ()
  {
    return xmlOutput;
  }

  public void setXmlInput (XmlInput xmlInput)
  {
    this.xmlInput = xmlInput;
  }

  public void setXmlOutput (XmlOutput xmlOutput)
  {
    this.xmlOutput = xmlOutput;
  }
}
