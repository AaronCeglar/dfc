package dsto.dfc.swing.persistence;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import dsto.dfc.collections.CountedSet;
import dsto.dfc.logging.Log;
import dsto.dfc.swing.text.selection.Selector;
import dsto.dfc.swing.text.selection.SelectorFilenameFilter;
import dsto.dfc.util.StateChangeEvent;
import dsto.dfc.util.StateChangeListener;
import dsto.dfc.util.WeakEventListener;

/**
 * A lightweight persistent object manager.  Maintains a set of
 * persistent objects, loading, unloading and saving modified objects
 * as required.  Each managed object is accessed via a logical key:
 * clients may register new persistent objects ({@link
 * #registerObject(String, Object)}), lookup existing
 * objects ({@link #findObject}) and mark objects modified ({@link
 * #objectModified}).  Objects that fire {@link dsto.dfc.util.StateChangeEvent}'s
 * are automatically marked changed by the manager. The manager ensures modifed
 * objects are periodically saved and that unreferenced objects are unloaded in
 * response to memory demand using the Java weak reference system.
 *
 * @author Matthew
 * @version $Revision$
 */
public class PersistenceManager
{
  /** The default storage directory for objects. This may be overridden on a
   * per-object basis. */
  protected String storageDir;
  /** Maps object key to PersistentRef instance. */
  protected HashMap objects = new HashMap ();
  protected final CountedSet modifiedObjects = new CountedSet ();
  protected final ReferenceQueue refQueue = new ReferenceQueue ();
  protected final Housekeeper housekeeper = new Housekeeper ();

  /**
   * Creates a new <code>PersistenceManager</code> instance.
   *
   * @param storageDir The base directory for persistent storage.
   * Serialized objects are saved here using filenames derived from
   * their logical keys.
   */
  public PersistenceManager (String storageDir)
  {
    this.storageDir = storageDir;

    housekeeper.start ();
  }

  /**
   * Get the object that is locked by the manager when modifying
   * state.  Multithreaded clients can use this to ensure atomicity of
   * mutiple method calls (eg findObject () and registerObject ()).
   *
   * @return an <code>Object</code> value
   */
  public Object getModifyLock ()
  {
    return objects;
  }

  /**
   * The default path used to store serialized data for a given key.
   * NOTE: This may not be the actual path if the storage directory
   * is explicity set for an object.
   *
   * @see #pathForKey(String, String)
   */
  public String pathForKey (String key)
  {
    return pathForKey (storageDir, key);
  }

  /**
   * The path used to store serialized data for a given key dir storage
   * directory.
   *
   * @param dir The storage directory.
   * @param key The key.
   * @return The storage path.
   */
  public String pathForKey (String dir, String key)
  {
    return dir + File.separator + key;
  }

  /**
   * Register a new persistent object using the default Java
   * serialization format.
   *
   * @exception IllegalArgumentException if key is already in use.
   *
   * @see #registerObject(String, Object, ISerializer)
   */
  public void registerObject (String key, Object object)
    throws IllegalArgumentException
  {
    registerObject (key, object, DefaultSerializer.getInstance ());
  }

  /**
   * Register a new persistent object.
   *
   * @param key The logical key for the object.  This is used for
   * subsequent access (eg see ({@link #findObject}).
   * @param dir The base directory for the saved object.
   * @param object The object to be managed.  Note, this object is not
   * automatically considered to be modified, and will not be serialized
   * until a call to objectModified() is made or the object fires a change
   * event.
   *
   * @exception IllegalArgumentException if key is already in use.
   *
   * @see #registerObject(String,Object)
   * @see #objectModified(String)
   */
  public void registerObject (String key, String dir, Object object)
  {
    registerObject (key, dir, object, DefaultSerializer.getInstance ());
  }

  /**
   * Register a new persistent object.
   *
   * @param key The logical key for the object.  This is used for
   * subsequent access (eg see ({@link #findObject}).
   * @param object The object to be managed.  Note, this object is not
   * automatically considered to be modified, and will not be serialized
   * until a call to objectModified() is made or the object fires a change
   * event.
   * @param serializer The serializer for the object.
   *
   * @exception IllegalArgumentException if key is already in use.
   *
   * @see #registerObject(String,Object)
   * @see #objectModified(String)
   */
  public void registerObject (String key, Object object,
                              ISerializer serializer)
    throws IllegalArgumentException
  {
    registerObject (key, storageDir, object, serializer);
  }

  /**
   * Register a new persistent object.
   *
   * @param key The logical key for the object. This is used for
   * subsequent access (eg see ({@link #findObject}).
   * @param dir The base directory for the saved object.
   * @param object The object to be managed.  Note, this object is not
   * automatically considered to be modified, and will not be serialized
   * until a call to objectModified() is made or the object fires a change
   * event.
   * @param serializer The serializer for the object.
   *
   * @exception IllegalArgumentException if key is already in use.
   *
   * @see #registerObject(String,Object)
   * @see #objectModified(String)
   */
  public void registerObject (String key, String dir, Object object,
                              ISerializer serializer)
    throws IllegalArgumentException
  {
    synchronized (objects)
    {
      if (key == null)
        throw new IllegalArgumentException ("Cannot use a null key");
      else if (objects.get (key) != null)
        throw new IllegalArgumentException ("Key \"" + key + "\" is already registered");

      PersistentRef ref = new PersistentRef (key, dir, object, serializer, refQueue);

      objects.put (key, ref);
    }
  }

  /**
   * Lookup a previously registered persistent object.  Note for
   * multithreaded clients: to stop race conditions between two
   * threads that call findObject () and then registerObject () if the
   * object is not found, ensure that these calls are done in a
   * synchronized block using {@link #getModifyLock}.
   *
   * @param key The key for the object
   * @return The object, or null if not defined.
   *
   * @exception IOException If an error occurred reading a previously
   * saved store.
   *
   * @see #registerObject(String, Object, ISerializer)
   */
  public Object findObject (String key)
    throws IOException
  {
    return findObject (key, storageDir, DefaultSerializer.getInstance ());
  }

  /**
   * Lookup a previously registered persistent object.  Note for
   * multithreaded clients: to stop race conditions between two
   * threads that call findObject () and then registerObject () if the
   * object is not found, ensure that these calls are done in a
   * synchronized block using {@link #getModifyLock}.
   *
   * @param key The key for the object
   * @param serializer The serializer to use if the object needs to be loaded.
   * @return The object, or null if not defined.
   *
   * @exception IOException If an error occurred reading a previously
   * saved store.
   *
   * @see #registerObject(String,Object,ISerializer)
   */
  public Object findObject (String key, ISerializer serializer)
    throws IOException
  {
    return findObject (key, storageDir, serializer);
  }

  /**
   * Lookup a previously registered persistent object.  Note for
   * multithreaded clients: to stop race conditions between two
   * threads that call findObject () and then registerObject () if the
   * object is not found, ensure that these calls are done in a
   * synchronized block using {@link #getModifyLock}.
   *
   * @param key The key for the object.
   * @param dir The directory to look for the object.
   * @return The object, or null if not defined.
   *
   * @exception IOException If an error occurred reading a previously
   * saved store.
   *
   * @see #registerObject(String,Object,ISerializer)
   */
  public Object findObject (String key, String dir)
    throws IOException
  {
    return findObject (key, dir, DefaultSerializer.getInstance ());
  }

  /**
   * Lookup a previously registered persistent object.  Note for
   * multithreaded clients: to stop race conditions between two
   * threads that call findObject () and then registerObject () if the
   * object is not found, ensure that these calls are done in a
   * synchronized block using {@link #getModifyLock}.
   *
   * @param key The key for the object
   * @param dir The directory to look for the object.
   * @param serializer The serializer to use if the object needs to be loaded.
   * @return The object, or null if not defined.
   *
   * @exception IOException If an error occurred reading a previously
   * saved store.
   *
   * @see #registerObject(String,Object,ISerializer)
   */
  public Object findObject (String key, String dir, ISerializer serializer)
    throws IOException
  {
    synchronized (objects)
    {
      PersistentRef ref = (PersistentRef)objects.get (key);

      if (ref == null)
      {
        try
        {
          ref = loadReference (key, dir, serializer);

          objects.put (key, ref);
        } catch (FileNotFoundException ex)
        {
          return null;
        } catch (ClassNotFoundException ex)
        {
          throw new IOException ("Failed to find serialized class: " + ex.getMessage ());
        }
      }

      ref.touch ();

      return ref.getObject ();
    }
  }

  /**
   * Retrieve the list of all available persistent objects.
   *
   * @see #listObjects(Selector)
   * @return The set of all available object keys.
   */
  public String [] listObjects ()
  {
    return listObjects (storageDir);
  }

  /**
   * Retrieve the list of all available persistent objects.
   *
   * @param dir The directory to search for saved objects.
   * @see #listObjects(Selector)
   * @return The set of all available object keys.
   */
  public String [] listObjects (String dir)
  {
    return listObjects (dir, Selector.SELECT_ALL);
  }

  /**
   * Retrieve the list of all available persistent objects matching a given
   * selector.
   *
   * @param selector A selector instance that is used to select object keys.
   * @return The set of matched object keys.
   */
  public String [] listObjects (Selector selector)
  {
    return listObjects (storageDir, selector);
  }

  /**
   * Retrieve the list of all available persistent objects matching a given
   * selector.
   *
   * @param dir The directory to search for saved objects.
   * @param selector A selector instance that is used to select object keys.
   * @return The set of matched object keys.
   */
  public String [] listObjects (String dir, Selector selector)
  {
    File directory = new File (dir);
    File [] files = directory.listFiles (new SelectorFilenameFilter (selector));
    HashSet selectedObjects = new HashSet ();

    // add add saved object files
    if (files != null)
    {
      for (int i = 0; i < files.length; i++)
        selectedObjects.add (files [i].getName ());
    }

    synchronized (objects)
    {
      // add all matching registered object keys
      for (Iterator i = objects.keySet ().iterator (); i.hasNext (); )
      {
        Object key = i.next ();

        if (selector.matches (key))
          selectedObjects.add (key);
      }
    }

    // convert set to string []
    String [] selectedObjectsArray = new String [selectedObjects.size ()];
    selectedObjects.toArray (selectedObjectsArray);

    return selectedObjectsArray;
  }

  private PersistentRef findReference (Object object)
  {
    synchronized (objects)
    {
      for (Iterator i = objects.values ().iterator (); i.hasNext (); )
      {
        PersistentRef ref = (PersistentRef)i.next ();

        if (ref.getObject () == object)
          return ref;
      }
    }

    return null;
  }

  /**
   * NOTE: not currently implemented.
   *
   * @deprecated
   */
  @Deprecated
  public void unregisterObject (String key)
  {
    throw new UnsupportedOperationException ("unimplemented");
  }

  /**
   * Mark an object as modified.  The object will be saved at the next
   * opportunity.
   *
   * @param key The key for the object.
   *
   * @see #objectModified(Object)
   */
  public void objectModified (String key)
  {
    synchronized (objects)
    {
      PersistentRef ref = (PersistentRef)objects.get (key);

      if (ref == null)
        throw new IllegalArgumentException
         ("Attempt to mark non-existent key \"" + key + "\" as modified");

      referenceModified (ref);
    }
  }

  /**
   * Mark an object as modified.  The object will be saved at the next
   * opportunity.
   *
   * @param object The object.
   *
   * @see #objectModified(String)
   */
  public void objectModified (Object object)
  {
    synchronized (objects)
    {
      PersistentRef ref = findReference (object);

      if (ref == null)
        throw new IllegalArgumentException
         ("Attempt to mark non-registered object \"" + object + "\" as modified");

      referenceModified (ref);
    }
  }

  /**
   * Manually signal that a persistent object has been accessed.  This serves as
   * as a hint not to unload the object.  This is automatically done whenever
   * findObject () is called or the object is modified.
   */
  public void objectAccessed (Object object)
  {
    synchronized (objects)
    {
      PersistentRef ref = findReference (object);

      if (ref == null)
        throw new IllegalArgumentException
         ("Attempt to mark non-registered object \"" + object + "\" as accessed");

      ref.touch ();
    }
  }

  private void referenceModified (PersistentRef ref)
  {
    synchronized (objects)
    {
      if (ref == null)
        throw new IllegalArgumentException
         ("Attempt to mark non-existent key \"" + ref.key + "\" as modified");

      // set hard reference to ensure object is not GC'd
      ref.hardRef = ref.get ();
      ref.modified = true;
      ref.touch ();

      modifiedObjects.add (ref);
    }
  }

  public boolean isModified (String key)
  {
    if (modifiedObjects.size () == 0)
      return false;

    synchronized (objects)
    {
      PersistentRef ref = (PersistentRef)objects.get (key);

      return ref.modified;
    }
  }

  /**
   * Save all modified objects.
   */
  public void saveModified ()
  {
    saveModified (false);
  }

  /**
   * Save modified objects.
   *
   * @param forceSaveAll Force save of all modified objects.  If not true,
   * only objects not recently modified (last 10 secs) or not saved for 2 mins
   * are saved.
   */
  public synchronized void saveModified (boolean forceSaveAll)
  {
    if (objects == null || modifiedObjects.size () == 0)
      return;

    // deadlock avoidance: copy modified object list to avoid having
    // to hold lock on objects while calling saveReference which locks
    // the persistent reference.
    ArrayList objectsToSave;
    ArrayList savedObjects = new ArrayList ();

    synchronized (objects)
    {
      objectsToSave = new ArrayList (modifiedObjects);
    }

    long now = System.currentTimeMillis ();

    // for each modified object...
    for (Iterator i = objectsToSave.iterator (); i.hasNext (); )
    {
      PersistentRef ref = (PersistentRef)i.next ();

      // if an modified object...
      if (ref.modified && ref.hardRef != null)
      {
        try
        {
          // if object was not accessed in the last 10 secs OR it hasn't been
          // saved for more than 2 mins, then save it
          if (forceSaveAll ||
              (now - ref.lastAccessed) >= 10000 ||
              (now - ref.lastSaved) > 120000)
          {
            saveReference (ref);

            savedObjects.add (ref);
          }
        } catch (Throwable ex)
        {
          Log.alarm ("Error saving object \"" + ref.key + "\"", this, ex);
        }
      } else
      {
        // object is not loaded: add to list for removal
        savedObjects.add (ref);
      }
    }

    synchronized (objects)
    {
      modifiedObjects.removeAll (savedObjects);
    }
  }

  /**
   * Unlock any unmodified objects that have not been accessed for 30 seconds,
   * freeing them for GC.
   */
  protected void unlockStaleObjects ()
  {
    if (objects == null)
      return;

    synchronized (objects)
    {
      long expirePoint = System.currentTimeMillis () - 30000;

      for (Iterator i = objects.values ().iterator (); i.hasNext (); )
      {
        PersistentRef ref = (PersistentRef)i.next ();

        if (!ref.modified && ref.lastAccessed < expirePoint)
        {
          // unlock persistent object
          ref.hardRef = null;
        }
      }
    }
  }

  /**
   * Perform a controlled shutdown of the manager.  This should be
   * called before system shutdown to ensure no data is lost.
   */
  public void shutdown ()
  {
    if (objects != null)
    {
      synchronized (objects)
      {
        // is this a good idea or not?
        // housekeeper.interrupt ();

        saveModified (true);

        objects.clear ();
        objects = null;
      }
    }
  }

  private PersistentRef loadReference (String key, String dir,
                                       ISerializer serializer)
    throws IOException, ClassNotFoundException
  {
    String path = pathForKey (dir, key);

    InputStream input = new BufferedInputStream (new FileInputStream (path));

    try
    {
      Object object = serializer.deserialize (input);

      input.close ();

      Log.diagnostic ("Loaded persistent object \"" + key + "\"", this);

      return new PersistentRef (key, dir, object, serializer, refQueue);
    } finally
    {
      try
      {
        input.close ();
      } catch (IOException ex)
      {
        // zip: don't care
      }
    }
  }

  private void saveReference (PersistentRef ref)
    throws IOException
  {
    File file = new File (ref.getPath ());
    Object object = ref.getObject ();

    ensureFileDirsCreated (file);

    synchronized (object)
    {
      // write to temp file to avoid leaving corrupted file on error
      File tempFile = File.createTempFile ("ser", null, file.getParentFile ());

      OutputStream output =
        new BufferedOutputStream (new FileOutputStream (tempFile));

      try
      {
        ref.serializer.serialize (output, object);
      } finally
      {
        output.close ();
      }

      if (file.exists ())
        file.delete ();

      tempFile.renameTo (file);

      Log.diagnostic ("Saved persistent object \"" + ref.key + "\"", this);

      ref.lastSaved = System.currentTimeMillis ();
      ref.modified = false;
    }
  }

  private static void ensureFileDirsCreated (File file)
  {
    File dir = file.getParentFile ();

    dir.mkdirs ();
  }

  private final class PersistentRef
    extends WeakReference implements StateChangeListener
  {
    public String key;
    public Object hardRef;
    public ISerializer serializer;
    public boolean modified;
    public long lastAccessed;
    public long lastSaved;
    public String localStorageDir;

    public PersistentRef (String key,
                          String localStorageDir,
                          Object object,
                          ISerializer serializer,
                          ReferenceQueue refQueue)
    {
      super (object, refQueue);

      this.key = key;
      this.serializer = serializer;
      this.modified = false;
      this.hardRef = object;
      this.lastSaved = System.currentTimeMillis ();
      this.localStorageDir = localStorageDir;

      touch ();

      WeakEventListener.createListener (StateChangeListener.class, object, this);
    }

    public void touch ()
    {
      lastAccessed = System.currentTimeMillis ();
    }

    public String getPath ()
    {
      return localStorageDir + '/' + key;
    }

    public Object getObject ()
    {
      if (hardRef != null)
        return hardRef;
      else
        return get ();
    }

    @SuppressWarnings ("unused")
    public void saved ()
    {
      lastSaved = System.currentTimeMillis ();
    }

    // ChangeListener interface

    public void stateChanged (StateChangeEvent e)
    {
      objectModified (e.getSource ());
    }
  }

  private final class Housekeeper extends Thread
  {
    public Housekeeper ()
    {
      super ("Persistence Housekeeper");

      setDaemon (true);
      setPriority (NORM_PRIORITY - 1);
    }

    @Override
    public void run ()
    {
      try
      {
        while (objects != null)
        {
          // wrap in exception handler to ensure errors do not kill thread
          try
          {
            saveModified (false);
            unlockStaleObjects ();

            PersistentRef ref = (PersistentRef)refQueue.remove (10 * 1000);

            if (ref != null)
            {
              Log.diagnostic ("Unloaded persistent object \"" + ref.key + "\"", this);

              synchronized (objects)
              {
                if (objects != null)
                  objects.remove (ref.key);
                else
                  return; // null objects => we are shutting down
              }
            }
          } catch (Throwable ex)
          {
            if (ex instanceof InterruptedException)
              throw (InterruptedException)ex;
            else
              Log.warn ("Persistence housekeeper found uncaught exception", this, ex);
          }
        }
      } catch (InterruptedException ex)
      {
        // OK, system is probably shutting down.
      }
    }
  }
}
