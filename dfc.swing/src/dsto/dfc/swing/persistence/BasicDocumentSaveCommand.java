package dsto.dfc.swing.persistence;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import dsto.dfc.swing.commands.BasicFileSaveCommand;

/**
 * Command to invoke the save () method on a {@link Document}.  This
 * command is only enabled then the document has been changed ({@link
 * Document#isChanged}.
 * 
 * @author Matthew Phillips
 * @version $Revision$
 */
public class BasicDocumentSaveCommand
  extends BasicFileSaveCommand implements PropertyChangeListener
{
  private Document document;

  public BasicDocumentSaveCommand (Document document)
  {
    super (document.getClient ());

    this.document = document;

    document.addPropertyChangeListener (this);

    setEnabled (document.isChanged ());
  }

  public void execute ()
  {
    document.save ();
  }

  public String getDescription ()
  {
    return "Save changes to the document";
  }

  // PropertyChangeListener interface

  public void propertyChange (PropertyChangeEvent e)
  {
    if (e.getPropertyName ().equals ("changed"))
      setEnabled (document.isChanged ());
  }
}
