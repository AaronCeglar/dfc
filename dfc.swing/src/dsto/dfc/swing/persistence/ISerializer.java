package dsto.dfc.swing.persistence;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Defines an object that can serialize/deserialize an object to a stream.
 * 
 * @author mpp
 * @version $Revision$
 */
public interface ISerializer
{
  public void serialize (OutputStream output, Object object)
    throws IOException;
    
  public Object deserialize (InputStream input)
    throws IOException, ClassNotFoundException;
}
