package dsto.dfc.swing.persistence;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import dsto.dfc.swing.controls.RadioBox;

/**
 * Panel designed to be used as the accessory panel in a FileChooser
 * for serializing objects using Serializer.  Allows selection of
 * output format (XML or binary) and compression.
 *
 * @version $Revision$
 *
 * @see BasicSerializeCommand
 */
public class SerializeAccessoryPanel extends JPanel
{
  private RadioBox typeRadio = new RadioBox();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private JLabel typeLabel = new JLabel();
  private JCheckBox compressCheckbox = new JCheckBox();
  private Border border2;

  public SerializeAccessoryPanel ()
  {
    try
    {
      jbInit();
    } catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public void setType (String newType)
  {
    typeRadio.setSelectedStringValue (newType);
  }

  public String getType ()
  {
    return typeRadio.getSelectedStringValue ();
  }

  public void setCompress (boolean newCompress)
  {
    compressCheckbox.setSelected (newCompress);
  }

  public boolean isCompress ()
  {
    return compressCheckbox.isSelected ();
  }

  private void jbInit ()
  {
    border2 = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 134)),"Save Options"),BorderFactory.createEmptyBorder(0,6,6,6));
    this.setBorder(border2);
    this.setLayout(gridBagLayout1);
    typeRadio.setPossibleStringValues(new String[] {"XML", "Binary"});
    typeRadio.setSelectedStringValue("XML");
    typeLabel.setDisplayedMnemonic('F');
    typeLabel.setText("Type:");
    compressCheckbox.setText("Compress");
    compressCheckbox.setToolTipText("Compress the data in the file to save space");
    compressCheckbox.setMnemonic('m');
    this.add(typeLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    this.add(compressCheckbox, new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0
            ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    this.add(typeRadio, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
  }
}
