package dsto.dfc.swing.persistence;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;

import fr.dyade.koala.xml.kbml.KBMLSerializer;

/**
 * Support for serializing objects in various formats.  Includes
 * support XML (KBML) and Java object serialization and adds optional
 * compression.
 *
 * @author Matthew.
 * @version $Revision$
 *
 * @see Deserializer
 */
public final class Serializer
{
  private Serializer ()
  {
    // zip
  }

  /**
   * Serialize an object using a given format.
   *
   * @param object The object to serialize.
   * @param file The file to serialize to.
   * @param format The format to use.
   */
  public static void save (Object object, String file,
                           SerializedFormat format)
    throws FileNotFoundException, IOException
  {
    if (format.getType ().equals (SerializedFormat.FORMAT_XML))
      saveAsKBML (object, file, format.isCompressed ());
    else
      saveAsBinary (object, file, format.isCompressed ());
  }

  public static void saveAsBinary (Object object, String fileName)
    throws FileNotFoundException, IOException
  {
    saveAsBinary (object, fileName, false);
  }

  public static void saveAsBinary (Object object, String fileName,
                                   boolean compress)
    throws FileNotFoundException, IOException
  {
    File file = new File (fileName);
    File tempFile = File.createTempFile ("ser", null, file.getParentFile ());

    OutputStream stream =
      new BufferedOutputStream (new FileOutputStream (tempFile));

    try
    {
      saveAsBinary (object, stream, compress);
    } finally
    {
      stream.close ();
    }

    if (file.exists ())
      file.delete ();

    tempFile.renameTo (file);
  }

  public static void saveAsBinary (Object object, OutputStream stream)
    throws IOException
  {
    saveAsBinary (object, stream, false);
  }

  public static void saveAsBinary (Object object, OutputStream stream,
                                   boolean compress)
    throws IOException
  {
    OutputStream output = compress ? new GZIPOutputStream (stream) : stream;

    ObjectOutputStream objectOutput = new ObjectOutputStream (output);

    objectOutput.writeObject (object);

    output.flush ();

    if (compress)
      ((GZIPOutputStream)output).finish ();
  }

  public static void saveAsKBML (Object object, String fileName)
    throws FileNotFoundException, IOException
  {
    saveAsKBML (object, fileName, false);
  }

  public static void saveAsKBML (Object object, String fileName,
                                 boolean compress)
    throws FileNotFoundException, IOException
  {
    File file = new File (fileName);
    File tempFile = File.createTempFile ("ser", null, file.getParentFile ());

    FileOutputStream fileStream = new FileOutputStream (tempFile);

    // only buffer non-compressed streams
    OutputStream stream =
      compress ? (OutputStream)fileStream : (OutputStream)new BufferedOutputStream (fileStream);

    try
    {
      saveAsKBML (object, stream, compress);
    } finally
    {
      stream.close ();
    }

    if (file.exists ())
      file.delete ();

    tempFile.renameTo (file);
  }

  public static void saveAsKBML (Object object, OutputStream stream)
    throws IOException
  {
    saveAsKBML (object, stream, false);
  }

  public static void saveAsKBML (Object object, OutputStream stream,
                                 boolean compress)
    throws IOException
  {
    OutputStream output = compress ? new GZIPOutputStream (stream) : stream;

    KBMLSerializer serializer = new KBMLSerializer (output);

    serializer.writeKBMLStartTag ();
    serializer.writeBean (object);
    serializer.writeKBMLEndTag ();
    serializer.flush ();

    if (compress)
      ((GZIPOutputStream)output).finish ();
  }

  /**
   * Open a compressed output stream.  Ensure a call to {@link
   * #finishOutput} is made before closing or the stream will be
   * invalid.
   */
  public static OutputStream openCompressedOutput (OutputStream output)
    throws IOException
  {
    return new GZIPOutputStream (output);
  }

  /**
   * Call this to finalize compressed output streams.  Has no effect
   * on normal streams.
   */
  public static void finishOutput (OutputStream output)
    throws IOException
  {
    if (output instanceof GZIPOutputStream)
      ((GZIPOutputStream)output).finish ();
  }

  /**
   * Write an object to an output stream using the appropriate writer.
   *
   * @param object The object to write.
   * @param output The stream.
   * @param format The format.  Only the type is used.
   * @exception IOException if an error occurs
   */
  public static void writeObject (Object object, OutputStream output,
                                  SerializedFormat format)
    throws IOException
  {
    if (format.getType ().equals (SerializedFormat.FORMAT_XML))
    {
      KBMLSerializer serializer = new KBMLSerializer (output);

      serializer.writeKBMLStartTag ();
      serializer.writeBean (object);
      serializer.writeKBMLEndTag ();
      serializer.flush ();
    } else if (format.getType ().equals (SerializedFormat.FORMAT_BINARY))
    {
      ObjectOutput objectOutput;

      if (output instanceof ObjectOutput)
        objectOutput = (ObjectOutput)output;
      else
        objectOutput = new ObjectOutputStream (output);

      objectOutput.writeObject (object);
    } else
    {
      throw new IOException ("Unrecognised format: " + format.getType ());
    }
  }
}
