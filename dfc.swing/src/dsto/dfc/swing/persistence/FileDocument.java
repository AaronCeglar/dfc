package dsto.dfc.swing.persistence;

import java.awt.Window;
import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;

import dsto.dfc.swing.controls.ExtensionFileFilter;

import dsto.dfc.util.Files;

/**
 * A base class for documents that are file-based.  The default
 * implementations of {@link #readModel} and {@link #writeModel} use the
 * {@link Deserializer} and {@link Serializer} classes, but these may be
 * overridden to provide other serialization mechanisms.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public abstract class FileDocument extends AbstractDocument
{
  protected File file;
  protected SerializedFormat format =
    new SerializedFormat (SerializedFormat.FORMAT_XML, false);

  public FileDocument ()
  {
    // zip
  }

  protected abstract Object createModelInstance ();

  /**
   * The default extension to apply to files with no explicit extension.
   */
  protected abstract String getDefaultExtension ();

  public abstract String getType ();

  public File getFile ()
  {
    return file;
  }

  public void setFile (File newValue)
  {
    setLocation (newValue);
  }

  /**
   * Customize a file chooser before it is displayed. May be overridden.
   */
  protected void customizeChooser (String mode, JFileChooser chooser)
  {
    chooser.setDialogTitle (mode + " " + getType ());

    if (mode.equals ("Save"))
    {
      SerializeAccessoryPanel accessoryPanel = new SerializeAccessoryPanel ();
      accessoryPanel.setType (format.getType ());
      accessoryPanel.setCompress (format.isCompressed ());
      chooser.setAccessory (accessoryPanel);
      chooser.putClientProperty ("save_accessory", accessoryPanel);
    }

    if (file != null)
      chooser.setSelectedFile (file);
  }

  /**
   * Customize the default file filter used in file choosers.
   */
  protected void customizeFileFilter (ExtensionFileFilter filter)
  {
    // zip
  }

  // AbstractDocument implementation

  protected void setLocation (Object newValue)
  {
    File oldValue = file;
    String oldName = getName ();

    file = (File)newValue;

    firePropertyChange ("file", oldValue, newValue);
    firePropertyChange ("name", oldName, getName ());
  }

  protected Object getLocation ()
  {
    return file;
  }

  protected Object selectLocation (String mode)
  {
    JFileChooser chooser = new JFileChooser ();

    customizeChooser (mode, chooser);

    ExtensionFileFilter filter = new ExtensionFileFilter ();
    customizeFileFilter (filter);
    chooser.addChoosableFileFilter (filter);

    Window window = SwingUtilities.windowForComponent (getClient ());

    if (chooser.showDialog (window, mode) == JFileChooser.APPROVE_OPTION)
    {
      File actualFile =
        Files.addDefaultExtension (chooser.getSelectedFile (),
                                   getDefaultExtension ());

      if (mode.equals ("Save"))
      {
        SerializeAccessoryPanel accessoryPanel =
          (SerializeAccessoryPanel)chooser.getClientProperty ("save_accessory");

        format.setType (accessoryPanel.getType ());
        format.setCompressed (accessoryPanel.isCompress ());
      }

      return actualFile;
    } else
      return null;
  }

  /**
   * Read a model using {@link Deserializer}.  May be overridden to enable
   * alternative deserialization mechanism.
   */
  protected Object readModel (Object location) throws IOException
  {
    File fileLocation = (File)location;

    try
    {
      return Deserializer.read (fileLocation, format);
    } catch (ClassNotFoundException ex)
    {
      throw new IOException ("Failed to resolve class in archive: " +
                              ex.getMessage ());
    }
  }

  /**
   * Write a model using {@link Serializer}.  May be overridden to enable
   * alternative serialization mechanism.
   */
  protected void writeModel (Object location, Object theModel)
    throws IOException
  {
    File fileLocation = (File)location;

    if (format.getType ().equals (SerializedFormat.FORMAT_XML))
    {
      Serializer.saveAsKBML (theModel, fileLocation.getPath (), format.isCompressed ());
    } else
    {
      Serializer.saveAsBinary (theModel, fileLocation.getPath (), format.isCompressed ());
    }
  }

  public Object newModel ()
  {
    setFile (null);

    // restore save format defsults
    format.setCompressed (false);
    format.setType (SerializedFormat.FORMAT_XML);

    return super.newModel ();
  }

  /**
   * Returns the file name without extension if one is set.
   */
  public String getName ()
  {
    if (file != null)
      return Files.removeExtension (file.getName ());
    else
      return null;
  }
}