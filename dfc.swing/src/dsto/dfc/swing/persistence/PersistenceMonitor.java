package dsto.dfc.swing.persistence;

import dsto.dfc.databeans.IDataObject;
import dsto.dfc.databeans.PropertyEvent;
import dsto.dfc.databeans.PropertyListener;
import dsto.dfc.logging.Log;
import dsto.dfc.util.WeakEventListener;

/**
 * Connects changes to an IDataObject/IDataBean to mark the object changed in
 * a {@link dsto.dfc.swing.persistence.PersistenceManager}.
 * 
 * @author mpp
 * @version $Revision$
 */
public class PersistenceMonitor implements PropertyListener
{
  private PersistenceManager manager;
  private WeakEventListener listener;

  /**
   * Create a new instance.
   * 
   * @param manager The persistence manager that the object is registered with.
   * @param object The peristent object.
   * @param weakListener If true, the monitor will register itself as a weak
   * listener on object, allowing object to be GC'd when nothing else refers
   * to it.
   */
  public PersistenceMonitor (PersistenceManager manager,
                             IDataObject object, boolean weakListener)
  {
    this.manager = manager;
   
    if (weakListener) 
    {
      listener =
        WeakEventListener.createListener (PropertyListener.class, object, this);
    }
    else
    {
      object.addPropertyListener (this);
    }
  }
  
  public void propertyValueChanged (PropertyEvent e)
  {
    // for debugging
    // System.out.println ("changed " + e.getName () + " (transient " + e.isTransientProperty () + ")");
    Object source = e.getSource ();
    
    try
    {
      if (e.transientProperty)
        manager.objectAccessed (source);
      else
        manager.objectModified (source);
    } catch (IllegalArgumentException ex)
    {
      // object is not persistent: stop listening
      Log.diagnostic ("Object is not persistent: disconnecting", this, ex);
      
      if (listener != null)
        listener.dispose ();
      else
        ((IDataObject)source).removePropertyListener (this);
    }
  }
}
