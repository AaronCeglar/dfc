package dsto.dfc.swing.text;

import java.util.List;

/**
 * A condition containing a set of child conditions.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public interface CompositeTerm extends Term
{
  public List getTerms ();

  public void setTerms (List newValue);
}
