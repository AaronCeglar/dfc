package dsto.dfc.swing.text;

import dsto.dfc.util.IllegalFormatException;

/**
 * Defines a class that provides conversion services from one form to
 * another (eg from int to String).
 *
 * @version $Revision$
 */
public interface ValueConverter
{
  /**
   * Convert a value to a given class.
   *
   * @param value The value to convert.
   * @param form The new class.
   * @return A new instance of form containing the converted value.
   * @exception IllegalFormatException if conversion was not possible.
   */
  public Object convertTo (Object value, Class form)
    throws IllegalFormatException;
} 
