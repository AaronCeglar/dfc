package dsto.dfc.swing.text;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import dsto.dfc.swing.controls.JTitledSeparator;
import dsto.dfc.swing.forms.EnumFormEditor;
import dsto.dfc.swing.forms.FormPanel;

/**
 * Base class for term customizers that select from a set of enumerated values.
 * Provides an EnumFormEditor for displaying/editing values.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class BasicEnumeratedTermCustomizer extends FormPanel
{
  protected EnumFormEditor editor = new EnumFormEditor ();
  private GridBagLayout gridBagLayout1 = new GridBagLayout();
  private JTitledSeparator separator = new JTitledSeparator();

  public BasicEnumeratedTermCustomizer ()
  {
    this (null, "Editor");
  }

  /**
   * Creates a new <code>BasicEnumeratedTermCustomizer</code> instance.
   *
   * @param property The property of the term that the enumeration editor
   * binds to.
   * @param label The label for the editor.
   */
  public BasicEnumeratedTermCustomizer (String property, String label)
  {
    if (property != null)
      addEditor (property, editor);

    jbInit ();

    separator.setTitle (label);
  }

  private void jbInit ()
  {
    setLayout (gridBagLayout1);

    separator.setTitle("Editor");
    add (editor, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 12, 0, 0), 0, 0));
    this.add(separator, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
  }
}
