package dsto.dfc.swing.text.selection;

import java.io.Serializable;

import dsto.dfc.swing.text.ValueConverter;
import dsto.dfc.util.IllegalFormatException;

/**
 * Defines an object that can be matched against other objects.
 * Selectors follow the immutable pattern and should not be modified
 * after there is any possibilty that other objects hold a reference
 * to them.
 *
 * @version $Revision$
 */
public interface Selector
{
  public static final SelectAllSelector SELECT_ALL = new SelectAllSelector ();

  /**
   * True if this selector matches the given value.
   */
  public boolean matches (Object value);

  public static final class SelectAllSelector
    implements Selector, ValueConverter, Serializable
  {
    private static final long serialVersionUID = -1L;

    public boolean matches (Object value)
    {
      return true;
    }

    public ValueConverter getConverter ()
    {
      return this;
    }

    public Object convertTo (Object value, Class form)
      throws IllegalFormatException
    {
      if (form.equals (SelectAllSelector.class))
        return this;
      else if (form.equals (String.class))
        return toString ();
      else
        throw new IllegalFormatException (this, value, form);
    }

    @Override
    public boolean equals (Object o)
    {
      return o instanceof SelectAllSelector;
    }

    @Override
    public int hashCode ()
    {
      return 83;
    }
  }
}
