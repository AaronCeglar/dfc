package dsto.dfc.swing.text.selection;

import dsto.dfc.util.IllegalFormatException;

import dsto.dfc.swing.text.ValueConverter;

/**
 * Simple selector matches values string-wise using toString () and equals ().
 *
 * @version $Revision$
 */
public class StringSelector implements PatternSelector
{
  public static final ValueConverter CONVERTER = new Converter ();

  protected String string;
  protected boolean caseSensitive;

  public StringSelector ()
  {
    this ("");
  }

  public StringSelector (String string)
  {
    this (string, true);
  }

  public StringSelector (String string, boolean caseSensitive)
  {
    this.string = string;
    this.caseSensitive = caseSensitive;
  }

  public void setExpression (String expression)
  {
    string = expression;
  }

  public String getExpression ()
  {
    return string;
  }

  public void setCaseSensitive (boolean newValue)
  {
    caseSensitive = newValue;
  }

  public boolean isCaseSensitive ()
  {
    return caseSensitive;
  }

  public boolean matches (Object value)
  {
    if (caseSensitive)
      return string.equals (value.toString ());
    else
      return string.equalsIgnoreCase (value.toString ());
  }

  public String toString ()
  {
    return string;
  }

  public ValueConverter getConverter ()
  {
    return CONVERTER;
  }

  static final class Converter implements ValueConverter
  {
    public Object convertTo (Object value, Class form)
      throws IllegalFormatException
    {
      if (form.equals (String.class) && value instanceof StringSelector)
      {
        return value.toString ();
      } else if (form.equals (StringSelector.class) &&
                 value instanceof String)
      {
        return new StringSelector ((String)value);
      } else
        throw new IllegalFormatException (this, value, form);
    }
  }
}
