package dsto.dfc.swing.text.selection;

import java.util.ArrayList;

/**
 * Pattern for NumberSelector's: represented as a set of Range values.
 *
 * @version $Revision$
 */
public class NumberSelectorPattern
{
  private ArrayList ranges = new ArrayList (5);

  public NumberSelectorPattern ()
  {
    // zip
  }

  public void addRange (Range range)
  {
    ranges.add (range);
  }

  public boolean matches (Object value)
  {
    if (value instanceof Integer)
    {
      return matches (((Integer)value).doubleValue ());
    } else if (value instanceof Float)
    {
      return matches (((Float)value).doubleValue ());
    } else if (value instanceof Double)
    {
      return matches (((Double)value).doubleValue ());
    } else
      return false;
  }
  
  public boolean matches (double value)
  {
    for (int i = 0; i < ranges.size (); i++)
    {
      Range range = (Range)ranges.get (i);

      if (range.matches (value))
        return true;
    }

    return false;
  }

  public static final class Range
  {
    public boolean lowerInc;
    public boolean upperInc;
    public double lower;
    public double upper;

    /**
     * Create a range that includes all values. 
     */
    public Range ()
    {
      this (true, -Double.MAX_VALUE, Double.MAX_VALUE, true); 
    }
    
    public Range (double lower, double upper)
    {
      this (true, lower, upper, true);
    }

    public Range (boolean lowerInc, double lower,
                  double upper, boolean upperInc)
    {
      this.lowerInc = lowerInc;
      this.upperInc = upperInc;
      this.lower = lower;
      this.upper = upper;
    }

    public boolean matches (double value)
    {
      return ((lowerInc && value >= lower) || (!lowerInc && value > lower)) &&
             ((upperInc && value <= upper) || (!upperInc && value < upper));

    }
  }
}
