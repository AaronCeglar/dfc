package dsto.dfc.swing.text.selection;

import java.util.Date;


import dsto.dfc.swing.text.Convertable;
import dsto.dfc.swing.text.ValueConverter;

/**
 * Factory class which provides factory methods for creation of
 * PatternSelector implementations.
 *
 * @author stewartr
 * @author mpp
 * @version $Revision$
 * 
 * @see PatternSelector
 * @see Selector
 **/
public final class PatternSelectorFactory
{
  private PatternSelectorFactory ()
  {
    // zip
  }

  /**
   * Get a value converter suitable for converting between a pattern selector
   * for the given value type and (at the minimum) strings.
   */
  public static ValueConverter getPatternConverter (Class valueType)
  {
    // todo could make this a bit quicker if needed
    return ((Convertable)createSelector (valueType)).getConverter ();
  }
  
  /**
   * Get a the default pattern for a selector for the given class.
   * 
   * @see #createSelector(Class)
   */
  public static final String defaultValue (Class valueClass)
  {
    // todo could make this a bit quicker if needed
    return createSelector (valueClass).getExpression ();
  }

  /**
   * Get a PatternSelector instance suitable for performing pattern
   * matching on a given value.
   * 
   * @see #createSelector(Class)
   * @see #getPatternType(Class)
   **/
  public static PatternSelector createSelector (Object value)
  {
    return createSelector (value.getClass ());
  }

  /**
   * Get a PatternSelector instance suitable for performing pattern
   * matching on a given value type.
   * 
   * @see #createSelector(Object)
   * @see #getPatternType(Class)
   **/
  public static PatternSelector createSelector (Class valueClass)
  {
    try
    {
      return (PatternSelector)getPatternType (valueClass).newInstance ();
    } catch (InstantiationException ex)
    {
      throw new Error ("Failed to create pattern selector: " + ex);
    } catch (IllegalAccessException ex)
    {
      throw new Error ("Pattern selector constructor not public: " + ex);
    }
  }

  /**
   * For a given value type, return an appropriate pattern selector class.
   * 
   * @param valueClass The type of value the selector will be looking at
   * (int, string, date, etc).
   * @return The pattern selector class. The 
   */
  public static Class getPatternType (Class valueClass)
  {
    if (Number.class.isAssignableFrom (valueClass) ||
        valueClass.isAssignableFrom (int.class) ||
        valueClass.isAssignableFrom (long.class) ||
        valueClass.isAssignableFrom (short.class)||
        valueClass.isAssignableFrom (float.class) ||
        valueClass.isAssignableFrom (double.class))
      return NumberSelector.class;
    
    else if (valueClass.isAssignableFrom(Boolean.class)||
             valueClass.isAssignableFrom(boolean.class))
      return BooleanSelector.class;

    else if (Date.class.isAssignableFrom (valueClass))
      return DateSelector.class;

    else 
      return RegexSelector.class;
  }
}