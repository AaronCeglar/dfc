package dsto.dfc.swing.text.selection;

import java.util.Collection;
import java.util.Iterator;

/**
 * A selector that applies a sub selector to match against
 * Collection's.
 *
 * @version $Revision$
 */
public class CollectionSelector implements Selector
{
  public static final int MATCH_ONE = 0;
  public static final int MATCH_ALL = 1;

  protected int mode;
  protected Selector subSelector;

  public CollectionSelector (Selector subSelector)
  {
    this (subSelector, MATCH_ONE);
  }
  
  public CollectionSelector (Selector subSelector, int mode)
  {
    this.mode = mode;
    this.subSelector = subSelector;
  }

  public boolean matches (Object value)
  {
    if (value instanceof Collection)
    {
      Collection collection = (Collection)value;

      for (Iterator i = collection.iterator (); i.hasNext (); )
      {
        Object entry = i.next ();

        if (subSelector.matches (entry))
        {
          if (mode == MATCH_ONE)
            return true;
        } else
        {
          if (mode == MATCH_ALL)
            return false;
        }
      }
    }

    return mode == MATCH_ALL;
  }
}
