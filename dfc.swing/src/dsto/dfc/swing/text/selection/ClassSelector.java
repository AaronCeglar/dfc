package dsto.dfc.swing.text.selection;

/**
 * A selector that matches compatible classes.
 *
 * @version $Revision$
 */
public class ClassSelector implements Selector
{
  protected Class matchClass;

  public ClassSelector (Class matchClass)
  {
    this.matchClass = matchClass;
  }

  public boolean matches (Object value)
  {
    return matchClass.isAssignableFrom (value.getClass ());
  }
} 
