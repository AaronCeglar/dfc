package dsto.dfc.swing.text.selection;

import java.io.Serializable;

import dsto.dfc.swing.text.Convertable;
import dsto.dfc.swing.text.ValueConverter;
import dsto.dfc.util.IllegalFormatException;

/**
 * Selects values based on a pattern.
 *
 * @version $Revision$
 */
public interface PatternSelector extends Selector, Convertable
{
  public static final SelectAllSelector SELECT_ALL = new SelectAllSelector ();

  /**
   * True if this pattern selector matches the given value.
   */
  public boolean matches (Object value);


  /**
   *  Set the value of the pattern used to match against.
   *
   * @param expression String value
   */
  public void setExpression (String expression)
    throws IllegalFormatException;

  public String getExpression ();

  /**
   * Return a converter that at least supports conversion to and from String.
   */
  public ValueConverter getConverter ();

  public static final class SelectAllSelector
    implements PatternSelector, ValueConverter, Serializable
  {
    private static final long serialVersionUID = -3137530613812416784L;

    public boolean matches (Object value)
    {
      return true;
    }

    public ValueConverter getConverter ()
    {
      return this;
    }

    public Object convertTo (Object value, Class form)
      throws IllegalFormatException
    {
      if (form.equals (SelectAllSelector.class))
        return this;
      else if (form.equals (String.class))
        return toString ();
      else
        throw new IllegalFormatException (this, value, form);
    }

    @Override
    public boolean equals (Object o)
    {
      return o instanceof SelectAllSelector;
    }

    @Override
    public int hashCode ()
    {
      return 17;
    }

    public void setExpression (String expression)
      throws IllegalFormatException
    {
      // zip: no pattern
    }

    public String getExpression ()
    {
      return "";
    }

    @Override
    public String toString ()
    {
      return "";
    }
  }
}
