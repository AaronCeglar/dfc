package dsto.dfc.swing.text.selection;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

import org.apache.oro.text.regex.MalformedPatternException;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.PatternCompiler;
import org.apache.oro.text.regex.PatternMatcher;
import org.apache.oro.text.regex.Perl5Compiler;
import org.apache.oro.text.regex.Perl5Matcher;

import dsto.dfc.logging.Log;

import dsto.dfc.swing.text.Convertable;
import dsto.dfc.swing.text.ValueConverter;

import dsto.dfc.util.Copyable;
import dsto.dfc.util.IllegalFormatException;
import dsto.dfc.util.Singleton;

/**
 * Selector that uses Perl 5 compatible regular expressions.
 * Implemented using ORO Matcher 1.1
 * http://www.quateams.com/oro/software/OROMatcher1.1.html.
 *
 * @author  mpp
 * @created  12th October 1999
 * @version $Revision$
 */
public class RegexSelector
  implements PatternSelector, Convertable, Copyable, Serializable
{
  private static final long serialVersionUID = 6622692369442327761L;

  public static final ValueConverter CONVERTER = new Converter ();
  private static final PatternCompiler compiler = new Perl5Compiler ();
  private static final PatternMatcher matcher = new Perl5Matcher ();

  private String regex;
  private boolean caseSensitive;
  private transient Pattern pattern;

  public RegexSelector () throws IllegalFormatException
  {
    this ("");
  }

  public RegexSelector (String regex) throws IllegalFormatException
  {
    this (regex, true);
  }

  public RegexSelector (String regex, boolean caseSensitive)
    throws IllegalFormatException
  {
    try
    {
      int options = Perl5Compiler.MULTILINE_MASK;

      if (!caseSensitive)
        options |= Perl5Compiler.CASE_INSENSITIVE_MASK;

      this.pattern = compiler.compile (regex, options);
      this.regex = regex;
      this.caseSensitive = caseSensitive;
    } catch (MalformedPatternException ex)
    {
      throw new IllegalFormatException (this, ex.getMessage (),
                                        regex, RegexSelector.class);
    }
  }

  public boolean matches (Object value)
  {
    return value != null && matcher.contains (value.toString (), pattern);
  }

  public String toString ()
  {
    return pattern.getPattern ();
  }

  public ValueConverter getConverter ()
  {
    return CONVERTER;
  }

  public String getExpression ()
  {
    return regex;
  }

  /**
   * Set the regex expression
   */
  public void setExpression (String regex) throws IllegalFormatException
  {
    try
    {
      int options = Perl5Compiler.MULTILINE_MASK;
      if (!caseSensitive)
        options |= Perl5Compiler.CASE_INSENSITIVE_MASK;
      
      this.pattern = compiler.compile (regex, options);
      this.regex = regex;
    } catch (MalformedPatternException ex)
    {
      throw new IllegalFormatException (this, ex.getMessage (),
                                        regex, RegexSelector.class);
      //ex.printStackTrace ();
    }
  }

  public Object clone () throws CloneNotSupportedException
  {
    return super.clone ();
  }

  private void readObject (ObjectInputStream in)
      throws IOException, ClassNotFoundException
  {
    in.defaultReadObject ();

    try
    {
      pattern = compiler.compile (regex);
    } catch (MalformedPatternException ex)
    {
      Log.warn ("regex pattern failed to compile while de-serializing", this, ex);
    }
  }

  static final class Converter
    implements ValueConverter, Singleton, Serializable
  {
    private static final long serialVersionUID = -8511250007308718151L;

    public Object convertTo (Object value, Class form)
      throws IllegalFormatException
    {
      if (form.equals (String.class) && value instanceof RegexSelector)
      {
        return value.toString ();
      } else if (form.equals (RegexSelector.class) &&
                 value instanceof String)
      {
        return new RegexSelector ((String)value);
      } else
        throw new IllegalFormatException (this, value, form);
    }

    private Object readResolve ()
    {
      return CONVERTER;
    }

    public static ValueConverter getSingletonInstance ()
    {
      return CONVERTER;
    }
  }
}
