package dsto.dfc.swing.text.selection;

import java.io.File;
import java.io.FileFilter;

/**
 * A file filter that uses a {@link Selector} to select files.  File names
 * are passed in as strings, using "/" as the path separator.
 *
 * @author Matthew Phillips
 * @version $Revision$
 *
 * @see SelectorFileFilter
 */
public class SelectorFileFilter implements FileFilter
{
  protected Selector selector;

  public SelectorFileFilter (Selector selector)
  {
    this.selector = selector;
  }

  public boolean accept (File pathname)
  {
    return selector.matches (pathname.toString ().replace ('\\', '/'));
  }
}