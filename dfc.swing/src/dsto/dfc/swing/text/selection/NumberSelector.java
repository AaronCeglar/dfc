package dsto.dfc.swing.text.selection;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

import dsto.dfc.util.IllegalFormatException;
import dsto.dfc.util.Singleton;

import dsto.dfc.swing.text.Convertable;
import dsto.dfc.swing.text.ValueConverter;

/**
 * Uses patterns such as "[0:1],10,>=12,(13:*],<200" to match numbers
 * (int, float and double).  When using ranges, '(' and ')' indicate
 * non-inclusivity, '[' and ']' indicate inclusivity.
 *
 * Grammar:
 * <pre>
 * <code>
 *   &lt;expr&gt;       ::= &lt;term&gt; (',' &lt;term&gt;)*
 *   &lt;term&gt;       ::= &lt;comparison&gt; | &lt;range&gt;
 *   &lt;comparison&gt; ::= ('>' | '>=' | '<' | '<=') &lt;number&gt;
 *   &lt;range&gt;      ::= &lt;value&gt; |  ( '[' | '(' ) &lt;value&gt; ':' &lt;value&gt; ( ')' | ']' )
 *   &lt;value&gt;      ::= '*' | &lt;number&gt;
 *   &lt;number&gt;     ::= [+-][0-9]+['.' [0-9]+]
 * </code>
 * </pre>
 *
 * @version $Revision$
 */
public class NumberSelector implements PatternSelector, Convertable, Serializable
{
  private static final long serialVersionUID = 1;

  /** ValueConverter for this class: converts to/from String. */
  public static final ValueConverter CONVERTER = new Converter ();

  protected static final String USAGE_TEXT =
    "<term> [',' <term>]...\n" +
    "Where a <term> is: \n" +
    "  * ['>' '<' '<=' '>='] <number>\n" +
    "  * ['[' '('] <number> ':' <number> [']' ')']\n" +
    "    a range where '(' is exclusive and '[' is inclusive\n" +
    "  * <number> may be a real value (with sign) or '*' to match any value";

  protected String expr;
  protected transient NumberSelectorPattern pattern;

  public NumberSelector ()
  {
    this ("*");
  }

  public NumberSelector (String expr)
    throws IllegalFormatException
  {
    setExpression (expr);
  }

  public ValueConverter getConverter ()
  {
    return CONVERTER;
  }

  public boolean matches (Object value)
  {
    return value != null && pattern.matches (value);
  }

  public boolean matches (double value)
  {
    return pattern.matches (value);
  }

  public String toString ()
  {
    return getExpression();
  }

  public String getExpression()
  {
    return expr;
  }

  public void setExpression (String newExpr) throws IllegalFormatException
  {
    this.expr = newExpr;

    compileExpr (expr);
  }

  protected static String makeErrorMessage (Throwable ex)
  {
    StringBuffer message = new StringBuffer ();
    message.append ("Invalid expression (");
    message.append (ex.getMessage ());
    message.append (").\nUsage:\n");
    message.append (USAGE_TEXT);

    return message.toString ();
  }

  protected void compileExpr (String exprStr) throws IllegalFormatException
  {
    exprStr = exprStr.trim ();

    if (exprStr.length () == 0 || exprStr.equals ("*"))
    {
      this.pattern = new NumberSelectorPattern ();
      this.pattern.addRange (new NumberSelectorPattern.Range ());
    } else
    {
      ByteArrayInputStream input = new ByteArrayInputStream (exprStr.getBytes ());
      NumberSelectorCompiler compiler = new NumberSelectorCompiler (input);

      try
      {
        compiler.compile ();

        this.pattern = compiler.getPattern ();
      } catch (ParseException ex)
      {
        throw new IllegalFormatException (this, makeErrorMessage (ex),
                                          exprStr, NumberSelector.class);
      } catch (TokenMgrError ex)
      {
        throw new IllegalFormatException (this, makeErrorMessage (ex),
                                          exprStr, NumberSelector.class);
      }
    }
  }

  private void readObject (ObjectInputStream in)
    throws IOException, ClassNotFoundException
  {
    in.defaultReadObject ();

    try
    {
      compileExpr (expr);
    } catch (IllegalFormatException ex)
    {
      throw new IOException ("Failed to recompile numeric expression: " + ex);
    }
  }

  static final class Converter
    implements ValueConverter, Singleton, Serializable
  {
    private static final long serialVersionUID = 5640557254427487654L;

    public Object convertTo (Object value, Class form)
      throws IllegalFormatException
    {
      if (value instanceof NumberSelector && form.equals (String.class))
      {
        return ((NumberSelector)value).expr;
      } else if (form.equals (NumberSelector.class) &&
                 value instanceof String)
      {
        return new NumberSelector ((String)value);
      } else
        throw new IllegalFormatException (this, value, form);
    }

    private Object readResolve ()
    {
      return CONVERTER;
    }

    public static ValueConverter getSingletonInstance ()
    {
      return CONVERTER;
    }
  }
}
