package dsto.dfc.swing.text.selection;

import java.io.File;
import java.io.FilenameFilter;

/**
 * A file filter that uses a {@link Selector} to select files by name only
 * (see {@link SelectorFileFilter} for a class that selects using the whole
 * file name).  File names are passed in as strings.
 *
 * @author Matthew Phillips
 * @version $Revision$
 *
 * @see SelectorFileFilter
 */
public class SelectorFilenameFilter implements FilenameFilter
{
  protected Selector selector;

  public SelectorFilenameFilter (Selector selector)
  {
    this.selector = selector;
  }

  public boolean accept (File dir, String filename)
  {
    return selector.matches (filename);
  }
}