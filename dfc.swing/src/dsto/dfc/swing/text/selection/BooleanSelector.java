package dsto.dfc.swing.text.selection;

import java.io.Serializable;

import dsto.dfc.util.Copyable;
import dsto.dfc.util.IllegalFormatException;
import dsto.dfc.util.Singleton;

import dsto.dfc.swing.text.Convertable;
import dsto.dfc.swing.text.ValueConverter;

/**
 * Selector that matches booleans. Valid values are "true", "false", "T",
 * and "F", case-insensitive. "1" and "0" are not valid values.
 * 
 * @author  weberd
 * @created  22/01/2003
 * @version $Revision$
 */
public class BooleanSelector
  implements PatternSelector, Convertable, Copyable, Serializable
{
  private static final long serialVersionUID = 0L;

  public static final ValueConverter CONVERTER = new Converter ();

  private String pattern;

  public BooleanSelector () throws IllegalFormatException
  {
    this ("true");
  }

  public BooleanSelector (String value) throws IllegalFormatException
  {
    setExpression (value);
  }

  public boolean matches (Object value)
  {
    if (value instanceof String)
    {
      String str = (String) value;
      if (str.equalsIgnoreCase ("y") || str.equalsIgnoreCase ("yes"))
        str = "true";
      else if (str.equalsIgnoreCase ("n") || str.equalsIgnoreCase ("no"))
        str = "false";
        
      if (str.length () == 1)
        return pattern.startsWith (str.toLowerCase ());
      else
        return pattern.equalsIgnoreCase (str) ;
    }
    
    else if (value != null)
      return pattern.equalsIgnoreCase (value.toString ());
      
    else
      return false;
  }

  public String toString ()
  {
    return pattern;
  }

  public ValueConverter getConverter ()
  {
    return CONVERTER;
  }

  public String getExpression ()
  {
    return pattern;
  }

  /**
   * Set the boolean value. "True", "False", "T", and "F" are valid values, 
   * case-insensitively, but "1" and "0" are not.
   */
  public void setExpression (String value) throws IllegalFormatException
  {
    if (value.equalsIgnoreCase ("true") || value.equalsIgnoreCase ("t") ||
        value.equalsIgnoreCase ("yes")  || value.equalsIgnoreCase ("y"))
      pattern = "true";
    else if (value.equalsIgnoreCase ("false") || value.equalsIgnoreCase ("f") ||
             value.equalsIgnoreCase ("no")    || value.equalsIgnoreCase ("n"))
      pattern = "false";
    else 
      throw new IllegalFormatException 
        (this, "Not a valid pattern value - please use [true,T,false,F,yes,Y,no,N]",
         value, BooleanSelector.class);
  }

  public Object clone () throws CloneNotSupportedException
  {
    return super.clone ();
  }

  static final class Converter
    implements ValueConverter, Singleton, Serializable
  {
    private static final long serialVersionUID = 0L;

    public Object convertTo (Object value, Class form)
      throws IllegalFormatException
    {
      if (form.equals (String.class) && value instanceof BooleanSelector)
        return value.toString ();

      else if (form.equals (BooleanSelector.class) && value instanceof String)
        return new BooleanSelector ((String) value);

      else
        throw new IllegalFormatException (this, value, form);
    }

    private Object readResolve () { return CONVERTER; }

    public static ValueConverter getSingletonInstance () { return CONVERTER; }
  }
}
// EOF