package dsto.dfc.swing.text.selection;

import java.util.Date;

import java.io.Serializable;

import java.text.ParsePosition;

import dsto.dfc.util.IllegalFormatException;
import dsto.dfc.util.Singleton;

import dsto.dfc.text.IsoDateFormat;
import dsto.dfc.text.StringUtility;

import dsto.dfc.swing.text.ValueConverter;

/**
 * A basic selector for java.util.Date objects. Currently allows "*" or an ISO
 * date (yyyy-mm-dd hh:mm:ss timezone or shortened versions of the same) as a
 * selector. Multiple dates can OR'd with ",". Ranges (inclusive) can be
 * specified using syntax like "date1 > date2".<p>
 * 
 * Examples:<p>
 * 
 * <pre>
 *   "*"
 *   "2001"
 *   "2001-01-02"
 *   "2001-01-02 10:14:01" (uses local timezone)
 *   "2001-01-02 10:14:01 UTC+9:30"
 *   "2001>2002,2004>2005"
 * </pre>
 * 
 * @see dsto.dfc.text.IsoDateFormat
 * 
 * @author Matthew Phillips
 */
public class DateSelector implements PatternSelector, Serializable
{
  private static final long serialVersionUID = 1;
  
  private static final String USAGE =
    "Need ISO date (yyyy-mm-dd [hh:mm:ss [timezone]]). " +
    "Can use ',' for multiple dates, 'date1 > date2' for ranges.";
  
  /** ValueConverter for this class: converts to/from String. */
  public static final ValueConverter CONVERTER = new Converter ();
  
  protected String expression;
  /** A set of time (Date.getTime ()) [lower, upper] pairs.
   *  Null if expression == "*". */
  protected transient long [] dateRanges; 
  
  /**
   * Create a default "select all" selector.
   */
  public DateSelector ()
  {
    this ("*");
  }
  
  /**
   * Create an instance using an expression. See class doc for format.
   */
  public DateSelector (String expression)
    throws IllegalArgumentException
  {
    this.expression = expression;
    
    parseExpr ();
  }
 
  /**
   * Parse the expression into the dateRanges array if needed.
   */
  private void parseExpr ()
    throws IllegalArgumentException
  {
    if (dateRanges != null || expression.equals ("*"))
      return;
    
    String [] subExprs = StringUtility.split (expression, ",");
    dateRanges = new long [subExprs.length * 2];

    try
    {
      for (int i = 0; i < subExprs.length; i++)
      {
        parseSubExpr (i, subExprs [i]);
      }
    } catch (IllegalArgumentException ex)
    {
      throw new IllegalArgumentException
        ("Error parsing date expression \"" + expression + "\" (" + ex + "). " +
         USAGE);
    }
  }

  /**
   * Parse a subexpression: either a date or a date range ("date1 > date2").
   */
  private void parseSubExpr (int rangeIndex, String expr)
    throws IllegalArgumentException
  {
    String [] ranges = StringUtility.split (expr, ">");
    
    if (ranges.length == 1)
    {
      Date date = parseDate (expression);
      
      dateRanges [rangeIndex] = date.getTime ();
      dateRanges [rangeIndex + 1] = date.getTime ();
    } else if (ranges.length == 2)
    {
      Date date1 = parseDate (ranges [0]);
      Date date2 = parseDate (ranges [1]);
      
      dateRanges [rangeIndex] = date1.getTime ();
      dateRanges [rangeIndex + 1] = date2.getTime ();
    } else
    {
      throw new IllegalArgumentException ("Too many >'s");
    }
  }

  /**
   * Parse a date and throw error if invalid date or extra crud at end.
   */
  private Date parseDate (String dateExpr)
    throws IllegalArgumentException
  {
    dateExpr = dateExpr.trim ();

    ParsePosition pos = new ParsePosition (0);
    
    Date date = IsoDateFormat.get ().parse (dateExpr, pos);
    
    if (pos.getErrorIndex () != -1)
    {  
      throw new IllegalArgumentException
        ("Error in date starting at \"" +
            dateExpr.substring (pos.getErrorIndex ()) + "\". " + USAGE);
    } else if (pos.getIndex () != dateExpr.length ())
    {  
      throw new IllegalArgumentException
        ("Extra characters at end of date: \"" +
            dateExpr.substring (pos.getIndex ()) + "\". " + USAGE);
    }
    
    return date;
  } 
    
  public boolean matches (Object value)
  {
    parseExpr ();
    
    if (dateRanges == null)
    {  
      return true;
    } else
    {
      long time = ((Date)value).getTime ();
      
      for (int i = 0; i < dateRanges.length; i++) 
      {
        // note the ++ in if statement. yes, dodgy
        if (time >= dateRanges [i++] && time <= dateRanges [i])
          return true;
      }
      
      return false;
    }
  }

  public void setExpression (String expression)
    throws IllegalArgumentException
  {
    this.expression = expression;
    this.dateRanges = null;
    
    parseExpr ();
  }

  public String getExpression ()
  {
    return expression;
  }

  public ValueConverter getConverter ()
  {
    return CONVERTER;
  }
  
  public String toString ()
  {
    return getExpression ();
  }
  
  static final class Converter
    implements ValueConverter, Singleton, Serializable
  {
    private static final long serialVersionUID = 1L;

    public Object convertTo (Object value, Class form)
      throws IllegalFormatException
    {
      if (value instanceof DateSelector && form.equals (String.class))
      {
        return ((DateSelector)value).expression;
      } else if (form.equals (DateSelector.class))
      {
        try
        {
          return new DateSelector (value.toString ());
        } catch (IllegalArgumentException ex)
        {
          throw new IllegalFormatException (this, value, form, ex);
        }
      } else
      {  
        throw new IllegalFormatException (this, value, form);
      }
    }

    private Object readResolve ()
    {
      return CONVERTER;
    }

    public static ValueConverter getSingletonInstance ()
    {
      return CONVERTER;
    }
  }
}
