package dsto.dfc.swing.text.selection;

import dsto.dfc.util.IllegalFormatException;

import dsto.dfc.swing.text.ValueConverter;

/**
 * Simple selector matches values string-wise using toString () and
 * contains ().
 *
 * @version $Revision$
 */
public class StringContainsSelector extends StringSelector
{
  private static final ValueConverter INSTANCE = new Converter ();

  public static ValueConverter getConverterInstance ()
  {
    return INSTANCE;
  }
  
  public StringContainsSelector ()
  {
    this ("");
  }

  public StringContainsSelector (String string)
  {
    super (string);
  }

  public StringContainsSelector (String string, boolean caseSensitive)
  {
    super (string, caseSensitive);
  }

  public boolean matches (Object value)
  {
    if (value == null)
      return false;

    String strValue = value.toString ();

    if (caseSensitive)
      return strValue.indexOf (string) != -1;
    else
      return strValue.toLowerCase ().indexOf (string.toLowerCase ()) != -1;
  }

  public ValueConverter getConverter ()
  {
    return INSTANCE;
  }

  static final class Converter implements ValueConverter
  {
    public Object convertTo (Object value, Class form)
      throws IllegalFormatException
    {
      if (form.equals (String.class) && value instanceof StringContainsSelector)
      {
        return value.toString ();
      } else if (form.equals (StringContainsSelector.class) &&
                 value instanceof String)
      {
        return new StringContainsSelector ((String)value);
      } else
        throw new IllegalFormatException (this, value, form);
    }
  }
}
