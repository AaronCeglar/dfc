package dsto.dfc.swing.text;

/**
 * Interface for terms that have one or more properties whose value can cycle
 * through a small set of enumerated values.  This can be used to avoid
 * having to provide a customizer for simple properties.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public interface EnumeratedTerm extends Term
{
  /**
   * Called to request that a property value be incremented.
   *
   * @param property The property to be changed.
   * @return True if the property is enumerated.  If false, the usual attempt
   * to show a customizer for the property will be made.
   */
  public boolean advanceValue (String property);
}
