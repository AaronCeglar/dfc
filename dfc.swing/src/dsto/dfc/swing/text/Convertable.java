package dsto.dfc.swing.text;


/**
 * Defines a value that may be converted using a ValueConverter.
 *
 * @version $Revision$
 */
public interface Convertable
{
  public ValueConverter getConverter ();
}
