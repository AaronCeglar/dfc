package dsto.dfc.swing.text;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.font.FontRenderContext;
import java.awt.image.BufferedImage;

import javax.swing.text.BadLocationException;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

/**
 *
 *
 * @author     Robbe WT Stewart
 * @created    June 30, 2000
 * @version    $Revision$
 */
public class SimpleDocument
{
  private String[] lines;
  private Color[] colors;
  private Font[] fonts;
  private LineMetrics[] lineSizes;
  private Dimension size;


  public SimpleDocument()
  {
    this(new String[0], new Color[0], new Font[0]);
  }


  public SimpleDocument(String[] lines, Color[] colors, Font[] fonts)
  {
    this.lines = lines;
    this.colors = colors;
    this.fonts = fonts;

    computeLineSizes();
  }


  public Dimension getSize()
  {
    return size;
  }


  public LineMetrics getLineSize(int index)
  {
    return lineSizes[index];
  }


  public String getText()
  {
    StringBuffer buffer = new StringBuffer(100);

    for (int i = 0; i < lines.length; i++)
    {
      buffer.append(lines[i]);

      if (i < lines.length - 1)
      {
        buffer.append("\n");
      }
    }

    return buffer.toString();
  }


  public int getLineCount()
  {
    return lines.length;
  }


  public String getLine(int index)
  {
    return lines[index];
  }


  public Color getColor(int index)
  {
    return colors[index];
  }


  public Font getFont(int index)
  {
    return fonts[index];
  }


  /**
   *  Convert and answer this object into JFC Document
   *
   * @return    styled document converted to JFC document
   * @see       javax.swing.text.Document
   */
  public javax.swing.text.Document createJFCDocument()
  {
    javax.swing.text.Document document = null;
    document = new javax.swing.text.DefaultStyledDocument();

    try
    {
      final int NUM_LINES = lines.length;
      for (int i = 0; i < NUM_LINES; i++)
      {
        MutableAttributeSet attrs = new SimpleAttributeSet();
        StyleConstants.setForeground(attrs, colors[i]);
        StyleConstants.setFontFamily(attrs, fonts[i].getFamily());
        StyleConstants.setFontSize(attrs, fonts[i].getSize());

        StyleConstants.setBold(attrs, fonts[i].isBold());
        StyleConstants.setItalic(attrs, fonts[i].isItalic());

        document.insertString(document.getLength(), lines[i], attrs);

        if (i != (NUM_LINES - 1))
        {
          document.insertString(document.getLength(), "\n", attrs);
        }
      }
    }
    catch (BadLocationException ble)
    {
      ble.printStackTrace();
    }
    return document;
  }


  protected void computeLineSizes()
  {
    GraphicsDevice screen =
      GraphicsEnvironment.getLocalGraphicsEnvironment ().getDefaultScreenDevice ();
    GraphicsConfiguration config = screen.getDefaultConfiguration ();
    BufferedImage image = config.createCompatibleImage (10, 10);
    
    Graphics2D gc = (Graphics2D)image.getGraphics ();
    FontRenderContext renderContext = gc.getFontRenderContext ();
    
    size = new Dimension(0, 0);
    lineSizes = new LineMetrics[lines.length];

    for (int i = 0; i < lines.length; i++)
    {
      // old deprecated version
      //      FontMetrics fm = toolkit.getFontMetrics(getFont(i));
      //
      //      LineMetrics lineSize =
      //          new LineMetrics(fm.stringWidth(getLine(i)),
      //          fm.getHeight(), fm.getAscent());
      //
      //      size.height += lineSize.height;
      //      size.width = Math.max(size.width, lineSize.width);
      
      Font font = getFont (i);
      String text = getLine (i);
      
      java.awt.font.LineMetrics lm =
        font.getLineMetrics (text, renderContext);
      
      int lineWidth =
        (int)Math.ceil (font.getStringBounds (text, renderContext).getWidth ());
      
      size.height += (int)Math.ceil (lm.getHeight ());
      size.width = Math.max (size.width, lineWidth);
      
      lineSizes [i] = 
        new LineMetrics (lineWidth, (int)lm.getHeight (), (int)lm.getAscent ());
    }
    
    gc.dispose ();
  }



  // ======================================================================
  //                  Nested inner class -- LineMetrics
  // ======================================================================
  public final static class LineMetrics
  {
    public int height;
    public int width;
    public int ascent;


    public LineMetrics(int width, int height, int ascent)
    {
      this.width = width;
      this.height = height;
      this.ascent = ascent;
    }
  }
  // end of inner class LineMetrics
}
// end of class StyledDocument
// -- EOF
