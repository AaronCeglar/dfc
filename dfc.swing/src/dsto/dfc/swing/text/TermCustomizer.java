package dsto.dfc.swing.text;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.Scrollable;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

import dsto.dfc.swing.controls.DfcOkCancelDialog;
import dsto.dfc.swing.forms.AbstractFormEditorComponent;
import dsto.dfc.swing.forms.FormDialog;
import dsto.dfc.swing.forms.FormEditor;
import dsto.dfc.text.StringUtility;
import dsto.dfc.util.IllegalFormatException;
import dsto.dfc.util.Objects;

/**
 * An editable rich text view of a {@link Term}.  Shows the heirachy of
 * CompositeTerm's and the text customizer for each term.
 *
 * @see Term#getCustomizerFormat
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class TermCustomizer
  extends AbstractFormEditorComponent implements Scrollable
{
  protected static final String DELETE_IMAGE_URL =
    TermCustomizer.class.getResource ("/dsto/dfc/icons/cross_red_small.gif").toExternalForm ();
  protected static final String ADD_IMAGE_URL =
    TermCustomizer.class.getResource ("/dsto/dfc/icons/plus_green_small.gif").toExternalForm ();

  protected Term rootTerm;
  protected Document document;
  protected ArrayList terms = new ArrayList ();
  protected HashMap parentMap = new HashMap ();
  protected List termTemplates = new ArrayList ();
  private BorderLayout borderLayout1 = new BorderLayout();
  private JTextPane textPane = new JTextPane();
  private transient Vector changeListeners;

  public TermCustomizer ()
  {
    textPane.setEditorKit (new HTMLEditorKit ());
    textPane.setFont (UIManager.getFont ("Label.font"));

    jbInit ();

    // setup hyperlink listener
    textPane.addHyperlinkListener (new HyperlinkListener ()
    {
      public void hyperlinkUpdate (HyperlinkEvent e)
      {
        if (e.getEventType () == HyperlinkEvent.EventType.ACTIVATED)
        {
          linkActivated (e.getDescription ());
        } else if (e.getEventType () == HyperlinkEvent.EventType.ENTERED)
        {
          linkEntered (e.getDescription ());
        } else
        {
          linkExited (e.getDescription ());
        }
      }
    });

    // enable tooltip[s
    ToolTipManager.sharedInstance ().registerComponent (textPane);
  }

  private void jbInit ()
  {
    textPane.setEditable (false);
    this.setLayout(borderLayout1);
    this.add(textPane, BorderLayout.CENTER);
  }

  public CompositeTerm getParent (Term child)
  {
    return (CompositeTerm)parentMap.get (child);
  }

  public List getTermTemplates ()
  {
    return termTemplates;
  }

  /**
   * Set the list of template terms that is presented to the user when the
   * add term button is clicked.  All values in the list must be cloneable
   * Term instances.
   */
  public void setTermTemplates (List newValue)
  {
    termTemplates = newValue;
  }

  public JTextPane getTextPane ()
  {
    return textPane;
  }

  /**
   * Set the term that is to be edited.  Synonym for setEditorValue (newValue).
   */
  public void setRootTerm (Term newValue)
  {
    try
    {
      setEditorValue (newValue);
    } catch (IllegalFormatException ex)
    {
      // can't happen
      ex.printStackTrace ();
    }
  }

  /**
   * Get the root term that is being edited.  Synonym for getEditorValue ().
   */
  public Term getRootTerm ()
  {
    return (Term)getEditorValue ();
  }

  // FormEditor interface

  public Object getEditorValue () throws IllegalFormatException
  {
    return rootTerm;
  }

  public void setEditorValue (Object value) throws IllegalFormatException
  {
    try
    {
      rootTerm = (Term)value;
    } catch (ClassCastException ex)
    {
      throw new IllegalFormatException (this, value, Term.class);
    }

    updateText ();

    fireStateChanged ();
  }

  public boolean isDirectEdit ()
  {
    return true;
  }

  public void commitEdits ()
  {
    // zip
  }

  // Scrollable interface - delegated to textPane

  public Dimension getPreferredScrollableViewportSize ()
  {
    return textPane.getPreferredScrollableViewportSize ();
  }

  public int getScrollableUnitIncrement (Rectangle visibleRect,
                                         int orientation, int direction)
  {
    return textPane.getScrollableUnitIncrement (visibleRect, orientation, direction);
  }

  public int getScrollableBlockIncrement (Rectangle visibleRect,
                                          int orientation, int direction)
  {
    return textPane.getScrollableBlockIncrement (visibleRect, orientation, direction);
  }

  public boolean getScrollableTracksViewportWidth ()
  {
    return textPane.getScrollableTracksViewportWidth ();
  }

  public boolean getScrollableTracksViewportHeight ()
  {
    return textPane.getScrollableTracksViewportHeight ();
  }

  /**
   * Called when a hyperlink in the text pane is entered and shows tooltip.
   */
  protected void linkEntered (String link)
  {
    String text = null;

    textPane.setToolTipText (null);

    if (link.startsWith ("@"))
      text = "Customize this field";
    else if (link.startsWith ("-"))
      text = "Delete this term";
    else if (link.startsWith ("+"))
      text = "Add a new term";

    textPane.setToolTipText (text);
  }

  /**
   * Called when a hyperlink in the text pane is exited and clears tooltip.
   */
  protected void linkExited (String link)
  {
    setToolTipText (null);
  }

  /**
   * Called when a hyperlink in the text pane is activated and delegates to
   * customize/remove/addTerm ().
   */
  protected void linkActivated (String link)
  {
    if (link.startsWith ("@"))
      customizeTerm (link.substring (1));
    else if (link.startsWith ("-"))
      removeTerm (Integer.parseInt (link.substring (1)));
    else if (link.startsWith ("+"))
      addTerm (Integer.parseInt (link.substring (1)));
  }

  /**
   * Remove the term at a given row in the term list.
   *
   * @param termRow The absolute row of the term in the tree of terms.
   */
  protected void removeTerm (int termRow)
  {
    Term term = (Term)terms.get (termRow);
    CompositeTerm parent = getParent (term);
    int childIndex = parent.getTerms ().indexOf (term);

    parent.getTerms ().remove (childIndex);

    updateText ();

    fireStateChanged ();
  }

  /**
   * Add a term at a given row in the term list.
   *
   * @param termRow The absolute row of the term in the tree of terms.
   */
  protected void addTerm (int termRow)
  {
    CompositeTerm parent = (CompositeTerm)terms.get (termRow);
    Term term = selectTemplateTerm ();

    if (term != null)
    {
      try
      {
        Term newTerm = (Term)Objects.cloneObject (term);

        parent.getTerms ().add (newTerm);

        updateText ();

        fireStateChanged ();
      } catch (CloneNotSupportedException ex)
      {
        throw new Error ("Term type " + term.getClass () + " not cloneable");
      }
    }
  }

  /**
   * Replace one term with another at the same location.
   * @param term The old term.
   * @param newTerm The new term.
   * @param termRow The absolute row in the tree that term is at.
   * @exception IllegalArgumentException if attempt is made to replace root
   * term.
   */
  protected void replaceTerm (Term term, Term newTerm, int termRow)
    throws IllegalArgumentException
  {
    CompositeTerm parent = getParent (term);

    if (parent != null)
    {
      int childIndex = parent.getTerms ().indexOf (term);

      // move children for blocks
      if (term instanceof CompositeTerm)
      {
        ((CompositeTerm)newTerm).setTerms
          (((CompositeTerm)term).getTerms ());
      }

      parent.getTerms ().set (childIndex, newTerm);
    } else
    {
      throw new IllegalArgumentException ("Cannot replace root term");
    }
  }

  /**
   * Customize a term property represented by a given link from its
   * customizerFormat.
   */
  protected void customizeTerm (String link)
  {
    int hashIndex = link.indexOf ('#');
    int termRow = Integer.parseInt (link.substring (0, hashIndex));
    String property = link.substring (hashIndex + 1);
    Term term = (Term)terms.get (termRow);

    if (term instanceof EnumeratedTerm &&
         ((EnumeratedTerm)term).advanceValue (property))
    {
      updateText ();

      fireStateChanged ();
    } else
    {
      Term newTerm = customizeTermVisually (term, property);

      if (newTerm != null)
      {
        replaceTerm (term, newTerm, termRow);
        updateText ();

        fireStateChanged ();
      }
    }
  }

  /**
   * Customize a property of a term using its customizer UI.
   *
   * @param term The term.
   * @param property The property to be edited.
   * @return The edited term, or null if editing was cancelled.
   */
  protected Term customizeTermVisually (Term term, String property)
  {
    String customizerClassName =
      term.getClass ().getName () +
        StringUtility.capitaliseFirst (property) + "Customizer";

    try
    {
      Class customizerClass = Class.forName (customizerClassName);
      FormEditor customizer = (FormEditor)customizerClass.newInstance ();

      FormDialog dialog = new FormDialog (this, "Customize " + property, true);
      dialog.setFormPanel (customizer);
      dialog.setEditorValue (term);

      Dimension size = dialog.getPreferredSize ();

      dialog.setSize (Math.max (size.width, 300), Math.max (size.height, 150));

      dialog.setVisible (true);
      dialog.dispose ();

      if (!dialog.isCancelled ())
      {
        return (Term)dialog.getEditorValue ();
      } else
      {
        return null;
      }
    } catch (Exception ex)
    {
      ex.printStackTrace ();

      return null;
    }
  }

  /**
   * Show a list of template terms and allow the user to select one.
   *
   * @return The selected term (an instance from termTemplates) or null if
   * editor was cancelled.
   */
  protected Term selectTemplateTerm ()
  {
    DfcOkCancelDialog dialog =
      new DfcOkCancelDialog (this, "Select Term", true);

    JList list = new JList (termTemplates.toArray ());
    JPanel panel = new JPanel (new BorderLayout (3, 3));
    panel.add (new JLabel ("Select a term to add:"), BorderLayout.NORTH);
    panel.add (new JScrollPane (list), BorderLayout.CENTER);

    dialog.setDialogPanel (panel);
    dialog.pack ();
    dialog.setVisible (true);

    if (!dialog.isCancelled ())
      return (Term)list.getSelectedValue ();
    else
      return null;
  }

  /**
   * Rebuild the text view of the term tree.
   */
  protected void updateText ()
  {
    document = new Document ();

    terms.clear ();
    parentMap.clear ();

    ArrayList buffer = new ArrayList (50);

    try
    {
      textifyTerm (buffer, rootTerm, 0);

      document.insert (buffer);

      textPane.setStyledDocument (document);
    } catch (BadLocationException ex)
    {
      ex.printStackTrace ();
    }
  }

  /**
   * Turn a term tree into its textual customizer form.
   *
   * @param buffer The buffer to add the generated ElementSpec's to.
   * @param term The root of the term tree to textify.
   * @param level The child nesting level (should be 0 for root).
   */
  protected void textifyTerm (ArrayList buffer, Term term, int level)
    throws BadLocationException
  {
    int index = terms.size ();
    terms.add (term);

    document.addTerm (buffer, term, index, level);

    if (term instanceof CompositeTerm)
    {
      CompositeTerm composite = (CompositeTerm)term;
      List termList = composite.getTerms ();

      for (int i = 0; i < termList.size (); i++)
      {
        Term child = (Term)termList.get (i);

        parentMap.put (child, term);

        textifyTerm (buffer, child, level + 1);
      }
    }
  }

  public synchronized void removeChangeListener (ChangeListener l)
  {
    if (changeListeners != null && changeListeners.contains(l))
    {
      Vector v = (Vector) changeListeners.clone();
      v.removeElement(l);
      changeListeners = v;
    }
  }

  public synchronized void addChangeListener (ChangeListener l)
  {
    Vector v = changeListeners == null ? new Vector(2) : (Vector) changeListeners.clone();
    if (!v.contains(l))
    {
      v.addElement(l);
      changeListeners = v;
    }
  }

  protected void fireStateChanged ()
  {
    if (changeListeners != null)
    {
      ChangeEvent e = new ChangeEvent (this);

      Vector listeners = changeListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((ChangeListener) listeners.elementAt(i)).stateChanged(e);
      }
    }
  }

  /**
   * The document used for the text display.  Provides a series of methods
   * for building the document from the term list.
   */
  protected class Document extends HTMLDocument
  {
    protected SimpleAttributeSet paraAttrs = new SimpleAttributeSet ();
    protected SimpleAttributeSet indentParaAttrs;
    protected SimpleAttributeSet contentAttrs = new SimpleAttributeSet ();
    protected ElementSpec startPara;
    protected ElementSpec content;
    protected ElementSpec endPara;

    public Document ()
    {
      super ();

      paraAttrs.addAttribute (StyleConstants.NameAttribute, HTML.Tag.P);

      Font font = TermCustomizer.this.getFont ();

      if (font == null)
        font = UIManager.getFont ("TextPane.font");

      StyleConstants.setFontSize (contentAttrs, font.getSize ());
      StyleConstants.setFontFamily (contentAttrs, font.getFamily ());
      contentAttrs.addAttribute (StyleConstants.NameAttribute, HTML.Tag.CONTENT);

      startPara = new ElementSpec (paraAttrs, ElementSpec.StartTagType);
      endPara = new ElementSpec (paraAttrs, ElementSpec.EndTagType);

      content = new ElementSpec (contentAttrs, ElementSpec.ContentType,
                                 new char [] {' '}, 0, 1);
    }

    /**
     * Insert a list of ElementSpec's from a list into the document.
     */
    public void insert (List myBuffer) throws BadLocationException
    {
      ElementSpec [] array = new ElementSpec [myBuffer.size ()];
      myBuffer.toArray (array);

      insert (getLength (), array);
    }

    /**
     * Insert a new indented para ElementSpec into a buffer.
     */
    public void newIndentedParagraph (ArrayList currentBuffer, int indent)
    {
      SimpleAttributeSet newIndentParaAttrs = new SimpleAttributeSet (paraAttrs);
      StyleConstants.setLeftIndent (newIndentParaAttrs, indent);
      ElementSpec startIndentedPara =
        new ElementSpec (newIndentParaAttrs, ElementSpec.StartTagType);

      currentBuffer.add (endPara);
      currentBuffer.add (startIndentedPara);
    }

    /**
     * Insert a new para ElementSpec into a buffer.
     */
    public void newParagraph () throws BadLocationException
    {
      insert (getLength (),
              new ElementSpec [] {endPara, startPara, content});
    }

    public void addText (String text) throws BadLocationException
    {
      addText (text, contentAttrs);
    }

    public void addText (String text, AttributeSet attrs)
      throws BadLocationException
    {
      insertString (getLength (), text, attrs);
    }

    public void addLink (String text, String link) throws BadLocationException
    {
      AttributeSet attrs = createLinkAttrs (link);

      insertString (getLength (), text, attrs);
    }

    /**
     * Insert the ElementSpec's for a term into a buffer.
     *
     * @param currentBuffer The buffer to add to.
     * @param term The term to add.
     * @param index The index of the term.
     * @param level The level of indent for the term.
     */
    public void addTerm (ArrayList currentBuffer, Term term, int index, int level)
      throws BadLocationException
    {
      newIndentedParagraph (currentBuffer, level * 20);

      parseFormat (currentBuffer, term.getCustomizerFormat (), index);

      if (term instanceof CompositeTerm)
      {
        addContent (currentBuffer, " ");
        addNewTermButton (currentBuffer, index);
      }

      if (index > 0)
      {
        addContent (currentBuffer, " ");
        addDeleteTermButton (currentBuffer, index);
      }
    }

    /**
     * Add an 'new term' button to a buffer.
     */
    public void addNewTermButton (ArrayList currentBuffer, int index)
      throws BadLocationException
    {
      addButton (currentBuffer, ADD_IMAGE_URL, "[+]", "+" + index);
    }

    /**
     * Add a 'delete term' button to a buffer.
     */
    public void addDeleteTermButton (ArrayList currentBuffer, int index)
      throws BadLocationException
    {
      addButton (currentBuffer, DELETE_IMAGE_URL, "[x]", "-" + index);
    }

    /**
     * Add a button.
     *
     * @param currentBuffer Buffer to add element specs to.
     * @param imageUrl An image URL.
     * @param text The text content for the image.
     * @param link The link to attach to the image.
     */
    public void addButton (ArrayList currentBuffer, String imageUrl,
                           String text, String link)
      throws BadLocationException
    {
      SimpleAttributeSet attrs = new SimpleAttributeSet ();

      SimpleAttributeSet linkAttrs = new SimpleAttributeSet ();
      linkAttrs.addAttribute (HTML.Attribute.HREF, link);

      attrs.addAttribute (StyleConstants.NameAttribute, HTML.Tag.IMG);
      attrs.addAttribute (HTML.Tag.A, linkAttrs);
      attrs.addAttribute (HTML.Attribute.BORDER, "0");
      attrs.addAttribute (HTML.Attribute.SRC, imageUrl);

      ElementSpec tagSpec =
        new ElementSpec (attrs, ElementSpec.StartTagType);

      ElementSpec endTagSpec =
        new ElementSpec (attrs, ElementSpec.EndTagType);

      currentBuffer.add (tagSpec);
      addContent (currentBuffer, text);
      currentBuffer.add (endTagSpec);
    }

    protected void addContent (ArrayList currentBuffer, String text)
    {
      if (text.length () > 0)
      {
        currentBuffer.add (new ElementSpec (contentAttrs, ElementSpec.ContentType,
                    text.toCharArray (), 0, text.length ()));
      }
    }

    protected MutableAttributeSet createLinkAttrs (String link)
    {
      SimpleAttributeSet attrs = new SimpleAttributeSet (contentAttrs);
      SimpleAttributeSet linkAttrs = new SimpleAttributeSet ();
      linkAttrs.addAttribute (HTML.Attribute.HREF, link);
      attrs.addAttribute (HTML.Tag.A, linkAttrs);
      StyleConstants.setForeground (attrs, Color.blue);
      StyleConstants.setUnderline (attrs, true);

      return attrs;
    }

    protected MutableAttributeSet createButtonAttrs (Color color, String link)
    {
      SimpleAttributeSet attrs = new SimpleAttributeSet (contentAttrs);
      SimpleAttributeSet linkAttrs = new SimpleAttributeSet ();
      linkAttrs.addAttribute (HTML.Attribute.HREF, link);
      attrs.addAttribute (HTML.Tag.A, linkAttrs);
      StyleConstants.setForeground (attrs, color);

      return attrs;
    }

    /**
     * Parse the customizer format for a Condtion, generating a stream of
     * ElementSpec's to a buffer.
     *
     * @param currentBuffer The buffer to add to.
     * @param format The customizer format (see
     * {@link Term#getCustomizerFormat}).
     * @para termRow The index of the term in the list.  Used to
     * generate the appropriate href's.
     */
    protected void parseFormat (ArrayList currentBuffer, String format, int termRow)
    {
      int index = 0;      // current index within format
      int tagStart = -1;  // start of next symbol (>= index)
      int tagEnd = -1;    // end of next symbol (> tagStart)

      for (;;)
      {
        tagStart = format.indexOf ("<", index);

        if (tagStart != -1)
        {
          // append text between index and start of tag
          addContent (currentBuffer, format.substring (index, tagStart));

          // find end of tag
          tagEnd = format.indexOf (">", tagStart);

          if (tagEnd != -1)
          {
            String tag = format.substring (tagStart + 1, tagEnd);

            // advance to end of tag
            index = tagEnd + 1;

            // find closing tag
            tagStart = format.indexOf ("</" + tag + ">", index);

            if (tagStart != -1)
            {
              // found closing tag
              String taggedText = format.substring (index, tagStart);

              // System.out.println("tag = " + tag + " text = " + taggedText);

              // advance to end of terminating tag
              index = tagStart + tag.length () + 3;

              AttributeSet attrs = createLinkAttrs ("@" + termRow + "#" + tag);
              ElementSpec tagSpec =
                new ElementSpec (attrs, ElementSpec.ContentType,
                                 taggedText.toCharArray (), 0,
                                 taggedText.length ());

              currentBuffer.add (tagSpec);
            } else
            {
              // no closing tag: add error text and abort
              addContent (currentBuffer, "!!unclosed tag " + tag + "!!");
              break;
            }
          } else
          {
            // missing ">": add error text and abort
            addContent (currentBuffer, "!!unterminated tag " + format.substring (tagStart) + "!!");
            break;
          }
        } else
        {
          // no tag start found, add remaining characters and exit
          addContent (currentBuffer, format.substring (index));
          break;
        }
      }
    }
  }
}
