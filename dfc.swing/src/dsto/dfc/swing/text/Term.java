package dsto.dfc.swing.text;


/**
 * Defines a term in an expression.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public interface Term
{
  /**
   * Get a description of this type of term suitable for use in choosing
   * a from a list of terms.
   */
  public String getTermDescription ();

  /**
   * Get a formatted string used to customize this condition in {@link
   * TermCustomizer}.  The string may contain a number of embedded
   * properties to be customized eg "Match
   * &lt;type&gt;chat&lt;/type&gt; messages" defines one customizable
   * property named "type".  The customizer displayed for each
   * property is the name of the condition class plus the property
   * plus "Customizer" eg MyTermTypeCustomizer.  The customizer class
   * must implement the {@link dsto.dfc.swing.forms.FormEditor}
   * interface.<p>
   *
   * For conditions that have simple enumerated properties (where a
   * property can cycle through a series of values), see the {@link
   * EnumeratedTerm} interface.
   */
  public String getCustomizerFormat ();
}
