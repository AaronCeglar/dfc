package dsto.dfc.swing.text;

import dsto.dfc.util.IllegalFormatException;

/**
 * Simple value converter that does no conversion at all.
 *
 * @version $Revision$
 */
public final class NullValueConverter implements ValueConverter
{
  public static final ValueConverter INSTANCE = new NullValueConverter ();

  private NullValueConverter ()
  {
    // zip
  }

  public Object convertTo (Object value, Class form)
    throws IllegalFormatException
  {
    if (value == null || form.isAssignableFrom (value.getClass ()))
      return value;
    else
      throw new IllegalFormatException (this, value, form);
  }
}
