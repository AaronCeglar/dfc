package dsto.dfc.swing.text;

import dsto.dfc.util.IllegalFormatException;


/**
 * A value converter that supports conversion of standard Java numeric
 * types to/from String form.
 *
 * @version $Revision$
 */
public class NumberConverter implements ValueConverter
{
  public static final NumberConverter INSTANCE = new NumberConverter ();
  
  public Object convertTo (Object value, Class form)
    throws IllegalFormatException
  {
    if (value.getClass ().equals (String.class))
    {
      try
      {
        if (form.equals (Integer.class))
          return new Integer ((String)value);
        else if (form.equals (Float.class))
          return new Float ((String)value);
        else if (form.equals (Double.class))
          return new Double ((String)value);
        else if (form.equals (Long.class))
          return new Long ((String)value);
        else if (form.equals (Short.class))
          return new Short ((String)value);
        else
          throw new IllegalFormatException (this, value, form);
      } catch (NumberFormatException ex)
      {
        throw new IllegalFormatException
          (this, "'" + value + "' is not a valid number", value, form);
      }
    } else if (form.equals (String.class))
    {
      // TODO: check if value is numeric?
      
      return value.toString ();
    } 

    throw new IllegalFormatException (this, value, form);
  }
}
