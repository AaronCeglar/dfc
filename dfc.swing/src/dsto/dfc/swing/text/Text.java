package dsto.dfc.swing.text;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import dsto.dfc.util.Beans;
import dsto.dfc.util.IllegalFormatException;
import dsto.dfc.util.Objects;


public final class Text
{
  private Text ()
  {
    // zip
  }

  /**
   * Attempt to convert a value to a given class. The Convertable
   * interface is used if available, otherwise attempts to use the
   * form's constructor.
   * 
   * @param value The value to convert.
   * @param targetForm The class to convert the value to. If the class
   *          is primitive, its equivalent Object-based class is used.
   * @param converter The converter to use. May be null in which case
   *          the default attempts to convert the value are used.
   * @return A new instance of form containing the converted value.
   * @exception IllegalFormatException if conversion was not possible
   *              (ie Convertable threw an exception, or no
   *              constructor available or constructor threw an
   *              exception).
   */
  public static Object convertValue (Object value, Class targetForm,
                                     ValueConverter converter)
    throws IllegalFormatException
  {
    Class form = Beans.primitiveToObject (targetForm);

    if (value == null)
    {
      // null is instance of universal class :/
      return null;
    } else if (targetForm.isAssignableFrom (value.getClass ()))
    {
      // value is already compatible
      return value;
    } else if (converter != null)
    {
      return converter.convertTo (value, form);
    } else if (value instanceof Convertable)
    {
      converter = ((Convertable)value).getConverter ();

      return converter.convertTo (value, form);
    } else
    {
      // try to find a registered converter
      converter = getDefaultConverter (value.getClass (), form);

      if (converter != null)
      {
        return converter.convertTo (value, form);
      } else
      {
        // last resort: try using a constructor to create a new
        // instance
        try
        {
          // try to bind to appropriate constructor
          Constructor constructor = form.getConstructor (new Class []{value.getClass ()});

          // try to create a new instance
          return constructor.newInstance (new Object []{value});
        } catch (Exception ex)
        {
          // can we use toString ()?
          if (form.equals (String.class))
            return value.toString ();
          else
          {
            Throwable cause = ex;

            // extract embedded exception if necessary
            if (ex instanceof InvocationTargetException)
              cause = ((InvocationTargetException)ex).getTargetException ();

            // we're out of options
            throw new IllegalFormatException (Objects.class, value,
                form, cause);
          }
        }
      }
    }
  }

  public static Object convertValue (Object value, Class form)
    throws IllegalFormatException
  {
    return convertValue (value, form, null);
  }

  public static ValueConverter getDefaultConverter (Class from, Class to)
  {
    if (from.equals (String.class))
    {
      if (Number.class.isAssignableFrom (to))
        return NumberConverter.INSTANCE;
    }
  
    // TODO: implement dynamic registry of converters
    return null;
  }
}
