package dsto.dfc.swing.panels;

/** A policy class for displaying titles in PanelManager.Panels.
 *  Titles are  hidden in the Tabbed Pane Tab, but still
 *  displayed in windowFrames. In the absence of a title, the
 *  Tabbed Pane Tab name is displayed as a tool tip.
 */

public class TitleHidePolicy extends TitleShowPolicy
{

public boolean isShowingName ()
{
  return false;
}

public String getTabDisplayText ()
{
  return changeIndicator;
}

public String getToolTipText ()
{
  return name;
}

}