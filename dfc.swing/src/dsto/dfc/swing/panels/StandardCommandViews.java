package dsto.dfc.swing.panels;

import java.util.Collection;

import dsto.dfc.swing.commands.Command;
import dsto.dfc.swing.commands.CommandSource;
import dsto.dfc.swing.commands.CommandView;

/**
 * StandardCommandViews encapsulates the main, toolbar, and context menus.
 * It allows addition and removal of commands to all menus with a single
 * method call.
 */

public class StandardCommandViews implements CommandSource
{
  CommandView mainCommandView    = new CommandView (CommandView.MAIN_MENU_VIEW   );
  CommandView contextCommandView = new CommandView (CommandView.CONTEXT_MENU_VIEW);
  CommandView toolbarCommandView = new CommandView (CommandView.TOOLBAR_VIEW     );

  /** CommandSource interface */

  public CommandView getCommandView (String viewName)
  {
    if ( viewName.equals ( CommandView.MAIN_MENU_VIEW    ))
      return mainCommandView;

    if ( viewName.equals ( CommandView.CONTEXT_MENU_VIEW ))
      return contextCommandView;

    if ( viewName.equals ( CommandView.TOOLBAR_VIEW      ))
      return toolbarCommandView;

    return null;
  }

  /** end CommandSource interface */

  public void setDefaultCommand (Command command)
  {
    mainCommandView   .setDefaultCommand (command);
    contextCommandView.setDefaultCommand (command);
    toolbarCommandView.setDefaultCommand (command);
  }

  public void addCommands (Collection commands)
  {
    mainCommandView   .addCommands (commands);
    contextCommandView.addCommands (commands);
    toolbarCommandView.addCommands (commands);
  }

  public void addCommand (Command command)
  {
    mainCommandView   .addCommand (command);
    contextCommandView.addCommand (command);
    toolbarCommandView.addCommand (command);
  }

  public void removeCommands (Collection commands)
  {
    mainCommandView   .removeCommands (commands);
    contextCommandView.removeCommands (commands);
    toolbarCommandView.removeCommands (commands);
  }
}