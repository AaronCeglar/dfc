package dsto.dfc.swing.panels;

import java.awt.Component;
import java.awt.Point;
import java.awt.Window;
import java.beans.PropertyVetoException;

import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLayeredPane;
import javax.swing.SwingUtilities;
import javax.swing.plaf.basic.BasicInternalFrameUI;

import dsto.dfc.swing.commands.Command;
import dsto.dfc.swing.commands.CommandMenus;
import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.swing.controls.Geometry;
import dsto.dfc.swing.icons.CompositeIcon;
import dsto.dfc.swing.icons.ImageLoader;

/** The Internal FloatState represents a lightweight window. It
 *  displays the component inside a PanelManager.Panel.
 */

public class InternalFloatState extends FixedFloatState
{
  protected static final Icon FLOAT_OVERLAY_ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/floated_overlay.gif");

  protected JInternalFrame frame;
  protected Window         baseWindow = null;

  public InternalFloatState (PanelManager.Panel panel,
                             RestrictedIndexTabbedPane tabPane,
                             Icon icon, int index)
  {
    super   (panel, tabPane, icon, index);
    setIcon (icon);
  }

  public void makeUncloseable ()
  {
    super.makeUncloseable ();
    makeFrameUncloseable ();
  }

  protected void makeFrameUncloseable ()
  {
     frame.setClosable (false);
  }

  public void dispose ()
  {
    if (frame != null)
    {
      frame.dispose ();
      frame = null;
    }
    
    super.dispose ();
  }

  public void showIcon ()
  {
    if (frame != null)
      frame.setFrameIcon (icon);
  }

  public Icon getTabDisplayIcon ()
  {
    return new CompositeIcon (icon, FLOAT_OVERLAY_ICON, Geometry.TOP_LEFT);
  }

  protected void refreshTitle (TitleShowPolicy titleDisplay)
  {
    frame.setTitle (titleDisplay.getWindowDisplayText ());
  }

  public void addListener (PanelManager.WindowEventListener listener)
  {
     frame.addInternalFrameListener (listener);
  }

  public void removeListener (PanelManager.WindowEventListener listener)
  {
     frame.removeInternalFrameListener (listener);
  }

  public void setActive (boolean active)
  {
    try {
      frame.setSelected (active);
    } catch (PropertyVetoException ex)
    {
      ex.printStackTrace ();
    }
  }

  protected void showFrame (boolean visible)
  {
     addToBaseWindow  (tabPane);
     frame.setVisible (visible);
     if ( frame.isIcon () )
       frame.getDesktopIcon ().setVisible (visible);
  }

  public void setVisible (boolean visible)
  {
    super.setVisible (visible);
    showFrame        (visible);
  }

  // need to delay identifying parent window to run time

  private void addToBaseWindow (Component myTabPane)
  {
    if (baseWindow != null)
      return;

    baseWindow = SwingUtilities.windowForComponent (myTabPane);

    if (baseWindow == null)
      return;

    ((JFrame)baseWindow).getLayeredPane ().add (frame, JLayeredPane.MODAL_LAYER);
    frame.pack ();
  }

  public String getFloating ()
  {
    return PanelManager.FLOAT_INTERNAL;
  }

  /** each subsequent frame in the PanelManager is offset so that
   *  all newly created frames are visible and do not obscure each
   *  other.
   */

  protected Point calculateLocation (int index)
  {
    return new Point ((index + 1) * PanelManager.TAB_WIDTH,
                      (index + 2) * PanelManager.TAB_HEIGHT);
  }

  protected void setUpDisplay (int index)
  {
    frame = new JInternalFrame  ("", true, true, false, true);
    frame.putClientProperty     ("panel", panel             );
    frame.getContentPane ().add (placeHolder                );
    frame.setLocation           (calculateLocation (index)  );
    frame.setDefaultCloseOperation (JInternalFrame.DO_NOTHING_ON_CLOSE);
    installContextMenuOnTitleBar ();
  }

  private void installContextMenuOnTitleBar ()
  {
    Component    titleBar   = frameTitleBar ();
    CommandView contextMenu = panel.getManager ().getCommandView (CommandView.CONTEXT_MENU_VIEW);

    if ( titleBar == null || contextMenu == null )
      return;

    CommandMenus.installContextMenu (titleBar, contextMenu);
  }

  // this may or may not be portable across differing look and feels

  private Component frameTitleBar ()
  {
    try
    {
      BasicInternalFrameUI lookAndFeel = (BasicInternalFrameUI) frame.getUI ();
      return lookAndFeel.getNorthPane ();
    }
    catch (ClassCastException unknownUI)
    {
      return null;
    }
  }

  public void setUpCommands ()
  {
    commands.add (unFloatCommand       );
    commands.add (externalFloatCommand );
    commands.add (closeCommand         );
  }

  public Command getDefaultCommand ()
  {
    return unFloatCommand;
  }

}