package dsto.dfc.swing.panels;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import dsto.dfc.swing.commands.CommandMenus;

/**
 * A Dynamic Command View alters the commands visible on the main menu,
 * context menu, and tool bar of the panel manager. The commands vary:
 *
 *   a) when a new panel becomes the active panel
 *   b) when the active panel changes state - floated or unfloated
 *
 * @version $Revision$
 * @author Peter J Smet
 */

public class DynamicCommandViews extends ClientCommandViews
  implements PanelManagerListener, PropertyChangeListener
{
  PanelManager manager;

  public DynamicCommandViews (PanelManager manager)
  {
    this.manager = manager;
    manager.addPanelManagerListener   (this               );
    manager.addPropertyChangeListener ("activePanel", this);

    CommandMenus.installContextMenu (manager.tabPane, contextCommandView);
  }

  public void panelFloated (PanelManagerEvent e)
  {
    updateDefaultCommand (e.getPanel());
  }

  public void panelUnfloated (PanelManagerEvent e)
  {
    updateDefaultCommand (e.getPanel());
  }

  private void updateDefaultCommand (PanelManager.Panel panel)
  {
    if ( panel == null )
      setDefaultCommand (null                      );
    else
      setDefaultCommand (panel.getDefaultCommand ());
  }

  private void activePanelChanged (PanelManager.Panel activePanel)
  {
    setClient            (activePanel);
    updateDefaultCommand (activePanel);
  }

  // will be activePanel event, since listener registration
  // specifies this

  public void propertyChange (PropertyChangeEvent e)
  {
      activePanelChanged ( (PanelManager.Panel) e.getNewValue() );
  }

  public void panelAdded (PanelManagerEvent e)
  {
    // zip
  }

  public void aboutToRemovePanel (PanelManagerEvent e)
  {
    // zip
  }

  public void panelRemoved (PanelManagerEvent e)
  {
    // zip
  }
}
