package dsto.dfc.swing.panels;

import java.awt.Point;

/**
 * BottomRight positions an inner frame in the bottom
 * right corner of a larger outer frame. The indentation level
 * nudges the inner frame by degrees towards the
 * center of the outer frame.
 *
 * @version $Revision$
 * @author Peter J Smet
 */

public class BottomRight extends TopRight
{
  protected Point moveToCorner (Point topLeft)
  {
    return makeBottomRight (topLeft);
  }

  protected int verticalInset (int indentLevel)
  {
    return - super.verticalInset (indentLevel);
  }

  protected Point makeBottomRight (Point topLeft)
  {
    topLeft.translate (widthDifference, heightDifference);
    return topLeft;
  }

  }