package dsto.dfc.swing.panels;

import dsto.dfc.swing.commands.AbstractMutableCommand;
import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.swing.controls.PrintUtility;


/**
 * A Command that opens up a print dialog.
 *
 * @author Luke Marsh
 * @revision $Revision$
 */
public class CmdPrintPanel extends AbstractMutableCommand
  implements PanelManagerListener
{

  protected PanelManager manager;
  private CmdPageLayoutPanel pageLayout;


  /**
   * Initializes variables.
   * 
   * @param manager The PanelManager that manages the panels and views.
   * @param pageLayout This variable is passed to PrintUtility and PrintPreview
   * in the execute () method so those classes can access the current settings of
   * the page layout.
   */
  public CmdPrintPanel (PanelManager manager, CmdPageLayoutPanel pageLayout)
  {
    super ("/dsto/dfc/icons/file_print.gif");
    this.manager = manager;
    this.pageLayout = pageLayout;
    manager.addPanelManagerListener (this);
    updateEnabled ();
  }


  /**
   * Opens a print dialog.
   */
  public void execute ()
  {
    PrintUtility.printComponent (manager.getActivePanel ().getComponent(), 
      pageLayout.getPageFormat ());
  }


  /**
   * Sets this command to be enabled if the PanelManager contains one or more
   * panels.
   */
  protected void updateEnabled ()
  {
    setEnabled (manager.getPanelCount () > 0);
  }


  /**
   * Returns the name of this command.
   * 
   * @return The name of the command.
   */
  public String getName ()
  {
    return "file.Print";
  }


  /**
   * Returns the description of this command.
   * 
   * @return The description of the command.
   */
  public String getDescription ()
  {
    return "Print the active panel";
  }


  /**
   * Returns if this command is interactive.
   * 
   * @return true.
   */
  public boolean isInteractive ()
  {
    return true;
  }


  /**
   * Assigns this command to the print group.
   * 
   * @param viewName The name of the view.
   * @return The group name, or null if no match was found.
   */
  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return "print";
    else if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return "File.print";
    else if (viewName.equals (CommandView.TOOLBAR_VIEW))
      return "print";
    else
      return null;
  }


  /**
   * Returns the mnemonic for this command.
   * 
   * @return The mnemonic.
   */
  public char getMnemonic ()
  {
    return 'p';
  }


  /**
   * Calls updateEnabled.
   * 
   * @param e The PanelManagerEvent.
   */
  public void panelAdded (PanelManagerEvent e)
  {
    updateEnabled ();
  }


  /**
   * Calls updateEnabled.
   * 
   * @param e The PanelManagerEvent.
   */
  public void panelRemoved (PanelManagerEvent e)
  {
    updateEnabled ();
  }


  /**
   * Not Implemented.
   * 
   * @param e The PanelManagerEvent.
   */
  public void panelFloated (PanelManagerEvent e)
  {
    // zip
  }


  /**
   * Not Implemented.
   * 
   * @param e The PanelManagerEvent.
   */
  public void panelUnfloated (PanelManagerEvent e)
  {
    // zip
  }


  /**
   * Not Implemented.
   * 
   * @param e The PanelManagerEvent.
   */
  public void aboutToRemovePanel (PanelManagerEvent e)
  {
    // zip
  }
}