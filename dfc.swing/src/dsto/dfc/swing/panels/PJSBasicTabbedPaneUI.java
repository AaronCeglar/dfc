package dsto.dfc.swing.panels;

import java.awt.Graphics;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTabbedPane;
import javax.swing.plaf.metal.MetalTabbedPaneUI;

/**
  *  Allows clients to set color of the selected tab using
  *  setBackgroundAt (int i). Ensures the MouseHandler
  *  no longer ignores reselection of selected index events.
  *  Required for the proper behaviour of PanelManager Panels.
  */

public class PJSBasicTabbedPaneUI extends MetalTabbedPaneUI
{
    protected void paintTabBackground( Graphics g, int tabPlacement,
                                       int tabIndex, int x, int y, int w, int h, boolean isSelected )
   {
       super.paintTabBackground( g, tabPlacement, tabIndex, x,  y,  w,  h, false );
   }

    protected MouseListener createMouseListener() {
        return new PJSMouseHandler();
    }

    protected FocusListener createFocusListener()
    {
      return new PJSFocusHandler();
    }
    
    public JTabbedPane getTabPane ()
    {
      return tabPane;
    }

     public class PJSMouseHandler extends MouseAdapter
     {
        public void mousePressed(MouseEvent e) {
            JTabbedPane mouseTtabPane = (JTabbedPane)e.getSource();

            int tabIndex = tabForCoordinate(mouseTtabPane, e.getX(), e.getY());
            if ( tabIndex > -1 )
              mouseTtabPane.setSelectedIndex(tabIndex);
        }
    }

     public class PJSFocusHandler extends FocusHandler {

        public void focusGained(FocusEvent e)
        {
           if ( getTabPane ().getSelectedIndex() > -1 )
             super.focusGained( e );
        }

        public void focusLost(FocusEvent e)
        {
           if ( getTabPane ().getSelectedIndex() > -1 )
             super.focusGained( e );
        }
    }




}