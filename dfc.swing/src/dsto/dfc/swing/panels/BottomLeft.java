package dsto.dfc.swing.panels;

import java.awt.Point;

/**
 * BottomLeft positions an inner frame in the bottom
 * left corner of a larger outer frame. The indentation level
 * nudges the inner frame by degrees towards the
 * center of the outer frame.
 *
 * @version $Revision$
 * @author Peter J Smet
 */

public class BottomLeft extends TopLeft
{
  protected Point moveToCorner (Point topLeft)
  {
    return makeBottomLeft (topLeft);
  }

  protected int verticalInset (int indentLevel)
  {
    return - super.verticalInset (indentLevel);
  }

  protected Point makeBottomLeft (Point topLeft)
  {
    topLeft.translate (0, heightDifference);
    return topLeft;
  }

  }