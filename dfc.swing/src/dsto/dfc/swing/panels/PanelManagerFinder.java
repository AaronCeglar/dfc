package dsto.dfc.swing.panels;

import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.SwingUtilities;

/**
 * Locates the Panel Manager for a given component, if any.
 *
 *
 * @version $Revision$
 * @author Peter J Smet
 */

public class PanelManagerFinder
{

 public static PanelManager findPanelManagerForComponent (Component component)
  {
    Container parent = component.getParent ();

    for ( ; ; parent = parent.getParent () )
    {
      PanelManager.Panel panel = extractManagedPanelFromFrame (parent);

      if ( panel != null )
        return panel.getManager ();

      if ( parent == null || parent instanceof PanelManager )
        return (PanelManager) parent;
    }
  }

  private static PanelManager.Panel extractManagedPanelFromFrame (Container frame)
  {
    Object panel = null;

    if ( frame instanceof JFrame )
      panel = ((JFrame) frame ).getRootPane ().getClientProperty ("panel");

    if ( frame instanceof JInternalFrame )
      panel = ((JInternalFrame) frame )       .getClientProperty ("panel");

    return (PanelManager.Panel) panel;
  }

  public static PanelManager.Panel findPanelForComponent (Component component)
  {
    PanelManager manager = findPanelManagerForComponent (component);

    if ( manager == null )
      return null;

    if ( manager.getPanel (component) != null )
      return manager.getPanel (component);

    Container parent = component.getParent ();

    for ( ; ; parent = parent.getParent () )
      if ( parent == null || manager.getPanel (parent) != null )
        return manager.getPanel (parent);
  }

  /**
   * Utility method to create a tool view on a client component,
   * managed by the PanelManager managing the client, or standalone if
   * the client is not in a PanelManager.
   *
   * @param tool The tool component to be placed in the tool view
   * frame.
   * @param toolViewOwner The owner of the tool view.
   * @return A new ToolView, either managed by the same PanelManager
   * as client, or standalone if client is not being managed.
   *
   * @see PanelManager.Panel#addToolView(Component)
   */

  public static ToolView addToolView (Component tool, Component toolViewOwner)
  {
    PanelManager.Panel panel = findPanelForComponent (toolViewOwner);

    if (panel != null)
      return panel.addToolView (tool);

    return createStandAloneToolView (tool, toolViewOwner);
  }

  private static ToolView createStandAloneToolView (Component tool, Component toolViewOwner)
  {
    ToolView toolView = new ToolView (null, tool, null, frameForComponent (toolViewOwner));
    toolView.changeState (PanelManager.FLOAT_EXTERNAL);

    return toolView;
  }

  private static Frame frameForComponent (Component component)
  {
    Window clientFrame = SwingUtilities.windowForComponent (component);

    if ( clientFrame instanceof Frame )
      return (Frame) clientFrame;

    return null;
  }

} // PanelManagerFinder