package dsto.dfc.swing.panels;

import java.util.EventListener;

/**
 * Listener interface for PanelManager events.
 *
 * @version $Revision$
 */
public interface PanelManagerListener extends EventListener
{
  public void panelAdded (PanelManagerEvent e);

  public void aboutToRemovePanel (PanelManagerEvent e);

  public void panelRemoved (PanelManagerEvent e);

  public void panelFloated (PanelManagerEvent e);

  public void panelUnfloated (PanelManagerEvent e);
}
