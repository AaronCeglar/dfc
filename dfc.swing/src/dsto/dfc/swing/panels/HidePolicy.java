package dsto.dfc.swing.panels;

/** A policy for deciding what to do when the PanelManager contains no
 *  panels. When the policy is toggled to neverHide (), the PanelManager
 *  remains visible at all times. When hideWhenEmpty () is set,
 *  the PanelManager becomes invisible when it contains no panels
 */

public class HidePolicy implements PanelManagerListener
{
  public  PanelManager manager;
  boolean neverHide = true;

  public HidePolicy ()
  {
    this (new PanelManager ());
  }

  public HidePolicy (PanelManager manager)
  {
    this.manager = manager;
    manager.addPanelManagerListener (this);
  }

  public void neverHide ()
  {
    neverHide = true;
    manager.setVisible (true);
  }

  public void hideWhenEmpty ()
  {
    neverHide = false;
    manager.setVisible (hasPanels ());
  }

  public void panelAdded (PanelManagerEvent e)
  {
    manager.setVisible (true);
  }

  public void panelRemoved (PanelManagerEvent e)
  {
    manager.setVisible (neverHide || hasPanels () );
  }

  public boolean hasPanels ()
  {
    return manager.getPanelCount () > 0;
  }

  public void aboutToRemovePanel (PanelManagerEvent e)
  {
    // zip
  }

  public void panelFloated (PanelManagerEvent e)
  {
    // zip
  }

  public void panelUnfloated (PanelManagerEvent e)
  {
    // zip
  }
}