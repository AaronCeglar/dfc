package dsto.dfc.swing.panels;

import java.awt.Component;
import java.awt.Point;

/**
 * Corner positions an inner frame in a corner of
 * of a larger outer frame. The indentation level
 * nudges the inner frame by degrees towards the
 * center of the outer frame.
 *
 * Used to position ToolView frames inside their parent frames.
 *
 * @version $Revision$
 * @author Peter J Smet
 */

public abstract class Corner
{
  public final static TopLeft     TOP_LEFT     = new TopLeft     ();
  public final static TopRight    TOP_RIGHT    = new TopRight    ();
  public final static BottomLeft  BOTTOM_LEFT  = new BottomLeft  ();
  public final static BottomRight BOTTOM_RIGHT = new BottomRight ();

  public void placeFrame (Component inner, Component outer )
  {
    placeFrame( inner, outer, 0 );
  }

  public void placeFrame (Component inner, Component outer, int indentLevel )
  {
    if ( inner == null )
      return;

    Point cornerPoint = locationFor( inner, outer, indentLevel );
    inner.setLocation( cornerPoint );
  }

  public Point locationFor( Component inner, Component outer )
  {
    return locationFor( inner, outer, 0 );
  }

  public abstract Point locationFor( Component inner, Component outer, int indentLevel );

}