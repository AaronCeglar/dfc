package dsto.dfc.swing.panels;

import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.undo.UndoableEdit;
import javax.swing.undo.UndoableEditSupport;

import dsto.dfc.swing.undo.UndoableEditSource;
import dsto.dfc.util.Beans;

/**
 * This class informs registered listeners of edits occurring within any
 * panels held by the panel manager.
 *
 * @author Peter J Smet
 * @version $ Revision: $
 */
public class EditPropagator
  implements PanelManagerListener, UndoableEditListener, UndoableEditSource
{
  PanelManager        manager;
  UndoableEditSupport editListeners = new UndoableEditSupport ();

  public EditPropagator (PanelManager manager)
  {
    this.manager = manager;
    manager.addPanelManagerListener (this);
  }

  // UndoableEditSource interface

  public void addUndoableEditListener (UndoableEditListener listener)
  {
    editListeners.addUndoableEditListener (listener);
  }

  public void removeUndoableEditListener (UndoableEditListener listener)
  {
    editListeners.removeUndoableEditListener (listener);
  }

  // UndoableEditListener interface

  public void undoableEditHappened (UndoableEditEvent event)
  {
    tellListenersAPanelWasEdited (event.getEdit());
  }

  // end UndoableEditListener interface

  protected void tellListenersAPanelWasEdited (UndoableEdit edit)
  {
    editListeners.postEdit (edit);
  }

  // PanelManagerListener interface

  public void panelAdded(PanelManagerEvent event)
  {
    listenForEditsFromPanel (event.getPanel ());
  }

  public void panelRemoved(PanelManagerEvent event)
  {
    stopListeningForEditsFromPanel (event.getPanel ());
  }

  // end PanelManagerListener interface

  protected void listenForEditsFromPanel (PanelManager.Panel panel)
  {
    Object editEventSource = panel.getComponent ();
    Beans.addListener (UndoableEditListener.class, editEventSource, this);
  }

  protected void stopListeningForEditsFromPanel (PanelManager.Panel panel)
  {
    Object editEventSource = panel.getComponent ();
    Beans.removeListener (UndoableEditListener.class, editEventSource, this);
  }

  public void aboutToRemovePanel(PanelManagerEvent event)
  {
    // zip
  }

  public void panelFloated(PanelManagerEvent event)
  {
    // zip
  }

  public void panelUnfloated(PanelManagerEvent event)
  {
    // zip
  }
}