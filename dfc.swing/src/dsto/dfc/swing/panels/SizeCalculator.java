package dsto.dfc.swing.panels;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

/**
 * Calculates the minimum and preferred sizes of the panel manager.
 *
 * The preferred size is the maximum preferred size of all
 * components displayed within the PanelManager's panels.
 *
 * The minimum size is the largest minimum size of all
 * components displayed within the PanelManager's panels.
 *
 * Both sizes are enlarged by a variable, platform dependent
 * measure, tabPaneInsets, which needs to be dynamically calculated.
 */

public class SizeCalculator
{
  PanelManager panelManager;

  static Dimension tabPaneInsets;

  public SizeCalculator (PanelManager panelManager)
  {
    this.panelManager = panelManager;
  }

  interface Measurer
  {
    public Dimension getSize (Component measurable);
  }

  private Measurer preferredSizeMeasurer = new Measurer ()
  {
    public Dimension getSize (Component measurable)
    {
      return measurable.getPreferredSize ();
    }
  };

  private Measurer minimumSizeMeasurer = new Measurer ()
  {
    public Dimension getSize (Component measurable)
    {
      return measurable.getMinimumSize ();
    }
  };

  public Dimension getPreferredSizeOfLargestPanel ()
  {
     return getSize (preferredSizeMeasurer);
  }

  public Dimension getMinimumSizeOfLargestPanel ()
  {
    return getSize (minimumSizeMeasurer);
  }

  protected Dimension getSize (Measurer measurer)
  {
    Dimension largestSize = new Dimension (0, 0);

    for ( int i = 0; i < panelManager.getPanelCount (); i++ )
    {
      Component panelComponent     = panelManager.getPanelAt (i).getComponent ();
      Dimension panelComponentSize = measurer.getSize (panelComponent);
      largestSize = maxDimension (largestSize, panelComponentSize);
    }

    return enlargeByTabPaneInsets (largestSize);
  }

  protected Dimension maxDimension (Dimension first, Dimension second)
  {
    int maxWidth  = Math.max (first.width,  second.width );
    int maxHeight = Math.max (first.height, second.height);

    return new Dimension (maxWidth, maxHeight);
  }

  protected Dimension enlargeByTabPaneInsets (Dimension first)
  {
    Dimension insets = getTabPaneInsets ();
    return new Dimension (first.width + insets.width, first.height + insets.height);
  }

 /**
   * Returns the space between the edges of the tabbed pane and the
   * component selected inside it.
   *
   * @return A Dimension object with the width and height difference
   * between the tabbed pane and the component selected inside it.
   */

  public static Dimension getTabPaneInsets ()
  {
    if (tabPaneInsets == null)
      tabPaneInsets = calculateTabPaneInsets ();

    return tabPaneInsets;
  }

  private static Dimension calculateTabPaneInsets ()
  {
    JFrame      frame   = new JFrame ();
    JTabbedPane tabPane = new JTabbedPane ();

    tabPane.addTab ("Component", PanelManager.NULL_ICON, new JButton ());
    frame.getContentPane ().add (tabPane);
    frame.pack ();

    Dimension tabPaneSize = tabPane.getSize ();
    Dimension compSize    = tabPane.getComponent (0).getSize ();

    int horizontalInset = tabPaneSize.width  - compSize.width;
    int verticalInset   = tabPaneSize.height - compSize.height;

    horizontalInset     = Math.max (0, horizontalInset);
    verticalInset       = Math.max (0, verticalInset  );

    /* @todo Find out why the fudge factors need to be doubled */
    return new Dimension (2 * horizontalInset, 2 * verticalInset);
  }
}