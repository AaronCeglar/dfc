package dsto.dfc.swing.panels;

import java.awt.Component;
import java.awt.Frame;

import javax.swing.Icon;
import javax.swing.JDialog;

/** Used by the ToolView class to display a tool component
 *  see also {@link ToolView}{@link GhostFrame}{@link InsideFrame}
 *  {@link FrameLike}
 */

public class OutsideFrame extends JDialog implements FrameLike
{
  public OutsideFrame (FrameLike template, Frame owner)
  {
    super      (owner                );
    setTitle   (template.getTitle  ());
    setVisible (template.isVisible ());
    addTool    (template.getTool   ());
    pack       ();
    setDefaultCloseOperation (template.getDefaultCloseOperation ());
  }

  public void addTool (Component toolOrNull)
  {
    if ( toolOrNull != null )
      getContentPane ().add (toolOrNull);
  }

  public void removeTool (Component toolOrNull)
  {
    if ( toolOrNull != null )
      getContentPane ().remove (toolOrNull);
  }

  public Component getTool ()
  {
    return null;
  }

  public void setActive (boolean active)
  {
    setVisible (active);
  }

  public boolean is_Active ()
  {
    return isVisible ();
  }

  public void setIcon (Icon icon)
  {
    // zip
  }

  public Icon getIcon ()
  {
    return null;
  }

  public Component getFrame ()
  {
    return this;
  }

}