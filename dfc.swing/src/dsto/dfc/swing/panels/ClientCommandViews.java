package dsto.dfc.swing.panels;

import dsto.dfc.swing.commands.CommandSource;
import dsto.dfc.swing.commands.CommandView;

/**
 * A Client Command View alters the commands visible on the main menu,
 * context menu, and tool bar for a changing client. When the client
 * changes:
 *
 *   a) the old clients commands are removed from the command views
 *   b) the new clients commands are added to the command views
 *
 * @version $Revision$
 * @author Peter J Smet
 */

public class ClientCommandViews extends StandardCommandViews
{
  private CommandSource client;

  private MenuMover menuAdder = new MenuMover ()
  {
    public void moveClientMenu (CommandView menu, CommandView clientMenu)
    {
      menu.addView (clientMenu);
    }
  };

  private MenuMover menuRemover = new MenuMover ()
  {
    public void moveClientMenu (CommandView menu, CommandView clientMenu)
    {
      menu.removeView (clientMenu);
    }
  };

  public void setClient (Object commandSourceOrNull)
  {
    removeClientCommands ();

    if (commandSourceOrNull instanceof CommandSource)
      client = (CommandSource) commandSourceOrNull;
    else
      client = null;

    addClientCommands ();
  }

  public void removeClientCommands ()
  {
    moveClientMenus (menuRemover);
  }

  public void addClientCommands ()
  {
    moveClientMenus (menuAdder);
  }

  protected void moveClientMenus (MenuMover menuMover)
  {
    if (client == null)
      return;

    moveClientMenu (CommandView.MAIN_MENU_VIEW   , menuMover);
    moveClientMenu (CommandView.CONTEXT_MENU_VIEW, menuMover);
    moveClientMenu (CommandView.TOOLBAR_VIEW     , menuMover);
  }

  protected void moveClientMenu (String commandViewName, MenuMover menuMover)
  {
    CommandView menu       = getCommandView        (commandViewName);
    CommandView clientMenu = client.getCommandView (commandViewName);

    if (clientMenu == null)
      return;

    menuMover.moveClientMenu (menu, clientMenu);
  }

  interface MenuMover
  {
    public void moveClientMenu (CommandView menu, CommandView clientMenu);
  }
}