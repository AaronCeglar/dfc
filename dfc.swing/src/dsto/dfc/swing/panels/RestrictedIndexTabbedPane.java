package dsto.dfc.swing.panels;

import java.awt.Color;

import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.plaf.InsetsUIResource;

/**
 * Title:        DFC General User Interface package
 * Description:
 * @author Peter Smet
 * @version
 *
 * A tabbed pane which will not allow some tabs to be selected.
 * Tabs which are associated with internal or external frames
 * can not be selected. Instead, the frames are brought to the
 * foreground. Used by the {@link PanelManager}.
 */

public class RestrictedIndexTabbedPane extends JTabbedPane
{
  static final Color  TAB_ACTIVE     = UIManager.getColor("activeCaption"        );
  static final Color  TAB_SELECTED   = UIManager.getColor("TabbedPane.selected"  );
  static final Color  TAB_DESELECTED = UIManager.getColor("TabbedPane.background");

  boolean      removingTab = false;
  PanelManager manager;

  public RestrictedIndexTabbedPane (PanelManager manager)
  {
    this.manager = manager;
    UIManager.put ("TabbedPane.tabInsets", new InsetsUIResource(6, 4, 0, 4));
    setUI (new PJSBasicTabbedPaneUI ());
  }

  public void removeTabAt (int index)
  {
    removingTab = true;
    super.removeTabAt (index);
    removingTab = false;
  }

  public void setSelectedIndex (int newIndex)
  {
    if ( removingTab )
      super.setSelectedIndex (newIndex);

    else
      setSelectedTabIfLegal (newIndex);

    if (manager.undoFlagIsSet ())
      return;

    int oldIndex = manager.indexOfActivePanel ();
    manager.setActiveIndex (newIndex);

    manager.fireNewActivePanelEdit (oldIndex, newIndex);
  }

  /** do not allow floating panels to be selected
   */

  protected void setSelectedTabIfLegal (int index)
  {
    PanelManager.Panel activePanel = manager.getPanelAt (index);

    if ( activePanel == null || activePanel.isFixed () )
      super.setSelectedIndex (index);
  }

   /** if index represents a fixed panel, select it.
    *  otherwise, select the nearest fixed panel
    */

  protected void setSelectedIndexToNearest (int index)
  {
    PanelManager.Panel activePanel = manager.getPanelAt (index);

    if ( activePanel == null )
    {
      super.setSelectedIndex (-1);
      return;
    }

    if ( activePanel.isFixed () )
    {
      super.setSelectedIndex (index);
      return;
    }

    PanelManager.Panel nextFixedPanel = manager.getNextFixedPanel (activePanel);
    int nextIndex  = manager.indexOfPanel (nextFixedPanel);

    super.setSelectedIndex (nextIndex);
  }

  /** Forces the GUI to update.
    */

  protected void updateHilite ()
  {
    for (int i = 0; i < getTabCount (); i++)
    {
      if ( i == manager.indexOfActivePanel () )
        setBackgroundAt (i, TAB_ACTIVE    );

      else if ( i == getSelectedIndex () )
        setBackgroundAt (i, TAB_SELECTED  );

      else
        setBackgroundAt (i, TAB_DESELECTED);
    }
  }




}