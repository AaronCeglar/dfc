package dsto.dfc.swing.panels;

import java.awt.print.PageFormat;
import java.awt.print.PrinterJob;

import dsto.dfc.swing.commands.AbstractMutableCommand;
import dsto.dfc.swing.commands.CommandView;


/**
 * A Command that opens up a page layout dialog.
 *
 * @author Luke Marsh
 * @revision $Revision$
 */
public class CmdPageLayoutPanel extends AbstractMutableCommand
  implements PanelManagerListener
{

  protected PanelManager manager;
  private PageFormat pageFormat;


  /**
   * Initializes variables.
   * 
   * @param manager The PanelManager that manages the panels and views.
   */
  public CmdPageLayoutPanel (PanelManager manager)
  {
    pageFormat = new PageFormat ();
    this.manager = manager;
    manager.addPanelManagerListener (this);
    updateEnabled ();
  }


  /**
   * Opens a Page Layout dialog.
   */
  public void execute ()
  {
	pageFormat = PrinterJob.getPrinterJob ().pageDialog (pageFormat);
  }


  /**
   * Returns the page format.
   * 
   * @return The Page Format.
   */
  public PageFormat getPageFormat ()
  {
    return pageFormat;
  }


  /**
   * Returns the mnemonic for this command.
   * 
   * @return The mnemonic.
   */
  public char getMnemonic ()
  {
    return 'y';
  }


  /**
   * Sets this command to be enabled if the PanelManager contains one or more
   * panels.
   */
  protected void updateEnabled ()
  {
    setEnabled (manager.getPanelCount () > 0);
  }


  /**
   * Returns the name of this command.
   * 
   * @return The name of the command.
   */
  public String getName ()
  {
    return "file.Page Layout";
  }


  /**
   * Returns the description of this command.
   * 
   * @return The description of the command.
   */
  public String getDescription ()
  {
    return "Set the page layout";
  }


  /**
   * Returns if this command is interactive.
   * 
   * @return true.
   */
  public boolean isInteractive ()
  {
    return true;
  }


  /**
   * Assigns this command to the print group.
   * 
   * @param viewName The name of the view.
   * @return The group name, or null if no match was found.
   */
  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return "print";
    else if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return "File.print";
    else if (viewName.equals (CommandView.TOOLBAR_VIEW))
      return "print";
    else
      return null;
  }


  /**
   * Calls updateEnabled.
   * 
   * @param e The PanelManagerEvent
   */
  public void panelAdded (PanelManagerEvent e)
  {
    updateEnabled ();
  }


  /**
   * Calls updateEnabled.
   * 
   * @param e The PanelManagerEvent
   */
  public void panelRemoved (PanelManagerEvent e)
  {
    updateEnabled ();
  }


  /**
   * Not Implemented.
   * 
   * @param e The PanelManagerEvent.
   */
  public void panelFloated (PanelManagerEvent e)
  {
    // zip
  }


  /**
   * Not Implemented.
   * @param e The PanelManagerEvent.
   */
  public void panelUnfloated (PanelManagerEvent e)
  {
    // zip
  }


  /**
   * Not Implemented.
   * 
   * @param e The PanelManagerEvent.
   */
  public void aboutToRemovePanel (PanelManagerEvent e)
  {
    // zip
  }
}