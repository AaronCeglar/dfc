package dsto.dfc.swing.panels;

import java.awt.Component;
import java.awt.Point;
import java.awt.Window;

import javax.swing.JInternalFrame;

/**
 * TopLeft positions an inner frame in the top
 * left corner of a larger outer frame. The indentation level
 * nudges the inner frame by degrees towards the
 * center of the outer frame.
 *
 * @version $Revision$
 * @author Peter J Smet
 */

public class TopLeft extends Corner
{
  int   horizontalIndent = PanelManager.TAB_WIDTH;
  int   verticalIndent   = PanelManager.TAB_HEIGHT;
  int   heightDifference;
  int   widthDifference;

  public Point locationFor (Component inner, Component outer, int indentLevel)
  {
    if ( inner == null || outer == null )
      return new Point ();

    Point topLeft               = new Point ();
    Point absoluteTopLeft       = new Point (outer.getLocation ());

    if ( innerFrameTooLarge (inner, outer) )
      return usesAbsoluteCoordinates (inner) ? absoluteTopLeft : topLeft;

    indent       (topLeft, indentLevel);
    moveToCorner (topLeft             );

    if ( pointLiesWithinFrame (topLeft, outer, indentLevel) )
    {
      if ( usesAbsoluteCoordinates (inner) )
        makePointAbsolute (topLeft, absoluteTopLeft);

      return topLeft;
    }

    return locationFor (inner, outer, indentLevel - 1);
  }

  protected void makePointAbsolute (Point topLeft, Point absoluteTopLeft)
  {
    topLeft.translate( (int) absoluteTopLeft.getX(),
                       (int) absoluteTopLeft.getY() );
  }

  protected boolean pointLiesWithinFrame(Point topLeft, Component frame, int indentLevel)
  {
    return frame.contains (topLeft) || indentLevel == 0;
  }

  protected int horizontalInset (int indentLevel)
  {
    return indentLevel * horizontalIndent;
  }

  protected int verticalInset (int indentLevel)
  {
    return indentLevel * verticalIndent;
  }

  protected Point moveToCorner (Point topLeft)
  {
    return topLeft;
  }

  protected void indent (Point startCorner, int indentLevel)
  {
    startCorner.translate( horizontalInset( indentLevel ),
                           verticalInset  ( indentLevel ) );
  }

  protected void calculateFrameSizeDifference (Component inner, Component outer)
  {
    heightDifference = outer.getHeight () - inner.getHeight ();
    widthDifference  = outer.getWidth  () - inner.getWidth  ();

    if ( inner instanceof JInternalFrame )
    {
      heightDifference = heightDifference - 29;
      widthDifference  = widthDifference  - 8;
    }
  }

  protected boolean innerFrameTooLarge (Component inner, Component outer)
  {
    calculateFrameSizeDifference( inner, outer );
    return heightDifference <= 0 || widthDifference <= 0;
  }

  protected boolean usesAbsoluteCoordinates (Component frame)
  {
    return frame instanceof Window;
  }

  }