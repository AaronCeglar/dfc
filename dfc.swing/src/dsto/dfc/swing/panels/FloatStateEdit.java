package dsto.dfc.swing.panels;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import javax.swing.Icon;

import dsto.dfc.swing.icons.ImageLoader;
import dsto.dfc.swing.undo.PropertyEdit;
import dsto.dfc.swing.undo.PropertyEditBean;

/**
 * Edit to represent a change in floating status, customised from the
 * {@link PropertyEdit} for its display name.
 *
 * @author Peter J Smet
 * @version $Revision$
 */
public class FloatStateEdit extends PropertyEditBean
{
  static String description =
    "Records a change in the float status of a PanelManager.Panel";
  static final Icon EDIT_ICON = ImageLoader.getIcon ("/dsto/ive/images/icons/overlays.gif");

  protected transient PanelManager panelManager;

  protected int    panelIndex;
  protected String panelName;


  /** For bean serialisation only */
  public FloatStateEdit ()
  {
    // zip
  }

  public FloatStateEdit (PanelManager panelManager, int panelIndex, String oldState, String newState)
  {
    super (null, "floating", oldState, newState);
    this.panelManager = panelManager;
    this.panelIndex   = panelIndex;

    if (panelManager == null)
      panelName = "null panel";
    else
      panelName = panelManager.nameForPanelAt (panelIndex);
  }

  // Bean methods

  /** Overridden to prevent PanelManager.Panel serialisation attempt. */
  public Object getSource ()
  {
    return null;
  }

  /** Overridden to prevent PanelManager.Panel serialisation attempt. */
  public void setSource (Object newSource)
  {
    // zip
  }

  public void setPanelIndex (int newPanelIndex)
  {
    panelIndex = newPanelIndex;
  }

  public int getPanelIndex ()
  {
    return panelIndex;
  }

  public void setPanelName (String newPanelName)
  {
    panelName = newPanelName;
  }

  public String getPanelName ()
  {
    return panelName;
  }

  public String getDescription ()
  {
    return description;
  }

  // end Bean methods

  public void undo ()
  {
    panelManager.setUndoFlag ();

    if (undoRedoTargetEstablished ())
      super.undo ();

    panelManager.unsetUndoFlag ();
  }

  public void redo ()
  {
    panelManager.setUndoFlag ();

    if (undoRedoTargetEstablished ())
      super.redo ();

    panelManager.unsetUndoFlag ();
  }

  private boolean undoRedoTargetEstablished ()
  {
    source = panelManager.getPanelAt (panelIndex);
    return source != null;
  }

  public boolean canUndo ()
  {
    return super.canUndo () && undoRedoTargetEstablished ();
  }

  public boolean canRedo ()
  {
    return super.canRedo () && undoRedoTargetEstablished ();
  }

  public String getPresentationName ()
  {
    return panelName  + " " + getNameForPanelType () +
      " [" + oldValue + " -> " + newValue + "]";
  }

  protected String getNameForPanelType ()
  {
    return "Panel";
  }

  private String mapFloatStateToPanelManagerConstant (Object floatState)
  {
    if (PanelManager.FLOAT_EXTERNAL.equals (floatState))
      return PanelManager.FLOAT_EXTERNAL;

    if (PanelManager.FLOAT_INTERNAL.equals (floatState))
      return PanelManager.FLOAT_INTERNAL;

    if (PanelManager.FIXED.equals (floatState))
      return PanelManager.FIXED;

    throw new IllegalArgumentException ("Misnamed Panel Manager Float State");
  }

  public Icon getIcon ()
  {
    return EDIT_ICON;
  }

  // Restorable interface

  public void restoreContext (HashMap context)
  {
    panelManager = (PanelManager) context.get ("panelManager");
    oldValue     = mapFloatStateToPanelManagerConstant (oldValue);
    newValue     = mapFloatStateToPanelManagerConstant (newValue);
  }

  // Serialisation support

  protected void writeSource (ObjectOutputStream out) throws IOException
  {
    out.writeObject (null);
  }
}