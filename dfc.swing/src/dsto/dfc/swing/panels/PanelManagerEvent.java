package dsto.dfc.swing.panels;

import java.util.EventObject;

/**
 * A PanelManagerEvent is passed as a parameter to
 * PanelManagerListener's when a Panel is floated, unfloated, added,
 * or removed. The remove panel event can be vetoed by one or more
 * listeners.
 *
 * @version $Revision$
 */
public class PanelManagerEvent extends EventObject
{
  private PanelManager.Panel panel;
  private boolean vetoed = false;

  public PanelManagerEvent (PanelManager.Panel panel)
  {
    super (panel.getManager ());

    this.panel = panel;
  }

  public PanelManager.Panel getPanel ()
  {
    return panel;
  }

  /**
   * True if an aboutToRemovePanel event has been vetoed with veto ().
   */
  public boolean isVetoed ()
  {
    return vetoed;
  }

  /**
   * Veto the removal of a panel.  Only valid while receiving an
   * aboutToRemovePanel () notification.
   */
  public void veto ()
  {
    vetoed = true;
  }
}
