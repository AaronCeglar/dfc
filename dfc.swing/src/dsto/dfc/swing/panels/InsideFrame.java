package dsto.dfc.swing.panels;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLayeredPane;

/** Used by the ToolView class to display a tool component
 *  see also {@link ToolView}{@link GhostFrame}{@link OutsideFrame}
 *  {@link FrameLike}
 */

public class InsideFrame extends JInternalFrame implements FrameLike
{
   public InsideFrame (FrameLike template, JFrame owner)
  {
    super      ("", true, true      );
    setTitle   (template.getTitle ());
    setVisible (template.isVisible());
    setIcon    (template.getIcon  ());
    addTool    (template.getTool  ());
    pack       ();
    setDefaultCloseOperation (template.getDefaultCloseOperation ());

    if ( owner != null )
      owner.getLayeredPane ().add
        (this, JLayeredPane.POPUP_LAYER);
  }

  public void setIcon (Icon icon)
  {
    setFrameIcon (icon);
  }

  public Icon getIcon ()
  {
    return getFrameIcon ();
  }

  public void setActive (boolean active)
  {
    setVisible (active);
  }

  public boolean is_Active ()
  {
    return isVisible ();
  }

  public void addTool (Component toolOrNull)
  {
    if ( toolOrNull != null )
      getContentPane ().add (toolOrNull);
  }

  public void removeTool (Component toolOrNull)
  {
    if ( toolOrNull != null )
      getContentPane ().remove (toolOrNull);
  }

  public Component getFrame ()
  {
    return this;
  }

  public Component getTool ()
  {
    return null;
  }

}