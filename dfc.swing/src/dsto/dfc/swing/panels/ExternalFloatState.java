package dsto.dfc.swing.panels;

import java.awt.Component;
import java.awt.Frame;
import java.awt.Window;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/** The External FloatState represents a native window. It
 *  displays the component inside a PanelManager.Panel.
 */

public class ExternalFloatState extends InternalFloatState
{
  protected JFrame localFrame;

  public ExternalFloatState (PanelManager.Panel panel,
                             RestrictedIndexTabbedPane tabPane,
                             Icon icon, int index)
  {
    super (panel, tabPane, icon, index);
  }

  // Unfortunately, cannot remove close icon from native windows

  protected void makeFrameUncloseable ()
  {
    // zip
  }

  public void dispose ()
  {
    if (localFrame != null)
    {
      localFrame.dispose ();
      localFrame = null;
    }
    
    super.dispose ();
  }

  public void showIcon ()
  {
    if ( localFrame == null )
      return;

    Window container = SwingUtilities.windowForComponent (tabPane);

    if ( icon instanceof ImageIcon )
      localFrame.setIconImage ( ((ImageIcon) icon ).getImage() );
    else
      if ( container instanceof Frame )
        localFrame.setIconImage ( ((Frame) container ).getIconImage() );
  }

  protected void refreshTitle (TitleShowPolicy titleDisplay)
  {
    localFrame.setTitle (titleDisplay.getWindowDisplayText ());
  }

  public void addListener (PanelManager.WindowEventListener listener)
  {
     localFrame.addWindowListener (listener);
  }

  public void removeListener (PanelManager.WindowEventListener listener)
  {
     localFrame.removeWindowListener (listener);
  }

  public void setActive (boolean active)
  {
     if (active)
       localFrame.requestFocus  ();
     else // transfer focus to parent window
     {
       Component next = SwingUtilities.windowForComponent (panel.getManager ());
       if (next != null)
         next.requestFocus ();
     }
  }

  protected void showFrame (boolean visible)
  {
     localFrame.setVisible (visible);
  }

  public String getFloating ()
  {
    return PanelManager.FLOAT_EXTERNAL;
  }

  protected void setUpDisplay (int index)
  {
    localFrame = new JFrame ();
    localFrame.getRootPane().putClientProperty ("panel", panel);
    localFrame.getContentPane ().add (placeHolder);
    java.awt.Dimension prefSize = panel.getComponent ().getPreferredSize ();
    localFrame.setSize ((int)(prefSize.width * 1.1), (int)(prefSize.height * 1.1));
    localFrame.setLocation (calculateLocation (index));
    localFrame.setDefaultCloseOperation (JFrame.DO_NOTHING_ON_CLOSE);
  }

  public void setUpCommands ()
  {
    commands.add (unFloatCommand       );
    commands.add (internalFloatCommand );
    commands.add (closeCommand         );
  }

}