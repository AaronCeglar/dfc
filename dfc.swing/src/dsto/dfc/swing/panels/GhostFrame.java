package dsto.dfc.swing.panels;

import java.awt.Component;

import javax.swing.Icon;

/** LightWeight imposter class acting as a proxy for internal or
 *  external frames. Used to implement lazy initialization of
 *  windows by the ToolView class {@link ToolView}
 *  see also {@link OutsideFrame}{@link InsideFrame}{@link FrameLike}
 */

public class GhostFrame implements FrameLike
{
  String                title   = "";
  boolean               visible = false;
  int                   defaultCloseOperation = ToolView.HIDE_ON_CLOSE;
  Component             tool;
  Icon                  icon;

  public void setTitle (String title)
  {
    this.title = title;
  }

  public String getTitle ()
  {
    return title;
  }

  public void setIcon (Icon icon)
  {
    this.icon = icon;
  }

  public Icon getIcon ()
  {
    return icon;
  }

  public void setActive (boolean active)
  {
    setVisible (active);
  }

  public boolean is_Active ()
  {
    return isVisible ();
  }

  public void setVisible (boolean visible)
  {
    this.visible = visible;
  }

  public boolean isVisible ()
  {
    return visible;
  }

  public void setDefaultCloseOperation (int operation)
  {
    defaultCloseOperation = operation;
  }

  public int getDefaultCloseOperation ()
  {
    return defaultCloseOperation;
  }

  public void addTool (Component toolOrNull)
  {
    tool = toolOrNull;
  }

  public void removeTool (Component toolOrNull)
  {
    tool = null;
  }

  public Component getTool ()
  {
    return tool;
  }

  public void dispose ()
  {
    // zip
  }

  public void pack ()
  {
    // zip
  }

  public Component getFrame ()
  {
    return null;
  }

}

