package dsto.dfc.swing.panels;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Scrollable;
import javax.swing.SwingUtilities;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.undo.UndoableEdit;

import dsto.dfc.logging.Log;
import dsto.dfc.swing.commands.Command;
import dsto.dfc.swing.commands.CommandSource;
import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.swing.icons.Iconic;
import dsto.dfc.swing.icons.NullIcon;
import dsto.dfc.swing.undo.UndoableEditSource;
import dsto.dfc.util.Disposable;

/**
 * Manages a set of panels similar to a Window Manager. Clients add a
 * panel by passing in a component to be managed within
 * PanelManager.Panel via addPanel (). The panel acts like a window
 * that may exist in one of three states. The FIXED state displays the
 * component within a tabbed pane, the FLOAT_INTERNAL state uses a
 * lightweight java frame, while the FLOAT_EXTERNAL state uses a
 * native window. The display state is controlled by setFloating ().
 * Panels may also have child "tool" views which are shown in floated
 * windows that are only visible when the panel is active (see
 * Panel.addToolView ()).
 *
 * @version $Revision$
 * @author Peter J Smet
 */
public class PanelManager extends JPanel
  implements CommandSource, UndoableEditSource, UndoableEditListener
{
  public static final Icon   NULL_ICON = new NullIcon (20, 20);
  public static final String NULL_NAME = "Not Named";

  protected static final int TAB_WIDTH  = 35;
  protected static final int TAB_HEIGHT = 28;

  protected static Dimension tabPaneInsets;

  public static final String FIXED          = "Unfloat";
  public static final String FLOAT_INTERNAL = "Float Internal";
  public static final String FLOAT_EXTERNAL = "Float External";

  protected List                      panels              = new ArrayList ();
  protected Panel                     activePanel;
  protected WindowEventListener       windowEventListener = new WindowEventListener ();
  protected EditPropagator            editPropagator      = new EditPropagator (this);
  protected HidePolicy                hidePolicy          = new HidePolicy (this);
  public    RestrictedIndexTabbedPane tabPane             = new RestrictedIndexTabbedPane (this);
  private   SizeCalculator            sizeCalculator      = new SizeCalculator (this);

  private int undoFlag = 0;

  protected DynamicCommandViews commandViewJuggler;

  protected transient Vector panelManagerListeners;

  public PanelManager ()
  {
    commandViewJuggler = new DynamicCommandViews (this);
    setLayout (new BorderLayout ());
    add (tabPane, BorderLayout.CENTER);
  }

  /**
   * When autoHide is true, the PanelManager becomes invisible when it
   * contains no panels
   */
  public void setAutoHide (boolean hideWhenNoPanels)
  {
    if ( hideWhenNoPanels )
      hidePolicy.hideWhenEmpty ();
    else
      hidePolicy.neverHide ();
  }

  // CommandSource interface

  public CommandView getCommandView (String viewName)
  {
    return commandViewJuggler.getCommandView (viewName);
  }

  // end CommandSource interface

  /**
   * Add a new panel to the manager. If the component implements
   * javax.swing.Scrollable, then the component will be added inside a
   * JScrollPane.
   *
   * @param component The component to be shown in the panel.
   * @param index The positio of the panel within the panels list.
   * @param name The name to display for the panel.
   * @param icon The icon to display next to the panel (may be null).
   * @return The new panel that is holding the component.
   */
  public Panel addPanel (Component component, int index, String name, Icon icon)
  {
    return addPanel (component, index, name, icon, true,
                     component instanceof Scrollable);
  }

  /**
   * Add a new panel to the manager.
   *
   * @param component The component to be shown in the panel.
   * @param name The name to display for the panel.
   * @param icon The icon to display next to the panel (may be null).
   * @return The new panel that is holding the component.
   */
  public Panel addPanel (Component component, String name, Icon icon)
  {
    return addPanel (component, getPanelCount(), name, icon);
  }

  /**
   * Add a new panel to the manager.
   *
   * @param component The component to be shown in the panel.
   * @param index The positio of the panel within the panels list.
   * @return The new panel that is holding the component.
   */
  public Panel addPanel (Component component, int index)
  {
    return addPanel (component, index, NULL_NAME, NULL_ICON);
  }

  /**
   * Add a new panel to the manager, using default settings for all
   * other options.
   *
   * @param component The component to be shown in the panel.
   * @return The new panel that is holding the component.
   */
  public Panel addPanel (Component component)
  {
    return addPanel (component, getPanelCount());
  }

  /**
   * Add a new panel to the manager.
   *
   * @param component The component to be shown in the panel.
   * @param index The positio of the panel within the panels list.
   * @param name The name to display for the panel.
   * @param icon The icon to display next to the panel (may be null).
   * @param showingName True if name should be shown.
   * @param useScrollPane True if panel should be hosted within a
   * scroll pane.
   * @return The new panel that is holding the component.
   */
  public Panel addPanel (Component component, int index, String name, Icon icon,
                         boolean showingName, boolean useScrollPane)
  {
    Panel managedPanel = new Panel (component, name, icon,
                                    showingName, useScrollPane, index);

    basicAddPanel (managedPanel, index);

    return managedPanel;
  }

  /**
   * Do the job of adding a new Panel instance, updating active view,
   * firing panel added event, etc.
   */
  protected void basicAddPanel (Panel panel, int index)
  {
    panels.add (index, panel);

    if (activePanel == null)
    {
      setUndoFlag ();
      try
      {
        setActivePanel (panel);
      }
      finally
      {
        unsetUndoFlag ();
      }
    }

    firePanelAdded (new PanelManagerEvent (panel));
  }

  /**
   * Remove all panels and dispose of any resources.
   */
  public void removeAllPanels ()
  {
    for (int i = getPanelCount () - 1; i >= 0; i--)
    {
      try
      {
        Panel panel = getPanelAt (i);
        removePanel (panel);
        panel.dispose ();
      }
      catch (Exception ex)
      {
        Log.warn ("Unable to close panel " + i, this, ex);
      }
    }
  }

  /**
   * True if a given panel is inside this manager.
   */
  public boolean containsPanel (Panel managedPanel)
  {
    return indexOfPanel (managedPanel) > -1;
  }

  /**
   * Remove a panel. If the active panel is removed, a suitable
   * remaining panel is made the active panel.
   */
  public boolean removePanel (Panel managedPanel)
  {
    setUndoFlag ();

    try
    {
      if ( ! containsPanel (managedPanel) ||
             listenersRejectPanelRemoval (managedPanel) )
        return false;

      boolean wasActive = managedPanel.isActive ();

      Panel nextPanel         = getNextPanel (managedPanel);
      int indexOfRemovedPanel = indexOfPanel (managedPanel);

      panels .remove      (managedPanel       );
      tabPane.removeTabAt (indexOfRemovedPanel);

      if ( wasActive )
        setActivePanel (nextPanel);

      tabPane.setSelectedIndexToNearest (indexOfActivePanel ());

      tabPane.updateHilite ();

      firePanelRemoved (new PanelManagerEvent (managedPanel));

      managedPanel.setVisible (false);
    }
    finally
    {
      unsetUndoFlag ();
    }

    return true;
  }

  /**
   * Replace an existing panel with a new one.
   *
   * @param oldPanel The existing panel to replace.
   * @param newComponent The new component to add.
   * @param name The name of the new panel.
   * @param icon The icon of the new panel.
   * @return The replacement panel.
   */
  public Panel replacePanel (Panel oldPanel, Component newComponent,
                             String name, Icon icon)
  {
    Panel newPanel = addPanel( newComponent, oldPanel.getIndex(),
                               name,  icon,
                               oldPanel.isShowingName(),
                               oldPanel.isScrollable() );

    newPanel.setFloating (oldPanel.getFloating ());
    removePanel (oldPanel);

    return newPanel;
  }

  /**
   * Replace an existing panel with a new one.
   *
   * @param oldPanel The existing panel to replace.
   * @param newComponent The new component to add.
   * @param name The name of the new panel.
   * @return The replacement panel.
   */
  public Panel replacePanel (Panel oldPanel, Component newComponent,
                             String name)
  {
    return replacePanel (oldPanel, newComponent, name, NULL_ICON);
  }

  /**
   * Replace an existing panel with a new one.
   *
   * @param oldPanel The existing panel to replace.
   * @param newComponent The new component to add.
   * @return The replacement panel.
   */
  public Panel replacePanel (Panel oldPanel, Component newComponent)
  {
    return replacePanel (oldPanel, newComponent, NULL_NAME);
  }

  /**
   * Set the active panel. Only one panel may be active at any time.
   */
  public void setActivePanel (Panel newActivePanel)
  {
    if (newActivePanel != null && !containsPanel (newActivePanel))
      return;

    Panel oldActivePanel = activePanel;

    if (oldActivePanel != newActivePanel)
      hideOldActivePanel ();

    activePanel = newActivePanel;

    showNewActivePanel ();

    tabPane.updateHilite ();

    firePropertyChange ("activePanel", oldActivePanel, newActivePanel);
  }

  /**
   * Set the active panel to be the panel at a given index.
   *
   * @param index The index of the new active panel.
   *
   * @see #setActivePanel
   */
  public void setActiveIndex (int index)
  {
    setActivePanel (getPanelAt (index));
  }

  /**
   * Return the active panel.
   *
   * @see #setActivePanel
   */
  public Panel getActivePanel ()
  {
    return activePanel;
  }

  /**
   * The number of panels.
   */
  public int getPanelCount ()
  {
    return panels.size();
  }

  /**
   * The index of a panel.

   * @param panel The panel.
   * @return The index of the panel, or -1 if not found.
   */
  public int indexOfPanel (Panel panel)
  {
    return panels.indexOf (panel);
  }

  private boolean isValidIndex (int index)
  {
    return index > -1 && index < getPanelCount ();
  }

  /**
   * The index of the currently active panel.
   */
  public int indexOfActivePanel ()
  {
    return indexOfPanel (activePanel);
  }

  /**
   * Get the panel at a given index.
   */
  public Panel getPanelAt (int index)
  {
    if ( isValidIndex (index) )
      return (Panel) panels.get (index);

    return null;
  }

  /**
   * The name for a panel at a given index.
   */
  public String nameForPanelAt (int index)
  {
    Panel panel = getPanelAt (index);
    if (panel == null)
      return "null panel";

    return panel.getName ();
  }

  /**
   * Get the panel that is hosting a given component.
   *
   * @param component The component.
   * @return The panel hosting component, or null if not found.
   */
  public Panel getPanel (Component component)
  {
    for (int i = 0; i < getPanelCount (); i++)
    {
      Panel panel = getPanelAt (i);

      if ( panel.getComponent () == component)
        return panel;
    }

    return null;
  }

  /**
   * Get the panel that is the logical next panel after a given panel.
   * This is the panel to the left of the panel, or the last panel if
   * the panel is first.
   */
  public Panel getNextPanel (Panel startPanel)
  {
    if ( getPanelCount () <= 1 )
      return null;

    int nextIndex = indexOfPanel (startPanel) - 1;
    int lastIndex = getPanelCount () - 1;

    if ( nextIndex == -1 )
      nextIndex = lastIndex;

    return getPanelAt (nextIndex);
  }

  protected void hideOldActivePanel ()
  {
    showActivePanel (false);
  }

  protected void showNewActivePanel ()
  {
    showActivePanel (true);
  }

  protected void showActivePanel (boolean show)
  {
    if (activePanel != null)
      activePanel.setActive (show);
  }

  @Override
  public Dimension getPreferredSize ()
  {
    return sizeCalculator.getPreferredSizeOfLargestPanel ();
  }

  @Override
  public Dimension getMinimumSize ()
  {
    return sizeCalculator.getMinimumSizeOfLargestPanel ();
  }

  protected Panel getNextFixedPanel (Panel startPanel)
  {
    Panel currentPanel = startPanel;

    for ( ; ; )
    {
      currentPanel = getNextPanel (currentPanel);

      if (currentPanel == startPanel || currentPanel == null)
        return null;

      if (currentPanel.isFixed())
        return currentPanel;
    }
  }

  protected boolean listenersRejectPanelRemoval (Panel panel)
  {
    return ! fireAboutToRemovePanel (panel);
  }

  /**
   * Create an UndoableEdit that represents a change in the currently
   * active panel
   */
  protected UndoableEdit createActivePanelEdit (int oldActiveIndex, int newActiveIndex)
  {
    return new ActivePanelEdit (this, oldActiveIndex, newActiveIndex);
  }

  /**
   * If the active panel has changed, let undoable edit listeners
   * know.  The MonitorPanel does not fire Edits - these get in the
   * way of the user.
   */
  protected void fireNewActivePanelEdit (int oldIndex, int newIndex)
  {
    boolean indicesIdentical = oldIndex == newIndex;
    boolean indicesInvalid   = ! (isValidIndex (oldIndex) && isValidIndex (newIndex));

    if ( indicesInvalid || indicesIdentical)
      return;

    if (panelDoesNotFireEdits (oldIndex) || panelDoesNotFireEdits (newIndex))
      return;

    UndoableEdit activePanelChangedEdit = createActivePanelEdit (oldIndex, newIndex);
    editPropagator.tellListenersAPanelWasEdited (activePanelChangedEdit);
  }

  protected boolean panelDoesNotFireEdits (int index)
  {
    return nameForPanelAt (index).equals ("Monitor");
  }

  /**
   * Create an UndoableEdit that represents a change in the floating
   * state of a panel
   */
  protected FloatStateEdit createNewFloatStateEdit (String oldFloatState,
                                                    String newFloatState)
  {
    return new FloatStateEdit (this, indexOfActivePanel (), oldFloatState, newFloatState);
  }

  /**
   * Workaround for avoiding ActivePanelEdits being fired while
   * undoing previous ActivePanelEdits. When invoking {@link
   * #setActivePanel} make sure to invoke <code>setUndoFlag</code>
   * before the call and <code>{@link #unsetUndoFlag}</code> after the
   * call.
   */
  protected void setUndoFlag ()
  {
    undoFlag++;
  }

  protected void unsetUndoFlag ()
  {
    if (undoFlag > 0)
      undoFlag--;
    else
      Log.warn ("Unbalanced invocation of unsetUndoFlag () ignored", this);
  }

  protected boolean undoFlagIsSet ()
  {
    return undoFlag > 0;
  }

  // UndoableEditSource interface

  public void addUndoableEditListener (UndoableEditListener listener)
  {
    editPropagator.addUndoableEditListener (listener);
  }

  public void removeUndoableEditListener (UndoableEditListener listener)
  {
    editPropagator.removeUndoableEditListener (listener);
  }

  // end UndoableEditSource interface

  // UndoableEditListener interface

  public void undoableEditHappened (UndoableEditEvent evt)
  {
    editPropagator.tellListenersAPanelWasEdited (evt.getEdit ());
  }

  // end UndoableEditListener interface

  public synchronized void removePanelManagerListener(PanelManagerListener l)
  {
    if(panelManagerListeners != null && panelManagerListeners.contains(l))
    {
      Vector v = (Vector) panelManagerListeners.clone();
      v.removeElement(l);
      panelManagerListeners = v;
    }
  }

  public synchronized void addPanelManagerListener(PanelManagerListener l)
  {
    Vector v = panelManagerListeners == null ? new Vector(2) : (Vector) panelManagerListeners.clone();
    if(!v.contains(l))
    {
      v.addElement(l);
      panelManagerListeners = v;
    }
  }

  protected void firePanelAdded(PanelManagerEvent e)
  {
    if(panelManagerListeners != null)
    {
      Vector listeners = panelManagerListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((PanelManagerListener) listeners.elementAt(i)).panelAdded(e);
      }
    }
  }

  protected void firePanelRemoved(PanelManagerEvent e)
  {
    if(panelManagerListeners != null)
    {
      Vector listeners = panelManagerListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((PanelManagerListener) listeners.elementAt(i)).panelRemoved(e);
      }
    }
  }

  protected void firePanelFloated(PanelManagerEvent e)
  {
    if(panelManagerListeners != null)
    {
      Vector listeners = panelManagerListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((PanelManagerListener) listeners.elementAt(i)).panelFloated(e);
      }
    }
  }

  protected void firePanelUnfloated(PanelManagerEvent e)
  {
    if(panelManagerListeners != null)
    {
      Vector listeners = panelManagerListeners;
      int count = listeners.size();
      for (int i = 0; i < count; i++)
      {
        ((PanelManagerListener) listeners.elementAt(i)).panelUnfloated(e);
      }
    }
  }

  /**
   * Fire the aboutToRemovePanel event.
   *
   * @return True if no listener vetoed the event (ie true if OK to remove
   * the panel).
   */
  protected boolean fireAboutToRemovePanel (Panel panel)
  {
    if (panelManagerListeners == null)
      return true;

    PanelManagerEvent e = new PanelManagerEvent (panel);
    Vector listeners = panelManagerListeners;
    int count = listeners.size ();

    for (int i = 0; i < count; i++)
      ((PanelManagerListener) listeners.elementAt(i)).aboutToRemovePanel (e);

    return ! e.isVetoed ();
  }

  //------inner classes-----------------------------------------------------------

  /**
   * A panel managed by the panel manager.
   */
  public class Panel implements CommandSource, Iconic, Disposable
  {
    /** Determines whether the Tab for this panel will display only an
     *  icon, (hideTitle) or and icon and a textual title (showTitle).
    */
    protected TitleShowPolicy  showTitle = new TitleShowPolicy ();
    protected TitleShowPolicy  hideTitle = new TitleHidePolicy ();
    public    TitleShowPolicy  titleDisplay;

    protected boolean            scrollable  = false;
    protected Icon               icon;

    /** The visible component to be displayed by this Panel */
    protected Component          component;

    /** represents the current active state of this Panel */
    protected FixedFloatState    state;
    /** the component is displayed in the tabbed pane in this state */
    protected FixedFloatState    fixedState;
    /** the component is displayed in a lightweight JFrame in this state */
    protected InternalFloatState internalFloatState;
    /** the component is displayed in a native Frame in this state */
    protected InternalFloatState externalFloatState;

    protected ClientCommandViews commandViews = new ClientCommandViews ();

    protected List toolViews = new ArrayList();

    protected Panel (Component component, String name, Icon icon,
                     boolean showingName, boolean scrollable, int index)
    {
      this.component   = component;
      this.scrollable  = scrollable;
      this.icon = (icon == null) ? PanelManager.NULL_ICON : icon;

      setTitleDisplay (showingName);
      titleDisplay.setName (name);

      fixedState = new FixedFloatState (this, tabPane, this.icon, index);
      state = fixedState;
      state.setVisible   (true);

      addAllCommands ();
    }

    /**
     * Ensure this panel cannot be closed.
     */
    public void makeUncloseable ()
    {
      removeAllCommands ();

      fixedState           .makeUncloseable ();
      internalFloatState ().makeUncloseable ();
      externalFloatState ().makeUncloseable ();

      addAllCommands ();
    }

    /**
     * Close (hide and remove) this panel.
     */
    public void close ()
    {
      state.close ();
    }

    /**
     * Hide/show the panel.
     */
    public void setVisible (boolean visible)
    {
      component.setVisible (visible);
      state.setVisible (visible);
    }

    public void dispose ()
    {
      if ( alreadyDisposed () )
        return;

      disposeToolViews ();

      fixedState.dispose ();
      disposeState (internalFloatState);
      disposeState (externalFloatState);

      if ( component instanceof Disposable )
         ((Disposable) component).dispose ();

      component = null;
      state     = null;
      fixedState = null;
      internalFloatState = null;
      externalFloatState = null;
      component = null;
    }

    protected boolean alreadyDisposed ()
    {
      return component == null && state == null;
    }

    public PanelManager getManager ()
    {
      return PanelManager.this;
    }

    /**
     * Create a new tool view attached to the panel containing a
     * component.  Tool views appear in a floating window when the
     * panel is active.
     */
    public ToolView addToolView (Component tool)
    {
      Frame  externalFrameOwner = ( (ExternalFloatState) externalFloatState() ).localFrame;
      Window window = SwingUtilities.windowForComponent (PanelManager.this);
      JFrame internalFrameOwner = window instanceof JFrame ? (JFrame) window : null;

      ToolView toolView = new ToolView (this, tool, internalFrameOwner, externalFrameOwner);
      toolViews.add        (toolView);
      toolView.putInCorner (toolView.defaultCorner(), toolViews.size());
      toolView.changeState (getFloating ());
      toolView.setActive   (isActive    ());
      toolView.setIcon     (icon          );

      return toolView;
    }

    /**
     * Remove a tool view.
     *
     * @see #addToolView
     */
    public void removeToolView (ToolView toolView)
    {
      toolViews.remove (toolView);
      toolView.dispose ();
    }

    /**
     * The number of tool views attached to this panel.
     */
    public int getToolViewCount ()
    {
      return toolViews.size ();
    }

    private void disposeState (InternalFloatState oldState)
    {
      if (oldState != null)
      {
        oldState.removeListener (windowEventListener);
        oldState.dispose ();
      }
    }

    // lazy initialization

    private InternalFloatState internalFloatState ()
    {
      if (internalFloatState == null)
        internalFloatState = createFloatState (FLOAT_INTERNAL);

      return internalFloatState;
     }

     // lazy initialization

    private InternalFloatState externalFloatState ()
    {
      if (externalFloatState == null)
        externalFloatState = createFloatState (FLOAT_EXTERNAL);

      return externalFloatState;
    }

    private InternalFloatState createFloatState (String floatState)
    {
      InternalFloatState newState = null;

      if (floatState == FLOAT_INTERNAL)
        newState = new InternalFloatState (this, tabPane, icon, getIndex ());

      else if (floatState == FLOAT_EXTERNAL)
        newState = new ExternalFloatState (this, tabPane, icon, getIndex ());

      if (newState != null)
      {
        newState.addListener  (windowEventListener);
        newState.refreshTitle (titleDisplay       );
      }

      return newState;
    }

    public CommandView getCommandView (String viewName)
    {
      return commandViews.getCommandView (viewName);
    }

    public Command getDefaultCommand ()
    {
      return state.getDefaultCommand ();
    }

    protected void refreshTitles ()
    {
      fixedState           .refreshTitle (titleDisplay);
      internalFloatState ().refreshTitle (titleDisplay);
      externalFloatState ().refreshTitle (titleDisplay);
    }

    public boolean getChanged ()
    {
      return titleDisplay.getChanged ();
    }

    /**
     * Called when the component represented by this Panel has been
     * logically edited or changed.  Changed panels are shown with an
     * asterisk next to their title.
     */
    public void setChanged (boolean changed)
    {
      titleDisplay.setChanged (changed);
      refreshTitles ();
    }

    /**
     * Set whether the panel name is displayed.
     */
    public void setShowingName (boolean showingName)
    {
      String   name    = titleDisplay.getName    ();
      boolean  changed = titleDisplay.getChanged ();

      setTitleDisplay (showingName);

      titleDisplay.setName    (name   );
      titleDisplay.setChanged (changed);

      fixedState.refreshTitle (titleDisplay);
    }

    protected void setTitleDisplay (boolean showingTitle)
    {
      if ( showingTitle )
        titleDisplay = showTitle;
      else
        titleDisplay = hideTitle;
    }

    public boolean isShowingName ()
    {
      return titleDisplay.isShowingName ();
    }

    public void setName (String name)
    {
      titleDisplay.setName (name);
      refreshTitles ();
    }

    public String getName ()
    {
      return titleDisplay.getName ();
    }

    public Icon getIcon ()
    {
      return state.getIcon();
    }

    public Icon getLargeIcon ()
    {
      return null;
    }

    public void setComponent (Component component)
    {
      this.component = component;
      extractCommandViewsFrom (component);
    }

    public Component getComponent ()
    {
      return component;
    }

    protected void extractCommandViewsFrom (Object commandSource)
    {
      commandViews.setClient (commandSource);
    }

    public int getIndex ()
    {
      return indexOfPanel (this);
    }

    public boolean isFloating ()
    {
       return ! isFixed();
    }

    public boolean isFixed ()
    {
       return getFloating () == FIXED;
    }

    public void fireFloatChangeEvent()
    {
      if (isFloating())
        firePanelFloated   (new PanelManagerEvent (this));

      else
        firePanelUnfloated (new PanelManagerEvent (this));
    }

    /** Set how the panel is displayed.  One of FLOAT_EXTERNAL,
     * FLOAT_INTERNAL or FIXED.
     */
    public void setFloating (String floatStatus)
    {
      if ( getFloating () == floatStatus )
        return;

      if ( floatStatus == FLOAT_INTERNAL )
        changeState( internalFloatState () );

      if ( floatStatus == FLOAT_EXTERNAL )
        changeState( externalFloatState () );

      if ( floatStatus == FIXED )
        changeState ( fixedState );

      tabPane.setIconAt (getIndex (), state.getTabDisplayIcon ());
      tabPane.updateHilite ();
      changeToolViewState (floatStatus);
      fireFloatChangeEvent ();
    }

    public String getFloating ()
    {
      return state.getFloating ();
    }

    private void changeState (FixedFloatState newState)
    {
      state   .setVisible (false);
      newState.setVisible (true );

      removeAllCommands ();

      state = newState;

      addAllCommands ();

      setActive (true);

      if ( isFloating () )
        showNextFixedPanelInTabPane ();
      else
        tabPane.setSelectedTabIfLegal (getIndex ());
    }

    /**
     * The panel's commands are merged with the client component's
     * commands.
     */
    public void addAllCommands ()
    {
      commandViews.addCommands (state.getCommands ());
      extractCommandViewsFrom  (component           );
    }

    public void removeAllCommands ()
    {
      commandViews.removeCommands (state.getCommands ());
      commandViews.setClient (null);
    }

    public void switchCloseCommand (Command publishedCloseCommand)
    {
      removeAllCommands ();

      fixedState           .switchCloseCommand (publishedCloseCommand);
      internalFloatState ().switchCloseCommand (publishedCloseCommand);
      externalFloatState ().switchCloseCommand (publishedCloseCommand);

      addAllCommands ();
    }

    protected void showNextFixedPanelInTabPane ()
    {
       tabPane.setSelectedIndexToNearest (getIndex ());
    }

    protected void disposeToolViews ()
    {
      for ( int i = 0; i < toolViews.size (); i++ )
        getToolViewAt (i).dispose ();
    }

    protected void setToolViewsActive (boolean active)
    {
      for ( int i = 0; i < toolViews.size (); i++ )
        getToolViewAt (i).setActive (active);
    }

    protected void changeToolViewState (String newState)
    {
      for ( int i = 0; i < toolViews.size (); i++ )
        getToolViewAt (i).changeState (newState);
    }

    protected ToolView getToolViewAt (int index)
    {
      return (ToolView) toolViews.get (index);
    }

    public void setActive (boolean active)
    {
      setToolViewsActive  (active);
      state.setActive     (active);
    }

    public boolean isActive ()
    {
      return activePanel == this;
    }

    protected boolean isScrollable ()
    {
      return scrollable;
    }

    public JScrollPane getTabPaneScrollPane () // todo: rename
    {
      return fixedState.placeHolder;
    }
  }

  //-------------------------------------------------------------------

  /**
   * Inner class to capture events generated by internal and external
   * frames. Enables frames and the tabPane to keep synchronized.
   */
  protected final class WindowEventListener extends InternalFrameAdapter implements WindowListener
  {
    @Override
    public void internalFrameActivated (InternalFrameEvent event)
    {
      setActivePanelAndFireEdit (extractPanelFromEvent (event));
    }

    private void setActivePanelAndFireEdit (Panel newActivePanel)
    {
      if (undoFlagIsSet ())
        return;

      int   oldIndex       = indexOfActivePanel ();
      setActivePanel (newActivePanel);
      int   newIndex       = indexOfActivePanel ();

      fireNewActivePanelEdit (oldIndex, newIndex);
    }

    @Override
    public void internalFrameClosing (InternalFrameEvent event)
    {
       extractPanelFromEvent (event).close ();
    }

    private Panel extractPanelFromEvent (InternalFrameEvent event)
    {
      return extractPanel ((JComponent) event.getSource ());
    }

    private Panel extractPanel (JComponent component)
    {
       return (Panel) component.getClientProperty ("panel");
    }

    private Panel extractPanelFromEvent (WindowEvent event)
    {
      JFrame sourceFrame = (JFrame) event.getSource ();
      return extractPanel (sourceFrame.getRootPane ());
    }

    public void windowActivated (WindowEvent event)
    {
      setActivePanelAndFireEdit (extractPanelFromEvent (event));
    }

    public void windowClosing (WindowEvent event)
    {
      extractPanelFromEvent (event).close ();
    }

    public void windowDeactivated(WindowEvent event)
    {
      // zip
    }

    public void windowClosed (WindowEvent event)
    {
      // zip
    }

    public void windowOpened (WindowEvent event)
    {
      // zip
    }

    public void windowIconified (WindowEvent event)
    {
      // zip
    }

    public void windowDeiconified (WindowEvent event)
    {
      // zip
    }
  }
}
