package dsto.dfc.swing.panels;

import java.awt.Component;

import javax.swing.Icon;

/**
 * This interface allow JFrames and Frames to be substitutable.
 * JFrames and Frames are fundamentally incompatible, since these are
 * not related by inheritance. However, the two have many methods in
 * common.  The interface enables lazy initialization of Frames by
 * using GhostFrames to act as frame imposters until the frame is
 * actually required.
 *
 * @see OutsideFrame
 * @see InsideFrame
 * @see GhostFrame
 *
 * @version $Revision$
 */
public interface FrameLike
{
  void setTitle (String title);

  String getTitle ();

  void setIcon (Icon icon);

  Icon getIcon ();

  void setActive (boolean active);

  boolean is_Active ();

  void setVisible (boolean visible);

  boolean isVisible ();

  void setDefaultCloseOperation (int operation);

  int getDefaultCloseOperation ();

  void addTool    (Component toolOrNull);

  void removeTool (Component toolOrNull);

  Component getTool ();

  void dispose();

  void pack ();

  Component getFrame ();

}
