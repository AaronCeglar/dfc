package dsto.dfc.swing.panels;

/** A policy class for displaying titles in PanelManager.Panels.
 *  Titles are always displayed in windowFrames, and in the
 *  Tabbed Pane Tab. Since the Tabbed Pane Tab has a title,
 *  the title is not also displayed as a tool tip.
 */

public class TitleShowPolicy
{
protected static final String CHANGED   = "*";
protected static final String UNCHANGED = "";

protected String name;

/** indicate whether a window has beed edited */
protected String changeIndicator = UNCHANGED;

public void setName (String name)
{
  this.name = name;
}

public String getName ()
{
  return name;
}

public boolean isShowingName ()
{
  return true;
}

public void setChanged (boolean changed)
{
  if ( changed )
    changeIndicator = CHANGED;
  else
    changeIndicator = UNCHANGED;
}

public boolean getChanged ()
{
  return changeIndicator == CHANGED;
}

/** Append an asterisk to the title if the
 *  window has been edited.
 */
public String getWindowDisplayText ()
{
  return name + changeIndicator;
}

public String getTabDisplayText ()
{
  return getWindowDisplayText ();
}

/** No tooltip will be displayed */
public String getToolTipText ()
{
  return null;
}

}