package dsto.dfc.swing.panels;

import java.awt.Component;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import dsto.dfc.util.BasicPropertyEventSource;

/**
 * A floating tool view of a PanelManager.Panel. A tool view is an
 * auxillary view to a PanelManager.Panel. Tool views are only
 * displayed when their parent panel is active and the tool view is
 * enabled. The state of the tool view depends on the state of it's
 * PanelManager.Panel. If the Panel is FIXED or FLOAT_INTERNAL, the
 * tool will be displayed in an internal frame.  If the Panel is
 * FLOAT_EXTERNAL, the tool is shown in a native window.
 *
 * @version $Revision$
 * @see PanelManager.Panel#addToolView(Component)
 *
 */
public class ToolView extends BasicPropertyEventSource
{
  public static final int HIDE_ON_CLOSE    = JFrame.HIDE_ON_CLOSE;
  public static final int DISPOSE_ON_CLOSE = JFrame.DISPOSE_ON_CLOSE;

  protected  PanelManager.Panel panel;

  public     FrameLike outsideFrame = new GhostFrame ();
  public     FrameLike insideFrame  = new GhostFrame ();
  public     FrameLike stateFrame   = insideFrame      ;

  protected  JFrame    internalFrameOwner;
  protected  Frame     externalFrameOwner;
  public     Component tool;

  protected Corner  corner;
  protected int     indentLevel;
  public    String  state        = PanelManager.FLOAT_INTERNAL;
  private   boolean enabled      = true;

  /**
   * Creates a new <code>ToolView</code> instance.  Clients should use
   * {@link PanelManager.Panel#addToolView}.
   *
   * @param panel The panel this view is associated with (null for
   * standalone views).
   * @param tool The component to go into the tool view frame.
   * @param internalFrameOwner The owner of internal tool view frames.
   * @param externalFrameOwner The owner of external tool view frames.
   */

  public ToolView (PanelManager.Panel panel,
                   Component tool,
                   JFrame internalFrameOwner,
                   Frame externalFrameOwner)
  {
    this.panel              = panel;
    this.internalFrameOwner = internalFrameOwner;
    this.externalFrameOwner = externalFrameOwner;

    setTool      (tool);
    putInCorner  (defaultCorner ());
  }

  public void setTitle (String title)
  {
    insideFrame .setTitle (title);
    outsideFrame.setTitle (title);
  }

  public String getTitle ()
  {
    return insideFrame.getTitle ();
  }

  public void setIcon (Icon icon)
  {
    insideFrame.setIcon (icon);
  }

  public Icon getIcon ()
  {
    return insideFrame.getIcon ();
  }

  /**
   * Set the <code>ToolView</code> within a corner of it's parent
   * frame.
   *
   * @param newCorner the corner to put the tool view in.
   */
  public void putInCorner (Corner newCorner)
  {
    putInCorner (newCorner, 1);
  }

  /**
   * Sets the content of the <code>ToolView</code> Frame.
   *
   * @param newTool the component that will be shown in the frame.
   */
  public void setTool (Component newTool)
  {
    stateFrame.removeTool (tool);
    tool = newTool;
    resizeFramesToFitNewTool ();
    stateFrame.addTool (newTool);
  }

  /**
   * Toggle the visibility of the <code>ToolView</code>.
   *
   * @param visible if true the ToolView will be made visible & vice versa.
   */
  public void setVisible (boolean visible)
  {
    enabled = visible;

    if (enabled)
      showFrameForCurrentState ();
    else
      hideFrames ();

    firePropertyChange ("visible", visible);
  }

  /**
   * Sets the default behaviour invoked when the <code>ToolView</code>
   * is closed.
   *
   * @param operation the operation to invoke when the <code>ToolView</code> is closed.
   */
  public void setDefaultCloseOperation (int operation)
  {
    insideFrame .setDefaultCloseOperation (operation);
    outsideFrame.setDefaultCloseOperation (operation);
  }

  /**
   * The tool has changed, so resize both Frames to fit the new tool
   * of the <code>ToolView</code>.
   */
  protected void resizeFramesToFitNewTool ()
  {
    resizeFrameToFitNewTool (outsideFrame);
    resizeFrameToFitNewTool (insideFrame );
  }

  protected void resizeFrameToFitNewTool (FrameLike frame)
  {
    frame.addTool    (tool);
    frame.pack       (    );
    frame.removeTool (tool);
  }

  /**
   * set the <code>ToolView</code> within a corner of it's parent frame.
   * @param newCorner the corner to put the tool view in.
   * @param newIndentLevel how far to indent the toolview towards the center.
   */

  public void putInCorner (Corner newCorner, int newIndentLevel)
  {
    this.corner        = newCorner;
    this.indentLevel   = newIndentLevel;

    newCorner.placeFrame (insideFrame .getFrame (), internalFrameOwner, newIndentLevel);
    newCorner.placeFrame (outsideFrame.getFrame (), externalFrameOwner, newIndentLevel);
  }

  /**
   * Normally called when the <code>PanelManager.Panel</code> that may
   * have created the tool view changes state.
   */
  public void changeState (String newState)
  {
    this.state = newState;

    stateFrame.removeTool (tool);

    if ( newState == PanelManager.FLOAT_EXTERNAL )
      stateFrame = outsideFrame;
    else
      stateFrame = insideFrame;

    stateFrame.addTool (tool);

    if ( enabled )
      showFrameForCurrentState ();
  }

  public void dispose ()
  {
    if ( isStandAlone () )
      simpleDispose ();
    else
      letPanelDoDisposing (panel);
  }

  protected void simpleDispose ()
  {
    insideFrame .dispose ();
    outsideFrame.dispose ();

    externalFrameOwner = null;
    internalFrameOwner = null;
    tool               = null;
    panel              = null;
  }

  protected void letPanelDoDisposing (PanelManager.Panel tombstone)
  {
    panel = null;
    tombstone.removeToolView (this);
  }

  protected boolean isStandAlone ()
  {
    return panel == null;
  }

  /**
   * Normally called when the <code>PanelManager.Panel</code> that may
   * have created the tool view becomes active.  shows the frame
   * corresponding the the current state of the <code>ToolView</code>.
   */
  protected void setActive (boolean active)
  {
    if ( enabled && active != stateFrame.is_Active () )
      stateFrame.setActive (active);
  }

  // lazy initialization used to conserve window resources

  private void showFrameForCurrentState ()
  {
    boolean externalVisible = state == PanelManager.FLOAT_EXTERNAL;

    if ( externalVisible )
      stateFrame = createOutsideFrameIfNeeded ();
    else
      stateFrame = createInsideFrameIfNeeded  ();

    outsideFrame.setVisible (  externalVisible);
    insideFrame .setVisible (! externalVisible);
  }

  private void hideFrames ()
  {
    outsideFrame.setVisible (false);
    insideFrame .setVisible (false);
  }

  protected Corner defaultCorner ()
  {
    return Corner.BOTTOM_RIGHT;
  }

  // lazy initialization used to conserve window resources

  protected FrameLike createOutsideFrameIfNeeded ()
  {
    if ( frameNotYetCreated (outsideFrame) )
    {
      OutsideFrame newFrame = new OutsideFrame (outsideFrame, externalFrameOwner);
      newFrame.addWindowListener( new WindowAdapter ()
      {
        public void windowClosing (WindowEvent e)
        {
          setVisible (false);
        }
      } );
      outsideFrame = newFrame;
      putInCorner (corner, indentLevel);
  }
    return outsideFrame;
  }

  protected FrameLike createInsideFrameIfNeeded ()
  {
    if ( frameNotYetCreated (insideFrame) )
    {
      InsideFrame newFrame = new InsideFrame (insideFrame, internalFrameOwner);
      newFrame.addInternalFrameListener ( new InternalFrameAdapter ()
      {
        public void internalFrameClosing (InternalFrameEvent e)
        {
          setVisible (false);
        }
      } );
      insideFrame = newFrame;
      putInCorner (corner, indentLevel);
    }

    return insideFrame;
  }

  protected boolean frameNotYetCreated (FrameLike frame)
  {
    return frame.getClass () == GhostFrame.class;
  }

} // ToolView



