package dsto.dfc.swing.panels;

import dsto.dfc.swing.commands.AbstractMutableCommand;
import dsto.dfc.swing.commands.CommandView;

/**
 * A Command that sets the floating state of the currently
 * active PanelManager.Panel to floatStatus. The behaviour
 * of this command is parameterized by the floatStatus
 * parameter passed to the constructor.
 *
 * @author Peter J Smet
 */

public class CmdFloatPanel extends AbstractMutableCommand
{
  private PanelManager manager;
  private String       floatStatus;
  private char         mnemonic;

  public CmdFloatPanel (PanelManager manager, String floatStatus)
  {
    this.manager     = manager;
    this.floatStatus = floatStatus;

    mnemonic = floatStatus.charAt (6);
  }

  public void execute ()
  {
    PanelManager.Panel activePanel = manager.getActivePanel ();
    String oldFloatStatus = activePanel.getFloating ();

    activePanel.setFloating (floatStatus);

    FloatStateEdit edit = manager.createNewFloatStateEdit (oldFloatStatus, floatStatus);
    manager.editPropagator.tellListenersAPanelWasEdited (edit);
  }

  public String getDisplayName ()
  {
    return floatStatus;
  }

  public String getName ()
  {
    return "view.Toggle " + getDisplayName ();
  }

  public String getDescription ()
  {
    return "Toggle whether the active view is floating";
  }

  public boolean isInteractive ()
  {
    return false;
  }

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return "panel_manager";
    else if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return "View.panel_manager";
    else
      return null;
  }

  public char getMnemonic ()
  {
    return mnemonic;
  }
}
