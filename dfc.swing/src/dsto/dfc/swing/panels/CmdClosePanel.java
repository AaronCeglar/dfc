package dsto.dfc.swing.panels;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;
import javax.swing.event.UndoableEditListener;
import javax.swing.undo.UndoableEditSupport;

import dsto.dfc.swing.commands.AbstractMutableCommand;
import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.swing.icons.ImageLoader;
import dsto.dfc.swing.undo.UndoableEditSource;

/**
 * A Command that removes the currently active panel from
 * the panel manager.
 *
 * @author Peter J Smet
 */

public class CmdClosePanel extends AbstractMutableCommand
  implements PanelManagerListener, UndoableEditSource
{
  protected PanelManager        manager;
  protected UndoableEditSupport editSupport;

  private static final KeyStroke ACCELERATOR =
    KeyStroke.getKeyStroke (KeyEvent.VK_F4,
                            Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask ());

  public CmdClosePanel (PanelManager manager)
  {
    editSupport  = new UndoableEditSupport (this);
    this.manager = manager;
    manager.addPanelManagerListener (this);
    this.addUndoableEditListener (manager);
    updateEnabled ();
    icon = ImageLoader.getIcon("/dsto/dfc/icons/file_close.gif");
  }

  public void dispose ()
  {
    removeUndoableEditListener (manager);
    manager.removePanelManagerListener (this);
  }
  
  public void execute ()
  {
    manager.removePanel (manager.getActivePanel ());
  }

  protected void updateEnabled ()
  {
    boolean hasPanels = manager.getPanelCount () > 0;
    setEnabled (hasPanels);
  }

  public String getDisplayName ()
  {
    return null;
  }

  public String getName ()
  {
    return "view.Close";
  }

  public String getDescription ()
  {
    return "Close the active view";
  }

  public boolean isInteractive ()
  {
    return false;
  }

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return "panel_manager";
    else if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return "View.panel_manager";
    else if (viewName.equals (CommandView.TOOLBAR_VIEW))
      return "panel_manager";
    else
      return null;
  }

  public char getMnemonic ()
  {
    return 'c';
  }

  public KeyStroke getAccelerator ()
  {
    return ACCELERATOR;
  }

  public void panelAdded (PanelManagerEvent e)
  {
    updateEnabled ();
  }

  public void panelRemoved (PanelManagerEvent e)
  {
    updateEnabled ();
  }

  public void panelFloated(PanelManagerEvent e)
  {
    //  zip
  }

  public void panelUnfloated(PanelManagerEvent e)
  {
    //   zip
  }

  public void aboutToRemovePanel (PanelManagerEvent e)
  {
    // zip
  }

  // UndoableEditSource interface

  public void addUndoableEditListener (UndoableEditListener listener)
  {
    editSupport.addUndoableEditListener (listener);
  }

  public void removeUndoableEditListener (UndoableEditListener listener)
  {
    editSupport.removeUndoableEditListener (listener);
  }
}


