package dsto.dfc.swing.panels;

import java.util.HashMap;

import javax.swing.Icon;

import dsto.dfc.swing.icons.ImageLoader;
import dsto.dfc.swing.undo.PropertyEditBean;

/**
 * Edit for changing the active panel of the PanelManager. Checks for invalid
 * panels.
 *
 * @author    Derek Weber
 * @created   9 August 2001
 * @version   $Revision$
 */

public class ActivePanelEdit extends PropertyEditBean
{
  public static boolean PANELS_UNLOCKED = false;
  static final String description = "Records a change in the active panel";
  static final Icon EDIT_ICON = ImageLoader.getIcon ("/dsto/dfc/icons/right_arrow.gif");

  protected String oldActivePanelName;
  protected String newActivePanelName;

  /** For bean serialisation only */
  public ActivePanelEdit ()
  {
    // zip
  }

  public ActivePanelEdit (PanelManager panelManager,
                          int oldActivePanelIndex,
                          int newActivePanelIndex)
  {
    super (panelManager,
           "activeIndex",
           new Integer (oldActivePanelIndex),
           new Integer (newActivePanelIndex));

    oldActivePanelName = nameForPanelAt (oldValue);
    newActivePanelName = nameForPanelAt (newValue);
  }

  // Bean methods - only done for bean serialisation

  /** Overridden to prevent PanelManager.Panel serialisation attempt. */
  public Object getSource ()
  {
    return null;
  }

  /** Overridden to prevent PanelManager.Panel serialisation attempt. */
  public void setSource (Object newSource)
  {
    // zip
  }

  public String getOldActivePanelName ()
  {
    return oldActivePanelName;
  }

  public void setOldActivePanelName (String name)
  {
    oldActivePanelName = name;
  }

  public String getNewActivePanelName ()
  {
    return newActivePanelName;
  }

  public void setNewActivePanelName (String name)
  {
    newActivePanelName = name;
  }

  public String getDescription ()
  {
    return description;
  }

  // end Bean methods

  public String nameForPanelAt (Object index)
  {
    int primitiveIndex = extractInt (index);

    if (panelExists (index))
      return panelManager ().nameForPanelAt (primitiveIndex);

    return "null panel";
  }

  private int extractInt (Object o)
  {
    return ((Integer) o).intValue ();
  }

  private boolean panelExists (Object index)
  {
    int primitiveIndex = extractInt (index);
    return panelManager () != null &&
           panelManager ().getPanelAt (primitiveIndex) != null;
  }

  public String getNameForPanelType ()
  {
    return "Panel";
  }

  public String getPresentationName ()
  {
    return new String ("Switch " + getNameForPanelType () + " ["
                       + oldActivePanelName
                       + " -> "
                       + newActivePanelName  + "]");
  }

  public boolean panelsUnlocked ()
  {
    return PANELS_UNLOCKED;
  }

  public void undo ()
  {
    if (panelsUnlocked ())
      super.undo ();
  }

  public void redo ()
  {
    if (panelsUnlocked ())
      super.redo ();
  }

  public boolean canUndo ()
  {
    return super.canUndo () && panelIndicesAreValid ();
  }

  public boolean canRedo ()
  {
    return super.canRedo () && panelIndicesAreValid ();
  }

  private boolean panelIndicesAreValid ()
  {
    return panelExists (oldValue) && panelExists (newValue);
  }

  private PanelManager panelManager ()
  {
    return (PanelManager) source;
  }

  public Icon getIcon ()
  {
    return EDIT_ICON;
  }

  // Restorable interface

  public void restoreContext (HashMap context)
  {
    source = context.get ("panelManager");
  }

  // Serializable methods

  protected void writeSource (java.io.ObjectOutputStream out)
    throws java.io.IOException
  {
    out.writeObject (null);
  }
}