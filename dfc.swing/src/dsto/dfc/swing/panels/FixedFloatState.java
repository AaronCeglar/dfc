package dsto.dfc.swing.panels;

import java.util.ArrayList;
import java.util.Collection;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JScrollPane;

import dsto.dfc.swing.commands.Command;

/** The FixedFloatState represents a single pane in a tabPane.
 *  It controls the commands relevant to that state.
 */

public class FixedFloatState
{
  Icon               icon;
  JScrollPane        placeHolder;
  PanelManager.Panel panel;
  RestrictedIndexTabbedPane tabPane;
  Collection         commands = new ArrayList ();

  Command            unFloatCommand;
  Command            internalFloatCommand;
  Command            externalFloatCommand;
  Command            closeCommand;

  public FixedFloatState (PanelManager.Panel panel,
                          RestrictedIndexTabbedPane tabPane,
                          Icon icon, int index)
  {
    this.icon     = icon;
    this.panel    = panel;
    this.tabPane  = tabPane;
    placeHolder   = makePlaceHolderForPanel ();
    setUpDisplay (index);

    unFloatCommand       = new CmdFloatPanel (panel.getManager(), "Unfloat"       );
    internalFloatCommand = new CmdFloatPanel (panel.getManager(), "Float Internal");
    externalFloatCommand = new CmdFloatPanel (panel.getManager(), "Float External");
    closeCommand         = new CmdClosePanel (panel.getManager()                  );

    setUpCommands ();
  }

  public void dispose ()
  {
    if (closeCommand instanceof CmdClosePanel)
      ((CmdClosePanel)closeCommand).dispose ();
  }
  
  // Some panels should not be able to be closed by the user.

  public void makeUncloseable ()
  {
    replaceCloseCommand (unFloatCommand);
  }

  public void replaceCloseCommand (Command unpublishedCloseCommand)
  {
    commands.remove (closeCommand);
    
    if (closeCommand instanceof CmdClosePanel)
      ((CmdClosePanel)closeCommand).dispose ();

    closeCommand = unpublishedCloseCommand;
  }

  public void switchCloseCommand (Command publishedCloseCommand)
  {
    replaceCloseCommand (publishedCloseCommand);
    commands.add (publishedCloseCommand);
  }

  public void close ()
  {
    closeCommand.execute ();
  }

  public Icon getIcon ()
  {
    return icon;
  }

  public Icon getTabDisplayIcon ()
  {
    return getIcon ();
  }

  public void setIcon (Icon icon)
  {
    this.icon = icon;
    showIcon ();
  }

  public void showIcon ()
  {
    // zip
  }

  public void setActive  (boolean active)
  {
    tabPane.setSelectedTabIfLegal (panel.getIndex());
  }

  public void setVisible (boolean visible)
  {
    if (visible)
      placeHolder.getViewport ().setView (panel.getComponent ());
  }

  public String getFloating ()
  {
    return PanelManager.FIXED;
  }

  /** without the placeHolder, the tabPanel will disappear when it does not
   *  contain a component.
   */

  protected JScrollPane makePlaceHolderForPanel ()
  {
    JScrollPane scroller = new JScrollPane ();
    scroller.getViewport ().setBackground (panel.getComponent ().getBackground ());
    scroller.getViewport ().setOpaque (true);

    if (! panel.isScrollable())
    {
      scroller.setBorder (BorderFactory.createEmptyBorder (1, 1, 1, 1));
      scroller.setHorizontalScrollBarPolicy (JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
      scroller.setVerticalScrollBarPolicy   (JScrollPane.VERTICAL_SCROLLBAR_NEVER  );
    }

    return scroller;
  }

  protected void refreshTitle (TitleShowPolicy titleDisplay)
  {
    if (panel.getIndex () == -1)
      return;

    tabPane.setTitleAt       (panel.getIndex (), titleDisplay.getTabDisplayText ());
    tabPane.setToolTipTextAt (panel.getIndex (), titleDisplay.getToolTipText ()   );
  }

  protected void setUpDisplay (int index)
  {
    panel.getManager ().setUndoFlag ();

    try
    {
      tabPane.insertTab (panel.titleDisplay.getTabDisplayText (),
                         icon, placeHolder,
                         panel.titleDisplay.getToolTipText (), index);
    }
    finally
    {
      panel.getManager ().unsetUndoFlag ();
    }
  }

  public void setUpCommands ()
  {
    commands.add ( internalFloatCommand );
    commands.add ( externalFloatCommand );
    commands.add ( closeCommand         );
  }

  public Collection getCommands ()
  {
    return commands;
  }

  public Command getDefaultCommand ()
  {
    return internalFloatCommand;
  }



}