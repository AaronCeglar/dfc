package dsto.dfc.swing.panels;

import java.awt.Point;

/**
 * TopRight positions an inner frame in the top
 * right corner of a larger outer frame. The indentation level
 * nudges the inner frame by degrees towards the
 * center of the outer frame.
 *
 * @version $Revision$
 * @author Peter J Smet
 */

public class TopRight extends TopLeft
{
  protected Point moveToCorner (Point topLeft)
  {
    return makeTopRight (topLeft);
  }

  protected int horizontalInset (int indentLevel)
  {
    return - super.horizontalInset (indentLevel);
  }

  protected Point makeTopRight (Point topLeft)
  {
    topLeft.translate (widthDifference, 0);
    return topLeft;
  }

  }