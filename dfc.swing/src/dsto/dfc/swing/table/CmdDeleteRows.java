package dsto.dfc.swing.table;

import dsto.dfc.swing.commands.BasicDeleteCommand;
import dsto.dfc.swing.event.SelectionEvent;

/**
 * Delete selected rows.
 *
 * @version $Revision$
 */
public class CmdDeleteRows extends BasicDeleteCommand
{
  protected DfcTable table;

  public CmdDeleteRows (DfcTable table)
  {
    super (table);

    this.table = table;

    updateEnabled ();
  }

  public void execute ()
  {
    /** @todo dispose deleted rows? */
    if (table.getModel () instanceof DfcTableModel)
    {
      DfcTableModel model = (DfcTableModel)table.getModel ();
      int [] selectedRows = table.getSelectedRows ();
      table.clearSelection ();

      int minRow = Integer.MAX_VALUE;

      for (int i = selectedRows.length - 1; i >= 0 ; i--)
      {
        if (model.canDeleteRows (selectedRows [i], selectedRows [i]))
          model.deleteRows (selectedRows [i], selectedRows [i]);

        minRow = Math.min (minRow, selectedRows [i]);
      }

      // select first row before deleted set
      if (model.getRowCount () > 0 && minRow != Integer.MAX_VALUE)
      {
        minRow--;

        table.getSelectionModel ().setSelectionInterval (minRow, minRow);
      }
    }
  }

  public String getName ()
  {
    return "table.Delete Rows";
  }

  public String getDescription ()
  {
    return "Delete selected rows from table";
  }

  protected void updateEnabled ()
  {
    DfcTableModel model = table.getDfcTableModel ();
    int selectedRow = table.getSelectedRow ();

    setEnabled (model != null &&
                selectedRow >= 0 &&
                selectedRow < model.getRowCount () &&
                model.canDeleteRows (selectedRow, selectedRow));
  }

  // SelectionListener interface

  public void selectionChanged (SelectionEvent e)
  {
    updateEnabled ();
  }
}
