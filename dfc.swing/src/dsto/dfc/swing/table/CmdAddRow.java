package dsto.dfc.swing.table;

import dsto.dfc.swing.commands.BasicAddEntryCommand;

/**
 * Add a new row to the end of the table.
 *
 * @version $Revision$
 */
public class CmdAddRow extends BasicAddEntryCommand
{
  protected DfcTable table;

  public CmdAddRow (DfcTable table)
  {
    this.table = table;
  }

  public void execute ()
  {
    if (table.getModel () instanceof DfcTableModel)
    {
      DfcTableModel model = (DfcTableModel)table.getModel ();
      int row = table.getRowCount ();

      if (model.canInsertRows (row, 1))
        model.insertRows (row, 1);

      table.clearSelection ();
      table.getSelectionModel ().addSelectionInterval (row, row);
      table.scrollToBottom ();
    }
  }

  public String getName ()
  {
    return "table.Add Row";
  }

  public String getDescription ()
  {
    return "Add a new row to the end of the table";
  }

  public char getMnemonic ()
  {
    return 'r';
  }
}
