package dsto.dfc.swing.table;

import dsto.dfc.swing.commands.AbstractMoveCommand;

/**
 * Moves rows up/down the table.
 *
 * @version $Revision$
 */
public class CmdMoveRow extends AbstractMoveCommand
{
  private DfcTable table;

  /**
   * Create a new instance.
   *
   * @param table The table to operate on.
   * @param direction The direction to move the entries (MOVE_UP or
   * MOVE_DOWN).
   */
  public CmdMoveRow (DfcTable table, boolean direction)
  {
    super (table, direction);

    this.table = table;

    updateEnabled ();
  }

  public void execute ()
  {
    int selectedRow = table.getSelectedRow ();

    if (selectedRow != -1 &&
        table.getModel () instanceof DfcTableModel)
    {
      DfcTableModel model = (DfcTableModel)table.getModel ();
      int newRow = getNextRow (model, selectedRow);

      model.moveRows (selectedRow, selectedRow, newRow);

      // select moved row
      table.getSelectionModel ().setSelectionInterval (newRow, newRow);
    }
  }

  protected void updateEnabled ()
  {
    boolean enabled = false;
    int selectedRow = table.getSelectedRow ();

    if (selectedRow != -1 &&
        table.getModel () instanceof DfcTableModel)
    {
      DfcTableModel model = (DfcTableModel)table.getModel ();
      int newRow = getNextRow (model, selectedRow);

      enabled = newRow != -1 &&
                model.canMoveRows (selectedRow, selectedRow, newRow);
    }

    setEnabled (enabled);
  }

  protected int getNextRow (DfcTableModel model, int row)
  {
    int newRow = -1;

    if (direction == MOVE_UP)
    {
      if (row > 0)
        newRow = row - 1;
    } else
    {
      if (row < model.getRowCount () - 1)
        newRow = row + 1;
    }

    return newRow;
  }
}
