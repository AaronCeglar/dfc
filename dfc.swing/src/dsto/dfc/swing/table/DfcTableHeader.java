package dsto.dfc.swing.table;

import java.awt.Event;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;

import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 * Extended JTable header component used by {@link DfcTable}.  Uses
 * {@link ButtonHeaderRenderer} as the default renderer, enables
 * sorting of tables using {@link SortedTableModel} by handling clicks
 * on column headers and support auto sizing when the user
 * double-clicks the resize area of a column header.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class DfcTableHeader extends JTableHeader
{
  protected JTable dfcTable;
  protected int pressedColumn = -1;

  public DfcTableHeader (JTable table)
  {
    super (table.getColumnModel ());

    this.dfcTable = table;
  }

  /**
   * The currently pressed column index (in model space) or -1 if no
   * header is currently pressed.  Can be used by header renderers to
   * make column behave like a button.
   */
  public int getPressedColumn ()
  {
    return pressedColumn;
  }

  /**
   * Test if a column may be resized.
   */
  protected boolean canResize (TableColumn column)
  {
    return (column != null) && getResizingAllowed () && column.getResizable ();
  }

  /**
   * See {@link #getResizingColumn(Point,int)}.
   */
  protected TableColumn getResizingColumn (Point p)
  {
    return getResizingColumn (p, getColumnModel ().getColumnIndexAtX (p.x));
  }

  /**
   * Get the column (if any) that a given point is in the resize zone
   * of.  This logic is ripped from {@link
   * javax.swing.plaf.basic.BasicTableHeaderUI}'s MouseInputHandler
   * class.
   *
   * @param p The point to test.
   * @param column The column to test.
   * @return The column instance of p lies in the resize zone, null if
   * not.
   */
  protected TableColumn getResizingColumn (Point p, int column)
  {
    if (column == -1)
      return null;

    Rectangle r = getHeaderRect (column);
    r.grow (-3, 0);

    if (r.contains(p))
      return null;

    int midPoint = r.x + r.width / 2;
    int columnIndex = (p.x < midPoint) ? column - 1 : column;

    if (columnIndex == -1)
      return null;

    return getColumnModel ().getColumn (columnIndex);
  }

  /**
   * Override to use {@link ButtonHeaderRenderer} as the header renderer.
   */
  protected TableCellRenderer createDefaultRenderer ()
  {
    return new ButtonHeaderRenderer ();
  }

  /**
   * Handles mouse clicks in header cells to change sort order and
   * auto column sizing when double-clicking in header resize zone.
   */
  protected void processMouseEvent (MouseEvent e)
  {
    super.processMouseEvent (e);

    // get column at point
    TableColumnModel currentColumnModel = dfcTable.getColumnModel ();
    int viewColumn = currentColumnModel.getColumnIndexAtX (e.getX ());
    int column = dfcTable.convertColumnIndexToModel (viewColumn);

    if (SwingUtilities.isLeftMouseButton (e))
    {
      // get the column that the mouse is in resize zone
      TableColumn mouseResizingColumn = getResizingColumn (e.getPoint ());

      if (e.getID () == MouseEvent.MOUSE_PRESSED)
      {
        // set pressed column index on mouse down outside of resize zone
        if (mouseResizingColumn == null)
        {
          pressedColumn = column;
          repaint ();
        }
      } else if (e.getID () == MouseEvent.MOUSE_RELEASED)
      {
        // clear pressed column on mouse up
        if (pressedColumn != -1)
        {
          pressedColumn = -1;
          repaint ();
        }
      } else if (e.getID () == MouseEvent.MOUSE_CLICKED && column != -1)
      {
        // if in header resize zone...
        if (mouseResizingColumn != null)
        {
          // if double-click in resize zone of a resizable column...
          if (canResize (mouseResizingColumn) && e.getClickCount () == 2)
          {
            // auto size column
            int resizingColumnIndex =
              currentColumnModel.getColumnIndex (mouseResizingColumn.getIdentifier ());
            int width = Tables.getBestColumnSize (dfcTable, mouseResizingColumn);

            // bizarre logic to get table to size column (fighting table's
            // auto resize logic).
            mouseResizingColumn.setMinWidth (0);
            mouseResizingColumn.setWidth (0);
            dfcTable.sizeColumnsToFit (resizingColumnIndex);
            mouseResizingColumn.setWidth (width);
            mouseResizingColumn.setPreferredWidth (width);
            dfcTable.sizeColumnsToFit (resizingColumnIndex);
          }
        } else if (dfcTable.getModel () instanceof SortedTableModel)
        {
          // toggle sort mode for column
          SortedTableModel sortedModel = (SortedTableModel)dfcTable.getModel ();
          SortedTableModel.SortingColumn sortingColumn =
            sortedModel.findSortingColumn (column);

          // if neither Ctrl or Shift is down, clear the current sort order
          if ((e.getModifiers () & (Event.SHIFT_MASK | Event.CTRL_MASK)) == 0)
            sortedModel.clearSorting ();

          // cycle between sort ascending, sort descending and not sorted
          if (sortingColumn == null)
            sortedModel.addSortingColumn (column, true);
          else if (sortingColumn.isAscending ())
            sortedModel.addSortingColumn (column, false);
          else
            sortedModel.removeSortingColumn (column);

          sortedModel.sort ();
          repaint ();
        }
      }
    }
  }
}
