package dsto.dfc.swing.table;

import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.event.TableModelEvent;

import dsto.dfc.util.Debug;

/**
 * Presents a DfcTableModel in sorted order.
 *
 * @todo Currently the C&P methods only support the case where a
 * single row is being copied - this should be extended at some point.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class SortedTableModel extends ProxyTableModel
{
  /** Maps rows from this table's row space to the enclosed table's
      row space.*/
  private int [] rowMap;
  /** An array of SortingColumn's in sort order. */
  private ArrayList sortingColumns = new ArrayList ();

  public SortedTableModel (DfcTableModel model)
  {
    super (model); // "I'm a model, you'll know what I mean when I shake my..."

    rowMap = new int [model.getRowCount ()];
    initRowMap ();
  }

  /**
   * Add a column to the end of the list of columns to sort by (ie last
   * in the sort order).  If the column is already in the sort order, it is
   * replaced and moved to the end of the order.<p>
   *
   * Note: this method, and all other methods that change the sort order, do
   * not automatically sort the table - see {@link #sort}.
   *
   * @param columnIndex The column to sort by.
   * @param ascending True if sorting should be by ascending value,
   * false for descending value.
   *
   * @see #removeSortingColumn
   */
  public SortingColumn addSortingColumn (int columnIndex,
                                         boolean ascending)
  {
    SortingColumn column = findSortingColumn (columnIndex);

    if (column != null)
      sortingColumns.remove (column);

    column = new SortingColumn (columnIndex, ascending);

    sortingColumns.add (column);

    return column;
  }

  /**
   * Remove a column from the sort order.
   *
   * @see #addSortingColumn
   * @see #clearSorting
   */
  public void removeSortingColumn (int columnIndex)
  {
    SortingColumn column = findSortingColumn (columnIndex);

    if (column != null)
      sortingColumns.remove (column);
  }

  /**
   * Remove all columns from the sort order.
   *
   * @see #addSortingColumn
   * @see #removeSortingColumn
   */
  public void clearSorting ()
  {
    sortingColumns.clear ();
  }

  /**
   * Find a sorting column on <code>sortingColumns</code> with a
   * matching column index.
   */
  public SortingColumn findSortingColumn (int columnIndex)
  {
    for (int i = 0; i < sortingColumns.size (); i++)
    {
      SortingColumn c = (SortingColumn)sortingColumns.get (i);

      if (c.getColumn () == columnIndex)
        return c;
    }

    return null;
  }

  /**
   * Map a row from this table's row space to the enclosed table's row
   * space.
   */
  public final int mapRow (int row)
  {
    return rowMap [row];
  }

  /**
   * Initialise row map with row indexes in unsorted (natural) order.
   */
  protected void initRowMap ()
  {
    int rowCount = model.getRowCount ();

    for (int row = 0; row < rowCount; row++)
      rowMap [row] = row;
  }

  /**
   * Check whether rowMap needs to be resized based on current table size.
   * Note that this may zap the sorted rows, so be sure sort () gets called
   * sometime after this.
   */
  protected void checkUpdateRowMapSize ()
  {
    int newSize = model.getRowCount ();

    if (newSize > rowMap.length)
    {
      // need to expand => expand by new size + 50% or 10, whichever is
      // larger.
      rowMap = new int [newSize + Math.max (newSize / 2, 10)];

      initRowMap ();
    } else if (rowMap.length > 50 && newSize < rowMap.length / 2)
    {
      // row map is larger than 50 and new size is less than half of that =>
      // size downwards.
      rowMap = new int [newSize];

      initRowMap ();
    }
  }

  /**
   * Simple check that the enclosed model has not changed without
   * informing us.
   */
  protected void checkModel ()
  {
    Debug.assertTrue (rowMap.length >= model.getRowCount (),
                      "Sorted table out of sync with contained table", this);
  }

  /**
   * (Re)Sort the model.
   */
  public void sort ()
  {
    checkModel ();

    if (sortingColumns.isEmpty ())
      initRowMap ();
    else
      shuttlesort (rowMap.clone (), rowMap, 0, model.getRowCount ());

    fireTableDataChanged ();
  }

  /**
   * (mpp) NOTE: this method is shamelessly ripped from Sun's
   * TableSorter demo class - I did make it look prettier though.
   * This is a home-grown implementation which we have not had time to
   * research - it may perform poorly in some circumstances. It
   * requires twice the space of an in-place algorithm and makes NlogN
   * assigments shuttling the values between the two arrays. The
   * number of compares appears to vary between N-1 and NlogN
   * depending on the initial order but the main reason for using it
   * here is that, unlike qsort, it is stable. */
  protected void shuttlesort (int from [], int to [], int low, int high)
  {
    if (high - low < 2)
      return;

    int middle = (low + high) / 2;
    shuttlesort (to, from, low, middle);
    shuttlesort (to, from, middle, high);

    int p = low;
    int q = middle;

    /* This is an optional short-cut; at each recursive call, check to
       see if the elements in this subset are already ordered.  If so,
       no further comparisons are needed; the sub-array can just be
       copied.  The array must be copied rather than assigned
       otherwise sister calls in the recursion might get out of sinc.
       When the number of elements is three they are partitioned so
       that the first set, [low, mid), has one element and and the
       second, [mid, high), has two. We skip the optimisation when the
       number of elements is three or less as the first compare in the
       normal merge will produce the same sequence of steps. This
       optimisation seems to be worthwhile for partially ordered lists
       but some analysis is needed to find out how the performance
       drops to Nlog(N) as the initial order diminishes - it may drop
       very quickly.  */

    if (high - low >= 4 && compare (from [middle - 1], from [middle]) <= 0)
    {
      for (int i = low; i < high; i++)
        to [i] = from [i];

      return;
    }

    // A normal merge.

    for (int i = low; i < high; i++)
    {
      if (q >= high || (p < middle && compare (from [p], from [q]) <= 0))
        to [i] = from [p++];
      else
        to [i] = from [q++];
    }
  }

  /**
   * Compare two rows in the table using the current column sort
   * order.
   */
  protected int compare (int row1, int row2)
  {
    for (int level = 0; level < sortingColumns.size (); level++)
    {
      SortingColumn column = (SortingColumn)sortingColumns.get (level);
      int result = compareRowsByColumn (row1, row2, column.getColumn ());

      if (result != 0)
        return column.isAscending () ? result : -result;
    }

    return 0;
  }

  /**
   * Compare two rows by their values in a given column.
   */
  protected int compareRowsByColumn (int row1, int row2, int column)
  {
    // Check for nulls

    Object o1 = model.getValueAt (row1, column);
    Object o2 = model.getValueAt (row2, column);

    // If both values are null return 0
    if (o1 == null && o2 == null)
      return 0;
    else if (o1 == null) // Define null less than everything.
      return -1;
    else if (o2 == null)
      return 1;
    else if (o1 instanceof Comparable)
      return ((Comparable)o1).compareTo (o2);
    else
    {
      // do string comparison
      String s1 = o1.toString ();
      String s2 = o2.toString ();
      int result = s1.compareTo (s2);

      if (result < 0)
        return -1;
      else if (result > 0)
        return 1;
      else
        return 0;
    }
  }

  // TableModelListener interface

  /**
   * Called when the enclosing table changes.
   */
  public void tableChanged (TableModelEvent e)
  {
    checkUpdateRowMapSize ();

    // if not a cell update, need to sort from scratch
    if (e.getType () != TableModelEvent.UPDATE)
      initRowMap ();

    if (sortingColumns.isEmpty ())
    {
      // if no sorting, just re-fire event
      super.tableChanged (e);
    } else
    {
      sort ();
    }
  }

  // DfcTableModel implementation

  public Object getValueAt (int row, int col)
  {
    return model.getValueAt (mapRow (row), col);
  }

  /**
   * NOTE: only handles case where startRow == endRow.
   */
  public boolean canMoveRows (int startRow, int endRow, int newStartRow)
  {
    if (startRow == endRow)
    {
      int mappedRow = mapRow (startRow);

      return model.canMoveRows (mappedRow, mappedRow, newStartRow);
    } else
    {
      return false;
    }
  }

  /**
   * NOTE: only handles case where rowStart == rowEnd.
   */
  public void deleteRows (int rowStart, int rowEnd)
    throws UnsupportedOperationException
  {
    if (rowStart == rowEnd)
    {
      int mappedRow = mapRow (rowStart);

      model.deleteRows (mappedRow, mappedRow);
    } else
    {
      throw new UnsupportedOperationException ();
    }
  }

  public boolean canCopyRows (int startRow, int endRow)
  {
    if (startRow == endRow)
    {
      int mappedRow = mapRow (startRow);

      return model.canCopyRows (mappedRow, mappedRow);
    } else
    {
      return false;
    }
  }

  public boolean canDeleteCells (int startRow, int endRow,
                                 int startColumn, int endColumn)
  {
    if (startRow == endRow)
    {
      int mappedRow = mapRow (startRow);

      return model.canDeleteCells (mappedRow, mappedRow,
                                   startColumn, endColumn);
    } else
    {
      return false;
    }
  }

  public void pasteCells (Transferable transferable,
                          int startRow, int endRow,
                          int startColumn, int endColumn)
    throws IOException, CloneNotSupportedException,
           UnsupportedFlavorException, UnsupportedOperationException
  {
    int mappedRow = mapRow (startRow);

    model.pasteCells (transferable, mappedRow, mappedRow,
                      startColumn, endColumn);
  }

  public boolean canDeleteRows (int rowStart, int rowEnd)
  {
    if (rowStart == rowEnd)
    {
      int mappedRow = mapRow (rowStart);

      return model.canDeleteRows (mappedRow, mappedRow);
    } else
    {
      return false;
    }
  }

  public Transferable copyRows (int startRow, int endRow)
    throws CloneNotSupportedException, UnsupportedOperationException
  {
    int mappedRow = mapRow (startRow);

    return model.copyRows (mappedRow, mappedRow);
  }

  public Transferable copyCells (int startRow, int endRow,
                                 int startColumn, int endColumn)
    throws CloneNotSupportedException, UnsupportedOperationException
  {
    int mappedRow = mapRow (startRow);

    return model.copyCells (mappedRow, mappedRow, startColumn, endColumn);
  }

  public boolean moveRows (int startRow, int endRow, int newStartRow)
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException ();
  }

  public boolean canPasteCells (Transferable transferable,
                                int startRow, int endRow,
                                int startColumn, int endColumn)
  {
    if (startRow == endRow)
    {
      int mappedRow = mapRow (startRow);

      return model.canPasteCells (transferable, mappedRow, mappedRow,
                                  startColumn, endColumn);
    } else
    {
      return false;
    }
  }

  public void setValueAt (Object aValue, int rowIndex, int columnIndex)
  {
    model.setValueAt (aValue, mapRow (rowIndex), columnIndex);
  }

  public void pasteRows (Transferable transferable,
                         int rowStart)
    throws IOException, CloneNotSupportedException, UnsupportedFlavorException,
           UnsupportedOperationException
  {
    model.pasteRows (transferable, mapRow (rowStart));
  }

  public boolean canCopyCells (int startRow, int endRow,
                               int startColumn, int endColumn)
  {
    if (startRow == endRow)
    {
      int mappedRow = mapRow (startRow);

      return model.canCopyCells (mappedRow, mappedRow,
                                 startColumn, endColumn);
    } else
    {
      return false;
    }
  }

  public void insertRows (int rowStart, int rowEnd)
  {
    int mappedRow = mapRow (rowStart);

    super.insertRows (mappedRow,  mappedRow);
  }

  public void deleteCells (int startRow, int endRow,
                           int startColumn, int endColumn)
    throws UnsupportedOperationException
  {
    int mappedRow = mapRow (startRow);

    model.deleteCells (mappedRow, mappedRow, startColumn, endColumn);
  }

  /**
   * Stores information about column to be sorted by.
   */
  public static final class SortingColumn
  {
    private int column;
    private boolean ascending;

    protected SortingColumn (int column, boolean ascending)
    {
      this.column = column;
      this.ascending = ascending;
    }

    public int getColumn ()
    {
      return column;
    }

    public boolean isAscending ()
    {
      return ascending;
    }
  }
}
