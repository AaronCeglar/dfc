package dsto.dfc.swing.table;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import javax.swing.table.TableModel;

/**
 * Datatransfer transferable implementation for TableModel's.
 *
 * @version $Revision$
 */
public final class TableModelTransferable
  implements Transferable, ClipboardOwner
{
  public static final DataFlavor TABLE_MODEL_FLAVOR =
    new DataFlavor (TableModel.class, "TableModel");

  private static final DataFlavor [] FLAVORS =
    new DataFlavor [] {TABLE_MODEL_FLAVOR,
                       DataFlavor.stringFlavor};

  TableModel tableModel;

  public TableModelTransferable (TableModel tableModel)
  {
    this.tableModel = tableModel;
  }

  public DataFlavor [] getTransferDataFlavors ()
  {
    return FLAVORS;
  }

  public boolean isDataFlavorSupported (DataFlavor flavor)
  {
    for (int i = 0; i < FLAVORS.length; i++)
    {
      if (FLAVORS [i].equals (flavor))
        return true;
    }

    return false;
  }

  public Object getTransferData (DataFlavor flavor)
    throws UnsupportedFlavorException, IOException
  {
    if (flavor.equals (TABLE_MODEL_FLAVOR))
    {
      return tableModel;
    } else if (flavor.equals (DataFlavor.stringFlavor))
    {
      return "Table Model";
    } else
    {
      throw new UnsupportedFlavorException (flavor);
    }
  }

  public void lostOwnership (Clipboard clipboard, Transferable contents)
  {
    tableModel = null;
  }
}
