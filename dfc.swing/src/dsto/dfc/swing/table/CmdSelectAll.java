package dsto.dfc.swing.table;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.JTable;
import javax.swing.KeyStroke;

import dsto.dfc.swing.commands.AbstractCommand;
import dsto.dfc.swing.commands.CommandView;

/**
 * The "Select All" command.
 *
 * @version $Revision$
 */
public class CmdSelectAll extends AbstractCommand
{
  private static final KeyStroke ACCELERATOR =
    KeyStroke.getKeyStroke (KeyEvent.VK_A,
                            Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask ());

  protected JTable table;

  public CmdSelectAll (JTable table)
  {
    this.table = table;
  }

  public void execute ()
  {
    // JTable.selectAll () throws IOB when there are no rows in the table
    if (table.getRowCount () > 0)
      table.selectAll ();
  }

  public String getName ()
  {
    return "table.Select All";
  }

  public String getDescription ()
  {
    return "Select all rows and columns";
  }

  public boolean isInteractive ()
  {
    return false;
  }

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return "select";
    else
      return null;
  }

  public char getMnemonic ()
  {
    return 'a';
  }

  public KeyStroke getAccelerator ()
  {
    return ACCELERATOR;
  }
}
