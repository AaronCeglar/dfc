package dsto.dfc.swing.table;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import dsto.dfc.swing.commands.AbstractCommand;
import dsto.dfc.swing.commands.CommandView;

/**
 * Insert a new row into the table.
 *
 * @version $Revision$
 */
public class CmdInsertRow extends AbstractCommand
{
  private static final KeyStroke ACCELERATOR =
    KeyStroke.getKeyStroke (KeyEvent.VK_I,
                            Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask ());

  protected DfcTable table;

  public CmdInsertRow (DfcTable table)
  {
    super ("insert_rows.gif");
    this.table = table;
  }

  public void execute ()
  {
    int selectedRow = table.getSelectedRow ();

    if (selectedRow == -1)
      selectedRow = table.getRowCount ();

    if (table.getModel () instanceof DfcTableModel)
    {
      DfcTableModel model = (DfcTableModel)table.getModel ();

      if (model.canInsertRows (selectedRow, 1))
        model.insertRows (selectedRow, 1);

      table.clearSelection ();
      table.getSelectionModel ().addSelectionInterval (selectedRow, selectedRow);
      table.scrollToRow (selectedRow);
    }
  }

  public String getName ()
  {
    return "table.Insert Row";
  }

  public String getDescription ()
  {
    return "Insert a new row at the current position";
  }

  public boolean isInteractive ()
  {
    return false;
  }

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return "edit";
    else
      return null;
  }

  public char getMnemonic ()
  {
    return 'i';
  }

  public KeyStroke getAccelerator ()
  {
    return ACCELERATOR;
  }
}
