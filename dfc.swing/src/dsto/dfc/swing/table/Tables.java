package dsto.dfc.swing.table;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

/**
 * General utility methods for JFC tables.
 *
 * @author Matthew Phillips
 * @version $Revision$
 */

public final class Tables
{
  private Tables ()
  {
    // zip
  }

  /**
   * Get the best size for a TableColumn.
   *
   * @param table The table containing the column.
   * @param column The column.
   */
  public static int getBestColumnHeaderSize (JTable table,
                                             TableColumn column)
  {
    int columnIndex =
      table.getColumnModel ().getColumnIndex (column.getIdentifier ());
    TableCellRenderer headerRenderer = column.getHeaderRenderer ();

    if (headerRenderer == null)
      headerRenderer = table.getTableHeader ().getDefaultRenderer ();

    Component component  = headerRenderer.getTableCellRendererComponent
        (table, column.getHeaderValue (), false, false, -1, columnIndex);

    return component.getPreferredSize ().width + (table.getRowMargin () * 2);
  }

  /**
   * Get the minimum width of a column required to display all items,
   * including the header.
   */
  public static int getBestColumnSize (JTable table, TableColumn column)
  {
    /** @todo size only based on rows in view (use rowAtPoint())  */
    TableModel model = table.getModel ();
    int columnIndex = column.getModelIndex ();
    int width = getBestColumnHeaderSize (table, column);

    for (int row = 0; row < model.getRowCount (); row++)
    {
      Object value = model.getValueAt (row, columnIndex);
      TableCellRenderer renderer = table.getCellRenderer (row, columnIndex);
      Component rendererComponent =
        renderer.getTableCellRendererComponent (table, value, false,
                                                false, row, columnIndex);
      width = Math.max (width, rendererComponent.getPreferredSize ().width);
    }

    // add cell margin
    width += table.getRowMargin () * 2;

    return width;
  }
}
