package dsto.dfc.swing.table;

import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.table.TableModel;

import dsto.dfc.util.Copyable;
import dsto.dfc.util.Objects;

/**
 * Fully dynamic implementation of DfcTableModel backed by
 * ArrayList's.
 *
 * @version $Revision$
 */
public class DefaultDfcTableModel
  extends AbstractDfcTableModel
  implements DfcTableModel, Copyable, Serializable
{
  private ArrayList rows = new ArrayList ();
  private ArrayList columnNames = new ArrayList ();
  private ArrayList columnClasses = new ArrayList ();
  private boolean allowInsertDeleteRows = true;
  private boolean allowInsertDeleteColumns = false;

  public DefaultDfcTableModel ()
  {
    // zip
  }

  /**
   * Create a new table model from a rectangular section of an
   * existing table.
   */
  public DefaultDfcTableModel (TableModel model,
                               int startRow, int endRow,
                               int startColumn, int endColumn)
    throws CloneNotSupportedException
  {
    for (int i = startColumn; i <= endColumn; i++)
    {
      columnNames.add (model.getColumnName (i));
      columnClasses.add (model.getColumnClass (i));
    }

    for (int i = startRow; i <= endRow; i++)
    {
      ArrayList newRow = new ArrayList (endColumn - startColumn);
      rows.add (newRow);

      for (int j = startColumn; j <= endColumn; j++)
        newRow.add (Objects.cloneObject (model.getValueAt (i, j)));
    }
  }

  // TableModel interface

  public int getRowCount ()
  {
    return rows.size ();
  }

  public int getColumnCount ()
  {
    return columnNames.size ();
  }

  public String getColumnName (int columnIndex)
  {
    return (String)columnNames.get (columnIndex);
  }

  public Class getColumnClass (int columnIndex)
  {
    return (Class)columnClasses.get (columnIndex);
  }

  public boolean isCellEditable (int rowIndex, int columnIndex)
  {
    return true;
  }

  public Object getValueAt (int rowIndex, int columnIndex)
  {
    ArrayList columns = (ArrayList)rows.get (rowIndex);

    Object value = columns.get (columnIndex);

    return value;
  }

  public void setValueAt (Object value, int rowIndex, int columnIndex)
  {
    ArrayList columns = (ArrayList)rows.get (rowIndex);

    columns.set (columnIndex, value);

    fireTableCellUpdated (rowIndex, columnIndex);
  }

  // DfcTableModel interface

  public void insertRows (int startRow, int rowCount)
  {
    for (int i = 0; i < rowCount; i++)
    {
      ArrayList newRow = new ArrayList (getColumnCount ());

      for (int j = 0; j < getColumnCount (); j++)
        newRow.add (null);

      rows.add (startRow + i, newRow);
    }

    fireTableRowsInserted (startRow, startRow + rowCount - 1);
  }

  public void deleteRows (int startRow, int endRow)
  {
    for (int i = startRow; i <= endRow; i++)
      rows.remove (startRow);

    fireTableRowsDeleted (startRow, endRow);
  }

  public boolean canInsertRows (int startRow, int rowCount)
  {
    return allowInsertDeleteRows;
  }

  public boolean canDeleteRows (int startRow, int endRow)
  {
    return allowInsertDeleteRows;
  }

  public boolean canMoveRows (int startRow, int endRow, int newStartRow)
  {
    return allowInsertDeleteRows;
  }

  public boolean moveRows (int startRow, int endRow, int newStartRow)
    throws UnsupportedOperationException
  {
    int rowCount = endRow - startRow + 1;
    ArrayList movedRows = new ArrayList (rowCount);

    for (int i = startRow; i <= endRow; i++)
      movedRows.add (rows.remove (startRow));

    fireTableRowsDeleted (startRow, endRow);

    for (int i = 0; i < rowCount; i++)
      rows.add (newStartRow + i, movedRows.get (i));

    fireTableRowsInserted (newStartRow, newStartRow+ rowCount - 1);

    return true;
  }

  // insert & delete columns

  public void insertColumn (int columnIndex,
                            String columnName, Class columnClass)
  {
    columnNames.add (columnIndex, columnName);
    columnClasses.add (columnIndex, columnClass);

    // insert values
    for (int i = 0; i < rows.size (); i++)
    {
      ArrayList row = (ArrayList)rows.get (i);

      row.add (columnIndex, null);
    }

    fireTableStructureChanged ();
  }

  public void deleteColumns (int startColumn, int endColumn)
  {
    for (int c = startColumn; c <= endColumn; c++)
    {
      columnNames.remove (c);
      columnClasses.remove (c);
    }

    // delete column values
    for (int i = 0; i < rows.size (); i++)
    {
      ArrayList row = (ArrayList)rows.get (i);

      for (int c = startColumn; c <= endColumn; c++)
        row.remove (c);
    }
  }

  public boolean canInsertColumn (int columnIndex,
                                  String columnName, Class columnClass)
  {
    return allowInsertDeleteColumns;
  }

  public boolean canDeleteColumns (int startColumn, int endColumn)
  {
    return allowInsertDeleteColumns;
  }

  public boolean canDeleteCells (int startRow, int endRow,
                                 int startColumn, int endColumn)
  {
    return true;
  }

  public void deleteCells (int startRow, int endRow,
                           int startColumn, int endColumn)
  {
    for (int i = startRow; i <= endRow; i++)
    {
      ArrayList row = (ArrayList)rows.get (i);

      for (int j = startColumn; j <= endColumn; j++)
        row.set (j, null);
    }

    fireTableRowsUpdated (startRow, endRow);
  }

  // copy & paste

  public boolean canCopyRows (int startRow, int endRow)
  {
    return true;
  }

  public Transferable copyRows (int startRow, int endRow)
    throws UnsupportedOperationException, CloneNotSupportedException
  {
    return copyCells (startRow, endRow, 0, getColumnCount () - 1);
  }

  public boolean canCopyCells (int startRow, int endRow,
                               int startColumn, int endColumn)
  {
    return true;
  }

  public Transferable copyCells (int startRow, int endRow,
                                 int startColumn, int endColumn)
    throws CloneNotSupportedException
  {
    DefaultDfcTableModel table =
      new DefaultDfcTableModel (this, startRow, endRow,
                                      startColumn, endColumn);

    return new TableModelTransferable (table);
  }

  public boolean canPasteRows (Transferable transferable, int startRow)
  {
    TableModel model;

    // try to extract table model from transferable

    try
    {
      model =
        (TableModel)transferable.getTransferData
          (TableModelTransferable.TABLE_MODEL_FLAVOR);
    } catch (UnsupportedFlavorException ex)
    {
      return false;
    } catch (IOException ex)
    {
      // data not available: be optimistic and assume it will be OK
      return true;
    }

    // check table has same column names and classes
    
    if (model.getColumnCount() != getColumnCount ())
      return false;

    for (int i = 0; i < getColumnCount (); i++)
    {
      if (!getColumnName (i).equals (model.getColumnName (i)))
        return false;

      if (!getColumnClass (i).equals (model.getColumnClass (i)))
        return false;
    }

    return true;
  }

  public void pasteRows (Transferable transferable, int startRow)
    throws UnsupportedOperationException,
           UnsupportedFlavorException, CloneNotSupportedException,
           IOException
  {
    TableModel model =
      (TableModel)transferable.getTransferData
        (TableModelTransferable.TABLE_MODEL_FLAVOR);

    for (int i = 0; i < model.getRowCount (); i++)
    {
      ArrayList newRow = new ArrayList (getColumnCount ());
      rows.add (i + startRow, newRow);

      for (int j = 0; j < getColumnCount (); j++)
        newRow.add (Objects.cloneObject (model.getValueAt (i, j)));
    }

    fireTableRowsInserted (startRow, startRow + model.getRowCount () - 1);
  }

  public Object clone () throws CloneNotSupportedException
  {
    DefaultDfcTableModel newObject = (DefaultDfcTableModel)super.clone ();

    newObject.rows = (ArrayList)rows.clone ();
    newObject.columnNames = (ArrayList)columnNames.clone ();
    newObject.columnClasses = (ArrayList)columnClasses.clone ();

    // deep copy rows
    for (int r = 0; r < rows.size (); r++)
    {
      ArrayList newRow = (ArrayList)((ArrayList)rows.get (r)).clone ();

      // clone contents
      for (int c = 0; c < getColumnCount (); c++)
      {
        Object value = newRow.get (c);

        newRow.set (c, Objects.cloneObject (value));
      }

      newObject.rows.set (r, newRow);
    }

    return newObject;
  }
}
