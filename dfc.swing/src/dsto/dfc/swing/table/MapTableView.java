package dsto.dfc.swing.table;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import dsto.dfc.swing.commands.BasicAddEntryCommand;
import dsto.dfc.swing.commands.Command;
import dsto.dfc.swing.commands.CommandToolBars;
import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.swing.controls.DfcOkCancelDialog;
import dsto.dfc.swing.forms.BasicFormEditorEventSource;
import dsto.dfc.swing.forms.FormEditor;
import dsto.dfc.swing.forms.FormEditorListener;
import dsto.dfc.util.IllegalFormatException;

/**
 * Edits a java.util.Map in a table view.  Supports the
 * {@link dsto.dfc.swing.forms.FormEditor} interface.
 *
 * @see MapTableModel
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class MapTableView extends DfcTable implements FormEditor
{
  protected boolean editable = false;
  protected Command addEntryCommand;
  protected Command deleteEntryCommand;
  protected CommandView toolbarCommandView;
  protected BasicFormEditorEventSource formListeners =
    new BasicFormEditorEventSource (this);
  protected Listener listener = new Listener ();

  public MapTableView ()
  {
    this (new MapTableModel ());
  }

  public MapTableView (Map map)
  {
    this (new MapTableModel (map));
  }

  public MapTableView (MapTableModel model)
  {
    super (model);

    this.addEntryCommand = new CmdAddProperty ();
    this.deleteEntryCommand = new CmdDeleteProperty ();

    CommandView toolbarView = getCommandView (CommandView.TOOLBAR_VIEW);

    // show full name of "add property" command on toolbar
    CommandToolBars.showName (toolbarView, addEntryCommand);

    setEditable (true);
  }

  public boolean isEditable ()
  {
    return editable;
  }

  /**
   * Set whether the view is editable.  Note this does not set whether the
   * model edit mode (see {@link #setEditMode}).
   */
  public void setEditable (boolean newValue)
  {
    boolean oldValue = editable;

    if (oldValue != newValue)
    {
      editable = newValue;

      updateEditCommands ();

      firePropertyChange ("editable", oldValue, newValue);
    }
  }

  /**
   * Update the command views when the editable property is changed.
   */
  protected void updateEditCommands ()
  {
    CommandView toolbarView = getCommandView (CommandView.TOOLBAR_VIEW);
    CommandView contextView = getCommandView (CommandView.CONTEXT_MENU_VIEW);

    if (isEditable ())
    {
      toolbarView.addCommand (addEntryCommand);
      toolbarView.addCommand (deleteEntryCommand);
      contextView.addCommand (addEntryCommand);
      contextView.addCommand (deleteEntryCommand);
    } else
    {
      toolbarView.removeCommand (addEntryCommand);
      toolbarView.removeCommand (deleteEntryCommand);
      contextView.removeCommand (addEntryCommand);
      contextView.removeCommand (deleteEntryCommand);
    }
  }

  /**
   * Set the edit mode for the underlying table model.  This is a shortcut to
   * {@link MapTableModel#setEditMode}.
   */
  public void setEditMode (int mode)
  {
    ((MapTableModel)getModel ()).setEditMode (mode);
  }

  public int getEditMode ()
  {
    return ((MapTableModel)getModel ()).getEditMode ();
  }

  /**
   * Adds a toolbar command view if none provided by DfcTable.
   */
  public CommandView getCommandView (String viewName)
  {
    CommandView view = super.getCommandView (viewName);

    if (viewName.equals (CommandView.TOOLBAR_VIEW))
    {
      if (toolbarCommandView == null)
      {
        if (view == null)
          toolbarCommandView = new CommandView (CommandView.TOOLBAR_VIEW);
        else
          toolbarCommandView = view;
      }

      return toolbarCommandView;
    } else
    {
      return view;
    }
  }

  // FormEditor interface

  public boolean isDirectEdit ()
  {
    return ((MapTableModel)getModel ()).getEditMode () == MapTableModel.EDIT_DIRECT;
  }

  public Class getPreferredValueType ()
  {
    return Map.class;
  }

  public void setEditorValue (Object value) throws IllegalFormatException
  {
    try
    {
      getModel ().removeTableModelListener (listener);

      ((MapTableModel)getModel ()).setMap ((Map)value);

      getModel ().addTableModelListener (listener);
    } catch (ClassCastException ex)
    {
      throw new IllegalFormatException (this, value, Map.class);
    }
  }

  public Object getEditorValue ()
  {
    return ((MapTableModel)getModel ()).getMap ();
  }

  public Component getEditorComponent ()
  {
    return this;
  }

  public String getEditorDescription ()
  {
    return null;
  }

  public void commitEdits () throws IllegalFormatException
  {
    if (isEditing ())
      stopEditing ();
  }

  public void addFormEditorListener (FormEditorListener l)
  {
    formListeners.addFormEditorListener (l);
  }

  public void removeFormEditorListener (FormEditorListener l)
  {
    formListeners.removeFormEditorListener (l);
  }

  /**
   * The "Add Property" command.
   */
  protected class CmdAddProperty extends BasicAddEntryCommand
  {
    public void execute ()
    {
      MapTableModel model = (MapTableModel)getModel ();

      JPanel keyValuePanel = new JPanel();
      JLabel propertyLabel = new JLabel();
      JTextField propertyField = new JTextField();
      JLabel valueLabel = new JLabel();
      JTextField valueField = new JTextField();
      GridBagLayout gridBagLayout1 = new GridBagLayout();

      propertyLabel.setDisplayedMnemonic('P');
      propertyLabel.setLabelFor(propertyField);
      propertyLabel.setText("Property:");
      keyValuePanel.setLayout(gridBagLayout1);
      propertyField.setColumns(10);
      valueLabel.setDisplayedMnemonic('V');
      valueLabel.setLabelFor(valueField);
      valueLabel.setText("Value:");
      valueField.setColumns(15);
      keyValuePanel.add(propertyLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
              ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 62), 0, 0));
      keyValuePanel.add(propertyField, new GridBagConstraints(0, 1, 1, 1, 0.0, 1.0
              ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(3, 0, 0, 0), 0, 0));
      keyValuePanel.add(valueLabel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0
              ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 12, 0, 0), 0, 0));
      keyValuePanel.add(valueField, new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0
              ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(3, 12, 0, 0), 0, 0));

      DfcOkCancelDialog dialog =
        new DfcOkCancelDialog (MapTableView.this, "Add Property", true);

      dialog.setDialogPanel (keyValuePanel);
      dialog.pack ();
      dialog.setVisible (true);

      if (!dialog.isCancelled ())
      {
        String key = propertyField.getText ();
        String value = valueField.getText ();

        model.putEntry (key, value);

        int index = model.findKeyIndex (key);

        clearSelection ();
        getSelectionModel ().setSelectionInterval (index, index);
        scrollToRow (index);
      }
    }

    public String getName ()
    {
      return "table.Add Property";
    }

    public boolean isInteractive ()
    {
      return true;
    }

    public String getDescription ()
    {
      return "Add a new property value";
    }

    public char getMnemonic ()
    {
      return 'p';
    }
  }

  /**
   * The "Delete Property" command.
   */
  protected class CmdDeleteProperty extends CmdDeleteRows
  {
    public CmdDeleteProperty ()
    {
      super (MapTableView.this);
    }

    public String getName ()
    {
      return "table.Delete Property";
    }

    public String getDescription ()
    {
      return "Delete the selected properties";
    }
  }

  /**
   * Listens for changes to the table model and fires edit committed events.
   */
  class Listener implements TableModelListener
  {
    public void tableChanged (TableModelEvent e)
    {
      formListeners.fireEditCommitted ();
    }
  }
}