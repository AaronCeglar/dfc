package dsto.dfc.swing.table;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import dsto.dfc.util.StringComparator;

/**
 * Present a java.util.Map as a Swing TableModel with two columns ("Property"
 * and "Value").  Also supports editing map: see {@link #setEditMode}.
 *
 * @see MapTableView
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class MapTableModel extends AbstractDfcTableModel
{
  public static final int EDIT_NONE = 0;
  public static final int EDIT_DIRECT = 1;
  public static final int EDIT_COPY = 2;

  /** The map being presented by this model. */
  protected Map map;
  /** A list of Map.Entry's in the row order displayed by the table. */
  protected ArrayList orderedEntries;
  /** The comparator used to sort orderedEntries by the keys of its Map.Entry's. */
  protected Comparator keyComparator;
  /** The edit setting: EDIT_NONE, EDIT_DIRECT or EDIT_COPY. */
  protected int editMode;

  public MapTableModel ()
  {
    this (Collections.EMPTY_MAP);
  }

  /**
   * Create a table model from map.  Model is sorted by string value of keys.
   */
  public MapTableModel (Map map)
  {
    this (map, StringComparator.getInstance ());
  }

  public MapTableModel (Map map, Comparator keyComparator)
  {
    this (map, keyComparator, EDIT_NONE);
  }

  public MapTableModel (Map map, int editMode)
  {
    this (map, StringComparator.getInstance (), editMode);
  }

  /**
   * Create a table model from map.  Model is sorted using keyComparator
   * on keys.
   */
  public MapTableModel (Map map, Comparator keyComparator, int editMode)
  {
    this.map = map;
    this.keyComparator = keyComparator;
    this.orderedEntries = new ArrayList (map.size ());
    this.editMode = editMode;

    loadOrderedEntries ();
  }

  /**
   * (Re)load orderedEntries from the map.
   */
  protected void loadOrderedEntries ()
  {
    orderedEntries.clear ();

    for (Iterator i = map.entrySet ().iterator (); i.hasNext (); )
    {
      orderedEntries.add (i.next ());
    }

    Collections.sort (orderedEntries, new MapEntryComparator ());
  }

  public void setMap (Map newMap)
  {
    if (orderedEntries.size () > 0)
      fireTableRowsDeleted (0, orderedEntries.size () - 1);

    map = newMap;

    loadOrderedEntries ();

    if (orderedEntries.size () > 0)
      fireTableRowsInserted (0, orderedEntries.size () - 1);
  }

  public Map getMap ()
  {
    return map;
  }

  /**
   * Set the edit mode for the table.
   *
   * @param mode The new mode: EDIT_NONE (no editing allowed), EDIT_DIRECT
   * (edit the map directly), or EDIT_COPY (edit the map by copying it first
   * using copyMap ()).
   */
  public void setEditMode (int mode)
  {
    editMode = mode;
  }

  public int getEditMode ()
  {
    return editMode;
  }

  /**
   * Put a new entry into the map, firing the appropriate events.
   *
   * @exception UnsupportedOperationException if the model is not editable.
   */
  public void putEntry (Object key, Object value)
    throws UnsupportedOperationException
  {
    checkEditable ();

    Object oldValue = map.get (key);

    if (oldValue == null || !value.equals (oldValue))
    {
      maybeCopyMap ();

      map.put (key, value);

      loadOrderedEntries ();
      int index = findKeyIndex (key);

      if (oldValue == null)
        fireTableRowsInserted (index, index);
      else
        fireTableCellUpdated (index, 1);
    }
  }

  /**
   * Find the index (row) that a key has in the table.
   */
  public int findKeyIndex (Object key)
  {
    for (int i = 0; i < orderedEntries.size (); i++)
    {
      Map.Entry entry = (Map.Entry)orderedEntries.get (i);

      if (entry.getKey ().equals (key))
        return i;
    }

    return -1;
  }

  // TableModel interface

  public int getColumnCount ()
  {
    return 2;
  }

  public String getColumnName (int column)
  {
    return column == 0 ? "Property" : "Value";
  }

  public Class getColumnClass (int columnIndex)
  {
    return Object.class;
  }

  public Object getValueAt (int row, int col)
  {
    Map.Entry entry = (Map.Entry)orderedEntries.get (row);

    return col == 0 ? entry.getKey () : entry.getValue ();
  }

  public int getRowCount ()
  {
    return map.size ();
  }

  public boolean isCellEditable (int row, int column)
  {
    return editMode != EDIT_NONE && column == 1;
  }

  public void setValueAt (Object value, int row, int column)
  {
    if (column == 1)
    {
      checkEditable ();

      Object key = getValueAt (row, 0);

      maybeCopyMap ();
      map.put (key, value);

      loadOrderedEntries ();

      fireTableCellUpdated (row, 1);
    } else
    {
      throw new UnsupportedOperationException ("Cannot edit key column");
    }
  }

  public boolean canDeleteRows (int rowStart, int rowEnd)
  {
    return editMode != EDIT_NONE;
  }

  public void deleteRows (int rowStart, int rowEnd)
    throws UnsupportedOperationException
  {
    checkEditable ();

    maybeCopyMap ();

    for (int row = rowEnd; row >= rowStart; row--)
    {
      Object key = getValueAt (row, 0);

      map.remove (key);
      orderedEntries.remove (row);
    }

    fireTableRowsDeleted (rowStart, rowEnd);
  }

  /**
   * Throws UnsupportedOperationException if the table is not editable.
   */
  protected void checkEditable () throws UnsupportedOperationException
  {
    if (editMode == EDIT_NONE)
      throw new UnsupportedOperationException ();
  }

  /**
   * Copy map if edit mode is EDIT_COPY by calling copyMap ().
   */
  protected void maybeCopyMap ()
  {
    if (editMode == EDIT_COPY)
      map = copyMap (map);
  }

  /**
   * Make a copy of the current map.  The default is to play safe and create
   * a new HashMap copy.  Subclasses may override this if different behaviour
   * is needed.
   */
  protected Map copyMap (Map srcMap)
  {
    return new HashMap (map);
  }

  /**
   * Compare Map.Entry's by using keyComparator on the key.
   */
  protected final class MapEntryComparator implements Comparator
  {
    public int compare (Object o1, Object o2)
    {
      Map.Entry e1 = (Map.Entry)o1;
      Map.Entry e2 = (Map.Entry)o2;

      return keyComparator.compare (e1.getKey (), e2.getKey ());
    }
  }
}