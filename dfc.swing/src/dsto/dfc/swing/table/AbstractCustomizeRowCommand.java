package dsto.dfc.swing.table;

import dsto.dfc.swing.commands.BasicPropertiesCommand;
import dsto.dfc.swing.event.SelectionEvent;
import dsto.dfc.swing.event.SelectionListener;
import dsto.dfc.util.Disposable;

/**
 * Abstract base class for commands that customize the selected row of
 * a table.
 *
 * @version $Revision$
 */
public abstract class AbstractCustomizeRowCommand
  extends BasicPropertiesCommand implements SelectionListener, Disposable
{
  protected DfcTable table;

  public AbstractCustomizeRowCommand (DfcTable table)
  {
    this.table = table;

    table.addSelectionListener (this);

    updateEnabled ();
  }

  public void dispose ()
  {
    if (table != null)
    {
      table.removeSelectionListener (this);
      table = null;
    }
  }

  public abstract void execute ();

  protected void updateEnabled ()
  {
    int selectedRow = table.getSelectedRow ();

    setEnabled (selectedRow != -1);
  }

  // SelectionListener interface

  public void selectionChanged (SelectionEvent e)
  {
    updateEnabled ();
  }
}
