package dsto.dfc.swing.table;

import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import javax.swing.table.AbstractTableModel;

/**
 * Base class for implementations of DfcTableModel.  Provides default
 * empty implementations of DfcTableModel methods.
 *
 * @version $Revision$
 */
public abstract class AbstractDfcTableModel
  extends AbstractTableModel implements DfcTableModel
{
  // DfcTableModel interface

  public void insertRows (int rowStart, int rowCount)
  {
    // zip
  }

  public void deleteRows (int rowStart, int rowEnd)
    throws UnsupportedOperationException
  {
    // zip
  }

  public boolean canInsertRows (int rowStart, int rowCount)
  {
    return false;
  }

  public boolean canDeleteRows (int rowStart, int rowEnd)
  {
    return false;
  }

  public boolean canMoveRows (int startRow, int endRow, int newStartRow)
  {
    return false;
  }

  public boolean moveRows (int startRow, int endRow, int newStartRow)
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException ("Move rows not supported");
  }

  // insert & delete columns

  public void insertColumn (int columnIndex,
                            String columnName, Class columnClass)
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException ();
  }

  public void deleteColumns (int columnStart, int columnEnd)
    throws UnsupportedOperationException
  {
    // zip
  }

  public boolean canInsertColumn (int columnIndex,
                                  String columnName, Class columnClass)
  {
    return false;
  }

  public boolean canDeleteColumns (int columnStart, int columnEnd)
  {
    return false;
  }

  public boolean canDeleteCells (int startRow, int endRow,
                                 int startColumn, int endColumn)
  {
    return false;
  }

  public void deleteCells (int startRow, int endRow,
                           int startColumn, int endColumn)
    throws UnsupportedOperationException
  {
    throw new UnsupportedOperationException ();
  }

  // copy & paste

  public boolean canCopyRows (int startRow, int endRow)
  {
    return false;
  }

  public Transferable copyRows (int startRow, int endRow)
    throws UnsupportedOperationException, CloneNotSupportedException
  {
    throw new UnsupportedOperationException ();
  }

  public boolean canPasteRows (Transferable transferable, int rowStart)
  {
    return false;
  }

  public void pasteRows (Transferable transferable, int rowStart)
    throws UnsupportedOperationException,
           UnsupportedFlavorException, CloneNotSupportedException,
           IOException
  {
    throw new UnsupportedOperationException ();
  }

  public boolean canCopyCells (int startRow, int endRow,
                               int startColumn, int endColumn)
  {
    return false;
  }

  public Transferable copyCells (int startRow, int endRow,
                                 int startColumn, int endColumn)
    throws UnsupportedOperationException, CloneNotSupportedException
  {
    throw new UnsupportedOperationException ();
  }

  public boolean canPasteCells (Transferable transferable,
                                int startRow, int endRow,
                                int startColumn, int endColumn)
  {
    return false;
  }

  public void pasteCells (Transferable transferable,
                          int startRow, int endRow,
                          int startColumn, int endColumn)
    throws UnsupportedOperationException,
           UnsupportedFlavorException, CloneNotSupportedException,
           IOException
  {
    throw new UnsupportedOperationException ();
  }
}
