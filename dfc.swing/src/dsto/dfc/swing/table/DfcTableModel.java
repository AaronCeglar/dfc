package dsto.dfc.swing.table;

import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import javax.swing.table.TableModel;

/**
 * Extension of JFC TableModel to support structural modification to
 * rows/columns and copy/paste of data.
 *
 * @version $Revision$
 */
public interface DfcTableModel extends TableModel
{
  // insert & delete rows

  /**
   * Test if an insertRows () operation is supported.
   *
   * @param startRow The row to begin inserting.
   * @param rowCount The number of rows to insert.
   * @return True if the insert is possible.
   */
  public boolean canInsertRows (int startRow, int rowCount);

  /**
   * Insert empty rows.
   *
   * @param startRow The row to begin inserting.
   * @param rowCount The number of rows to insert.
   * @exception UnsupportedOperationException if this is not supported.
   * @see #canInsertRows
   */
  public void insertRows (int startRow, int rowCount)
    throws UnsupportedOperationException;

  /**
   * Test if a deleteRows () operation is supported.
   *
   * @param startRow The row to begin inserting.
   * @param endRow The last row to delete.
   * @return True if the delete is possible.
   */
  public boolean canDeleteRows (int startRow, int endRow);

  /**
   * Delete rows.
   *
   * @param startRow The row to begin inserting.
   * @param endRow The last row to delete.
   * @exception UnsupportedOperationException if this is not supported.
   * @see #canDeleteRows
   */
  public void deleteRows (int startRow, int endRow)
    throws UnsupportedOperationException;

  public boolean canMoveRows (int startRow, int endRow, int newStartRow);

  public boolean moveRows (int startRow, int endRow, int newStartRow)
    throws UnsupportedOperationException;

  // insert & delete columns

  /**
   * Test if an insertColumn () operation is supported.
   *
   * @param columnIndex The index for the new column.
   * @param columnName The name of the new column.  May be null to indicate
   * automatic name.
   * @param columnClass The class of values in the column.
   * @return True if the insert is possible.
   */
  public boolean canInsertColumn (int columnIndex,
                                  String columnName, Class columnClass);

  /**
   * Test if a deleteColumns () operation is supported.
   *
   * @param startColumn The index of the first column.
   * @param endColumn The index of the last column.
   * @return True if the delete is possible.
   */
  public boolean canDeleteColumns (int startColumn, int endColumn);

  /**
   * Insert a new column containing blank values.
   *
   * @param columnIndex The index for the new column.
   * @param columnName The name of the new column.  May be null to
   * indicate automatic name.
   * @param columnClass The class of values in the column.
   * @exeption UnsupportedOperationException if this is not supported.
   * @see #canInsertColumn
   */
  public void insertColumn (int columnIndex,
                            String columnName, Class columnClass)
    throws UnsupportedOperationException;

  /**
   * Delete columns.
   *
   * @param startColumn The index of the first column.
   * @param endColumn The index of the last column.
   * @exeption UnsupportedOperationException if this is not supported.
   * @see #canDeleteColumns
   */
  public void deleteColumns (int startColumn, int endColumn)
    throws UnsupportedOperationException;

  /**
   * Test if a deleteCells () operation is permitted.
   *
   * @param startRow The first row to delete.
   * @param endRow The last row to delete.
   * @param startColumn The first column to delete.
   * @param endColumn The last column to delete.
   * @return True if the operation is permitted.
   */
  public boolean canDeleteCells (int startRow, int endRow,
                                 int startColumn, int endColumn);

  /**
   * Delete a rectangular block of cells.  The deleted cells should be
   * reset to their default values.
   *
   * @param startRow The first row to delete.
   * @param endRow The last row to delete.
   * @param startColumn The first column to delete.
   * @param endColumn The last column to delete.
   * @exeption UnsupportedOperationException if this is not supported.
   * @see #canDeleteCells
   */
  public void deleteCells (int startRow, int endRow,
                           int startColumn, int endColumn)
    throws UnsupportedOperationException;

  // copy & paste

  public boolean canCopyRows (int startRow, int endRow);

  public Transferable copyRows (int startRow, int endRow)
    throws UnsupportedOperationException, CloneNotSupportedException;

  /**
   * Should return true if rows may be pasted from a given
   * Transferable at the given row.  Note that the data in the
   * transferable may not be accessible at this time (eg during a drap
   * and drop operation): calling getTransferData () will result in an
   * IOException.
   *
   * @param transferable The transferable.
   * @param startRow The row to insert the transferable at.
   * @return True if the transferable can be pasted at startRow.
   */
  public boolean canPasteRows (Transferable transferable, int startRow);

  public void pasteRows (Transferable transferable, int startRow)
    throws UnsupportedOperationException,
           UnsupportedFlavorException, CloneNotSupportedException,
           IOException;

  /**
   * Test if a copyCells () operation is permitted.
   *
   * @param startRow The first row to copy.
   * @param endRow The last row to copy.
   * @param startColumn The first column to copy.
   * @param endColumn The last column to copy.
   * @return True if the operation is permitted.
   */
  public boolean canCopyCells (int startRow, int endRow,
                               int startColumn, int endColumn);

  /**
   * Copy a rectangular block of cells into an AWT Transferable.  The
   * flavors supported by the transferable may include
   * TableModelTransferable.TABLE_MODEL_FLAVOR but this is not
   * required.
   *
   * @param startRow The first row to copy.
   * @param endRow The last row to copy.
   * @param startColumn The first column to copy.
   * @param endColumn The last column to copy.
   * @exeption UnsupportedOperationException if this is not supported.
   * @exception CloneNotSupportedException if a data item could not be cloned.
   * @see #canCopyRows
   */
  public Transferable copyCells (int startRow, int endRow,
                                 int startColumn, int endColumn)
    throws UnsupportedOperationException, CloneNotSupportedException;

  /**
   * Should return true if rows may be pasted from a given
   * Transferable into a block of cells.  Note that the data in the
   * transferable may not be accessible at this time (eg during a drap
   * and drop operation): calling getTransferData () will result in an
   * IOException in this case.
   *
   * @param transferable The transferable.
   * @param startRow The row to insert the transferable at.
   * @return True if the transferable can be pasted at startRow.
   */
  public boolean canPasteCells (Transferable transferable,
                                int startRow, int endRow,
                                int startColumn, int endColumn);

  public void pasteCells (Transferable transferable,
                          int startRow, int endRow,
                          int startColumn, int endColumn)
    throws UnsupportedOperationException,
           UnsupportedFlavorException, CloneNotSupportedException,
           IOException;
}

