package dsto.dfc.swing.table;

import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

/**
 * Generic base class for a table model that act as a proxy for
 * another table model.  All calls are by default forwarded to the
 * enclosed table model, events are forwarded out (but with the source
 * changed to the proxy).
 *
 * @author Matthew Phillips
 * @version $Revision$
 */
public class ProxyTableModel
  extends AbstractDfcTableModel implements DfcTableModel, TableModelListener
{
  protected DfcTableModel model;

  public ProxyTableModel (DfcTableModel model)
  {
    this.model = model;

    model.addTableModelListener (this);
  }

  public DfcTableModel getSubModel ()
  {
    return model;
  }

  public int getColumnCount ()
  {
    return model.getColumnCount ();
  }

  public Object getValueAt (int row, int col)
  {
    return model.getValueAt (row, col);
  }

  public int getRowCount ()
  {
    return model.getRowCount ();
  }

  public String getColumnName (int column)
  {
    return model.getColumnName (column);
  }

  public boolean canMoveRows (int startRow, int endRow, int newStartRow)
  {
    return model.canMoveRows (startRow, endRow, newStartRow);
  }

  public boolean canPasteRows (Transferable transferable, int rowStart)
  {
    return model.canPasteRows (transferable, rowStart);
  }

  public void deleteRows (int rowStart, int rowEnd)
    throws UnsupportedOperationException
  {
    model.deleteRows (rowStart, rowEnd);
  }

  public void deleteColumns (int columnStart, int columnEnd)
    throws UnsupportedOperationException
  {
    model.deleteColumns (columnStart, columnEnd);
  }

  public boolean canCopyRows (int startRow, int endRow)
  {
    return model.canCopyRows (startRow, endRow);
  }

  public boolean canInsertRows (int rowStart, int rowEnd)
  {
    return model.canInsertRows (rowStart, rowEnd);
  }

  public boolean isCellEditable (int rowIndex, int columnIndex)
  {
    return model.isCellEditable (rowIndex, columnIndex);
  }

  public boolean canDeleteCells (int startRow, int endRow,
                                 int startColumn, int endColumn)
  {
    return canDeleteCells (startRow, endRow, startColumn, endColumn);
  }

  public void pasteCells (Transferable transferable,
                          int startRow, int endRow,
                          int startColumn, int endColumn)
    throws IOException, CloneNotSupportedException,
           UnsupportedFlavorException, UnsupportedOperationException
  {
    model.pasteCells (transferable, startRow,
                      endRow, startColumn, endColumn);
  }

  public boolean canDeleteRows (int rowStart, int rowEnd)
  {
    return model.canDeleteRows (rowStart,  rowEnd);
  }

  public Transferable copyRows (int startRow, int endRow)
    throws CloneNotSupportedException, UnsupportedOperationException
  {
    return model.copyRows (startRow, endRow);
  }

  public Transferable copyCells (int startRow, int endRow,
                                 int startColumn, int endColumn)
    throws CloneNotSupportedException, UnsupportedOperationException
  {
    return model.copyCells (startRow, endRow, startColumn, endColumn);
  }

  public boolean canInsertColumn (int columnIndex, String columnName,
                                  Class columnClass)
  {
    return model.canInsertColumn (columnIndex, columnName, columnClass);
  }

  public boolean moveRows (int startRow, int endRow, int newStartRow)
    throws UnsupportedOperationException
  {
    return model.moveRows (startRow, endRow, newStartRow);
  }

  public boolean canDeleteColumns (int columnStart, int columnEnd)
  {
    return model.canDeleteColumns (columnStart, columnEnd);
  }

  public boolean canPasteCells (Transferable transferable,
                                int startRow, int endRow,
                                int startColumn, int endColumn)
  {
    return model.canPasteCells (transferable, startRow,
                                endRow, startColumn,  endColumn);
  }

  public void setValueAt (Object aValue, int rowIndex, int columnIndex)
  {
    model.setValueAt (aValue,  rowIndex,  columnIndex);
  }

  public void pasteRows (Transferable transferable, int rowStart)
    throws IOException, CloneNotSupportedException,
           UnsupportedFlavorException, UnsupportedOperationException
  {
    model.pasteRows (transferable, rowStart);
  }

  public boolean canCopyCells (int startRow, int endRow,
                               int startColumn, int endColumn)
  {
    return model.canCopyCells (startRow, endRow, startColumn, endColumn);
  }

  public void insertRows (int rowStart, int rowEnd)
  {
    model.insertRows (rowStart, rowEnd);
  }

  public void deleteCells (int startRow, int endRow,
                           int startColumn, int endColumn)
    throws UnsupportedOperationException
  {
    model.deleteCells (startRow,  endRow,  startColumn,  endColumn);
  }

  public void insertColumn (int columnIndex, String columnName,
                            Class columnClass)
    throws UnsupportedOperationException
  {
    model.insertColumn (columnIndex,  columnName,  columnClass);
  }

  // TableModelListener interface

  public void tableChanged (TableModelEvent e)
  {
    TableModelEvent newEvent =
      new TableModelEvent (this, e.getFirstRow (), e.getLastRow (),
                           e.getColumn (), e.getType ());

    fireTableChanged (newEvent);
  }
}
