package dsto.dfc.swing.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Rectangle;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import dsto.dfc.swing.icons.ImageLoader;

/**
 * Custom cell renderer designed to be used as a header cell renderer
 * in a JTable.  Supports display of column sorting for tables using
 * {@link SortedTableModel}.
 *
 * @author Matthew Phillips
 * @version $Revision$
 *
 * @see javax.swing.table.JTableHeader
 */
public class ButtonHeaderRenderer extends JButton implements TableCellRenderer
{
  public static final Icon UP_ARROW_ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/arrow_up_grey.png");
  public static final Icon DOWN_ARROW_ICON =
    ImageLoader.getIcon ("/dsto/dfc/icons/arrow_down_grey.png");

  public ButtonHeaderRenderer ()
  {
    // make button look like a header
    setBorder (BorderFactory.createCompoundBorder
      (UIManager.getBorder ("TableHeader.cellBorder"),
       BorderFactory.createEmptyBorder (0, 2, 0, 2)));

    // another way to do it
    // setBorder (BorderFactory.createCompoundBorder
    //   (new ThinBevelBorder (ThinBevelBorder.RAISED),
    //    BorderFactory.createEmptyBorder (0, 2, 0, 2)));

    setHorizontalTextPosition (LEADING);
    setHorizontalAlignment (LEFT);
    setOpaque (true);
  }

  public Component getTableCellRendererComponent (JTable table, Object value,
                                                  boolean isSelected,
                                                  boolean hasFocus,
                                                  int row, int column)
  {
    TableColumn tableColumn = table.getColumnModel ().getColumn (column);

    setForeground (table.getTableHeader ().getForeground ());
    setBackground (table.getTableHeader ().getBackground ());

    // support DfcTableHeader's pressedColumn property
    if (table.getTableHeader () instanceof DfcTableHeader)
    {
      int pressedColumn =
        ((DfcTableHeader)table.getTableHeader ()).getPressedColumn ();

      if (tableColumn.getModelIndex () == pressedColumn)
      {
        setBackground (Color.darkGray);
        setForeground (Color.white);
      }
    }

    setIcon (null);

    if (table.getModel () instanceof SortedTableModel)
    {
      // show icon for sorted columns
      SortedTableModel tableModel = (SortedTableModel)table.getModel ();
      SortedTableModel.SortingColumn sortingColumn =
        tableModel.findSortingColumn (tableColumn.getModelIndex ());

      if (sortingColumn != null)
        setIcon (sortingColumn.isAscending () ? UP_ARROW_ICON : DOWN_ARROW_ICON);
    }

    setFont (table.getFont ());
    setText (value.toString ());

    return this;
  }

  /*
   * The following methods are overridden as a performance measure to
   * to prune code-paths are often called in the case of renders but
   * which we know are unnecessary.  Great care should be taken when
   * writing your own renderer to weigh the benefits and drawbacks of
   * overriding methods like these.
   */

  /**
   * Overridden for performance reasons.
   */
  public void validate()
  {
    // zip
  }

  /**
   * Overridden for performance reasons.
   */
  public void revalidate ()
  {
    // zip
  }

  /**
   * Overridden for performance reasons.
   */
  public void repaint(long tm, int x, int y, int width, int height)
  {
    // zip
  }

  /**
   * Overridden for performance reasons.
   */
  public void repaint(Rectangle r)
  {
    // zip
  }

  /**
   * Overridden for performance reasons.
   */
  protected void firePropertyChange (String propertyName, Object oldValue, Object newValue)
  {
    // Strings get interned...
    if (propertyName=="text") {
      super.firePropertyChange(propertyName, oldValue, newValue);
    }
  }

  /**
   * Overridden for performance reasons.
   */
  public void firePropertyChange (String propertyName, boolean oldValue, boolean newValue)
  {
    // zip
  }
}
