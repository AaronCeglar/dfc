package dsto.dfc.swing.table;

import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import dsto.dfc.logging.Log;
import dsto.dfc.swing.commands.CommandMenus;
import dsto.dfc.swing.commands.CommandSource;
import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.swing.controls.ColorCellRenderer;
import dsto.dfc.swing.controls.ColorChooserCombo;
import dsto.dfc.swing.controls.EnumComboBox;
import dsto.dfc.swing.controls.FontCellRenderer;
import dsto.dfc.swing.controls.FontPopupSelector;
import dsto.dfc.swing.controls.JTextFieldEditorAdapter;
import dsto.dfc.swing.dnd.CmdCopy;
import dsto.dfc.swing.dnd.CmdCut;
import dsto.dfc.swing.dnd.CmdPaste;
import dsto.dfc.swing.dnd.CnpProvider;
import dsto.dfc.swing.dnd.DfcMouseDragGestureRecognizer;
import dsto.dfc.swing.dnd.DragDropComponent;
import dsto.dfc.swing.dnd.DragSourceAdapter;
import dsto.dfc.swing.dnd.DropTargetAdapter;
import dsto.dfc.swing.dnd.MissingDataTransferable;
import dsto.dfc.swing.event.BasicSelectionEventSource;
import dsto.dfc.swing.event.SelectionEventSource;
import dsto.dfc.swing.event.SelectionListener;
import dsto.dfc.swing.forms.FormToCellEditorAdapter;
import dsto.dfc.swing.icons.IconicCellRenderer;
import dsto.dfc.util.EnumerationValue;
import dsto.dfc.util.PropertyEventSource;

/**
 * DFC extended table component.
 *
 * @version $Revision$
 */
public class DfcTable extends JTable
  implements PropertyEventSource, SelectionEventSource, CommandSource,
             CnpProvider, ListSelectionListener, DragDropComponent
{
  private CommandView contextMenuView =
    new CommandView (CommandView.CONTEXT_MENU_VIEW);
  private boolean selectEditorByClass = false;
  private boolean selectRendererByClass = false;
  private boolean dndEnabled = false;
  private boolean cnpCommandsEnabled = false;
  private HashMap mapInterfaceToEditor = new HashMap ();
  private JTextFieldEditorAdapter defaultEditor = new JTextFieldEditorAdapter (new JTextField ());
  private transient BasicSelectionEventSource selectionListeners;
  private int dragStartRow = -1;
  private transient boolean autoSelectNewRows = false;
  private transient boolean cnpCopyEnabled = false;
  private transient boolean cnpCutEnabled = false;

  public DfcTable ()
  {
    this (new DefaultDfcTableModel ());
  }

  public DfcTable (TableModel model)
  {
    super (model);

    setTableHeader (new DfcTableHeader (this));

    // setup default renderers and editors
    setDefaultRenderer (Object.class, new IconicCellRenderer ());
    setDefaultRenderer (Color.class, new ColorCellRenderer ());
    setDefaultEditor (Color.class,
                      new FormToCellEditorAdapter (new ColorChooserCombo ()));
    setDefaultEditor (Font.class, new FontPopupSelector ());
    setDefaultRenderer (Font.class, new FontCellRenderer ());
    setDefaultEditor (EnumerationValue.class, new EnumComboBox ());

    contextMenuView.addCommand (new CmdSelectAll (this));

    CommandMenus.installContextMenu (this, contextMenuView);

    updateCnpEnabled ();
  }

  public void enableDragAndDrop ()
  {
    if (!dndEnabled)
    {
      // NOTE: using DfcMouseDragGestureRecognizer instead of system
      // default to work around bug (see DfcMouseDragGestureRecognizer
      // header comment for full description).
      DfcMouseDragGestureRecognizer recognizer =
        new DfcMouseDragGestureRecognizer (this, DnDConstants.ACTION_COPY_OR_MOVE);
      new DragSourceAdapter (this, recognizer);
      new DropTargetAdapter (this);
      dndEnabled = true;
    }
  }

  public DfcTableModel getDfcTableModel ()
  {
    if (getModel () instanceof DfcTableModel)
      return (DfcTableModel)getModel ();
    else
      return null;
  }

  public void cancelEditing ()
  {
    TableCellEditor editor = getCellEditor ();

    if (editor != null)
      editor.cancelCellEditing ();
  }

  public void stopEditing ()
  {
    TableCellEditor editor = getCellEditor ();

    if (editor != null)
      editor.stopCellEditing ();
  }

  /**
   * Size all columns to best fit.  When auto resize is on, last column
   * is not resized.
   *
   * @see #sizeColumnToFit
   */
  public void sizeColumnsToFit ()
  {
    TableColumnModel columns = getColumnModel ();

    int lastColumn = columns.getColumnCount ();

    if (getAutoResizeMode () != AUTO_RESIZE_OFF)
      lastColumn--;

    for (int i = 0; i < lastColumn; i++)
    {
      sizeColumnToFit (columns.getColumn (i));
    }
  }

  /**
   * Auto size a column to the smallest width required to display all
   * items, including the header.
   *
   * @param column The column to resize.
   * @return The new size of the column.
   *
   * @see #sizeColumnsToFit
   * @see Tables#getBestColumnSize
   */
  public int sizeColumnToFit (TableColumn column)
  {
    TableColumnModel columns = getColumnModel ();
    int columnIndex =
      columns.getColumnIndex (column.getIdentifier ());
    int width =
      Math.max (Tables.getBestColumnSize (this, column), column.getMinWidth ());

    // bizarre logic to get table to size column (fighting table's
    // auto resize logic).
    column.setWidth (0);
    sizeColumnsToFit (columnIndex);
    column.setWidth (width);
    column.setPreferredWidth (width);
    sizeColumnsToFit (columnIndex);

    return width;
  }

  public void setCnpCommandsEnabled (boolean newValue)
  {
    if (cnpCommandsEnabled != newValue)
    {
      cnpCommandsEnabled = newValue;

      if (cnpCommandsEnabled)
      {
        contextMenuView.addCommand (new CmdCut (this), 0);
        contextMenuView.addCommand (new CmdCopy (this), 1);
        contextMenuView.addCommand (new CmdPaste (this), 2);
      } else
      {
        contextMenuView.removeCommand ("edit.Cut");
        contextMenuView.removeCommand ("edit.Copy");
        contextMenuView.removeCommand ("edit.Paste");
      }

      firePropertyChange ("cnpCommandsEnabled",
                          !cnpCommandsEnabled, cnpCommandsEnabled);
    }
  }

  public boolean getCnpCommandsEnabled ()
  {
    return cnpCommandsEnabled;
  }

  /**
   * Enable cell renderer selection by class of value in cells of a column
   * rather than by getColumnClass () of model.
   */
  public void enableRenderCellsByClass (TableColumn column)
  {
    selectRendererByClass = true;
    column.setCellRenderer (null);
  }

  /**
   * Enable cell editor selection by class of value in cells of a column
   * rather than by getColumnClass () of model.
   */
  public void enableEditCellsByClass (TableColumn column)
  {
    selectEditorByClass = true;
    column.setCellEditor (null);
  }

  /**
   * Override JTable's getCellEditor ().
   */
  public TableCellEditor getCellEditor (int row, int col)
  {
    TableColumn column = getColumnModel ().getColumn (col);

    if (selectEditorByClass && column.getCellEditor () == null)
    {
      Object value = getModel ().getValueAt (row, column.getModelIndex ());

      return getDefaultEditor (value.getClass ());
    } else
      return super.getCellEditor (row, col);
  }

  public TableCellRenderer getCellRenderer (int row, int col)
  {
    TableColumn column = getColumnModel ().getColumn (col);

    if (selectRendererByClass && column.getCellRenderer () == null)
    {
      Object value = getModel ().getValueAt (row, column.getModelIndex ());

      if (value != null)
        return getDefaultRenderer (value.getClass ());
      else
        return super.getCellRenderer (row, col);
    } else
    {
      return super.getCellRenderer (row, col);
    }
  }

  /**
   * Override JTable getDefaultEditor () to provide editor mapping via
   * interface as well as by class.
   */
  public TableCellEditor getDefaultEditor (Class valueClass)
  {
    TableCellEditor editor =
      (TableCellEditor)defaultEditorsByColumnClass.get (valueClass);

    if (editor == null)
    {
      // see if we can find a matching interface

      for (Iterator i = mapInterfaceToEditor.keySet ().iterator ();
           i.hasNext () && editor == null; )
      {
        Class valueInterface = (Class)i.next ();

        if (valueInterface.isAssignableFrom (valueClass))
          editor = (TableCellEditor)mapInterfaceToEditor.get (valueInterface);
      }

      // okay, just use a (more intelligent) text editor
      if (editor == null)
      {
        defaultEditor.setValueClass (valueClass);
        editor = defaultEditor;
      }
    }

    return editor;
  }

  /**
   * Override JTable setDefaultEditor () to store interfaces in a
   * separate table.
   *
   * @see #getDefaultEditor
   */
  public void setDefaultEditor (Class valueClass, TableCellEditor editor)
  {
    if (valueClass.isInterface ())
      mapInterfaceToEditor.put (valueClass, editor);
    else
      super.setDefaultEditor (valueClass, editor);
  }

  /**
   * Ensure a given row is visible.
   */
  public void scrollToRow (int row)
  {
    scrollRectToVisible
      (new Rectangle (0, row * getRowHeight (), 1, getRowHeight ()));
  }

  /**
   * Ensure bottom part of table is visible.
   */
  public void scrollToBottom ()
  {
    scrollRectToVisible (new Rectangle (0, getHeight (), 1, getRowHeight ()));
  }

  /**
   * Ensure top part of table is visible.
   */
  public void scrollToTop ()
  {
    scrollRectToVisible (new Rectangle (0, 0, 1, getRowHeight ()));
  }

  // SelectionEventSource interface

  public void addSelectionListener (SelectionListener l)
  {
    if (selectionListeners == null)
      selectionListeners = new BasicSelectionEventSource (this);

    selectionListeners.addSelectionListener (l);
  }

  public void removeSelectionListener (SelectionListener l)
  {
    if (selectionListeners != null)
      selectionListeners.removeSelectionListener (l);
  }

  // CommandSource interface

  public CommandView getCommandView (String viewName)
  {
    if (viewName.equals (CommandView.CONTEXT_MENU_VIEW))
      return contextMenuView;
    else
      return null;
  }

  // CnpProvider interface

  public boolean isCnpCopyEnabled ()
  {
    RectSelection selection = getRectSelection ();

    if (getModel () instanceof DfcTableModel &&
        !selection.isEmpty () &&
        selection.isInRange (getModel ()))
    {
      DfcTableModel model = getDfcTableModel ();

      if (selection.isRowSelection ())
        return model.canCopyRows (selection.startRow, selection.endRow);
      else if (selection.isCellSelection ())
        return model.canCopyCells (selection.startRow, selection.endRow,
                                   selection.startColumn, selection.endColumn);
    }

    return false;
  }

  public boolean isCnpCutEnabled ()
  {
    RectSelection selection = getRectSelection ();

    if (getModel () instanceof DfcTableModel &&
        !selection.isEmpty () &&
        selection.isInRange (getModel ()))
    {
      DfcTableModel model = getDfcTableModel ();

      if (selection.isRowSelection ())
      {
        return model.canCopyRows (selection.startRow, selection.endRow) &&
                 model.canDeleteRows (selection.startRow, selection.endRow);
      } else if (selection.isCellSelection ())
      {
        return model.canCopyCells (selection.startRow, selection.endRow,
                                   selection.startColumn, selection.endColumn) &&
               model.canDeleteCells (selection.startRow, selection.endRow,
                                     selection.startColumn, selection.endColumn);
      }
    }

    return false;
  }

  public boolean isCnpPasteEnabled (Transferable transferable)
  {
    RectSelection selection = getRectSelection ();

    if (getModel () instanceof DfcTableModel)
    {
      DfcTableModel model = getDfcTableModel ();

      if (selection.isRowSelection ())
      {
        // if empty selection, paste at end
        if (selection.startRow == -1)
          selection.startRow = selection.endRow = model.getRowCount ();

        return model.canPasteRows (transferable,
                                   selection.startRow);
      } else if (!selection.isEmpty () &&
                 selection.isCellSelection () &&
                 selection.isInRange (getModel ()))
      {
        return model.canPasteCells (transferable,
                                    selection.startRow, selection.endRow,
                                    selection.startColumn, selection.endColumn);
      }
    }

    return false;
  }

  public Transferable cnpCopy ()
    throws UnsupportedOperationException, CloneNotSupportedException
  {
    RectSelection selection = getRectSelection ();

    if (getModel () instanceof DfcTableModel && !selection.isEmpty ())
    {
      stopEditing ();

      DfcTableModel model = getDfcTableModel ();

      if (selection.isRowSelection ())
        return model.copyRows (selection.startRow, selection.endRow);
      else if (selection.isCellSelection ())
        return model.copyCells (selection.startRow, selection.endRow,
                                selection.startColumn, selection.endColumn);
    }

    // if we got this far, operation is not supported
    throw new UnsupportedOperationException ();
  }

  public Transferable cnpCut ()
    throws UnsupportedOperationException, CloneNotSupportedException
  {
    RectSelection selection = getRectSelection ();

    if (getModel () instanceof DfcTableModel && !selection.isEmpty ())
    {
      stopEditing ();

      DfcTableModel model = getDfcTableModel ();

      if (selection.isRowSelection ())
      {
        Transferable transferable =
          model.copyRows (selection.startRow, selection.endRow);

        model.deleteRows (selection.startRow, selection.endRow);

        return transferable;
      } else if (selection.isCellSelection ())
      {
        Transferable transferable =
          model.copyCells (selection.startRow, selection.endRow,
                           selection.startColumn, selection.endColumn);

        model.deleteCells (selection.startRow, selection.endRow,
                           selection.startColumn, selection.endColumn);

        return transferable;
      }
    }

    // if we got this far, operation is not supported
    throw new UnsupportedOperationException ();
  }

  public boolean cnpPaste (Transferable transferable)
    throws UnsupportedOperationException,
    UnsupportedFlavorException, CloneNotSupportedException, IOException
  {
    RectSelection selection = getRectSelection ();

    if (getModel () instanceof DfcTableModel)
    {
      DfcTableModel model = getDfcTableModel ();

      stopEditing ();
      clearSelection ();
      autoSelectNewRows = true;

      try
      {
        if (selection.isRowSelection ())
        {
          // if empty selection, paste at end
          if (selection.startRow == -1)
            selection.startRow = selection.endRow = model.getRowCount ();

          model.pasteRows (transferable, selection.startRow);
        } else if (!selection.isEmpty () && selection.isCellSelection ())
          model.pasteCells (transferable,
                            selection.startRow, selection.endRow,
                            selection.startColumn, selection.endColumn);
      } finally
      {
        autoSelectNewRows = false;
      }
    }

    return false;
  }

  // DragDropComponent interface

  public int getSupportedDragActions ()
  {
    return DnDConstants.ACTION_COPY_OR_MOVE;
  }

  public boolean isDragOK (DragGestureEvent e)
  {
    DfcTableModel model = getDfcTableModel ();
    int row = rowAtPoint (e.getDragOrigin ());

    if (model != null && row != -1)
    {
      boolean canCopy = model.canCopyRows (row, row);
      boolean canDelete = model.canDeleteRows (row, row);

      if ((e.getDragAction () & DnDConstants.ACTION_MOVE) != 0)
        return canCopy && canDelete;
      else if ((e.getDragAction () & DnDConstants.ACTION_COPY) != 0)
        return canCopy;
    }

    return false;
  }

  public Transferable startDrag (DragGestureEvent e)
  {
    Transferable transferable = null;
    DfcTableModel model = getDfcTableModel ();
    int row = rowAtPoint (e.getDragOrigin ());

    if (model != null && row != -1)
    {
      try
      {
        if (model.canCopyRows (row, row))
        {
          transferable = model.copyRows (row, row);
          dragStartRow = row;
        }
      } catch (CloneNotSupportedException ex)
      {
        Log.warn ("Failed to clone for drag", this, ex);
      }
    }

    return transferable;
  }

  public void endDrag (DragSourceDropEvent e, boolean moveData)
  {
    DfcTableModel model = getDfcTableModel ();

    if (moveData && model != null && dragStartRow != -1)
    {
      // if move within same table, dragStartRow will have been cleared
      // by executeDrop ()
      if (dragStartRow != -1)
        model.deleteRows (dragStartRow, dragStartRow);
    }

    dragStartRow = -1;
  }

  public void executeDrop (DropTargetDropEvent e)
  {
    DfcTableModel model = getDfcTableModel ();
    int row = rowAtPoint (e.getLocation ());

    if (model != null && row != -1)
    {
      try
      {
        // if moving data and drag/drop are in same table (ie dragStartRow
        // is set) then try to use moveRows () rather than pasteRows () for a
        // little more speed.
        if ((e.getDropAction () & DnDConstants.ACTION_MOVE) != 0 &&
             dragStartRow != -1 &&
             model.canMoveRows (dragStartRow, dragStartRow, row))
        {
          model.moveRows (dragStartRow, dragStartRow, row);
          dragStartRow = -1;
        } else if (model.canPasteRows (e.getTransferable (), row))
          model.pasteRows (e.getTransferable (), row);
      } catch (Exception ex)
      {
        Log.warn ("drop failed", this, ex);
      }
    }
  }

  public int getSupportedDropActions ()
  {
    return DnDConstants.ACTION_COPY_OR_MOVE;
  }

  public boolean isDropOK (DropTargetDragEvent e)
  {
    DfcTableModel model = getDfcTableModel ();

    if (model != null)
    {
      int row = rowAtPoint (e.getLocation ());

      if (row != -1)
      {
        // check for internal move
        if ((e.getDropAction () & DnDConstants.ACTION_MOVE) != 0 &&
             dragStartRow != -1)
        {
          return model.canMoveRows (dragStartRow, dragStartRow, row);
        } else
        {
          Transferable transferable =
            new MissingDataTransferable (e.getCurrentDataFlavors ());

          return model.canPasteRows (transferable, row);
        }
      }
    }

    return false;
  }

  public void showDragUnderFeedback (boolean dropOK, DropTargetDragEvent e)
  {
    // zip
  }

  public void hideDragUnderFeedback ()
  {
    // zip
  }

  // ListSelectionModelListener interface

  /**
   * Override of JTable's ListSelectionModelListener interface implementation.
   * (do not invoke directly).
   *
   * @todo This is a bit dodgy: we are relying on JTable registering itself
   * as a listener to both row and column selection models.
   */
  public void valueChanged (ListSelectionEvent e)
  {
    super.valueChanged (e);

    // if event is an adjustment event OR a column selection event
    // when column selection not enabled, ignore it
    if (e.getValueIsAdjusting () ||
         (!getColumnSelectionAllowed () &&
           e.getSource () == getColumnModel ().getSelectionModel ()))
    {
      return;
    }

    updateCnpEnabled ();

    if (selectionListeners != null)
      selectionListeners.fireSelectionChanged (e);
  }

  /**
   * TableModelListener interface implementation only: do not invoke directly.
   */
  public void tableChanged (TableModelEvent e)
  {
    super.tableChanged (e);

    if (autoSelectNewRows && e.getType () == TableModelEvent.INSERT)
    {
      getSelectionModel ().addSelectionInterval (e.getFirstRow (), e.getLastRow ());
    }
  }

  /**
   * Update the settings of the cnpCopyEnabled and cnpCutEnabled properties.
   */
  protected final void updateCnpEnabled ()
  {
    boolean copyEnabled = isCnpCopyEnabled ();
    boolean cutEnabled = isCnpCutEnabled ();

    if (copyEnabled != cnpCopyEnabled)
    {
      cnpCopyEnabled = copyEnabled;
      firePropertyChange ("cnpCopyEnabled", !cnpCopyEnabled, cnpCopyEnabled);
    }

    if (cutEnabled != cnpCutEnabled)
    {
      cnpCutEnabled = cutEnabled;
      firePropertyChange ("cnpCutEnabled", !cnpCutEnabled, cnpCutEnabled);
    }
  }

  /**
   * Compute a rectangular selection from the current row and column
   * selection models and the selection mode settings for the table.
   */
  protected final RectSelection getRectSelection ()
  {
    ListSelectionModel selectedRows = getSelectionModel ();
    ListSelectionModel selectedColumns = getColumnModel ().getSelectionModel ();
    RectSelection selection = new RectSelection ();

    if (getRowSelectionAllowed ())
      selection.setRowSelection ();
    else
    {
      selection.startColumn = selectedColumns.getMinSelectionIndex ();
      selection.endColumn = selectedColumns.getMaxSelectionIndex ();
    }

    if (getColumnSelectionAllowed ())
      selection.setColumnSelection ();
    else
    {
      selection.startRow = selectedRows.getMinSelectionIndex ();
      selection.endRow = selectedRows.getMaxSelectionIndex ();
    }

    return selection;
  }

  /**
   * Represents a selection of rows, columns or a rectangular selection
   * of cells.
   */
  protected static final class RectSelection
  {
    public int startRow;
    public int endRow;
    public int startColumn;
    public int endColumn;

    public boolean isEmpty ()
    {
      return startRow == -1 || startColumn == -1;
    }

    public void setColumnSelection ()
    {
      startRow = 0;
      endRow = -1;
    }

    public void setRowSelection ()
    {
      startColumn = 0;
      endColumn = -1;
    }

    public void setCellSelection ()
    {
      endColumn = -1;
      endRow = -1;
    }

    /**
     * True if selection is a series of rows (ie all columns selected).
     * Selection may still be empty.
     */
    public boolean isRowSelection ()
    {
      return endColumn == -1;
    }

    /**
     * True if selection is a rectangle of cells.  Selection may still be
     * empty.
     */
    public boolean isCellSelection ()
    {
      return endRow != -1;
    }

    public boolean isInRange (TableModel model)
    {
      return startRow >= 0 &&
             startRow < model.getRowCount () &&
             startColumn >= 0 &&
             endColumn < model.getColumnCount ();
    }
  }
}
