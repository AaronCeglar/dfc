package test.text;

import java.util.Date;

import java.text.ParseException;

import dsto.dfc.text.IsoDateFormat;

import dsto.dfc.swing.text.selection.DateSelector;

import junit.framework.TestCase;

/**
 * Test the {@link dsto.dfc.swing.text.selection.DateSelector} class.
 * 
 * @author Matthew Phillips
 */
public class JUTestDateSelector extends TestCase
{
  public void testDateSelector ()
    throws Exception
  {
    DateSelector selector;
    
    // test "*"
    selector = new DateSelector ("*");
    assertTrue (selector.matches (makeDate  ("2001")));
    assertTrue (selector.matches (makeDate  ("2002")));
    
    // test short
    selector = new DateSelector ("2001");
    assertTrue  (selector.matches (makeDate  ("2001")));
    assertTrue  (selector.matches (makeDate  ("2001-01-01")));
    assertTrue  (selector.matches (makeDate  ("2001-01-01 00:00:00")));
    assertFalse (selector.matches (makeDate  ("2002")));
    
    // test long
    selector = new DateSelector ("2001-02-03 10:11:10");
    assertTrue  (selector.matches (makeDate  ("2001-02-03 10:11:10")));
    assertFalse (selector.matches (makeDate  ("2001-02-03 10:11:11")));
    
    // test long with timezone diff
    selector = new DateSelector ("2001-02-03 10:11:10 GMT+9:30");
    assertTrue  (selector.matches (makeDate  ("2001-02-03 10:11:10 GMT+9:30")));
    assertFalse (selector.matches (makeDate  ("2001-02-03 10:11:10 GMT+8:30")));
    
    // test range
    selector = new DateSelector ("2004-04-10 11:12:03>2004-04-10 12:00:00");
    assertTrue  (selector.matches (makeDate  ("2004-04-10 11:12:03")));
    assertTrue  (selector.matches (makeDate  ("2004-04-10 11:30:00")));
    assertTrue  (selector.matches (makeDate  ("2004-04-10 12:00:00")));
    assertFalse (selector.matches (makeDate  ("2004-04-10 11:12:02")));
    assertFalse (selector.matches (makeDate  ("2004-04-10 12:00:01")));
    
    // test ranges
    selector = new DateSelector ("1972>1980,2003>2004");
    assertTrue  (selector.matches (makeDate  ("1972")));
    assertTrue  (selector.matches (makeDate  ("1973")));
    assertTrue  (selector.matches (makeDate  ("1980")));
    assertTrue  (selector.matches (makeDate  ("2003")));
    assertFalse (selector.matches (makeDate  ("1971")));
    assertFalse (selector.matches (makeDate  ("2004-04-10 11:12:02")));
  }
  
  private Date makeDate (String date) throws ParseException
  {
    return IsoDateFormat.get ().parse (date);
  }

  public static void main(String [] args)
  {
    junit.textui.TestRunner.run (JUTestDateSelector.class);
  }

}
