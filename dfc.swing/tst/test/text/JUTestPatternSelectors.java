package test.text;

import dsto.dfc.util.IllegalFormatException;

import junit.framework.TestCase;

import dsto.dfc.swing.text.selection.BooleanSelector;
import dsto.dfc.swing.text.selection.RegexSelector;

/**
 * JUnit test to test the text pattern matchers in
 * dsto.dfc.text.selection.
 * 
 * @author weberd
 * @created 22/01/2003
 * @version $Revision$
 */
public class JUTestPatternSelectors extends TestCase
{
  public void testBooleanSelectors ()
    throws IllegalFormatException
  {
    BooleanSelector sel = new BooleanSelector ();
    sel.setExpression ("True");
    assertTrue (sel.matches ("true"));
    assertTrue (sel.matches ("True"));
    assertTrue (sel.matches ("T"));
    assertTrue (sel.matches ("t"));
    assertTrue (sel.matches ("y"));
    assertTrue (sel.matches ("Y"));
    assertTrue (sel.matches ("Yes"));
    assertTrue (sel.matches ("yes"));
    assertTrue (sel.matches ("tRUe"));
    assertFalse (sel.matches ("Truefalse"));
    assertFalse (sel.matches ("false"));
    assertFalse (sel.matches ("False"));
    assertFalse (sel.matches ("blah"));
    assertFalse (sel.matches ("f"));
    assertFalse (sel.matches ("F"));
    assertFalse (sel.matches ("Flab"));
  }

  public void testRegexSelectors ()
    throws IllegalFormatException
  {
    RegexSelector sel = new RegexSelector ("True", false);
    sel.setExpression ("True");
    assertTrue (sel.matches ("true"));
    assertTrue (sel.matches ("True"));
    assertTrue (sel.matches ("Truefalse"));
    assertTrue (sel.matches ("tRUe"));
    assertFalse (sel.matches ("false"));
    assertFalse (sel.matches ("False"));
    assertFalse (sel.matches ("blah"));
  }

}
