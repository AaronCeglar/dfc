package test.ui;

import java.awt.BorderLayout;

import dsto.dfc.swing.tree.DefaultDfcTreeModel;
import dsto.dfc.swing.tree.DfcTree;
import dsto.dfc.test.TestFrame;

public class TestDfcTree extends TestFrame
{
  DfcTree tree = new DfcTree ()
  {
//    public int getSupportedDragActions ()
//    {
//      return DnDConstants.ACTION_COPY;
//    }

//    public int getSupportedDropActions ()
//    {
//      return DnDConstants.ACTION_NONE;
//    }
  };

  DefaultDfcTreeModel model = new DefaultDfcTreeModel ();

  public TestDfcTree ()
  {
    TestIconicFolder srcFolder = new TestIconicFolder ("src");
    TestIconic file1 = new TestIconic ("file1.java", "information.gif");
    TestIconicFolder classesFolder = new TestIconicFolder ("classes");
    TestIconic file2 = new TestIconic ("file1.class", "information.gif");
    TestIconic file3 = new TestIconic ("file3.class", "information.gif");
    model.addEntry (model.getRoot (), srcFolder, -1);
    model.addEntry (srcFolder, file1, -1);
    model.addEntry (model.getRoot (), classesFolder, -1);
    model.addEntry (classesFolder, file2, -1);
    model.addEntry (file2, file3, -1);

    tree.enableDragAndDrop ();
//    model.setMutable (false);
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }

    tree.expandAll (model.getTreePathForEntry (model.getRoot ()));
  }

  public static void main (String[] args)
  {
    TestDfcTree testDfcTree = new TestDfcTree();
    testDfcTree.setSize (600, 400);
    testDfcTree.setVisible (true);
  }

  private void jbInit() throws Exception
  {
    tree.setModel (model);
    tree.setCnpCommandsEnabled (true);
    this.getContentPane().add(tree, BorderLayout.CENTER);
  }
}