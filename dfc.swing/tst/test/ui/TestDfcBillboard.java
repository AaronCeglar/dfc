package test.ui;

import java.awt.BorderLayout;
import java.awt.Image;

import dsto.dfc.swing.controls.DfcBillboard;
import dsto.dfc.swing.icons.ImageLoader;
import dsto.dfc.test.TestFrame;

public class TestDfcBillboard extends TestFrame
{
  DfcBillboard billboard = new DfcBillboard();

  public TestDfcBillboard()
  {

//    Image img1 = ImageLoader.getImage("/images/Map_of_Australia.gif");
//    Image img2 = ImageLoader.getImage("/images/ReplicationNetwork.gif");
    Image img = ImageLoader.getImage("/images/blkmarble.jpg");
    Image img3 = ImageLoader.getImage("/images/ibanner.gif");

//    billboard.addImage(img2, DfcBillboard.LAYOUT_STRETCHED_ASPECT);
//    billboard.addImage(img1, DfcBillboard.LAYOUT_STRETCHED_NOASPECT);
    billboard.addImage(img, DfcBillboard.LAYOUT_TILED);
    billboard.addImage(img3, DfcBillboard.LAYOUT_SINGLE, DfcBillboard.ALIGNMENT_BOTTOM_RIGHT);

    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception
  {
    this.getContentPane().add(billboard, BorderLayout.CENTER);
    this.setVisible(true);
  }

  public static void main (String[] args)
  {
    new TestDfcBillboard();
  }
}
