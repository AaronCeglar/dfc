package test.ui;

import dsto.dfc.swing.controls.AbstractIconicFolder;
import dsto.dfc.util.Copyable;

public class TestIconicFolder
  extends AbstractIconicFolder implements Copyable
{
  String name;

  public TestIconicFolder (String name)
  {
    this.name = name;
  }

  public String getName ()
  {
    return name;
  }

  public Object clone () throws CloneNotSupportedException
  {
    return super.clone ();
  }
}