package test.ui;

import java.awt.GridLayout;

import dsto.dfc.swing.controls.ColorChooserPanel;
import dsto.dfc.test.TestFrame;

/**
 * This tester puts the dsto.dfc.swing.controls.color_chooser.ColorChooserPanel
 * panel in a TestFrame.
 *
 * @author       Luke Marsh
 * @version      $Revision$
 */
public class TestColorChooser extends TestFrame
{

  private ColorChooserPanel CCP;
  private GridLayout gridLayout1;

  /**
   * Initializes variables and runs jbInit ()
   */
  public TestColorChooser ()
  {
    try
    {
      CCP = new ColorChooserPanel ();
      gridLayout1 = new GridLayout ();
      jbInit ();
    }
    catch (Exception e)
    {
      e.printStackTrace ();
    }
  }


  /**
   * Creates, sizes and displays the JFrame.
   */
  public static void main (String[] args)
  {
    TestColorChooser testColorChooser = new TestColorChooser ();
    testColorChooser.pack ();
    testColorChooser.setVisible (true);
  }


  /**
   * Initializes layouts.
   */
  private void jbInit () throws Exception
  {
    this.getContentPane ().setLayout (gridLayout1);
    this.getContentPane ().add (CCP, null);
  }
}