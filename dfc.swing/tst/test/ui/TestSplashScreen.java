package test.ui;

import dsto.dfc.swing.controls.SplashScreen;
import dsto.dfc.test.TestFrame;

public class TestSplashScreen extends TestFrame
{

  public TestSplashScreen()
  {
    SplashScreen splashScreen = new SplashScreen (this, null, 8);
    splashScreen.setVersionLine ("Version 1.1");

    splashScreen.popup ();

    try
    {
      Thread.sleep (3000);
    } catch (Exception ex)
    {
      ex.printStackTrace ();
    }

    splashScreen.popdown ();

    setSize (600, 300);
    setVisible (true);
  }

  public static void main(String[] args)
  {
    new TestSplashScreen ();
  }
} 