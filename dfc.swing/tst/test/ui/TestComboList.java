package test.ui;

import java.awt.BorderLayout;

import dsto.dfc.swing.controls.ComboList;
import dsto.dfc.test.TestFrame;

public class TestComboList extends TestFrame
{
  ComboList comboList1 = new ComboList(new String [] {"One", "Two", "Three", "Four", "Five", "Six"});

  public TestComboList()
  {
    try 
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  public static void main(String[] args)
  {
    TestComboList testComboList = new TestComboList();
    testComboList.pack ();
    testComboList.setVisible (true);
  }

  private void jbInit() throws Exception
  {
    this.getContentPane().add(comboList1, BorderLayout.CENTER);
  }
}

