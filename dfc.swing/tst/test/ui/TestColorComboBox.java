package test.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.SystemColor;

import javax.swing.JLabel;
import javax.swing.JPanel;

import dsto.dfc.swing.controls.ColorComboBox;
import dsto.dfc.test.TestFrame;

public class TestColorComboBox extends TestFrame
{
  JPanel jPanel1 = new JPanel();
  ColorComboBox comboBox2 = new ColorComboBox();
  JLabel label = new JLabel();
  JLabel color1Label = new JLabel();
  ColorComboBox comboBox1 = new ColorComboBox();

  public TestColorComboBox()
  {
    try 
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  public static void main (String[] args)
  {
    // force app to come up in the system L&F
/*    String laf = UIManager.getSystemLookAndFeelClassName();

    try
    {
      UIManager.setLookAndFeel(laf);
    } catch (UnsupportedLookAndFeelException e)
    {
    } catch (Exception exc)
    {
    }*/

    TestColorComboBox testColorComboBox = new TestColorComboBox();
    testColorComboBox.setSize (400, 300);
    testColorComboBox.setVisible (true);
  }

  private void jbInit() throws Exception
  {
    label.setLabelFor(comboBox2);
    label.setDisplayedMnemonic ('2');
    label.setText("Color 2:");
    color1Label.setText("Color 1:");
    color1Label.setDisplayedMnemonic('1');
    color1Label.setLabelFor(comboBox1);
    comboBox2.setSelectedColor(Color.orange);
    comboBox1.setSelectedColor(SystemColor.textHighlight);
    this.getContentPane().add(jPanel1, BorderLayout.CENTER);
    jPanel1.add(color1Label, null);
    jPanel1.add(comboBox1, null);
    jPanel1.add(label, null);
    jPanel1.add(comboBox2, null);
  }
}