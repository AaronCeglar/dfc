package test.ui;

import java.awt.FlowLayout;

import javax.swing.UIManager;

import dsto.dfc.swing.controls.ToolButton;
import dsto.dfc.test.TestFrame;

/**
 * @author Matthew Phillips
 * @version $Revision$
 */
public class TestToolButton extends TestFrame
{
  ToolButton toolButton1 = new ToolButton();
  FlowLayout flowLayout1 = new FlowLayout();
  public TestToolButton ()
  {
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  public void run ()
  {
    pack ();
    setVisible (true);
  }

  public static void main(String[] args) throws Exception
  {
    UIManager.setLookAndFeel ("com.incors.plaf.kunststoff.KunststoffLookAndFeel");
    TestToolButton app = new TestToolButton();
    app.run ();
  }

  private void jbInit() throws Exception
  {
    toolButton1.setText("Tool Button");
    this.getContentPane().setLayout(flowLayout1);
    flowLayout1.setAlignment(FlowLayout.LEFT);
    this.getContentPane().add(toolButton1, null);
  }
}