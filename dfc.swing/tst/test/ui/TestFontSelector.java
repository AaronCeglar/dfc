package test.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import dsto.dfc.swing.controls.FontPopupSelector;
import dsto.dfc.swing.controls.FontSelectorDialog;
import dsto.dfc.test.TestFrame;

public class TestFontSelector
  extends TestFrame implements PropertyChangeListener
{
  JLabel fontLabel = new JLabel();
  JPanel jPanel1 = new JPanel();
  JButton showButton = new JButton();
  JButton showNonmodal = new JButton();
  FontPopupSelector fontPopupSelector = new FontPopupSelector();

  public TestFontSelector()
  {
    try 
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  public static void main(String[] args)
  {
    TestFontSelector testFontSelector = new TestFontSelector();
    testFontSelector.pack ();
    testFontSelector.setVisible (true);
  }

  private void jbInit() throws Exception
  {
    showButton.setText("Show Modal");
    showButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        showButton_actionPerformed(e);
      }
    });
    showNonmodal.setText("Show");
    showNonmodal.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        showNonmodal_actionPerformed(e);
      }
    });
    this.getContentPane().add(fontLabel, BorderLayout.SOUTH);
    this.getContentPane().add(jPanel1, BorderLayout.CENTER);
    jPanel1.add(showButton, null);
    jPanel1.add(showNonmodal, null);
    jPanel1.add(fontPopupSelector, null);
  }

  void showButton_actionPerformed(ActionEvent e)
  {
    FontSelectorDialog fontSelectorDialog = new FontSelectorDialog(this, "Select Font", true);

    fontSelectorDialog.setVisible (true);
    fontLabel.setFont (fontSelectorDialog.getSelectedFont ());
    fontLabel.setText (fontSelectorDialog.getSelectedFont ().toString ());
  }

  void showNonmodal_actionPerformed(ActionEvent e)
  {
    FontSelectorDialog fontSelectorDialog = new FontSelectorDialog(this, "Select Font", false);
    fontSelectorDialog.addPropertyChangeListener (this);

    fontSelectorDialog.setVisible (true);
  }

  public void propertyChange (PropertyChangeEvent e)
  {
    if (e.getPropertyName ().equals ("selectedFont"))
    {
      FontSelectorDialog fontSelectorDialog = (FontSelectorDialog)e.getSource ();

      fontLabel.setFont (fontSelectorDialog.getSelectedFont ());
      fontLabel.setText (fontSelectorDialog.getSelectedFont ().toString ());
    }
  }
} 