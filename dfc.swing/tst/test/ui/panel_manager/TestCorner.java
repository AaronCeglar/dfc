package test.ui.panel_manager;

import java.awt.Point;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLayeredPane;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import dsto.dfc.swing.panels.Corner;

public class TestCorner extends TestCase  {

 JFrame           outer;
 JFrame           inner;
 JInternalFrame   inside;
 Point            location;
 JButton          button;

 public static Test suite() {

   return new TestSuite( TestCorner.class );
 }

 public TestCorner( String name ) {

   super( name );
 }

 public void setUp() {

   outer       = new JFrame         ();
   inner       = new JFrame         ();
   inside      = new JInternalFrame ();
   outer .setSize   ( 450, 400 );
   inner .setSize   ( 150, 120 );
   inside.setSize   ( 150, 120 );
   outer.setLocation( 300, 100 );
   outer.getLayeredPane().add( inside, JLayeredPane.MODAL_LAYER );
 }

  public void tearDown() {

   outer.dispose ();
   inner.dispose ();
   inside.dispose();
 }

 private void testCorner( Corner corner, Point answerOne, Point answerTwo ) {

   corner.placeFrame( inner,  outer );
   corner.placeFrame( inside, outer );

   assertEquals( answerOne, inner .getLocation() );
   assertEquals( answerTwo, inside.getLocation() );
 }

 public void testTopLeft() {

   testCorner( Corner.TOP_LEFT, new Point( 300, 100), new Point( 0, 0 ) );
 }

  public void testBottomRight() {

   testCorner( Corner.BOTTOM_RIGHT, new Point( 600, 380), new Point( 292, 251) );
 }

  public void testTopRight() {

   testCorner( Corner.TOP_RIGHT, new Point( 600, 100), new Point( 292, 0) );
}

  public void testBottomLeft() {

    testCorner( Corner.BOTTOM_LEFT, new Point( 300, 380), new Point( 0, 251) );
 }

  public void testIndentTopLeft() {

    location = Corner.TOP_LEFT.locationFor( inner, outer, 1 );
    assertEquals( new Point( 335, 128), location );
 }

  public void testIndentTopRight() {

    location = Corner.TOP_RIGHT.locationFor( inner,  outer, 4 );
    assertEquals( new Point( 460, 212 ), location );

    location = Corner.TOP_RIGHT.locationFor( inside, outer, 4 );
    assertEquals( new Point( 152, 112 ), location );
 }

 public void testIndentBottomLeft() {

    location = Corner.BOTTOM_LEFT.locationFor( inner,  outer, 2 );
    assertEquals( new Point( 370, 324 ), location );

    location = Corner.BOTTOM_LEFT.locationFor( inside, outer, 2 );
    assertEquals( new Point( 70, 195 ), location );
 }

 public void testIndentBottomRight() {

   location = Corner.BOTTOM_RIGHT.locationFor( inner,  outer, 3 );
   assertEquals( new Point( 495, 296 ), location );

   location = Corner.BOTTOM_RIGHT.locationFor( inside, outer, 3 );
   assertEquals( new Point( 187, 167 ), location );
 }

 public void testBounds() {

   location = Corner.BOTTOM_RIGHT.locationFor( inner,  outer, 14 );
   assertTrue( outer.contains( location ));

   location = Corner.BOTTOM_LEFT.locationFor( inside,  outer, 14 );
   assertTrue( outer.contains( location ));
 }

 public void testTooLarge() {

   inner.setSize( 5000, 5000 );
   location = Corner.BOTTOM_RIGHT.locationFor( inner,  outer, 3 );
   assertEquals( new Point( 300, 100 ), location );
   location = Corner.TOP_RIGHT.locationFor( inner,  outer, 0 );
   assertEquals( new Point( 300, 100 ), location );

   inner.setSize( 450, 400 );
   location = Corner.TOP_LEFT.locationFor( inner,  outer, 1 );
   assertEquals( new Point( 300, 100), location );

   inside.setSize( 450, 400 );
   location = Corner.TOP_LEFT.locationFor( inside,  outer, 0 );
   assertEquals( new Point(), location );
 }

 public void testNullFrames() {

   Corner.BOTTOM_LEFT.placeFrame ( null, null, 4         );
   Corner.BOTTOM_RIGHT.placeFrame( new JFrame(), null, 0 );
   Corner.TOP_LEFT.placeFrame    ( null, new JButton(), 1);
   Corner.TOP_RIGHT.placeFrame   ( null, new JButton(), 1);

   assertEquals( new Point(), Corner.TOP_LEFT.locationFor( null, null) );
  }

 } // TestCorner