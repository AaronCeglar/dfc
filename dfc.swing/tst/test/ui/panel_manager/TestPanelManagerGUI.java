package test.ui.panel_manager;

import java.awt.BorderLayout;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JPanel;

import dsto.dfc.swing.icons.ImageLoader;
import dsto.dfc.swing.icons.NullIcon;
import dsto.dfc.swing.panels.PanelManager;
import dsto.dfc.swing.panels.PanelManagerFinder;
import dsto.dfc.swing.panels.ToolView;
import dsto.dfc.test.TestFrame;

public class TestPanelManagerGUI extends TestFrame

{
protected static final Icon NULL_ICON = new NullIcon (20, 20);
protected static final Icon TEST_ICON =
ImageLoader.getIcon ("/dsto/dfc/icons/file_open.gif");
PanelManager pm;

  public static void main (String[] args) throws Exception
  {
    TestPanelManagerGUI testApp = new TestPanelManagerGUI ();
    testApp.setSize (600, 400);
    testApp.setLocation ( 100, 125 );
    testApp.setVisible (true);
    testApp.initialize();
  }

  private void initialize() throws Exception
  {
    pm = new PanelManager();
    this.getContentPane().add(pm, BorderLayout.CENTER);
    JPanel panelOne = new JPanel();
    JPanel panelTwo = new JPanel();
    JPanel panelThree = new JPanel();
    JPanel panelFour = new JPanel();

    panelOne.add(new JButton("test1"));
    panelTwo.add(new JButton("test2"));
    panelThree.add(new JButton("test3"));
    panelFour.add(new JButton("test4"));

    pm.addPanel(panelOne, 0, "One", TEST_ICON).setChanged  ( true );
    pm.addPanel(panelTwo, 1, "Two", TEST_ICON).setFloating ( PanelManager.FLOAT_EXTERNAL );
    pm.setActivePanel (pm.addPanel(panelThree, 2, "Three", TEST_ICON));

    PanelManager.Panel addedPanel = pm.addPanel(panelFour, 3, "Four", TEST_ICON);
    addedPanel.setShowingName( false );
    ToolView toolView = addedPanel.addToolView( new JButton("tool Four A") );
    toolView.setTitle( "tool Four A" );
    toolView.setTool ( new JButton ("tool Four Z") );
    JButton buttonB = new JButton("tool Four B");
    buttonB.setSize(175, 45 );
    addedPanel.addToolView( buttonB );
    addedPanel.addToolView( new JButton("tool Four C") );

    PanelManagerFinder.addToolView( new JButton("tool Four D"), panelOne );

    PanelManagerFinder.addToolView( new JButton("tool Four E"), panelTwo );
    PanelManagerFinder.addToolView( new JButton("tool Four F"), panelTwo );
    PanelManagerFinder.addToolView( new JButton("tool Four G"), panelTwo );
    PanelManagerFinder.addToolView( new JButton("tool Four H"), panelTwo );
    PanelManagerFinder.addToolView( new JButton("tool Four I"), panelTwo );
    PanelManagerFinder.addToolView( new JButton("tool Four J"), panelTwo );
    PanelManagerFinder.addToolView( new JButton("tool Four K"), panelTwo );
    PanelManagerFinder.addToolView( new JButton("tool Four L"), panelTwo );
    PanelManagerFinder.addToolView( new JButton("tool Four M"), panelTwo );
    PanelManagerFinder.addToolView( new JButton("tool Four N"), panelTwo );

    JButton button = new JButton();
    this.getContentPane().add (button);


    PanelManagerFinder.addToolView( new JButton("tool Four G"), button );

  //   pm.getPanelAt (1).makeUncloseable ();
     pm.setAutoHide (false);

 //   addedPanel.setChanged( true );
 //   pm.replacePanel( addedPanel, new JButton("test5"));
 //   pm.replacePanel( pm.getPanelAt( 1 ), new JButton("test6"));

  //   pm.setFloating(panelOne, false);
 //    pm.setFloating(panelFour, true);

 //    pm.removePanel(panelOne);
 //    pm.removePanel(panelThree);
 //   pm.removePanel(panelTwo);

 //   pm.addPanel(new JPanel());

   //   pm.replacePanel(panelOne,panelThree,"Three");
 //    pm.replacePanel(panelTwo,(Component) panelFour,"Four",TEST_ICON);
 //   pm.setFloating(panelThree, true);
  //  pm.setFloating(panelTwo, true);

//    pm.addPanel(new JPanel());

 //   pm.setFloating(panelThree, true);

 //   pm.setFloating(jp, false);

 //   pm.replacePanel(jp, new JPanel());
 //   pm.addPanel(new JPanel());
 //   pm.removePanel(jp);

 //   pm.addPanel(comboBox1);
 //   pm.addPanel(color1Label);

  }
}