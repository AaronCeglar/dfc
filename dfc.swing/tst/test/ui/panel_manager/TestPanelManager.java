package test.ui.panel_manager;

import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JPanel;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.swing.icons.CompositeIcon;
import dsto.dfc.swing.panels.PanelManager;
import dsto.dfc.swing.panels.PanelManagerFinder;

 public class TestPanelManager extends TestCase {

  protected PanelManager pm;
  protected JPanel jpOne;
  protected JPanel jpTwo;
  protected JPanel jpThree;

  public TestPanelManager (String name)
  {
    super(name);
  }

  protected void setUp() {

    pm      = new PanelManager ();
    jpOne   = new JPanel();
    jpTwo   = new JPanel();
    jpThree = new JPanel();
  }

  protected void tearDown()
  {
    pm.removeAllPanels ();
  }

  public static Test suite()
  {
    return new TestSuite(TestPanelManager.class);
  }

  public void testAddPanel() {

      assertEquals( 0, pm.getPanelCount () );
      pm.addPanel( jpOne );
      assertEquals( 1, pm.getPanelCount () );
      pm.addPanel( jpTwo );
      assertEquals( 2, pm.getPanelCount () );
  }

  public void testRemovePanel() {

      PanelManager.Panel one = pm.addPanel( jpOne );
      PanelManager.Panel two = pm.addPanel( jpTwo );
      assertEquals( 2, pm.getPanelCount () );
      pm.removePanel( two );
      assertTrue(   pm.containsPanel( one ) );
      assertTrue( ! pm.containsPanel( two ) );
      assertEquals(1, pm.getPanelCount ());

      pm.removePanel( two  );
      pm.removePanel( null );

      assertTrue(   pm.containsPanel( one ) );
      assertTrue( ! pm.containsPanel( two ) );
      assertEquals( 1, pm.getPanelCount () );
  }

  public void testMultipleAddRemovePanel() {

      PanelManager.Panel one = pm.addPanel( jpOne );
      PanelManager.Panel two = pm.addPanel( jpTwo );

      pm.addPanel( jpThree );
      pm.removePanel( one );
      two.setFloating( PanelManager.FLOAT_INTERNAL );
      assertTrue( two.isFloating() );
      two.setFloating( PanelManager.FIXED );
      assertTrue( ! two.isFloating() );
      two.setFloating( PanelManager.FLOAT_INTERNAL );
      two.setFloating( PanelManager.FLOAT_INTERNAL );
      assertTrue( two.isFloating() );
      pm.removePanel( two );
      assertTrue( ! pm.containsPanel( two) );
  }

  public void testReplacePanel() {

      PanelManager.Panel one =   pm.addPanel( jpOne   );
      PanelManager.Panel two =   pm.addPanel( jpTwo   );
      two.setFloating( PanelManager.FLOAT_INTERNAL );
      pm.addPanel( jpThree );

      JButton button = new JButton();

      pm.replacePanel( one, new JPanel() );
      assertEquals( 3, pm.getPanelCount () );
      assertEquals( 1, pm.indexOfPanel( two ) );

      pm.replacePanel( two, button );
      assertEquals( 1, pm.indexOfPanel( pm.getPanel( button ) ) );
      assertTrue( ! pm.containsPanel( two ) );
  }

  public void testFloat() {

       PanelManager.Panel one = pm.addPanel( jpOne );
       one.setFloating( PanelManager.FLOAT_INTERNAL );
       assertTrue( one.isFloating() );
       one.setFloating( PanelManager.FLOAT_INTERNAL );
       assertTrue( one.isFloating() );
       assertEquals( PanelManager.FLOAT_INTERNAL, one.getFloating() );
       one.setFloating( PanelManager.FIXED );
       assertTrue( ! one.isFloating() );
       one.setFloating( PanelManager.FLOAT_EXTERNAL );
       assertTrue( one.isFloating() );
       assertEquals( PanelManager.FLOAT_EXTERNAL, one.getFloating() );
  }

  public void testChanged() {

       PanelManager.Panel one = pm.addPanel( jpOne );
       assertTrue ( one.isShowingName() );
       assertEquals( one.titleDisplay.getTabDisplayText(), "Not Named" );
       one.setChanged ( true );
       assertEquals( one.titleDisplay.getTabDisplayText(), "Not Named*" );
       one.setChanged ( true );
       one.setFloating( PanelManager.FLOAT_INTERNAL );
       assertTrue( one.getChanged() );
       assertEquals( one.titleDisplay.getTabDisplayText(), "Not Named*" );
       one.setChanged( false );
       assertTrue( ! one.getChanged() );
       assertEquals( one.titleDisplay.getTabDisplayText(), "Not Named" );
       one.setChanged( true );
       assertEquals( one.titleDisplay.getTabDisplayText(), "Not Named*" );
       one.setFloating( PanelManager.FLOAT_EXTERNAL );
       assertEquals( one.titleDisplay.getTabDisplayText(), "Not Named*" );
       one.setChanged( false );
       assertEquals( one.titleDisplay.getTabDisplayText(), "Not Named" );
  }

    public void testChangedForToolTip () {

       PanelManager.Panel one = pm.addPanel( jpOne );
       assertTrue ( one.isShowingName () );
       assertNull( one.titleDisplay.getToolTipText() );
       one.setChanged ( true );
       assertNull( one.titleDisplay.getToolTipText() );
       one.setShowingName (false);
       assertEquals( one.titleDisplay.getToolTipText(), "Not Named" );
       assertEquals( one.titleDisplay.getTabDisplayText(), "*" );
       one.setShowingName (true);
       assertNull( one.titleDisplay.getToolTipText() );
  }

   public void testIndex() {

     PanelManager.Panel one = pm.addPanel( jpOne );
     PanelManager.Panel two = pm.addPanel( jpTwo );
     assertEquals ( 0, pm.indexOfPanel( one ) );
     assertEquals ( 1, pm.indexOfPanel( two ) );
     assertEquals ( one, pm.getPanelAt( 0 ) );
     assertEquals ( two, pm.getPanelAt( 1 ) );
     assertNull   ( pm.getPanelAt     (500) );
     assertNull   ( pm.getPanelAt     (-37) );
  }

  public void testActive() {

    assertNull ( pm.getActivePanel() );
    PanelManager.Panel one = pm.addPanel( jpOne );
    PanelManager.Panel two = pm.addPanel( jpTwo );
    assertEquals ( one, pm.getActivePanel() );

    pm.setActivePanel( two );
    assertEquals ( two, pm.getActivePanel() );

    one.setFloating( PanelManager.FLOAT_INTERNAL );
    assertEquals ( two, pm.getActivePanel() );
    two.setFloating( PanelManager.FLOAT_INTERNAL );
    assertEquals ( two, pm.getActivePanel() );
    pm.removePanel( two );
    assertEquals ( one, pm.getActivePanel());
    pm.setActivePanel( null );
    assertNull ( pm.getActivePanel() );
    pm.setActivePanel( one );
    assertEquals ( one, pm.getActivePanel());
    pm.removePanel( one );
    assertNull ( pm.getActivePanel() );
  }

  public void testRemoveActive() {

    PanelManager.Panel one = pm.addPanel( jpOne );
    PanelManager.Panel two = pm.addPanel( jpTwo );

    assertEquals     ( one, pm.getActivePanel());
    pm.setActivePanel( two );
    assertEquals     ( two, pm.getActivePanel());
    pm.removePanel   ( two );
    assertEquals     ( one, pm.getActivePanel());
    pm.removePanel   ( one );
    assertNull       ( pm.getActivePanel     ());
  }

  public void testRemoveActiveTabPaneSynchronization()
  {
    PanelManager.Panel one   = pm.addPanel( jpOne   );
    PanelManager.Panel two   = pm.addPanel( jpTwo   );
    PanelManager.Panel three = pm.addPanel( jpThree );

    assertSame  ( one, pm.getActivePanel() );
    pm.removePanel ( one );
    assertSame  ( three, pm.getActivePanel() );
    assertEquals( 1, pm.tabPane.getSelectedIndex() );

    pm.removePanel ( three );
    assertSame  ( two, pm.getActivePanel() );
    assertEquals( 0, pm.tabPane.getSelectedIndex() );

    pm.removePanel ( two );
    assertNull  ( pm.getActivePanel() );
    assertEquals( -1, pm.tabPane.getSelectedIndex() );
  }

  public void testRemoveActiveLast() {

    PanelManager.Panel one = pm.addPanel( jpOne );
    PanelManager.Panel two = pm.addPanel( jpTwo );
    pm.setActivePanel( one );
    pm.removePanel   ( one );
    pm.removePanel   ( two);
    assertNull       ( pm.getActivePanel());
  }

  /* todo: fix this test - fails on some machines some of the time */
//  public void testRemoveExternalFloat() {
//
//    PanelManager.Panel one = pm.addPanel( jpOne );
//    PanelManager.Panel two = pm.addPanel( jpTwo );
//    pm.setActivePanel( one );
//    one.setFloating( PanelManager.FLOAT_EXTERNAL );
//    assertEquals   ( one, pm.getActivePanel()    );
//    two.setFloating( PanelManager.FLOAT_EXTERNAL );
//    assertEquals   ( one, pm.getActivePanel()    );
//
//    two.setFloating( PanelManager.FIXED          );
//    assertEquals   ( one, pm.getActivePanel()    );
//
//    pm.removePanel ( two );
//    assertEquals   ( one, pm.getActivePanel()    );
//    pm.removePanel ( one );
//    assertNull     ( pm.getActivePanel()   );
//  }

  public void testIcons() {

    PanelManager.Panel one = pm.addPanel( jpOne );
    pm.addPanel( jpTwo );
    pm.setActivePanel( one );

    assertTrue( ! ( pm.tabPane.getIconAt(0) instanceof CompositeIcon ) );
    assertTrue( one.getIcon() == PanelManager.NULL_ICON );
    one.setFloating( PanelManager.FLOAT_INTERNAL );
    assertTrue( pm.tabPane.getIconAt(0) instanceof CompositeIcon );
    one.setFloating( PanelManager.FLOAT_EXTERNAL );
    assertTrue( pm.tabPane.getIconAt(0) instanceof CompositeIcon );
    assertTrue( one.getIcon() == PanelManager.NULL_ICON );
    one.setFloating( PanelManager.FIXED );
    assertTrue( ! ( pm.tabPane.getIconAt(0) instanceof CompositeIcon ) );
  }

  public void testNullIcon() {

    PanelManager.Panel one = pm.addPanel( jpOne, "one", null );
    assertTrue( one.getIcon() == PanelManager.NULL_ICON );
  }

  public void testPanelMenus() {

    PanelManager.Panel one = pm.addPanel( new DummyMenu() );
    CommandView commands = one.getCommandView(CommandView.MAIN_MENU_VIEW);

    assertNotNull( one.getCommandView(CommandView.CONTEXT_MENU_VIEW ) );

    assertEquals ( 4, commands.getEntryCount());
    assertNotNull( commands.findCommand ( "view.Toggle Float Internal" ) );
    assertNotNull( commands.findCommand ( "view.Toggle Float External" ) );
    assertNull   ( commands.findCommand ( "view.Toggle Unfloat"        ) );
  }

  public void testFindPanelManager() {

    pm.addPanel( jpOne );
    assertEquals( pm, PanelManagerFinder.findPanelManagerForComponent( jpOne) );
    assertNull  ( PanelManagerFinder.findPanelManagerForComponent( new JButton() ) );
    assertNull  ( PanelManagerFinder.findPanelManagerForComponent( jpTwo         ) );
  }

  public void testDeepFindPanelManager() {

    JButton one, two, three, four, five;

    one   = new JButton();
    two   = new JButton();
    three = new JButton();
    four  = new JButton();
    five  = new JButton();

    one  .add( two   );
    two  .add( three );
    three.add( four  );
    four .add( five  );

    pm.add( jpOne );
    pm.addPanel( one );
    assertEquals( pm, PanelManagerFinder.findPanelManagerForComponent( jpOne) );
    assertEquals( pm, PanelManagerFinder.findPanelManagerForComponent( five ) );
    assertEquals( pm, PanelManagerFinder.findPanelManagerForComponent( four ) );
    assertEquals( pm, PanelManagerFinder.findPanelManagerForComponent( three ) );

    pm.add( one );

    assertEquals( pm, PanelManagerFinder.findPanelManagerForComponent( jpOne) );
    assertEquals( pm, PanelManagerFinder.findPanelManagerForComponent( five ) );
    assertEquals( pm, PanelManagerFinder.findPanelManagerForComponent( four ) );
    assertEquals( pm, PanelManagerFinder.findPanelManagerForComponent( three ) );

    assertNull  ( PanelManagerFinder.findPanelManagerForComponent( new JButton() ) );
  }

  public void testFindPanelManagerWithFrames() {

    PanelManager.Panel one = pm.addPanel( jpOne );
    assertEquals   ( pm, PanelManagerFinder.findPanelManagerForComponent( jpOne) );
    one.setFloating( PanelManager.FLOAT_INTERNAL );
    assertEquals   ( pm, PanelManagerFinder.findPanelManagerForComponent( jpOne) );
    one.setFloating( PanelManager.FLOAT_EXTERNAL );
    assertEquals   ( pm, PanelManagerFinder.findPanelManagerForComponent( jpOne) );
  }

  public void testFindPanel() {

    PanelManager.Panel one = pm.addPanel( jpOne );
    assertEquals( one, PanelManagerFinder.findPanelForComponent( jpOne) );
    assertNull  ( PanelManagerFinder.findPanelManagerForComponent( new JButton() ) );
    assertNull  ( PanelManagerFinder.findPanelManagerForComponent( jpTwo         ) );
  }

  public void testDeepFindPanel() {

    JButton one, two, three, four, five;

    one   = new JButton();
    two   = new JButton();
    three = new JButton();
    four  = new JButton();
    five  = new JButton();

    one  .add( two   );
    two  .add( three );
    three.add( four  );
    four .add( five  );

    assertNull( PanelManagerFinder.findPanelForComponent( five ) );
    PanelManager.Panel first = pm.addPanel( jpOne );
    jpOne.add( one );

    assertEquals( first, PanelManagerFinder.findPanelForComponent( jpOne ) );
    assertEquals( first, PanelManagerFinder.findPanelForComponent( five  ) );
    assertEquals( first, PanelManagerFinder.findPanelForComponent( four  ) );
    assertEquals( first, PanelManagerFinder.findPanelForComponent( three ) );
    assertEquals( first, PanelManagerFinder.findPanelForComponent( one   ) );

    pm.add( one );

    assertNull( PanelManagerFinder.findPanelForComponent( one  ) );
    assertNull( PanelManagerFinder.findPanelForComponent( five ) );
  }

  public void testCorrectActiveAfterFloatRemoval ()
  {
    pm.addPanel (jpOne  );
    pm.addPanel (jpTwo  );
    pm.addPanel (jpThree);
    pm.addPanel (new JPanel ());
    pm.getPanelAt (1).setFloating (PanelManager.FLOAT_EXTERNAL);
    pm.getPanelAt (3).setActive (true);
    pm.getPanelAt (1).setActive (true);
    pm.removePanel (pm.getPanelAt (1));

    int indexOfActivePanel = pm.indexOfPanel (pm.getActivePanel());
    int indexOfSelectedTab = pm.tabPane.getSelectedIndex ();
    assertEquals (indexOfActivePanel, indexOfSelectedTab);
  }

  public void testAdjacentFloatRemoval ()
  {
    pm.addPanel (jpOne  );
    pm.addPanel (jpTwo  );
    pm.addPanel (jpThree);
    pm.addPanel (new JPanel ());
    pm.getPanelAt (1).setFloating (PanelManager.FLOAT_EXTERNAL);
    pm.getPanelAt (2).setFloating (PanelManager.FLOAT_EXTERNAL);
    pm.getPanelAt (1).setActive (true);
    pm.removePanel (pm.getPanelAt (1));
  }

  public void testInternalFloatRemoval ()
  {
    pm.addPanel (jpOne  );
    pm.addPanel (jpTwo  );
    pm.addPanel (jpThree);
    pm.getPanelAt (1).setFloating (PanelManager.FLOAT_EXTERNAL);
    pm.getPanelAt (2).setFloating (PanelManager.FLOAT_INTERNAL);
    pm.setActiveIndex (0);
    pm.getPanelAt (0).setActive (true);
    pm.getPanelAt (2).setActive (true);
    assertEquals (pm.tabPane.getSelectedIndex (), pm.indexOfActivePanel ());
    pm.getPanelAt (2).setFloating (PanelManager.FIXED);
    assertEquals (2, pm.tabPane.getSelectedIndex ());
  }

  public void testNextPanel ()
  {
    assertNull (pm.getNextPanel (null));
    PanelManager.Panel first = pm.addPanel (jpOne  );
    assertNull ( pm.getNextPanel (first) );
    PanelManager.Panel second = pm.addPanel (jpTwo  );
    assertSame (second, pm.getNextPanel (first));
    assertSame (first, pm.getNextPanel (second));
    pm.removePanel (first);
    assertNull (pm.getNextPanel (first));
  }

  public void testPreferredSize ()
  {
    pm.addPanel (jpOne  );
    assertEquals (new Dimension (20, 72), pm.getPreferredSize ());
  }

  public void testMinimumSize ()
  {
    assertEquals (new Dimension (10, 62), pm.getPreferredSize ());
  }

  public void testUnclosablePanel ()
  {
    PanelManager.Panel notCloseable = pm.addPanel (jpOne);
    notCloseable.makeUncloseable ();
    PanelManager.Panel closeable    = pm.addPanel (jpTwo);

    pm.setActivePanel (closeable);
    closeable   .close ();
    notCloseable.close ();

    assertSame (notCloseable, pm.getActivePanel ());
    assertTrue (   pm.containsPanel (notCloseable));
    assertTrue ( ! pm.containsPanel (closeable));
  }
}