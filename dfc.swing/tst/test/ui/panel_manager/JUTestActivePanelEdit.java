package test.ui.panel_manager;

import javax.swing.JButton;

import junit.framework.TestCase;

import dsto.dfc.swing.panels.ActivePanelEdit;
import dsto.dfc.swing.panels.PanelManager;
import dsto.dfc.swing.undo.MonitoredUndoManager;

public class JUTestActivePanelEdit extends TestCase
{
  PanelManager         panelManager;
  MonitoredUndoManager undoManager;
  ActivePanelEdit      activePanelEdit;

  public JUTestActivePanelEdit (String name)
  {
    super (name);
  }

  protected void setUp ()
  {
    panelManager    = new PanelManager    ();
    panelManager.addPanel (new JButton ());
    panelManager.addPanel (new JButton ());
    activePanelEdit = new ActivePanelEdit (panelManager, 0, 1);
    undoManager     = new MonitoredUndoManager ();
    undoManager.addEdit (activePanelEdit);
  }

  protected void tearDown ()
  {
    panelManager.removeAllPanels ();
  }

  public void testPanelLocking ()
  {
    ActivePanelEdit.PANELS_UNLOCKED = true;
    assertEquals (0, panelManager.indexOfActivePanel ());
    panelManager.setActiveIndex (1);
    assertEquals (1, panelManager.indexOfActivePanel ());
    undoManager.undo ();
    assertEquals (0, panelManager.indexOfActivePanel ());
    undoManager.redo ();
    assertEquals (1, panelManager.indexOfActivePanel ());

    ActivePanelEdit.PANELS_UNLOCKED = false;

    undoManager.undo ();
    assertEquals (1, panelManager.indexOfActivePanel ());
    undoManager.redo ();
    assertEquals (1, panelManager.indexOfActivePanel ());

    panelManager.setActiveIndex (0);
    assertEquals (0, panelManager.indexOfActivePanel ());
  }

  public void testNullPanelManager ()
  {
    activePanelEdit = new ActivePanelEdit (null, 0, 1);
    activePanelEdit.undo ();
    activePanelEdit.redo ();
  }

  public void testInvalidIndices ()
  {
    Integer firstBadIndex  = new Integer (17);
    Integer secondBadIndex = new Integer (-1);

    activePanelEdit.setOldValue (firstBadIndex );
    activePanelEdit.setNewValue (secondBadIndex);

    activePanelEdit.undo ();
    activePanelEdit.redo ();

    assertNullName (firstBadIndex );
    assertNullName (secondBadIndex);
  }

  private void assertNullName (Object index)
  {
    assertEquals ("null panel", activePanelEdit.nameForPanelAt (index));
  }
}