package test.ui.panel_manager;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import dsto.dfc.swing.panels.Corner;
import dsto.dfc.swing.panels.FrameLike;
import dsto.dfc.swing.panels.GhostFrame;
import dsto.dfc.swing.panels.InsideFrame;
import dsto.dfc.swing.panels.OutsideFrame;
import dsto.dfc.swing.panels.PanelManager;
import dsto.dfc.swing.panels.PanelManagerFinder;
import dsto.dfc.swing.panels.ToolView;

public class TestToolView extends TestCase
{
  protected JFrame             window;
  protected PanelManager       pm;
  protected JPanel             jpOne, jpTwo, jpThree;
  protected PanelManager.Panel one,   two,   three;
  protected JButton            buttonOne, buttonTwo;
  protected ToolView           toolView;

  public TestToolView (String name)
  {
    super (name);
  }

  protected void setUp ()
  {
    window  = new JFrame ();
    pm      = new PanelManager ();
    jpOne   = new JPanel ();
    jpTwo   = new JPanel ();

    one     = pm.addPanel (jpOne);
    two     = pm.addPanel (jpTwo);

    buttonOne = new JButton ("one");
    buttonTwo = new JButton ("two");

    window.setSize (500, 500);
    window.getContentPane ().add (pm);

    toolView = one.addToolView (buttonOne);
  }

  protected void tearDown ()
  {
    window.dispose ();
    pm.removeAllPanels ();
  }

  public static Test suite ()
  {
    return new TestSuite (TestToolView.class);
  }

  public void testStateChangeInternal ()
  {
    toolView.changeState (PanelManager.FLOAT_INTERNAL);
    assertInsideFrameVisible ();
  }

  public void testStateChangeExternal ()
  {
    toolView.changeState (PanelManager.FLOAT_EXTERNAL);
    assertOutsideFrameVisible ();

    toolView.changeState (PanelManager.FIXED);
    assertInsideFrameVisible ();
  }

  public void testDefaultState ()
  {
    assertInsideFrameVisible ();
    toolView.setTool (buttonTwo);
    assertInsideFrameVisible ();
  }

  public void testActive ()
  {
    pm.setActivePanel (one);
    assertInsideFrameVisible ();

    pm.setActivePanel (two);
    assertToolViewIsNotVisible ();
  }

  public void testActivePanel ()
  {
    assertSame ( one, pm.getActivePanel ()  );
    assertTrue ( toolView.tool.isVisible () );
    assertInsideFrameVisible ();
  }

  public void testNonActivePanel ()
  {
    ToolView second  = two.addToolView (buttonTwo);

    assertOnlyFirstFrameIsVisible (toolView.insideFrame, second.insideFrame);

    pm.setActivePanel (two);
    assertSame (two, pm.getActivePanel ());

    assertOnlyFirstFrameIsVisible (second.insideFrame, toolView.insideFrame);
  }

  public void testMultipleTools ()
  {
    ToolView second = one.addToolView (buttonTwo);

    assertBothFramesAreVisible (toolView.insideFrame, second.insideFrame);

    pm.setActivePanel (two);
    assertSame ( two, pm.getActivePanel () );

    assertNeitherFrameIsVisible (toolView.insideFrame, second.insideFrame);
  }

  public void testAddToolToNonActive ()
  {
    ToolView first  = two.addToolView (buttonOne);
    ToolView second = two.addToolView (buttonTwo);

    assertNeitherFrameIsVisible (first.insideFrame, second.insideFrame);
  }

  public void testAddToolToActive ()
  {
    ToolView second = one.addToolView (buttonTwo);
    ToolView third  = one.addToolView (buttonOne);
    assertBothFramesAreVisible (second.insideFrame, third.insideFrame);
  }

  public void testTitle ()
  {
    String title = "junit - simplicity";

    assertEquals ( "", toolView.insideFrame .getTitle() );
    assertEquals ( "", toolView.outsideFrame.getTitle() );

    toolView.setTitle (title);

    assertEquals ( title, toolView.insideFrame .getTitle() );
    assertEquals ( title, toolView.outsideFrame.getTitle() );
  }

  public void testIcon ()
  {
    toolView.setIcon ( one.getIcon() );

    assertSame ( one.getIcon (), toolView.getIcon () );
    assertSame ( one.getIcon (), toolView.insideFrame.getIcon () );
  }

  public void testRemove ()
  {
    assertEquals (1, one.getToolViewCount ());
    one.removeToolView (toolView);
    toolView.dispose ();
    assertEquals (0, one.getToolViewCount ());
    one.removeToolView (toolView);
    assertEquals (0, one.getToolViewCount ());
  }

  public void testStateSynchronization ()
  {
    one.setFloating (PanelManager.FLOAT_EXTERNAL);
    ToolView newToolView = one.addToolView (buttonTwo);
    assertTrue (newToolView.outsideFrame.isVisible ());
    assertEquals (GhostFrame.class, newToolView.insideFrame.getClass ());
    one.setActive (true);
    assertEquals (GhostFrame.class, newToolView.insideFrame.getClass ());
    one.setFloating (PanelManager.FIXED);
    assertEquals (InsideFrame.class, newToolView.insideFrame.getClass ());
    assertTrue (newToolView.insideFrame.isVisible ());
    one.setFloating (PanelManager.FLOAT_EXTERNAL);
    assertTrue ( ! newToolView.insideFrame.isVisible() );
  }

  public void testDisable ()
  {
    one.setFloating (PanelManager.FLOAT_EXTERNAL);
    assertOutsideFrameVisible ();
    toolView.setVisible (false);
    assertToolViewIsNotVisible ();
    pm.setActivePanel (one);
    assertToolViewIsNotVisible ();
    toolView.changeState (PanelManager.FIXED);
    assertToolViewIsNotVisible ();
    toolView.setVisible (true);
    assertInsideFrameVisible ();
    toolView.changeState (PanelManager.FLOAT_EXTERNAL);
    assertOutsideFrameVisible ();
  }

  public void testStaticToolView ()
  {
    assertNotNull (PanelManagerFinder.addToolView (jpOne, jpTwo));
  }

  public void testStaticToolViewWithNoPanelManager ()
  {
    Frame  dummyFrame  = new Frame  ();
    JFrame dummyJFrame = new JFrame ();
    ToolView toolWindow;

    dummyFrame                   .add (buttonOne);
    dummyJFrame.getContentPane ().add (buttonTwo);

    toolWindow = PanelManagerFinder.addToolView (jpOne, new JButton ());
    assertNotNull (toolWindow);
    toolWindow.dispose ();

    toolWindow = PanelManagerFinder.addToolView (jpOne, buttonOne     );
    assertNotNull (toolWindow);
    toolWindow.dispose ();

    toolWindow = PanelManagerFinder.addToolView (jpOne, buttonTwo     );
    assertNotNull (toolWindow);
    toolWindow.dispose ();

    dummyFrame .dispose ();
    dummyJFrame.dispose ();
  }

  public void testEnable ()
  {
    assertInsideFrameVisible ();
    toolView.setVisible (false);
    assertToolViewIsNotVisible ();
    toolView.setVisible (true);
    assertInsideFrameVisible ();
  }

  public void testClose ()
  {
    assertEquals (ToolView.HIDE_ON_CLOSE,    JFrame.HIDE_ON_CLOSE   );
    assertEquals (ToolView.DISPOSE_ON_CLOSE, JFrame.DISPOSE_ON_CLOSE);

    int insideFrameClose =  getInsideFrame ().getDefaultCloseOperation ();
    int outsideFrameClose = toolView.outsideFrame.getDefaultCloseOperation ();

    assertEquals (ToolView.HIDE_ON_CLOSE, insideFrameClose );
    assertEquals (ToolView.HIDE_ON_CLOSE, outsideFrameClose);

    toolView.setDefaultCloseOperation (ToolView.DISPOSE_ON_CLOSE);

    insideFrameClose  = getInsideFrame ()    .getDefaultCloseOperation ();
    outsideFrameClose = toolView.outsideFrame.getDefaultCloseOperation ();

    assertEquals (ToolView.DISPOSE_ON_CLOSE, insideFrameClose );
    assertEquals (ToolView.DISPOSE_ON_CLOSE, outsideFrameClose);

    one.setFloating (PanelManager.FLOAT_EXTERNAL);

    outsideFrameClose = getOutsideFrame ().getDefaultCloseOperation ();
    assertEquals (ToolView.DISPOSE_ON_CLOSE, outsideFrameClose);

    toolView.setDefaultCloseOperation (ToolView.HIDE_ON_CLOSE);

    insideFrameClose  =  getInsideFrame  () .getDefaultCloseOperation ();
    outsideFrameClose =  getOutsideFrame () .getDefaultCloseOperation ();

    assertEquals (ToolView.HIDE_ON_CLOSE,  insideFrameClose );
    assertEquals (ToolView.HIDE_ON_CLOSE,  outsideFrameClose);
  }

  public void testListener ()
  {
    DummyListener listener = new DummyListener ();
    assertTrue ( ! listener.heard );
    toolView.addPropertyChangeListener (listener);
    assertTrue ( ! listener.heard );
    pm.setActivePanel (one);
    toolView.changeState (PanelManager.FLOAT_EXTERNAL);
    assertTrue ( ! listener.heard );

    toolView.setVisible (true);
    assertTrue (listener.heard);
    listener.heard = false;
    toolView.setVisible (false);
    assertTrue (listener.heard);
    listener.heard = false;
    toolView.removePropertyChangeListener (listener);
    toolView.setVisible (false);
    assertTrue ( ! listener.heard );
  }

  public void testNullToolView ()
  {
    ToolView empty = new ToolView (null, buttonOne, null, null);
    assertNotNull (empty);
  }

  public void testToolTransferAcrossFrames ()
  {
    toolView.setTool (buttonTwo);

    assertSame ( buttonTwo, toolView.tool );
    assertEquals (1, getInsideFrame ().getContentPane ().getComponentCount ());
    assertNull( toolView.outsideFrame.getTool () );

    toolView.changeState (PanelManager.FLOAT_EXTERNAL);
    assertEquals ( 0, getInsideFrame ().getContentPane ().getComponentCount ());
    assertSame ( toolView.outsideFrame, toolView.stateFrame );

    toolView.setTool (buttonOne);
    assertEquals (0, getInsideFrame  ().getContentPane ().getComponentCount ());
    assertEquals (1, getOutsideFrame ().getContentPane() .getComponentCount ());
    assertEquals (PanelManager.FLOAT_EXTERNAL, toolView.state);
  }

  public void testLazyInitialization ()
  {
    ToolView virgin = new ToolView (one, buttonOne, null, null);
    assertEquals ( GhostFrame.class, virgin.insideFrame .getClass () );
    assertEquals ( GhostFrame.class, virgin.outsideFrame.getClass () );

    assertEquals (PanelManager.FLOAT_INTERNAL, virgin.state);

    virgin.setVisible (true);

    assertEquals ( InsideFrame.class, virgin.insideFrame .getClass () );
    assertEquals ( GhostFrame .class, virgin.outsideFrame.getClass () );

    virgin.changeState (PanelManager.FLOAT_EXTERNAL);

    assertEquals ( OutsideFrame .class, virgin.outsideFrame.getClass () );

    virgin.dispose ();
  }

  public void testSizing ()
  {
    ToolView secondTool = one.addToolView (buttonTwo);
    secondTool.changeState (PanelManager.FLOAT_EXTERNAL);
    assertEquals ( 2, one.getToolViewCount () );

    Dimension correctSize = new Dimension ( 65, 61 );
    Dimension actualSize = ((InsideFrame) secondTool.insideFrame ).getSize ();
    assertEquals ( correctSize, actualSize );
  }

  public void testLocation ()
  {
    ToolView tool = new ToolView (one, buttonTwo, window, window);
    tool.putInCorner (Corner.TOP_RIGHT, 2);
    tool.setVisible (true);
    Point correctPlace = new Point (357, 56);
    assertEquals ( correctPlace, getInsideFrame (tool).getLocation () );

    tool.changeState (PanelManager.FLOAT_EXTERNAL);
    correctPlace = new Point (318, 56);
    assertEquals( correctPlace, getOutsideFrame (tool).getLocation () );
  }

  public void testDoubleDispose ()
  {
    toolView.dispose ();
    toolView.dispose ();
  }

  private void assertInsideFrameVisible ()
  {
    assertOnlyFirstFrameIsVisible (toolView.insideFrame, toolView.outsideFrame);
  }

  private void assertOutsideFrameVisible ()
  {
    assertOnlyFirstFrameIsVisible (toolView.outsideFrame, toolView.insideFrame);
  }

  private void assertOnlyFirstFrameIsVisible (FrameLike first, FrameLike second)
  {
    assertTrue (   first .isVisible () );
    assertTrue ( ! second.isVisible () );
  }

  private void assertNeitherFrameIsVisible (FrameLike first, FrameLike second)
  {
    assertTrue ( ! first .isVisible () );
    assertTrue ( ! second.isVisible () );
  }

  private void assertBothFramesAreVisible (FrameLike first, FrameLike second)
  {
    assertTrue ( first .isVisible () );
    assertTrue ( second.isVisible () );
  }

  private void assertToolViewIsNotVisible ()
  {
    assertNeitherFrameIsVisible (toolView.insideFrame, toolView.outsideFrame);
  }

  private JInternalFrame getInsideFrame ()
  {
    return getInsideFrame (toolView);
  }

  private JDialog getOutsideFrame ()
  {
    return getOutsideFrame (toolView);
  }

  private JInternalFrame getInsideFrame (ToolView theView)
  {
    return (InsideFrame) theView.insideFrame ;
  }

  private JDialog getOutsideFrame (ToolView theView)
  {
    return (OutsideFrame) theView.outsideFrame;
  }

  class DummyListener implements PropertyChangeListener
  {
    public boolean heard = false;
    public void propertyChange(PropertyChangeEvent evt)
    {
      if ( evt.getPropertyName().equals( "visible" ) )
        heard = true;
    }
  }

  } // ToolViewTest