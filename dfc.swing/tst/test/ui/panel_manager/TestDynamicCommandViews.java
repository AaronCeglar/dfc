package test.ui.panel_manager;

import javax.swing.JButton;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import dsto.dfc.swing.commands.Command;
import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.swing.panels.CmdFloatPanel;
import dsto.dfc.swing.panels.DynamicCommandViews;
import dsto.dfc.swing.panels.PanelManager;

public class TestDynamicCommandViews extends TestCase
{
  DynamicCommandViews menuMover;
  PanelManager        manager;
  JButton             buttonOne, buttonTwo;
  CommandView         mainMenu, contextMenu, toolbarMenu;

  public TestDynamicCommandViews (String name)
  {
    super (name);
  }

  protected void setUp ()
  {
    manager     = new PanelManager ();
    menuMover   = new DynamicCommandViews  (manager);

    mainMenu    = menuMover.getCommandView (CommandView.MAIN_MENU_VIEW   );
    contextMenu = menuMover.getCommandView (CommandView.CONTEXT_MENU_VIEW);
    toolbarMenu = menuMover.getCommandView (CommandView.TOOLBAR_VIEW     );

    buttonOne   = new JButton ();
    buttonTwo   = new JButton ();
  }

  protected void tearDown() {

    manager.removeAllPanels ();
  }

  public static Test suite() {

    return new TestSuite (TestDynamicCommandViews.class);
  }

  public void testMenusExist ()
  {
   assertNotNull (mainMenu   );
   assertNotNull (contextMenu);
   assertNotNull (toolbarMenu);
  }

  public void testDefaultMenuCommands ()
  {
    assertMenusAreEmpty ();
  }

  public void testActivePanelFloated ()
  {
    PanelManager.Panel panel = manager.addPanel (new DummyMenu ());

    assertEntryCountIs (4); // 3 commands + dummyMenu Command View...

    panel.setFloating (PanelManager.FLOAT_INTERNAL);

    assertEntryCountIs (4);
  }

  public void testActivePanelUnFloated ()
  {
    PanelManager.Panel panel = manager.addPanel (buttonOne);

    assertCommandsMatchFixedState ();

    panel.setFloating (PanelManager.FLOAT_EXTERNAL);

    assertCommandsMatchFloatState ();
    assertEntryCountIs (3);

    panel.setFloating (PanelManager.FIXED);

    assertCommandsMatchFixedState ();
    assertEntryCountIs (3);
  }

  public void testActivePanelChanged ()
  {
    PanelManager.Panel panelA = manager.addPanel (buttonOne);
    PanelManager.Panel panelB = manager.addPanel (buttonTwo);

    panelA.setFloating (PanelManager.FLOAT_EXTERNAL);

    assertEntryCountIs (3);
    assertCommandsMatchFloatState ();

    panelA.setFloating (PanelManager.FIXED);

    assertCommandsMatchFixedState ();

    panelA.setFloating (PanelManager.FLOAT_EXTERNAL);
    manager.setActivePanel (panelB);

    assertEntryCountIs (3);
    assertCommandsMatchFixedState ();
  }

  public void testNoActivePanel ()
  {
    assertMenusAreEmpty ();

    manager.addPanel (buttonOne);

    assertNotNull (findMenuCommand (mainMenu, "Float Internal"));
    assertNotNull (findMenuCommand (mainMenu, "Float External"));
    assertNotNull (mainMenu.findCommand ( "view.Close"        ));
    assertNull    (findMenuCommand (mainMenu, "Unfloat"       ));

    assertCommandsMatchFixedState ();

    manager.setActivePanel (null);

    assertMenusAreEmpty ();
  }

  public void testRemoveLastFloatingPanel ()
  {
   PanelManager.Panel panel = manager.addPanel (buttonOne);
   panel.setFloating (PanelManager.FLOAT_INTERNAL);
   manager.removePanel (panel);

   assertMenusAreEmpty ();
  }

  public void testComponentMenus ()
  {
    PanelManager.Panel panel = manager.addPanel (buttonOne);

    assertEntryCountIs (3);
    assertTrue (mainMenu.containsView (panel.getCommandView (CommandView.MAIN_MENU_VIEW)));

    manager.removePanel (panel);

    assertMenusAreEmpty ();
    assertTrue (! mainMenu.containsView (panel.getCommandView (CommandView.MAIN_MENU_VIEW)));

    panel = manager.addPanel (new DummyMenu ());

    assertTrue (panel.isActive ());
    assertEntryCountIs (4);

    CommandView panelMainMenu    = panel.getCommandView (CommandView.MAIN_MENU_VIEW   );
    CommandView panelContextMenu = panel.getCommandView (CommandView.CONTEXT_MENU_VIEW);

    assertTrue (mainMenu   .containsView (panelMainMenu   ));
    assertTrue (contextMenu.containsView (panelContextMenu));

    panel = manager.addPanel (buttonTwo);
    manager.setActivePanel   (panel);

    assertTrue (! mainMenu   .containsView( panelMainMenu   ));
    assertTrue (! contextMenu.containsView( panelContextMenu));

    assertTrue (mainMenu.containsView (panel.getCommandView (CommandView.MAIN_MENU_VIEW)));

    assertEntryCountIs (3);
  }

public void testPanelMenus ()
{
  PanelManager.Panel panel = manager.addPanel (new DummyMenu ());

  assertEquals (2, getPanelMenuEntries(panel, CommandView.MAIN_MENU_VIEW   ));
  assertEquals (2, getPanelMenuEntries(panel, CommandView.CONTEXT_MENU_VIEW));
  assertEquals (1, getPanelMenuEntries(panel, CommandView.TOOLBAR_VIEW     ));
}

public void testMultipleAdditionOfSameCommand ()
{
   Command unFloatA = new CmdFloatPanel (null, "UnFloat");
   Command unFloatB = new CmdFloatPanel (null, "UnFloat");

   PanelManager.Panel panel = manager.addPanel (new DummyMenu());
   panel.getCommandView (CommandView.CONTEXT_MENU_VIEW);
   contextMenu.addCommand (unFloatA);
   contextMenu.addCommand (unFloatA);
   contextMenu.addCommand (unFloatB);
   contextMenu.addCommand (unFloatB);
   panel.setFloating (PanelManager.FLOAT_INTERNAL);
   contextMenu.addCommand (unFloatB);
   contextMenu.addCommand (unFloatB);
}

private int getPanelMenuEntries (PanelManager.Panel panel, String menuName)
{
  return panel.getCommandView (menuName).getView (3).getEntryCount ();
}

private CommandView getContextSubMenu ()
{
  return contextMenu.getView (0);
}

private CommandView getMainSubMenu ()
{
  return mainMenu.getView (0);
}

private void assertEntryCountIs (int entryCount)
{
  assertEntryCountIs (entryCount, getMainSubMenu    ());
  assertEntryCountIs (entryCount, getContextSubMenu ());
  assertEntryCountIs (entryCount, toolbarMenu.getView (0));
}

private void assertEntryCountIs (int entryCount, CommandView menu)
{
  assertEquals (entryCount, menu.getEntryCount ());
}

private Command findMenuCommand (CommandView menu, String commandName)
{
  return menu.findCommand ("view.Toggle " + commandName);
}

private void assertCommandPresent (String commandName)
{
  assertNotNull (findMenuCommand (getMainSubMenu    (), commandName));
  assertNotNull (findMenuCommand (getContextSubMenu (), commandName));
}

private void assertCommandAbsent (String commandName)
{
  assertNull (findMenuCommand (getMainSubMenu    (), commandName));
  assertNull (findMenuCommand (getContextSubMenu (), commandName));
}

private void assertMenusAreEmpty ()
{
  assertEntryCountIs (0, mainMenu   );
  assertEntryCountIs (0, contextMenu);
  assertEntryCountIs (0, toolbarMenu);
}

private void assertCommandsMatchFloatState ()
{
  assertCommandPresent ("Unfloat"       );
  assertCommandPresent ("Float Internal");
  assertCommandAbsent  ("Float External");

  assertDefaultCommandNameIs ("Unfloat");
}

private void assertCommandsMatchFixedState ()
{
  assertCommandPresent ("Float Internal");
  assertCommandPresent ("Float External");
  assertCommandAbsent  ("Unfloat"       );

  assertDefaultCommandNameIs ("Float Internal");
}

private void assertDefaultCommandNameIs (String correctName)
{
  String defaultCommandName = contextMenu.getDefaultCommand ().getName ();
  assertTrue (defaultCommandName.endsWith (correctName));
}
}