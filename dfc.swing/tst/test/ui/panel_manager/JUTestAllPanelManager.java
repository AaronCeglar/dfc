package test.ui.panel_manager;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class JUTestAllPanelManager extends TestCase
{
  public JUTestAllPanelManager (String name)
  {
    super (name);
  }

  public static Test suite ()
  {
    TestSuite suite = new TestSuite ("test all PanelManager classes");

    suite.addTest ( new TestSuite ( TestPanelManager.class        ) );
    suite.addTest ( new TestSuite ( TestDynamicCommandViews.class ) );
    suite.addTest ( new TestSuite ( TestClientCommandViews.class  ) );
    suite.addTest ( new TestSuite ( TestToolView.class            ) );
    suite.addTest ( new TestSuite ( TestCorner.class              ) );
    suite.addTest ( new TestSuite ( TestHidePolicy.class          ) );
    suite.addTest ( new TestSuite ( TestEditEvents.class          ) );
    suite.addTest ( new TestSuite ( JUTestActivePanelEdit.class   ) );
    suite.addTest ( new TestSuite ( TestSizeCalculator.class      ) );

    return suite;
  }
}