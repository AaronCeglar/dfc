package test.ui.panel_manager;

import javax.swing.JButton;

import junit.framework.TestCase;

import dsto.dfc.swing.panels.HidePolicy;
import dsto.dfc.swing.panels.PanelManager;

public class TestHidePolicy extends TestCase
{

protected HidePolicy hidePolicy;
protected PanelManager manager;

public TestHidePolicy (String name)
{
  super (name);
}

protected void setUp()
{
  hidePolicy  = new HidePolicy ();
  manager     = hidePolicy.manager;
}

protected void tearDown ()
{
  manager.removeAllPanels ();
}

public void testNoPanels ()
{
  assertTrue ( manager.isVisible () );
  hidePolicy.neverHide ();
  assertTrue ( manager.isVisible () );
  hidePolicy.hideWhenEmpty ();
  assertTrue ( ! manager.isVisible () );
  hidePolicy.neverHide ();
  assertTrue ( manager.isVisible () );
}

public void testRemoveLast ()
{
  manager.addPanel (new JButton ());
  hidePolicy.hideWhenEmpty ();
  assertTrue ( manager.isVisible () );
  manager.removeAllPanels ();
  assertTrue ( ! manager.isVisible () );
}

public void testRemoveLastNoHiding ()
{
  manager.addPanel (new JButton ());
  hidePolicy.neverHide ();
  assertTrue ( manager.isVisible () );
  manager.removeAllPanels ();
  assertTrue ( manager.isVisible () );
}

public void testAddFirst ()
{
  hidePolicy.hideWhenEmpty ();
  assertTrue ( ! manager.isVisible () );
  manager.addPanel (new JButton ());
  assertTrue ( manager.isVisible () );
  manager.removeAllPanels ();
  assertTrue ( ! manager.isVisible () );
}

public void testAddFirstNoHiding ()
{
  hidePolicy.neverHide ();
  assertTrue ( manager.isVisible () );
  manager.addPanel (new JButton ());
  assertTrue ( manager.isVisible () );
  manager.removeAllPanels ();
  assertTrue ( manager.isVisible () );
}

public void testAddExtra ()
{
  hidePolicy.hideWhenEmpty ();
  assertTrue ( ! manager.isVisible () );
  manager.addPanel (new JButton ());
  assertTrue ( manager.isVisible () );
  manager.addPanel (new JButton ());
  assertTrue ( manager.isVisible () );
  manager.removePanel (manager.getPanelAt (1));
  assertTrue ( manager.isVisible () );
  manager.removePanel (manager.getPanelAt (0));
  assertTrue ( ! manager.isVisible () );
}
}