package test.ui.panel_manager;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.swing.panels.ClientCommandViews;

public class TestClientCommandViews extends TestCase
{
  ClientCommandViews menuMover;
  DummyMenu menu;
  CommandView dummyContextMenu;
  CommandView dummyToolbarMenu;

  CommandView mainMenu;
  CommandView contextMenu;
  CommandView toolbarMenu;

	public TestClientCommandViews (String name)
  {
		super (name);
	}

  public static Test suite ()
  {
		return new TestSuite (TestClientCommandViews.class);
	}

	protected void setUp ()
  {
    menuMover        = new ClientCommandViews ();
    menu             = new DummyMenu          ();
    dummyContextMenu = menu.getCommandView (CommandView.CONTEXT_MENU_VIEW);
    dummyToolbarMenu = menu.getCommandView (CommandView.TOOLBAR_VIEW     );

    mainMenu         = menuMover.getCommandView (CommandView.MAIN_MENU_VIEW   );
    contextMenu      = menuMover.getCommandView (CommandView.CONTEXT_MENU_VIEW);
    toolbarMenu      = menuMover.getCommandView (CommandView.TOOLBAR_VIEW     );
	}

  public void testNoClient ()
  {
    assertAllMenusEmpty ();
  }

  public void testSingleClient ()
  {
    assertEquals ( 0, toolbarMenu.getEntryCount () );
    menuMover.setClient (menu);
    assertEquals ( 2, mainMenu.getView (0).getEntryCount ());
    assertEquals ( 1, toolbarMenu         .getEntryCount ());
    assertTrue (   toolbarMenu.containsView (dummyToolbarMenu) );
    assertTrue ( ! toolbarMenu.containsView (dummyContextMenu) );
  }

  public void testMultipleClients ()
  {
    menuMover.setClient (menu);
    menuMover.setClient (new DummyMenu ());
    assertEquals ( 2, mainMenu.getView (0).getEntryCount ());
    assertEquals ( 1, toolbarMenu         .getEntryCount ());
    assertTrue( ! toolbarMenu.containsView  (dummyToolbarMenu) );
    assertTrue( ! contextMenu.containsView  (dummyContextMenu) );
  }

  public void testRemoveClient ()
  {
    menuMover.setClient  (menu);
    assertEquals ( 1, toolbarMenu           .getEntryCount() );
    assertEquals ( 2, contextMenu.getView(0).getEntryCount() );

    menuMover.removeClientCommands();
    assertAllMenusEmpty ();
    assertTrue ( ! contextMenu.containsView (dummyContextMenu) );

    menuMover.setClient  (menu);
    menuMover.setClient  (null);
    assertAllMenusEmpty ();
    assertTrue ( ! contextMenu.containsView (dummyContextMenu) );
  }

  public void testRemoveBadClient ()
  {
    menuMover.removeClientCommands ();
    menuMover.setClient (menu);
    menuMover.setClient (menu);
    menuMover.removeClientCommands ();
    menuMover.removeClientCommands ();

    assertAllMenusEmpty ();
    assertTrue ( ! contextMenu.containsView (dummyContextMenu));
  }

  public void testNestedViewRemoval ()
  {
     CommandView nestedOne = new CommandView (CommandView.MAIN_MENU_VIEW);
     CommandView nestedTwo = new CommandView (CommandView.MAIN_MENU_VIEW);

     nestedOne.addView (nestedTwo);
     menu.getCommandView (CommandView.MAIN_MENU_VIEW).addView (nestedOne);

     menuMover.setClient (menu);
     assertTrue (mainMenu.getView (0).containsView (nestedOne));

     menuMover.setClient (null);
     assertEquals( 0, mainMenu.getEntryCount () );
  }

  public void assertAllMenusEmpty ()
  {
    assertEquals ( 0, toolbarMenu.getEntryCount () );
    assertEquals ( 0, contextMenu.getEntryCount () );
    assertEquals ( 0, mainMenu   .getEntryCount () );
  }
}