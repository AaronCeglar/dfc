package test.ui.panel_manager;

import java.awt.Dimension;

import javax.swing.JButton;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import dsto.dfc.swing.panels.PanelManager;
import dsto.dfc.swing.panels.SizeCalculator;

public class TestSizeCalculator extends TestCase
{
  SizeCalculator sizeCalculator;
  PanelManager   panelManager;

  final int biggestMinimumSize   = 30;
  final int biggestPreferredSize = 300;

  public TestSizeCalculator (String name)
  {
    super (name);
  }

  public static Test suite ()
  {
    return new TestSuite (TestSizeCalculator.class);
  }

  public void setUp ()
  {
    panelManager     = new PanelManager ();
    sizeCalculator   = new SizeCalculator (panelManager);

    addPanelOfSize (10, 100);
    addPanelOfSize (biggestMinimumSize, biggestPreferredSize);
    addPanelOfSize (20, 200);
    addPanelOfSize (0,  0  );
  }

  public void tearDown ()
  {
    panelManager.removeAllPanels ();
  }

  public void testMinimumSize ()
  {
    Dimension largestMinimumSize = enlargeByInsets (biggestMinimumSize);
    assertEquals (largestMinimumSize, sizeCalculator.getMinimumSizeOfLargestPanel ());
  }

  public void testPreferredSize ()
  {
    Dimension largestPreferredSize = enlargeByInsets (biggestPreferredSize);
    assertEquals (largestPreferredSize, sizeCalculator.getPreferredSizeOfLargestPanel ());
  }

  private Dimension enlargeByInsets (int correctRawSize)
  {
    Dimension insets = SizeCalculator.getTabPaneInsets ();

    int widthAdjustment  = (int) insets.getWidth  ();
    int heightAdjustment = (int) insets.getHeight ();

    int correctedWidth  = correctRawSize + widthAdjustment;
    int correctedHeight = correctRawSize + heightAdjustment;

    return new Dimension (correctedWidth, correctedHeight);
  }

  private void addPanelOfSize (int minimumSize,  int preferredSize)
  {
    JButton button = new JButton ();

    button.setMinimumSize   (new Dimension (minimumSize,   minimumSize));
    button.setPreferredSize (new Dimension (preferredSize, preferredSize));

    panelManager.addPanel (button);
  }
}