package test.ui.panel_manager;

import javax.swing.JPanel;

import dsto.dfc.swing.commands.Command;
import dsto.dfc.swing.commands.CommandSource;
import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.swing.panels.CmdFloatPanel;
import dsto.dfc.swing.panels.StandardCommandViews;

public class DummyMenu extends JPanel implements CommandSource
{
  CommandSource commandViews = new StandardCommandViews();

  public DummyMenu()
  {
    Command cmdUnFloat = new CmdFloatPanel( null, "UnFloat" );
    Command cmdFloat   = new CmdFloatPanel( null, "Float Internal" );

    getCommandView( CommandView.CONTEXT_MENU_VIEW ).addCommand( cmdUnFloat );
    getCommandView( CommandView.CONTEXT_MENU_VIEW ).addCommand( cmdFloat   );

    getCommandView( CommandView.MAIN_MENU_VIEW    ).addCommand( cmdUnFloat );
    getCommandView( CommandView.MAIN_MENU_VIEW    ).addCommand( cmdFloat   );

    getCommandView( CommandView.TOOLBAR_VIEW     ).addCommand( cmdFloat   );
  }

  public CommandView getCommandView (String viewName)
  {
     return commandViews.getCommandView( viewName );
  }
}