package test.ui.panel_manager;

import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import test.ui.monitor.EditEventSource;

import dsto.dfc.swing.panels.PanelManager;
import dsto.dfc.util.Beans;

public class TestEditEvents extends TestCase implements UndoableEditListener
{
  PanelManager    manager;
  EditEventSource editEventSourceA;
  EditEventSource editEventSourceB;

  boolean      eventFiredFromA;
  boolean      eventFiredFromB;

  public TestEditEvents (String name)
  {
    super (name);
  }

  public static Test suite ()
  {
    return new TestSuite (TestEditEvents.class);
  }

  protected void setUp ()
  {
    manager          = new PanelManager         ();
    editEventSourceA = new EditEventSource ();
    editEventSourceB = new EditEventSource ();
    eventFiredFromA  = false;
    eventFiredFromB  = false;
  }

  protected void tearDown ()
  {
    manager.removeAllPanels ();
  }

  public void testBeanUtility ()
  {
    assertTrue (Beans.addListener    (UndoableEditListener.class, editEventSourceA, this));
    assertTrue (Beans.removeListener (UndoableEditListener.class, editEventSourceA, this));
  }

  public void testEditEventFiring ()
  {
    manager.addPanel (editEventSourceA);
    manager.addUndoableEditListener (this);
    editEventSourceA.fire ();
    assertTrue (eventFiredFromA);
  }

  public void testMultipleEditEventSources ()
  {
    manager.addPanel (editEventSourceA);
    manager.addPanel (editEventSourceB);
    manager.addUndoableEditListener (this);

    editEventSourceA.fire ();

    assertTrue (  eventFiredFromA);
    assertTrue (! eventFiredFromB);

    editEventSourceB.fire ();

    assertTrue (eventFiredFromA);
    assertTrue (eventFiredFromB);
  }

  public void testStopEditEventNotification ()
  {
    manager.addPanel (editEventSourceA);
    manager.addPanel (editEventSourceA);
    manager.addUndoableEditListener (this);
    manager.removeAllPanels ();

    editEventSourceA.fire ();
    editEventSourceB.fire ();
    editEventSourceA.fire ();

    assertTrue ( ! eventFiredFromA);
    assertTrue ( ! eventFiredFromB);
  }

  public void testStopListening ()
  {
    manager.addPanel (editEventSourceA);
    manager.addPanel (editEventSourceA);
    manager.addUndoableEditListener    (this);
    manager.removeUndoableEditListener (this);

    editEventSourceA.fire ();
    editEventSourceB.fire ();
    editEventSourceA.fire ();

    assertTrue ( ! eventFiredFromA);
    assertTrue ( ! eventFiredFromB);
  }

  public void undoableEditHappened(UndoableEditEvent e)
  {
    if ( eventFiredFromA )
      eventFiredFromB = true;
    else
      eventFiredFromA = true;
  }

  }