package test.ui;

import java.awt.BorderLayout;

import dsto.dfc.swing.controls.ColorChooserCombo;
import dsto.dfc.test.TestFrame;

/**
 * Puts the dsto.dfc.swing.controls.color_chooser.ColorChooserCombo in a TestFrame.
 *
 * @author       Luke Marsh
 * @version      $Revision$
 */
public class TestPopupMenu extends TestFrame
{

  private ColorChooserCombo colorChooserCombo;

  /**
   * Initializes Variables and calls jbInit ();
   */
  public TestPopupMenu ()
  {
    try
    {
      colorChooserCombo = new ColorChooserCombo ();
      jbInit ();
    }
    catch (Exception e)
    {
      e.printStackTrace ();
    }
  }


  /**
   * Creates and show frame.
   */
  public static void main (String[] args)
  {
    TestPopupMenu testPopupMenu = new TestPopupMenu ();
    testPopupMenu.pack ();
    testPopupMenu.setVisible (true);
  }


  /**
   * Adds Components;
   */
  private void jbInit () throws Exception
  {
    this.getContentPane ().add (colorChooserCombo, BorderLayout.CENTER);
  }
}