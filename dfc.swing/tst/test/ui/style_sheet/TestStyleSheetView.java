package test.ui.style_sheet;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import dsto.dfc.swing.styles.AbstractStyle;
import dsto.dfc.swing.styles.StyleSheet;
import dsto.dfc.swing.styles.StyleSheetTableView;
import dsto.dfc.test.TestFrame;
import dsto.dfc.util.EnumerationValue;

/**
 * Title:        DFC main project
 * Description:
 * Copyright:    Copyright (c) 1999
 * Company:      ITD/DSTO
 * @author Matthew Phillips
 * @version $Revision$
 */

public class TestStyleSheetView extends TestFrame
{
  public static final TestStyle FILL_COLOUR;
  public static final TestStyle TEXT_FONT;
  public static final TestStyle TEXT_COLOUR;
  public static final TestStyle TEXT_SIZE;

  public static final TestStyle [] STYLES;

  public static final StyleSheet BASE_SHEET;
  JPanel jPanel1 = new JPanel();
  JScrollPane jScrollPane1 = new JScrollPane();
  StyleSheetTableView styleSheetTableView1 = new StyleSheetTableView(BASE_SHEET, new StyleSheet (STYLES));
  BorderLayout borderLayout1 = new BorderLayout();

  static
  {
    FILL_COLOUR =
      new TestStyle ("Fill Colour", "fill", Color.class,
                     "Fill colour for the shape",
                     "/dsto/dfc/icons/bucket_fill.gif");

    TEXT_FONT =
      new TestStyle ("Text Font", "font", Font.class,
                     "Font for text in shape",
                     "/dsto/dfc/icons/font.gif");

    TEXT_COLOUR =
      new TestStyle ("Text Colour", "text colour", Color.class,
                     "Colour for text in shape",
                     "/dsto/dfc/icons/text_colour.gif");

    TEXT_SIZE =
      new TestStyle ("Text Size", "text size", Integer.class,
                     "Size of text in shape", null);

    STYLES = new TestStyle []
    {
      FILL_COLOUR, TEXT_FONT, TEXT_COLOUR, TEXT_SIZE
    };

    BASE_SHEET = new StyleSheet (STYLES);

    BASE_SHEET.put (FILL_COLOUR, Color.black);
    BASE_SHEET.put (TEXT_COLOUR, Color.blue);
    BASE_SHEET.put (TEXT_FONT, new Font ("Dialog", Font.PLAIN, 12));
    BASE_SHEET.put (TEXT_SIZE, new Integer (12));
  }


  public TestStyleSheetView()
  {
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }

//    RepaintManager repaintManager = RepaintManager.currentManager(styleSheetTableView1);
//    repaintManager.setDoubleBufferingEnabled(false);
//    styleSheetTableView1.setDebugGraphicsOptions (DebugGraphics.FLASH_OPTION);
  }

  public void run ()
  {
    pack ();
    setVisible (true);
  }

  public static void main (String[] args)
  {
    TestStyleSheetView app = new TestStyleSheetView();
    app.run ();
  }

  private void jbInit() throws Exception
  {
    jPanel1.setLayout(borderLayout1);
    jPanel1.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
    this.getContentPane().add(jPanel1, BorderLayout.CENTER);
    jPanel1.add(jScrollPane1, BorderLayout.CENTER);
    jScrollPane1.getViewport ().add (styleSheetTableView1, null);
  }

  public static class TestStyle extends AbstractStyle
  {
    public TestStyle ()
    {
      // zip
    }

    public TestStyle (String name, String text, Class type,
                      String description, String iconResource)
    {
      super (name, text, type, description, iconResource);
    }

    public EnumerationValue [] getEnumValues ()
    {
      return STYLES;
    }
  }
}