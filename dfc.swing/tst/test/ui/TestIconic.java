package test.ui;

import java.io.Serializable;

import javax.swing.Icon;

import dsto.dfc.swing.icons.Iconic;
import dsto.dfc.swing.icons.ImageLoader;
import dsto.dfc.util.Copyable;

public class TestIconic implements Iconic, Copyable, Serializable
{
  private String name;
  private Icon icon;

  public TestIconic (String name, String iconName)
  {
    this.name = name;
    this.icon = ImageLoader.getIcon ("/dsto/dfc/icons/" + iconName);
  }

  public Icon getIcon ()
  {
    return icon;
  }

  public String getName ()
  {
    return name;
  }

  public Icon getLargeIcon()
  {
    return null;
  }

  public Object clone () throws CloneNotSupportedException
  {
    return super.clone ();
  }
}