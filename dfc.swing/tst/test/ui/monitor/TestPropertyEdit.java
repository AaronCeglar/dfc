package test.ui.monitor;

import java.beans.PropertyChangeEvent;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import dsto.dfc.swing.undo.PropertyEdit;

public class TestPropertyEdit extends TestCase
{
  PropertyChangeEvent  event;
  PropertyEdit         edit;
  String               color;

  public TestPropertyEdit (String name)
  {
    super (name);
  }

  public static Test suite ()
  {
    return new TestSuite (TestPropertyEdit.class);
  }

  public String getColor ()
  {
    return color;
  }

  public void setColor (String color)
  {
    this.color = color;
  }

  public void setUp ()
  {
    color = "newColor";
    event = new PropertyChangeEvent (this, "color", "oldColor", "newColor");
    edit  = new PropertyEdit (event);
  }

  public void testUndo ()
  {
    assertEquals ("newColor", color);
    edit.undo ();
    assertEquals ("oldColor", color);
  }

  public void testRedo ()
  {
    edit.undo ();
    edit.redo ();
    assertEquals ("newColor", color);
  }

  public void testDoubleRedoPasses ()
  {
    edit.undo ();
    edit.redo ();
    edit.redo ();

    assertEquals ("newColor", color);
  }

  public String toString ()
  {
    return "test.ui.monitor.PropertyEdit@2345XXX ";
  }

  public void testDescription ()
  {
    String string        = ".package.test.Class@56789tggAa_";
    event                  = new PropertyChangeEvent (this, "color", string, string);
    PropertyEdit testEdit  = new PropertyEdit (event);

    String expectedPresentationName = "TestPropertyEdit change color [ -> ]";
    String presentationName         = testEdit.getPresentationName ();

    assertEquals (expectedPresentationName, presentationName);
  }
}