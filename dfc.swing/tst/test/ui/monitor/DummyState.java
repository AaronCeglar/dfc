package test.ui.monitor;

import java.util.Hashtable;

import javax.swing.undo.StateEditable;

import dsto.dfc.util.BasicPropertyEventSource;

/**
 * StateEdit subclass created for testing purposes.
 *
 * @author Peter J Smet
 * @version $ Revision: $
 */
public class DummyState extends BasicPropertyEventSource implements StateEditable
{
  protected String name = null;

  public DummyState (String name)
  {
    this.name = name;
  }

  public void setName (String newName)
  {
    this.name = newName;
  }

  // StateEditable methods

  public void storeState (Hashtable stateMap)
  {
    stateMap.put ("name", this.name);
  }

  public void restoreState (Hashtable stateMap)
  {
    if (stateMap != null)
      setName ((String)stateMap.get ("name"));
    // else the name has not been changed
  }

}