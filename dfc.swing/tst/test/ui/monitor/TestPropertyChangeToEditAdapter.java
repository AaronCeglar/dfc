package test.ui.monitor;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import dsto.dfc.swing.undo.PropertyChangeToEditAdapter;
import dsto.dfc.swing.undo.PropertyEdit;
import dsto.dfc.util.BasicPropertyEventSource;
import dsto.dfc.util.PropertyEventSource;

public class TestPropertyChangeToEditAdapter extends TestCase implements UndoableEditListener, PropertyEventSource
{
  PropertyChangeToEditAdapter editAdapter;
  PropertyChangeEvent         event;
  PropertyChangeEvent         heardEvent;
  BasicPropertyEventSource   pcSupport;

  /** Dummy property */
  private String property;

  public TestPropertyChangeToEditAdapter (String name)
  {
    super (name);
  }

  public static Test suite ()
  {
    return new TestSuite (TestPropertyChangeToEditAdapter.class);
  }

  public void setUp ()
  {
    editAdapter = new PropertyChangeToEditAdapter ();
    event       = new PropertyChangeEvent (editAdapter, "string", "oldValue", "newValue");
    pcSupport   = new BasicPropertyEventSource (this);
    property    = "oldValue";

    this.addPropertyChangeListener (editAdapter);
    editAdapter.addUndoableEditListener (this);
  }

  public void tearDown ()
  {
    editAdapter.removeUndoableEditListener (this);
    this.removePropertyChangeListener (editAdapter);
  }

  public String getProperty ()
  {
    return property;
  }

  public void setProperty (String newValue)
  {
    String oldValue = property;
    property = newValue;

    pcSupport.firePropertyChange ("property", oldValue, newValue);
  }

  public void testDirectPropertyChangeEvent ()
  {
    editAdapter.propertyChange (event);
    assertSame (event, heardEvent);
  }

  public void testPropertyChange ()
  {
    this.setProperty ("newValue");
    assertEquals ("newValue", heardEvent.getNewValue ());
    assertEquals ("oldValue", heardEvent.getOldValue ());
    assertEquals ("property", heardEvent.getPropertyName ());
  }

  public void assertSame (PropertyChangeEvent ev1, PropertyChangeEvent ev2)
  {
    assertSame (ev1.getSource (),       ev2.getSource ());
    assertSame (ev1.getPropertyName (), ev2.getPropertyName ());
    assertSame (ev1.getOldValue (),     ev2.getOldValue ());
    assertSame (ev1.getNewValue (),     ev2.getNewValue ());
  }

  // UndoableEditListener interface

  public void undoableEditHappened (UndoableEditEvent e)
  {
    PropertyEdit edit = (PropertyEdit) e.getEdit ();
    heardEvent = new PropertyChangeEvent (edit.source, edit.propertyName, edit.oldValue, edit.newValue);
  }

  // PropertyEventSource interface

  public void addPropertyChangeListener (PropertyChangeListener l)
  {
    pcSupport.addPropertyChangeListener (l);
  }

  public void removePropertyChangeListener (PropertyChangeListener l)
  {
    pcSupport.removePropertyChangeListener (l);
  }
}
