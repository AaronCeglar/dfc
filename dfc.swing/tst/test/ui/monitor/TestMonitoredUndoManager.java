package test.ui.monitor;

import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.CompoundEdit;
import javax.swing.undo.StateEdit;
import javax.swing.undo.UndoableEdit;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import dsto.dfc.swing.undo.MonitoredEditGroup;
import dsto.dfc.swing.undo.MonitoredUndoManager;

/**
 * Title:        TestMonitoredEditManager
 * Description:  JUnit test for the MonitoredEditManager class.
 * Company:      ITD/DSTO
 * @author Peter J Smet
 * @author Derek Weber
 * @version
 */
public class TestMonitoredUndoManager extends TestCase
{
  protected MonitoredUndoManager undoManager;
  protected EditEventSource      evtSrc;
  protected DummyState           firstState ;
  protected DummyState           secondState;
  protected StateEdit            firstEdit;
  protected StateEdit            secondEdit;
  protected StateEdit            thirdEdit;
  protected StateEdit            fourthEdit;
  protected StateEdit            fifthEdit;
  protected MonitoredEditGroup   groupEdit;
  protected NonUndoableEdit      nonUndoableEdit;

  public TestMonitoredUndoManager (String name)
  {
    super (name);
  }

  public static Test suite ()
  {
    return new TestSuite (TestMonitoredUndoManager.class);
  }

  public void setUp ()
  {
    undoManager     = new MonitoredUndoManager ();
    evtSrc          = new EditEventSource ();
    evtSrc.addUndoableEditListener (undoManager);
    firstState      = new DummyState ("first");
    secondState     = new DummyState ("second");
    firstEdit       = new StateEdit (firstState, "first");
    secondEdit      = new StateEdit (secondState, "second");
    thirdEdit       = new StateEdit (new DummyState ("third" ), "third");
    fourthEdit      = new StateEdit (new DummyState ("fourth"), "fourth");
    fifthEdit       = new StateEdit (new DummyState ("fifth"),  "fifth");
    groupEdit       = new MonitoredEditGroup ();
    nonUndoableEdit = new NonUndoableEdit ();
  }

  public void tearDown ()
  {
    evtSrc.removeUndoableEditListener (undoManager);
  }

  public void addFirstTwoEditsToManager ()
  {
    undoManager.addEdit (firstEdit);
    undoManager.addEdit (secondEdit);
  }

  public void addFirstThreeEditsToManager ()
  {
    addFirstTwoEditsToManager ();
    undoManager.addEdit (thirdEdit);
  }

  public void addNonUndoableEditToManager ()
  {
    undoManager.addEdit (nonUndoableEdit);
  }

  public void testEventAdding ()
  {
     addFirstThreeEditsToManager ();

     assertEquals (3, undoManager.getEditCount ());

     assertTrue ( undoManager.containsEdit (firstEdit) );

     undoManager.addEdit (groupEdit);
     assertTrue ( undoManager.containsEdit (groupEdit) );
  }

  public void testUndo ()
  {
    undoManager.addEdit (firstEdit);

    firstState.setName ("newFirst");
    assertEquals ("State change didn't work", "newFirst", firstState.name);
    firstEdit.end ();

    undoManager.undo ();
    assertEquals ("Undo didn't work", "first", firstState.name);
  }

  public void testRedo ()
  {
    firstState.setName ("newFirst");
    undoManager.addEdit (firstEdit);
    firstEdit.end ();
    undoManager.undo ();
    assertEquals ("Undo didn't work", "first", firstState.name);
    undoManager.redo ();
    assertEquals ("Redo didn't work", "newFirst", firstState.name);
  }

  public void testGroupEditRedo ()
  {
    firstState.setName ("newFirst");
    groupEdit.addEdit (firstEdit);
    evtSrc.fire (groupEdit);
    firstEdit.end ();
    groupEdit.end ();

    undoManager.undo ();
    assertEquals ("Undo didn't work", "first", firstState.name);
    undoManager.redo ();
    assertEquals ("Undo didn't work", "newFirst", firstState.name);
  }

  public void testNestedGroupEditRedo ()
  {
    MonitoredEditGroup innerGroupEdit = new MonitoredEditGroup ();
    firstState.setName ("newFirst");
    innerGroupEdit.addEdit (firstEdit);
    groupEdit.addEdit (innerGroupEdit);

    evtSrc.fire (groupEdit);
    firstEdit.end ();
    innerGroupEdit.end ();
    groupEdit.end ();

    undoManager.undo ();
    assertEquals ("Undo didn't work", "first", firstState.name);
    undoManager.redo ();
    assertEquals ("Undo didn't work", "newFirst", firstState.name);
  }

  public void testAddTwoEditsDirectly ()
  {
    addFirstTwoEditsToManager ();
    undoTwoEdits ();
  }

  public void testAddTwoEditsByEvents ()
  {
    evtSrc.fire (firstEdit);
    evtSrc.fire (secondEdit);
    undoTwoEdits ();
  }

  public void undoTwoEdits ()
  {
    firstState.setName ("newFirst");
    secondState.setName ("newSecond");

    assertEquals ("First state change",  "newFirst",   firstState.name);
    assertEquals ("Second state change", "newSecond", secondState.name);

    undoManager.undo ();

    assertEquals ("No effect of first undo on first state", "newFirst", firstState.name);
    assertEquals ("First undo undid second edit",   "second",   secondState.name);

    undoManager.undo ();

    assertEquals ("Second undo undid firstEdit",  "first",  firstState.name);
    assertEquals ("Second undo had no effect on second edit",  "second", secondState.name);
  }

  public void testAlreadyUndone ()
  {
    groupEdit.addEdit (firstEdit);
    evtSrc.fire (groupEdit);
    firstEdit.end ();
    groupEdit.end ();

    firstEdit.undo ();
    try
    {
      undoManager.undo ();
      fail ("Should have thrown a CannotUndoException");
    }
    catch (CannotUndoException cue)
    {
      // test passes
    }
  }

  public void testUndoUpTo ()
  {
    addFirstTwoEditsToManager ();

    assertTrue ( firstEdit.canUndo  () );
    assertTrue ( secondEdit.canUndo () );

    undoManager.undoTo (firstEdit);
    assertTrue ( ! firstEdit.canUndo  () );

    assertTrue ( firstEdit.canRedo  () );
    assertTrue ( secondEdit.canRedo () );
  }

  public void testRedoUpTo ()
  {
    firstEdit.end ();
    secondEdit.end ();
    thirdEdit.end ();

    addFirstThreeEditsToManager ();

    undoManager.undo ();
    undoManager.undo ();
    undoManager.undo ();

    undoManager.redoTo (secondEdit);

    checkTwoOfThreeEditsAreUndoable ();
  }

  public void testRedoUpToMissing ()
  {
    addFirstTwoEditsToManager ();

    undoManager.undo ();
    undoManager.undo ();

    doRedoToToFail (null);
    doRedoToToFail (thirdEdit);

    assertEquals (2, undoManager.getEditCount ());
    assertTrue (firstEdit.canRedo ());
    assertTrue (undoManager.undoCursorIsNull ());
  }

  protected void doRedoToToFail (UndoableEdit edit)
  {
    try
    {
      undoManager.redoTo (edit);
      fail ("Should have received a CannotRedoException here");
    }
    catch (CannotRedoException cre)
    {
      /* pass test */
    }
  }

  protected void doUndoToToFail (UndoableEdit edit)
  {
    try
    {
      undoManager.undoTo (edit);
      fail ("Should have received a CannotUndoException here");
    }
    catch (CannotUndoException cue)
    {
      /* pass test */
    }
  }

  public void testMultipleUndoUpTo ()
  {
    addFirstThreeEditsToManager ();
    evtSrc.fire (fourthEdit);

    assertTrue ( firstEdit.canUndo  () );
    assertTrue ( ! secondEdit.canRedo () );
    assertTrue ( thirdEdit.canUndo  () );
    assertTrue ( ! fourthEdit.canRedo () );

    undoManager.undoTo (secondEdit);

    assertSame (firstEdit, undoManager.getUndoCursor ());

    assertTrue (   firstEdit.canUndo  () );
    assertTrue ( ! secondEdit.canUndo () );
    assertTrue ( ! fourthEdit.canUndo () );

    assertTrue ( ! firstEdit.canRedo  () );
    assertTrue (   thirdEdit.canRedo  () );
    assertTrue (   fourthEdit.canRedo () );

    undoManager.undoTo (firstEdit);

    assertTrue ( firstEdit.canRedo () );
  }

  public void testUndoUpToMissing ()
  {
     UndoableEdit missingEdit = fourthEdit;
     missingEdit.undo ();
     addFirstTwoEditsToManager ();
     assertSame (secondEdit, undoManager.getUndoCursor ());

     doUndoToToFail (missingEdit);

     assertSame (secondEdit, undoManager.getUndoCursor ());
     checkFirstTwoEditsAreUndoable ();
  }

  public void testUndoToAfterCursor ()
  {
     addFirstThreeEditsToManager ();

     undoManager.undo ();

     doUndoToToFail (thirdEdit);

     checkTwoOfThreeEditsAreUndoable ();
  }

  public void testRedoToNonRedoable ()
  {
    NonUndoableEdit nonRedoableEdit = new NonUndoableEdit ();
    addFirstTwoEditsToManager ();
    undoManager.undo ();
    undoManager.undo ();

    assertTrue (undoManager.undoCursorIsNull ());

    undoManager.insertEdit (2, nonRedoableEdit);
    doRedoToToFail (nonRedoableEdit);

    assertSame (secondEdit, undoManager.getUndoCursor ());
    assertSame (nonRedoableEdit, undoManager.getRedoCursor ());

    assertEquals (3, undoManager.getEditCount ());
    checkFirstTwoEditsAreUndoable ();
  }

  public void testRedoToBeforeCursor ()
  {
    addFirstThreeEditsToManager ();

    undoManager.undo ();

    doRedoToToFail (firstEdit);
    doRedoToToFail (secondEdit);

    assertEquals (3, undoManager.getEditCount ());

    checkTwoOfThreeEditsAreUndoable ();
  }

  public void checkFirstTwoEditsAreUndoable ()
  {
    assertTrue (undoManager.inUndoList (firstEdit ));
    assertTrue (undoManager.inUndoList (secondEdit));
  }

  public void checkTwoOfThreeEditsAreUndoable ()
  {
     checkFirstTwoEditsAreUndoable ();
     assertTrue (thirdEdit.canRedo  ());
  }

  public void testDiscardAllEdits ()
  {
    addFirstThreeEditsToManager ();

    undoManager.discardAllEdits ();

    assertEquals (0, undoManager.getEditCount ());
    assertTrue   ( ! firstEdit.canUndo ());
    assertTrue   ( ! thirdEdit.canUndo ());

    UndoableEdit extraEdit = new StateEdit (new DummyState("extra" ), "extra");
    undoManager.addEdit (extraEdit);
    assertSame (extraEdit, undoManager.getUndoCursor ());
    assertTrue (undoManager.redoCursorIsNull ());
    undoManager.undo ();
    assertTrue (undoManager.undoCursorIsNull ());
    assertSame (extraEdit, undoManager.getRedoCursor ());
  }

  public void checkNestedGroupStructure (MonitoredEditGroup [] editGroups)
  {
    assertEquals (3, undoManager.getEditCount ());
    assertEquals (2, editGroups [0].getEditCount ());
    assertEquals (1, editGroups [1].getEditCount ());
  }

  public void testUndoPlusAdditionOfNewEdits ()
  {
    addFirstTwoEditsToManager ();
    undoManager.undo ();
    assertEquals (firstEdit, undoManager.getUndoCursor ());
    undoManager.addEdit (thirdEdit);
    assertEquals (2, undoManager.getEditCount ());
    assertEquals (1, undoManager.indexOfEdit (thirdEdit));
    assertTrue (undoManager.containsEdit (thirdEdit));
    undoManager.addEdit (fourthEdit);
    assertTrue (undoManager.containsEdit (thirdEdit));
    assertEquals (3, undoManager.getEditCount ());
    assertEquals (2, undoManager.indexOfEdit (fourthEdit));
  }

  public void testDoubleUndoPlusAdditionOfNewEdits ()
  {
    addFirstThreeEditsToManager ();
    undoManager.undo ();
    undoManager.undo ();
    undoManager.addEdit (fourthEdit);
    undoManager.addEdit (fifthEdit);
    assertEquals (3, undoManager.getEditCount ());
  }

  public void testInvalidIndexInsertion ()
  {
    addFirstThreeEditsToManager ();
    assertTrue (! undoManager.insertEdit ( 4, fourthEdit));
    assertTrue (! undoManager.insertEdit (-1, fourthEdit));
    assertTrue (  undoManager.insertEdit ( 0, fourthEdit));
    assertEquals (4, undoManager.getEditCount ());
    assertSame (fourthEdit, undoManager.getEditOrNullEdit (0));
  }

  public boolean canStillUndoEditAddedTo (UndoableEdit group)
  {
    assertTrue ( secondEdit.canUndo () );

    undoManager.addEdit (group);
    assertTrue (undoManager.containsEdit (group));
    undoManager.addEdit (secondEdit);
    assertTrue ( ! secondEdit.canRedo () );

    return secondEdit.canUndo ();
  }

  public void testUndoWhenAddingToEditGroup ()
  {
    assertTrue ( canStillUndoEditAddedTo (new MonitoredEditGroup ()));
  }

  public void testUndoWhenAddingToCompoundEdit ()
  {
    assertTrue ( ! canStillUndoEditAddedTo (new CompoundEdit ()));
  }

  public void testSingleNestedUndoUpTo ()
  {
    evtSrc.fire (firstEdit);
    evtSrc.fire (groupEdit);

    undoManager.addEdit (secondEdit);

    groupEdit.end ();

    undoManager.addEdit (thirdEdit);

    assertTrue ( ! undoManager.containsEdit (secondEdit));
    assertTrue ( groupEdit.getEdits ().contains(secondEdit) );
    assertTrue ( ! groupEdit.getEdits ().contains(thirdEdit) );

    assertTrue ( ! groupEdit.canRedo  () );
    assertTrue ( ! secondEdit.canRedo () );

    assertTrue ( firstEdit.canUndo  () );
    assertTrue ( secondEdit.canUndo () );

    assertSame ( thirdEdit, undoManager.getUndoCursor () );

    undoManager.undoTo (thirdEdit);

    assertSame ( groupEdit, undoManager.getUndoCursor () );

    undoManager.undoTo (groupEdit);

    assertSame ( firstEdit, undoManager.getUndoCursor () );

    assertTrue (   firstEdit.canUndo  () );
    assertTrue ( ! secondEdit.canUndo () );
    assertTrue ( ! thirdEdit.canUndo  () );

    assertTrue ( ! firstEdit.canRedo  () );
    assertTrue (   secondEdit.canRedo () );
    assertTrue (   thirdEdit.canRedo  () );

    undoManager.undoTo (firstEdit);

    assertTrue ( ! firstEdit.canUndo () );
  }

  public void testUndoOnEmptyEditList ()
  {
    doSingleUndoToFail ();
  }

  protected void doSingleUndoToFail ()
  {
    try
    {
      undoManager.undo ();
      fail ("Should have heard a CannotUndoException");
    }
    catch (Throwable ex)
    {
      /* pass test */
    }
  }

  public void testSingleNonUndoableEdit ()
  {
    addNonUndoableEditToManager ();

    doSingleUndoToFail ();
    assertSame (nonUndoableEdit, undoManager.getUndoCursor ());

    undoManager.skipUndo ();
    assertTrue (undoManager.undoCursorIsNull ());
  }

  public void testFirstUndoPastNonUndoableEdit ()
  {
    undoManager.addEdit (firstEdit);
    addNonUndoableEditToManager ();

    doSingleUndoToFail ();
    undoManager.skipUndo ();

    assertSame (firstEdit, undoManager.getUndoCursor ());
  }

  public void testMultipleUndoToPastNonUndoableEdit ()
  {
    addFirstTwoEditsToManager ();
    addNonUndoableEditToManager ();

    try
    {
      undoManager.undoTo (secondEdit);
      fail ("Should have received a CannotUndoException");
    }
    catch (CannotUndoException ex)
    {
      /* pass test */
    }
    undoManager.skipUndo ();
    undoManager.undoTo (secondEdit);

    assertSame (firstEdit, undoManager.getUndoCursor ());
    assertEquals (2, undoManager.getEditCount ());
  }

  public void testUndoPastNonUndoableEdit ()
  {
    addFirstTwoEditsToManager ();
    addNonUndoableEditToManager ();

    doSingleUndoToFail ();
    undoManager.skipUndo ();
    assertSame (secondEdit, undoManager.getUndoCursor ());

    assertEquals (2, undoManager.getEditCount ());
  }


  public void testSingleNonRedoableEdit ()
  {
    addNonUndoableEditToManager ();

    doSingleUndoToFail ();
    undoManager.skipUndo ();
    doSingleRedoToFail (); // won't shift undo cursor

    assertTrue (undoManager.undoCursorIsNull ());
    assertEquals (0, undoManager.getEditCount ());
  }

  protected void doSingleRedoToFail ()
  {
    try
    {
      undoManager.redo ();
      fail ("Should have heard a CannotRedoException");
    }
    catch (Throwable ex)
    {
      /* pass test */
    }
  }

  public void testFirstRedoPastNonUndoableEdit ()
  {
    undoManager.addEdit (firstEdit);
    addNonUndoableEditToManager ();
    undoManager.addEdit (secondEdit);

    undoManager.undo ();     // secondEdit
    doSingleUndoToFail ();   // non-undoable edit - will fail
    undoManager.skipUndo (); // skips the non-undoable edit & removes the tail of the list
    assertSame (firstEdit, undoManager.getUndoCursor ());

    assertEquals (1, undoManager.getEditCount ());
  }

  public void testDivertEditStream ()
  {
    MonitoredUndoManager editEventRecorder = new MonitoredUndoManager ();
    addFirstTwoEditsToManager ();
    undoManager.broadcastEditEventsTo (editEventRecorder);

    assertEquals ( 2, undoManager.getEditCount ());
    assertEquals ( 0, editEventRecorder.getEditCount ());
    evtSrc.fire ();
    assertEquals ( 3, undoManager.getEditCount ());
    assertEquals ( 1, editEventRecorder.getEditCount ());
  }

  public void testStopDivertingEditStream ()
  {
    MonitoredUndoManager editEventRecorder = new MonitoredUndoManager ();
    undoManager.broadcastEditEventsTo (editEventRecorder);
    undoManager.stopBroadcastingEditEvents ();
    evtSrc.fire ();
    evtSrc.fire ();
    assertEquals ( 2, undoManager.getEditCount ());
    assertEquals ( 0, editEventRecorder.getEditCount ());
  }

  public void testMultipleRedoPastNonUndoableEdit ()
  {
    undoManager.addEdit (thirdEdit);
    addNonUndoableEditToManager ();
    addFirstTwoEditsToManager ();

    try
    {
      undoManager.undoTo (nonUndoableEdit);
      fail ("Should not have received a CannotUndoException");
    }
    catch (CannotUndoException ex)
    {
      /* pass test */
    }
    undoManager.skipUndo ();
    assertSame (thirdEdit, undoManager.getUndoCursor ());

    assertEquals (1, undoManager.getEditCount() );
    doSingleRedoToFail ();
  }

  public void testUndoCursorFindsNonUndoableEdit ()
  {
    addNonUndoableEditToManager ();
    addFirstTwoEditsToManager ();
    undoManager.undoTo (firstEdit);

    assertSame (nonUndoableEdit, undoManager.getUndoCursor ());

    assertEquals (3, undoManager.getEditCount() );
    undoManager.redo ();
    assertEquals (3, undoManager.getEditCount() );
  }

  public void testSimpleMultipleRedo ()
  {
    addFirstTwoEditsToManager ();
    try
    {
      undoManager.undo ();
      undoManager.undo ();
      undoManager.redo ();
      undoManager.redo ();
    }
    catch (Throwable ex)
    {
      ex.printStackTrace ();
      fail ("exception should not have been thrown: " + ex);
    }
  }

  public void testInUndoList ()
  {
    addFirstThreeEditsToManager ();
    undoManager.undoTo ( secondEdit );

    assertTrue ( undoManager.missingFromUndoList (thirdEdit) );
    assertTrue ( undoManager.missingFromUndoList (secondEdit) );
    assertTrue ( undoManager.inUndoList (firstEdit) );

    undoManager.redoTo ( thirdEdit );
    addNonUndoableEditToManager ();
    assertTrue ( undoManager.inUndoList (nonUndoableEdit) );

    try
    {
      undoManager.undoTo (firstEdit);
      fail ("CannotUndoException should be thrown");
    }
    catch (CannotUndoException cue)
    {
      /* pass test */
    }
    undoManager.skipUndo ();
    undoManager.undoTo (firstEdit);
    assertTrue (undoManager.undoCursorIsNull ());
    assertTrue ( undoManager.missingFromUndoList (nonUndoableEdit) );
  }

  public void testRedoAtEndOfHistory ()
  {
    addFirstTwoEditsToManager ();

    doSingleRedoToFail ();

    assertSame (secondEdit, undoManager.getUndoCursor ());
  }

  public void testDropFirstEdit ()
  {
    addFirstThreeEditsToManager();
    undoManager.undo();
    undoManager.dropFirstEdit ();
    assertEquals (0, undoManager.indexOfUndoCursor());
    assertEquals (1, undoManager.indexOfRedoCursor());
    undoManager.addEdit (fourthEdit);
    assertEquals (1, undoManager.indexOfEdit(fourthEdit));
  }

  public void testDropLastEdit ()
  {
    addFirstThreeEditsToManager();
    undoManager.undo();
    undoManager.dropEdit (thirdEdit);
    assertEquals (1, undoManager.indexOfUndoCursor());
    assertTrue (undoManager.redoCursorIsNull ());
    undoManager.addEdit (fourthEdit);
    assertEquals (2, undoManager.indexOfEdit(fourthEdit));
  }


}