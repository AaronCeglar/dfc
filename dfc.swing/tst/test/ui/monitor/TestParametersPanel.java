package test.ui.monitor;

import javax.swing.undo.StateEdit;
import javax.swing.undo.UndoableEdit;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import dsto.dfc.swing.undo.EditListView;
import dsto.dfc.swing.undo.MonitorPanel;
import dsto.dfc.swing.undo.MonitoredEdit;
import dsto.dfc.swing.undo.MonitoredUndoManager;
import dsto.dfc.swing.undo.ParametersPanel;

/**
 * Title:        TestMonitorPanel
 * Description:  JUnit test for the MonitorPanel
 * Company:      ITD/DSTO
 * @author Peter J Smet
 * @author Derek Weber
 * @version $Revision$
 */
public class TestParametersPanel extends TestCase
{
  protected MonitorPanel monitorPanel;
  ParametersPanel        parametersPanel;

  public TestParametersPanel (String name)
  {
    super (name);
  }

  public static Test suite ()
  {
    return new TestSuite (TestParametersPanel.class);
  }

  public void setUp ()
  {
    monitorPanel    = new MonitorPanel (new MonitoredUndoManager ());
    parametersPanel = monitorPanel.getParametersPanel ();

  }

  public void testNullEdit ()
  {
    parametersPanel.setEdit (MonitoredUndoManager.NULL_EDIT);
  }

  public void testNoParameters ()
  {
    probeParameterLimit (0);
  }

  public void testAtParameterLimits ()
  {
    probeParameterLimit (ParametersPanel.MAX_PARAMETERS);
  }

  public void testOverParameterLimits ()
  {
    probeParameterLimit (ParametersPanel.MAX_PARAMETERS + 1);
  }

  protected void probeParameterLimit (int limit)
  {
    String [] names  = new String [limit];
    Object [] values = new Object [limit];
    DummyMonitoredEdit boundaryProbe = new DummyMonitoredEdit (names, values);

    parametersPanel.setEdit (boundaryProbe);
  }

  public void testParameterNameDisplay ()
  {
    MonitoredUndoManager undoMgr   = monitorPanel.getUndoManager ();
    EditListView         list      = monitorPanel.getEditListView ();
    MonitoredEdit        paramEdit = new DummyMonitoredEdit ();
    UndoableEdit         firstEdit = createEdit ("edit1");

    undoMgr.addEdit (firstEdit);
    undoMgr.addEdit (createEdit ("edit2"));
    undoMgr.addEdit (paramEdit);
    undoMgr.addEdit (createEdit ("edit3"));

    list.setSelectedEdit (paramEdit);
    assertSame (paramEdit, parametersPanel.getEdit ());

    MonitoredEdit edit = (MonitoredEdit) parametersPanel.getEdit ();
    assertEquals ("shrek", edit.getParameterNames ()[0]);

    list.setSelectedEdit (firstEdit);
    assertSame (firstEdit, parametersPanel.getEdit ());
  }

  public static UndoableEdit createEdit (String name)
  {
     StateEdit edit = new StateEdit ( new DummyState (name), name );
     edit.end ();
     return edit;
  }
}