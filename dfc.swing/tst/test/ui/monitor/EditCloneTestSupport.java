package test.ui.monitor;

import javax.swing.undo.UndoableEdit;

import junit.framework.TestCase;

import dsto.dfc.swing.undo.MonitoredEdit;

/**
 * Class with static methods to support testing of edit cloning.
 *
 * @author Derek Weber
 * @version $Revision$
 */
public class EditCloneTestSupport extends TestCase
{
  public EditCloneTestSupport ()
  {
    super ("edit clone test support");
  }

  public static UndoableEdit basicCloneTest (MonitoredEdit edit)
    throws CloneNotSupportedException
  {
    assertTrue (isAlive (edit));

    MonitoredEdit editClone = (MonitoredEdit) edit.clone ();
    assertTrue (isAlive (editClone));

    edit.die ();
    assertTrue (!isAlive (edit));
    assertTrue ( isAlive (editClone));

    editClone.redo ();
    editClone.undo ();

    return editClone;
  }

  public static boolean isAlive (UndoableEdit edit)
  {
    return edit.canUndo () && edit.canRedo ();
  }
}