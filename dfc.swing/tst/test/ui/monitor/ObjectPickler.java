package test.ui.monitor;

import java.io.File;

import junit.framework.TestCase;

import dsto.dfc.swing.persistence.Deserializer;
import dsto.dfc.swing.persistence.Serializer;

public class ObjectPickler extends TestCase
{
  public ObjectPickler ()
  {
    this ("testSerialization");
  }

  public ObjectPickler (String name)
  {
    super (name);
  }

  public Object saveAndRestore (Object serializable)
  {
    Object restoredObject = null;

    try
    {
      String testFileName = "junk.test";
      Serializer.saveAsKBML (serializable, testFileName);
      restoredObject = Deserializer.read (testFileName);
      new File (testFileName).delete ();
    }
    catch (Throwable ex)
    {
      ex.printStackTrace ();
      fail ("serialization exception: ");
    }

    return restoredObject;
  }
}