package test.ui.monitor;


public class NonSignificantEdit extends DummyMonitoredEdit
{

  public NonSignificantEdit ()
  {
    this ("Non-significant edit");
  }

  public NonSignificantEdit (String name)
  {
    super (name);
  }

  public boolean isSignificant ()
  {
    return false;
  }
}