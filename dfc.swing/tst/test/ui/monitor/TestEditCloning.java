package test.ui.monitor;

import javax.swing.JButton;

import junit.framework.TestCase;

import dsto.dfc.swing.panels.ActivePanelEdit;
import dsto.dfc.swing.panels.FloatStateEdit;
import dsto.dfc.swing.panels.PanelManager;
import dsto.dfc.swing.undo.PropertyEdit;

/**
 * Test class for edit cloning.
 *
 * @author Derek Weber
 * @version $Revision$
 */
public class TestEditCloning extends TestCase
{
  private boolean              property;
  private PanelManager         panelManager;

  public TestEditCloning (String name)
  {
    super (name);
  }

  public void setUp ()
  {
    panelManager = new PanelManager ();
    panelManager.addPanel (new JButton ("1"));
    panelManager.addPanel (new JButton ("2"));
  }

  public void tearDown ()
  {
    panelManager.removeAllPanels ();
    System.gc ();
  }

  public void setProperty (boolean newProperty)
  {
    property = newProperty;
  }

  public boolean getProperty ()
  {
    return property;
  }

  public void testSimpleClone () throws CloneNotSupportedException
  {
    PropertyEdit edit = new PropertyEdit (this, "property", Boolean.TRUE, Boolean.FALSE);
    EditCloneTestSupport.basicCloneTest (edit);
  }

  public void testActivePanelEditCloning () throws CloneNotSupportedException
  {
    ActivePanelEdit edit      = new ActivePanelEdit (panelManager, 0, 1);
    EditCloneTestSupport.basicCloneTest (edit);
  }

  public void testFloatStateEditCloning () throws CloneNotSupportedException
  {
    FloatStateEdit edit =
      new FloatStateEdit (panelManager, 0, PanelManager.FIXED, PanelManager.FLOAT_EXTERNAL);
    EditCloneTestSupport.basicCloneTest (edit);
  }
}