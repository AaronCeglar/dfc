package test.ui.monitor;

import java.util.HashMap;

import javax.swing.JButton;

import junit.framework.TestCase;

import dsto.dfc.swing.panels.ActivePanelEdit;
import dsto.dfc.swing.panels.FloatStateEdit;
import dsto.dfc.swing.panels.PanelManager;
import dsto.dfc.swing.undo.EditListModel;
import dsto.dfc.swing.undo.PropertyEdit;

/**
 * Base test class for edit serialisation.
 *
 * @author Derek Weber
 * @version $Revision$
 */
public class TestEditSerialisation extends TestCase
{
  PanelManager  panelManager;
  ObjectPickler objectPickler = new ObjectPickler ();

  public TestEditSerialisation (String name)
  {
    super (name);
  }

  public void setUp ()
  {
    panelManager = new PanelManager ();
    ActivePanelEdit.PANELS_UNLOCKED = true;
  }

  public void tearDown ()
  {
    panelManager.removeAllPanels ();
  }

  public Object saveAndRestore (Object serializable)
  {
    return objectPickler.saveAndRestore (serializable);
  }

  public void testEditListModelSerialization ()
  {
    EditListModel edits = new EditListModel ();
    edits.add (new ActivePanelEdit (panelManager, 0, 1));
    edits.add (new ActivePanelEdit (panelManager, 0, 1));

    EditListModel restoredEdits = (EditListModel) saveAndRestore (edits);
    assertEquals (edits.getSize (), restoredEdits.getSize ());
  }

  public void testActivePanelEditSerialization ()
  {
    HashMap context = new HashMap ();
    context.put ("panelManager", panelManager);

    PanelManager differentPM = new PanelManager ();
    differentPM.addPanel (new JButton ("diff_one"));
    differentPM.addPanel (new JButton ("diff_two"));

    ActivePanelEdit edit = new ActivePanelEdit (differentPM, 0, 1);
    ActivePanelEdit restoredEdit = (ActivePanelEdit) saveAndRestore (edit);
    assertEquals (restoredEdit.oldValue, new Integer (0));
    assertEquals (restoredEdit.newValue, new Integer (1));
    assertNull   (restoredEdit.source);

    restoredEdit.restoreContext (context);
    assertSame  (restoredEdit.source, panelManager);
    panelManager.addPanel (new JButton ("real_one"));
    panelManager.addPanel (new JButton ("real_two"));

    assertTrue   (restoredEdit.canRedo ());
    assertEquals (0, panelManager.indexOfActivePanel ());
    restoredEdit.redo ();
    assertEquals (1, panelManager.indexOfActivePanel ());
    restoredEdit.undo ();
    assertEquals (0, panelManager.indexOfActivePanel ());
  }

  public void testBasicPropertyEditSerialisation ()
  {
    PropertyEdit edit = new PropertyEdit (null, "bob", null, null);
    PropertyEdit redit = (PropertyEdit) saveAndRestore (edit);

    assertTrue (redit.canRedo ());
  }

  public void testBasicFloatStateEditSerialisation ()
  {
    PanelManager.Panel panel = panelManager.addPanel (new JButton ());
    panel.setFloating (PanelManager.FLOAT_INTERNAL);

    FloatStateEdit edit = new FloatStateEdit (panelManager, 0, PanelManager.FIXED, PanelManager.FLOAT_EXTERNAL);
    FloatStateEdit redit = (FloatStateEdit) saveAndRestore (edit);

    HashMap context = new HashMap ();
    context.put ("panelManager", panelManager);
    redit.restoreContext (context);

    assertEquals(PanelManager.FLOAT_INTERNAL, panel.getFloating ());

    redit.redo ();
    assertEquals (PanelManager.FLOAT_EXTERNAL, panel.getFloating ());
    assertSame (panel, redit.source);
    redit.undo ();
    assertEquals (PanelManager.FIXED, panel.getFloating ());
  }
}