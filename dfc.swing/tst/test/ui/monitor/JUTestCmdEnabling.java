package test.ui.monitor;

import javax.swing.undo.StateEdit;
import javax.swing.undo.UndoableEdit;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import dsto.dfc.swing.undo.CmdRedoTo;
import dsto.dfc.swing.undo.CmdUndoTo;
import dsto.dfc.swing.undo.EditListView;
import dsto.dfc.swing.undo.MonitoredUndoManager;

public class JUTestCmdEnabling extends TestCase
{
  EditListView         editListView;
  MonitoredUndoManager undoManager;
  CmdRedoTo            cmdRedo;
  CmdUndoTo            cmdUndo;

  UndoableEdit         firstEdit;
  UndoableEdit         secondEdit;

  public JUTestCmdEnabling (String name)
  {
    super (name);
  }

  public static Test suite ()
  {
    return new TestSuite (JUTestCmdEnabling.class);
  }

  public void setUp ()
  {
    undoManager  = new MonitoredUndoManager ();
    editListView = new EditListView (undoManager);
    cmdRedo      = new CmdRedoTo (editListView, undoManager);
    cmdUndo      = new CmdUndoTo (editListView, undoManager);

    firstEdit    = new StateEdit (new DummyState ("first"));
    secondEdit   = new StateEdit (new DummyState ("second"));
  }

  public void testCmdEnabling ()
  {
      assertTrue ( ! cmdRedo.isEnabled () );
      assertTrue ( ! cmdUndo.isEnabled () );
      undoManager.addEdit (firstEdit);
      editListView.setSelectedEdit (firstEdit);

      assertTrue ( cmdUndo.isEnabled () );
      assertTrue ( ! cmdRedo.isEnabled () );
      undoManager.undo ();
      assertTrue ( ! cmdUndo.isEnabled () );
      assertTrue ( cmdRedo.isEnabled () );

      undoManager.redo ();
      undoManager.addEdit (secondEdit);
      undoManager.undo ();

      assertTrue ( ! cmdRedo.isEnabled () );
      assertTrue ( cmdUndo.isEnabled () );

      editListView.setSelectedEdit (secondEdit);

      assertTrue ( cmdRedo.isEnabled () );
      assertTrue ( ! cmdUndo.isEnabled () );

      editListView.setSelectedEdit (firstEdit);

      assertTrue ( ! cmdRedo.isEnabled () );
      assertTrue ( cmdUndo.isEnabled () );

      undoManager.discardAllEdits ();
      assertEquals (-1, editListView.getSelectedIndex ());

      assertTrue ( ! cmdRedo.isEnabled () );
      assertTrue ( ! cmdUndo.isEnabled () );
  }
}
