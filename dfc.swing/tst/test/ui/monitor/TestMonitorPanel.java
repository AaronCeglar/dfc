package test.ui.monitor;

import javax.swing.JButton;
import javax.swing.undo.StateEdit;
import javax.swing.undo.UndoableEdit;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import dsto.dfc.swing.panels.ActivePanelEdit;
import dsto.dfc.swing.panels.PanelManager;
import dsto.dfc.swing.panels.RestrictedIndexTabbedPane;
import dsto.dfc.swing.undo.EditListView;
import dsto.dfc.swing.undo.MonitorPanel;
import dsto.dfc.swing.undo.MonitoredUndoManager;

/**
 * Title:        TestMonitorPanel
 * Description:  JUnit test for the MonitorPanel
 * Company:      ITD/DSTO
 * @author Peter J Smet
 * @author Derek Weber
 * @version
 */
public class TestMonitorPanel extends TestCase
{
  MonitorPanel         monitorPanel;
  EditListView         editListView;
  MonitoredUndoManager undoManager;

  UndoableEdit         firstEdit;
  UndoableEdit         secondEdit;

  public TestMonitorPanel (String name)
  {
    super (name);
  }

  public static Test suite ()
  {
    return new TestSuite (TestMonitorPanel.class);
  }

  public void setUp ()
  {
    undoManager  = new MonitoredUndoManager ();
    monitorPanel = new MonitorPanel (undoManager);
    editListView = monitorPanel.getEditListView ();

    firstEdit    = new StateEdit (new DummyState ("first"));
    secondEdit   = new StateEdit (new DummyState ("second"));
  }

  public void testSelectionCursor ()
  {
    assertSame (MonitoredUndoManager.NULL_EDIT, editListView.getSelectedEdit ());

    undoManager.addEdit (firstEdit);
    assertSame (MonitoredUndoManager.NULL_EDIT, editListView.getSelectedEdit ());

    editListView.setSelectedEdit (firstEdit);
    undoManager.addEdit (secondEdit);
    assertSame (firstEdit, editListView.getSelectedEdit ());
  }

  public void testButtonDisablingWhenCursorsAtEnd ()
  {
    final boolean enabled  = true;
    final boolean disabled = false;

    assertUndoIs (disabled);
    assertRedoIs (disabled);

    undoManager.addEdit (firstEdit);

    assertUndoIs (enabled );
    assertRedoIs (disabled);

    undoManager.addEdit (secondEdit);

    assertUndoIs (enabled );
    assertRedoIs (disabled);

    undoManager.undo ();

    assertUndoIs (enabled );
    assertRedoIs (enabled);

    undoManager.undo ();

    assertUndoIs (disabled);
    assertRedoIs (enabled);

    undoManager.redo ();
    undoManager.redo ();

    assertUndoIs (enabled );
    assertRedoIs (disabled);
  }

  private void assertRedoIs (boolean enabled)
  {
    assertEquals (enabled, monitorPanel.getRedoCommand ().isEnabled ());
  }

  private void assertUndoIs (boolean enabled)
  {
    assertEquals (enabled, monitorPanel.getUndoCommand ().isEnabled ());
  }

  public void testPanelLocking ()
  {
    ActivePanelEdit.PANELS_UNLOCKED = true;
    monitorPanel.lockActivePanelDuringPlayback (true);
    assertTrue (! ActivePanelEdit.PANELS_UNLOCKED);
  }

  public void testNoEditsGenerated ()
  {
    PanelManager panelManager         = new PanelManager ();
    RestrictedIndexTabbedPane tabPane = panelManager.tabPane;
    panelManager.addUndoableEditListener (undoManager);
    panelManager.addPanel (new JButton ());
    panelManager.addPanel (new JButton ());
    panelManager.addPanel (monitorPanel, "Monitor", null);

    assertEquals (0, undoManager.getEditCount ());
    tabPane.setSelectedIndex (1);

    assertEquals (1, undoManager.getEditCount ());
    tabPane.setSelectedIndex (2);
    assertEquals (1, undoManager.getEditCount ());
    tabPane.setSelectedIndex (0);
    assertEquals (1, undoManager.getEditCount ());
    tabPane.setSelectedIndex (1);
    assertEquals (2, undoManager.getEditCount ());
  }
}


