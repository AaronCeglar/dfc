package test.ui.monitor;

import javax.swing.JButton;
import javax.swing.event.UndoableEditListener;
import javax.swing.undo.StateEdit;
import javax.swing.undo.UndoableEdit;
import javax.swing.undo.UndoableEditSupport;

import dsto.dfc.swing.undo.UndoableEditSource;

public class EditEventSource extends JButton implements UndoableEditSource
{
  private UndoableEditSupport editListeners = new UndoableEditSupport (this);
  private int                 editCount = 0;

  // UndoableEditSource interface

  public void addUndoableEditListener (UndoableEditListener l)
  {
    editListeners.addUndoableEditListener (l);
  }

  public void removeUndoableEditListener (UndoableEditListener l)
  {
    editListeners.removeUndoableEditListener (l);
  }

  // end UndoableEditSource interface

  public void fire ()
  {
    fire (new StateEdit (new DummyState ("edit " + editCount++)));
  }

  public void fire (UndoableEdit edit)
  {
    editListeners.postEdit (edit);
  }
}