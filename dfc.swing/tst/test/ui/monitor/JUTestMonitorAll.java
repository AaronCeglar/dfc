package test.ui.monitor;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Title:        TestMonitorAll
 * Description:  JUnit test suite which carries out all DFC Monitor package tests.
 *
 * @author Derek Weber
 * @version $Revision$
 */
public class JUTestMonitorAll extends TestCase
{
  public JUTestMonitorAll(String name)
  {
    super (name);
  }

  public static Test suite ()
  {
    TestSuite suite = new TestSuite ("test all Monitor classes");

    suite.addTest (new TestSuite (TestMonitoredUndoManager.class));
    suite.addTest (new TestSuite (TestPropertyEdit.class));
    suite.addTest (new TestSuite (TestMonitorPanel.class));
    suite.addTest (new TestSuite (TestParametersPanel.class));
    suite.addTest (new TestSuite (TestMonitoredEditGroup.class));
    suite.addTest (new TestSuite (TestPropertyChangeToEditAdapter.class));
    suite.addTest (new TestSuite (TestEditCloning.class));
    suite.addTest (new TestSuite (TestEditSerialisation.class));
    suite.addTest (new TestSuite (JUTestCmdEnabling.class));

    return suite;
  }
}