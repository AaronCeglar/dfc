package test.ui.monitor;

import java.beans.PropertyChangeEvent;
import java.io.File;

import javax.swing.JButton;
import javax.swing.undo.StateEdit;
import javax.swing.undo.UndoableEdit;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import dsto.dfc.swing.persistence.Deserializer;
import dsto.dfc.swing.persistence.Serializer;
import dsto.dfc.swing.undo.MonitoredEditGroup;
import dsto.dfc.swing.undo.PropertyEdit;

public class TestMonitoredEditGroup extends TestCase
{
  String              testFileName = "junk.test";
  MonitoredEditGroup  editGroup;
  MonitoredEditGroup  restoredGroup;
  UndoableEdit        parameterisedEdit;
  UndoableEdit        dummyEdit;
  UndoableEdit        propertyEdit;
  PropertyChangeEvent propertyEvent;

  public TestMonitoredEditGroup (String name)
  {
    super (name);
  }

  public static Test suite ()
  {
    return new TestSuite (TestMonitoredEditGroup.class);
  }

  public void setUp ()
  {
    editGroup = new MonitoredEditGroup ("Buck Rogers");
    parameterisedEdit = new DummyMonitoredEdit ();
    dummyEdit = new StateEdit (new DummyState ("test"));
    ((StateEdit)dummyEdit).end ();
    propertyEvent = new PropertyChangeEvent (new JButton (), "text", "old", "new");
    propertyEdit  = new PropertyEdit (propertyEvent);
  }

  public void tearDown ()
  {
    File file = new File (testFileName);
    file.delete ();
  }
  
/*
  public void testBasicSerialization ()
  {
    serialiseAndRestore ();

    assertEquals (editGroup.getName (), restoredGroup.getName ());
  }

  public void testComplexSerialisation ()
  {
    editGroup.addEdit (parameterisedEdit);
    editGroup.addEdit (dummyEdit);
    editGroup.addEdit (propertyEdit);

    serialiseAndRestore ();

    assertEquals (3, restoredGroup.getEditCount());
    JButton btn = (JButton) propertyEvent.getSource ();
    assertEquals ("old", btn.getText ());
  }
*/

  protected void serialiseAndRestore ()
  {
    try
    {
      Serializer.saveAsBinary (editGroup, testFileName);
      restoredGroup = (MonitoredEditGroup) Deserializer.read (testFileName);
    }
    catch (Exception ex)
    {
      ex.printStackTrace ();
      fail ();
    }
  }
}