package test.ui.monitor;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.undo.StateEdit;
import javax.swing.undo.UndoableEdit;

import dsto.dfc.swing.undo.MonitorPanel;
import dsto.dfc.swing.undo.MonitoredEdit;
import dsto.dfc.swing.undo.MonitoredEditGroup;
import dsto.dfc.swing.undo.MonitoredUndoManager;
import dsto.dfc.test.TestFrame;

/**
 * Title:        TestMonitorPanel
 * Description:  JUnit test for the MonitorPanel
 * Company:      ITD/DSTO
 * @author Peter J Smet
 * @author Derek Weber
 * @version $Revision$
 */
public class TestMonitorPanelGUI extends TestFrame
{
  public static boolean UNDOABLE     = true;
  public static boolean NON_UNDOABLE = false;

  public static void main(String[] args)
  {
    MonitorPanel editsPanel   = new MonitorPanel (new MonitoredUndoManager ());
    JPanel       editGenPanel = new JPanel (new GridLayout (1, 2));

    UndoableEdit firstEdit   = createEdit ("first",  UNDOABLE);
    UndoableEdit secondEdit  = createEdit ("second", UNDOABLE);
    UndoableEdit thirdEdit   = createEdit ("third",  UNDOABLE);
    UndoableEdit fourthEdit  = createEdit ("fourth", UNDOABLE);
    UndoableEdit fifthEdit   = createEdit ("fifth",  UNDOABLE);
    UndoableEdit sixthEdit   = new DummyMonitoredEdit ();
    UndoableEdit seventhEdit = new DummyMonitoredEdit ();
    UndoableEdit eighthEdit = new MonitoredEdit () {
      public String[] getParameterNames     () { return new String [] {"blah1", "blah2"};}
      public Object[] getParameterValues    () { return new Object [] {"obj1",  "obj2" };}
      public String getPresentationName     () { return "blah"; }
      public String getUndoPresentationName () { return "undo_blah"; }
      public String getRedoPresentationName () { return "redo_blah"; }
      public boolean isSignificant () { return false; }
      public boolean replaceEdit (UndoableEdit edit) { return false; }
      public boolean addEdit (UndoableEdit edit) { return false; }
      public void die () {/*zip*/}
      public boolean canRedo () { return true; }
      public boolean canUndo () { return true; }
      public void redo () {/*zip*/}
      public void undo () {/*zip*/}
      public void restoreContext (java.util.HashMap context) {/*zip*/}
      public String getDescription () { return ""; }
      public Object clone () { return null; }
      public Icon getLargeIcon () { return NULL_ICON; }
      public Icon getIcon () { return NULL_ICON; }
      public String getName () { return "blah"; }
    };

    new MonitoredEditGroup ("First Level");
    new MonitoredEditGroup ("Second Level");

    evtSrc = new EditEventSource ();
    evtSrc.addUndoableEditListener (editsPanel.getUndoManager ());

    JButton fireEditButton            = new JButton ("Fire an edit");
    JButton fireNonUndoableEditButton = new JButton ("Fire a non-undoable edit");
    fireEditButton.addActionListener (new ActionListener()
    {
      public void actionPerformed (ActionEvent e)
      {
        fireEvent (UNDOABLE);
      }
    });
    fireNonUndoableEditButton.addActionListener (new ActionListener()
    {
      public void actionPerformed (ActionEvent e)
      {
        fireEvent (NON_UNDOABLE);
      }
    });
    editGenPanel.add (fireEditButton);
    editGenPanel.add (fireNonUndoableEditButton);

    evtSrc.fire (firstEdit);
    evtSrc.fire (secondEdit);
    evtSrc.fire (fourthEdit);
    evtSrc.fire (seventhEdit);
    evtSrc.fire (thirdEdit);
    evtSrc.fire (fifthEdit);
    evtSrc.fire (sixthEdit);
    evtSrc.fire (eighthEdit);

    TestFrame testMonitorPanel = new TestMonitorPanelGUI ();
    testMonitorPanel.getContentPane ().setLayout (new BorderLayout ());
    testMonitorPanel.getContentPane ().add (editsPanel, BorderLayout.CENTER);
    testMonitorPanel.getContentPane ().add (editGenPanel, BorderLayout.SOUTH);
    testMonitorPanel.setSize (500, 600);
    testMonitorPanel.setVisible (true);
  }

  public static UndoableEdit createEdit (String name, boolean undoable)
  {
    UndoableEdit edit = null;
    if (undoable)
    {
      edit = new StateEdit (new DummyState (name), name);
      ((StateEdit)edit).end ();
    }
    else
    {
      edit = new NonUndoableEdit ("NUE " + name);
    }
    return edit;
  }

  protected static EditEventSource evtSrc;
  private static int evtCounter = 0;
  public static void fireEvent (boolean undoable)
  {
    UndoableEdit newEdit = createEdit ("newEdit " + (evtCounter++), undoable);
    evtSrc.fire (newEdit);
  }
}


