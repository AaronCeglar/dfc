package test.ui.monitor;

import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;

public class NonUndoableEdit extends DummyMonitoredEdit
{

  public NonUndoableEdit ()
  {
    super ("Non-undoable edit");
  }

  public NonUndoableEdit (String name)
  {
    super (name);
  }

  public void undo () throws CannotUndoException
  {
    throw new CannotUndoException ();
  }

  public void redo () throws CannotRedoException
  {
    throw new CannotRedoException ();
  }

  public boolean canUndo ()
  {
    return false;
  }

  public boolean canRedo ()
  {
    return false;
  }
}