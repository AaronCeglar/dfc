package test.ui.monitor;

import javax.swing.JButton;

import dsto.dfc.swing.undo.AbstractMonitoredEdit;

public class DummyMonitoredEdit extends AbstractMonitoredEdit
{
  static String description = "Dummy MonitoredEdit for testing purposes";

  String    name;
  String [] names  = new String [] { "shrek", "fiona", "donkey" };
  Object [] values = new Object [] { new Object (), null, new JButton ("test") };

  public DummyMonitoredEdit ()
  {
    this ("ParameterEdit");
  }

  public DummyMonitoredEdit (String name)
  {
    this.name = name;
  }

  public DummyMonitoredEdit (String [] names, Object [] values)
  {
    this.name   = "dummy";
    this.names  = names;
    this.values = values;
  }

  public String [] getParameterNames ()
  {
    return names;
  }

  public Object [] getParameterValues ()
  {
    return values;
  }

  public String getPresentationName()
  {
    return name;
  }

  public String getUndoPresentationName()
  {
    return getPresentationName ();
  }

  public String getRedoPresentationName()
  {
    return getPresentationName ();
  }

  public String getDescription ()
  {
    return description;
  }

  // Copyable interface

  public Object clone ()
  {
    return new DummyMonitoredEdit (name);
  }

  // Serializable methods

  private void writeObject (java.io.ObjectOutputStream out) throws java.io.IOException
  {
    Object tmp1 = values[0];
    Object tmp2 = values[2];
    values[0] = null;
    values[2] = null;
    out.defaultWriteObject ();
    values[0] = tmp1;
    values[2] = tmp2;
  }

  private void readObject (java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException
  {
    in.defaultReadObject ();
    values[0] = new Object ();
    values[2] = new JButton ("test");
  }
}