package test.ui;

/**
 * Title:        DFC General User Interface package
 * Description:
 * Copyright:    Copyright (c) 1999
 * Company:      ITD/DSTO
 * @author Matthew Phillips
 * @version
 */

import java.awt.event.ActionEvent;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import dsto.dfc.swing.controls.DfcBoundingBox;
import dsto.dfc.swing.controls.NumericSpinner;

public class TestDfcBoundingBox extends JFrame
{

  DfcBoundingBox bb;
  NumericSpinner spin1, spin2, spin3, spin4;

  public TestDfcBoundingBox()
  {
    JPanel jPanel = new JPanel(new BoxLayout (this, BoxLayout.Y_AXIS));
    JCheckBox checkbox = new JCheckBox("Turn it on...");
    spin1 = new NumericSpinner(-180, 180, 20);
    spin2 = new NumericSpinner(-180, 180, 0);
    spin3 = new NumericSpinner(-80, 80, -20);
    spin4 = new NumericSpinner(-80, 80, 0);
    bb = new DfcBoundingBox(spin2.getModel(),
          spin4.getModel(), spin1.getModel(), spin3.getModel(), true);

    checkbox.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        useBoundingBox_actionPerformed(e);
      }
    });

    this.getContentPane().setLayout(new BoxLayout (this, BoxLayout.Y_AXIS));
    this.getContentPane().add(checkbox);
    this.getContentPane().add(bb);
    jPanel.add(spin1);
    jPanel.add(spin3);
    jPanel.add(spin2);
    jPanel.add(spin4);
    this.getContentPane().add(jPanel);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    bb.setEnabled(false);
    spin1.setEnabled(false);
    spin2.setEnabled(false);
    spin3.setEnabled(false);
    spin4.setEnabled(false);
  }

  void useBoundingBox_actionPerformed(ActionEvent e)
  {
    JCheckBox checkbox = (JCheckBox)e.getSource();
    bb.setEnabled(checkbox.isSelected());
    spin1.setEnabled(checkbox.isSelected());
    spin2.setEnabled(checkbox.isSelected());
    spin3.setEnabled(checkbox.isSelected());
    spin4.setEnabled(checkbox.isSelected());
  }

  public static void main(String[] args)
  {
    TestDfcBoundingBox testDfcBoundingBox1 = new TestDfcBoundingBox();
    testDfcBoundingBox1.pack();
    testDfcBoundingBox1.setVisible(true);
  }
}