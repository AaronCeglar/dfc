package test.commands;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.JTree;
import javax.swing.tree.TreePath;

import dsto.dfc.swing.commands.CommandMenus;
import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.swing.commands.CommandViewTreeModel;
import dsto.dfc.test.TestFrame;

public class TestCommandMenus extends TestFrame
{
  CommandView mainMenuView = new CommandView (CommandView.MAIN_MENU_VIEW);

  TestCommand fileOpen = new TestCommand ("file.Open", "File.oc", "file_open.gif");
  TestCommand fileClose = new TestCommand ("file.Close", "File.oc", "file_close.gif");
  TestCommand fileNew = new TestCommand ("file.New", "File.new");
  TestCommand fileExit = new TestCommand ("file.Exit", "File.exit");
  TestCommand editCopy = new TestCommand ("edit.Copy", "Edit.cnp", "edit_copy.gif");
  TestCommand editPaste = new TestCommand ("edit.Paste", "Edit.cnp", "edit_paste.gif");
  TestCommand editProperties = new TestCommand ("edit.Properties", "Edit.props");

  CommandView subMainMenuView = new CommandView (CommandView.MAIN_MENU_VIEW);
  CommandView sub1MainMenuView = new CommandView (CommandView.MAIN_MENU_VIEW);
  CommandView sub2MainMenuView = new CommandView (CommandView.MAIN_MENU_VIEW);
  TestCommand fileProperties = new TestCommand ("file.Properties", "File.props.Props.props");
  TestCommand fileRename = new TestCommand ("file.Rename", "File.oc");
  TestCommand editDelete = new TestCommand ("edit.Delete", "Edit.cnp");
  TestCommand editDelete2 = new TestCommand ("edit.Delete", "Edit.cnp", "edit_cut.gif");

  boolean changed = false;
//  DfcTree mainTree = new DfcTree();
  JTree mainTree = new JTree();
  JPanel buttonPanel = new JPanel();
  JButton changeButton = new JButton();
  JScrollPane mainScrollPane = new JScrollPane();
  JPanel centerPanel = new JPanel();
  JScrollPane subScrollPane = new JScrollPane();
//  DfcTree subTree = new DfcTree();
  JTree subTree = new JTree();
  GridBagLayout gridBagLayout1 = new GridBagLayout();
  JMenuBar mainMenuBar = new JMenuBar();
  JToggleButton sub1Button = new JToggleButton();
  JToggleButton sub2Button = new JToggleButton();
  ButtonGroup buttonGroup = new ButtonGroup();

  public TestCommandMenus ()
  {
    fileOpen.setInteractive (true);
    
    mainMenuView.addCommand (fileOpen);
    mainMenuView.addCommand (fileClose);
    mainMenuView.addCommand (fileNew);
    mainMenuView.addCommand (fileExit);
    mainMenuView.addCommand (editCopy);
    mainMenuView.addCommand (editPaste);
    mainMenuView.addCommand (editProperties);

    sub1MainMenuView.addCommand (fileProperties);
    sub1MainMenuView.addCommand (editDelete);

    sub2MainMenuView.addCommand (fileRename);
    sub2MainMenuView.addCommand (editDelete2);

    subMainMenuView.addView (sub1MainMenuView);
    subMainMenuView.addView (sub2MainMenuView);

//    CommandMenus.makePopupMenu (fileMenu.getPopupMenu (), mainMenuView);
    CommandMenus.makeMenuBar (mainMenuBar, mainMenuView);

    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }

    mainTree.setModel (new CommandViewTreeModel (mainMenuView.getTreeRoot ()));
    subTree.setModel (new CommandViewTreeModel (subMainMenuView.getTreeRoot ()));
    mainTree.expandRow (0);
    subTree.expandRow (0);

    expandAll (mainTree);
    expandAll (subTree);
  }

  public static void main(String[] args)
  {
    TestCommandMenus testCommands = new TestCommandMenus ();
    testCommands.setSize (600, 400);
    testCommands.setVisible (true);
  }

  private void jbInit() throws Exception
  {
    buttonGroup.add (sub1Button);
    buttonGroup.add (sub2Button);
    changeButton.setText("Change");
    changeButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        changeButton_actionPerformed(e);
      }
    });
    centerPanel.setLayout(gridBagLayout1);
    this.setJMenuBar(mainMenuBar);
    sub1Button.setText("Sub 1 Active");
    sub1Button.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        sub1Button_actionPerformed(e);
      }
    });
    sub2Button.setText("Sub 2 Active");
    sub2Button.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        sub2Button_actionPerformed(e);
      }
    });
    this.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
    buttonPanel.add(changeButton, null);
    buttonPanel.add(sub1Button, null);
    buttonPanel.add(sub2Button, null);
    this.getContentPane().add(centerPanel, BorderLayout.CENTER);
    centerPanel.add(mainScrollPane, new GridBagConstraints(0, 0, 1, 1, 0.5, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
    mainScrollPane.getViewport().add(mainTree, null);
    centerPanel.add(subScrollPane, new GridBagConstraints(1, 0, 1, 1, 0.5, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
    subScrollPane.getViewport().add(subTree, null);
  }

  void expandAll (JTree tree)
  {
    expandAll (tree, new TreePath (new Object [] {tree.getModel ().getRoot ()}));
  }

  void expandAll (JTree tree, TreePath path)
  {
    Object [] newPath = new Object [path.getPathCount () + 1];
    System.arraycopy (path.getPath (), 0, newPath, 0, path.getPathCount ());

    tree.expandPath (path);

    Object entry = path.getLastPathComponent ();

    for (int i = tree.getModel ().getChildCount (entry) - 1; i >= 0; i--)
    {
      newPath [newPath.length - 1] = tree.getModel ().getChild (entry, i);
      expandAll (tree, new TreePath (newPath)); 
    }
  }
  
  void changeButton_actionPerformed(ActionEvent e)
  {
    if (!changed)
    {
      changed = true;
//      mainMenuView.addCommand (fileNew, 0);
      mainMenuView.addView (subMainMenuView);
      fileClose.setEnabled (false);
      fileClose.setGroupInView (CommandView.MAIN_MENU_VIEW, "File.foobar");
      
//      fileNew.setGroupInView (CommandView.MAIN_MENU_VIEW, "file.oc");
    } else
    {
//      mainMenuView.removeCommand (fileNew);
//      mainMenuView.removeCommand (fileExit);
//      subMainMenuView.removeCommand (fileRename);
//      subMainMenuView.removeCommand (fileProperties);
//      mainMenuView.removeCommand (editPaste);
      mainMenuView.removeView (subMainMenuView);
      fileClose.setEnabled (true);
      fileClose.setGroupInView (CommandView.MAIN_MENU_VIEW, "File.oc");
//      fileNew.setGroupInView (CommandView.MAIN_MENU_VIEW, null);
      changed = false;
    }

    expandAll (mainTree);
    expandAll (subTree);
  }

  void sub1Button_actionPerformed(ActionEvent e)
  {
    sub1MainMenuView.activate ();
  }

  void sub2Button_actionPerformed(ActionEvent e)
  {
    sub2MainMenuView.activate ();
  }
}
