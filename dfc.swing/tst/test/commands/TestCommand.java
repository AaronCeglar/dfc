package test.commands;

import javax.swing.Icon;
import javax.swing.KeyStroke;

import dsto.dfc.swing.commands.Command;
import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.swing.icons.ImageLoader;
import dsto.dfc.util.BasicPropertyEventSource;

public class TestCommand extends BasicPropertyEventSource implements Command
{
  String name;
  String mainMenuGroup;
  Icon icon;
  boolean enabled = true;
  boolean interactive = false;
  
  public TestCommand (String name, String mainMenuGroup)
  {
    this (name, mainMenuGroup, null);
  }

  public TestCommand (String name, String mainMenuGroup, String iconName)
  {
    this.name = name;
    this.mainMenuGroup = mainMenuGroup;
    if (iconName != null)
      this.icon = ImageLoader.getIcon ("/dsto/dfc/icons/" + iconName);
  }

  public void execute ()
  {
    System.out.println ("executed " + getName ());
  }

  public String getDisplayName ()
  {
    return name;
  }

  public String getDescription ()
  {
    return "";
  }

  public String getLogString ()
  {
    return "";
  }

  public boolean isEnabled ()
  {
    return enabled;
  }

  public void setEnabled (boolean enabled)
  {
    boolean oldValue = this.enabled;
    this.enabled = enabled;
    firePropertyChange ("enabled", oldValue, enabled);
  }

  public boolean isInteractive ()
  {
    return interactive;
  }

  public void setInteractive (boolean newValue)
  {
    boolean oldValue = this.interactive;
    this.interactive = newValue;
    firePropertyChange ("interactive", oldValue, newValue);
  }

  public String getGroupInView (String viewName)
  {
    if (viewName.equals (CommandView.MAIN_MENU_VIEW))
      return mainMenuGroup;
    else
      return null;
  }

  public void setGroupInView (String viewName, String group)
  {
    if (viewName.equals (CommandView.MAIN_MENU_VIEW))
    {
      String oldGroup = mainMenuGroup;
      mainMenuGroup = group;

      firePropertyChange ("groupInView", oldGroup, mainMenuGroup);
    }
  }

  public boolean canReplace (Command command)
  {
    return false;
  }

  public Icon getIcon ()
  {
    return icon;
  }

  public String getName ()
  {
    return name;
  }

  public Icon getLargeIcon ()
  {
    return null;
  }

  public String toString ()
  {
    return name;
  }

  public int hashCode ()
  {
    return name.hashCode ();
  }

  public boolean equals (Object o)
  {
    if (o instanceof Command)
    {
      return ((Command)o).getName ().equals (name);
    } else
      return super.equals (o);
  }

  public KeyStroke getAccelerator ()
  {
    return null;
  }

  public char getMnemonic ()
  {
    return 0;
  }
}
