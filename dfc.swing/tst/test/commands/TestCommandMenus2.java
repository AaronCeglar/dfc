package test.commands;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.TreePath;

import dsto.dfc.swing.commands.Command;
import dsto.dfc.swing.commands.CommandMenus;
import dsto.dfc.swing.commands.CommandView;
import dsto.dfc.swing.commands.CommandViewTreeModel;
import dsto.dfc.swing.tree.DfcTree;
import dsto.dfc.test.TestFrame;

public class TestCommandMenus2 extends TestFrame
{
  CommandView view1 = new CommandView (CommandView.MAIN_MENU_VIEW);
  CommandView view1_1 = new CommandView (CommandView.MAIN_MENU_VIEW);
  CommandView view2 = new CommandView (CommandView.MAIN_MENU_VIEW);
  CommandView view3 = new CommandView (CommandView.MAIN_MENU_VIEW);
  Command bogusCommand = new TestCommand ("View.group1.bogus", "View.group1");
  
  boolean changed = false;
  DfcTree mainTree = new DfcTree();
  JPanel buttonPanel = new JPanel();
  JButton changeButton = new JButton();
  JScrollPane mainScrollPane = new JScrollPane();
  JPanel centerPanel = new JPanel();
  JScrollPane subScrollPane = new JScrollPane();
  DfcTree subTree = new DfcTree();
  GridBagLayout gridBagLayout1 = new GridBagLayout();
  JMenuBar mainMenuBar = new JMenuBar();
  ButtonGroup buttonGroup = new ButtonGroup();

  public TestCommandMenus2 ()
  {
    view1.addCommand (new TestCommand ("File.group1.1", "File.group1"));
    view1.addCommand (new TestCommand ("File.group2.1", "File.group2"));
    view1.addCommand (new TestCommand ("File.group3.1", "File.group3"));

    view1_1.addCommand (new TestCommand ("File.group2.2", "File.group2"));

    view2.addCommand (new TestCommand ("File.group1.2", "File.group1"));
    view2.addCommand (new TestCommand ("File.group2.2", "File.group2"));
    view2.addCommand (new TestCommand ("File.group3.2", "File.group3"));

    view3.addCommand (new TestCommand ("View.group1.3", "View.group1"));
    view3.addCommand (new TestCommand ("View.group2.3", "View.group2"));
    view3.addCommand (new TestCommand ("View.group3.3", "View.group3"));

    view1.addView (view2);

    CommandMenus.makeMenuBar (mainMenuBar, view1);

    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }

    mainTree.setModel (new CommandViewTreeModel (view1.getTreeRoot ()));
    subTree.setModel (new CommandViewTreeModel (view2.getTreeRoot ()));
    mainTree.expandRow (0);
    subTree.expandRow (0);

    expandAll (mainTree);
    expandAll (subTree);
  }

  public static void main(String[] args)
  {
    TestCommandMenus2 testCommands = new TestCommandMenus2 ();
    testCommands.setSize (600, 400);
    testCommands.setVisible (true);
  }

  private void jbInit() throws Exception
  {
    changeButton.setText("Change");
    changeButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        changeButton_actionPerformed(e);
      }
    });
    centerPanel.setLayout(gridBagLayout1);
    this.setJMenuBar(mainMenuBar);
    this.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
    buttonPanel.add(changeButton, null);
    this.getContentPane().add(centerPanel, BorderLayout.CENTER);
    centerPanel.add(mainScrollPane, new GridBagConstraints(0, 0, 1, 1, 0.5, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
    mainScrollPane.getViewport().add(mainTree, null);
    centerPanel.add(subScrollPane, new GridBagConstraints(1, 0, 1, 1, 0.5, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
    subScrollPane.getViewport().add(subTree, null);
  }

  void expandAll (JTree tree)
  {
    expandAll (tree, new TreePath (new Object [] {tree.getModel ().getRoot ()}));
  }

  void expandAll (JTree tree, TreePath path)
  {
    Object [] newPath = new Object [path.getPathCount () + 1];
    System.arraycopy (path.getPath (), 0, newPath, 0, path.getPathCount ());

    tree.expandPath (path);

    Object entry = path.getLastPathComponent ();

    for (int i = tree.getModel ().getChildCount (entry) - 1; i >= 0; i--)
    {
      newPath [newPath.length - 1] = tree.getModel ().getChild (entry, i);
      expandAll (tree, new TreePath (newPath)); 
    }
  }
  
  void changeButton_actionPerformed(ActionEvent e)
  {
    if (!changed)
    {
      changed = true;
      view2.addView (view3);
      view1.addView (view1_1, 0);
      // view1.addCommand (bogusCommand, 0);

    } else
    {
      changed = false;
      view2.removeView (view3);
      view1.removeView (view1_1);
      // view1.removeCommand (bogusCommand);
    }

    expandAll (mainTree);
    expandAll (subTree);
  }
}
