package test.forms;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import dsto.dfc.swing.controls.RadioBox;
import dsto.dfc.swing.forms.FormDialog;
import dsto.dfc.swing.forms.JFileChooserFormEditor;
import dsto.dfc.swing.forms.JTextFieldFormEditor;
import dsto.dfc.swing.forms.PopupFormEditor;

public class TestFormPanel extends FormDialog
{
  private JPanel mainPanel = new JPanel ();
  private JLabel jLabel1 = new JLabel();
  private JTextField intField = new JTextField();
  private JLabel jLabel2 = new JLabel();
  private JTextField floatField = new JTextField();
  PopupFormEditor popupEditor = new PopupFormEditor();
  GridBagLayout gridBagLayout1 = new GridBagLayout();
  PopupFormEditor popupFileEditor =
    new PopupFormEditor(new JTextFieldFormEditor (), new JFileChooserFormEditor ());
  RadioBox radioBox = new RadioBox();
  JLabel jLabel3 = new JLabel();

  public TestFormPanel ()
  {
    super (null, "Test Forms", false);

    try
    {
      jbInit ();
      setDialogPanel (mainPanel);
    } catch (Exception e)
    {
      e.printStackTrace ();
    }

    addEditor ("integer", new JTextFieldFormEditor (intField));
    addEditor ("floating", new JTextFieldFormEditor (floatField));
    addEditor ("string", popupEditor);
    addEditor ("file", popupFileEditor);
    addEditor ("stringItem", radioBox);

    setEditorValue (new FormValue ());
  }

  public static void main (String[] args)
  {
    TestFormPanel window = new TestFormPanel();

    window.pack ();
    window.setVisible (true);
  }

  private void jbInit () throws Exception
  {
    jLabel1.setText("Enter an integer:");
    intField.setColumns(4);
    jLabel2.setText("Enter a float:");
    floatField.setColumns(4);
    mainPanel.setLayout(gridBagLayout1);
    jLabel3.setText("Select a button:");
    radioBox.setPossibleStringValues(new String[] {"Item 1", "Item 2", "Item 3"});
    mainPanel.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 63, 0, 0), 0, 0));
    mainPanel.add(intField, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 198), 0, 0));
    mainPanel.add(jLabel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 63, 0, 22), 0, 0));
    mainPanel.add(floatField, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 198), 0, 0));
    mainPanel.add(popupEditor, new GridBagConstraints(0, 3, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(16, 63, 0, 0), 0, 0));
    mainPanel.add(popupFileEditor, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 10, 0, 0), 0, 0));
    mainPanel.add(radioBox, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
    mainPanel.add(jLabel3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
            ,GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
  }

  public static final class FormValue
  {
    private int integer = 42;
    private float floating = 3.1415f;
    private String string = "hello world";
    private File file = new File ("c:\\config.sys");
    private String stringItem = "Item 2";

    public int getInteger ()
    {
      return integer;
    }

    public void setInteger (int value)
    {
      System.out.println ("set integer: " + value);

      this.integer = value;
    }

    public float getFloating ()
    {
      return floating;
    }

    public void setFloating (float newValue)
    {
      System.out.println ("set floating: " + newValue);
      
      floating = newValue;
    }

    public void setString (String newValue)
    {
      System.out.println ("set string: " + newValue);

      string = newValue;
    }

    public String getString ()
    {
      return string;
    }

    public void setStringItem (String newValue)
    {
      System.out.println ("set string item: " + newValue);

      stringItem = newValue;
    }

    public String getStringItem ()
    {
      return stringItem;
    }

    public void setFile (File newValue)
    {
      System.out.println ("set file: " + newValue);

      file = newValue;
    }

    public File getFile ()
    {
      return file;
    }

    public String toString ()
    {
      return "FormValue [integer = " + integer +
             ", floating = " + floating +
             ", string = " + string +
             ", file = " + file + "]";
    }
  }
}