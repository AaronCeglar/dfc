package test.logging;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

import dsto.dfc.logging.Log;

import dsto.dfc.swing.logging.LogWindowPopper;

public class TestLogWindow
{
  JFrame controlFrame = new JFrame ("Control");
  LogWindowPopper popper = new LogWindowPopper (controlFrame);  
  JPanel controlPanel = new JPanel ();
  JButton errorButton = new JButton();
  JButton warningButton = new JButton();

  public TestLogWindow()
  {
    controlFrame.getContentPane ().add (controlPanel, BorderLayout.CENTER);

    popper.setPopupOnAlarm (true);
    popper.setPopupOnWarning (true);

    try
    {
      jbInit ();
    } catch (Exception ex)
    {
      ex.printStackTrace ();
    }

    controlFrame.pack ();
    controlFrame.setVisible (true);
    popper.showLogWindow ();

    Log.info ("Information only", TestLogWindow.class);
    Log.warn ("A warning", TestLogWindow.class);
    Log.alarm ("Alarm!", TestLogWindow.class);
  }

  private void jbInit () throws Exception
  {
    errorButton.setText("Error");
    errorButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        errorButton_actionPerformed(e);
      }
    });
    warningButton.setText("Warning");
    warningButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        warningButton_actionPerformed(e);
      }
    });
    controlPanel.add(errorButton, null);
    controlPanel.add(warningButton, null);
  }
  
  //Main method
  public static void main(String[] args) throws Exception
  {
    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    new TestLogWindow();
  }

  void errorButton_actionPerformed(ActionEvent e)
  {
    Log.alarm ("Alarum!", this, new Exception ("exceptional!"));
  }

  void warningButton_actionPerformed(ActionEvent e)
  {
    Log.warn ("Warning, Will Robertson", this);
  }
} 
