package test.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import dsto.dfc.swing.table.DefaultDfcTableModel;
import dsto.dfc.swing.table.DfcTable;
import dsto.dfc.test.TestFrame;

public class TestDfcTableModel extends TestFrame
{
  DefaultDfcTableModel tableModel1 = new DefaultDfcTableModel();
  DefaultDfcTableModel tableModel2 = new DefaultDfcTableModel();
  JPanel jPanel1 = new JPanel();
  DfcTable table1 = new DfcTable();
  JScrollPane scrollPane1 = new JScrollPane();
  GridBagLayout gridBagLayout1 = new GridBagLayout();
  DfcTable table2 = new DfcTable();
  JScrollPane scrollPane2 = new JScrollPane();

  public TestDfcTableModel()
  {
    tableModel1.insertColumn (0, "Name", String.class);
    tableModel1.insertColumn (1, "Age", Integer.class);
    tableModel1.insertColumn (2, "Color", Color.class);
    tableModel1.insertRows (0, 2);
    tableModel1.setValueAt ("Matthew", 0, 0);
    tableModel1.setValueAt (new Integer (27), 0, 1);
    tableModel1.setValueAt (Color.green, 0, 2);
    tableModel1.setValueAt ("Anna", 1, 0);
    tableModel1.setValueAt (new Integer (25), 1, 1);
    tableModel1.setValueAt (Color.blue, 1, 2);


    tableModel2.insertColumn (0, "Name", String.class);
    tableModel2.insertColumn (1, "Age", Integer.class);
    tableModel2.insertColumn (2, "Color", Color.class);
    tableModel2.insertRows (0, 1);
    tableModel2.setValueAt ("Bob", 0, 0);
    tableModel2.setValueAt (new Integer (2), 0, 1);
    tableModel2.setValueAt (Color.red, 0, 2);

    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }

    table1.enableRenderCellsByClass (table1.getColumn ("Color"));
    table1.enableEditCellsByClass (table1.getColumn ("Color"));
    table1.enableRenderCellsByClass (table2.getColumn ("Color"));
    table1.enableEditCellsByClass (table2.getColumn ("Color"));
    table1.enableDragAndDrop ();
    table2.enableDragAndDrop ();
  }

  public static void main(String[] args)
  {
/*    String laf = UIManager.getSystemLookAndFeelClassName();

    try
    {
      UIManager.setLookAndFeel (laf);
    } catch (UnsupportedLookAndFeelException e)
    {
    } catch (Exception exc)
    {
    }*/

    TestDfcTableModel testDfcTableModel = new TestDfcTableModel();
    testDfcTableModel.setVisible (true);
  }

  private void jbInit() throws Exception
  {
    this.setTitle("Test DfcTable");
//    table.setRowSelectionAllowed (true);
//    table.setCellSelectionEnabled (false);
//    table.setColumnSelectionAllowed(false);
    table1.setCnpCommandsEnabled (true);
    table1.setModel(tableModel1);
    table2.setModel(tableModel2);
    table2.setCnpCommandsEnabled (true);
    table1.setRowHeight(23);
    jPanel1.setLayout(gridBagLayout1);
    table2.setRowHeight(23);
    this.getContentPane().add(jPanel1, BorderLayout.CENTER);
    jPanel1.add(scrollPane1, new GridBagConstraints(0, 0, 1, 1, 0.5, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    jPanel1.add(scrollPane2, new GridBagConstraints(1, 0, 1, 1, 0.5, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 10, 0, 0), 0, 0));
    scrollPane2.getViewport().add(table2, null);
    scrollPane1.getViewport().add(table1, null);
  }

  void deleteRowsButton_actionPerformed(ActionEvent e)
  {
    int [] rows = table1.getSelectedRows ();

    for (int i = 0; i < rows.length; i++)
      tableModel1.deleteRows (rows [i], rows [i]);
  }
}