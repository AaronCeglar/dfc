package test.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import dsto.dfc.swing.table.DefaultDfcTableModel;
import dsto.dfc.swing.table.DfcTable;
import dsto.dfc.swing.table.SortedTableModel;
import dsto.dfc.test.TestFrame;

public class TestSortedTable extends TestFrame
{
  DefaultDfcTableModel tableModel = new DefaultDfcTableModel();
  SortedTableModel sortedModel = new SortedTableModel (tableModel);
  JPanel jPanel = new JPanel();
  DfcTable table = new DfcTable();
  JScrollPane scrollPane = new JScrollPane();
  GridBagLayout gridBagLayout1 = new GridBagLayout();

  public TestSortedTable()
  {
    tableModel.insertColumn (0, "Name", String.class);
    tableModel.insertColumn (1, "Age", Integer.class);
    tableModel.insertColumn (2, "Color", Color.class);
    tableModel.insertRows (0, 4);
    tableModel.setValueAt ("Matthew", 0, 0);
    tableModel.setValueAt (new Integer (27), 0, 1);
    tableModel.setValueAt (Color.green, 0, 2);
    tableModel.setValueAt ("Anna", 1, 0);
    tableModel.setValueAt (new Integer (25), 1, 1);
    tableModel.setValueAt (Color.blue, 1, 2);
    tableModel.setValueAt ("Zeta", 2, 0);
    tableModel.setValueAt (new Integer (31), 2, 1);
    tableModel.setValueAt (Color.magenta, 2, 2);
    tableModel.setValueAt ("Zeta", 3, 0);
    tableModel.setValueAt (new Integer (1), 3, 1);
    tableModel.setValueAt (Color.magenta, 3, 2);

    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }

//    table.getTableHeader ().setDefaultRenderer (new ButtonHeaderRenderer ());
    table.enableRenderCellsByClass (table.getColumn ("Color"));
    table.enableEditCellsByClass (table.getColumn ("Color"));

    table.enableDragAndDrop ();
//    table.setAutoResizeMode (JTable.AUTO_RESIZE_OFF);

    sortedModel.addSortingColumn (0, false);
    sortedModel.addSortingColumn (1, true);
    sortedModel.addSortingColumn (2, true);
    sortedModel.sort ();

    table.getColumn ("Name").setMinWidth (10);
  }

  public static void main(String[] args)
  {
//    String laf = UIManager.getSystemLookAndFeelClassName();
//
//    try
//    {
//      UIManager.setLookAndFeel (laf);
//    } catch (UnsupportedLookAndFeelException e)
//    {
//    } catch (Exception exc)
//    {
//    }

    TestSortedTable testSortedTable = new TestSortedTable();
    testSortedTable.setVisible (true);
  }

  private void jbInit() throws Exception
  {
    this.setTitle("Test DfcTable");
    table.setCnpCommandsEnabled (true);
    table.setModel(sortedModel);
    table.setRowHeight(23);
    jPanel.setLayout(gridBagLayout1);
    this.getContentPane().add(jPanel, BorderLayout.CENTER);
    jPanel.add(scrollPane, new GridBagConstraints(0, 0, 1, 1, 0.5, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    scrollPane.getViewport().add(table, null);
  }
}