/**
 *
 */
package dsto.dfc.swing.dnd;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;

import junit.framework.TestCase;

/**
 *
 * @author WeberD
 * @created 03/03/2008
 */
public class JUTestObjectTransferable extends TestCase
{
  public void testFindCommonClass () throws FileNotFoundException
  {
    Object[] throwables =
      new Object[] { new RuntimeException (), new IOException (),
          new Throwable () };

    Class common = ObjectTransferable.findCommonClass (throwables);

    assertEquals ("java.lang.Throwable", common.getName ());


    Object[] objects =
      new Object[] { new File ("."), new String ("/"), new StringReader ("") };

    common = ObjectTransferable.findCommonClass (objects);

    assertEquals ("java.lang.Object", common.getName ());


    try
    {
      common =
        ObjectTransferable.findCommonClass (new Object[] { null, null, null });
      fail ("Should have thrown a NullPointerException");
    } catch (NullPointerException e)
    {
      // pass
    }
  }

}
