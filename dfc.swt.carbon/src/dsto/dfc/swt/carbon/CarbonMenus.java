package dsto.dfc.swt.carbon;

import org.eclipse.swt.internal.Callback;
import org.eclipse.swt.internal.carbon.HICommand;
import org.eclipse.swt.internal.carbon.OS;
import org.eclipse.swt.widgets.Display;

import dsto.dfc.swt.commands.Command;

/**
 * Extended menu support for Mac OS X.
 * <p>
 *
 * Adapted from code snippet at
 * http://dev.eclipse.org/viewcvs/index.cgi/org.eclipse.swt.snippets/src/org/eclipse/swt/snippets/Snippet178.java?rev=HEAD&content-type=text/vnd.viewcvs-markup
 *
 * @author Matthew Phillips
 */
public final class CarbonMenus
{
  private static final int kHICommandPreferences = ('p' << 24)
      + ('r' << 16) + ('e' << 8) + 'f';

  private static final int kHICommandAbout = ('a' << 24)
      + ('b' << 16) + ('o' << 8) + 'u';

  private static final int kHICommandServices = ('s' << 24)
      + ('e' << 16) + ('r' << 8) + 'v';

  private CarbonMenus ()
  {
    // zip
  }

  /**
   * Hook the OS X application menu "Preferences" and "About'
   * commands.
   *
   * @param display The display instance.
   * @param cmdAbout The "About" command.
   * @param cmdPreferences The "Preferences" command.
   */
  public static void hookAppMenu (Display display,
                                  Command cmdAbout,
                                  Command cmdPreferences)
  {
    final Callback commandCallback =
      new Callback (new CallbackTarget (cmdAbout, cmdPreferences),
                    "commandProc", 3);

    int commandProc = commandCallback.getAddress ();

    if (commandProc == 0)
    {
      commandCallback.dispose ();
      return; // give up
    }

    // Install event handler for commands
    int [] mask = new int []{OS.kEventClassCommand,
        OS.kEventProcessCommand};
    OS.InstallEventHandler (OS.GetApplicationEventTarget (),
                            commandProc, mask.length / 2, mask, 0,
                            null);

    // create About ... menu command
    String aboutName = cmdAbout.getDisplayName ();
    int [] outMenu = new int [1];
    short [] outIndex = new short [1];
    if (OS.GetIndMenuItemWithCommandID (0, kHICommandPreferences, 1,
                                        outMenu, outIndex) == OS.noErr
        && outMenu [0] != 0)
    {
      int menu = outMenu [0];

      int l = aboutName.length ();
      char buffer[] = new char [l];
      aboutName.getChars (0, l, buffer, 0);
      int str =
        OS.CFStringCreateWithCharacters (OS.kCFAllocatorDefault, buffer, l);
      OS.InsertMenuItemTextWithCFString (menu, str, (short)0, 0,
                                         kHICommandAbout);
      OS.CFRelease (str);

      // add separator between About & Preferences
      OS.InsertMenuItemTextWithCFString (menu, 0, (short)1,
                                         OS.kMenuItemAttrSeparator, 0);

      // enable pref menu
      OS.EnableMenuCommand (menu, kHICommandPreferences);

      // disable services menu
      OS.DisableMenuCommand (menu, kHICommandServices);
    }

    // schedule disposal of callback object
    display.disposeExec (new Runnable ()
    {
      public void run ()
      {
        commandCallback.dispose ();
      }
    });
  }

  /**
   * Handle OS event callbacks
   */
  private static class CallbackTarget
  {
    private Command cmdAbout;
    private Command cmdPreferences;

    public CallbackTarget (Command cmdAbout, Command cmdPreferences)
    {
      this.cmdAbout = cmdAbout;
      this.cmdPreferences = cmdPreferences;
    }

    @SuppressWarnings ("unused")
    public int commandProc (int nextHandler, int theEvent, int userData)
    {
      if (OS.GetEventKind (theEvent) == OS.kEventProcessCommand)
      {
        HICommand command = new HICommand ();
        OS.GetEventParameter (theEvent, OS.kEventParamDirectObject,
                              OS.typeHICommand, null,
                              HICommand.sizeof, null, command);
        switch (command.commandID)
        {
          case kHICommandPreferences:
            cmdPreferences.execute ();
            return OS.noErr;
          case kHICommandAbout:
            cmdAbout.execute ();
            return OS.noErr;
          default:
            break;
        }
      }

      return OS.eventNotHandledErr;
    }
  }
}
