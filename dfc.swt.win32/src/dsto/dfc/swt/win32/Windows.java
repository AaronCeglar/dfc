package dsto.dfc.swt.win32;

import java.lang.reflect.Method;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import dsto.dfc.logging.Log;
import dsto.dfc.util.Objects;
import dsto.dfc.util.win32.Win32;

/**
 * SWT Win32 access routines.
 * 
 * @author mpp
 * @version $Revision$
 */
public final class Windows
{
  protected static Shell messageWindow;
  protected static int dockedPosition;
  protected static Shell dockedWindow;
  protected static DockedWindowListener dockedWindowListener;
   
  static
  {
    Win32.initLibrary ();
  }
  
  private Windows ()
  {
    // zip
  }

  public static void registerMessageWindow ()
  {
    if (messageWindow == null)
    {
      messageWindow = new Shell (Display.getCurrent ());
    
      Win32.winRegisterMessageWindow (messageWindow.handle);
    }
  }

  public static void setAlwaysOnTop (Shell window, boolean onTop)
  {
    Win32.winSetAlwaysOnTop (window.handle, onTop);
  }
  
  /**
   * Set the window to be on top or bottom of the stack of standard windows.
   */
  public static void setOnTop (Shell shell, boolean onTop)
  {
    Win32.winSetOnTop (shell.handle, onTop);
  }

  /**
   * Dock a window with the Windows desktop. Only one window may
   * be docked at a time. Can be called again with same window and same
   * position to update docked size to current window size.
   *
   * @param window The window to dock.
   * @param position The position to dock the window at (DESKTOP_TOP,
   * DESKTOP_LEFT, DESKTOP_RIGHT or DESKTOP_BOTTOM).
   * @param width The requested width of the docked window.
   * @param height The requested height of the docked window.
   * @exception IllegalStateException if a window is already docked or
   * the handle for the window could not be retrieved.
   *
   * @see #desktopUndock(Shell)
   */
  public static void desktopDock (Shell window, int position,
                                    int width, int height)
    throws IllegalStateException
  {
    if (dockedWindow == null)
    { 
      Win32.winAppbarDock (window.handle, position, width, height);

      dockedWindow = window;
      dockedPosition = position;
      dockedWindowListener = new DockedWindowListener ();

      window.addDisposeListener (dockedWindowListener);
    } else if (dockedWindow == window && dockedPosition == position)
    {
      Point windowSize = window.getSize ();
      if (windowSize.x != width || windowSize.y != height)
      {
        Win32.winAppbarUndock (window.handle);
        Win32.winAppbarDock (window.handle, position, width, height);
      } 
    } else
    {
      throw new IllegalStateException
        ("Can only have one docked window at a time");
    }
  }

  /**
   * Undock a previously docked window from the Windows desktop.
   *
   * @param window The window to undock.
   * @exception IllegalStateException if window handle was not able to
   * be retrieved.
   *
   * @see #desktopDock(Shell, int, int, int)
   */
  public static void desktopUndock (Shell window)
    throws IllegalStateException
  {
    if (dockedWindow == window)
    {
      Win32.winAppbarUndock (window.handle);

      window.removeDisposeListener (dockedWindowListener);
      dockedWindowListener = null;
      dockedWindow = null;
    }
  }
  
  /**
   * Test if a full screen application is running.
   */
  public static boolean inFullScreenMode ()
    throws IllegalStateException
  {
    return Win32.winInFullScreenMode ();
  }

  /**
   * Get the current desktop area for the primary screen. On Windows, this
   * avoids treating the entire virtual display (ie possibly across multiple
   * monitors) as the desktop.
   * 
   * @deprecated As of SWT 3.0, use {@link Display#getPrimaryMonitor()}.
   */
  public static Rectangle getPrimaryDesktopArea (Display display)
  {
    if (SWT.getPlatform ().equals ("win32"))
    {
      try
      {
        // reflection code below avoids compilation errors on non-win32 and
        // is the equivalent of:
        
        // RECT rect = new RECT ();
        // OS.SystemParametersInfo (OS.SPI_GETWORKAREA, 0, rect, 0);
        //      
        // int width = rect.right - rect.left;
        // int height = rect.bottom - rect.top;
        //      
        // return new Rectangle (rect.left, rect.top, width, height);
        
        Class<?> rectClass = Class.forName ("org.eclipse.swt.internal.win32.RECT");
        Object rect = rectClass.newInstance ();
        Class<?> os = Class.forName ("org.eclipse.swt.internal.win32.OS");
        Method systemParametersInfo =
          os.getMethod ("SystemParametersInfo",
                        new Class [] {int.class, int.class, rectClass, int.class});
        systemParametersInfo.invoke
          (null, new Object [] {new Integer (0x30), new Integer (0),
                                 rect, new Integer (0)});
        
        int width =
            Objects.getIntField (rect, "right") - Objects.getIntField (rect, "left");
        int height =
            Objects.getIntField (rect, "bottom") - Objects.getIntField (rect, "top");
        
        return new Rectangle
          (Objects.getIntField (rect, "left"),
           Objects.getIntField (rect, "top"),
           width, height);

      } catch (Exception ex)
      {
        Log.diagnostic
          ("Failed to find Win32 desktop area", Windows.class, ex);
        
        return display.getClientArea ();
      }
    } else
    {
      return display.getClientArea ();
    }
  }
  
  static class DockedWindowListener implements DisposeListener
  {
    public void widgetDisposed (DisposeEvent e)
    {
      desktopUndock (dockedWindow);
    }
  } 
}
