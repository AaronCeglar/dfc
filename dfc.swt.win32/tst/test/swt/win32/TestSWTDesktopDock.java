package test.swt.win32;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import dsto.dfc.swt.win32.Windows;
import dsto.dfc.util.win32.Win32;

/**
 * 
 * 
 * @author mpp
 * @version $Revision$
 */
public class TestSWTDesktopDock
{
  public static void main (String[] args)
  {
    Display display = new Display ();
    final Shell shell = new Shell (display);
    shell.setLayout (new RowLayout (SWT.VERTICAL));
    
    final Button top = new Button (shell, SWT.CENTER);
    final Button bottom = new Button (shell, SWT.CENTER);
    final Button left = new Button (shell, SWT.CENTER);
    final Button right = new Button (shell, SWT.CENTER);
    final Button undock = new Button (shell, SWT.CENTER);
    
    top.setText ("Top");
    bottom.setText ("Bottom");
    right.setText ("Right");
    left.setText ("Left");
    undock.setText ("Undock");
    
    SelectionListener listener = new SelectionListener ()
    {
      public void widgetSelected (SelectionEvent e)
      {
        Point size = shell.getSize ();
        
        if (e.getSource () == top)
          Windows.desktopDock (shell, Win32.DESKTOP_TOP, size.x, size.y);
        else if (e.getSource () == bottom)
          Windows.desktopDock (shell, Win32.DESKTOP_BOTTOM, size.x, size.y);
        else if (e.getSource () == left)
          Windows.desktopDock (shell, Win32.DESKTOP_LEFT, size.x, size.y);
        else if (e.getSource () == right)
          Windows.desktopDock (shell, Win32.DESKTOP_RIGHT, size.x, size.y);
        else if (e.getSource () == undock)
          Windows.desktopUndock (shell);
      }
      
      public void widgetDefaultSelected (SelectionEvent e)
      {
        // zip
      }
    };
    
    top.addSelectionListener (listener);
    bottom.addSelectionListener (listener);
    right.addSelectionListener (listener);
    left.addSelectionListener (listener);
    undock.addSelectionListener (listener);
    
    shell.pack ();
    
    shell.open ();
    
    while (!shell.isDisposed ())
    {
      if (!display.readAndDispatch ())
        display.sleep ();
    }
    
    System.out.println ("exit");
    display.dispose ();
  }
}
